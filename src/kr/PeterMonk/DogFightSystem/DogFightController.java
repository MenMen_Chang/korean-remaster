package kr.PeterMonk.DogFightSystem;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import javolution.util.FastTable;
import l1j.server.L1DatabaseFactory;
import l1j.server.server.ActionCodes;
import l1j.server.server.GeneralThreadPool;
import l1j.server.server.ObjectIdFactory;
import l1j.server.server.datatables.ItemTable;
import l1j.server.server.datatables.ShopTable;
import l1j.server.server.model.Broadcaster;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1NpcInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.model.shop.L1Shop;
import l1j.server.server.serverpackets.S_AttackPacket;
import l1j.server.server.serverpackets.S_DoActionGFX;
import l1j.server.server.serverpackets.S_HPMeter;
import l1j.server.server.serverpackets.S_NpcChatPacket;
import l1j.server.server.serverpackets.S_SkillSound;
import l1j.server.server.templates.L1Npc;
import l1j.server.server.templates.L1ShopItem;

public class DogFightController implements Runnable {
	private static DogFightController _instance;

	private static int FIGHT_INTERVAL = 1 * 60 * 1000;

	public static final int EXECUTE_STATUS_NONE = 0;
	public static final int EXECUTE_STATUS_PREPARE = 1;
	public static final int EXECUTE_STATUS_READY = 2;
	public static final int EXECUTE_STATUS_STANDBY = 3;
	public static final int EXECUTE_STATUS_PROGRESS = 4;
	public static final int EXECUTE_STATUS_FINALIZE = 5;
	
	private int _executeStatus = EXECUTE_STATUS_NONE;

	public int _FightCount = 0;
	long _nextFightTime = System.currentTimeMillis() + 60 * 1000;
	public int _dogfightState = 2;

	public int _ticketSellRemainTime;
	public int _FightWatingTime;
	public int _currentBroadcastFighter;

	L1NpcInstance[] _npc = new L1NpcInstance[3];

	public int[] _ticketCount = new int[2];
	public int[] _ticketId = new int[2];
	private static Random _rnd = new Random(System.nanoTime());
	private static DecimalFormat _df = new DecimalFormat("#.#");

	public int _ranking = 0;
	public boolean _complete = false;

	List<L1ShopItem> _purchasingList = new ArrayList<L1ShopItem>();
	public L1NpcInstance[] _dogfight = new L1NpcInstance[2];

	int Lucky = 0;
	private static Random rnd = new Random(System.nanoTime());

	/** 투견 추가 **/
	private final HashMap<Integer, L1DogFightTicket> _fight = new HashMap<Integer, L1DogFightTicket>(32);
	
	public HashMap<Integer, L1DogFightTicket> getAllTemplates() {
		return _fight;
	}

	private int Start_X[] = { 33528, 33528 };
	private int Start_Y[] = { 32864, 32866 };
	public int[] Number = { 1, 2 };

	private static final ArrayList<DogStruct> _dogs;
	static{
		_dogs = new ArrayList<DogStruct>(32);
		_dogs.add(new DogStruct(1, 17133, "# 도베르만"));
		_dogs.add(new DogStruct(2, 17172, "# 세퍼드"));
		_dogs.add(new DogStruct(3, 17204, "# 허스키"));
		_dogs.add(new DogStruct(4, 17239, "# 진돗개"));
		_dogs.add(new DogStruct(5, 17211, "# 콜리"));
		_dogs.add(new DogStruct(6, 17168, "# 세인트"));
		_dogs.add(new DogStruct(7, 17197, "# 유니콘"));
		_dogs.add(new DogStruct(8, 17315, "# 그리폰"));
		_dogs.add(new DogStruct(9, 17278, "# 드래곤"));
		_dogs.add(new DogStruct(11, 17282, "# 해츨링"));
		_dogs.add(new DogStruct(12, 17298, "# 해츨링"));
		_dogs.add(new DogStruct(13, 17247, "# 호랑이"));
		_dogs.add(new DogStruct(14, 17121, "# 늑대"));
	}
	
	private int[] SKILLID = {  17324, 17322, 17320, 17318, 17322, 17320 };
	private int[] WINEFFECT = {  17376, 17340, 17356, 17344, 17392, 17352, 17360, 17396, 17328, 17376, 17392, 17252, 17364 };

	public static int[] _time = new int[2];
	public static String _first = null;
	
	public int[] ticket = { 0, 0 };
	
	public double[] _winRate = { 0, 0 };
	
	public String[] _FightCondition = { "좋음", "좋음" };
	
	public double _ration[] = { 0, 0 };

	public static DogFightController getInstance() {
		if (_instance == null) {
			_instance = new DogFightController();
		}
		return _instance;
	}
	
	
	public void run() {
		try {
			switch (_executeStatus) {
			case EXECUTE_STATUS_NONE: {
				if (checkStartFight()) {
					initFightGame();
					_executeStatus = EXECUTE_STATUS_PREPARE;
					GeneralThreadPool.getInstance().schedule(this, 30 * 1000L);
				} else {
					GeneralThreadPool.getInstance().schedule(this, 1000L); // 1초
				}
			}
				break;
			case EXECUTE_STATUS_PREPARE: {
				startSellTicket();
				_executeStatus = EXECUTE_STATUS_READY;
				GeneralThreadPool.getInstance().schedule(this, 1000L);
			}
				break;
			case EXECUTE_STATUS_READY: {
				long remainTime = checkTicketSellTime();
				if (remainTime > 0) {
					GeneralThreadPool.getInstance().schedule(this, remainTime);
				} else {
					_executeStatus = EXECUTE_STATUS_STANDBY;
					GeneralThreadPool.getInstance().schedule(this, 1000L);
				}
			}
				break;
			case EXECUTE_STATUS_STANDBY: {
				if (checkWatingTime()) {
					initShopNpc();
					startFight();
					_executeStatus = EXECUTE_STATUS_PROGRESS;
				}
				GeneralThreadPool.getInstance().schedule(this, 1000L);
			}
				break;

			case EXECUTE_STATUS_PROGRESS: {
				if (broadcastBettingRate()) {
					if (_complete) {
						_executeStatus = EXECUTE_STATUS_FINALIZE;
					}
				}
				GeneralThreadPool.getInstance().schedule(this, 1000L);
			}
				break;
			case EXECUTE_STATUS_FINALIZE: {
				_FightCount = _FightCount + 1;
				setdogState(2);
				broadcastNpc("다음경기를 준비하는데 " + 1 + "분이 소요됩니다");
				_executeStatus = EXECUTE_STATUS_NONE;
				GeneralThreadPool.getInstance().schedule(this, 1000L);
			}
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public boolean checkStartFight() {
		long currentTime = System.currentTimeMillis();
		if (_nextFightTime < currentTime) {
			_nextFightTime = currentTime + FIGHT_INTERVAL;
			return true;
		}
		return false;
	}

	public void initNpc() {
		L1NpcInstance n = null;
		for (Object obj : L1World.getInstance().getVisibleObjects(4).values()) {
			if (obj instanceof L1NpcInstance) {
				n = (L1NpcInstance) obj;
				if (n.getNpcTemplate().get_npcId() == 170041) { //투견 티켓 상인
					_npc[0] = n;
				} 
				if (n.getNpcTemplate().get_npcId() == 170042) { //투견 도우미
					_npc[1] = n;
				} 
			}
		}
	}
	
	public void initShopNpc() {
		List<L1ShopItem> sellingList = new ArrayList<L1ShopItem>();

		L1Shop shop = new L1Shop(170041, sellingList, _purchasingList);
		ShopTable.getInstance().addShop(170041, shop);
	}
	
	private void sleepTime() {
		for (int i = 0; i < 2; i++) {
			int dogState = _rnd.nextInt(5);
			int addValue = 0;

			switch (dogState) {
			case 0:
				_FightCondition[i] = "좋음";
				addValue = -15;
				break;
			case 1:
				_FightCondition[i] = "좋음";
				addValue = -10;
				break;
			case 2:
				_FightCondition[i] = "나쁨";
				addValue = 10;
				break;
			case 3:
				_FightCondition[i] = "나쁨";
				addValue = 15;
				break;
			default:
				_FightCondition[i] = "보통";
				addValue = 5;
				break;
			}
			_time[i] = 1100 + addValue;
		}
	}
	
	private FastTable<L1NpcInstance> list = new FastTable<L1NpcInstance>();

	public Iterator<L1NpcInstance> get_Fight_iter() {
		return list.iterator();
	}

	public void initWinRate() {
		L1DogFight dogfight = null;
		for (int i = 0; i < 2; i++) {
			dogfight = DogFightTable.getInstance().getTemplate(_dogfight[i].get_num());
			double rate = (double) dogfight.getWinCount() * 100.0 / (double) (dogfight.getWinCount() + dogfight.getLoseCount());
			_winRate[i] = Double.parseDouble(_df.format(rate));
		}
	}
	
	public void setSpeed(int i, int speed){
		_time[i] = speed;
	}
	
	public int getSpeed(int i){
		return _time[i];
	}
	
	private void loadDog() {
		L1Npc dogs = null;
		List<L1PcInstance> players = null;
		
		list.clear();
		
		Collections.shuffle(_dogs);
		for (int m = 0; m < 2; ++m) {
			try {
				DogStruct bs = _dogs.get(m);
				dogs = new L1Npc();
				dogs.set_family(0);
				dogs.set_agrofamily(0);
				dogs.set_picupitem(false);

				Object[] parameters = { dogs };

				_dogfight[m] = (L1NpcInstance) Class.forName("l1j.server.server.model.Instance.L1NpcInstance")
						.getConstructors()[0].newInstance(parameters);
				
				_dogfight[m].getGfxId().setGfxId(bs.gfx);
				_dogfight[m].getGfxId().setTempCharGfx(bs.gfx);

				if (m == 0)
					_dogfight[m].setNameId("홀"+bs.name);
				else 
					_dogfight[m].setNameId("짝"+bs.name);
				_dogfight[m].setName(_dogfight[m].getNameId());
				_dogfight[m].setMaxHp(200);
				_dogfight[m].setCurrentHp(200);
				_dogfight[m].set_num(bs.id);
				_dogfight[m].setX(Start_X[m]);
				_dogfight[m].setY(Start_Y[m]);
				_dogfight[m].setMap((short) 4);
				
				if (m == 0) 
					_dogfight[m].getMoveState().setHeading(4);
				else 
					_dogfight[m].getMoveState().setHeading(0);
				_dogfight[m].setId(ObjectIdFactory.getInstance().nextId());
				L1World.getInstance().storeObject(_dogfight[m]);
				L1World.getInstance().addVisibleObject(_dogfight[m]);
				players = L1World.getInstance().getVisiblePlayer(_dogfight[m]);
				for (L1PcInstance member : players) {
					if (member != null) {
						member.updateObject();
						member.sendPackets(new S_SkillSound(_dogfight[m].getId(), 5935));
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void broadcastNpc(String msg) {
			if (_npc[0] != null) {
				_npc[0].broadcastPacket(new S_NpcChatPacket(_npc[0], msg, 2));
			}
	}
	
	public void broadcastNpc1(String msg) {
		if (_npc[1] != null) {
			_npc[1].broadcastPacket(new S_NpcChatPacket(_npc[1], msg, 0));
		}
}
	
	public void WinnerMent(String msg) {
			if (_npc[0] != null) {
				_npc[0].broadcastPacket(new S_NpcChatPacket(_npc[0], msg, 2));
			}
			if (_npc[1] != null) {
				_npc[1].broadcastPacket(new S_NpcChatPacket(_npc[1], msg, 0));
			}
	}
	
	public void startSellTicket() {
		LoadNpcShopList();
		broadcastNpc("투견 티켓 판매를 시작하였습니다.");

		this.setdogState(0);

		_ticketSellRemainTime = 60 * 1;
	}

	public long checkTicketSellTime() {
		if (_ticketSellRemainTime == 2 * 60) {
			_ticketSellRemainTime -= 60;
			broadcastNpc("[투견]경기 시작 2분전!");
			return 30 * 1000;
		} else if (_ticketSellRemainTime == 1 * 60) { // 2
			_ticketSellRemainTime -= 60;
			broadcastNpc("[투견]경기 시작 1분전!");
			return 30 * 1000;
		}
		_ticketSellRemainTime = 0;
		broadcastNpc("10초 후 투견 티켓 판매를 마감합니다.");
		_FightWatingTime = 10;
		return 0;
	}
	
	private boolean checkWatingTime() {		
		if (_FightWatingTime > 0) {
			broadcastNpc1(_FightWatingTime + "초");
			--_FightWatingTime;

			return false;
		} else {
			SettingRate();
			setdogState(1);
		}
		return true;
	}
	
	public int getdogState() {
		return this._dogfightState;
	}
	
	public void setdogState(int state) {
		this._dogfightState = state;
	}
		
	public void LoadNpcShopList() {
		try {
			List<L1ShopItem> sellingList = new ArrayList<L1ShopItem>();

			for (int i = 0; i < 2; i++) {
				ticket[i] = 9000000 + GetIssuedTicket() + 1;
				if (i == 0)
					SaveFight(ticket[i], "투견 티켓  [홀]" + _dogfight[i].getNameId() + "-" + (i + 1));
				else 
					SaveFight(ticket[i], "투견 티켓  [짝]" + _dogfight[i].getNameId() + "-" + (i + 1));
				L1ShopItem item = new L1ShopItem(ticket[i], 500, 1);
				sellingList.add(item);
				this._ticketId[i] = ticket[i];
			}
			
			for (int i = 0; i < 2; i++) {
				L1ShopItem item1 = new L1ShopItem(ticket[i], 25000000, 50000);
				sellingList.add(item1);
			}
			
			for (int i = 0; i < 2; i++) {
				L1ShopItem item1 = new L1ShopItem(ticket[i], 50000000, 100000);
				sellingList.add(item1);
			}

			L1Shop shop = new L1Shop(170041, sellingList, _purchasingList);
			ShopTable.getInstance().addShop(170041, shop);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public int GetIssuedTicket() {
		return _fight.size();
	}
	
	private void SaveFight(int i, String j) {
		L1DogFightTicket etcItem = new L1DogFightTicket();
		etcItem.setType2(0);
		etcItem.setItemId(i);
		etcItem.setName(j);
		etcItem.setNameId(j);
		etcItem.setType(12);
		etcItem.setType1(12);
		etcItem.setMaterial(5);
		etcItem.setWeight(0);
		etcItem.set_price(1000);
		etcItem.setGfxId(143);
		etcItem.setGroundGfxId(151);
		etcItem.setMinLevel(0);
		etcItem.setMaxLevel(0);
		etcItem.setBless(1);
		etcItem.setTradable(false);
		etcItem.setDmgSmall(0);
		etcItem.setDmgLarge(0);
		etcItem.set_stackable(true);
		AddTicket(etcItem);
	}
	
	public void AddTicket(L1DogFightTicket fight) {
		_fight.put(new Integer(fight.getItemId()), fight);
		ItemTable.getInstance().getAllTemplates().put(fight.getItemId(), fight);
	}
	
	private void StartGame() {
		RunFight fight = new RunFight(0);
		RunFight fight1 = new RunFight(1);

		GeneralThreadPool.getInstance().schedule(fight, 300);
		GeneralThreadPool.getInstance().schedule(fight1, 400);
	}
	
	public class RunFight implements Runnable {
		private int _status = 0;

		private int _dogId;
		private Random _rndGen = new Random(System.nanoTime());

		public RunFight(int dogId) {
			_dogId = dogId;
		}

		@Override
		public void run() {
			try {
				switch (_status) {
				case 0: {
					L1NpcInstance target = null;
					int damage = _rndGen.nextInt(10)+5;
					int skill = _rndGen.nextInt(5)+1;
					if (_dogId == 0) {
						target = _dogfight[1];
					} else {
						target = _dogfight[0];
					}
					
					if (target.getCurrentHp() > 0 && !_dogfight[_dogId].isDead()) {
						int newhp = 0;
						if (_rndGen.nextInt(100) < 10) {
							_dogfight[_dogId].broadcastPacket(new S_DoActionGFX(_dogfight[_dogId].getId(), ActionCodes.ACTION_SkillAttack));
							_dogfight[_dogId].broadcastPacket(new S_SkillSound(target.getId(), SKILLID[skill]));
							target.broadcastPacket(new S_AttackPacket(target, target.getId(), ActionCodes.ACTION_Damage));
							newhp = target.getCurrentHp() - (damage+damage);
						} else {
							_dogfight[_dogId].broadcastPacket(new S_DoActionGFX(_dogfight[_dogId].getId(), ActionCodes.ACTION_Attack));
							target.broadcastPacket(new S_AttackPacket(target, target.getId(), ActionCodes.ACTION_Damage));
							newhp = target.getCurrentHp() - damage;
						}
						
						if (newhp <= 0) {
							if (_dogfight[_dogId].isDead()) break;
							Broadcaster.broadcastPacket(target, new S_DoActionGFX(target.getId(), ActionCodes.ACTION_Die), true);
							newhp = 0;
							target.NpcDie();
						}
						
						target.setCurrentHp(newhp);
						GeneralThreadPool.getInstance().schedule(this, _time[_dogId]);
					} else if (target.getCurrentHp() == 0 && _dogfight[_dogId].getCurrentHp() > 0){
						WinnerMent("제 " + _FightCount + " 회 승자는 '" + _dogfight[_dogId].getNameId() + "' 입니다.");
					
						if (_dogfight[_dogId].getCurrentHp() > 0) {
							Thread.sleep(2000);
						_dogfight[_dogId].broadcastPacket(new S_SkillSound(_dogfight[_dogId].getId(), WINEFFECT[_dogId]));
						_dogfight[_dogId].broadcastPacket(new S_DoActionGFX(_dogfight[_dogId].getId(), 67));	
						
						Thread.sleep(4000);
						
						goalIn(_dogId);
						_dogfight[_dogId].deleteMe();
						}
					} else if (target.isDead() && _dogfight[_dogId].isDead()){
						WinnerMent("제 " + _FightCount + " 회 결과는 비겼습니다.");
						Thread.sleep(4000);
						goalIn(0);
						_dogfight[_dogId].deleteMe();
						target.deleteMe();
					}
					
					List<L1PcInstance> players = null;
					for (int i = 0; i < 2; i++) {
					players = L1World.getInstance().getVisiblePlayer(_dogfight[i]);
					for (L1PcInstance member : players) {
						if (member != null) {
							member.sendPackets(new S_HPMeter(_dogfight[i]));
							}
						}
					}
					
				}
				break;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void goalIn(int i) {		
		synchronized (this) {
			_ranking = _ranking + 1;
			if (_ranking == 1) {
				_first = _dogfight[i].getName();
				SetWinFightTicketPrice(ticket[i], _ration[i]);
				AddWinCount(i);
			} 
		}
		if (i == 0) {
			SetLoseFightTicketPrice(ticket[1], _ration[1]);
			AddLoseCount(1);
		} else {
			SetLoseFightTicketPrice(ticket[0], _ration[0]);
			AddLoseCount(0);
		}

		
		_complete = true;
	}
	
	public void initFightGame() {
		try {
			_ranking = 0;
			_complete = false;
			_first = null;
			Lucky = rnd.nextInt(50);

			broadcastNpc("잠시후 투견 경기가 시작됩니다.");
			initTicketCount();
			initNpc();
			initShopNpc();
			sleepTime();
			loadDog();
			initWinRate();
			
		} catch (Exception e) {
		}
	}
	
	public int getTotalTicketCount() {
		int total = 0;
		for (int row = 0; row < 2; row++) {
			total += _ticketCount[row];
		}
		return total;
	}
	
	public void SettingRate() {
		for (int row = 0; row < 2; row++) {
			double rate = 0;
			int total = getTotalTicketCount();
			int cnt = _ticketCount[row];
			if (total == 0)
				total = 1;
			if (cnt != 0) {
				rate = (double) total / (double) cnt;
				if (Lucky == row) {
					rate *= 1.0;
				}
			}
			_ration[row] = Double.parseDouble(_df.format(rate));
		}
	}
	
	public void initTicketCount() {
		for (int row = 0; row < 2; row++) {
			_ticketCount[row] = 0;
		}
	}
	
	public void SetWinFightTicketPrice(int id, double rate) {	
		//배당 고정 아닐시 아래 주석
		rate = 1.95;
		L1ShopItem newItem = new L1ShopItem(id, (int) ((500 * rate) * 0.9D), 1);
		_purchasingList.add(newItem);
		initShopNpc();
	}
	
	public void AddWinCount(int j) {
		L1DogFight Fighter = DogFightTable.getInstance().getTemplate(_dogfight[j].get_num());
		Fighter.setWinCount(Fighter.getWinCount() + 1);
		Fighter.setLoseCount(Fighter.getLoseCount());
		SaveAllFighter(Fighter, _dogfight[j].get_num());
	}
	
	public void SaveAllFighter(L1DogFight Fighter, int num) {
		java.sql.Connection con = null;
		PreparedStatement statement = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("UPDATE util_Fighter SET WinCount=?, LoseCount=? WHERE Num=" + num);
			statement.setInt(1, Fighter.getWinCount());
			statement.setInt(2, Fighter.getLoseCount());
			statement.execute();
		} catch (SQLException e) {
			System.out.println("[::::::] SaveAllFighter 메소드 에러 발생");
		} finally {
			if (statement != null) {
				try {
					statement.close();
				} catch (Exception e) {
				}
			}
			;
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
				}
			}
			;
		}
	}
	
	public void SetLoseFightTicketPrice(int id, double rate) {
		L1ShopItem newItem = new L1ShopItem(id, 0, 1);
		_purchasingList.add(newItem);
		initShopNpc();
	}
	
	public void AddLoseCount(int j) {
		L1DogFight Fighter = DogFightTable.getInstance().getTemplate(_dogfight[j].get_num());
		Fighter.setWinCount(Fighter.getWinCount());
		Fighter.setLoseCount(Fighter.getLoseCount() + 1);
		SaveAllFighter(Fighter, _dogfight[j].get_num());
	}
	
	private void startFight() {

		broadcastNpc("경기 시작!");
		broadcastNpc1("경기 시작!");
	    _npc[1].broadcastPacket(new S_DoActionGFX(_npc[1].getId(), 67));	
		StartGame();

		_currentBroadcastFighter = 0;
	}
	
	private boolean broadcastBettingRate() {
		if (_currentBroadcastFighter == 2) {
			return true;
		}

		if (_currentBroadcastFighter == 0) {
			broadcastNpc("배팅 배율을 발표하겠습니다.");
		}

		// 배당 고정아닐시 아래 주석
		_ration[_currentBroadcastFighter] = 1.95;
		broadcastNpc(_dogfight[_currentBroadcastFighter].getNameId() + ": " + _ration[_currentBroadcastFighter] + " ");

		++_currentBroadcastFighter;

		return false;
	}
	
	static class DogStruct{
		public DogStruct(int id, int gfx, String name){
			this.id = id;
			this.gfx = gfx;
			this.name = name;
		}
		public int id;
		public int gfx;
		public String name;
	}
	
}
