
package server.controller;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

import server.manager.eva;
import l1j.server.server.GeneralThreadPool;

/** 1시간 마다 서버로그창 저장 -> 지우기 * */
public class PandoraLogDelControler implements Runnable {
 private static Logger _log = Logger.getLogger(PandoraLogDelControler.class
   .getName());

 private final int _runTime;

 public PandoraLogDelControler(int runTime) {
  _runTime = runTime;
 }

 public void start() {
  GeneralThreadPool.getInstance().scheduleAtFixedRate(
    PandoraLogDelControler.this, 0, _runTime);
 }
 
 private static String getLogTime() {
		Calendar currentDate = Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd HH:mm:ss");
		String time = dateFormat.format(currentDate.getTime());
		return time;
	}

 public void run() {
  try {
   eva.savelog();
   System.out.println("──────────────────────────────────");
   System.out.println("[서버 메세지] 매니저 초기화 및 저장 완료");
   System.out.println("[서버 메세지] 최근 자동 로그 저장 시각은 "+ getLogTime() +"입니다.");
   System.out.println("──────────────────────────────────");
  } catch (Exception e) {
   _log.log(Level.SEVERE, e.getLocalizedMessage(), e);
  }
 }

}



