package server.controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import javolution.util.FastTable;
import l1j.server.Config;
import l1j.server.L1DatabaseFactory;
import l1j.server.server.GeneralThreadPool;
import l1j.server.server.clientpackets.ClientBasePacket;
import l1j.server.server.datatables.ItemTable;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1ItemInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.serverpackets.S_ACTION_UI;
import l1j.server.server.serverpackets.S_PacketBox;
import l1j.server.server.serverpackets.S_ServerMessage;
import l1j.server.server.serverpackets.S_SystemMessage;
import l1j.server.server.utils.SQLUtil;
import server.manager.eva;

public class MagicDollFusionController implements Runnable {

	
	static public void init() {
		//
	}

	static public void close() {
		//
	}

	private static FastTable<Integer> doll_list1 = new FastTable<Integer>();
	
	/**
	 * 조합 요청 처리 메서드.
	 * 
	 * @param pc
	 * @param cbp
	 */
	static public void fusion(L1PcInstance pc, ClientBasePacket cbp) {
		//
		
		cbp.readC();
	    int typelevel = cbp.readC();
	    int[] doll_list = new int[4];
		while (cbp.isRead(8)) {
			cbp.readC(); // 12
			int bit_count = cbp.readC() - 6; // 07
			cbp.readC(); // 08
			int idx = cbp.readC(); //
			cbp.readD(); // 10 f1 13 18
			doll_list[idx - 1] = cbp.read4(bit_count);
		}
		// 버그확인.
		List<L1ItemInstance> temp_list = new ArrayList<L1ItemInstance>();
		
		doll_list1.clear();
		
		for (int objId : doll_list) {
			L1ItemInstance item = pc.getInventory().getItem(objId);
			if (item == null)
				continue;
			temp_list.add(item);
		}
		if (temp_list.size() <= 1) {
			// 마법인형 합성은 2개 이상만 가능.
			pc.sendPackets(new S_SystemMessage("마법인형은 2개 이상으로 합성할 수 있습니다."));
			return;
		}
		int temp_level;
		if (typelevel != 5) {
			temp_level = 0;
			for (L1ItemInstance item : temp_list) {
				int level = getMagicDollLevel(item);
				if (temp_level == 0)
					temp_level = level;
				if (level != temp_level) {
					// 마법인형 합성은 같은 등급만 가능.
					pc.sendPackets(new S_SystemMessage("같은 등급의 마법인형만 합성할 수 있습니다."));
					return;
				}
			}
		}
		
		// 마법인형 제거.
		for (L1ItemInstance item : temp_list)
			pc.getInventory().removeItem(item);
		//

		// 확률 체크.
		int rate = 100; // 기본 사용자 확률
		int rnd = random(0, 100); // 랜덤 확률 0~100
		if (typelevel == 1) {
			rate = Config.DOLL_1; // 인형 단계별로 확률 따로 설정.
		} else if (typelevel == 2) {
			rate = Config.DOLL_2; // 인형 단계별로 확률 따로 설정.
		} else if (typelevel == 3) {
			rate = Config.DOLL_3; // 인형 단계별로 확률 따로 설정.
		} else if (typelevel == 4) {
			rate = Config.DOLL_4; // 인형 단계별로 확률 따로 설정.
		} else if (typelevel == 5) {
			rate = 100; // 인형 단계별로 확률 따로 설정.
		} else {
			rate = Config.DOLL_4;
		}
		// 인형 단계별로 설정된 확률에 재료로 사용된 인형을 곱하고 나누기 / 2 를 한 값을 rateㄹ 설정함.
		// 재료 2개 사용햇을경우 사용자확률이 50 일때.
		// : (50 * 2) / 2 = 50 << 재료 2개일때
		// : (50 * 4) / 2 = 100 << 재료 4개일때
		// : 재료가 많아질수록 rate값이 올라감. 그래서 확률이 높아지고 성공할 확률이 높아짐

		rate = (rate * (temp_list.size() - 1)) / 2; // (기본확률(등급별) * 재료갯수) / 2
		// System.out.println(rate+" vs "+rnd);
		boolean success = rate >= rnd;
		
		if (typelevel == 5) {
			success = true;
		}
		
		// success = true;
		L1ItemInstance doll_item = null;
		int item_id = 0;
		// 성공여부에따라 현재마법인형 재소환 및 다음 인형 생성.
		
		if (success) {
			// 다음 단계 마법인형 랜덤 생성.
			//System.out.println("타입 " + typelevel);
			switch (typelevel) {
			case 1: {
				doll_list1 = DollList(typelevel);
				try {
					item_id = doll_list1.get(random(0, doll_list1.size()-1));
				} catch (Exception e) {
					item_id = doll_list1.get(0);
				}
			}
				break;
			case 2: {
				doll_list1 = DollList(typelevel);
				try {
					item_id = doll_list1.get(random(0, doll_list1.size()-1));
				} catch (Exception e) {
					item_id = doll_list1.get(0);
				}
			}
				break;
			case 3: {
				doll_list1 = DollList(typelevel);
				try {
					item_id = doll_list1.get(random(0, doll_list1.size()-1));
				} catch (Exception e) {
					item_id = doll_list1.get(0);
				}
				pc.인형메세지 = true;
			}
				break;
			case 4: {
				doll_list1 = DollList(typelevel);
				try {
					item_id = doll_list1.get(random(0, doll_list1.size()-1));
				} catch (Exception e) {
					item_id = doll_list1.get(0);
				}
				 pc.인형메세지 = true;
			}
				break;
			case 5:{
				doll_list1 = DollList(typelevel);
				try {
					item_id = doll_list1.get(random(0, doll_list1.size()-1));
				} catch (Exception e) {
					item_id = doll_list1.get(0);
				}
				 pc.인형메세지 = true;
				}
				break;
			default:
				pc.sendPackets(new S_SystemMessage("마법인형 합성에 실패하였습니다."));
				return;
			}
			//
			doll_item = ItemTable.getInstance().createItem(item_id);
			if (doll_item != null) {
				doll_item.setCount(1);
				doll_item.setEnchantLevel(0);
				doll_item.setIdentified(false);
			} else {
				pc.sendPackets(new S_SystemMessage("마법인형 합성에 실패하였습니다.."));
				return;
			}
		} else {
				try { // 재료에 사용된 마법인형 랜덤 1개 추출 다시 인벤에 등록.
					doll_item = temp_list.get(random(0, temp_list.size() - 1));
			} catch (Exception e) {
					doll_item = temp_list.get(0);
				}
			}
		pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.DOLL_MAKING_RESULT, success, doll_item));
		// System.out.println("");
		// System.out.println("성공여부 " + success);

		//
		GeneralThreadPool.getInstance().execute(new MagicDollFusionController(pc, typelevel, success, doll_item));
	}

	/**
	 * 마법인형 아이템에 단계를 확인해주는 메서드.
	 * 
	 * @param item
	 * @return
	 */
	static public int getMagicDollLevel(L1ItemInstance item) {
		// System.out.println("단계 " + item.getItem().getItemDescId());
		switch (item.getItem().getItemDescId()) {
		case 41248:
		case 41250:
		case 430000:
		case 430002:
		case 430004:
		case 600241:
			return 1;
		case 430001:
		case 41249:
		case 430500:
		case 500108:
		case 500109:
		case 600242:
			return 2;
		case 500205:
		case 500204:
		case 500203:
		case 60324:
		case 500110:
		case 600243:
			return 3;
		case 500202:
		case 5000035:
		case 600244:
		case 600245:
		case 41551:
		case 41552:
		case 19006:
			return 4;
		case 19005:
		case 41553:
		case 600246:
		case 600247:
		case 19007:
		case 19008:
		case 19009:
			return 5;
		}
		return 0;
	}

	private L1PcInstance pc;
	private int step;
	private boolean success;
	private L1ItemInstance doll_item;

	public MagicDollFusionController(L1PcInstance pc, int step, boolean success, L1ItemInstance doll_item) {
		this.pc = pc;
		this.step = step;
		this.success = success;
		this.doll_item = doll_item;
	}

	@Override
	public void run() {
		//
		try {
			// 조합모션이 동작중인 시간이 있으므로 클라와 대충 싱크 맞추기위해 슬립을 넣음.
			Thread.sleep(1000 * 10);
			//

			pc.getInventory().storeItem(doll_item);
			if (this.success && pc.인형메세지) {
				L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE,"누군가가 " + doll_item.getItem().getNameId()+ " 합성에 성공 하였습니다."));
				L1World.getInstance().broadcastPacketToAll(new S_SystemMessage("누군가가 "+doll_item.getItem().getNameId()+" 합성에 성공하였습니다."));
				eva.LogEnchantAppend("성공[인형합성]: [",pc.getName(),"] - ", doll_item.getItem().getName(), doll_item.getId());
				pc.인형메세지 = false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	static public int random(int lbound, int ubound) {
		if (ubound < 0)
			return (int) ((Math.random() * (ubound - lbound - 1)) + lbound);
		else
			return (int) ((Math.random() * (ubound - lbound + 1)) + lbound);
	}
	
	private static FastTable<Integer> DollList(int type) {
		Connection con = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		FastTable<Integer> idlist = new FastTable<Integer>();
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("select * from doll_mix WHERE type = ?");
			pstm.setInt(1, type);
			rs = pstm.executeQuery();
			while (rs.next()) {
				idlist.add(rs.getInt("itemnum"));
			}
		} catch (SQLException e) {
			//_log.log(Level.SEVERE, "북마크의 추가로 에러가 발생했습니다.", e);
		} finally {
			SQLUtil.close(rs);
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
		return idlist;
	}
	
	public Integer[] getList() {
		Integer[] l = null;
		synchronized (doll_list1) {
			l = (Integer[]) doll_list1.toArray(new Integer[doll_list1.size()]);
		}
		return l;
	}

}
