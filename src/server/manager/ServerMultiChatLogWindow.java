package server.manager;

import java.awt.Color;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.StringTokenizer;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import l1j.server.ServerChat;


@SuppressWarnings("serial")
public class ServerMultiChatLogWindow extends JInternalFrame {
	private JTextPane worldChatText = null;
	private JTextPane nomalChatText = null;
	private JTextPane partyChatText = null;
	
	
	private JScrollPane worldChatScroll = null;
	private JScrollPane nomalChatScroll = null;
	private JScrollPane partyChatScroll = null;
	
	private JTextField txt_ChatUser = null;
	private JTextField txt_ChatSend = null;
	private JButton btn_Clear = null;
	private JTabbedPane jJTabbedPane = null;
	
	public ServerMultiChatLogWindow(String windowName, int x, int y, int width, int height, boolean resizable, boolean closable) {
		super();
		
		initialize(windowName, x, y, width, height, resizable, closable);
	}
	
	public void initialize(String windowName, int x, int y, int width, int height, boolean resizable, boolean closable) {
		this.title = windowName;
		this.closable = closable;      
		this.isMaximum = false;	   
		this.maximizable = false;
		this.resizable = resizable;
		this.iconable = true;   
		this.isIcon = false;			  
	    setSize(width, height);
		setBounds(x, y, width, height);
		setVisible(true);
		frameIcon = new ImageIcon("");
		setRootPaneCheckingEnabled(true);
		
		updateUI();
	    
		try {
		    jJTabbedPane = new JTabbedPane(JTabbedPane.TOP,JTabbedPane.SCROLL_TAB_LAYOUT);
		    
		    worldChatText = new JTextPane();
		    nomalChatText = new JTextPane();
		    partyChatText = new JTextPane();
		   
	
		    worldChatScroll = new JScrollPane(worldChatText);
		    nomalChatScroll = new JScrollPane(nomalChatText);
		    partyChatScroll = new JScrollPane(partyChatText);
		   
		    
		    worldChatText.setEditable(false);		 
		    nomalChatText.setEditable(false);		 	
		    partyChatText.setEditable(false);		 
		    
		    	
		    worldChatScroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		    worldChatScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		    
		    nomalChatScroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		    nomalChatScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		    
		    
		    partyChatScroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		    partyChatScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		    
		    
		    setStyle(worldChatText);
		    setStyle(nomalChatText);
		    setStyle(partyChatText);
		   
		    
		    jJTabbedPane.addTab("전체", worldChatScroll);
		    jJTabbedPane.addTab("일반", nomalChatScroll);
		    jJTabbedPane.addTab("파티", partyChatScroll);
		   
		   
		    txt_ChatUser = new JTextField();
		    txt_ChatSend = new JTextField();
		    txt_ChatSend.addKeyListener(new KeyAdapter() {
				public void keyPressed(KeyEvent evt) {
					chatKeyPressed(evt);
				}
			});
		    
		 
		    btn_Clear = new JButton("Clear");
		    btn_Clear.setToolTipText("모든 데이터 로그를 저장하고, 매니저창을 클리어 합니다.");
		    btn_Clear.addMouseListener(new java.awt.event.MouseAdapter() {
				public void mouseClicked(java.awt.event.MouseEvent evt) {
					try {
						File f = null;
						String sTemp = "";
						synchronized (eva.lock) {
							sTemp = eva.getDate();
							StringTokenizer s = new StringTokenizer(sTemp, " ");
							eva.date = s.nextToken();
							eva.time = s.nextToken();
							f = new File("ServerLog/" + eva.date);
							if (!f.exists()) {
								f.mkdir();
							}
							eva.jSystemLogWindow.savelog();
							eva.jTradeLogWindow.savelog();
							eva.jWareHouseLogWindow.savelog();
							eva.jEventLogWindow.savelog();
							eva.jBossLogWindow.savelog();
							eva.jObserveLogWindow.savelog();
							eva.jAccountLogWindow.savelog();
							eva.jCommandLogWindow.savelog();
							eva.jRStatLogWindow.savelog();
							eva.jWhisperChatLogWindow.savelog();
							eva.jClanChatLogWindow.savelog();
							eva.flush(worldChatText, "[" + eva.time + "] 월드", eva.date);
							eva.flush(nomalChatText, "[" + eva.time + "] 일반", eva.date);
							eva.flush(partyChatText, "[" + eva.time + "] 파티", eva.date);
							sTemp = null;
							eva.date = null;
							eva.time = null;							
						}
						
						worldChatText.setText("");
					    nomalChatText.setText("");
					    partyChatText.setText("");
					} catch (Exception e) {
						// TODO: handle exception
					}
				}
		    });
		    
			GroupLayout layout = new GroupLayout(getContentPane());
			getContentPane().setLayout(layout);
			
			GroupLayout.SequentialGroup main_horizontal_grp = layout.createSequentialGroup();
			
			GroupLayout.SequentialGroup horizontal_grp = layout.createSequentialGroup();
			GroupLayout.SequentialGroup vertical_grp   = layout.createSequentialGroup();
			
			GroupLayout.ParallelGroup main = layout.createParallelGroup(GroupLayout.Alignment.LEADING);
			GroupLayout.ParallelGroup col1 = layout.createParallelGroup(GroupLayout.Alignment.LEADING);
			GroupLayout.ParallelGroup col2 = layout.createParallelGroup(GroupLayout.Alignment.LEADING);
			GroupLayout.ParallelGroup col3 = layout.createParallelGroup(GroupLayout.Alignment.LEADING);

				
			
			main.addGroup(horizontal_grp);
			main_horizontal_grp.addGroup(main);	
			
			layout.setHorizontalGroup(main_horizontal_grp);
			layout.setVerticalGroup(vertical_grp);
			
			col1.addComponent(txt_ChatUser, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE);
			col2.addComponent(txt_ChatSend, GroupLayout.PREFERRED_SIZE, 355, GroupLayout.PREFERRED_SIZE);
			col3.addComponent(btn_Clear, GroupLayout.PREFERRED_SIZE, 82, GroupLayout.PREFERRED_SIZE);
			
			
			horizontal_grp.addGroup(col1).addGap(5);
			horizontal_grp.addGroup(col2).addGap(5);
			horizontal_grp.addGroup(col3).addGap(5);
			
			main.addGroup(layout.createSequentialGroup().addComponent(jJTabbedPane, GroupLayout.PREFERRED_SIZE, 588, GroupLayout.PREFERRED_SIZE));
			vertical_grp.addGap(5).addContainerGap().addGroup(layout.createBaselineGroup(true, true).addComponent(jJTabbedPane));
			vertical_grp.addGap(5).addContainerGap().addGroup(layout.createBaselineGroup(false, false).addComponent(txt_ChatUser).addComponent(txt_ChatSend).addComponent(btn_Clear)).addGap(5);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void chatKeyPressed(KeyEvent evt) {
		// 서버 채팅
		if (eva.isServerStarted) {
			if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
				// 특정유저가 선택되어 있지 않다면
				if (txt_ChatUser.getText().equalsIgnoreCase("")) {
					ServerChat.getInstance().sendChatToAll("\\fW[***] " + txt_ChatSend.getText());
					eva.LogChatAppend("[전체]", "[***]", txt_ChatSend.getText());
				} else {
					boolean result = ServerChat.getInstance().whisperToPlayer(txt_ChatUser.getText(), txt_ChatSend.getText());
					if (result)
						eva.LogChatWisperAppend("[귓말]", "[*ㅋ**]", txt_ChatUser.getText(), txt_ChatSend.getText(), ">");
					else
						eva.errorMsg(txt_ChatUser.getText() + eva.NoConnectUser);
				}
				txt_ChatSend.setText("");
			}
		} else {
			eva.errorMsg(eva.NoServerStartMSG);
		}
	}
	
	public void append(String paneName, String msg, String color) {
		StyledDocument doc = null;
		
		Style style = null;
		
		if (paneName.equals("worldChatText")) {		
			doc = worldChatText.getStyledDocument();	
			
			if(color == "Blue"){
				style = worldChatText.addStyle("Blue", null);
				StyleConstants.setForeground(style, Color.blue);
				StyleConstants.setBold(style, true);
				} else if(color == "Orange") {
				style = worldChatText.addStyle("Orange", null);
				StyleConstants.setForeground(style, Color.orange);
				StyleConstants.setBold(style, true);
				} else if(color == "Green") {
				style = worldChatText.addStyle("Green", null);
				StyleConstants.setForeground(style, Color.green);
				StyleConstants.setBold(style, true);
				} else {
				style = worldChatText.addStyle("Black", null);
				StyleConstants.setForeground(style, Color.black);
				StyleConstants.setBold(style, true);
				}
			
			try {
				doc.insertString(doc.getLength(), msg, style);
				worldChatText.setCaretPosition(worldChatText.getDocument().getLength());
			} catch (BadLocationException e) {
				e.printStackTrace();
			}
		} else if (paneName.equals("nomalChatText")) {		
			doc = nomalChatText.getStyledDocument();
			
			if(color == "Blue"){
				style = worldChatText.addStyle("Blue", null);
				StyleConstants.setForeground(style, Color.blue);
				StyleConstants.setBold(style, true);
				} else if(color == "Orange") {
				style = worldChatText.addStyle("Orange", null);
				StyleConstants.setForeground(style, Color.orange);
				StyleConstants.setBold(style, true);
				} else {
				style = worldChatText.addStyle("Black", null);
				StyleConstants.setForeground(style, Color.black);
				StyleConstants.setBold(style, true);
				}
			
			try {
				doc.insertString(doc.getLength(), msg, style);
			    nomalChatText.setCaretPosition(nomalChatText.getDocument().getLength());
			} catch (BadLocationException e) {
				e.printStackTrace();
			}
		} else if (paneName.equals("partyChatText")) {		
			doc = partyChatText.getStyledDocument();	
			if(color == "Blue"){
				style = worldChatText.addStyle("Blue", null);
				StyleConstants.setForeground(style, Color.blue);
				StyleConstants.setBold(style, true);
				} else if(color == "Orange") {
				style = worldChatText.addStyle("Orange", null);
				StyleConstants.setForeground(style, Color.orange);
				StyleConstants.setBold(style, true);
				} else {
				style = worldChatText.addStyle("Black", null);
				StyleConstants.setForeground(style, Color.black);
				StyleConstants.setBold(style, true);
				}
			try {
				doc.insertString(doc.getLength(), msg, style);
			    partyChatText.setCaretPosition(partyChatText.getDocument().getLength());
			} catch (BadLocationException e) {
				e.printStackTrace();
			}
		}

		
	}
	
	private void setStyle(JTextPane textPane) {
		try {
			Style style = null;
			style = textPane.addStyle("White", null);
			StyleConstants.setForeground(style, Color.white);
			style = textPane.addStyle("Black", null);
			StyleConstants.setForeground(style, Color.black);
			style = textPane.addStyle("Red", null);
			StyleConstants.setForeground(style, Color.red);
			style = textPane.addStyle("Orange", null);
			StyleConstants.setForeground(style, Color.orange);
			style = textPane.addStyle("Yellow", null);
			StyleConstants.setForeground(style, Color.yellow);
			style = textPane.addStyle("Green", null);
			StyleConstants.setForeground(style, Color.green);
			style = textPane.addStyle("Cyan", null);
			StyleConstants.setForeground(style, Color.cyan);
			style = textPane.addStyle("Blue", null);
			StyleConstants.setForeground(style, Color.blue);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void savelog() {
		try {
			File f = null;
			String sTemp = "";
			synchronized (eva.lock) {
				sTemp = eva.getDate();
				StringTokenizer s = new StringTokenizer(sTemp, " ");
				eva.date = s.nextToken();
				eva.time = s.nextToken();
				f = new File("ServerLog/" + eva.date);
				if (!f.exists()) {
					f.mkdir();
				}
				
				eva.flush(worldChatText, "[" + eva.time + "] 1.월드", eva.date);
				eva.flush(nomalChatText, "[" + eva.time + "] 2.일반", eva.date);
				eva.flush(partyChatText, "[" + eva.time + "] 5.파티", eva.date);
				sTemp = null;
				eva.date = null;
				eva.time = null;							
			}
			
			worldChatText.setText("");
		    nomalChatText.setText("");
		    partyChatText.setText("");
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
}
