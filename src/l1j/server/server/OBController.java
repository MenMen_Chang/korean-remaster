package l1j.server.server;

import java.util.Calendar;
import java.util.Locale;
import java.util.Random;
import java.text.SimpleDateFormat;

import server.manager.eva;

import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.serverpackets.S_Message_YN;
import l1j.server.server.serverpackets.S_PacketBox;
import l1j.server.server.utils.L1SpawnUtil;

public class OBController extends Thread {
	
		private static OBController _instance;

		private boolean _OBStart;
		private static Random _random = new Random(System.nanoTime());
		
		public boolean getOBStart() {
			return _OBStart;
		}
		public void setOBStart(boolean OB) {
			_OBStart = OB;
		}

		public boolean isGmOpen = false;
		
		public static OBController getInstance() {
			if(_instance == null) {
				_instance = new OBController();
			}
			return _instance;
		}
		
		@Override
			public void run() {
					while (true) {
						try	{
							if(isOpen()){
								Spawn1(); 
								Thread.sleep(60000);
								End();
								}
								if(isOpen2()){
								Spawn2(); 
								Thread.sleep(60000);
								End();
								}
						
				} catch(Exception e){
					e.printStackTrace();
				} finally{
					try{
						Thread.sleep(1000L);
					} catch(Exception e){
						e.printStackTrace();
				}
				}
			}
			}

			/** private void Spawn1(){ 
				 try{
						int val1 = _random.nextInt(10);
						int val2 = _random.nextInt(10);
						int val3 = _random.nextInt(10);
						int val4 = _random.nextInt(10);
						int val5 = _random.nextInt(10);
						L1World.getInstance().broadcastPacketToAll
						(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\fO[보스 알림]: 오만의 탑 1~5 보스."));
						L1World.getInstance().broadcastServerMessage("\\aD[보스 알림]: 오만의 탑 1~5 보스.");
						
						if(val1 <= 8){
							L1SpawnUtil.spawn2(32864, 32736, (short) 101, 45513, 50, 3600*1000, 0);//제니스 퀸
							eva.BossLogAppend("[보스스폰] 오만의 탑 1층 (제니스 퀸)");
						}
						if(val2 <= 8){
							L1SpawnUtil.spawn2(32803, 32740, (short) 102, 45547, 50, 3600*1000, 0);//시어
							eva.BossLogAppend("[보스스폰] 오만의 탑 2층 (시어)");
						}
						if(val3 <= 8){
							L1SpawnUtil.spawn2(32802, 32754, (short) 103, 45606, 50, 3600*1000, 0);//뱀파이어
							eva.BossLogAppend("[보스스폰] 오만의 탑 3층 (뱀파이어)");
						}
						if(val4 <= 8){
							L1SpawnUtil.spawn2(32675, 32845, (short) 104, 45650, 50, 3600*1000, 0);//좀비로드
							eva.BossLogAppend("[보스스폰] 오만의 탑 4층 (좀비로드)");
						}
						if(val5 <= 8){
							L1SpawnUtil.spawn2(32669, 32861, (short) 105, 45652, 50, 3600*1000, 0);//쿠거
							eva.BossLogAppend("[보스스폰] 오만의 탑 5층 (쿠거)");
						}
				 }catch(Exception e2){
						e2.printStackTrace();
				 }
				}
			 
			 private void Spawn2(){ 
				 try{
						int val6 = _random.nextInt(10);
						int val7 = _random.nextInt(10);
						int val8 = _random.nextInt(10);
						int val9 = _random.nextInt(10);
						int val10 = _random.nextInt(10);
						L1World.getInstance().broadcastPacketToAll
						(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\fO[보스 알림]: 오만의 탑 6~10 보스."));
						L1World.getInstance().broadcastServerMessage("\\aD[보스 알림]: 오만의 탑 6~10 보스.");
						
						if(val6 <= 8){
							L1SpawnUtil.spawn2(32722, 32878, (short) 106, 45653, 50, 3600*1000, 0);//머미로드
							eva.BossLogAppend("[보스스폰] 오만의 탑 6층 (머미로드)");
						}
						if(val7 <= 8){
							L1SpawnUtil.spawn2(32721, 32848, (short) 107, 45654, 50, 3600*1000, 0);//아이리스
							eva.BossLogAppend("[보스스폰] 오만의 탑 7층 (아이리스)");
						}
						if(val8 <= 8){
							L1SpawnUtil.spawn2(32688, 32858, (short) 108, 45618, 50, 3600*1000, 0);//나이트 발드
							eva.BossLogAppend("[보스스폰] 오만의 탑 8층 (나이트발드)");
						}
						if(val9 <= 8){
							L1SpawnUtil.spawn2(32654, 32859, (short) 109, 45672, 50, 3600*1000, 0);//리치
							eva.BossLogAppend("[보스스폰] 오만의 탑 9층 (리치)");
						}
						if(val10 <= 8){
							L1SpawnUtil.spawn2(32805, 32805, (short) 110, 100002, 50, 3600*1000, 0);//우그누스
							eva.BossLogAppend("[보스스폰] 오만의 탑 10층 (우그누스)");
						}
				 }catch(Exception e2){
						e2.printStackTrace();
				 }
				}**/
		private void Spawn1(){ 
				 try{
						int gr1 =(10);
						
						if(gr1 == 10){
							L1World.getInstance().broadcastServerMessage("\\aD[보스 알림]: 기르타스가 출현합니다. ( 기르타스의 전초기지 )");
							L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\fO[보스 알림]: 기르타스가 출현합니다. ( 기르타스의 전초기지 )"), true);
							 L1SpawnUtil.spawn2(32867, 32862, (short) 537, 100589, 0, 0*0, 0);//뱀파이어
						}

				 }catch(Exception e2){
						e2.printStackTrace();
				 }
				}
			 
		private void Spawn2(){ 
			 try{
					int val1 =(10);
				
					if(val1 == 10){
						GeneralThreadPool.getInstance().execute(new 리퍼등장());
					}
			 }catch(Exception e2){
					e2.printStackTrace();
			 }
			}
		 
			 private boolean isOpen() {
				  Calendar calender = Calendar.getInstance();
				  int hour, minute;
				  hour = calender.get(Calendar.HOUR_OF_DAY);
				  minute = calender.get(Calendar.MINUTE);
			   if (hour == 00 && minute == 00 ||hour == 8 && minute == 00||hour == 12 && minute == 00
					   ||hour == 20 && minute == 00) {
				  return true;
				  }
				  return false;
				 }
			 
			 private boolean isOpen2() {
				  Calendar calender = Calendar.getInstance();
				  int hour, minute;
				  hour = calender.get(Calendar.HOUR_OF_DAY);
				  minute = calender.get(Calendar.MINUTE);
			   if (hour == 3 && minute == 00||hour == 7 && minute == 00||hour == 11 && minute == 00
					   ||hour == 15 && minute == 00||hour == 19 && minute == 00||hour == 23 && minute == 00) {
				  return true;
				  }
				  return false;
				 }
			 

			 /** 종료 **/
			 public void End() {
				 setOBStart(false);
			 }
}