package l1j.server.server.model;

import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.model.skill.L1SkillId;
import l1j.server.server.serverpackets.S_CharRegistNotice;
import l1j.server.server.utils.IntRange;

public class Resistance {
	private static final int LIMIT_MIN = -128;
	private static final int LIMIT_MAX = 127;
	private static final int LIMIT_MIN_MR = -250;
	private static final int LIMIT_MAX_MR = 250;

	private int baseMr = 0; // 기본 마법 방어
	private int addedMr = 0; // 아이템이나 마법에 의해 추가된 마법 방어를 포함한 마법 방어

	private int fire = 0; // 불 저항
	private int water = 0; // 물 저항
	private int wind = 0; // 바람 저항
	private int earth = 0; // 땅 저항

	private int technique = 0; // 기술 내성
	private int spirit = 0; // 정령 내성
	private int dragonlang = 0; // 용언 내성
	private int fear = 0; // 공포 내성

	private int technique_hit = 0; // 기술 적중
	private int spirit_hit = 0; // 정령 적중
	private int dragonlang_hit = 0; // 용언 적중
	private int fear_hit = 0; // 공포 적중
	
	private int ain_booster = 0; // 공포 적중

	private L1Character character = null;

	public Resistance() {
	}

	public Resistance(L1Character cha) {
		init();
		character = cha;
	}

	public void init() {
		baseMr = addedMr = 0;
		fire = water = wind = earth = 0;
		technique = spirit = dragonlang = fear = 0;
	}

	private int checkMrRange(int i, final int MIN) {
		return IntRange.ensure(i, MIN, LIMIT_MAX_MR);
	}

	private byte checkRange(int i) {
		return (byte) IntRange.ensure(i, LIMIT_MIN, LIMIT_MAX);
	}

	public int getEffectedMrBySkill() {
		int effectedMr = getMr();
		if (character.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.ERASE_MAGIC))
			effectedMr /= 2;
		return effectedMr;
	}

	public int getAddedMr() {
		return addedMr;
	}

	public int getMr() {
		return checkMrRange(baseMr + addedMr, LIMIT_MIN_MR);
	}

	public int getBaseMr() {
		return baseMr;
	}

	public void addMr(int i) {
		setAddedMr(addedMr + i);
	}

	public void setBaseMr(int i) {
		baseMr = checkMrRange(i, LIMIT_MIN_MR);
	}

	private void setAddedMr(int i) {
		addedMr = checkMrRange(i, -baseMr);
	}

	public int getTechnique() {
		return technique;
	}

	public int getSpirit() {
		return spirit;
	}

	public int getDragonLang() {
		return dragonlang;
	}

	public int getFear() {
		return fear;
	}

	public int getTechniqueHit() {
		return technique_hit;
	}

	public int getSpiritHit() {
		return spirit_hit;
	}

	public int getDragonLangHit() {
		return dragonlang_hit;
	}

	public int getFearHit() {
		return fear_hit;
	}

	public int getAinBooster(){
		return ain_booster;
	}
	
	public int getFire() {
		return fire;
	}

	public int getWater() {
		return water;
	}

	public int getWind() {
		return wind;
	}

	public int getEarth() {
		return earth;
	}

	public void addFire(int i) {
		fire = checkRange(fire + i);
	}

	public void addWater(int i) {
		water = checkRange(water + i);
	}

	public void addWind(int i) {
		wind = checkRange(wind + i);
	}

	public void addEarth(int i) {
		earth = checkRange(earth + i);
	}

	public void addTechnique(int i) {
		technique = checkRange(technique + i);
		if (character instanceof L1PcInstance) {
			L1PcInstance pc = (L1PcInstance) character;
			pc.sendPackets(new S_CharRegistNotice(S_CharRegistNotice.CHAR_REGIST_NOTICE, S_CharRegistNotice.REGIST, 1,
					technique));
		}
	}

	public void addSpirit(int i) {
		spirit = checkRange(spirit + i);
		if (character instanceof L1PcInstance) {
			L1PcInstance pc = (L1PcInstance) character;
			pc.sendPackets(new S_CharRegistNotice(S_CharRegistNotice.CHAR_REGIST_NOTICE, S_CharRegistNotice.REGIST, 2,
					spirit));
		}
	}

	public void addDragonLang(int i) {
		dragonlang = checkRange(dragonlang + i);
		if (character instanceof L1PcInstance) {
			L1PcInstance pc = (L1PcInstance) character;
			pc.sendPackets(new S_CharRegistNotice(S_CharRegistNotice.CHAR_REGIST_NOTICE, S_CharRegistNotice.REGIST, 3,
					dragonlang));
		}
	}

	public void addFear(int i) {
		fear = checkRange(fear + i);
		if (character instanceof L1PcInstance) {
			L1PcInstance pc = (L1PcInstance) character;
			pc.sendPackets(
					new S_CharRegistNotice(S_CharRegistNotice.CHAR_REGIST_NOTICE, S_CharRegistNotice.REGIST, 4, fear));
		}
	}

	public void addTechniqueHit(int i) {
		technique_hit = checkRange(technique_hit + i);
		if (character instanceof L1PcInstance) {
			L1PcInstance pc = (L1PcInstance) character;
			pc.sendPackets(new S_CharRegistNotice(S_CharRegistNotice.CHAR_REGIST_NOTICE, S_CharRegistNotice.HIT, 1,
					technique_hit));
		}
	}

	public void addSpiritHit(int i) {
		spirit_hit = checkRange(spirit_hit + i);
		if (character instanceof L1PcInstance) {
			L1PcInstance pc = (L1PcInstance) character;
			pc.sendPackets(new S_CharRegistNotice(S_CharRegistNotice.CHAR_REGIST_NOTICE, S_CharRegistNotice.HIT, 2,
					spirit_hit));
		}
	}

	public void addDragonLangHit(int i) {
		dragonlang_hit = checkRange(dragonlang_hit + i);
		if (character instanceof L1PcInstance) {
			L1PcInstance pc = (L1PcInstance) character;
			pc.sendPackets(new S_CharRegistNotice(S_CharRegistNotice.CHAR_REGIST_NOTICE, S_CharRegistNotice.HIT, 3,
					dragonlang_hit));
		}
	}

	public void addFearHit(int i) {
		fear_hit = checkRange(fear_hit + i);
		if (character instanceof L1PcInstance) {
			L1PcInstance pc = (L1PcInstance) character;
			pc.sendPackets(new S_CharRegistNotice(S_CharRegistNotice.CHAR_REGIST_NOTICE, S_CharRegistNotice.HIT, 4, fear_hit));
		}
	}
	
	public void addAinBooster(int i) {
		ain_booster = checkRange(ain_booster + i);
	}


	public void addAllRegist(int i) {
		addTechnique(i);
		addSpirit(i);
		addDragonLang(i);
		addFear(i);
	}

	public void addAllHit(int i) {
		addTechniqueHit(i);
		addSpiritHit(i);
		addDragonLangHit(i);
		addFearHit(i);
	}

	public void addAllNaturalResistance(int i) {
		addFire(i);
		addWater(i);
		addWind(i);
		addEarth(i);
	}
}
