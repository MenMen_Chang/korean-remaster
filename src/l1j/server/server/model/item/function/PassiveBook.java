package l1j.server.server.model.item.function;

import l1j.server.server.SkillCheck;
import l1j.server.server.clientpackets.ClientBasePacket;
import l1j.server.server.datatables.SkillsTable;
import l1j.server.server.model.Broadcaster;
import l1j.server.server.model.Instance.L1ItemInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.model.skill.L1SkillId;
import l1j.server.server.serverpackets.S_ACTION_UI;
import l1j.server.server.serverpackets.S_AddSkill;
import l1j.server.server.serverpackets.S_ServerMessage;
import l1j.server.server.serverpackets.S_SkillSound;
import l1j.server.server.templates.L1Skills;

public class PassiveBook {

	private static PassiveBook in;

	public static PassiveBook getIn() {
		if (in == null) {
			in = new PassiveBook();
		}
		return in;
	}

	public void toUse(L1PcInstance pc, L1ItemInstance book, ClientBasePacket cbp) {
		int id = book.getItemId(), skillId = 0, lv = pc.getLevel();

		if (pc.isDragonknight() && id == 300092) {
			if (id == 300092) { // -- 용기사의 서판 (아우라키아)
				skillId = 317;
			}
		}

		if (skillId == 0) {
			// -- 아무일도 일어나지 않았습니다.
			pc.sendPackets(new S_ServerMessage(79));
			return;
		}

		if (SkillCheck.getInstance().CheckSkill(pc, skillId)) {
			// -- 이미 학습을 한 상태 입니다.
			pc.sendPackets(new S_ServerMessage(3237));
			return;
		}

		L1Skills skill = SkillsTable.getInstance().getTemplate(skillId);

		if (skill == null) {
			return;
		}

		S_SkillSound sound = new S_SkillSound(pc.getId(), 224);

		if (skill.getSkillLevel() == 99) {
			pc.sendPackets(new S_ACTION_UI(146, skill.getId()));
		} else {
			int[] attr = new int[30];
			attr[27] = skill.getId();
			pc.sendPackets(new S_AddSkill(pc, attr));
		}

		if (skillId == L1SkillId.AURAKIA) {
			pc.toSkill().setAura(true);
		}
		SkillsTable.getInstance().spellMastery(pc.getId(), skillId, skill.getName(), 0, 0);
		pc.sendPackets(sound);
		Broadcaster.broadcastPacket(pc, sound);
		pc.getInventory().removeItem(book, 1);
	}

}
