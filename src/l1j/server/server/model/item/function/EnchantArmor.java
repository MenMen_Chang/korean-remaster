/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */

package l1j.server.server.model.item.function;

import java.util.Random;

import l1j.server.Config;
import l1j.server.server.clientpackets.ClientBasePacket;
import l1j.server.server.datatables.EnchantArmorChance;
import l1j.server.server.model.L1Character;
import l1j.server.server.model.Instance.L1ItemInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.model.item.L1ItemId;
import l1j.server.server.serverpackets.S_ServerMessage;
import l1j.server.server.serverpackets.S_SystemMessage;
import l1j.server.server.templates.L1Armor;
import l1j.server.server.templates.L1EnchantArmorChance;
import l1j.server.server.templates.L1Item;
import l1j.server.server.utils.CommonUtil;

@SuppressWarnings("serial")
public class EnchantArmor extends Enchant {

	private static Random _random = new Random(System.nanoTime());

	public EnchantArmor(L1Item item) {
		super(item);
	}

	@Override
	public void clickItem(L1Character cha, ClientBasePacket packet) {
		if (cha instanceof L1PcInstance) {
			L1PcInstance pc = (L1PcInstance) cha;
			L1ItemInstance useItem = pc.getInventory().getItem(this.getId());
			int itemId = this.getItemId();
			L1ItemInstance l1iteminstance1 = pc.getInventory().getItem(packet.readD());
			if (l1iteminstance1 == null) {
				pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true); // \f1 아무것도 일어나지 않았습니다.
				return;
			}
			//-- 여기 한군대에서만 넣으시면 다 막혀요.
			if (l1iteminstance1.isEquipped()) {
				pc.sendPackets(new S_SystemMessage("착용을 해제한 후 강화할 수 있습니다."));
				return;
			}
			if (l1iteminstance1.getItem().getType2() != 2) {
				pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true); // \f1 아무것도 일어나지 않았습니다.
				return;
			} 
			if (pc.getLastEnchantItemid() == l1iteminstance1.getId()) {
				pc.setLastEnchantItemid(l1iteminstance1.getId(), l1iteminstance1);
				return;
			} 
			if (l1iteminstance1.getBless() >= 128) { // 봉인템
				pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true); // \f1 아무것도 일어나지 않았습니다.
				return;
			} 
			
			int safe_enchant = ((L1Armor) l1iteminstance1.getItem()).get_safeenchant();

			if (safe_enchant < 0) { // 강화 불가
				pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
				return;
			}

			int armorId = l1iteminstance1.getItem().getItemId();
			int armortype = l1iteminstance1.getItem().getType();
			int armorgrade = l1iteminstance1.getItem().getGrade();

			if (armorId == 30910 || armorId == 30911 || armorId == 30912 || armorId == 30913 || armorId == 30914
					|| armorId == 30195 || armorId == 30196 || armorId == 30197 || armorId == 30198 || armorId == 30199
					|| armorId == 30200 || armorId == 30201 || armorId == 31910 || armorId == 31911 || armorId == 31912
					|| armorId == 31913 || armorId == 32910 || armorId == 32911 || armorId == 32912) {
				pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
				return;
			} 
			if (armorId == 10110) {
				if (itemId != 6353) {
					pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
					return;
				}
			} 
			if (itemId == 6353) {
				if (armorId != 10110) {
					pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
					return;
				}

			} 
			/** 환상의 갑옷 마법 주문서 **/

			if (armorId >= 9114 && armorId <= 9117 || armorId >= 91140 && armorId <= 91170) {
				if (itemId == 40127 || itemId == 140127 || itemId == 240127) {
				} else {
					pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
					return;
				}
			} 
			if (itemId == 40127 || itemId == 140127 || itemId == 240127) {
				if (armorId >= 9114 && armorId <= 9117 || armorId >= 91140 && armorId <= 91170) {
				} else {
					pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
					return;
				}
			} 
			/** 환상의 갑옷 마법 주문서 **/

			//-- 아몰랑..
			if (armorId != 21137 && itemId == 60251) {
				pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
				if (l1iteminstance1.isEquipped()) {
					pc.sendPackets(new S_SystemMessage("착용을 해제한 후 강화할 수 있습니다."));
					return;
				}
			} else if (armorId == 21137 && itemId != 60251) {
				pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
				if (l1iteminstance1.isEquipped()) {
					pc.sendPackets(new S_SystemMessage("착용을 해제한 후 강화할 수 있습니다."));
					return;
				}
			}

			if (armorId != 21269 && itemId == 160511) {
				pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
				if (l1iteminstance1.isEquipped()) {
					pc.sendPackets(new S_SystemMessage("착용을 해제한 후 강화할 수 있습니다."));
					return;
				}
			} else if (armorId == 21269 && itemId != 160511) {
				pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
				if (l1iteminstance1.isEquipped()) {
					pc.sendPackets(new S_SystemMessage("착용을 해제한 후 강화할 수 있습니다."));
					return;
				}
			}

			if (armorId == 20028 || armorId == 20283 || armorId == 20126 || armorId == 20173 || armorId == 20206
					|| armorId == 20232 || armorId == 421030 || armorId == 421031 || armorId == 21051
					|| armorId == 21052 || armorId == 21053 || armorId == 21054 || armorId == 21055 || armorId == 21056
					|| armorId == 21098 || armorId == 20081 || armorId == 21102 || armorId == 21103 || armorId == 21104
					|| armorId == 21105 || armorId == 21106 || armorId == 21107 || armorId == 21108 || armorId == 21109
					|| armorId == 21110 || armorId == 21111 || armorId == 21112 || armorId == 21254 || armorId == 20082
					|| armorId == 500220) {
				if (itemId == L1ItemId.여행방어주문서 || itemId == 60141) {
					if (armorId == 20028 || armorId == 20126 || armorId == 20283 || armorId == 20173 || armorId == 20206
							|| armorId == 20232 || armorId == 21098 || armorId == 20081 || armorId == 20082
							|| armorId == 500220) {
						int enchant_level = l1iteminstance1.getEnchantLevel();
						if (enchant_level >= 4) {
							pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
							if (l1iteminstance1.isEquipped()) {
								pc.sendPackets(new S_SystemMessage("착용을 해제한 후 강화할 수 있습니다."));
								return;
							}
						}
					} else if (

							armorId == 21102 || armorId == 21103 || armorId == 21104 || armorId == 21105 || armorId == 21106
							|| armorId == 21107 || armorId == 21108 || armorId == 21109 || armorId == 21110
							|| armorId == 21111 || armorId == 21112 || armorId == 21254) {
						int enchant_level = l1iteminstance1.getEnchantLevel();
						if (enchant_level >= 6) {
							pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
							if (l1iteminstance1.isEquipped()) {
								pc.sendPackets(new S_SystemMessage("착용을 해제한 후 강화할 수 있습니다."));
								return;
							}
						}

					}
				} else {
					pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
					if (l1iteminstance1.isEquipped()) {
						pc.sendPackets(new S_SystemMessage("착용을 해제한 후 강화할 수 있습니다."));
						return;
					}
				}
			}
			if (itemId == L1ItemId.여행방어주문서 || itemId == 60141) {
				if (armorId == 20028 || armorId == 20283 || armorId == 20126 || armorId == 20173 || armorId == 20206
						|| armorId == 20232 || armorId == 21098 || armorId == 421030 || armorId == 421031
						|| armorId == 21051 || armorId == 21052 || armorId == 21053 || armorId == 21054
						|| armorId == 21055 || armorId == 21056 || armorId == 20081 || armorId == 20082
						|| armorId == 500220) {
					if (armorId == 20028 || armorId == 20126 || armorId == 20283 || armorId == 20173 || armorId == 20206
							|| armorId == 20232 || armorId == 21098 || armorId == 20081 || armorId == 20082
							|| armorId == 500220) {
						int enchant_level = l1iteminstance1.getEnchantLevel();
						if (enchant_level >= 4) {
							pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
							if (l1iteminstance1.isEquipped()) {
								pc.sendPackets(new S_SystemMessage("착용을 해제한 후 강화할 수 있습니다."));
								return;
							}
						}
					}
				} else {
					if (((armorId >= 21102 && armorId <= 21112) || armorId == 21102 || armorId == 21103
							|| armorId == 21104 || armorId == 21105 || armorId == 21106 || armorId == 21107
							|| armorId == 21108 || armorId == 21109 || armorId == 21110 || armorId == 21111
							|| armorId == 21112 || armorId == 21254) && l1iteminstance1.getEnchantLevel() < 6) {
					} else {
						pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
						if (l1iteminstance1.isEquipped()) {
							pc.sendPackets(new S_SystemMessage("착용을 해제한 후 강화할 수 있습니다."));
							return;
						}
					}
				}
			} else if (((armorId >= 21102 && armorId <= 21112) || armorId == 21254)) {
				pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
				return;
			}

			/** 창천의 갑옷 마법 주문서 **/
			if (armorId == 91113) {
				if (itemId == L1ItemId.CHANGCHUN_ENCHANT_ARMOR_SCROLL) {
				} else {
					pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
					if (l1iteminstance1.isEquipped()) {
						pc.sendPackets(new S_SystemMessage("착용을 해제한 후 강화할 수 있습니다."));
						return;
					}
				}
			}
			if (itemId == L1ItemId.CHANGCHUN_ENCHANT_ARMOR_SCROLL) {
				if (armorId == 91113) {
				} else {
					pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
					if (l1iteminstance1.isEquipped()) {
						pc.sendPackets(new S_SystemMessage("착용을 해제한 후 강화할 수 있습니다."));
						return;
					}
				}
			}
			/** 창천의 갑옷 마법 주문서 **/

			if (itemId == 61500) {
				if (armortype == 16) {

				} else {
					pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
					if (l1iteminstance1.isEquipped()) {
						pc.sendPackets(new S_SystemMessage("착용을 해제한 후 강화할 수 있습니다."));
						return;
					}
				}
			}

			if (armortype == 16) {
				if (itemId == 61500) {

				} else {
					pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
					if (l1iteminstance1.isEquipped()) {
						pc.sendPackets(new S_SystemMessage("착용을 해제한 후 강화할 수 있습니다."));
						return;
					}
				}
			}

			if (itemId >= L1ItemId.Inadril_T_ScrollA && itemId <= L1ItemId.Inadril_T_ScrollD) {
				if (!(armorId >= 490000 && armorId <= 490017)) {
					pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
					if (l1iteminstance1.isEquipped()) {
						pc.sendPackets(new S_SystemMessage("착용을 해제한 후 강화할 수 있습니다."));
						return;
					}
				}
			}
			if (armorId >= 490000 && armorId <= 490017) {
				if (!(itemId >= L1ItemId.Inadril_T_ScrollA && itemId <= L1ItemId.Inadril_T_ScrollD)) {
					pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
					if (l1iteminstance1.isEquipped()) {
						pc.sendPackets(new S_SystemMessage("착용을 해제한 후 강화할 수 있습니다."));
						return;
					}
				}
			}

			/** 장신구 강화 주문서 */
			if (itemId == 160511 || itemId == L1ItemId.ORIM_ACCESSORY_ENCHANT_SCROLL
					|| itemId == L1ItemId.ORIM_ACCESSORY_ENCHANT_SCROLL_B || itemId == L1ItemId.ACCESSORY_ENCHANT_SCROLL
					|| itemId == 430040 || itemId == 5000500 || itemId == 530040 || itemId == 60417) {
				if (armortype >= 8 && armortype <= 12) {
				} else {
					pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
					if (l1iteminstance1.isEquipped()) {
						pc.sendPackets(new S_SystemMessage("착용을 해제한 후 강화할 수 있습니다."));
						return;
					}
				}
			}
			if (armortype >= 8 && armortype <= 12) {
				if (itemId == 160511 || itemId == L1ItemId.ORIM_ACCESSORY_ENCHANT_SCROLL
						|| itemId == L1ItemId.ORIM_ACCESSORY_ENCHANT_SCROLL_B
						|| itemId == L1ItemId.ACCESSORY_ENCHANT_SCROLL || itemId == 430040 || itemId == 5000500
						|| itemId == 530040 || itemId == 60417) {
				} else {
					pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
					if (l1iteminstance1.isEquipped()) {
						pc.sendPackets(new S_SystemMessage("착용을 해제한 후 강화할 수 있습니다."));
						return;
					}
				}
			}

			/** 장신구 강화 주문서 */
			/** 룸티스의 강화 주문서 **/
			if (itemId == 5000500) {
				if ((armorId >= 500007 && armorId <= 500010) || (armorId >= 502007 && armorId <= 502010)) {
				} else {
					pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
					if (l1iteminstance1.isEquipped()) {
						pc.sendPackets(new S_SystemMessage("착용을 해제한 후 강화할 수 있습니다."));
						return;
					}
				}
			}
			if ((armorId >= 500007 && armorId <= 500010) || (armorId >= 502007 && armorId <= 502010)) {
				if (itemId == 5000500) {
				} else {
					pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
					if (l1iteminstance1.isEquipped()) {
						pc.sendPackets(new S_SystemMessage("착용을 해제한 후 강화할 수 있습니다."));
						return;
					}
				}
			}
			/** 룸티스의 강화 주문서 **/
			/** 스냅퍼의 반지 강화 주문서 */
			if (itemId == 60417) {
				if (armorId >= 21246 && armorId <= 21253) {
				} else {
					pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
					return;
				}
			}
			if (armorId >= 21246 && armorId <= 21253) {
				if (itemId == 60417) {
				} else {
					pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
					return;
				}
			}
			/** 스냅퍼의 반지 강화 주문서 */

			/** 신기한 반지 강화 주문서 */
			if (itemId == 530040) {
				if ((armorId >= 525109 && armorId <= 525113) || (armorId >= 625109 && armorId <= 625113)) {
				} else {
					pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
					return;
				}
			}

			if ((armorId >= 525109 && armorId <= 525113) || (armorId >= 625109 && armorId <= 625113)) {
				if (itemId == 530040) {
				} else {
					pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
					return;
				}
			}
			/** 신기한 반지 강화 주문서 */

			/** 순백의 반지 강화 주문서 */
			if (itemId == 430040) {
				if (armorId >= 425109 && armorId <= 425113) {

				} else {
					pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
					return;
				}
			}

			if (armorId >= 425109 && armorId <= 425113) {
				if (itemId == 430040) {

				} else {
					pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
					return;
				}
			}
			/** 순백의 반지 강화 주문서 */
			/** 티셔츠 전용 강화 주문서 */
			if (itemId == 430041 || itemId == 1430041 || itemId == 2430041) {
				if (armorId == 21028 || armorId == 21029 || armorId == 21030 || armorId == 21031 || armorId == 21032
						|| armorId == 21033 || armorId == 425106 || armorId == 425107 || armorId == 425108
						|| (armorId >= 21183 && armorId <= 21206)) {
				} else {
					pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
					return;
				}
			}
			if (armorId == 21028 || armorId == 21029 || armorId == 21030 || armorId == 21031 || armorId == 21032
					|| armorId == 21033 || armorId == 425106 || armorId == 425107 || armorId == 425108
					|| (armorId >= 21183 && armorId <= 21206)) {
				if (itemId == 430041 || itemId == 1430041 || itemId == 2430041) {
				} else {
					pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
					return;
				}
			}
			/** 티셔츠 전용 강화 주문서 */
			int enchant_level = l1iteminstance1.getEnchantLevel();

			if (armorId == 10110) {
				if (enchant_level >= 9) {
					pc.sendPackets(new S_SystemMessage("해당 아이템은 9이상은 인챈트 할 수 없습니다."), true);
					return;
				}
			}

			if (armortype == 16) {
				if (enchant_level >= 8) {
					pc.sendPackets(new S_SystemMessage("해당 장신구는 8이상은 인챈트 할 수 없습니다."), true);
					return;
				}
			}

			if (armortype >= 8 && armortype <= 12) { // 강도 : 특
				if (armorgrade != 3) {
					if (enchant_level >= 9) {
						pc.sendPackets(new S_SystemMessage("해당 장신구는 9이상은 인챈트 할 수 없습니다."), true);
						return;
					}
				} else {
					if (enchant_level >= 8) {
						pc.sendPackets(new S_SystemMessage("해당 장신구는 8이상은 인챈트 할 수 없습니다."), true);
						return;
					}
				}
			}

			if (enchant_level >= Config.MAX_ARMOR) {
				pc.sendPackets(new S_SystemMessage("\\fW모든 방어구는 현재 +" + Config.MAX_ARMOR + "이상은 인챈할 수 없습니다."), true);
				return;
			}

			if (armorId >= 500007 && armorId <= 500010 || armorId >= 502007 && armorId <= 502010) {
				if (enchant_level >= Config.MAX_ACCESSORY) {
					pc.sendPackets(new S_SystemMessage("룸티스 장신구는 현재 +" + Config.MAX_ACCESSORY + "이상은 인챈할 수 없습니다."));
					return;
				}
			}
			if (Config.GAME_SERVER_TYPE == 1 && enchant_level >= safe_enchant + 3) {
				pc.sendPackets(new S_SystemMessage("테스트서버에서는 안전인챈+3 이상은 인챈하실수 없습니다."), true);
				return;
			}
			if (safe_enchant == 0){
				if(enchant_level >= Config.MAX_WEAPON1){
					pc.sendPackets(new S_SystemMessage("\\fW안전인챈 0 무기는 현재 +" + Config.MAX_WEAPON1 + "이상은 인챈할수 없습니다."), true);
					return;
				}
			}
			if (itemId == L1ItemId.고대갑옷) { // 고대의 서
				if (CommonUtil.random(100) < Config.godea_ENCHANT) {
					SuccessEnchant(pc, l1iteminstance1, RandomELevel(l1iteminstance1, itemId));
					pc.sendPackets(new S_SystemMessage("\\fW축하합니다!인첸트에 성공하여 아이템의 인첸수치가 +1 올라갔습니다."));
				} else {
					pc.sendPackets(new S_SystemMessage("\\fW인첸트에 실패하였으나 인첸트 수치가 보존 됩니다."));
				}
				pc.getInventory().removeItem(useItem, 1);
				/*
				if (l1iteminstance1 != null && l1iteminstance1.getItem().getType2() == 1
						|| l1iteminstance1.getItem().getType2() == 2) {
					Random random = new Random();
					int k3 = random.nextInt(100);
					if (k3 >= 4 && k3 <= 8) { // +1 될확율 4~5%
						SuccessEnchant(pc, l1iteminstance1, RandomELevel(l1iteminstance1, itemId));
						pc.sendPackets(new S_SystemMessage("\\fW축하합니다!인첸트에 성공하여 아이템의 인첸수치가 +1 올라갔습니다."));
						pc.getInventory().removeItem(useItem, 1);
						return;
					}
					if (k3 >= 9 && k3 <= 100) { // 확률은 알아서
						pc.sendPackets(new S_SystemMessage("\\fW인첸트에 실패하였으나 인첸트 수치가 보존 됩니다."));
						pc.getInventory().removeItem(useItem, 1);
						return;
					}
				} else {
					pc.sendPackets(new S_ServerMessage(79)); // \f1 아무것도 일어나지
					// 않았습니다.
				}
				 */
				return;
			}

			if (itemId == L1ItemId.C_SCROLL_OF_ENCHANT_ARMOR || itemId == 2430041 || itemId == 260600
					|| itemId == 240127 || itemId == L1ItemId.Inadril_T_ScrollC) { // c-zel
				pc.getInventory().removeItem(useItem, 1);
				int rnd = _random.nextInt(100) + 1;
				if (l1iteminstance1.isEquipped()) {
					pc.sendPackets(new S_SystemMessage("착용을 해제한 후 강화할 수 있습니다."));
					return;
				}
				if (safe_enchant == 0 && rnd <= 30) {
					FailureEnchant(pc, l1iteminstance1);
					return;
				}
				if (enchant_level < -6) {
					// -6이상은 할 수 없다.
					FailureEnchant(pc, l1iteminstance1);
				} else {
					SuccessEnchant(pc, l1iteminstance1, -1);
				}
			} else if (itemId >= 5000138 && itemId <= 5000141) {
				DragonArmorEnchant(pc, l1iteminstance1, itemId);
				// pc.getInventory().removeItem(useItem, 1);
				if (l1iteminstance1.isEquipped()) {
					pc.sendPackets(new S_SystemMessage("착용을 해제한 후 강화할 수 있습니다."));
					return;
				}
			} else if (itemId == L1ItemId.Inadril_T_ScrollD) {
				pc.getInventory().removeItem(useItem, 1);
				RegistEnchant(pc, l1iteminstance1, itemId);
				if (l1iteminstance1.isEquipped()) {
					pc.sendPackets(new S_SystemMessage("착용을 해제한 후 강화할 수 있습니다."));
					return;
				}
			} else if (enchant_level < safe_enchant) {
				pc.getInventory().removeItem(useItem, 1);
				SuccessEnchant(pc, l1iteminstance1, RandomELevel(l1iteminstance1, itemId));
				if (l1iteminstance1.isEquipped()) {
					pc.sendPackets(new S_SystemMessage("착용을 해제한 후 강화할 수 있습니다."));
					return;
				}
			} else {
				pc.getInventory().removeItem(useItem, 1);
				int rnd = _random.nextInt(100) + 1;
				int chance = 0;
				/** 방어구 인첸당 확률 **/
				L1EnchantArmorChance l1enchantchance = EnchantArmorChance.getInstance()
						.getTemplate(l1iteminstance1.getItem().getItemId());
				int e0 = l1enchantchance.get0();
				int e1 = l1enchantchance.get1();
				int e2 = l1enchantchance.get2();
				int e3 = l1enchantchance.get3();
				int e4 = l1enchantchance.get4();
				int e5 = l1enchantchance.get5();
				int e6 = l1enchantchance.get6();
				int e7 = l1enchantchance.get7();
				int e8 = l1enchantchance.get8();
				int e9 = l1enchantchance.get9();
				int e10 = l1enchantchance.get10();
				int e11 = l1enchantchance.get11();
				int e12 = l1enchantchance.get12();
				int e13 = l1enchantchance.get13();
				int e14 = l1enchantchance.get14();
				try {
					if (l1enchantchance != null) {
						int enchant = l1iteminstance1.getEnchantLevel();
						switch (enchant) {
						case 0:
							chance += e0;
							break;
						case 1:
							chance += e1;
							break;
						case 2:
							chance += e2;
							break;
						case 3:
							chance += e3;
							break;
						case 4:
							chance += e4;
							break;
						case 5:
							chance += e5;
							break;
						case 6:
							chance += e6;
							break;
						case 7:
							chance += e7;
							break;
						case 8:
							chance += e8;
							break;
						case 9:
							chance += e9;
							break;
						case 10:
							chance += e10;
							break;
						case 11:
							chance += e11;
							break;
						case 12:
							chance += e12;
							break;
						case 13:
							chance += e13;
							break;
						case 14:
							chance += e14;
							break;
						default:
							break;
						}
					}
				} catch (Exception e) {
					System.out.println("Character Enchant Armor Chance Load Error");
				}
				if (pc.인첸축복) {
					chance = 100;
					pc.인첸축복 = false;
				}
				if (pc.isGm()) {
					pc.sendPackets(new S_SystemMessage("\\fY성공확률: [ " + chance + " ]"));
					pc.sendPackets(new S_SystemMessage("\\fY찬스: [ " + rnd + " ]"));
				}
				if (rnd < chance) {
					int randomEnchantLevel = RandomELevel(l1iteminstance1, itemId);
					SuccessEnchant(pc, l1iteminstance1, randomEnchantLevel);
				} else if (enchant_level >= 9 && rnd < (chance * 2)) {
					String item_name_id = l1iteminstance1.getName();
					String pm = "";
					String msg = "";
					if (enchant_level > 0) {
						pm = "+";
					}
					msg = (new StringBuilder()).append(pm + enchant_level).append(" ").append(item_name_id).toString();
					pc.sendPackets(new S_ServerMessage(160, msg, "$252", "$248"), true);
				} else {
					if (itemId == L1ItemId.ORIM_ACCESSORY_ENCHANT_SCROLL) {
						int rnddd = _random.nextInt(100);
						if (rnddd < 30) {
							String item_name_id = l1iteminstance1.getName();
							String pm = "";
							String msg = "";
							if (enchant_level > 0) {
								pm = "+";
							}
							msg = (new StringBuilder()).append(pm + enchant_level).append(" ").append(item_name_id)
									.toString();
							pc.sendPackets(new S_ServerMessage(4056, msg));
							return;
						}
					} else if (itemId == L1ItemId.ORIM_ACCESSORY_ENCHANT_SCROLL_B) {
						String item_name_id = l1iteminstance1.getName();
						String pm = "";
						String msg = "";
						if (enchant_level > 0) {
							pm = "+";
						}
						msg = (new StringBuilder()).append(pm + enchant_level).append(" ").append(item_name_id)
								.toString();
						pc.sendPackets(new S_ServerMessage(4056, msg));
						return;
					}
					FailureEnchant(pc, l1iteminstance1, itemId);
				}
			}
		}
	}
}
