/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */

package l1j.server.server.model.item.function;

import java.util.Random;

import l1j.server.Config;
import l1j.server.server.clientpackets.ClientBasePacket;
import l1j.server.server.datatables.EnchantArmorChance;
import l1j.server.server.model.L1Character;
import l1j.server.server.model.Instance.L1ItemInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.model.item.L1ItemId;
import l1j.server.server.serverpackets.S_ServerMessage;
import l1j.server.server.serverpackets.S_SystemMessage;
import l1j.server.server.templates.L1Armor;
import l1j.server.server.templates.L1EnchantArmorChance;
import l1j.server.server.templates.L1Item;

@SuppressWarnings("serial")
public class EnchantMoonjang extends Enchant {

	private static Random _random = new Random(System.nanoTime());

	public EnchantMoonjang(L1Item item) {
		super(item);
	}

	@Override
	public void clickItem(L1Character cha, ClientBasePacket packet) {
		if (cha instanceof L1PcInstance) {
			L1PcInstance pc = (L1PcInstance) cha;
			L1ItemInstance useItem = pc.getInventory().getItem(this.getId());
			int itemId = this.getItemId();
			L1ItemInstance l1iteminstance1 = pc.getInventory().getItem(packet.readD());
			if (l1iteminstance1 == null) {
				pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true); // \f1 아무것도 일어나지  않았습니다.
				return;
			}
			int safe_enchant = 0;
			if (l1iteminstance1.getItem().getType2() != 2) {
				pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true); // \f1  아무것도 일어나지 않았습니다.
				return;
			}

			if (pc.getLastEnchantItemid() == l1iteminstance1.getId()) {
				pc.setLastEnchantItemid(l1iteminstance1.getId(),
						l1iteminstance1);
				return;
			}
			if (l1iteminstance1.getBless() >= 128) { // 봉인템
				pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true); // \f1 아무것도 일어나지 않았습니다.
				return;
			}

			safe_enchant = ((L1Armor) l1iteminstance1.getItem()).get_safeenchant();

			if (safe_enchant < 0) { // 강화 불가
				pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
				return;
			}

			int armorId = l1iteminstance1.getItem().getItemId();
			//int armortype = l1iteminstance1.getItem().getType();
			/** 환상의 갑옷 마법 주문서 **/

			if(armorId == 30910 || armorId == 30911 || armorId == 30912 || armorId == 30913 || armorId == 30914
					|| armorId == 30195 || armorId == 30196 || armorId == 30197 || armorId == 30198 || armorId == 30199
					|| armorId == 30200 || armorId == 30201 || armorId == 31910 || armorId == 31911 || armorId == 31912
					|| armorId == 31913 || armorId == 32910 || armorId == 32911 || armorId == 32912){
				if (itemId == 402210) {
				} else {
					pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
					return;
				}
			}
			
			if (itemId == 402210) {
				if(armorId == 30910 || armorId == 30911 || armorId == 30912 || armorId == 30913 || armorId == 30914
						|| armorId == 30195 || armorId == 30196 || armorId == 30197 || armorId == 30198 || armorId == 30199
						|| armorId == 30200 || armorId == 30201 || armorId == 31910 || armorId == 31911 || armorId == 31912
						|| armorId == 31913 || armorId == 32910 || armorId == 32911 || armorId == 32912){
				} else {
					pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
					return;
				}
			}
			
			int enchant_level = l1iteminstance1.getEnchantLevel();
			if (enchant_level >= 10) {
				pc.sendPackets(new S_SystemMessage(pc, "해당 아이템은 +10이상 인챈트 할 수 없습니다."), true);
				return;
			}

			if (enchant_level < safe_enchant) {
				pc.getInventory().removeItem(useItem, 1);
				SuccessEnchant(pc, l1iteminstance1,
						RandomELevel(l1iteminstance1, itemId));
			} else {
				pc.getInventory().removeItem(useItem, 1);
				int rnd = _random.nextInt(100) + 1;				
				int chance = 0;
				/** 방어구 인첸당  확률 **/
				L1EnchantArmorChance l1enchantchance = 
				EnchantArmorChance.getInstance().getTemplate(l1iteminstance1.getItem().getItemId());
				int e0 = l1enchantchance.get0();
				int e1 = l1enchantchance.get1();
				int e2 = l1enchantchance.get2();
				int e3 = l1enchantchance.get3();
				int e4 = l1enchantchance.get4();
				int e5 = l1enchantchance.get5();
				int e6 = l1enchantchance.get6();
				int e7 = l1enchantchance.get7();
				int e8 = l1enchantchance.get8();
				int e9 = l1enchantchance.get9();
				int e10 = l1enchantchance.get10();
				int e11 = l1enchantchance.get11();
				int e12 = l1enchantchance.get12();
				int e13 = l1enchantchance.get13();
				int e14 = l1enchantchance.get14();
				try{
					if(l1enchantchance != null){
					int enchant = l1iteminstance1.getEnchantLevel();
					switch(enchant){
					case 0 : chance += e0; break;
					case 1 : chance += e1; break;
					case 2 : chance += e2; break;
					case 3 : chance += e3; break;
					case 4 : chance += e4; break;
					case 5 : chance += e5; break;
					case 6 : chance += e6; break;
					case 7 : chance += e7; break;
					case 8 : chance += e8; break;
					case 9 : chance += e9; break;
					case 10 : chance += e10; break;
					case 11 : chance += e11; break;
					case 12 : chance += e12; break;
					case 13 : chance += e13; break;
					case 14 : chance += e14; break;
					default:
					break;
					}
				}
				} catch (Exception e){
					 System.out.println("Character Enchant Armor Chance Load Error");
				}
				if(pc.인첸축복){
					chance = 100;
					pc.인첸축복 = false;
				}
				if (pc.isGm()) {
					pc.sendPackets(new S_SystemMessage("\\fY성공확률: [ " + chance + " ]"));
					pc.sendPackets(new S_SystemMessage("\\fY찬스: [ " + rnd + " ]"));
				}
				if (rnd < chance) {
					int randomEnchantLevel = RandomELevel(l1iteminstance1,itemId);
					SuccessEnchant(pc, l1iteminstance1, randomEnchantLevel);
				} else if (enchant_level >= 9 && rnd < (chance * 2)) {
					String item_name_id = l1iteminstance1.getName();
					String pm = "";
					String msg = "";
					if (enchant_level > 0) {
						pm = "+";
					}
					msg = (new StringBuilder()).append(pm + enchant_level).append(" ").append(item_name_id).toString();
					pc.sendPackets(new S_ServerMessage(160, msg, "$252", "$248"), true);
				} else {					
					FailureEnchant(pc, l1iteminstance1, itemId);
				}
			}
		}
	}
}
