/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */

package l1j.server.server.model.item.function;

import java.util.Random;

import l1j.server.Config;
import l1j.server.server.clientpackets.ClientBasePacket;
import l1j.server.server.datatables.EnchantWeaponChance;
import l1j.server.server.model.L1Character;
import l1j.server.server.model.L1PcInventory;
import l1j.server.server.model.Instance.L1ItemInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.model.item.L1ItemId;
import l1j.server.server.serverpackets.S_ServerMessage;
import l1j.server.server.serverpackets.S_SystemMessage;
import l1j.server.server.templates.L1EnchantWeaponChance;
import l1j.server.server.templates.L1Item;
import l1j.server.server.utils.CommonUtil;

@SuppressWarnings("serial")
public class EnchantWeapon extends Enchant {

	private static Random _random = new Random(System.nanoTime());

	public EnchantWeapon(L1Item item) {
		super(item);
	}

	@Override
	public void clickItem(L1Character cha, ClientBasePacket packet) {
		if (cha instanceof L1PcInstance) {
			L1PcInstance pc = (L1PcInstance) cha;
			L1ItemInstance useItem = pc.getInventory().getItem(this.getId());
			int itemId = this.getItemId();
			L1ItemInstance l1iteminstance1 = pc.getInventory().getItem(packet.readD());

			if (l1iteminstance1 == null) {
				// System.out.println("무기 인챈 버그 의심"+pc.getName());
				pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true); // \f1 아무것도 일어나지 않았습니다.
				return;
			}

			if (pc.getLastEnchantItemid() == l1iteminstance1.getId()) {
				pc.setLastEnchantItemid(l1iteminstance1.getId(),
						l1iteminstance1);
				return;
			}
			if (l1iteminstance1.isEquipped()) {
				pc.sendPackets(new S_SystemMessage(
						"착용을 해제한 후 강화할 수 있습니다.")); return; }
			if (l1iteminstance1 == null
					|| l1iteminstance1.getItem().getType2() != 1) {
				pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true); // \f1 아무것도 일어나지 않았습니다.
				return;
			}
			if (l1iteminstance1.isEquipped()) {
				pc.sendPackets(new S_SystemMessage(
						"착용을 해제한 후 강화할 수 있습니다.")); return; }
			if (l1iteminstance1.getBless() >= 128) { // 봉인템
				pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true); // \f1
				// 아무것도
				// 일어나지
				// 않았습니다.
				return;
			}
			int safe_enchant = l1iteminstance1.getItem().get_safeenchant();
			if (l1iteminstance1.isEquipped()) {
				pc.sendPackets(new S_SystemMessage(
						"착용을 해제한 후 강화할 수 있습니다.")); return; }
			if (safe_enchant < 0) { // 강화 불가
				pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true); // \f1
				// 아무것도
				// 일어나지
				// 않았습니다.
				return;
			}
			int weaponId = l1iteminstance1.getItem().getItemId();
			if (l1iteminstance1.isEquipped()) {
				pc.sendPackets(new S_SystemMessage(
						"착용을 해제한 후 강화할 수 있습니다.")); return; }
			if (weaponId >= 246 && weaponId <= 249) { // 강화 불가
				if (itemId == L1ItemId.SCROLL_OF_ENCHANT_QUEST_WEAPON) {// 시련의
					// 스크롤
				} else {
					pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."),
							true); // \f1 아무것도 일어나지 않았습니다.
					return;
				}
			}
			if (l1iteminstance1.isEquipped()) {
				pc.sendPackets(new S_SystemMessage(
						"착용을 해제한 후 강화할 수 있습니다.")); return; }
			if (itemId == L1ItemId.SCROLL_OF_ENCHANT_QUEST_WEAPON) {
				// 시련의 스크롤
				if (weaponId >= 246 && weaponId <= 249) { // 강화 불가
				} else {
					pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."),
							true); // \f1 아무것도 일어나지 않았습니다.
					return;
				}
			}
			if (l1iteminstance1.isEquipped()) {
				pc.sendPackets(new S_SystemMessage(
						"착용을 해제한 후 강화할 수 있습니다.")); return; }
			/** 환상의 무기 마법 주문서 **/
			if (weaponId >= 413000 && weaponId <= 413007) { // 이외에 강화 불가
				if (itemId == L1ItemId.SCROLL_OF_ENCHANT_FANTASY_WEAPON) {// 환상의무기마법주문서
				} else {
					pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."),
							true); // \f1 아무것도 일어나지 않았습니다.
					return;
				}
			}
			if (itemId == L1ItemId.SCROLL_OF_ENCHANT_FANTASY_WEAPON) {// 환상의무기마법주문서
				if (weaponId >= 413000 && weaponId <= 413007) { // 이외에 강화 불가
				} else {
					pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."),
							true); // \f1 아무것도 일어나지 않았습니다.
					return;
				}
			}
			/** 환상의 무기 마법 주문서 **/
			if (weaponId >= 284 && weaponId <= 290) { // 이외에 강화 불가
				if (itemId == 60473 && l1iteminstance1.getEnchantLevel() < 10) {// 환상의무기마법주문서
					// (10검)
				} else {
					pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."),
							true); // \f1 아무것도 일어나지 않았습니다.
					return;
				}
			}
			if (itemId == 60473) {// 환상의무기마법주문서 (10검)
				if (weaponId >= 284 && weaponId <= 290) { // 이외에 강화 불가
				} else {
					pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."),
							true); // \f1 아무것도 일어나지 않았습니다.
					return;
				}
			}

			/** 환상의 무기 마법 주문서 **/
			/**	if (weaponId >= 90085 && weaponId <= 90092) { // 이외에 강화 불가
				if (itemId == 160510) {// 환상의무기마법주문서 (10검)
				} else {
					pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."),
							true); // \f1 아무것도 일어나지 않았습니다.
					return;
				}
			}
			if (itemId == 160510) {// 환상의무기마법주문서 (10검)
				if (weaponId >= 90085 && weaponId <= 90092) { // 이외에 강화 불가
				} else {
					pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."),
							true); // \f1 아무것도 일어나지 않았습니다.
					return;
				}
			}**/

			if (weaponId == 7 || weaponId == 35 || weaponId == 48
					|| weaponId == 73 || weaponId == 105 || weaponId == 120
					|| weaponId == 147 || weaponId == 156 || weaponId == 174
					|| weaponId == 175 || weaponId == 224 || weaponId == 7232) {
				if (itemId == L1ItemId.여행무기주문서 || itemId == 60142) {
					int enchant_level = l1iteminstance1.getEnchantLevel();
					if (enchant_level >= 6) {
						pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."),
								true);
						return;
					}
				} else {
					pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."),
							true);
					return;
				}
			}
			if (itemId == L1ItemId.여행무기주문서 || itemId == 60142) {
				if (weaponId == 7 || weaponId == 35 || weaponId == 48
						|| weaponId == 73 || weaponId == 105 || weaponId == 120
						|| weaponId == 147 || weaponId == 156
						|| weaponId == 174 || weaponId == 175
						|| weaponId == 7232 || weaponId == 224) {
					int enchant_level = l1iteminstance1.getEnchantLevel();
					if (enchant_level >= 6) {
						pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."),
								true);
						return;
					}
				} else {
					pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."),
							true);
					return;
				}
			}

			/** 창천 무기 마법 주문서 **/
			if (weaponId >= 411000 && weaponId <= 411035) {
				if (itemId == L1ItemId.CHANGCHUN_ENCHANT_WEAPON_SCROLL) {
				} else {
					pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."),
							true);
					return;
				}
			}
			if (itemId == L1ItemId.CHANGCHUN_ENCHANT_WEAPON_SCROLL) {
				if (weaponId >= 411000 && weaponId <= 411035) {
				} else {
					pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."),
							true);
					return;
				}
			}
			/** 창천 무기 마법 주문서 **/

			int enchant_level = l1iteminstance1.getEnchantLevel();
			/*
			 * if (Config.GAME_SERVER_TYPE == 0 && enchant_level >=
			 * safe_enchant+3 && (itemId != L1ItemId.WIND_ENCHANT_WEAPON_SCROLL
			 * || itemId != L1ItemId.EARTH_ENCHANT_WEAPON_SCROLL || itemId !=
			 * L1ItemId.WATER_ENCHANT_WEAPON_SCROLL || itemId !=
			 * L1ItemId.FIRE_ENCHANT_WEAPON_SCROLL)){ pc.sendPackets(new
			 * S_SystemMessage("테스트서버에서는 안전인챈+3 이상은 인챈하실수 없습니다.")); return; }
			 */

			if (safe_enchant == 0){
				if(enchant_level >= Config.MAX_WEAPON1){
					pc.sendPackets(new S_SystemMessage("\\fW안전인챈 0 무기는 현재 +" + Config.MAX_WEAPON1 + "이상은 인챈할수 없습니다."), true);
					return;
				}
			}
			if (itemId == L1ItemId.고대무기) {
				if (CommonUtil.random(100) < Config.godew_ENCHANT) {
					SuccessEnchant(pc, l1iteminstance1, RandomELevel(l1iteminstance1, itemId));
					pc.sendPackets(new S_SystemMessage("\\fW축하합니다!인첸트에 성공하여 아이템의 인첸수치가 +1 올라갔습니다."));
				} else {
					pc.sendPackets(new S_SystemMessage("\\fW인첸트에 실패하였으나 인첸트 수치가 보존 됩니다."));
				}
				pc.getInventory().removeItem(useItem, 1);
				return;
			} 
			if (itemId == 60510) { // 장인의 무기 마법 주문서
				if (safe_enchant == 0 || enchant_level != 9) {
					pc.sendPackets(new S_SystemMessage("이 아이템에 사용할 수 없습니다."), true);
					return;
				} 
				
				if (enchant_level == 9) {
					if (_random.nextInt(100) < Config.JANGIN_ENCHANT) {
						SuccessEnchant(pc, l1iteminstance1, 1);
					} else {
						pc.sendPackets(new S_ServerMessage(160, l1iteminstance1.getLogName(), "$245", "$248"), true);
					}
					pc.getInventory().removeItem(useItem, 1);
				}
			} else if (itemId == L1ItemId.C_SCROLL_OF_ENCHANT_WEAPON) { // c-dai
				pc.getInventory().removeItem(useItem, 1);
				if (enchant_level < -6) {
					// -7이상은 할 수 없다.
					FailureEnchant(pc, l1iteminstance1);
				} else {
					SuccessEnchant(pc, l1iteminstance1, -1);
				}
				if (l1iteminstance1.isEquipped()) {
					pc.sendPackets(new S_SystemMessage(
							"착용을 해제한 후 강화할 수 있습니다.")); return; }
			} else if (itemId == L1ItemId.WIND_ENCHANT_WEAPON_SCROLL_100
					|| itemId == L1ItemId.EARTH_ENCHANT_WEAPON_SCROLL_100
					|| itemId == L1ItemId.WATER_ENCHANT_WEAPON_SCROLL_100
					|| itemId == L1ItemId.FIRE_ENCHANT_WEAPON_SCROLL_100
					|| itemId == L1ItemId.WIND_ENCHANT_WEAPON_SCROLL
					|| itemId == L1ItemId.EARTH_ENCHANT_WEAPON_SCROLL
					|| itemId == L1ItemId.WATER_ENCHANT_WEAPON_SCROLL
					|| itemId == L1ItemId.FIRE_ENCHANT_WEAPON_SCROLL) {
				AttrEnchant(pc, l1iteminstance1, itemId);
				if (l1iteminstance1.isEquipped()) {
					pc.sendPackets(new S_SystemMessage(
							"착용을 해제한 후 강화할 수 있습니다.")); return; }
			} else if (itemId >= 60355 && itemId <= 60358) {
				AttrChange(pc, l1iteminstance1, itemId);
				if (l1iteminstance1.isEquipped()) {
					pc.sendPackets(new S_SystemMessage(
							"착용을 해제한 후 강화할 수 있습니다.")); return; }
			} else if (itemId == L1ItemId.Add_ENCHANT_WEAPON_SCROLL
					|| itemId == L1ItemId.Add_ENCHANT_WEAPON_SCROLL_100) {
				StepEnchant(pc, l1iteminstance1, itemId);
				if (l1iteminstance1.isEquipped()) {
					pc.sendPackets(new S_SystemMessage(
							"착용을 해제한 후 강화할 수 있습니다.")); return; }
			} else if (enchant_level < safe_enchant && safe_enchant > 0) {
				pc.getInventory().removeItem(useItem, 1);
				SuccessEnchant(pc, l1iteminstance1,
						RandomELevel(l1iteminstance1, itemId));
				if (l1iteminstance1.isEquipped()) {
					pc.sendPackets(new S_SystemMessage(
							"착용을 해제한 후 강화할 수 있습니다.")); return; }
			} else {
				pc.getInventory().removeItem(useItem, 1);
				if (enchant_level >= Config.MAX_WEAPON)
					if (!(itemId == L1ItemId.WIND_ENCHANT_WEAPON_SCROLL
					&& itemId == L1ItemId.EARTH_ENCHANT_WEAPON_SCROLL
					&& itemId != L1ItemId.Add_ENCHANT_WEAPON_SCROLL// 추가강화줌서.
					|| itemId == L1ItemId.WATER_ENCHANT_WEAPON_SCROLL
					&& itemId == L1ItemId.FIRE_ENCHANT_WEAPON_SCROLL)) {
						pc.sendPackets(new S_SystemMessage("\\fW모든 무기는 현재 +"
								+ Config.MAX_WEAPON + "이상은 인챈할수 없습니다."), true);
						return;
					}

				int rnd = _random.nextInt(100) + 1;
				int chance = 0;
				/** 무기별 인첸당  확률 **/
				L1EnchantWeaponChance l1enchantchance = EnchantWeaponChance.getInstance().getTemplate(l1iteminstance1.getItem().getItemId());
				int e0 = l1enchantchance.get0();
				int e1 = l1enchantchance.get1();
				int e2 = l1enchantchance.get2();
				int e3 = l1enchantchance.get3();
				int e4 = l1enchantchance.get4();
				int e5 = l1enchantchance.get5();
				int e6 = l1enchantchance.get6();
				int e7 = l1enchantchance.get7();
				int e8 = l1enchantchance.get8();
				int e9 = l1enchantchance.get9();
				int e10 = l1enchantchance.get10();
				int e11 = l1enchantchance.get11();
				int e12 = l1enchantchance.get12();
				int e13 = l1enchantchance.get13();
				int e14 = l1enchantchance.get14();
				try{
					if(l1enchantchance != null){
						int enchant = l1iteminstance1.getEnchantLevel();
						switch(enchant){
						case 0 : chance += e0; break;
						case 1 : chance += e1; break;
						case 2 : chance += e2; break;
						case 3 : chance += e3; break;
						case 4 : chance += e4; break;
						case 5 : chance += e5; break;
						case 6 : chance += e6; break;
						case 7 : chance += e7; break;
						case 8 : chance += e8; break;
						case 9 : chance += e9; break;
						case 10 : chance += e10; break;
						case 11 : chance += e11; break;
						case 12 : chance += e12; break;
						case 13 : chance += e13; break;
						case 14 : chance += e14; break;
						default:
							break;
						}
					}
				} catch (Exception e){
					System.out.println("Character Enchant Weapon Chance Load Error");
				}		
				if (itemId == L1ItemId.TEST_ENCHANT_WEAPON) { // 빨간부분 추가
					chance = 10000;
				}
				if(pc.인첸축복){
					chance = 100;
					pc.인첸축복 = false;
				}
				if (pc.isGm()) {
					pc.sendPackets(new S_SystemMessage("\\fY성공확률 : [ " + chance + " ]"));
					pc.sendPackets(new S_SystemMessage("\\fY찬스 : [ " + rnd + " ]"));
				}
				// 100%인첸주문서 //
				if (rnd < chance) {
					int randomEnchantLevel = RandomELevel(l1iteminstance1, itemId);
					SuccessEnchant(pc, l1iteminstance1, randomEnchantLevel);
				} else if (enchant_level >= 9 && rnd < (chance * 2)) {
					pc.sendPackets(new S_ServerMessage(160, l1iteminstance1 .getLogName(), "$245", "$248"), true);
				} else {
					FailureEnchant(pc, l1iteminstance1);
					if (l1iteminstance1.isEquipped()) {
						pc.sendPackets(new S_SystemMessage(
								"착용을 해제한 후 강화할 수 있습니다.")); return; 
					}
				}
			}
		}
	}




	private void AttrChange(L1PcInstance pc, L1ItemInstance item, int itemId) {
		// TODO 자동 생성된 메소드 스텁
		int attr_level = item.getAttrEnchantLevel();
		if (attr_level == 0) {
			pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
			return;
		}
		if (itemId == 60355) {// 풍령 속성 변환
			if (attr_level == 1 || attr_level == 6 || attr_level == 16)
				item.setAttrEnchantLevel(11);
			else if (attr_level == 2 || attr_level == 7 || attr_level == 17)
				item.setAttrEnchantLevel(12);
			else if (attr_level == 3 || attr_level == 8 || attr_level == 18)
				item.setAttrEnchantLevel(13);
			else if (attr_level == 4 || attr_level == 9 || attr_level == 19)
				item.setAttrEnchantLevel(14);
			else if (attr_level == 5 || attr_level == 10 || attr_level == 20)
				item.setAttrEnchantLevel(15);
		} else if (itemId == 60356) {// 지령 속성 변환
			if (attr_level == 1 || attr_level == 6 || attr_level == 11)
				item.setAttrEnchantLevel(16);
			else if (attr_level == 2 || attr_level == 7 || attr_level == 12)
				item.setAttrEnchantLevel(17);
			else if (attr_level == 3 || attr_level == 8 || attr_level == 13)
				item.setAttrEnchantLevel(18);
			else if (attr_level == 4 || attr_level == 9 || attr_level == 14)
				item.setAttrEnchantLevel(19);
			else if (attr_level == 5 || attr_level == 10 || attr_level == 15)
				item.setAttrEnchantLevel(20);
		} else if (itemId == 60357) {// 수령 속성 변환
			if (attr_level == 1 || attr_level == 11 || attr_level == 16)
				item.setAttrEnchantLevel(6);
			else if (attr_level == 2 || attr_level == 12 || attr_level == 17)
				item.setAttrEnchantLevel(7);
			else if (attr_level == 3 || attr_level == 13 || attr_level == 18)
				item.setAttrEnchantLevel(8);
			else if (attr_level == 4 || attr_level == 14 || attr_level == 19)
				item.setAttrEnchantLevel(9);
			else if (attr_level == 5 || attr_level == 15 || attr_level == 20)
				item.setAttrEnchantLevel(10);
		} else if (itemId == 60358) {// 화령 속성 변환
			if (attr_level == 6 || attr_level == 11 || attr_level == 16)
				item.setAttrEnchantLevel(1);
			else if (attr_level == 7 || attr_level == 12 || attr_level == 17)
				item.setAttrEnchantLevel(2);
			else if (attr_level == 8 || attr_level == 13 || attr_level == 18)
				item.setAttrEnchantLevel(3);
			else if (attr_level == 9 || attr_level == 14 || attr_level == 19)
				item.setAttrEnchantLevel(4);
			else if (attr_level == 10 || attr_level == 15 || attr_level == 20)
				item.setAttrEnchantLevel(5);
		}

		pc.sendPackets(new S_ServerMessage(1410, item.getLogName()), true);
		pc.getInventory().consumeItem(itemId, 1);
		pc.getInventory().updateItem(item, L1PcInventory.COL_ATTRENCHANTLVL);
		pc.getInventory().saveItem(item, L1PcInventory.COL_ATTRENCHANTLVL);
		pc.saveInventory();
	}
}
