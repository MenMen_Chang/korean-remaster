/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package l1j.server.server.model;

import static l1j.server.server.model.skill.L1SkillId.DETECTION;
import static l1j.server.server.model.skill.L1SkillId.ENCHANT_WEAPON;
import static l1j.server.server.model.skill.L1SkillId.EXTRA_HEAL;
import static l1j.server.server.model.skill.L1SkillId.GREATER_HASTE;
import static l1j.server.server.model.skill.L1SkillId.HASTE;
import static l1j.server.server.model.skill.L1SkillId.HEAL;
import static l1j.server.server.model.skill.L1SkillId.PHYSICAL_ENCHANT_DEX;
import static l1j.server.server.model.skill.L1SkillId.PHYSICAL_ENCHANT_STR;

import java.util.ArrayList;

import l1j.server.server.datatables.SkillsTable;
import l1j.server.server.model.Instance.L1DollInstance;
import l1j.server.server.model.Instance.L1ItemInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.model.skill.L1SkillId;
import l1j.server.server.serverpackets.S_ACTION_UI;
import l1j.server.server.serverpackets.S_Ability;
import l1j.server.server.serverpackets.S_AddSkill;
import l1j.server.server.serverpackets.S_DelSkill;
import l1j.server.server.serverpackets.S_Invis;
import l1j.server.server.serverpackets.S_ItemStatus;
import l1j.server.server.serverpackets.S_NewCreateItem;
import l1j.server.server.serverpackets.S_OwnCharAttrDef;
import l1j.server.server.serverpackets.S_OwnCharStatus;
import l1j.server.server.serverpackets.S_PacketBox;
import l1j.server.server.serverpackets.S_SPMR;
import l1j.server.server.serverpackets.S_SabuBox;
import l1j.server.server.serverpackets.S_SkillHaste;
import l1j.server.server.serverpackets.S_SkillIconBlessOfEva;
import l1j.server.server.serverpackets.S_SkillIconGFX;
import l1j.server.server.serverpackets.S_WorldPutObject;
import l1j.server.server.templates.L1Item;

public class L1EquipmentSlot {

	private L1PcInstance _owner;

	private ArrayList<L1ArmorSet> _currentArmorSet;

	private L1ItemInstance _weapon;

	private ArrayList<L1ItemInstance> _armors;

	public L1EquipmentSlot(L1PcInstance owner) {
		_owner = owner;

		_armors = new ArrayList<L1ItemInstance>();
		_currentArmorSet = new ArrayList<L1ArmorSet>();
	}

	private void setWeapon(L1ItemInstance weapon, boolean doubleweapon) {
		if (doubleweapon) {
			_owner.setSecondWeapon(weapon);
			_owner.setCurrentWeapon(88);
			weapon._isSecond = true;
		} else {
			_owner.setWeapon(weapon);
			_owner.setCurrentWeapon(weapon.getItem().getType1());
		}

		weapon.startEquipmentTimer(_owner);
		_weapon = weapon;

		int itemId = weapon.getItem().getItemId();
		int itemlvl = weapon.getEnchantLevel();

		/*
		 * if (weapon.getItem().getType() == 7 || weapon.getItem().getType() == 16 ||
		 * weapon.getItem().getType() == 17) { if (weapon.getBless() == 0 ||
		 * weapon.getBless() == 128) { _owner.getAbility().addSp(1);
		 * _owner.sendPackets(new S_SPMR(_owner)); } }
		 */

		if (itemId == 124 || itemId == 121 || itemId == 100124) {
			switch (itemlvl) {
			case 7:
				_owner.getAbility().addSp(1);
				break;
			case 8:
				_owner.getAbility().addSp(2);
				break;
			default:
				if (itemlvl >= 9) {
					_owner.getAbility().addSp(3);
				}
				break;
			}
			_owner.sendPackets(new S_SPMR(_owner));
		}

		if (itemId >= 11011 && itemId <= 11013) {
			L1PolyMorph.doPoly(_owner, 8768, 0, L1PolyMorph.MORPH_BY_LOGIN);
		} // �߰�
		if (itemId == 7236) {
			L1PolyMorph.doPoly(_owner, 12232, 0, L1PolyMorph.MORPH_BY_LOGIN);
		}
		// ����
		if (itemId == 61 || itemId == 12 || itemId == 86 || itemId == 134 || itemId == 9100 || itemId == 9101
				|| itemId == 9103) {
			_owner.addDmgup(itemlvl * 2);
		}

		if (weapon.getItemId() == 61 || itemId == 9101 || itemId == 9103) {
			_owner.add�ٰŸ�ġ����(weapon.getEnchantLevel() + 1);
		}

		if (weapon.getItemId() == 134) {
			_owner.addSuccMagic(weapon.getEnchantLevel() + 2);
		}

		if (weapon.getItemId() == 9102) {
			_owner.addCriMagic(weapon.getEnchantLevel() + 1);
		}

		if (weapon.getItemId() == 66) { // �彽
			_owner.addDmgup(itemlvl * 2);
		}
		// ����
		if (weapon.getItemId() == 59) {
			_owner.addHitup(3);
		}
		if (weapon.getItemId() == 9100) {
			_owner.addDamageReductionByArmor(2);
			_owner.add���Ÿ�ġ����(weapon.getEnchantLevel() + 1);
		}
		if (weapon.getItemId() == 293 || weapon.getItemId() == 189 || weapon.getItemId() == 100189) {
			_owner.add���Ÿ�ġ����(3);
		}
		if (weapon.getItemId() == 30220) {
			_owner.add���Ÿ�ġ����(2);
		}
		if (weapon.getItemId() == 413105) {
			_owner.add���Ÿ�ġ����(1);
		}
		if (weapon.getItemId() == 6101) {
			switch (weapon.getEnchantLevel()) {
			case 0:
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
				_owner.addCriMagic(1);
				break;
			case 7:
				_owner.addCriMagic(2);
				break;
			case 8:
				_owner.addCriMagic(3);
				break;
			case 9:
				_owner.addCriMagic(4);
				break;
			default:
				if (weapon.getEnchantLevel() >= 10) {
					_owner.addCriMagic(5);
				}
				break;
			}
		}

		/** �ູ �ֹ��� 3 �ܰ� �ý��� by K **/
		int BlessLevel = weapon.getBlessEnchantLevel() * 2;
		if (weapon.getBlessEnchantLevel() > 0) {
			if (weapon.getBless() == 0 || weapon.getBless() == 128) {
				switch (weapon.getItem().getType()) {
				case 7:
				case 16:
				case 17:
					_owner.getAbility().addSp(1 + BlessLevel);
					_owner.addSuccMagic(1 + BlessLevel);
					_owner.sendPackets(new S_SPMR(_owner));
					break;
				case 4:
				case 13:
					_owner.addBowDmgup(1 + BlessLevel);
					_owner.addBowHitup(1 + BlessLevel);
					break;
				case 1:
				case 2:
				case 3:
				case 5:
				case 6:
				case 9:
				case 10:
				case 11:
				case 12:
				case 14:
				case 15:
				case 18:
					_owner.addDmgup(1 + BlessLevel);
					_owner.addHitup(1 + BlessLevel);
					break;
				}
			}
		}
		/** ������� **/

		if (weapon.getItem().getType() == 17 && weapon.getItem().getType2() == 1 && weapon.getStepEnchantLevel() != 0) {
			_owner.getAbility().addAddedInt(weapon.getEnchantLevel());
			_owner.sendPackets(new S_OwnCharStatus(_owner));
		}

		if (weapon.getItemId() == 134 || weapon.getItemId() == 9102) {
			_owner.getAbility().addSp((weapon.getEnchantLevel()));
			_owner.sendPackets(new S_SPMR(_owner));
		}

		if (weapon.getItemId() == 261) { // ������ ������
			_owner.addMpr(weapon.getEnchantLevel());
		}

		if (weapon.getItem().getType2() == 1 && weapon.getItem().getType() != 7 && weapon.getItem().getType() != 17
				&& weapon.getStepEnchantLevel() != 0) {
			_owner.addDmgup(weapon.getStepEnchantLevel());
		}

		/** ������� ������ ���� **/
		if (weapon.get_durability() > 0)
			_owner.sendPackets(new S_PacketBox(S_PacketBox.����ջ󸶿콺, weapon.get_durability()), true);

		if (weapon.getSkill() != null && weapon.getSkill().getSkillId() != 0) {
			/*
			 * switch (weapon.getSkill().getSkillId()) { case L1SkillId.BLESS_WEAPON:
			 * _owner.sendPackets(new S_PacketBox(S_PacketBox.SKILL_WEAPON_ICON, 2176,
			 * weapon.getSkill().getTime(), doubleweapon, false)); break; case
			 * L1SkillId.HOLY_WEAPON: _owner.sendPackets(new
			 * S_PacketBox(S_PacketBox.SKILL_WEAPON_ICON, 2165, weapon.getSkill().getTime(),
			 * false, false)); break; case L1SkillId.ENCHANT_WEAPON: _owner.sendPackets(new
			 * S_PacketBox(S_PacketBox.SKILL_WEAPON_ICON, 747, weapon.getSkill().getTime(),
			 * doubleweapon, false)); break; case L1SkillId.SHADOW_FANG:
			 * _owner.sendPackets(new S_PacketBox(S_PacketBox.SKILL_WEAPON_ICON, 2951,
			 * weapon.getSkill().getTime(), false, false)); break; default: break; }
			 */
		}
	}

	public L1ItemInstance getWeapon() {
		return _weapon;
	}

	private void setArmor(L1ItemInstance armor, boolean loaded) {
		L1Item item = armor.getItem();
		int itemlvl = armor.getEnchantLevel();
		int itemtype = armor.getItem().getType();
		int itemId = armor.getItem().getItemId();
		int itemgrade = armor.getItem().getGrade();
		int RegistLevel = armor.getRegistLevel();

		if (itemtype == 2) {
			_owner.getAC()
					.addAc(item.get_ac() - armor.getEnchantLevel() - armor.getAcByMagic() + armor.get_durability());
		} else if (itemtype >= 8 && itemtype <= 12) {
			_owner.getAC().addAc(item.get_ac() - armor.getAcByMagic());
		} else if (itemtype == 18 || itemtype == 16) {
			_owner.getAC().addAc(item.get_ac() - armor.getAcByMagic());
		} else {
			if (itemId == 20016 || itemId == 20294 || itemId == 120016 || itemId == 220294) {
				_owner.getAC().addAc(item.get_ac());
				_owner.getAC().addAc(armor.getEnchantLevel());
				_owner.getAC().addAc(armor.getAcByMagic());
			} else {
				_owner.getAC().addAc(item.get_ac() - armor.getEnchantLevel() - armor.getAcByMagic());
			}
		}

		/** �ູ �ֹ��� 3 �ܰ� �ý��� by K **/
		if (armor.getBlessEnchantLevel() != 0) {
			if (!(armor.getItem().getType() >= 8 && armor.getItem().getType() <= 12)) {
				_owner.addDamageReductionByArmor(armor.getBlessEnchantLevel());
			}
		}
		/** ������� **/

		if (itemId == 420104 || itemId == 420105 || itemId == 420106 || itemId == 420107) {
			_owner.startPapuBlessing();
		}
		if (itemId >= 420108 && itemId <= 420111) {
			_owner.startLindBlessing();
		}
		if (itemId == 20050) {
			if (itemlvl == 5) {
				_owner.Add_Er(1);
			} else if (itemlvl == 6) {
				_owner.Add_Er(2);
			} else if (itemlvl == 7) {
				_owner.Add_Er(3);
			} else if (itemlvl == 8) {
				_owner.Add_Er(4);
			} else if (itemlvl >= 9) {
				_owner.Add_Er(5);
			}
		}
		if (itemId == 21255)
			_owner.startHalloweenArmorBlessing();

		_owner.addDamageReductionByArmor(item.getDamageReduction());
		_owner.addWeightReduction(item.getWeightReduction());

		if (item.getWeightReduction() != 0) {
			_owner.sendPackets(new S_NewCreateItem("����", _owner));
		}

		if (itemId == 9900 && itemId == 9974) {
			_owner.addPVPMagicDamageReduction(1);
		}
		if (itemId == 9800 && itemId == 9874) {
			_owner.addPVPMagicDamageReduction(2);
		}
		if (itemId == 9700 && itemId == 9774) {
			_owner.addPVPMagicDamageReduction(3);
		}

		if (itemId == 7246) {
			if (itemlvl > 5) {
				int en = itemlvl - 5;
				_owner.addWeightReduction(en * 60);
			}
		}
		if (itemId == 9112) {
			_owner.addMaxHp(itemlvl * 5);
		}
		if (itemId == 9208) {
			_owner.addSuccMagic(2);
		}
		if (itemId == 20048) {
			_owner.addSuccMagic(5);
		}
		if (itemId >= 9730 && itemId <= 9734) {
			_owner.addSuccMagic(10);
		}
		if (itemId == 9206) {
			_owner.addSuccMagic(1);
		}

		if (itemId == 421216) {
			_owner.addSuccMagic(2);
		}
		if (itemId == 421219) {
			_owner.addSuccMagic(6);
		}
		if (itemId == 431219) {
			_owner.addSuccMagic(2);
		}
		if (itemId == 20049||itemId == 20079||itemId == 420104||itemId == 420105||itemId == 420106||itemId == 20107) { // By.�ں� 25��þ �ǿ���,�����۾�
			switch (itemlvl) {
			case 25:
				
				_owner.addMaxHp(100);
				_owner.addMaxMp(50);
				_owner.addDamageReductionByArmor(2);
				break;
		
			}
		}
		if (itemId == 9501) { // ��ȣ�� ��������
			switch (itemlvl) {
			case 5:
				_owner.getAC().addAc(-3);
				_owner.addMaxHp(30);
				_owner.addDamageReductionByArmor(1);
				_owner.addDmgupByArmor(1);
				break;
			case 6:
				_owner.getAC().addAc(-5);
				_owner.addMaxHp(35);
				_owner.addDamageReductionByArmor(2);
				_owner.getResistance().addMr(3);
				_owner.addDmgupByArmor(2);
				_owner.addHitupByArmor(1);
				_owner.add�ٰŸ�ġ����(1);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 7:
				_owner.getAC().addAc(-6);
				_owner.addMaxHp(40);
				_owner.addDamageReductionByArmor(3);
				_owner.getResistance().addMr(5);
				_owner.addDmgupByArmor(3);
				_owner.addHitupByArmor(3);
				_owner.add�ٰŸ�ġ����(3);
				_owner.addPvPReductionByArmor(1);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 8:
				_owner.getAC().addAc(-7);
				_owner.addMaxHp(50);
				_owner.addDamageReductionByArmor(4);
				_owner.getResistance().addMr(7);
				_owner.addDmgupByArmor(4);
				_owner.addHitupByArmor(5);
				_owner.add�ٰŸ�ġ����(5);
				_owner.addPvPReductionByArmor(2);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			}
		}
		if (itemId == 9502) { // ��ȣ�� ��������
			switch (itemlvl) {
			case 5:
				_owner.getAC().addAc(-3);
				_owner.addMaxHp(30);
				_owner.addDamageReductionByArmor(1);
				_owner.addDmgupByArmor(1);
				break;
			case 6:
				_owner.getAC().addAc(-5);
				_owner.addMaxHp(35);
				_owner.addDamageReductionByArmor(2);
				_owner.getResistance().addMr(3);
				_owner.addBowDmgupByArmor(2);
				_owner.addBowHitupByArmor(1);
				_owner.add���Ÿ�ġ����(1);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 7:
				_owner.getAC().addAc(-6);
				_owner.addMaxHp(40);
				_owner.addDamageReductionByArmor(3);
				_owner.getResistance().addMr(5);
				_owner.addBowDmgupByArmor(3);
				_owner.addBowHitupByArmor(3);
				_owner.add���Ÿ�ġ����(3);
				_owner.addPvPReductionByArmor(1);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 8:
				_owner.getAC().addAc(-7);
				_owner.addMaxHp(50);
				_owner.addDamageReductionByArmor(4);
				_owner.getResistance().addMr(7);
				_owner.addBowDmgupByArmor(4);
				_owner.addBowHitupByArmor(5);
				_owner.add���Ÿ�ġ����(5);
				_owner.addPvPReductionByArmor(2);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			}
		}
		if (itemId == 9503) { // ��ȣ�� ��������
			switch (itemlvl) {
			case 5:
				_owner.getAC().addAc(-3);
				_owner.addMaxHp(30);
				_owner.addDamageReductionByArmor(1);
				_owner.addDmgupByArmor(1);
				break;
			case 6:
				_owner.getAC().addAc(-5);
				_owner.addMaxHp(35);
				_owner.addDamageReductionByArmor(2);
				_owner.getResistance().addMr(3);
				_owner.addSuccMagic(2);
				_owner.addHitupByArmor(2);
				_owner.addCriMagic(1);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 7:
				_owner.getAC().addAc(-6);
				_owner.addMaxHp(40);
				_owner.addDamageReductionByArmor(3);
				_owner.getResistance().addMr(5);
				_owner.addSuccMagic(3);
				_owner.addHitupByArmor(3);
				_owner.addCriMagic(2);
				_owner.addPvPReductionByArmor(1);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 8:
				_owner.getAC().addAc(-7);
				_owner.addMaxHp(50);
				_owner.addDamageReductionByArmor(4);
				_owner.getResistance().addMr(7);
				_owner.addSuccMagic(5);
				_owner.addHitupByArmor(2);
				_owner.addCriMagic(4);
				_owner.addPvPReductionByArmor(2);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			}
		}
		if (itemId == 31910) {
			switch (itemlvl) {
			case 4:
				_owner.addHitupByArmor(1);
				break;
			case 5:
				_owner.addDmgupByArmor(1);
				_owner.addHitupByArmor(1);
				break;
			case 6:
				_owner.addDmgupByArmor(2);
				_owner.addHitupByArmor(2);
				break;
			case 7:
				_owner.addDmgupByArmor(3);
				_owner.addHitupByArmor(3);
				break;
			case 8:
				_owner.addDmgupByArmor(4);
				_owner.addHitupByArmor(4);
				break;
			default:
				break;
			}
		}
		if (itemId == 31911) {
			switch (itemlvl) {
			case 4:
				_owner.addBowHitupByArmor(1);
				break;
			case 5:
				_owner.addBowDmgupByArmor(1);
				_owner.addBowHitupByArmor(1);
				break;
			case 6:
				_owner.addBowDmgupByArmor(2);
				_owner.addBowHitupByArmor(2);
				break;
			case 7:
				_owner.addBowDmgupByArmor(3);
				_owner.addBowHitupByArmor(3);
				break;
			case 8:
				_owner.addBowDmgupByArmor(4);
				_owner.addBowHitupByArmor(4);
				break;
			default:
				break;
			}
		}
		if (itemId == 31912) {
			switch (itemlvl) {
			case 4:
				_owner.addSuccMagic(1);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 5:
				_owner.getAbility().addSp(1);
				_owner.addSuccMagic(1);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 6:
				_owner.getAbility().addSp(2);
				_owner.addSuccMagic(2);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 7:
				_owner.getAbility().addSp(3);
				_owner.addSuccMagic(3);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 8:
				_owner.getAbility().addSp(4);
				_owner.addSuccMagic(4);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			default:
				break;
			}
		}
		if (itemId == 31913) {
			switch (itemlvl) {
			case 4:
				_owner.getResistance().addMr(1);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 5:
				_owner.getResistance().addMr(2);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 6:
				_owner.getResistance().addMr(3);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 7:
				_owner.getResistance().addMr(4);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 8:
				_owner.getResistance().addMr(5);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			default:
				break;
			}
		}
		if (itemId == 32910) {
			switch (itemlvl) {
			case 5:
				_owner.addDmgupByArmor(1);
				_owner.addHitupByArmor(2);
				_owner.getResistance().addMr(4);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 6:
				_owner.addDmgupByArmor(2);
				_owner.addHitupByArmor(3);
				_owner.getResistance().addMr(6);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 7:
				_owner.addDmgupByArmor(3);
				_owner.addHitupByArmor(4);
				_owner.getResistance().addMr(8);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 8:
				_owner.addDmgupByArmor(4);
				_owner.addHitupByArmor(5);
				_owner.getResistance().addMr(10);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			default:
				break;
			}
		}
		if (itemId == 32911) {
			switch (itemlvl) {
			case 5:
				_owner.addBowDmgupByArmor(1);
				_owner.addBowHitupByArmor(2);
				_owner.getResistance().addMr(4);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 6:
				_owner.addBowDmgupByArmor(2);
				_owner.addBowHitupByArmor(3);
				_owner.getResistance().addMr(6);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 7:
				_owner.addBowDmgupByArmor(3);
				_owner.addBowHitupByArmor(4);
				_owner.getResistance().addMr(8);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 8:
				_owner.addBowDmgupByArmor(4);
				_owner.addBowHitupByArmor(5);
				_owner.getResistance().addMr(10);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			default:
				break;
			}
		}
		if (itemId == 32912) {
			switch (itemlvl) {
			case 5:
				_owner.getAbility().addSp(1);
				_owner.addSuccMagic(2);
				_owner.getResistance().addMr(4);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 6:
				_owner.getAbility().addSp(2);
				_owner.addSuccMagic(3);
				_owner.getResistance().addMr(6);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 7:
				_owner.getAbility().addSp(3);
				_owner.addSuccMagic(4);
				_owner.getResistance().addMr(8);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 8:
				_owner.getAbility().addSp(4);
				_owner.addSuccMagic(5);
				_owner.getResistance().addMr(10);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			default:
				break;
			}
		}
		if (itemId == 9300) {
			switch (itemlvl) {
			case 0:
				_owner.addMaxHp(5);
				break;
			case 1:
				_owner.addMaxHp(10);
				break;
			case 2:
				_owner.addMaxHp(15);
				break;
			case 3:
				_owner.addMaxHp(20);
				break;
			case 4:
				_owner.addMaxHp(25);
				_owner.getAC().addAc(-1);
				break;
			case 5:
				_owner.addMaxHp(30);
				_owner.getAC().addAc(-2);
				_owner.addDmgupByArmor(1);
				break;
			case 6:
				_owner.addMaxHp(35);
				_owner.getAC().addAc(-3);
				_owner.addDmgupByArmor(2);
				_owner.add�ٰŸ�ġ����(1);
				break;
			case 7:
				_owner.addMaxHp(40);
				_owner.getAC().addAc(-3);
				_owner.addDmgupByArmor(3);
				_owner.add�ٰŸ�ġ����(3);
				break;
			case 8:
				_owner.addMaxHp(50);
				_owner.getAC().addAc(-3);
				_owner.addDmgupByArmor(4);
				_owner.add�ٰŸ�ġ����(5);
				break;
			default:
				if (itemlvl > 8) {
					_owner.addMaxHp(50);
					_owner.getAC().addAc(-3);
					_owner.addDmgupByArmor(4);
					_owner.add�ٰŸ�ġ����(5);
				}
				break;
			}
		}
		if (itemId == 9301) {
			switch (itemlvl) {
			case 0:
				_owner.addMaxHp(5);
				break;
			case 1:
				_owner.addMaxHp(10);
				break;
			case 2:
				_owner.addMaxHp(15);
				break;
			case 3:
				_owner.addMaxHp(20);
				break;
			case 4:
				_owner.addMaxHp(25);
				_owner.getAC().addAc(-1);
				break;
			case 5:
				_owner.addMaxHp(30);
				_owner.getAC().addAc(-2);
				_owner.addBowDmgupByArmor(1);
				break;
			case 6:
				_owner.addMaxHp(35);
				_owner.getAC().addAc(-3);
				_owner.addBowDmgupByArmor(2);
				_owner.add�ٰŸ�ġ����(1);
				break;
			case 7:
				_owner.addMaxHp(40);
				_owner.getAC().addAc(-3);
				_owner.addBowDmgupByArmor(3);
				_owner.add�ٰŸ�ġ����(3);
				break;
			case 8:
				_owner.addMaxHp(50);
				_owner.getAC().addAc(-3);
				_owner.addBowDmgupByArmor(4);
				_owner.add���Ÿ�ġ����(5);
				break;
			default:
				if (itemlvl > 8) {
					_owner.addMaxHp(50);
					_owner.getAC().addAc(-3);
					_owner.addBowDmgupByArmor(4);
					_owner.add���Ÿ�ġ����(5);
				}
				break;
			}
		}
		if (itemId == 9302) {
			switch (itemlvl) {
			case 0:
				_owner.addMaxHp(5);
				break;
			case 1:
				_owner.addMaxHp(10);
				break;
			case 2:
				_owner.addMaxHp(15);
				break;
			case 3:
				_owner.addMaxHp(20);
				break;
			case 4:
				_owner.addMaxHp(25);
				_owner.getAC().addAc(-1);
				break;
			case 5:
				_owner.addMaxHp(30);
				_owner.getAC().addAc(-2);
				_owner.addHitupByArmor(1);
				break;
			case 6:
				_owner.addMaxHp(35);
				_owner.getAC().addAc(-3);
				_owner.addHitupByArmor(2);
				_owner.addCriMagic(1);
				break;
			case 7:
				_owner.addMaxHp(40);
				_owner.getAC().addAc(-3);
				_owner.addHitupByArmor(3);
				_owner.addCriMagic(2);
				break;
			case 8:
				_owner.addMaxHp(50);
				_owner.getAC().addAc(-3);
				_owner.addHitupByArmor(4);
				_owner.addCriMagic(4);
				break;
			default:
				if (itemlvl > 8) {
					_owner.addMaxHp(50);
					_owner.getAC().addAc(-3);
					_owner.addHitupByArmor(4);
					_owner.addCriMagic(4);
				}
				break;
			}
		}
		if (itemId == 9303) {
			switch (itemlvl) {
			case 0:
				_owner.addMaxHp(5);
				break;
			case 1:
				_owner.addMaxHp(10);
				break;
			case 2:
				_owner.addMaxHp(15);
				break;
			case 3:
				_owner.getAC().addAc(-1);
				_owner.addMaxHp(20);
				break;
			case 4:
				_owner.addMaxHp(25);
				_owner.getAC().addAc(-2);
				break;
			case 5:
				_owner.addMaxHp(30);
				_owner.getAC().addAc(-3);
				_owner.addDamageReductionByArmor(1);
				break;
			case 6:
				_owner.addMaxHp(35);
				_owner.getAC().addAc(-5);
				_owner.addDamageReductionByArmor(2);
				_owner.getResistance().addMr(3);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 7:
				_owner.addMaxHp(40);
				_owner.getAC().addAc(-6);
				_owner.addDamageReductionByArmor(3);
				_owner.getResistance().addMr(5);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 8:
				_owner.addMaxHp(50);
				_owner.getAC().addAc(-7);
				_owner.addDamageReductionByArmor(4);
				_owner.getResistance().addMr(7);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			default:
				if (itemlvl > 8) {
					_owner.addMaxHp(50);
					_owner.getAC().addAc(-7);
					_owner.addDamageReductionByArmor(4);
					_owner.getResistance().addMr(7);
					_owner.sendPackets(new S_SPMR(_owner));
				}
				break;
			}
		}
		if (itemId == 10101) {
			if (itemlvl > 6) {
				_owner.getAbility().addSp(itemlvl - 6);
				_owner.sendPackets(new S_SPMR(_owner));
			}
		}
		if (itemId == 10102 || itemId == 500221) {
			if (itemlvl > 4) {
				_owner.addHitup(itemlvl - 4);
			}
		}
		if (itemId == 10103 || itemId == 500222) {
			if (itemlvl > 4) {
				_owner.addBowHitup(itemlvl - 4);
			}
		}
		if (itemId == 500223) {
			if (itemlvl > 4) {
				_owner.addSuccMagic(itemlvl - 4);
			}
		}

		if (itemId == 22110) {
			switch (itemlvl) {
			case 5:
				_owner.addHitupByArmor(1);
				break;
			case 6:
				_owner.addHitupByArmor(2);
				break;
			case 7:
				_owner.addHitupByArmor(3);
				_owner.addDmgupByArmor(1);
				break;
			case 8:
				_owner.addHitupByArmor(4);
				_owner.addDmgupByArmor(2);
				break;
			case 9:
				_owner.addHitupByArmor(5);
				_owner.addDmgupByArmor(3);
				break;
			case 10:
				_owner.addHitupByArmor(6);
				_owner.addDmgupByArmor(4);
				break;
			}
		}
		if (itemId == 22111) {
			switch (itemlvl) {
			case 5:
				_owner.addBowHitupByArmor(1);
				break;
			case 6:
				_owner.addBowHitupByArmor(2);
				break;
			case 7:
				_owner.addBowHitupByArmor(3);
				_owner.addBowDmgupByArmor(1);
				break;
			case 8:
				_owner.addBowHitupByArmor(4);
				_owner.addBowDmgupByArmor(2);
				break;
			case 9:
				_owner.addBowHitupByArmor(5);
				_owner.addBowDmgupByArmor(3);
				break;
			case 10:
				_owner.addBowHitupByArmor(6);
				_owner.addBowDmgupByArmor(4);
				break;
			}
		}
		if (itemId == 22112) {
			switch (itemlvl) {
			case 5:
				_owner.addSuccMagic(1);
				break;
			case 6:
				_owner.addSuccMagic(2);
				break;
			case 7:
				_owner.addSuccMagic(3);
				_owner.getAbility().addSp(1);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 8:
				_owner.addSuccMagic(4);
				_owner.getAbility().addSp(2);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 9:
				_owner.addSuccMagic(5);
				_owner.getAbility().addSp(3);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 10:
				_owner.addSuccMagic(6);
				_owner.getAbility().addSp(4);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			}
		}
		if (itemId == 22113) {
			switch (itemlvl) {
			case 5:
				/*
				 * _owner.getResistance().addMr(2); _owner.sendPackets(new S_SPMR(_owner));
				 */
				break;
			case 6:
				/*
				 * _owner.getResistance().addMr(3); _owner.sendPackets(new S_SPMR(_owner));
				 */
				break;
			case 7:
				_owner.addDamageReductionByArmor(1);
				/*
				 * _owner.getResistance().addMr(4); _owner.sendPackets(new S_SPMR(_owner));
				 */
				break;
			case 8:
				_owner.addDamageReductionByArmor(2);
				/*
				 * _owner.getResistance().addMr(5); _owner.sendPackets(new S_SPMR(_owner));
				 */
				break;
			case 9:
				_owner.addDamageReductionByArmor(3);
				/*
				 * _owner.getResistance().addMr(6); _owner.sendPackets(new S_SPMR(_owner));
				 */
				break;
			case 10:
				_owner.addDamageReductionByArmor(4);
				/*
				 * _owner.getResistance().addMr(7); _owner.sendPackets(new S_SPMR(_owner));
				 */
				break;
			}
		}
		if (itemId == 30914) {
			switch (itemlvl) {
			case 5:
			case 6:
				_owner.addSuccMagic(1);
				break;
			case 7:
				_owner.addSuccMagic(2);
				break;
			case 8:
				_owner.addSuccMagic(3);
				break;
			case 9:
				_owner.addSuccMagic(4);
				break;
			case 10:
				_owner.addSuccMagic(5);
				break;
			default:
				break;
			}
		}
		if (itemId == 30218 || itemId == 21259 || itemId == 21265 || itemId == 21266) {
			switch (itemlvl) {
			case 0:
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
				break;
			case 7:
				_owner.addMaxHp(20);
				break;
			case 8:
				_owner.addMaxHp(40);
				break;
			case 9:
			case 10:
			case 11:
			case 12:
				_owner.addDamageReductionByArmor(1);
				_owner.addMaxHp(60);
				break;
			default:
				break;
			}
		}

		if (itemId == 9125 && itemlvl >= 9) {
			_owner.addDmgupByArmor(1);
		}
		if (itemId == 9126 && itemlvl >= 9) {
			_owner.addBowDmgupByArmor(1);
		}
		if (itemId == 9127 && itemlvl >= 9) {
			_owner.getAbility().addSp(1);
			_owner.sendPackets(new S_SPMR(_owner));
		}
		if (itemId == 9306) {
			_owner.addSuccMagic(1);
		}
		if (itemId == 4810) {
			_owner.addSuccMagic(2);
		}
		if (itemId == 30912 || itemId == 30195 || itemId == 30199) {
			switch (itemlvl) {
			case 5:
				_owner.addHitupByArmor(1);
			case 6:
				_owner.addDmgupByArmor(1);
				_owner.addHitupByArmor(1);
				break;
			case 7:
				_owner.addDmgupByArmor(2);
				_owner.addHitupByArmor(2);
				break;
			case 8:
				_owner.addDmgupByArmor(3);
				_owner.addHitupByArmor(3);
				break;
			case 9:
				_owner.addDmgupByArmor(4);
				_owner.addHitupByArmor(4);
				break;
			case 10:
				_owner.addDmgupByArmor(5);
				_owner.addHitupByArmor(5);
				break;
			default:
				break;
			}
		}
		if (itemId == 30913 || itemId == 30196 || itemId == 30200) {
			switch (itemlvl) {
			case 5:
				_owner.addBowHitupByArmor(1);
			case 6:
				_owner.addBowDmgupByArmor(1);
				_owner.addBowHitupByArmor(1);
				break;
			case 7:
				_owner.addBowDmgupByArmor(2);
				_owner.addBowHitupByArmor(2);
				break;
			case 8:
				_owner.addBowDmgupByArmor(3);
				_owner.addBowHitupByArmor(3);
				break;
			case 9:
				_owner.addBowDmgupByArmor(4);
				_owner.addBowHitupByArmor(4);
				break;
			case 10:
				_owner.addBowDmgupByArmor(5);
				_owner.addBowHitupByArmor(5);
				break;
			default:
				break;
			}
		}
		if (itemId == 30914 || itemId == 30197 || itemId == 30201) {
			switch (itemlvl) {
			case 6:
				_owner.getAbility().addSp(1);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 7:
				_owner.getAbility().addSp(2);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 8:
				_owner.getAbility().addSp(3);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 9:
				_owner.getAbility().addSp(4);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 10:
				_owner.getAbility().addSp(5);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			default:
				break;
			}
		}
		if (itemId == 9202 || itemId == 9203) {
			switch (itemlvl) {
			case 5:
				_owner.getResistance().addMr(4);
				break;
			case 6:
				_owner.getResistance().addMr(8);
				break;
			case 7:
				_owner.getResistance().addMr(12);
				break;
			case 8:
				_owner.getResistance().addMr(16);
				break;
			case 9:
			case 10:
			case 11:
			case 12:
				_owner.getResistance().addMr(20);
				break;
			default:
				break;
			}
		}
		if (itemId == 9113) {
			int reduc = 0;
			if (itemlvl >= 6) {
				reduc = itemlvl - 5;
				_owner.addDamageReductionByArmor(reduc);
			}
		}
		if (itemId == 9114 || itemId == 91140) {
			switch (itemlvl) {
			case 5:
				_owner.getResistance().addMr(4);
				break;
			case 6:
				_owner.getResistance().addMr(5);
				break;
			case 7:
				_owner.getResistance().addMr(6);
				break;
			case 8:
				_owner.getResistance().addMr(8);
				_owner.addDamageReductionByArmor(1);
				break;
			case 9:
				_owner.getResistance().addMr(11);
				_owner.addDamageReductionByArmor(2);
				break;
			case 10:
			case 11:
			case 12:
				_owner.addPvPDmgByArmor(1);
				_owner.addPvPReductionByArmor(1);
				_owner.getResistance().addMr(14);
				_owner.addMaxHp(100);
				_owner.addDamageReductionByArmor(2);
				break;
			default:
				break;
			}
		}

		if (itemId == 9115) {
			switch (itemlvl) {
			case 7:
				_owner.addDmgupByArmor(1);
				break;
			case 8:
				_owner.addDmgupByArmor(1);
				_owner.addHitupByArmor(2);
				break;
			case 9:
				_owner.addDmgupByArmor(2);
				_owner.addHitupByArmor(4);
				break;
			case 10:
			case 11:
			case 12:
				_owner.addPvPDmgByArmor(1);
				_owner.addPvPReductionByArmor(1);
				_owner.addMaxHp(100);
				_owner.addHitupByArmor(6);
				_owner.addDmgupByArmor(2);
				break;
			default:
				break;
			}
		}
		if (itemId == 91150) {
			switch (itemlvl) {
			case 7:
				_owner.addHitupByArmor(1);
				_owner.addDmgupByArmor(1);
				break;
			case 8:
				_owner.addHitupByArmor(3);
				_owner.addDmgupByArmor(1);
				break;
			case 9:
				_owner.addDmgupByArmor(2);
				_owner.addHitupByArmor(5);
				break;
			case 10:
			case 11:
			case 12:
				_owner.addPvPDmgByArmor(1);
				_owner.addPvPReductionByArmor(1);
				_owner.addMaxHp(100);
				_owner.addHitupByArmor(7);
				_owner.addDmgupByArmor(2);
				break;
			default:
				break;
			}
		}

		if (itemId == 9116) {
			switch (itemlvl) {
			case 7:
				_owner.addBowDmgupByArmor(1);
				break;
			case 8:
				_owner.addBowDmgupByArmor(1);
				_owner.addBowHitupByArmor(2);
				break;
			case 9:
				_owner.addBowDmgupByArmor(2);
				_owner.addBowHitupByArmor(4);
				break;
			case 10:
			case 11:
			case 12:
				_owner.addBowHitupByArmor(6);
				_owner.addPvPDmgByArmor(1);
				_owner.addPvPReductionByArmor(1);
				_owner.addMaxHp(100);
				_owner.addBowDmgupByArmor(2);
				break;
			default:
				break;
			}
		}
		if (itemId == 91160) {
			switch (itemlvl) {
			case 7:
				_owner.addBowDmgupByArmor(1);
				_owner.addBowHitupByArmor(1);
				break;
			case 8:
				_owner.addBowDmgupByArmor(1);
				_owner.addBowHitupByArmor(3);
				break;
			case 9:
				_owner.addBowDmgupByArmor(2);
				_owner.addBowHitupByArmor(5);
				break;
			case 10:
			case 11:
			case 12:
				_owner.addBowHitupByArmor(7);
				_owner.addPvPDmgByArmor(1);
				_owner.addPvPReductionByArmor(1);
				_owner.addMaxHp(100);
				_owner.addBowDmgupByArmor(2);
				break;
			default:
				break;
			}
		}
		if (itemId == 9117) {
			switch (itemlvl) {
			case 8:
				_owner.getAbility().addSp(1);
				_owner.addSuccMagic(1);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 9:
				_owner.getAbility().addSp(1);
				_owner.addSuccMagic(3);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 10:
			case 11:
			case 12:
				_owner.addPvPDmgByArmor(1);
				_owner.addPvPReductionByArmor(1);
				_owner.addMaxHp(100);
				_owner.getAbility().addSp(2);
				_owner.addSuccMagic(4);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			default:
				break;
			}
		}
		if (itemId == 91170) {
			switch (itemlvl) {
			case 7:
				_owner.getAbility().addSp(1);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 8:
				_owner.getAbility().addSp(1);
				_owner.addSuccMagic(2);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 9:
				_owner.getAbility().addSp(2);
				_owner.addSuccMagic(4);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 10:
			case 11:
			case 12:
				_owner.addPvPDmgByArmor(1);
				_owner.addPvPReductionByArmor(1);
				_owner.addMaxHp(100);
				_owner.getAbility().addSp(2);
				_owner.addSuccMagic(5);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			default:
				break;
			}
		}
		if (itemId == 130220 && armor.getEnchantLevel() >= 7) {
			_owner.addHitupByArmor(armor.getEnchantLevel() - 3);
			_owner.addBowHitupByArmor(item.getBowHitup());
		} else if (itemId == 9200 && armor.getEnchantLevel() >= 5) {
			_owner.addHitupByArmor(armor.getEnchantLevel() - 4);
			_owner.addBowHitupByArmor(item.getBowHitup());
		} else if (itemId == 9201 && armor.getEnchantLevel() >= 5) {
			_owner.addHitupByArmor(item.getHitup());
			_owner.addBowHitupByArmor(armor.getEnchantLevel() - 4);
		} else {
			_owner.addHitupByArmor(item.getHitup());
			_owner.addBowHitupByArmor(item.getBowHitup());
		}

		_owner.addDmgupByArmor(item.getDmgup());
		_owner.addBowDmgupByArmor(item.getBowDmgup());
		_owner.getResistance().addEarth(item.get_defense_earth());
		_owner.getResistance().addWind(item.get_defense_wind());
		_owner.getResistance().addWater(item.get_defense_water());
		_owner.getResistance().addFire(item.get_defense_fire());

		_armors.add(armor);

		for (L1ArmorSet armorSet : L1ArmorSet.getAllSet()) {
			if (armorSet.isPartOfSet(itemId) && armorSet.isValid(_owner)) {
				if (armor.getItem().getType2() == 2
						&& (armor.getItem().getType() == 9 || armor.getItem().getType() == 11)) {
					if (!armorSet.isEquippedRingOfArmorSet(_owner)) {
						armorSet.giveEffect(_owner);
						_currentArmorSet.add(armorSet);
						if (item.getMainId() != 0) {
							L1ItemInstance main = _owner.getInventory().findItemId(item.getMainId());
							if (main != null) {
								if (main.isEquipped())
									_owner.sendPackets(new S_ItemStatus(main, _owner, true, true));
							}
						}
						if (item.getMainId2() != 0) {
							L1ItemInstance main = _owner.getInventory().findItemId(item.getMainId2());
							if (main != null) {
								if (main.isEquipped())
									_owner.sendPackets(new S_ItemStatus(main, _owner, true, true));
							}
						}
						if (item.getMainId3() != 0) {
							L1ItemInstance main = _owner.getInventory().findItemId(item.getMainId3());
							if (main != null) {
								if (main.isEquipped())
									_owner.sendPackets(new S_ItemStatus(main, _owner, true, true));
							}
						}

					}
				} else {
					armorSet.giveEffect(_owner);
					_currentArmorSet.add(armorSet);
					if (item.getMainId() != 0) {
						L1ItemInstance main = _owner.getInventory().findItemId(item.getMainId());
						if (main != null) {
							if (main.isEquipped())
								_owner.sendPackets(new S_ItemStatus(main, _owner, true, true));
						}
					}
					if (item.getMainId2() != 0) {
						L1ItemInstance main = _owner.getInventory().findItemId(item.getMainId2());
						if (main != null) {
							if (main.isEquipped())
								_owner.sendPackets(new S_ItemStatus(main, _owner, true, true));
						}
					}
					if (item.getMainId3() != 0) {
						L1ItemInstance main = _owner.getInventory().findItemId(item.getMainId3());
						if (main != null) {
							if (main.isEquipped())
								_owner.sendPackets(new S_ItemStatus(main, _owner, true, true));
						}
					}
				}
			}
		}
		if (itemId >= 490000 && itemId <= 490017) {
			_owner.getResistance().addFire(RegistLevel * 2);
			_owner.getResistance().addWind(RegistLevel * 2);
			_owner.getResistance().addEarth(RegistLevel * 2);
			_owner.getResistance().addWater(RegistLevel * 2);
		}

		if (itemId == 420100 || itemId == 420100 || itemId == 420100 || itemId == 420100) {
			if (itemlvl == 7) {
				_owner.addDamageReductionByArmor(1);
			}
			if (itemlvl == 8) {
				_owner.addDamageReductionByArmor(2);
			}
			if (itemlvl == 9) {
				_owner.addDamageReductionByArmor(3);
			}
		}
		/** ���� ���� ��þ�� ��Ÿ �ο� **/
		if (itemId == 420000) {
			if (itemlvl == 5 || itemlvl == 6) {
				_owner.addBowDmgup(1);
			}
			if (itemlvl == 7 || itemlvl == 8) {
				_owner.addBowDmgup(2);
			}

			if (itemlvl >= 9) {
				_owner.addBowDmgup(3);
			}
		}
		if (itemId == 420003) {
			if (itemlvl == 5 || itemlvl == 6) {
				_owner.addDmgup(1);
			}
			if (itemlvl == 7 || itemlvl == 8) {
				_owner.addDmgup(2);
			}
			if (itemlvl >= 9) {
				_owner.addDmgup(3);
			}
		}

		if (armor.getItemId() == 20107 || armor.getItemId() == 120107) {
			if (armor.getEnchantLevel() >= 3) {
				_owner.getAbility().addSp((armor.getEnchantLevel() - 2));
				_owner.sendPackets(new S_SPMR(_owner));
			}
		}

		if (RegistLevel == 10) {// �ǵ��� ���ɹ���
			_owner.getResistance().addFire(10);
			_owner.getResistance().addWind(10);
			_owner.getResistance().addEarth(10);
			_owner.getResistance().addWater(10);
		} else if (RegistLevel == 11) {// �ǵ��� ��������
			_owner.addMaxMp(30);
		} else if (RegistLevel == 12) {// �ǵ��� ü�¹���
			_owner.addMaxHp(50);
		} else if (RegistLevel == 13) {// �ǵ��� �긶����
			_owner.getResistance().addMr(10);
		} else if (RegistLevel == 14) {// �ǵ��� ��ö����
			_owner.getAC().addAc(-1);
		} else if (RegistLevel == 15) {// �ǵ��� ȸ������
			_owner.addHpr(1);
			_owner.addMpr(1);
		}

		if (itemId == 423014) {
			_owner.startAHRegeneration();
		}
		if (itemId == 423015) {
			_owner.startSHRegeneration();
		}
		if (itemId == 20380) {
			_owner.startHalloweenRegeneration();
		}

		if (armor.getAttrEnchantLevel() > 0) {
			switch (armor.getAttrEnchantLevel()) {
			case 21:
			case 22:
			case 23:
				_owner.addDamageReductionByArmor(armor.getAttrEnchantLevel() - 20);
				break;
			case 24:
			case 25:
			case 26:
				_owner.addMpr(armor.getAttrEnchantLevel() - 23);
				break;
			case 27:
			case 28:
			case 29:
				_owner.getResistance().addMr(armor.getAttrEnchantLevel() - 26);
				break;
			case 30:
			case 31:
			case 32:
				_owner.addDmgup(armor.getAttrEnchantLevel() - 29);
				break;
			default:
				break;
			}
		}

		if (itemId == 20077 || itemId == 20062 || itemId == 120077) {
			if (!_owner.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.INVISIBILITY)) {
				for (L1DollInstance doll : _owner.getDollList()) {
					doll.deleteDoll();
					_owner.sendPackets(new S_SkillIconGFX(56, 0));
					_owner.sendPackets(new S_OwnCharStatus(_owner));
				}
				_owner.getSkillEffectTimerSet().killSkillEffectTimer(L1SkillId.BLIND_HIDING);
				_owner.getSkillEffectTimerSet().setSkillEffect(L1SkillId.INVISIBILITY, 0);
				_owner.sendPackets(new S_Invis(_owner.getId(), 1));
				Broadcaster.broadcastPacket(_owner, new S_Invis(_owner.getId(), 1));
				if (_owner.isInParty()) {
					for (L1PcInstance tar : L1World.getInstance().getVisiblePlayer(_owner, -1)) {
						if (_owner.getParty().isMember(tar)) {
							tar.sendPackets(new S_WorldPutObject(_owner));
						}
					}
				}

				// S_RemoveObject sremove = new S_RemoveObject(_owner.getId());

				for (L1PcInstance pc2 : L1World.getInstance().getVisiblePlayer(_owner)) {
					// pc2.sendPackets(sremove);
					if (pc2.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.STATUS_FLOATING_EYE)
							&& pc2.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.CURSE_BLIND)) {
						pc2.sendPackets(new S_WorldPutObject(_owner));
					}
				}

			}
		}

		if (itemId == 20288 || itemId == 21288) {
			_owner.sendPackets(new S_Ability(1, true));
		}
		if (itemId == 20281 || itemId == 120281) {
			_owner.sendPackets(new S_Ability(2, true));
		}
		if (itemId == 21281) {
			_owner.sendPackets(new S_Ability(2, true));
			_owner.sendPackets(new S_Ability(7, true));
		}
		if (itemId == 20284 || itemId == 120284) {
			_owner.sendPackets(new S_Ability(5, true));
		}
		if (itemId == 20036) {
			_owner.sendPackets(new S_Ability(3, true));
		}
		if (itemId == 20207) {
			_owner.sendPackets(new S_SkillIconBlessOfEva(_owner.getId(), -1));
		}
		if (itemId == 21097) {// �������� ����
			switch (itemlvl) {
			case 5:
			case 6:
				_owner.getAbility().addSp(1);
				break;
			case 7:
			case 8:
				_owner.getAbility().addSp(2);
				break;
			default:
				if (itemlvl >= 9)
					_owner.getAbility().addSp(3);
				break;
			}
		}
		if (itemId == 21095) { // ü���� ����
			switch (itemlvl) {
			case 5:
			case 6:
				_owner.addMaxHp(25);
				break;
			case 7:
			case 8:
				_owner.addMaxHp(50);
				break;
			default:
				if (itemlvl >= 9)
					_owner.addMaxHp(75);
				break;
			}
		}
		if (itemId == 21096) {// ��ȣ�� ����
			switch (itemlvl) {
			case 5:
			case 6:
				_owner.addDamageReductionByArmor(1);
				break;
			case 7:
			case 8:
				_owner.addDamageReductionByArmor(2);
				break;
			default:
				if (itemlvl >= 9)
					_owner.addDamageReductionByArmor(3);
				break;
			}
		}
		if (itemtype >= 8 && itemtype <= 12) {
			if (itemlvl > 0) {
				if (itemgrade != 3) {
					if (itemtype == 8) {// �����
						switch (itemlvl) {
						case 1:
							_owner.addMaxHp(5);
							break;
						case 2:
							_owner.addMaxHp(10);
							break;
						case 3:
							_owner.addMaxHp(20);
							break;
						case 4:
							_owner.addMaxHp(30);
							break;
						case 5:
							_owner.addMaxHp(40);
							_owner.addPotionPlus(1);
							_owner.getAC().addAc(-1);
							_owner.getResistance().addMr(1);
							break;
						case 6:
							_owner.addMaxHp(40);
							_owner.addPotionPlus(2);
							_owner.getAC().addAc(-2);
							_owner.getResistance().addMr(3);
							break;
						case 7:
							_owner.addMaxHp(50);
							_owner.addPotionPlus(3);
							_owner.getAC().addAc(-3);
							_owner.getResistance().addMr(5);
							break;
						case 8:
							_owner.addMaxHp(50);
							_owner.addPotionPlus(4);
							_owner.getAC().addAc(-4);
							_owner.getResistance().addMr(7);
							break;
						case 9:
							_owner.addMaxHp(60);
							_owner.addPotionPlus(6);
							_owner.getAC().addAc(-5);
							_owner.getResistance().addMr(10);
							break;
						default:
						}
					} else if (itemtype == 12) {// �Ͱ���
						switch (itemlvl) {
						case 1:
							_owner.addMaxHp(5);
							break;
						case 2:
							_owner.addMaxHp(10);
							break;
						case 3:
							_owner.addMaxHp(20);
							break;
						case 4:
							_owner.addMaxHp(30);
							break;
						case 5:
							_owner.addMaxHp(40);
							_owner.addPotionPlus(1);
							_owner.getAC().addAc(-1);
							break;
						case 6:
							_owner.addMaxHp(40);
							_owner.addPotionPlus(2);
							_owner.getAC().addAc(-2);
							break;
						case 7:
							_owner.addMaxHp(50);
							_owner.addPotionPlus(3);
							_owner.getAC().addAc(-3);
							break;
						case 8:
							_owner.addMaxHp(50);
							_owner.addPotionPlus(4);
							_owner.getAC().addAc(-4);
							break;
						case 9:
							_owner.addMaxHp(60);
							_owner.addPotionPlus(6);
							_owner.getAC().addAc(-5);
							break;
						default:
						}
					} else if (itemtype == 9 || itemtype == 11) {// ����
						switch (itemlvl) {
						case 1:
							_owner.addMaxHp(5);
							break;
						case 2:
							_owner.addMaxHp(10);
							break;
						case 3:
							_owner.addMaxHp(20);
							break;
						case 4:
							_owner.addMaxHp(30);
							break;
						case 5:
							_owner.addMaxHp(40);
							_owner.addDmgupByArmor(1);
							_owner.addBowDmgupByArmor(1);
							break;
						case 6:
							_owner.addMaxHp(40);
							_owner.addDmgupByArmor(2);
							_owner.addBowDmgupByArmor(2);
							_owner.getResistance().addMr(1);
							_owner.addPvPDmgByArmor(1);
							break;
						case 7:
							_owner.getAbility().addSp(1);
							_owner.addMaxHp(50);
							_owner.addDmgupByArmor(3);
							_owner.addBowDmgupByArmor(3);
							_owner.getResistance().addMr(3);
							_owner.addPvPDmgByArmor(2);
							break;
						case 8:
							_owner.getAbility().addSp(2);
							_owner.addMaxHp(50);
							_owner.addDmgupByArmor(4);
							_owner.addBowDmgupByArmor(4);
							_owner.getResistance().addMr(5);
							_owner.addPvPDmgByArmor(3);
							break;
						case 9:
							_owner.getAbility().addSp(3);
							_owner.addMaxHp(60);
							_owner.addDmgupByArmor(5);
							_owner.addBowDmgupByArmor(5);
							_owner.getResistance().addMr(7);
							_owner.addPvPDmgByArmor(5);
							break;
						case 10:
							_owner.getAbility().addSp(4);
							_owner.addMaxHp(70);
							_owner.addDmgupByArmor(6);
							_owner.addBowDmgupByArmor(6);
							_owner.getResistance().addMr(9);
							_owner.addPvPDmgByArmor(7);
							break;
						case 11:
							_owner.getAbility().addSp(5);
							_owner.addMaxHp(80);
							_owner.addDmgupByArmor(7);
							_owner.addBowDmgupByArmor(7);
							_owner.getResistance().addMr(11);
							_owner.addPvPDmgByArmor(9);
							break;
						case 12:
							_owner.getAbility().addSp(6);
							_owner.addMaxHp(100);
							_owner.addDmgupByArmor(8);
							_owner.addBowDmgupByArmor(8);
							_owner.getResistance().addMr(13);
							_owner.addPvPDmgByArmor(11);
							break;
						default:
						}
					} else if (itemtype == 10) {// ��Ʈ
						switch (itemlvl) {
						case 1:
							_owner.addMaxMp(5);
							break;
						case 2:
							_owner.addMaxMp(10);
							break;
						case 3:
							_owner.addMaxMp(20);
							break;
						case 4:
							_owner.addMaxMp(30);
							break;
						case 5:
							_owner.addMaxMp(40);
							_owner.addDamageReductionByArmor(1);
							break;
						case 6:
							_owner.addMaxMp(40);
							_owner.addDamageReductionByArmor(2);
							_owner.addMaxHp(20);
							_owner.addPvPReductionByArmor(1);
							break;
						case 7:
							_owner.addMaxHp(30);
							_owner.addMaxMp(50);
							_owner.addDamageReductionByArmor(3);
							_owner.addPvPReductionByArmor(3);
							break;
						case 8:
							_owner.addMaxHp(40);
							_owner.addMaxMp(50);
							_owner.addDamageReductionByArmor(4);
							_owner.addPvPReductionByArmor(5);
							break;
						case 9:
							_owner.addMaxHp(50);
							_owner.addMaxMp(60);
							_owner.addDamageReductionByArmor(5);
							_owner.addPvPReductionByArmor(7);
							break;
						default:
						}
					}

				} else if (itemId == 500007) { // ��Ƽ���� �������Ͱ���
					switch (itemlvl) {
					case 1:
						_owner.addMaxHp(20);
						break;
					case 2:
						_owner.addMaxHp(30);
						break;
					case 3:
						_owner.addMaxHp(40);
						_owner.addDamageReductionByArmor(1);
						break;
					case 4:
						_owner.addMaxHp(50);
						_owner.addDamageReductionByArmor(1);
						break;
					case 5:
						_owner.addMaxHp(60);
						_owner.addDamageReductionByArmor(2);
						break;
					case 6:
						_owner.getAC().addAc(-7);
						_owner.addMaxHp(70);
						_owner.addDamageReductionByArmor(3);
						break;
					case 7:
						_owner.getAC().addAc(-8);
						_owner.addMaxHp(80);
						_owner.addDamageReductionByArmor(4);
						_owner.addDmgupByArmor(1);
						_owner.addBowDmgupByArmor(1);
						break;
					case 8:
						_owner.getAC().addAc(-9);
						_owner.addMaxHp(90);
						_owner.addDamageReductionByArmor(5);
						_owner.addDmgupByArmor(3);
						_owner.addBowDmgupByArmor(3);
						break;
					default:
					}
				} else if (itemId == 502007) { // ��Ƽ���� �������Ͱ���
					switch (itemlvl) {
					case 3:
						_owner.addMaxHp(50);
						_owner.addDamageReductionByArmor(1);
						break;
					case 4:
						_owner.addMaxHp(60);
						_owner.addDamageReductionByArmor(2);
						break;
					case 5:
						_owner.getAC().addAc(-7);
						_owner.addMaxHp(70);
						_owner.addDamageReductionByArmor(3);
						break;
					case 6:
						_owner.getAC().addAc(-8);
						_owner.addMaxHp(80);
						_owner.addDamageReductionByArmor(4);
						_owner.addDmgupByArmor(1);
						_owner.addBowDmgupByArmor(1);
						break;
					case 7:
						_owner.getAC().addAc(-9);
						_owner.addMaxHp(90);
						_owner.addDamageReductionByArmor(5);
						_owner.addDmgupByArmor(3);
						_owner.addBowDmgupByArmor(3);
						break;
					case 8:
						_owner.getAC().addAc(-10);
						_owner.addMaxHp(140);
						_owner.addDamageReductionByArmor(6);
						_owner.addDmgupByArmor(5);
						_owner.addBowDmgupByArmor(5);
						break;
					default:
					}
				} else if (itemId == 500008) { // ��Ƽ���� Ǫ�����Ͱ���
					switch (itemlvl) {
					case 1:
					case 2:
					case 3:
					case 4:
						break;
					case 5:
						_owner.getAC().addAc(-1);
						break;
					case 6:
						_owner.getAC().addAc(-2);
						break;
					case 7:
						_owner.getAC().addAc(-2);
						break;
					case 8:
						_owner.getAC().addAc(-3);
						break;
					default:
					}
				} else if (itemId == 502008) { // ��Ƽ���� Ǫ�����Ͱ���
					switch (itemlvl) {
					case 4:
						_owner.getAC().addAc(-1);
						break;
					case 5:
						_owner.getAC().addAc(-2);
						break;
					case 6:
						_owner.getAC().addAc(-2);
						break;
					case 7:
						_owner.getAC().addAc(-3);
						break;
					case 8:
						_owner.getAC().addAc(-4);
						break;
					default:
					}
				} else if (itemId == 500009) { // ��Ƽ���� �������Ͱ���
					switch (itemlvl) {
					case 1:
						_owner.addMaxMp(10);
						break;
					case 2:
						_owner.addMaxMp(15);
						break;
					case 3:
						_owner.addMaxMp(30);
						_owner.getAbility().addSp(1);
						break;
					case 4:
						_owner.addMaxMp(35);
						_owner.getAbility().addSp(1);
						break;
					case 5:
						_owner.addMaxMp(50);
						_owner.getAbility().addSp(2);
						break;
					case 6:
						_owner.getAC().addAc(-1);
						_owner.addMaxMp(55);
						_owner.getAbility().addSp(2);
						break;
					case 7:
						_owner.getAC().addAc(-2);
						_owner.addMaxMp(70);
						_owner.getAbility().addSp(3);
						_owner.addSuccMagic(1);
						break;
					case 8:
						_owner.getAC().addAc(-3);
						_owner.addMaxMp(95);
						_owner.getAbility().addSp(3);
						_owner.addSuccMagic(3);
						break;
					default:
					}
				} else if (itemId == 502009) { // ��Ƽ���� �������Ͱ���
					switch (itemlvl) {
					case 3:
						_owner.addMaxMp(35);
						_owner.getAbility().addSp(1);
						break;
					case 4:
						_owner.addMaxMp(50);
						_owner.getAbility().addSp(2);
						break;
					case 5:
						_owner.getAC().addAc(-1);
						_owner.addMaxMp(55);
						_owner.getAbility().addSp(2);
						break;
					case 6:
						_owner.getAC().addAc(-2);
						_owner.addMaxMp(70);
						_owner.getAbility().addSp(3);
						_owner.addSuccMagic(1);
						break;
					case 7:
						_owner.getAC().addAc(-3);
						_owner.addMaxMp(95);
						_owner.getAbility().addSp(3);
						_owner.addSuccMagic(3);
						break;
					case 8:
						_owner.getAC().addAc(-4);
						_owner.addMaxMp(130);
						_owner.getAbility().addSp(4);
						_owner.addSuccMagic(5);
						break;
					default:
					}
				} else if ((itemId >= 425109 && itemId <= 425113) || (itemId >= 525109 && itemId <= 525113)
						|| (itemId >= 625109 && itemId <= 625113)) {
					if (itemlvl > 0) {
						_owner.addMaxHp(((itemlvl * 5) + 10));
					}
					switch (itemlvl) {
					case 2:
					case 3:
					case 4:
						_owner.getAC().addAc(-(itemlvl - 1));
						break;
					case 5:
					case 6:
					case 7:
					case 8:
						_owner.getAC().addAc(-4);
						_owner.addDmgup(itemlvl - 4);
						_owner.addBowDmgup(itemlvl - 4);
						break;
					}

				} else if (itemId == 21247 || itemId == 21251) { // ������ ��������
					if (itemlvl > 0) {
						_owner.addMaxHp(((itemlvl * 5) + 10));
					}
				} else if (itemId == 21248 || itemId == 21252) { // ������ ü��
					if (itemlvl > 0) {
						_owner.addMaxHp(((itemlvl * 5) + 10));
						if (itemId == 21252 && itemlvl >= 6) {
							_owner.addMaxHp((itemlvl - 5) * 5);
						}
					}
				} else if (itemId == 21246 || itemId == 21250) { // ������ ����
					if (itemlvl > 0) {
						if (itemId == 21250) {
							if (itemlvl == 1) {
								_owner.addMaxHp(5);
							} else if (itemlvl == 2) {
								_owner.addMaxHp(10);
							} else if (itemlvl == 3) {
								_owner.addMaxHp(20);
							} else if (itemlvl == 4) {
								_owner.addMaxHp(25);
								_owner.getAbility().addSp(1);
							} else if (itemlvl == 5) {
								_owner.addMaxHp(30);
								_owner.getAbility().addSp(2);
							} else if (itemlvl == 6) {
								_owner.addMaxHp(35);
								_owner.getAbility().addSp(3);
								_owner.addSuccMagic(1);
							} else if (itemlvl == 7) {
								_owner.addMaxHp(40);
								_owner.addMaxMp(15);
								_owner.getAbility().addSp(4);
								_owner.addSuccMagic(2);
							} else if (itemlvl == 8) {
								_owner.addMaxHp(50);
								_owner.addMaxMp(20);
								_owner.getAbility().addSp(5);
								_owner.addSuccMagic(3);
							}
						} else {
							_owner.addMaxHp(itemlvl * 5);
							if (itemlvl == 5) {
								_owner.getAbility().addSp(1);
							} else if (itemlvl == 6) {
								_owner.getAbility().addSp(2);
							} else if (itemlvl == 7) {
								_owner.getAbility().addSp(3);
								_owner.addSuccMagic(1);
							} else if (itemlvl == 8) {
								_owner.addMaxMp(15);
								_owner.getAbility().addSp(4);
								_owner.addSuccMagic(2);
							}
						}
					}
				} else if (itemId == 21249 || itemId == 21253) { // ������ ���
					if (itemlvl >= 3) {
						_owner.addMaxHp((itemlvl - 2) * 5);
					}
					switch (itemlvl) {
					case 0:
						break;
					case 1:
					case 2:
					case 3:
					case 4:
						_owner.getAC().addAc(-itemlvl);
						break;
					case 5:
					case 6:
						_owner.getAC().addAc(-4);
						break;
					case 7:
					case 8:
						_owner.getAC().addAc(-4);
						if (itemId == 21253)
							_owner.getAC().addAc(-1);
						break;
					}
				}

				if (itemId == 21249) {
					switch (itemlvl) {
					case 5:
					case 6:
					case 7:
					case 8:
						_owner.addHitup((itemlvl - 4));
						_owner.addBowHitup(-(itemlvl - 4));
						break;
					}
				}
				if (itemId == 21253) {
					switch (itemlvl) {
					case 4:
					case 5:
					case 6:
					case 7:
					case 8:
						_owner.addHitup((itemlvl - 3));
						_owner.addBowHitup((itemlvl - 3));
						break;
					}
				}

				if (itemId == 21247) {
					switch (itemlvl) {
					case 7:
						_owner.addȮ������ȸ��(1);
						break;
					case 8:
						_owner.addȮ������ȸ��(3);
						break;
					}
				}
				if (itemId == 21251) {
					switch (itemlvl) {
					case 6:
						_owner.addȮ������ȸ��(1);
						break;
					case 7:
						_owner.addȮ������ȸ��(3);
						break;
					case 8:
						_owner.addȮ������ȸ��(5);
						break;
					}
				}
				if (itemId == 21248) { // �����۹���
					switch (itemlvl) {
					case 7:
						_owner.addDamageReductionByArmor(1);
						break;
					case 8:
						_owner.addDamageReductionByArmor(2);
						break;
					}
				}
				if (itemId == 21252) { // �����۹���
					switch (itemlvl) {
					case 6:
						_owner.addDamageReductionByArmor(1);
						break;
					case 7:
						_owner.addDamageReductionByArmor(2);
						break;
					case 8:
						_owner.addDamageReductionByArmor(3);
						break;
					}
				}
				if (itemId >= 21247 && itemId <= 21249) { // �����۹���
					switch (itemlvl) {
					case 5:
					case 6:
					case 7:
					case 8:
						_owner.addDmgup((itemlvl - 4));
						_owner.addBowDmgup((itemlvl - 4));
						break;
					}
				}
				if (itemId >= 21251 && itemId <= 21253) { // �����۹���
					switch (itemlvl) {
					case 4:
					case 5:
					case 6:
					case 7:
					case 8:
						_owner.addDmgup((itemlvl - 3));
						_owner.addBowDmgup((itemlvl - 3));
						break;
					}
				}

				if (itemId == 21246) { // ������ ���� ����
					switch (itemlvl) {
					case 0:
					case 1:
						break;
					case 2:
					case 3:
					case 4:
						_owner.getAC().addAc(-(itemlvl - 1));
						break;
					case 5:
						_owner.getAC().addAc(-3);
						break;
					case 6:
					case 7:
						_owner.getAC().addAc(-4);
						break;
					case 8:
						_owner.getAC().addAc(-5);
						break;
					}
				}
				if (itemId == 21247) { // ������ ���� ����
					switch (itemlvl) {
					case 0:
					case 1:
						break;
					case 2:
					case 3:
					case 4:
						_owner.getAC().addAc(-(itemlvl - 1));
						break;
					case 5:
					case 6:
					case 7:
					case 8:
						_owner.getAC().addAc(-4);
						break;
					}
				}
				if (itemId == 21248) { // ������ ü�� ����
					switch (itemlvl) {
					case 0:
					case 1:
						break;
					case 2:
					case 3:
					case 4:
						_owner.getAC().addAc(-(itemlvl - 1));
						break;
					case 5:
					case 6:
					case 7:
					case 8:
						_owner.getAC().addAc(-4);
						break;
					}
				}
				if (itemId == 21250) { // �����۹���
					switch (itemlvl) {
					case 0:
					case 1:
						break;
					case 2:
					case 3:
					case 4:
						_owner.getAC().addAc(-(itemlvl - 1));
						break;
					case 5:
					case 6:
						_owner.getAC().addAc(-4);
						break;
					case 7:
					case 8:
						_owner.getAC().addAc(-4);
						break;
					}
				}
				if (itemId == 21251) { // �� ������ ���� ����
					switch (itemlvl) {
					case 0:
					case 1:
						break;
					case 2:
					case 3:
					case 4:
						_owner.getAC().addAc(-(itemlvl - 1));
						break;
					case 5:
					case 6:
						_owner.getAC().addAc(-4);
						break;
					case 7:
					case 8:
						_owner.getAC().addAc(-5);
						break;
					}
				}
				if (itemId == 21252) { // �� ������ ü�� ����
					switch (itemlvl) {
					case 0:
					case 1:
						break;
					case 2:
					case 3:
					case 4:
						_owner.getAC().addAc(-(itemlvl - 1));
						break;
					case 5:
					case 6:
						_owner.getAC().addAc(-4);
						break;
					case 7:
					case 8:
						_owner.getAC().addAc(-5);
						break;
					}
				}

				if (itemId == 500010 || itemId == 502010) {
					int ac = itemlvl;
					if (item.getBless() == 0 && itemlvl >= 3) {
						ac += 1;
					}
					_owner.getAC().addAc(-ac);
					int dm = itemlvl - 2;
					if (item.getBless() != 0 && itemlvl >= 4)
						dm -= 1;
					_owner.addDmgup(dm);
					_owner.addBowDmgup(dm);
				}
			}
		}
		armor.startEquipmentTimer(_owner);
		if (armor.getSkill() != null && armor.getSkill().getSkillId() != 0) {
			/*
			 * switch (armor.getSkill().getSkillId()) { case L1SkillId.BLESSED_ARMOR:
			 * _owner.sendPackets(new S_PacketBox(S_PacketBox.SKILL_WEAPON_ICON, 748,
			 * armor.getSkill().getTime(), false, false)); break; default: break; }
			 */
		}
	}

	public ArrayList<L1ItemInstance> getArmors() {
		return _armors;
	}

	public void removeWeapon(L1ItemInstance weapon, boolean doubleweapon) {
		if (doubleweapon) {
			_owner.setSecondWeapon(null);
			_owner.setCurrentWeapon(_owner.getWeapon().getItem().getType1());
			_owner.getWeapon()._isSecond = false;
			weapon._isSecond = false;
		} else {
			_owner.setWeapon(null);
			if (_owner.getSecondWeapon() != null) {
				_owner.setWeapon(_owner.getSecondWeapon());
				_owner.sendPackets(new S_SabuBox(S_SabuBox.�������������԰���, _owner.getSecondWeapon().getId(), 8, false));
				_owner.sendPackets(new S_SabuBox(S_SabuBox.�������������԰���, _owner.getSecondWeapon().getId(), 9, true));
				_owner.setSecondWeapon(null);
				_owner.setCurrentWeapon(_owner.getWeapon().getItem().getType1());
			} else {
				_owner.setCurrentWeapon(0);
			}
		}

		weapon.stopEquipmentTimer();
		_weapon = null;
		int itemId = weapon.getItem().getItemId();

		int itemlvl = weapon.getEnchantLevel();

		/*
		 * if (weapon.getItem().getType() == 7 || weapon.getItem().getType() == 16 ||
		 * weapon.getItem().getType() == 17) { if (weapon.getBless() == 0 ||
		 * weapon.getBless() == 128) { _owner.getAbility().addSp(-1);
		 * _owner.sendPackets(new S_SPMR(_owner)); } }
		 */

		if (itemId == 124 || itemId == 121 || itemId == 100124) {
			switch (itemlvl) {
			case 7:
				_owner.getAbility().addSp(-1);
				break;
			case 8:
				_owner.getAbility().addSp(-2);
				break;
			default:
				if (itemlvl >= 9) {
					_owner.getAbility().addSp(-3);
				}
				break;
			}
			_owner.sendPackets(new S_SPMR(_owner));
		}

		if (itemId >= 11011 && itemId <= 11013) {
			L1PolyMorph.undoPoly(_owner);
		} // �߰�
		if (itemId == 7236) {
			L1PolyMorph.undoPoly(_owner);
		}
		// ����
		if (itemId == 61 || itemId == 12 || itemId == 86 || itemId == 134 || itemId == 9100 || itemId == 9101
				|| itemId == 9103) {
			_owner.addDmgup(-(itemlvl * 2));
		}

		if (weapon.getItemId() == 61 || itemId == 9101 || itemId == 9103) {
			_owner.add�ٰŸ�ġ����(-(weapon.getEnchantLevel() + 1));
		}

		if (weapon.getItemId() == 134) {
			_owner.addSuccMagic(-(weapon.getEnchantLevel() + 2));
		}

		if (weapon.getItemId() == 9102) {
			_owner.addCriMagic(-(weapon.getEnchantLevel() + 1));
		}

		if (weapon.getItemId() == 66) { // �彽
			_owner.addDmgup(-itemlvl * 2);
		}
		// ����
		if (weapon.getItemId() == 59) {
			_owner.addHitup(-3);
		}
		if (weapon.getItemId() == 9100) {
			_owner.addDamageReductionByArmor(-2);
			_owner.add���Ÿ�ġ����(-(weapon.getEnchantLevel() + 1));
		}
		if (weapon.getItemId() == 293 || weapon.getItemId() == 189 || weapon.getItemId() == 100189) {
			_owner.add���Ÿ�ġ����(-3);
		}
		if (weapon.getItemId() == 30220) {
			_owner.add���Ÿ�ġ����(-2);
		}
		if (weapon.getItemId() == 413105) {
			_owner.add���Ÿ�ġ����(-1);
		}
		if (weapon.getItemId() == 6101) {
			switch (weapon.getEnchantLevel()) {
			case 0:
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
				_owner.addCriMagic(-1);
				break;
			case 7:
				_owner.addCriMagic(-2);
				break;
			case 8:
				_owner.addCriMagic(-3);
				break;
			case 9:
				_owner.addCriMagic(-4);
				break;
			default:
				if (weapon.getEnchantLevel() >= 10) {
					_owner.addCriMagic(-5);
				}
				break;
			}
		}
		if (weapon.getItemId() == 134 || weapon.getItemId() == 9102) {
			_owner.getAbility().addSp(-(weapon.getEnchantLevel()));
			_owner.sendPackets(new S_SPMR(_owner));
		}
		if (weapon.getItem().getType() == 17 && weapon.getItem().getType2() == 1 && weapon.getStepEnchantLevel() != 0) {
			_owner.getAbility().addAddedInt(-(weapon.getEnchantLevel()));
			_owner.sendPackets(new S_OwnCharStatus(_owner));
		}
		if (weapon.getItemId() == 261) { // ������ ������
			_owner.addMpr(-weapon.getEnchantLevel());
		}

		// ������ ��æ�� ����
		// ���������� sp ȸ���κ�

		if (_owner.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.COUNTER_BARRIER)) {
			_owner.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.COUNTER_BARRIER);
			_owner.sendPackets(new S_PacketBox(S_PacketBox.ICON_AURA, 90, 0, 0));// ī�� �����

		} else if (_owner.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.���丣��)) {
			_owner.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.���丣��);
			_owner.sendPackets(new S_PacketBox(S_PacketBox.ICON_AURA, 135, 0, 0));// ���丣�� �����

		} else if (_owner.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.FIRE_BLESS)) {
			_owner.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.FIRE_BLESS);

		} else if (_owner.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.���彺��)) {
			_owner.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.���彺��);
		} else if (_owner.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.�㸮����)) {
			_owner.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.�㸮����);
		} else if (_owner.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.��Ŀ�����̺�)) {
			_owner.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.��Ŀ�����̺�);
		} else if (_owner.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.��Ŀ�����̺�2��)) {
			_owner.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.��Ŀ�����̺�2��);
		} else if (_owner.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.��Ŀ�����̺�3��)) {
			_owner.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.��Ŀ�����̺�3��);
		}

		if (weapon.getItem().getType2() == 1 && weapon.getItem().getType() != 7 && weapon.getItem().getType() != 17
				&& weapon.getStepEnchantLevel() != 0) {
			_owner.addDmgup(-weapon.getStepEnchantLevel());
		}

		/** �ູ �ֹ��� 3 �ܰ� �ý��� by K **/
		int BlessLevel = weapon.getBlessEnchantLevel() * 2;
		if (weapon.getBlessEnchantLevel() > 0) {
			if (weapon.getBless() == 0 || weapon.getBless() == 128) {
				switch (weapon.getItem().getType()) {
				case 7:
				case 16:
				case 17:
					_owner.getAbility().addSp(-(1 + BlessLevel));
					_owner.addSuccMagic(-(1 + BlessLevel));
					_owner.sendPackets(new S_SPMR(_owner));
					break;
				case 4:
				case 13:
					_owner.addBowDmgup(-(1 + BlessLevel));
					_owner.addBowHitup(-(1 + BlessLevel));
					break;
				case 1:
				case 2:
				case 3:
				case 5:
				case 6:
				case 9:
				case 10:
				case 11:
				case 12:
				case 14:
				case 15:
				case 18:
					_owner.addDmgup(-(1 + BlessLevel));
					_owner.addHitup(-(1 + BlessLevel));
					break;
				}
			}
		}
		/** ������� **/
		if (weapon.get_durability() > 0)
			_owner.sendPackets(new S_PacketBox(S_PacketBox.����ջ󸶿콺, 0), true);

		/*
		 * if(_owner.getSkillEffectTimerSet().hasSkillEffect(L1SkillId. BLESS_WEAPON )){
		 * _owner.sendPackets(new S_PacketBox(S_PacketBox.SKILL_WEAPON_ICON, 2176, 0,
		 * false, false)); }
		 */

		/*
		 * if(_owner.getWeapon() != null){ _owner.sendPackets(new
		 * S_PacketBox(S_PacketBox.SKILL_WEAPON_ICON, 747, 0, false, false));
		 * _owner.sendPackets(new S_PacketBox(S_PacketBox.SKILL_WEAPON_ICON, 747, 0,
		 * true, false)); if(_owner.getWeapon().getSkill() != null &&
		 * _owner.getWeapon().getSkill().getSkillId() == L1SkillId.ENCHANT_WEAPON){
		 * _owner.sendPackets(new S_PacketBox(S_PacketBox.SKILL_WEAPON_ICON, 747,
		 * _owner.getWeapon().getSkill().getTime(), false, false)); } }else{
		 * _owner.sendPackets(new S_PacketBox(S_PacketBox.SKILL_WEAPON_ICON, 747, 0,
		 * false, false)); }
		 */
		try {
			if (weapon.getSkill() != null && weapon.getSkill().getSkillId() != 0) {
				/*
				 * switch (weapon.getSkill().getSkillId()) { case L1SkillId.SHADOW_FANG:
				 * _owner.sendPackets(new S_PacketBox(S_PacketBox.SKILL_WEAPON_ICON, 2951, 0,
				 * false, false)); break; case L1SkillId.ENCHANT_WEAPON: if (_owner.getWeapon()
				 * != null) { _owner.sendPackets(new S_PacketBox(S_PacketBox.SKILL_WEAPON_ICON,
				 * 747, 0, false, false)); _owner.sendPackets(new
				 * S_PacketBox(S_PacketBox.SKILL_WEAPON_ICON, 747, 0, true, false)); if
				 * (_owner.getWeapon().getSkill() != null &&
				 * _owner.getWeapon().getSkill().getSkillId() == L1SkillId.ENCHANT_WEAPON) {
				 * _owner.sendPackets(new S_PacketBox(S_PacketBox.SKILL_WEAPON_ICON, 747,
				 * _owner.getWeapon().getSkill().getTime(), false, false)); } } else {
				 * _owner.sendPackets(new S_PacketBox(S_PacketBox.SKILL_WEAPON_ICON, 747, 0,
				 * false, false)); } break; case L1SkillId.BLESS_WEAPON: if (_owner.getWeapon()
				 * != null) { _owner.sendPackets(new S_PacketBox(S_PacketBox.SKILL_WEAPON_ICON,
				 * 2176, 0, false, false)); _owner.sendPackets(new
				 * S_PacketBox(S_PacketBox.SKILL_WEAPON_ICON, 2176, 0, true, false)); if
				 * (_owner.getWeapon().getSkill() != null &&
				 * _owner.getWeapon().getSkill().getSkillId() == L1SkillId.BLESS_WEAPON) {
				 * _owner.sendPackets(new S_PacketBox(S_PacketBox.SKILL_WEAPON_ICON, 2176,
				 * _owner.getWeapon().getSkill().getTime(), false, false)); } } else {
				 * _owner.sendPackets(new S_PacketBox(S_PacketBox.SKILL_WEAPON_ICON, 2176, 0,
				 * false, false)); } break;
				 */

				/*
				 * if(_owner.getWeapon() != null){ _owner.sendPackets(new
				 * S_PacketBox(S_PacketBox.SKILL_WEAPON_ICON, 2176, 0 , false, false));
				 * if(_owner.getWeapon().getSkill() != null &&
				 * _owner.getWeapon().getSkill().getSkillId() == L1SkillId.BLESS_WEAPON){
				 * _owner.sendPackets(new S_PacketBox(S_PacketBox.SKILL_WEAPON_ICON, 2176,
				 * _owner.getWeapon().getSkill().getTime() , false, false)); } }else{
				 * _owner.sendPackets(new S_PacketBox(S_PacketBox.SKILL_WEAPON_ICON, 2176, 0 ,
				 * false, false)); } break;
				 */
				/*
				 * case L1SkillId.HOLY_WEAPON: if (_owner.getWeapon() != null) {
				 * _owner.sendPackets(new S_PacketBox(S_PacketBox.SKILL_WEAPON_ICON, 2165, 0,
				 * false, false)); if (_owner.getWeapon().getSkill() != null &&
				 * _owner.getWeapon().getSkill().getSkillId() == L1SkillId.HOLY_WEAPON) {
				 * _owner.sendPackets(new S_PacketBox(S_PacketBox.SKILL_WEAPON_ICON, 2165,
				 * _owner.getWeapon().getSkill().getTime(), false, false)); } } else {
				 * _owner.sendPackets(new S_PacketBox(S_PacketBox.SKILL_WEAPON_ICON, 2165, 0,
				 * false, false)); } break;
				 * 
				 * default: break; }
				 */
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void removeArmor(L1ItemInstance armor) {
		L1Item item = armor.getItem();
		int itemId = armor.getItem().getItemId();
		int itemlvl = armor.getEnchantLevel();
		int itemtype = armor.getItem().getType();
		int itemgrade = armor.getItem().getGrade();
		int RegistLevel = armor.getRegistLevel();

		if (itemtype == 2) {
			_owner.getAC()
					.addAc(-(item.get_ac() - armor.getEnchantLevel() - armor.getAcByMagic() + armor.get_durability()));
		} else if (itemtype >= 8 && itemtype <= 12) {
			_owner.getAC().addAc(-(item.get_ac() - armor.getAcByMagic()));
		} else if (itemtype == 18 || itemtype == 16) {
			_owner.getAC().addAc(-(item.get_ac() - armor.getAcByMagic()));
		} else {
			if (itemId == 20016 || itemId == 20294 || itemId == 120016 || itemId == 220294) {
				_owner.getAC().addAc(-item.get_ac());
				_owner.getAC().addAc(-armor.getEnchantLevel());
				_owner.getAC().addAc(-armor.getAcByMagic());
			} else {
				_owner.getAC().addAc(-(item.get_ac() - armor.getEnchantLevel() - armor.getAcByMagic()));
			}
		}
		if (itemId == 420104 || itemId == 420105 || itemId == 420106 || itemId == 420107) {
			_owner.stopPapuBlessing();
		}
		if (itemId >= 420108 && itemId <= 420111) {
			_owner.stopLindBlessing();
		}
		if (itemId == 9112) {
			_owner.addMaxHp(-(itemlvl * 5));
		}

		if (itemId == 20050) {
			if (itemlvl == 5) {
				_owner.Add_Er(-1);
			} else if (itemlvl == 6) {
				_owner.Add_Er(-2);
			} else if (itemlvl == 7) {
				_owner.Add_Er(-3);
			} else if (itemlvl == 8) {
				_owner.Add_Er(-4);
			} else if (itemlvl >= 9) {
				_owner.Add_Er(-5);
			}
		}

		/** �ູ �ֹ��� 3 �ܰ� �ý��� by K **/
		if (armor.getBlessEnchantLevel() != 0) {
			if (!(armor.getItem().getType() >= 8 && armor.getItem().getType() <= 12)) {
				_owner.addDamageReductionByArmor(-armor.getBlessEnchantLevel());
			}
		}
		/** ������� **/

		if (itemId == 9900 && itemId == 9974) {
			_owner.addPVPMagicDamageReduction(-1);
		}
		if (itemId == 9800 && itemId == 9874) {
			_owner.addPVPMagicDamageReduction(-2);
		}
		if (itemId == 9700 && itemId == 9774) {
			_owner.addPVPMagicDamageReduction(-3);
		}
		if (itemId == 20049||itemId == 20079||itemId == 420104||itemId == 420105||itemId == 420106||itemId == 20107) {  // By.�ں� 25��þ �ǿ���,�����۾�
			switch (itemlvl) {
			case 25:
				
				_owner.addMaxHp(-100);
				_owner.addMaxMp(-50);
				_owner.addDamageReductionByArmor(-2);
				break;
		
			}
		}
		if (itemId == 9501) { // ��ȣ�� ��������
			switch (itemlvl) {
			case 5:
				_owner.getAC().addAc(3);
				_owner.addMaxHp(-30);
				_owner.addDamageReductionByArmor(-1);
				_owner.addDmgupByArmor(-1);
				break;
			case 6:
				_owner.getAC().addAc(5);
				_owner.addMaxHp(-35);
				_owner.addDamageReductionByArmor(-2);
				_owner.getResistance().addMr(-3);
				_owner.addDmgupByArmor(-2);
				_owner.addHitupByArmor(-1);
				_owner.add�ٰŸ�ġ����(-1);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 7:
				_owner.getAC().addAc(6);
				_owner.addMaxHp(-40);
				_owner.addDamageReductionByArmor(-3);
				_owner.getResistance().addMr(-5);
				_owner.addDmgupByArmor(-3);
				_owner.addHitupByArmor(-3);
				_owner.add�ٰŸ�ġ����(-3);
				_owner.addPvPReductionByArmor(-1);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 8:
				_owner.getAC().addAc(7);
				_owner.addMaxHp(-50);
				_owner.addDamageReductionByArmor(-4);
				_owner.getResistance().addMr(-7);
				_owner.addDmgupByArmor(-4);
				_owner.addHitupByArmor(-5);
				_owner.add�ٰŸ�ġ����(-5);
				_owner.addPvPReductionByArmor(-2);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			}
		}
		if (itemId == 9502) { // ��ȣ�� ��������
			switch (itemlvl) {
			case 5:
				_owner.getAC().addAc(3);
				_owner.addMaxHp(-30);
				_owner.addDamageReductionByArmor(-1);
				_owner.addDmgupByArmor(-1);
				break;
			case 6:
				_owner.getAC().addAc(5);
				_owner.addMaxHp(-35);
				_owner.addDamageReductionByArmor(-2);
				_owner.getResistance().addMr(-3);
				_owner.addBowDmgupByArmor(-2);
				_owner.addBowHitupByArmor(-1);
				_owner.add���Ÿ�ġ����(-1);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 7:
				_owner.getAC().addAc(6);
				_owner.addMaxHp(-40);
				_owner.addDamageReductionByArmor(-3);
				_owner.getResistance().addMr(-5);
				_owner.addBowDmgupByArmor(-3);
				_owner.addBowHitupByArmor(-3);
				_owner.add���Ÿ�ġ����(-3);
				_owner.addPvPReductionByArmor(-1);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 8:
				_owner.getAC().addAc(7);
				_owner.addMaxHp(-50);
				_owner.addDamageReductionByArmor(-4);
				_owner.getResistance().addMr(-7);
				_owner.addBowDmgupByArmor(-4);
				_owner.addBowHitupByArmor(-5);
				_owner.add���Ÿ�ġ����(-5);
				_owner.addPvPReductionByArmor(-2);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			}
		}
		if (itemId == 9503) { // ��ȣ�� ��������
			switch (itemlvl) {
			case 5:
				_owner.getAC().addAc(3);
				_owner.addMaxHp(-30);
				_owner.addDamageReductionByArmor(-1);
				_owner.addDmgupByArmor(-1);
				break;
			case 6:
				_owner.getAC().addAc(5);
				_owner.addMaxHp(-35);
				_owner.addDamageReductionByArmor(-2);
				_owner.getResistance().addMr(-3);
				_owner.addSuccMagic(-2);
				_owner.addHitupByArmor(-2);
				_owner.addCriMagic(-1);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 7:
				_owner.getAC().addAc(6);
				_owner.addMaxHp(-40);
				_owner.addDamageReductionByArmor(-3);
				_owner.getResistance().addMr(-5);
				_owner.addSuccMagic(-3);
				_owner.addHitupByArmor(-3);
				_owner.addCriMagic(-2);
				_owner.addPvPReductionByArmor(-1);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 8:
				_owner.getAC().addAc(7);
				_owner.addMaxHp(-50);
				_owner.addDamageReductionByArmor(-4);
				_owner.getResistance().addMr(-7);
				_owner.addSuccMagic(-5);
				_owner.addHitupByArmor(-2);
				_owner.addCriMagic(-4);
				_owner.addPvPReductionByArmor(-2);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			}
		}
		if (itemId == 31910) {
			switch (itemlvl) {
			case 4:
				_owner.addHitupByArmor(-1);
				break;
			case 5:
				_owner.addDmgupByArmor(-1);
				_owner.addHitupByArmor(-1);
				break;
			case 6:
				_owner.addDmgupByArmor(-2);
				_owner.addHitupByArmor(-2);
				break;
			case 7:
				_owner.addDmgupByArmor(-3);
				_owner.addHitupByArmor(-3);
				break;
			case 8:
				_owner.addDmgupByArmor(-4);
				_owner.addHitupByArmor(-4);
				break;
			default:
				break;
			}
		}
		if (itemId == 31911) {
			switch (itemlvl) {
			case 4:
				_owner.addBowHitupByArmor(-1);
				break;
			case 5:
				_owner.addBowDmgupByArmor(-1);
				_owner.addBowHitupByArmor(-1);
				break;
			case 6:
				_owner.addBowDmgupByArmor(-2);
				_owner.addBowHitupByArmor(-2);
				break;
			case 7:
				_owner.addBowDmgupByArmor(-3);
				_owner.addBowHitupByArmor(-3);
				break;
			case 8:
				_owner.addBowDmgupByArmor(-4);
				_owner.addBowHitupByArmor(-4);
				break;
			default:
				break;
			}
		}
		if (itemId == 31912) {
			switch (itemlvl) {
			case 4:
				_owner.addSuccMagic(-1);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 5:
				_owner.getAbility().addSp(-1);
				_owner.addSuccMagic(-1);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 6:
				_owner.getAbility().addSp(-2);
				_owner.addSuccMagic(-2);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 7:
				_owner.getAbility().addSp(-3);
				_owner.addSuccMagic(-3);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 8:
				_owner.getAbility().addSp(-4);
				_owner.addSuccMagic(-4);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			default:
				break;
			}
		}

		if (itemId == 31913) {
			switch (itemlvl) {
			case 4:
				_owner.getResistance().addMr(-1);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 5:
				_owner.getResistance().addMr(-2);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 6:
				_owner.getResistance().addMr(-3);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 7:
				_owner.getResistance().addMr(-4);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 8:
				_owner.getResistance().addMr(-5);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			default:
				break;
			}
		}
		if (itemId == 32910) {
			switch (itemlvl) {
			case 5:
				_owner.addDmgupByArmor(-1);
				_owner.addHitupByArmor(-2);
				_owner.getResistance().addMr(-4);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 6:
				_owner.addDmgupByArmor(-2);
				_owner.addHitupByArmor(-3);
				_owner.getResistance().addMr(-6);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 7:
				_owner.addDmgupByArmor(-3);
				_owner.addHitupByArmor(-4);
				_owner.getResistance().addMr(-8);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 8:
				_owner.addDmgupByArmor(-4);
				_owner.addHitupByArmor(-5);
				_owner.getResistance().addMr(-10);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			default:
				break;
			}
		}
		if (itemId == 32911) {
			switch (itemlvl) {
			case 5:
				_owner.addBowDmgupByArmor(-1);
				_owner.addBowHitupByArmor(-2);
				_owner.getResistance().addMr(-4);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 6:
				_owner.addBowDmgupByArmor(-2);
				_owner.addBowHitupByArmor(-3);
				_owner.getResistance().addMr(-6);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 7:
				_owner.addBowDmgupByArmor(-3);
				_owner.addBowHitupByArmor(-4);
				_owner.getResistance().addMr(-8);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 8:
				_owner.addBowDmgupByArmor(-4);
				_owner.addBowHitupByArmor(-5);
				_owner.getResistance().addMr(-10);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			default:
				break;
			}
		}
		if (itemId == 32912) {
			switch (itemlvl) {
			case 5:
				_owner.getAbility().addSp(-1);
				_owner.addSuccMagic(-2);
				_owner.getResistance().addMr(-4);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 6:
				_owner.getAbility().addSp(-2);
				_owner.addSuccMagic(-3);
				_owner.getResistance().addMr(-6);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 7:
				_owner.getAbility().addSp(-3);
				_owner.addSuccMagic(-4);
				_owner.getResistance().addMr(-8);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 8:
				_owner.getAbility().addSp(-4);
				_owner.addSuccMagic(-5);
				_owner.getResistance().addMr(-10);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			default:
				break;
			}
		}
		if (itemId == 9300) {
			switch (itemlvl) {
			case 0:
				_owner.addMaxHp(-5);
				break;
			case 1:
				_owner.addMaxHp(-10);
				break;
			case 2:
				_owner.addMaxHp(-15);
				break;
			case 3:
				_owner.addMaxHp(-20);
				break;
			case 4:
				_owner.addMaxHp(-25);
				_owner.getAC().addAc(1);
				break;
			case 5:
				_owner.addMaxHp(-30);
				_owner.getAC().addAc(2);
				_owner.addDmgupByArmor(-1);
				break;
			case 6:
				_owner.addMaxHp(-35);
				_owner.getAC().addAc(3);
				_owner.addDmgupByArmor(-2);
				_owner.add�ٰŸ�ġ����(-1);
				break;
			case 7:
				_owner.addMaxHp(-40);
				_owner.getAC().addAc(3);
				_owner.addDmgupByArmor(-3);
				_owner.add�ٰŸ�ġ����(-3);
				break;
			case 8:
				_owner.addMaxHp(-50);
				_owner.getAC().addAc(3);
				_owner.addDmgupByArmor(-4);
				_owner.add�ٰŸ�ġ����(-4);
				break;
			default:
				if (itemlvl > 8) {
					_owner.addMaxHp(-50);
					_owner.getAC().addAc(3);
					_owner.addDmgupByArmor(-4);
					_owner.add�ٰŸ�ġ����(-4);
				}
				break;
			}
		}
		if (itemId == 9301) {
			switch (itemlvl) {
			case 0:
				_owner.addMaxHp(-5);
				break;
			case 1:
				_owner.addMaxHp(-10);
				break;
			case 2:
				_owner.addMaxHp(-15);
				break;
			case 3:
				_owner.addMaxHp(-20);
				break;
			case 4:
				_owner.addMaxHp(-25);
				_owner.getAC().addAc(1);
				break;
			case 5:
				_owner.addMaxHp(-30);
				_owner.getAC().addAc(2);
				_owner.addBowDmgupByArmor(-1);
				break;
			case 6:
				_owner.addMaxHp(-35);
				_owner.getAC().addAc(3);
				_owner.addBowDmgupByArmor(-2);
				_owner.add���Ÿ�ġ����(-1);
				break;
			case 7:
				_owner.addMaxHp(-40);
				_owner.getAC().addAc(3);
				_owner.addBowDmgupByArmor(-3);
				_owner.add���Ÿ�ġ����(-3);
				break;
			case 8:
				_owner.addMaxHp(-50);
				_owner.getAC().addAc(3);
				_owner.addBowDmgupByArmor(-4);
				_owner.add���Ÿ�ġ����(-5);
				break;
			default:
				if (itemlvl > 8) {
					_owner.addMaxHp(-50);
					_owner.getAC().addAc(3);
					_owner.addBowDmgupByArmor(-4);
					_owner.add���Ÿ�ġ����(-5);
				}
				break;
			}
		}
		if (itemId == 9302) {
			switch (itemlvl) {
			case 0:
				_owner.addMaxHp(-5);
				break;
			case 1:
				_owner.addMaxHp(-10);
				break;
			case 2:
				_owner.addMaxHp(-15);
				break;
			case 3:
				_owner.addMaxHp(-20);
				break;
			case 4:
				_owner.addMaxHp(-25);
				_owner.getAC().addAc(1);
				break;
			case 5:
				_owner.addMaxHp(-30);
				_owner.getAC().addAc(2);
				_owner.addHitupByArmor(-1);
				break;
			case 6:
				_owner.addMaxHp(-35);
				_owner.getAC().addAc(3);
				_owner.addHitupByArmor(-2);
				_owner.addCriMagic(-1);
				break;
			case 7:
				_owner.addMaxHp(-40);
				_owner.getAC().addAc(3);
				_owner.addHitupByArmor(-3);
				_owner.addCriMagic(-2);
				break;
			case 8:
				_owner.addMaxHp(-50);
				_owner.getAC().addAc(3);
				_owner.addHitupByArmor(-4);
				_owner.addCriMagic(-4);
				break;
			default:
				if (itemlvl > 8) {
					_owner.addMaxHp(-50);
					_owner.getAC().addAc(3);
					_owner.addHitupByArmor(-4);
					_owner.addCriMagic(-4);
				}
				break;
			}
		}
		if (itemId == 9303) {
			switch (itemlvl) {
			case 0:
				_owner.addMaxHp(-5);
				break;
			case 1:
				_owner.addMaxHp(-10);
				break;
			case 2:
				_owner.addMaxHp(-15);
				break;
			case 3:
				_owner.getAC().addAc(1);
				_owner.addMaxHp(-20);
				break;
			case 4:
				_owner.addMaxHp(-25);
				_owner.getAC().addAc(2);
				break;
			case 5:
				_owner.addMaxHp(-30);
				_owner.getAC().addAc(3);
				_owner.addDamageReductionByArmor(-1);
				break;
			case 6:
				_owner.addMaxHp(-35);
				_owner.getAC().addAc(5);
				_owner.addDamageReductionByArmor(-2);
				_owner.getResistance().addMr(-3);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 7:
				_owner.addMaxHp(-40);
				_owner.getAC().addAc(6);
				_owner.addDamageReductionByArmor(-3);
				_owner.getResistance().addMr(-5);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 8:
				_owner.addMaxHp(-50);
				_owner.getAC().addAc(7);
				_owner.addDamageReductionByArmor(-4);
				_owner.getResistance().addMr(-7);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			default:
				if (itemlvl > 8) {
					_owner.addMaxHp(-50);
					_owner.getAC().addAc(7);
					_owner.addDamageReductionByArmor(-4);
					_owner.getResistance().addMr(-7);
					_owner.sendPackets(new S_SPMR(_owner));
				}
				break;
			}
		}
		if (itemId == 9306) {
			_owner.addSuccMagic(-1);
		}
		if (itemId == 4810) {
			_owner.addSuccMagic(-2);
		}
		if (itemId == 421216) {
			_owner.addSuccMagic(-2);
		}
		if (itemId == 421219) {
			_owner.addSuccMagic(-6);
		}
		if (itemId == 431219) {
			_owner.addSuccMagic(-2);
		}
		if (itemId == 30912 || itemId == 30195 || itemId == 30199) {
			switch (itemlvl) {
			case 5:
				_owner.addHitupByArmor(-1);
			case 6:
				_owner.addDmgupByArmor(-1);
				_owner.addHitupByArmor(-1);
				break;
			case 7:
				_owner.addDmgupByArmor(-2);
				_owner.addHitupByArmor(-2);
				break;
			case 8:
				_owner.addDmgupByArmor(-3);
				_owner.addHitupByArmor(-3);
				break;
			case 9:
				_owner.addDmgupByArmor(-4);
				_owner.addHitupByArmor(-4);
				break;
			case 10:
				_owner.addDmgupByArmor(-5);
				_owner.addHitupByArmor(-5);
				break;
			default:
				break;
			}
		}
		if (itemId == 30913 || itemId == 30196 || itemId == 30200) {
			switch (itemlvl) {
			case 5:
				_owner.addBowHitupByArmor(-1);
			case 6:
				_owner.addBowDmgupByArmor(-1);
				_owner.addBowHitupByArmor(-1);
				break;
			case 7:
				_owner.addBowDmgupByArmor(-2);
				_owner.addBowHitupByArmor(-2);
				break;
			case 8:
				_owner.addBowDmgupByArmor(-3);
				_owner.addBowHitupByArmor(-3);
				break;
			case 9:
				_owner.addBowDmgupByArmor(-4);
				_owner.addBowHitupByArmor(-4);
				break;
			case 10:
				_owner.addBowDmgupByArmor(-5);
				_owner.addBowHitupByArmor(-5);
				break;
			default:
				break;
			}
		}
		if (itemId == 10101) {
			if (itemlvl > 6) {
				_owner.getAbility().addSp(-(itemlvl - 6));
				_owner.sendPackets(new S_SPMR(_owner));
			}
		}
		if (itemId == 10102 || itemId == 500221) {
			if (itemlvl > 4) {
				_owner.addHitup(-(itemlvl - 4));
			}
		}
		if (itemId == 10103 || itemId == 500222) {
			if (itemlvl > 4) {
				_owner.addBowHitup(-(itemlvl - 4));
			}
		}
		if (itemId == 500223) {
			if (itemlvl > 4) {
				_owner.addSuccMagic(-(itemlvl - 4));
			}
		}
		if (itemId == 22110) {
			switch (itemlvl) {
			case 5:
				_owner.addHitupByArmor(-1);
				break;
			case 6:
				_owner.addHitupByArmor(-2);
				break;
			case 7:
				_owner.addHitupByArmor(-3);
				_owner.addDmgupByArmor(-1);
				break;
			case 8:
				_owner.addHitupByArmor(-4);
				_owner.addDmgupByArmor(-2);
				break;
			case 9:
				_owner.addHitupByArmor(-5);
				_owner.addDmgupByArmor(-3);
				break;
			case 10:
				_owner.addHitupByArmor(-6);
				_owner.addDmgupByArmor(-4);
				break;
			}
		}
		if (itemId == 22111) {
			switch (itemlvl) {
			case 5:
				_owner.addBowHitupByArmor(-1);
				break;
			case 6:
				_owner.addBowHitupByArmor(-2);
				break;
			case 7:
				_owner.addBowHitupByArmor(-3);
				_owner.addBowDmgupByArmor(-1);
				break;
			case 8:
				_owner.addBowHitupByArmor(-4);
				_owner.addBowDmgupByArmor(-2);
				break;
			case 9:
				_owner.addBowHitupByArmor(-5);
				_owner.addBowDmgupByArmor(-3);
				break;
			case 10:
				_owner.addBowHitupByArmor(-6);
				_owner.addBowDmgupByArmor(-4);
				break;
			}
		}
		if (itemId == 22112) {
			switch (itemlvl) {
			case 5:
				_owner.addSuccMagic(-1);
				break;
			case 6:
				_owner.addSuccMagic(-2);
				break;
			case 7:
				_owner.addSuccMagic(-3);
				_owner.getAbility().addSp(-1);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 8:
				_owner.addSuccMagic(-4);
				_owner.getAbility().addSp(-2);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 9:
				_owner.addSuccMagic(-5);
				_owner.getAbility().addSp(-3);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 10:
				_owner.addSuccMagic(-6);
				_owner.getAbility().addSp(-4);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			}
		}
		if (itemId == 22113) {
			switch (itemlvl) {
			case 5:
				/*
				 * _owner.getResistance().addMr(-2); _owner.sendPackets(new S_SPMR(_owner));
				 */
				break;
			case 6:
				/*
				 * _owner.getResistance().addMr(-3); _owner.sendPackets(new S_SPMR(_owner));
				 */
				break;
			case 7:
				_owner.addDamageReductionByArmor(-1);
				/*
				 * _owner.getResistance().addMr(-4); _owner.sendPackets(new S_SPMR(_owner));
				 */
				break;
			case 8:
				_owner.addDamageReductionByArmor(-2);
				/*
				 * _owner.getResistance().addMr(-5); _owner.sendPackets(new S_SPMR(_owner));
				 */
				break;
			case 9:
				_owner.addDamageReductionByArmor(-3);
				/*
				 * _owner.getResistance().addMr(-6); _owner.sendPackets(new S_SPMR(_owner));
				 */
				break;
			case 10:
				_owner.addDamageReductionByArmor(-4);
				_owner.getResistance().addMr(-7);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			}
		}
		if (itemId == 30914 || itemId == 30197 || itemId == 30201) {
			switch (itemlvl) {
			case 6:
				_owner.getAbility().addSp(-1);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 7:
				_owner.getAbility().addSp(-2);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 8:
				_owner.getAbility().addSp(-3);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 9:
				_owner.getAbility().addSp(-4);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 10:
				_owner.getAbility().addSp(-5);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			default:
				break;
			}
		}
		if (itemId == 9208) {
			_owner.addSuccMagic(-2);
		}
		if (itemId == 20048) {
			_owner.addSuccMagic(-5);
		}
		if (itemId >= 9730 && itemId <= 9734) {
			_owner.addSuccMagic(-10);
		}
		if (itemId == 9206) {
			_owner.addSuccMagic(-1);
		}
		if (itemId == 30914) {
			switch (itemlvl) {
			case 5:
			case 6:
				_owner.addSuccMagic(-1);
				break;
			case 7:
				_owner.addSuccMagic(-2);
				break;
			case 8:
				_owner.addSuccMagic(-3);
				break;
			case 9:
				_owner.addSuccMagic(-4);
				break;
			case 10:
				_owner.addSuccMagic(-5);
				break;
			default:
				break;
			}
		}
		if (itemId == 30218 || itemId == 21259 || itemId == 21265 || itemId == 21266) {
			switch (itemlvl) {
			case 0:
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
				break;
			case 7:
				_owner.addMaxHp(-20);
				break;
			case 8:
				_owner.addMaxHp(-40);
				break;
			case 9:
			case 10:
			case 11:
			case 12:
				_owner.addDamageReductionByArmor(-1);
				_owner.addMaxHp(-60);
				break;
			default:
				break;
			}
		}
		if (itemId == 9113) {
			int reduc = 0;
			if (itemlvl >= 6) {
				reduc = itemlvl - 5;
				_owner.addDamageReductionByArmor(-reduc);
			}
		}
		if (itemId == 9202 || itemId == 9203) {
			switch (itemlvl) {
			case 5:
				_owner.getResistance().addMr(-4);
				break;
			case 6:
				_owner.getResistance().addMr(-8);
				break;
			case 7:
				_owner.getResistance().addMr(-12);
				break;
			case 8:
				_owner.getResistance().addMr(-16);
				break;
			case 9:
			case 10:
			case 11:
			case 12:
				_owner.getResistance().addMr(-20);
				break;
			default:
				break;
			}
		}
		if (itemId == 9114 || itemId == 91140) {
			switch (itemlvl) {
			case 5:
				_owner.getResistance().addMr(-4);
				break;
			case 6:
				_owner.getResistance().addMr(-5);
				break;
			case 7:
				_owner.getResistance().addMr(-6);
				break;
			case 8:
				_owner.getResistance().addMr(-8);
				_owner.addDamageReductionByArmor(-1);
				break;
			case 9:
				_owner.getResistance().addMr(-11);
				_owner.addDamageReductionByArmor(-2);
				break;
			case 10:
			case 11:
			case 12:
				_owner.addPvPDmgByArmor(1);
				_owner.addPvPReductionByArmor(1);
				_owner.getResistance().addMr(-14);
				_owner.addMaxHp(-100);
				_owner.addDamageReductionByArmor(-2);
				break;
			default:
				break;
			}
		}
		if (itemId == 9115) {
			switch (itemlvl) {
			case 7:
				_owner.addDmgupByArmor(-1);
				break;
			case 8:
				_owner.addDmgupByArmor(-1);
				_owner.addHitupByArmor(-2);
				break;
			case 9:
				_owner.addDmgupByArmor(-2);
				_owner.addHitupByArmor(-4);
				break;
			case 10:
			case 11:
			case 12:
				_owner.addPvPDmgByArmor(-1);
				_owner.addPvPReductionByArmor(-1);
				_owner.addMaxHp(-100);
				_owner.addHitupByArmor(-6);
				_owner.addDmgupByArmor(-2);
				break;
			default:
				break;
			}
		}
		if (itemId == 91150) {
			switch (itemlvl) {
			case 7:
				_owner.addDmgupByArmor(-1);
				_owner.addHitupByArmor(-1);
				break;
			case 8:
				_owner.addDmgupByArmor(-1);
				_owner.addHitupByArmor(-3);
				break;
			case 9:
				_owner.addDmgupByArmor(-2);
				_owner.addHitupByArmor(-5);
				break;
			case 10:
			case 11:
			case 12:
				_owner.addPvPDmgByArmor(-1);
				_owner.addPvPReductionByArmor(-1);
				_owner.addMaxHp(-100);
				_owner.addHitupByArmor(-7);
				_owner.addDmgupByArmor(-2);
				break;
			default:
				break;
			}
		}
		if (itemId == 9116) {
			switch (itemlvl) {
			case 7:
				_owner.addBowDmgupByArmor(1);
				break;
			case 8:
				_owner.addBowDmgupByArmor(1);
				_owner.addBowHitupByArmor(2);
				break;
			case 9:
				_owner.addBowDmgupByArmor(2);
				_owner.addBowHitupByArmor(4);
				break;
			case 10:
			case 11:
			case 12:
				_owner.addBowHitupByArmor(6);
				_owner.addPvPDmgByArmor(1);
				_owner.addPvPReductionByArmor(1);
				_owner.addMaxHp(100);
				_owner.addBowDmgupByArmor(2);
				break;
			default:
				break;
			}
		}
		if (itemId == 91160) {
			switch (itemlvl) {
			case 7:
				_owner.addBowDmgupByArmor(-1);
				_owner.addBowHitupByArmor(-1);
				break;
			case 8:
				_owner.addBowDmgupByArmor(-1);
				_owner.addBowHitupByArmor(-3);
				break;
			case 9:
				_owner.addBowDmgupByArmor(-2);
				_owner.addBowHitupByArmor(-5);
				break;
			case 10:
			case 11:
			case 12:
				_owner.addBowHitupByArmor(-7);
				_owner.addPvPDmgByArmor(-1);
				_owner.addPvPReductionByArmor(-1);
				_owner.addMaxHp(-100);
				_owner.addBowDmgupByArmor(-2);
				break;
			default:
				break;
			}
		}
		if (itemId == 9117) {
			switch (itemlvl) {
			case 8:
				_owner.getAbility().addSp(-1);
				_owner.addSuccMagic(-1);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 9:
				_owner.getAbility().addSp(-1);
				_owner.addSuccMagic(-3);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 10:
			case 11:
			case 12:
				_owner.addPvPDmgByArmor(-1);
				_owner.addPvPReductionByArmor(-1);
				_owner.addMaxHp(-100);
				_owner.getAbility().addSp(-2);
				_owner.addSuccMagic(-4);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			default:
				break;
			}
		}
		if (itemId == 91170) {
			switch (itemlvl) {
			case 7:
				_owner.getAbility().addSp(-1);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 8:
				_owner.getAbility().addSp(-1);
				_owner.addSuccMagic(-2);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 9:
				_owner.getAbility().addSp(-2);
				_owner.addSuccMagic(-4);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			case 10:
			case 11:
			case 12:
				_owner.addPvPDmgByArmor(-1);
				_owner.addPvPReductionByArmor(-1);
				_owner.addMaxHp(-100);
				_owner.getAbility().addSp(-2);
				_owner.addSuccMagic(-5);
				_owner.sendPackets(new S_SPMR(_owner));
				break;
			default:
				break;
			}
		}

		if (itemId == 9125 && itemlvl >= 9) {
			_owner.addDmgupByArmor(-1);
		}
		if (itemId == 9126 && itemlvl >= 9) {
			_owner.addBowDmgupByArmor(-1);
		}
		if (itemId == 9127 && itemlvl >= 9) {
			_owner.getAbility().addSp(-1);
			_owner.sendPackets(new S_SPMR(_owner));
		}
		if (itemId == 21255)
			_owner.stopHalloweenArmorBlessing();

		_owner.addDamageReductionByArmor(-item.getDamageReduction());
		_owner.addWeightReduction(-item.getWeightReduction());

		if (itemId == 7246) {
			if (itemlvl > 5) {
				int en = itemlvl - 5;
				_owner.addWeightReduction(-(en * 60));
			}
		}

		if (item.getWeightReduction() != 0) {
			_owner.sendPackets(new S_NewCreateItem("����", _owner));
		}

		if (itemId == 130220 && armor.getEnchantLevel() >= 7) {
			_owner.addHitupByArmor(-(armor.getEnchantLevel() - 3));
			_owner.addBowHitupByArmor(-(item.getBowHitup()));
		} else if (itemId == 9200 && armor.getEnchantLevel() >= 5) {
			_owner.addHitupByArmor(-(armor.getEnchantLevel() - 4));
			_owner.addBowHitupByArmor(-(item.getBowHitup()));
		} else if (itemId == 9201 && armor.getEnchantLevel() >= 5) {
			_owner.addHitupByArmor(-(item.getHitup()));
			_owner.addBowHitupByArmor(-(armor.getEnchantLevel() - 4));
		} else {
			_owner.addHitupByArmor(-(item.getHitup()));
			_owner.addBowHitupByArmor(-(item.getBowHitup()));
		}

		_owner.addDmgupByArmor(-item.getDmgup());
		_owner.addBowDmgupByArmor(-item.getBowDmgup());
		_owner.getResistance().addEarth(-item.get_defense_earth());
		_owner.getResistance().addWind(-item.get_defense_wind());
		_owner.getResistance().addWater(-item.get_defense_water());
		_owner.getResistance().addFire(-item.get_defense_fire());

		for (L1ArmorSet armorSet : L1ArmorSet.getAllSet()) {
			if (armorSet.isPartOfSet(itemId) && _currentArmorSet.contains(armorSet) && !armorSet.isValid(_owner)) {
				armorSet.cancelEffect(_owner);
				_currentArmorSet.remove(armorSet);
				if (item.getMainId() != 0) {
					L1ItemInstance main = _owner.getInventory().findItemId(item.getMainId());
					if (main != null) {
						_owner.sendPackets(new S_ItemStatus(main, _owner, true, false));
					}
				}
				if (item.getMainId2() != 0) {
					L1ItemInstance main = _owner.getInventory().findItemId(item.getMainId2());
					if (main != null) {
						_owner.sendPackets(new S_ItemStatus(main, _owner, true, false));
					}
				}
				if (item.getMainId3() != 0) {
					L1ItemInstance main = _owner.getInventory().findItemId(item.getMainId3());
					if (main != null) {
						_owner.sendPackets(new S_ItemStatus(main, _owner, true, false));
					}
				}

			}
		}

		if (itemId >= 490000 && itemId <= 490017) {
			_owner.getResistance().addFire(-RegistLevel * 2);
			_owner.getResistance().addWind(-RegistLevel * 2);
			_owner.getResistance().addEarth(-RegistLevel * 2);
			_owner.getResistance().addWater(-RegistLevel * 2);
		}

		if (armor.getItemId() == 20107 || armor.getItemId() == 120107) {
			if (armor.getEnchantLevel() >= 3) {
				_owner.getAbility().addSp(-(armor.getEnchantLevel() - 2));
				_owner.sendPackets(new S_SPMR(_owner));
			}
		}

		/*
		 * if(itemId == 7246){ if(armor.getEnchantLevel() > 5){
		 * _owner.addWeightReduction(-(armor.getEnchantLevel() - 5)); } }
		 */
		if (RegistLevel == 10) {// �ǵ��� ���ɹ���
			_owner.getResistance().addFire(-10);
			_owner.getResistance().addWind(-10);
			_owner.getResistance().addEarth(-10);
			_owner.getResistance().addWater(-10);
		} else if (RegistLevel == 11) {// �ǵ��� ��������
			_owner.addMaxMp(-30);
		} else if (RegistLevel == 12) {// �ǵ��� ü�¹���
			_owner.addMaxHp(-50);
		} else if (RegistLevel == 13) {// �ǵ��� �긶����
			_owner.getResistance().addMr(-10);
		} else if (RegistLevel == 14) {// �ǵ��� ��ö����
			_owner.getAC().addAc(1);
		} else if (RegistLevel == 15) {// �ǵ��� ȸ������
			_owner.addHpr(-1);
			_owner.addMpr(-1);
		}

		if (itemId == 423014) {
			_owner.stopAHRegeneration();
		}
		if (itemId == 423015) {
			_owner.stopSHRegeneration();
		}
		if (itemId == 20380) {
			_owner.stopHalloweenRegeneration();
		}
		if (itemId == 420100 || itemId == 420100 || itemId == 420100 || itemId == 420100) {
			if (itemlvl == 7) {
				_owner.addDamageReductionByArmor(-1);
			}
			if (itemlvl == 8) {
				_owner.addDamageReductionByArmor(-2);
			}
			if (itemlvl == 9) {
				_owner.addDamageReductionByArmor(-3);
			}
		}

		if (itemId == 20077 || itemId == 20062 || itemId == 120077) {
			_owner.delInvis();
		}
		if (itemId == 20288 | itemId == 21288) {
			_owner.sendPackets(new S_Ability(1, false));
		}
		if (itemId == 20036) {
			_owner.sendPackets(new S_Ability(3, false));
		}
		if (itemId == 20281 || itemId == 120281) {
			_owner.sendPackets(new S_Ability(2, false));
		}
		if (itemId == 21281) {
			_owner.sendPackets(new S_Ability(2, false));
			_owner.sendPackets(new S_Ability(7, false));
		}
		if (itemId == 20284 || itemId == 120284) {
			_owner.sendPackets(new S_Ability(5, false));
		}
		if (itemId == 20207) {
			_owner.sendPackets(new S_SkillIconBlessOfEva(_owner.getId(), 0));
		}
		/** ���� ���� ��þ�� ��Ÿ �ο� **/
		if (itemId == 420000) {
			if (itemlvl == 5 || itemlvl == 6) {
				_owner.addBowDmgup(-1);
			}
			if (itemlvl == 7 || itemlvl == 8) {
				_owner.addBowDmgup(-2);
			}

			if (itemlvl >= 9) {
				_owner.addBowDmgup(-3);
			}
		}
		if (itemId == 420003) {
			if (itemlvl == 5 || itemlvl == 6) {
				_owner.addDmgup(-1);
			}
			if (itemlvl == 7 || itemlvl == 8) {
				_owner.addDmgup(-2);
			}
			if (itemlvl >= 9) {
				_owner.addDmgup(-3);
			}
		}

		if (itemId == 21097) {// �������� ����
			switch (itemlvl) {
			case 5:
			case 6:
				_owner.getAbility().addSp(-1);
				break;
			case 7:
			case 8:
				_owner.getAbility().addSp(-2);
				break;
			default:
				if (itemlvl >= 9)
					_owner.getAbility().addSp(-3);
				break;
			}
		}
		if (itemId == 21095) { // ü���� ����
			switch (itemlvl) {
			case 5:
			case 6:
				_owner.addMaxHp(-25);
				break;
			case 7:
			case 8:
				_owner.addMaxHp(-50);
				break;
			default:
				if (itemlvl >= 9)
					_owner.addMaxHp(-75);
				break;
			}
		}
		if (itemId == 21096) {// ��ȣ�� ����
			switch (itemlvl) {
			case 5:
			case 6:
				_owner.addDamageReductionByArmor(-1);
				break;
			case 7:
			case 8:
				_owner.addDamageReductionByArmor(-2);
				break;
			default:
				if (itemlvl >= 9)
					_owner.addDamageReductionByArmor(-3);
				break;
			}
		}

		if (itemtype >= 8 && itemtype <= 12) {
			if (itemlvl > 0) {
				if (itemgrade != 3) {
					if (itemtype == 8) {// �����
						switch (itemlvl) {
						case 1:
							_owner.addMaxHp(-5);
							break;
						case 2:
							_owner.addMaxHp(-10);
							break;
						case 3:
							_owner.addMaxHp(-20);
							break;
						case 4:
							_owner.addMaxHp(-30);
							break;
						case 5:
							_owner.addMaxHp(-40);
							_owner.addPotionPlus(-1);
							_owner.getAC().addAc(1);
							_owner.getResistance().addMr(-1);
							break;
						case 6:
							_owner.addMaxHp(-40);
							_owner.addPotionPlus(-2);
							_owner.getAC().addAc(2);
							_owner.getResistance().addMr(-3);
							break;
						case 7:
							_owner.addMaxHp(-50);
							_owner.addPotionPlus(-3);
							_owner.getAC().addAc(3);
							_owner.getResistance().addMr(-5);
							break;
						case 8:
							_owner.addMaxHp(-50);
							_owner.addPotionPlus(-4);
							_owner.getAC().addAc(4);
							_owner.getResistance().addMr(-7);
							break;
						case 9:
							_owner.addMaxHp(-60);
							_owner.addPotionPlus(-6);
							_owner.getAC().addAc(5);
							_owner.getResistance().addMr(-10);
							break;
						default:
						}
					} else if (itemtype == 12) {// �Ͱ���
						switch (itemlvl) {
						case 1:
							_owner.addMaxHp(-5);
							break;
						case 2:
							_owner.addMaxHp(-10);
							break;
						case 3:
							_owner.addMaxHp(-20);
							break;
						case 4:
							_owner.addMaxHp(-30);
							break;
						case 5:
							_owner.addMaxHp(-40);
							_owner.addPotionPlus(-1);
							_owner.getAC().addAc(1);
							break;
						case 6:
							_owner.addMaxHp(-40);
							_owner.addPotionPlus(-2);
							_owner.getAC().addAc(2);
							break;
						case 7:
							_owner.addMaxHp(-50);
							_owner.addPotionPlus(-3);
							_owner.getAC().addAc(3);
							break;
						case 8:
							_owner.addMaxHp(-50);
							_owner.addPotionPlus(-4);
							_owner.getAC().addAc(4);
							break;
						case 9:
							_owner.addMaxHp(-60);
							_owner.addPotionPlus(-6);
							_owner.getAC().addAc(5);
							break;
						default:
						}
					} else if (itemtype == 9 || itemtype == 11) {// ����
						switch (itemlvl) {
						case 1:
							_owner.addMaxHp(-5);
							break;
						case 2:
							_owner.addMaxHp(-10);
							break;
						case 3:
							_owner.addMaxHp(-20);
							break;
						case 4:
							_owner.addMaxHp(-30);
							break;
						case 5:
							_owner.addMaxHp(-40);
							_owner.addDmgupByArmor(-1);
							_owner.addBowDmgupByArmor(-1);
							break;
						case 6:
							_owner.addMaxHp(-40);
							_owner.addDmgupByArmor(-2);
							_owner.addBowDmgupByArmor(-2);
							_owner.getResistance().addMr(-1);
							_owner.addPvPDmgByArmor(-1);
							break;
						case 7:
							_owner.getAbility().addSp(-1);
							_owner.addMaxHp(-50);
							_owner.addDmgupByArmor(-3);
							_owner.addBowDmgupByArmor(-3);
							_owner.getResistance().addMr(-3);
							_owner.addPvPDmgByArmor(-2);
							break;
						case 8:
							_owner.getAbility().addSp(-2);
							_owner.addMaxHp(-50);
							_owner.addDmgupByArmor(-4);
							_owner.addBowDmgupByArmor(-4);
							_owner.getResistance().addMr(-5);
							_owner.addPvPDmgByArmor(-3);
							break;
						case 9:
							_owner.getAbility().addSp(-3);
							_owner.addMaxHp(-60);
							_owner.addDmgupByArmor(-5);
							_owner.addBowDmgupByArmor(-5);
							_owner.getResistance().addMr(-7);
							_owner.addPvPDmgByArmor(-5);
							break;
						case 10:
							_owner.getAbility().addSp(-4);
							_owner.addMaxHp(-70);
							_owner.addDmgupByArmor(-6);
							_owner.addBowDmgupByArmor(-6);
							_owner.getResistance().addMr(-9);
							_owner.addPvPDmgByArmor(-7);
							break;
						case 11:
							_owner.getAbility().addSp(-5);
							_owner.addMaxHp(-80);
							_owner.addDmgupByArmor(-7);
							_owner.addBowDmgupByArmor(-7);
							_owner.getResistance().addMr(-11);
							_owner.addPvPDmgByArmor(-9);
							break;
						case 12:
							_owner.getAbility().addSp(-6);
							_owner.addMaxHp(-100);
							_owner.addDmgupByArmor(-8);
							_owner.addBowDmgupByArmor(-8);
							_owner.getResistance().addMr(-13);
							_owner.addPvPDmgByArmor(-11);
							break;
						default:
						}
					} else if (itemtype == 10) {// ��Ʈ
						switch (itemlvl) {
						case 1:
							_owner.addMaxMp(-5);
							break;
						case 2:
							_owner.addMaxMp(-10);
							break;
						case 3:
							_owner.addMaxMp(-20);
							break;
						case 4:
							_owner.addMaxMp(-30);
							break;
						case 5:
							_owner.addMaxMp(-40);
							_owner.addDamageReductionByArmor(-1);
							break;
						case 6:
							_owner.addMaxMp(-40);
							_owner.addDamageReductionByArmor(-2);
							_owner.addMaxHp(-20);
							_owner.addPvPReductionByArmor(-1);
							break;
						case 7:
							_owner.addMaxHp(-30);
							_owner.addMaxMp(-50);
							_owner.addDamageReductionByArmor(-3);
							_owner.addPvPReductionByArmor(-3);
							break;
						case 8:
							_owner.addMaxHp(-40);
							_owner.addMaxMp(-50);
							_owner.addDamageReductionByArmor(-4);
							_owner.addPvPReductionByArmor(-5);
							break;
						case 9:
							_owner.addMaxHp(-50);
							_owner.addMaxMp(-60);
							_owner.addDamageReductionByArmor(-5);
							_owner.addPvPReductionByArmor(-7);
							break;
						default:
						}
					}
				} else if (itemId == 500007) { // ��Ƽ���� �������Ͱ���
					switch (itemlvl) {
					case 1:
						_owner.addMaxHp(-20);
						break;
					case 2:
						_owner.addMaxHp(-30);
						break;
					case 3:
						_owner.addMaxHp(-40);
						_owner.addDamageReductionByArmor(-1);
						break;
					case 4:
						_owner.addMaxHp(-50);
						_owner.addDamageReductionByArmor(-1);
						break;
					case 5:
						_owner.addMaxHp(-60);
						_owner.addDamageReductionByArmor(-2);
						break;
					case 6:
						_owner.getAC().addAc(7);
						_owner.addMaxHp(-70);
						_owner.addDamageReductionByArmor(-3);
						break;
					case 7:
						_owner.getAC().addAc(8);
						_owner.addMaxHp(-80);
						_owner.addDamageReductionByArmor(-4);
						_owner.addDmgupByArmor(-1);
						_owner.addBowDmgupByArmor(-1);
						break;
					case 8:
						_owner.getAC().addAc(9);
						_owner.addMaxHp(-90);
						_owner.addDamageReductionByArmor(-5);
						_owner.addDmgupByArmor(-3);
						_owner.addBowDmgupByArmor(-3);
						break;
					default:
					}
				} else if (itemId == 502007) { // ��Ƽ���� �������Ͱ���
					switch (itemlvl) {
					case 3:
						_owner.addMaxHp(-50);
						_owner.addDamageReductionByArmor(-1);
						break;
					case 4:
						_owner.addMaxHp(-60);
						_owner.addDamageReductionByArmor(-2);
						break;
					case 5:
						_owner.getAC().addAc(7);
						_owner.addMaxHp(-70);
						_owner.addDamageReductionByArmor(-3);
						break;
					case 6:
						_owner.getAC().addAc(8);
						_owner.addMaxHp(-80);
						_owner.addDamageReductionByArmor(-4);
						_owner.addDmgupByArmor(-1);
						_owner.addBowDmgupByArmor(-1);
						break;
					case 7:
						_owner.getAC().addAc(9);
						_owner.addMaxHp(-90);
						_owner.addDamageReductionByArmor(-5);
						_owner.addDmgupByArmor(-3);
						_owner.addBowDmgupByArmor(-3);
						break;
					case 8:
						_owner.getAC().addAc(10);
						_owner.addMaxHp(-140);
						_owner.addDamageReductionByArmor(-6);
						_owner.addDmgupByArmor(-5);
						_owner.addBowDmgupByArmor(-5);
						break;
					default:
					}
				} else if (itemId == 500008) { // ��Ƽ���� Ǫ�����Ͱ���
					switch (itemlvl) {
					case 1:
					case 2:
					case 3:
					case 4:
						break;
					case 5:
						_owner.getAC().addAc(1);
						break;
					case 6:
						_owner.getAC().addAc(2);
						break;
					case 7:
						_owner.getAC().addAc(2);
						break;
					case 8:
						_owner.getAC().addAc(3);
						break;
					default:
					}
				} else if (itemId == 502008) { // ��Ƽ���� Ǫ�����Ͱ���
					switch (itemlvl) {
					case 4:
						_owner.getAC().addAc(1);
						break;
					case 5:
						_owner.getAC().addAc(2);
						break;
					case 6:
						_owner.getAC().addAc(2);
						break;
					case 7:
						_owner.getAC().addAc(3);
						break;
					case 8:
						_owner.getAC().addAc(4);
						break;
					default:
					}
				} else if (itemId == 500009) { // ��Ƽ���� ������ �Ͱ���
					switch (itemlvl) {
					case 1:
						_owner.addMaxMp(-10);
						break;
					case 2:
						_owner.addMaxMp(-15);
						break;
					case 3:
						_owner.addMaxMp(-30);
						_owner.getAbility().addSp(-1);
						break;
					case 4:
						_owner.addMaxMp(-35);
						_owner.getAbility().addSp(-1);
						break;
					case 5:
						_owner.addMaxMp(-50);
						_owner.getAbility().addSp(-2);
						break;
					case 6:
						_owner.getAC().addAc(1);
						_owner.addMaxMp(-55);
						_owner.getAbility().addSp(-2);
						break;
					case 7:
						_owner.getAC().addAc(2);
						_owner.addMaxMp(-70);
						_owner.getAbility().addSp(-3);
						_owner.addSuccMagic(-1);
						break;
					case 8:
						_owner.getAC().addAc(3);
						_owner.addMaxMp(-95);
						_owner.getAbility().addSp(-3);
						_owner.addSuccMagic(-3);
						break;
					default:
					}
				} else if (itemId == 502009) { // ��Ƽ���� �������Ͱ���
					switch (itemlvl) {
					case 3:
						_owner.addMaxMp(-35);
						_owner.getAbility().addSp(-1);
						break;
					case 4:
						_owner.addMaxMp(-50);
						_owner.getAbility().addSp(-2);
						break;
					case 5:
						_owner.getAC().addAc(1);
						_owner.addMaxMp(-55);
						_owner.getAbility().addSp(-2);
						break;
					case 6:
						_owner.getAC().addAc(2);
						_owner.addMaxMp(-70);
						_owner.getAbility().addSp(-3);
						_owner.addSuccMagic(-1);
						break;
					case 7:
						_owner.getAC().addAc(3);
						_owner.addMaxMp(-95);
						_owner.getAbility().addSp(-3);
						_owner.addSuccMagic(-3);
						break;
					case 8:
						_owner.getAC().addAc(4);
						_owner.addMaxMp(-130);
						_owner.getAbility().addSp(-4);
						_owner.addSuccMagic(-5);
						break;
					}
				} else if ((itemId >= 425109 && itemId <= 425113) || (itemId >= 525109 && itemId <= 525113)
						|| (itemId >= 625109 && itemId <= 625113)) {
					if (itemlvl > 0) {
						_owner.addMaxHp(-((itemlvl * 5) + 10));
					}
					switch (itemlvl) {
					case 2:
					case 3:
					case 4:
						_owner.getAC().addAc(itemlvl - 1);
						break;
					case 5:
					case 6:
					case 7:
					case 8:
						_owner.getAC().addAc(4);
						_owner.addDmgup(-(itemlvl - 4));
						_owner.addBowDmgup(-(itemlvl - 4));
						break;
					}
				} else if (itemId == 21247 || itemId == 21251) { // ������ ��������
					if (itemlvl > 0) {
						_owner.addMaxHp(-((itemlvl * 5) + 10));
					}
				} else if (itemId == 21248 || itemId == 21252) { // ������ ü��
					if (itemlvl > 0) {
						_owner.addMaxHp(-((itemlvl * 5) + 10));
						if (itemId == 21252 && itemlvl >= 6) {
							_owner.addMaxHp(-(itemlvl - 5) * 5);
						}
					}
				} else if (itemId == 21246 || itemId == 21250) { // ������ ����
					if (itemlvl > 0) {
						if (itemId == 21250) {
							if (itemlvl == 1) {
								_owner.addMaxHp(-5);
							} else if (itemlvl == 2) {
								_owner.addMaxHp(-10);
							} else if (itemlvl == 3) {
								_owner.addMaxHp(-20);
							} else if (itemlvl == 4) {
								_owner.addMaxHp(-25);
								_owner.getAbility().addSp(-1);
							} else if (itemlvl == 5) {
								_owner.addMaxHp(-30);
								_owner.getAbility().addSp(-2);
							} else if (itemlvl == 6) {
								_owner.addMaxHp(-35);
								_owner.getAbility().addSp(-3);
								_owner.addSuccMagic(-1);
							} else if (itemlvl == 7) {
								_owner.addMaxHp(-40);
								_owner.addMaxMp(-15);
								_owner.getAbility().addSp(-4);
								_owner.addSuccMagic(-2);
							} else if (itemlvl == 8) {
								_owner.addMaxHp(-50);
								_owner.addMaxMp(-20);
								_owner.getAbility().addSp(-5);
								_owner.addSuccMagic(-3);
							}
						} else {
							_owner.addMaxHp(-(itemlvl * 5));
							if (itemlvl == 5) {
								_owner.getAbility().addSp(-1);
							} else if (itemlvl == 6) {
								_owner.getAbility().addSp(-2);
							} else if (itemlvl == 7) {
								_owner.getAbility().addSp(-3);
								_owner.addSuccMagic(-1);
							} else if (itemlvl == 8) {
								_owner.addMaxMp(-15);
								_owner.getAbility().addSp(-4);
								_owner.addSuccMagic(-2);
							}
						}
					}
				} else if (itemId == 21249 || itemId == 21253) { // ������ ���
					if (itemlvl >= 3) {
						_owner.addMaxHp(-(itemlvl - 2) * 5);
					}
					switch (itemlvl) {
					case 0:
						break;
					case 1:
					case 2:
					case 3:
					case 4:
						_owner.getAC().addAc(itemlvl);
						break;
					case 5:
					case 6:
						_owner.getAC().addAc(4);
						break;
					case 7:
					case 8:
						_owner.getAC().addAc(4);
						if (itemId == 21253)
							_owner.getAC().addAc(1);
						break;
					}
				}

				if (itemId == 21249) {
					switch (itemlvl) {
					case 5:
					case 6:
					case 7:
					case 8:
						_owner.addHitup(-(itemlvl - 4));
						_owner.addBowHitup(-(itemlvl - 4));
						break;
					}
				}
				if (itemId == 21253) {
					switch (itemlvl) {
					case 4:
					case 5:
					case 6:
					case 7:
					case 8:
						_owner.addHitup(-(itemlvl - 3));
						_owner.addBowHitup(-(itemlvl - 3));
						break;
					}
				}
				if (itemId == 21247) {
					switch (itemlvl) {
					case 7:
						_owner.addȮ������ȸ��(1);
						break;
					case 8:
						_owner.addȮ������ȸ��(3);
						break;
					}
				}
				if (itemId == 21251) {
					switch (itemlvl) {
					case 6:
						_owner.addȮ������ȸ��(1);
						break;
					case 7:
						_owner.addȮ������ȸ��(3);
						break;
					case 8:
						_owner.addȮ������ȸ��(5);
						break;
					}
				}
				if (itemId == 21248) { // �����۹���
					switch (itemlvl) {
					case 7:
						_owner.addDamageReductionByArmor(-1);
						break;
					case 8:
						_owner.addDamageReductionByArmor(-2);
						break;
					}
				}
				if (itemId == 21252) { // �����۹���
					switch (itemlvl) {
					case 6:
						_owner.addDamageReductionByArmor(-1);
						break;
					case 7:
						_owner.addDamageReductionByArmor(-2);
						break;
					case 8:
						_owner.addDamageReductionByArmor(-3);
						break;
					}
				}
				if (itemId >= 21247 && itemId <= 21249) { // �����۹���
					switch (itemlvl) {
					case 5:
					case 6:
					case 7:
					case 8:
						_owner.addDmgup(-(itemlvl - 4));
						_owner.addBowDmgup(-(itemlvl - 4));
						break;
					}
				}
				if (itemId >= 21251 && itemId <= 21253) { // �����۹���
					switch (itemlvl) {
					case 4:
					case 5:
					case 6:
					case 7:
					case 8:
						_owner.addDmgup(-(itemlvl - 3));
						_owner.addBowDmgup(-(itemlvl - 3));
						break;
					}
				}

				if (itemId == 21246) { // ������ ���� ����
					switch (itemlvl) {
					case 0:
					case 1:
						break;
					case 2:
					case 3:
					case 4:
						_owner.getAC().addAc(itemlvl - 1);
						break;
					case 5:
						_owner.getAC().addAc(3);
						break;
					case 6:
					case 7:
						_owner.getAC().addAc(4);
						break;
					case 8:
						_owner.getAC().addAc(5);
						break;
					}
				}
				if (itemId == 21247) { // ������ ���� ����
					switch (itemlvl) {
					case 0:
					case 1:
						break;
					case 2:
					case 3:
					case 4:
						_owner.getAC().addAc(itemlvl - 1);
						break;
					case 5:
					case 6:
					case 7:
					case 8:
						_owner.getAC().addAc(4);
						break;
					}
				}
				if (itemId == 21248) { // ������ ü�� ����
					switch (itemlvl) {
					case 0:
					case 1:
						break;
					case 2:
					case 3:
					case 4:
						_owner.getAC().addAc(itemlvl - 1);
						break;
					case 5:
					case 6:
					case 7:
					case 8:
						_owner.getAC().addAc(4);
						break;
					}
				}
				if (itemId == 21250) { // �����۹���
					switch (itemlvl) {
					case 0:
					case 1:
						break;
					case 2:
					case 3:
					case 4:
						_owner.getAC().addAc(itemlvl - 1);
						break;
					case 5:
					case 6:
						_owner.getAC().addAc(4);
						break;
					case 7:
					case 8:
						_owner.getAC().addAc(4);
						break;
					}
				}
				if (itemId == 21251) { // �� ������ ���� ����
					switch (itemlvl) {
					case 0:
					case 1:
						break;
					case 2:
					case 3:
					case 4:
						_owner.getAC().addAc(itemlvl - 1);
						break;
					case 5:
					case 6:
						_owner.getAC().addAc(4);
						break;
					case 7:
					case 8:
						_owner.getAC().addAc(5);
						break;
					}
				}
				if (itemId == 21252) { // �� ������ ü�� ����
					switch (itemlvl) {
					case 0:
					case 1:
						break;
					case 2:
					case 3:
					case 4:
						_owner.getAC().addAc(itemlvl - 1);
						break;
					case 5:
					case 6:
						_owner.getAC().addAc(4);
						break;
					case 7:
					case 8:
						_owner.getAC().addAc(5);
						break;
					}
				}
				if (itemId == 500010 || itemId == 502010) {
					int ac = itemlvl;

					if (item.getBless() == 0 && itemlvl >= 3) {
						ac += 1;
					}
					_owner.getAC().addAc(ac);

					int dm = itemlvl - 2;

					if (item.getBless() != 0 && itemlvl >= 4)
						dm -= 1;

					_owner.addDmgup(-dm);
					_owner.addBowDmgup(-dm);
				}
			}
		}
		armor.stopEquipmentTimer();

		_armors.remove(armor);

		if (armor.getSkill() != null && armor.getSkill().getSkillId() != 0) {
			/*
			 * switch (armor.getSkill().getSkillId()) { case L1SkillId.BLESSED_ARMOR:
			 * _owner.sendPackets(new S_PacketBox(S_PacketBox.SKILL_WEAPON_ICON, 748, 0,
			 * false, false)); break; default: break; }
			 */
		}
	}

	public void set(L1ItemInstance equipment, boolean doubleweapon, boolean loaded) {
		L1Item item = equipment.getItem();

		if (item.getType2() == 0) {
			return;
		}

		if (item.get_addhp() != 0) {
			_owner.addMaxHp(item.get_addhp());
		}
		if (item.get_addmp() != 0) {
			if (item.getItemId() == 21166)
				_owner.addMaxMp(item.get_addmp() + (equipment.getEnchantLevel() * 10));
			else
				_owner.addMaxMp(item.get_addmp());
		}
		// _owner.sendPackets(new S_ACTION_UI(S_ACTION_UI.EINHASAD, _owner));
		_owner.getAbility().addAddedStr(item.get_addstr());
		_owner.getAbility().addAddedCon(item.get_addcon());
		_owner.getAbility().addAddedDex(item.get_adddex());
		_owner.getAbility().addAddedInt(item.get_addint());
		_owner.getAbility().addAddedWis(item.get_addwis());
		if (item.get_addwis() != 0) {
			_owner.resetBaseMr();
		}
		_owner.getAbility().addAddedCha(item.get_addcha());

		if (item.getItemId() == 22222) {
			if (equipment.getEnchantLevel() == 7) {
				_owner.getAbility().addAddedCha(1);
			} else if (equipment.getEnchantLevel() == 8) {
				_owner.getAbility().addAddedCha(2);
			} else if (equipment.getEnchantLevel() >= 9) {
				_owner.getAbility().addAddedCha(3);
			}
		}
		if (item.getItemId() == 118) {
			if (equipment.getEnchantLevel() >= 9) {
				_owner.getAbility().addAddedCha(1);
			}
		}
		if (item.getItemId() == 20112 || item.getItemId() == 120112 || item.getItemId() == 20016
				|| item.getItemId() == 120016) {
			if (equipment.getEnchantLevel() >= 7) {
				_owner.getAbility().addAddedCha(1);
			}
		}

		int addMr = 0;
		addMr += equipment.getMr();
		if (item.getItemId() == 20236 && _owner.isElf()) {
			addMr += 5;
		}
		if (addMr != 0) {
			_owner.getResistance().addMr(addMr);
			_owner.sendPackets(new S_SPMR(_owner));
		}
		if (item.get_addsp() != 0) {
			_owner.getAbility().addSp(item.get_addsp());
			_owner.sendPackets(new S_SPMR(_owner));
		}
		if (item.isHasteItem()) {
			_owner.addHasteItemEquipped(1);
			_owner.removeHasteSkillEffect();
			if (_owner.getMoveState().getMoveSpeed() != 1) {
				_owner.getMoveState().setMoveSpeed(1);
				_owner.sendPackets(new S_SkillHaste(_owner.getId(), 1, -1));
				Broadcaster.broadcastPacket(_owner, new S_SkillHaste(_owner.getId(), 1, 0));
			}
		}
		_owner.getEquipSlot().setMagicHelm(equipment);

		if (equipment.getRegistTechnique() != 0) {
			_owner.getResistance().addTechnique(equipment.getRegistTechnique());
		}

		if (equipment.getRegistSpirit() != 0) {
			_owner.getResistance().addSpirit(equipment.getRegistSpirit());
		}

		if (equipment.getRegistDragonLang() != 0) {
			_owner.getResistance().addDragonLang(equipment.getRegistDragonLang());
		}

		if (equipment.getRegistFear() != 0) {
			_owner.getResistance().addFear(equipment.getRegistFear());
		}

		if (equipment.getHitTechnique() != 0) {
			_owner.getResistance().addTechniqueHit(equipment.getHitTechnique());
		}

		if (equipment.getHitSpirit() != 0) {
			_owner.getResistance().addSpiritHit(equipment.getHitSpirit());
		}

		if (equipment.getHitDragonLang() != 0) {
			_owner.getResistance().addDragonLangHit(equipment.getHitDragonLang());
		}

		if (equipment.getHitFear() != 0) {
			_owner.getResistance().addFearHit(equipment.getHitFear());
		}

		if (equipment.getAinBooster() != 0) {
			_owner.getResistance().addAinBooster(equipment.getAinBooster());
			_owner.sendPackets(new S_ACTION_UI(S_ACTION_UI.EINHASAD, _owner));
		}

		if (item.getType2() == 1) {
			setWeapon(equipment, doubleweapon);
		} else if (item.getType2() == 2) {
			setArmor(equipment, loaded);
			_owner.sendPackets(new S_SPMR(_owner));
		}
		_owner.sendPackets(new S_PacketBox(S_PacketBox.char_ER, _owner.get_PlusEr()), true);
		_owner.sendPackets(new S_OwnCharAttrDef(_owner));
	}

	public void remove(L1ItemInstance equipment, boolean doubleweapon) {
		L1Item item = equipment.getItem();
		if (item.getType2() == 0) {
			return;
		}

		if (item.get_addhp() != 0) {
			_owner.addMaxHp(-item.get_addhp());
		}
		if (item.get_addmp() != 0) {
			if (item.getItemId() == 21166)
				_owner.addMaxMp(-(item.get_addmp() + (equipment.getEnchantLevel() * 10)));
			else
				_owner.addMaxMp(-item.get_addmp());
		}

		_owner.getAbility().addAddedStr((byte) -item.get_addstr());
		_owner.getAbility().addAddedCon((byte) -item.get_addcon());
		_owner.getAbility().addAddedDex((byte) -item.get_adddex());
		_owner.getAbility().addAddedInt((byte) -item.get_addint());
		_owner.getAbility().addAddedWis((byte) -item.get_addwis());
		if (item.get_addwis() != 0) {
			_owner.resetBaseMr();
		}
		_owner.getAbility().addAddedCha((byte) -item.get_addcha());
		if (item.getItemId() == 22222) {
			if (equipment.getEnchantLevel() == 7) {
				_owner.getAbility().addAddedCha(-1);
			} else if (equipment.getEnchantLevel() == 8) {
				_owner.getAbility().addAddedCha(-2);
			} else if (equipment.getEnchantLevel() >= 9) {
				_owner.getAbility().addAddedCha(-3);
			}
		}
		if (item.getItemId() == 118) {
			if (equipment.getEnchantLevel() >= 9) {
				_owner.getAbility().addAddedCha(-1);
			}
		}
		if (item.getItemId() == 20112 || item.getItemId() == 120112 || item.getItemId() == 20016
				|| item.getItemId() == 120016) {
			if (equipment.getEnchantLevel() >= 7) {
				_owner.getAbility().addAddedCha(-1);
			}
		}
		int addMr = 0;
		addMr -= equipment.getMr();
		if (item.getItemId() == 20236 && _owner.isElf()) {
			addMr -= 5;
		}
		if (addMr != 0) {
			_owner.getResistance().addMr(addMr);
			_owner.sendPackets(new S_SPMR(_owner));
		}
		if (item.get_addsp() != 0) {
			_owner.getAbility().addSp(-item.get_addsp());
			_owner.sendPackets(new S_SPMR(_owner));
		}
		if (item.isHasteItem()) {
			_owner.addHasteItemEquipped(-1);
			if (_owner.getHasteItemEquipped() == 0) {
				_owner.getMoveState().setMoveSpeed(0);
				_owner.sendPackets(new S_SkillHaste(_owner.getId(), 0, 0));
				Broadcaster.broadcastPacket(_owner, new S_SkillHaste(_owner.getId(), 0, 0));
			}
		}
		_owner.getEquipSlot().removeMagicHelm(_owner.getId(), equipment);

		if (equipment.getRegistTechnique() != 0) {
			_owner.getResistance().addTechnique(-equipment.getRegistTechnique());
		}

		if (equipment.getRegistSpirit() != 0) {
			_owner.getResistance().addSpirit(-equipment.getRegistSpirit());
		}

		if (equipment.getRegistDragonLang() != 0) {
			_owner.getResistance().addDragonLang(-equipment.getRegistDragonLang());
		}

		if (equipment.getRegistFear() != 0) {
			_owner.getResistance().addFear(-equipment.getRegistFear());
		}

		if (equipment.getHitTechnique() != 0) {
			_owner.getResistance().addTechniqueHit(-equipment.getHitTechnique());
		}

		if (equipment.getHitSpirit() != 0) {
			_owner.getResistance().addSpiritHit(-equipment.getHitSpirit());
		}

		if (equipment.getHitDragonLang() != 0) {
			_owner.getResistance().addDragonLangHit(-equipment.getHitDragonLang());
		}

		if (equipment.getHitFear() != 0) {
			_owner.getResistance().addFearHit(-equipment.getHitFear());
		}
		if (equipment.getAinBooster() != 0) {
			_owner.getResistance().addAinBooster(-equipment.getAinBooster());
			_owner.sendPackets(new S_ACTION_UI(S_ACTION_UI.EINHASAD, _owner));
		}
		if (item.getType2() == 1) {
			removeWeapon(equipment, doubleweapon);
		} else if (item.getType2() == 2) {
			removeArmor(equipment);
		}
		_owner.sendPackets(new S_PacketBox(S_PacketBox.char_ER, _owner.get_PlusEr()), true);
		_owner.sendPackets(new S_OwnCharAttrDef(_owner));
	}

	public void setMagicHelm(L1ItemInstance item) {
		switch (item.getItemId()) {
		case 20008:
			_owner.setSkillMastery(HASTE);
			_owner.sendPackets(new S_AddSkill(0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
					0, 0, 0, 0, 0, 0, 0, _owner.getElfAttr()));
			break;
		case 20013:
		case 120013:
			_owner.setSkillMastery(PHYSICAL_ENCHANT_DEX);
			_owner.setSkillMastery(HASTE);
			_owner.sendPackets(new S_AddSkill(0, 0, 0, 2, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
					0, 0, 0, 0, 0, 0, 0, _owner.getElfAttr()));
			break;
		case 20014:
		case 120014:
			_owner.setSkillMastery(HEAL);
			_owner.setSkillMastery(EXTRA_HEAL);
			_owner.sendPackets(new S_AddSkill(1, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
					0, 0, 0, 0, 0, 0, 0, _owner.getElfAttr()));
			break;
		case 20015:
		case 120015:
			_owner.setSkillMastery(ENCHANT_WEAPON);
			_owner.setSkillMastery(DETECTION);
			_owner.setSkillMastery(PHYSICAL_ENCHANT_STR);
			_owner.sendPackets(new S_AddSkill(0, 24, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
					0, 0, 0, 0, 0, 0, 0, 0, _owner.getElfAttr()));
			break;
		case 20023:
			_owner.setSkillMastery(GREATER_HASTE);
			_owner.sendPackets(new S_AddSkill(0, 0, 0, 0, 0, 0, 32, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
					0, 0, 0, 0, 0, 0, 0, 0, _owner.getElfAttr()));
			break;
		}
	}

	public void removeMagicHelm(int objectId, L1ItemInstance item) {
		switch (item.getItemId()) {
		case 20008:
			if (!SkillsTable.getInstance().spellCheck(objectId, HASTE)) {
				_owner.removeSkillMastery(HASTE);
				_owner.sendPackets(new S_DelSkill(0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
						0, 0, 0, 0, 0));
			}
			break;
		case 20013:
			if (!SkillsTable.getInstance().spellCheck(objectId, PHYSICAL_ENCHANT_DEX)) {
				_owner.removeSkillMastery(PHYSICAL_ENCHANT_DEX);
				_owner.sendPackets(new S_DelSkill(0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
						0, 0, 0, 0, 0));
			}
			if (!SkillsTable.getInstance().spellCheck(objectId, HASTE)) {
				_owner.removeSkillMastery(HASTE);
				_owner.sendPackets(new S_DelSkill(0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
						0, 0, 0, 0, 0));
			}
			break;
		case 20014:
			if (!SkillsTable.getInstance().spellCheck(objectId, HEAL)) {
				_owner.removeSkillMastery(HEAL);
				_owner.sendPackets(new S_DelSkill(1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
						0, 0, 0, 0, 0));
			}
			if (!SkillsTable.getInstance().spellCheck(objectId, EXTRA_HEAL)) {
				_owner.removeSkillMastery(EXTRA_HEAL);
				_owner.sendPackets(new S_DelSkill(0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
						0, 0, 0, 0, 0));
			}
			break;
		case 20015:
			if (!SkillsTable.getInstance().spellCheck(objectId, ENCHANT_WEAPON)) {
				_owner.removeSkillMastery(ENCHANT_WEAPON);
				_owner.sendPackets(new S_DelSkill(0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
						0, 0, 0, 0, 0));
			}
			if (!SkillsTable.getInstance().spellCheck(objectId, DETECTION)) {
				_owner.removeSkillMastery(DETECTION);
				_owner.sendPackets(new S_DelSkill(0, 16, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
						0, 0, 0, 0, 0));
			}
			if (!SkillsTable.getInstance().spellCheck(objectId, PHYSICAL_ENCHANT_STR)) {
				_owner.removeSkillMastery(PHYSICAL_ENCHANT_STR);
				_owner.sendPackets(new S_DelSkill(0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
						0, 0, 0, 0, 0));
			}
			break;
		case 20023:
			if (!SkillsTable.getInstance().spellCheck(objectId, GREATER_HASTE)) {
				_owner.removeSkillMastery(GREATER_HASTE);
				_owner.sendPackets(new S_DelSkill(0, 0, 0, 0, 0, 0, 32, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
						0, 0, 0, 0, 0));
			}
			break;
		}
	}

}
