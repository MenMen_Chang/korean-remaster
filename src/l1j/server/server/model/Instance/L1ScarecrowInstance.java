package l1j.server.server.model.Instance;

import java.util.ArrayList;

import l1j.server.server.model.Broadcaster;
import l1j.server.server.model.L1Attack;
import l1j.server.server.serverpackets.S_ChangeHeading;
import l1j.server.server.serverpackets.S_SystemMessage;
import l1j.server.server.templates.L1Npc;
import l1j.server.server.utils.CalcExp;

public class L1ScarecrowInstance extends L1NpcInstance {

	private static final long serialVersionUID = 1L;

	public L1ScarecrowInstance(L1Npc template) {
		super(template);
	}

	@Override
	public void onAction(L1PcInstance player) {
		L1Attack attack = new L1Attack(player, this);
		if (attack.calcHit()) {
			
			if (player.getLevel() < 5) {
				ArrayList<L1PcInstance> targetList = new ArrayList<L1PcInstance>();
				targetList.add(player);
				ArrayList<Integer> hateList = new ArrayList<Integer>();
				hateList.add(1);
				if(this.getNpcId() != 202064){
				CalcExp.calcExp(player, getId(), targetList, hateList, getExp());
				}
			}

			int heading = getMoveState().getHeading();
			if (heading < 7)
				heading++;
			else
				heading = 0;
			getMoveState().setHeading(heading);
			S_ChangeHeading ch = new S_ChangeHeading(this);
			Broadcaster.broadcastPacket(this, ch, true);
		}
		 if (this.getNpcId() == 202064){
			 player.sendPackets(new S_SystemMessage(player, "[알림] 스승 군터에게 보급품을 지급 받으세요."), true);
		 }
		attack.action();
		attack = null;
	}

	@Override
	public void onTalkAction(L1PcInstance l1pcinstance) {
	}

	public void onFinalAction() {
	}

	public void doFinalAction() {
	}
}
