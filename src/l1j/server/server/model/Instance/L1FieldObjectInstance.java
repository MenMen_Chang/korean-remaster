/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package l1j.server.server.model.Instance;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import l1j.server.Config;
import l1j.server.GameSystem.Antaras.AntarasRaid;
import l1j.server.GameSystem.Antaras.AntarasRaidSystem;
import l1j.server.GameSystem.Antaras.AntarasRaidTimer;
import l1j.server.GameSystem.Lind.Lind;
import l1j.server.GameSystem.Lind.LindRaid;
import l1j.server.GameSystem.Lind.LindThread;
import l1j.server.GameSystem.Papoo.PaPooRaid;
import l1j.server.GameSystem.Papoo.PaPooRaidSystem;
import l1j.server.GameSystem.Papoo.PaPooTimer;
import l1j.server.GameSystem.valakas.ValaRaid;
import l1j.server.GameSystem.valakas.ValaRaidSystem;
import l1j.server.server.GeneralThreadPool;
import l1j.server.server.datatables.ExpTable;
import l1j.server.server.datatables.ItemTable;
import l1j.server.server.model.L1Inventory;
import l1j.server.server.model.L1Object;
import l1j.server.server.model.L1PcInventory;
import l1j.server.server.model.L1Quest;
import l1j.server.server.model.L1Teleport;
import l1j.server.server.model.L1World;
import l1j.server.server.model.skill.L1SkillId;
import l1j.server.server.serverpackets.S_CloseList;
import l1j.server.server.serverpackets.S_EffectLocation;
import l1j.server.server.serverpackets.S_Message_YN;
import l1j.server.server.serverpackets.S_RemoveObject;
import l1j.server.server.serverpackets.S_ReturnedStat;
import l1j.server.server.serverpackets.S_SabuTell;
import l1j.server.server.serverpackets.S_ServerMessage;
import l1j.server.server.serverpackets.S_SkillSound;
import l1j.server.server.serverpackets.S_SystemMessage;
import l1j.server.server.templates.L1Npc;
import l1j.server.server.utils.L1SpawnUtil;

public class L1FieldObjectInstance extends L1NpcInstance {

	private static final SimpleDateFormat ss = new SimpleDateFormat("MM-dd HH:mm", Locale.KOREA);
	private static final long serialVersionUID = 1L;

	public boolean create = false;
	public int moveMapId;
	public int Potal_Open_pcid = 0;

	public boolean 개미지옥몹스폰가능 = true;

	public L1FieldObjectInstance(L1Npc template) {
		super(template);
		if (getNpcId() == 81106) {
			GeneralThreadPool.getInstance().schedule(new 개미지옥(), 1);
		}
	}

	@Override
	public void onAction(L1PcInstance pc) {
	}

	@Override
	public void onTalkAction(L1PcInstance pc) {
		int npcid = getNpcTemplate().get_npcId();
		switch (npcid) {
		case 202065:{
				if (pc.getInventory().getSize() >= 50) {
					pc.sendPackets(new S_SystemMessage(pc, "아이템 사용 실패: 인벤토리의 갯수를 줄인후 사용해주세요."), true);
					return;
				}
				
				if (pc.getInventory().calcWeightpercent() >= 50) {
					pc.sendPackets(new S_SystemMessage(pc, "아이템 사용 실패: 무게 게이지 50% 이상 사용 불가"), true);
					return;
				}
				
				if(pc.getLevel() < 70){
				pc.addExp((ExpTable.getExpByLevel(70) - 1) - pc.getExp() + ((ExpTable.getExpByLevel(70) - 1) / 100)); 
				}
				
				L1Teleport.teleport(pc, 33432, 32800, (short) 4, 6, true);
				
				pc.sendPackets(new S_CloseList(pc.getId()), true);
				
				createNewItem(pc, 40308, 200000);
				
				
				
				createNewItem(pc, 40024, 200); // 수련자의 체력 회복제
				
				createNewItem(pc, 40018, 10); // 수련자의 속도 향상 물약
				
				createNewItem(pc, 41246, 1000); // 결정체
				createNewItem(pc, 60360, 10); // 조우의 이동 기억책
				
				createNewItem(pc, 40081, 20); // 기란 귀환 주문서
				createNewItem(pc, 40088, 10); //  변신 주문서 
				createNewItem(pc, 40100, 10); // 순간이동 주문서
				createNewItem(pc, 140100, 10); // 축복받은 순간이동 주문서
				createNewItem(pc, 430005, 1); // 회상의 촛불
				createNewItem(pc, 60080, 1); // 희미한 기억의 구슬
				createNewItem(pc, 600228, 1); // 고대의 금빛 릴
				createNewItem(pc, 40033, 1); // 엘릭서
				createNewItem(pc, 40034, 1); // 엘릭서
				createNewItem(pc, 40035, 1); // 엘릭서
				createNewItem(pc, 40036, 1); // 엘릭서
				createNewItem(pc, 60217, 1); // 코마
				createNewItem(pc, 40036, 1); // 엘릭서
				createNewItem(pc, 40036, 1); // 엘릭서
				
				if (pc.isWarrior()) {
					createNewItem(pc, 60134, 10); // 수련자의 용기의 물약
					
					createNewItem(pc, 61072, 10); // 수련자의 한우 스테이크
					createNewItem(pc, 61075, 10); // 수련자의 닭고기 스프
					
					createNewItem3(pc, 9103, 1, 0); // 타이탄의 분노
					createNewItem3(pc, 9103, 1, 0); // 타이탄의 분노
					
					createNewItem3(pc, 20082, 1, 5); // 수련자의 티셔츠
					createNewItem3(pc, 21103, 1, 7); // 수련자의 견고한 장갑
					createNewItem3(pc, 21105, 1, 7); // 수련자의 견고한 투구
					createNewItem3(pc, 21098, 1, 5); // 수련자의 견고한 망토
					createNewItem3(pc, 21104, 1, 7); // 수련자의 견고한 갑옷
					createNewItem3(pc, 21106, 1, 7); // 수련자의 견고한 부츠
					
					createNewItem3(pc, 500220, 1, 5); // 수련자의 각반
					
					createNewItem3(pc, 21100, 1, 0); // 수련자의 반지
					createNewItem3(pc, 21100, 1, 0); // 수련자의 반지
					createNewItem3(pc, 21101, 1, 0); // 수련자의 목걸이
					createNewItem3(pc, 21099, 1, 0); // 수련자의 벨트
					createNewItem3(pc, 420010, 1, 0); // 수련자의 귀걸이
					pc.sendPackets(new S_SystemMessage(pc, "전사 보급품이 지급 되었습니다."));
				}
				if (pc.isKnight()) {
					createNewItem(pc, 60134, 10); // 수련자의 용기의 물약
					
					createNewItem(pc, 61072, 10); // 수련자의 한우 스테이크
					createNewItem(pc, 61075, 10); // 수련자의 닭고기 스프
					
					createNewItem3(pc, 61, 1, 0); // 진명황의 집행검
					
					createNewItem3(pc, 20082, 1, 5); // 수련자의 티셔츠
					createNewItem3(pc, 21103, 1, 7); // 수련자의 견고한 장갑
					createNewItem3(pc, 21105, 1, 7); // 수련자의 견고한 투구
					createNewItem3(pc, 21098, 1, 5); // 수련자의 견고한 망토
					createNewItem3(pc, 21104, 1, 7); // 수련자의 견고한 갑옷
					createNewItem3(pc, 21106, 1, 7); // 수련자의 견고한 부츠
					
					createNewItem3(pc, 500220, 1, 4); // 수련자의 각반
					
					createNewItem3(pc, 21100, 1, 0); // 수련자의 반지
					createNewItem3(pc, 21100, 1, 0); // 수련자의 반지
					createNewItem3(pc, 21101, 1, 0); // 수련자의 목걸이
					createNewItem3(pc, 21099, 1, 0); // 수련자의 벨트
					createNewItem3(pc, 420010, 1, 0); // 수련자의 귀걸이
					pc.sendPackets(new S_SystemMessage(pc, "기사 보급품이 지급 되었습니다."));
				}
				if (pc.isDragonknight()) {
					createNewItem(pc, 60140, 20); // 수련자의 각인의 뼈조각
					
					createNewItem(pc, 61072, 10); // 수련자의 한우 스테이크
					createNewItem(pc, 61075, 10); // 수련자의 닭고기 스프
					
					createNewItem3(pc, 9101, 1, 0); // 크로노스의 공포
					
					createNewItem3(pc, 20082, 1, 5); // 수련자의 티셔츠
					createNewItem3(pc, 21103, 1, 7); // 수련자의 견고한 장갑
					createNewItem3(pc, 21105, 1, 7); // 수련자의 견고한 투구
					createNewItem3(pc, 21098, 1, 5); // 수련자의 견고한 망토
					createNewItem3(pc, 21104, 1, 7); // 수련자의 견고한 갑옷
					createNewItem3(pc, 21106, 1, 7); // 수련자의 견고한 부츠
					
					createNewItem3(pc, 500220, 1, 4); // 수련자의 각반
					
					createNewItem3(pc, 21100, 1, 0); // 수련자의 반지
					createNewItem3(pc, 21100, 1, 0); // 수련자의 반지
					createNewItem3(pc, 21101, 1, 0); // 수련자의 목걸이
					createNewItem3(pc, 21099, 1, 0); // 수련자의 벨트
					createNewItem3(pc, 420010, 1, 0); // 수련자의 귀걸이
					pc.sendPackets(new S_SystemMessage("용기사 보급품이 지급 되었습니다."));
				}
				if (pc.isCrown()) {
					createNewItem(pc, 60133, 10); // 수련자의 악마의 피
					
					createNewItem(pc, 61072, 10); // 수련자의 한우 스테이크
					createNewItem(pc, 61075, 10); // 수련자의 닭고기 스프
					
					createNewItem3(pc, 12, 1, 0); // 바람칼날의 단검
					createNewItem3(pc, 21102, 1, 6); // 수련자의 견고한 방패
					
					createNewItem3(pc, 20082, 1, 5); // 수련자의 티셔츠
					createNewItem3(pc, 21103, 1, 7); // 수련자의 견고한 장갑
					createNewItem3(pc, 21105, 1, 7); // 수련자의 견고한 투구
					createNewItem3(pc, 21098, 1, 5); // 수련자의 견고한 망토
					createNewItem3(pc, 21104, 1, 7); // 수련자의 견고한 갑옷
					createNewItem3(pc, 21106, 1, 7); // 수련자의 견고한 부츠
					
					createNewItem3(pc, 500220, 1, 5); // 수련자의 각반
					
					createNewItem3(pc, 21100, 1, 0); // 수련자의 반지
					createNewItem3(pc, 21100, 1, 0); // 수련자의 반지
					createNewItem3(pc, 21101, 1, 0); // 수련자의 목걸이
					createNewItem3(pc, 21099, 1, 0); // 수련자의 벨트
					createNewItem3(pc, 420010, 1, 0); // 수련자의 귀걸이
					pc.sendPackets(new S_SystemMessage("군주 보급품이 지급 되었습니다."));
				}
				if (pc.isDarkelf()) {
					createNewItem(pc, 60139, 30); // 흑요석
					
					createNewItem(pc, 61072, 10); // 수련자의 한우 스테이크
					createNewItem(pc, 61075, 10); // 수련자의 닭고기 스프
					
					createNewItem3(pc, 86, 1, 0); // 붉은그림자의 이도류
					
					createNewItem3(pc, 20082, 1, 5); // 수련자의 티셔츠
					createNewItem3(pc, 21103, 1, 7); // 수련자의 견고한 장갑
					createNewItem3(pc, 21105, 1, 7); // 수련자의 견고한 투구
					createNewItem3(pc, 21098, 1, 5); // 수련자의 견고한 망토
					createNewItem3(pc, 21104, 1, 7); // 수련자의 견고한 갑옷
					createNewItem3(pc, 21106, 1, 7); // 수련자의 견고한 부츠
					
					createNewItem3(pc, 500220, 1, 5); // 수련자의 각반
					
					createNewItem3(pc, 21100, 1, 0); // 수련자의 반지
					createNewItem3(pc, 21100, 1, 0); // 수련자의 반지
					createNewItem3(pc, 21101, 1, 0); // 수련자의 목걸이
					createNewItem3(pc, 21099, 1, 0); // 수련자의 벨트
					createNewItem3(pc, 420010, 1, 0); // 수련자의 귀걸이
					pc.sendPackets(new S_SystemMessage("다크엘프 보급품이 지급 되었습니다."));
				}
				if (pc.isWizard()) {
					createNewItem(pc, 60138, 30); // 수련자의 마력의 돌
					
					createNewItem(pc, 61074, 10); // 수련자의 칠면조 구이
					createNewItem(pc, 61075, 10); // 수련자의 닭고기 스프
					
					createNewItem3(pc, 134, 1, 0); // 수정결정체지팡이
					createNewItem3(pc, 21254, 1, 6); // 수련자의 신성한 방패

					createNewItem3(pc, 20082, 1, 5); // 수련자의 티셔츠
					createNewItem3(pc, 21108, 1, 7); // 수련자의 신성한 장갑
					createNewItem3(pc, 21109, 1, 7); // 수련자의 신성한 투구
					createNewItem3(pc, 21112, 1, 5); // 수련자의 신성한 망토
					createNewItem3(pc, 21111, 1, 7); // 수련자의 신성한 갑옷
					createNewItem3(pc, 21110, 1, 7); // 수련자의 신성한 부츠
					
					createNewItem3(pc, 500220, 1, 5); // 수련자의 각반
					
					createNewItem3(pc, 21100, 1, 0); // 수련자의 반지
					createNewItem3(pc, 21100, 1, 0); // 수련자의 반지
					createNewItem3(pc, 21101, 1, 0); // 수련자의 목걸이
					createNewItem3(pc, 21099, 1, 0); // 수련자의 벨트
					createNewItem3(pc, 420010, 1, 0); // 수련자의 귀걸이
					pc.sendPackets(new S_SystemMessage("마법사 보급품이 지급 되었습니다."));
				}
				if (pc.isIllusionist()) {
					createNewItem(pc, 60136, 10); // 수련자의 유그드라열매
					
					createNewItem(pc, 61074, 10); // 수련자의 칠면조 구이
					createNewItem(pc, 61075, 10); // 수련자의 닭고기 스프
					
					createNewItem3(pc, 9102, 1, 0); // 수련자의 도끼

					createNewItem3(pc, 20082, 1, 5); // 수련자의 티셔츠
					createNewItem3(pc, 21108, 1, 7); // 수련자의 신성한 장갑
					createNewItem3(pc, 21109, 1, 7); // 수련자의 신성한 투구
					createNewItem3(pc, 21112, 1, 5); // 수련자의 신성한 망토
					createNewItem3(pc, 21111, 1, 7); // 수련자의 신성한 갑옷
					createNewItem3(pc, 21110, 1, 7); // 수련자의 신성한 부츠
					
					createNewItem3(pc, 500220, 1, 5); // 수련자의 각반
					
					createNewItem3(pc, 21100, 1, 0); // 수련자의 반지
					createNewItem3(pc, 21100, 1, 0); // 수련자의 반지
					createNewItem3(pc, 21101, 1, 0); // 수련자의 목걸이
					createNewItem3(pc, 21099, 1, 0); // 수련자의 벨트
					createNewItem3(pc, 420010, 1, 0); // 수련자의 귀걸이
					pc.sendPackets(new S_SystemMessage("환술사 보급품이 지급 되었습니다."));
				}
				if (pc.isElf()) {
					createNewItem(pc, 60135, 10); // 수련자의 엘븐 와퍼
					
					createNewItem(pc, 61073, 10); // 수련자의 연어 찜
					createNewItem(pc, 61075, 10); // 수련자의 닭고기 스프
					
					createNewItem(pc, 60137, 30); // 수련자의 정령옥
					
					createNewItem(pc, 8601, 1000); // 사냥꾼의 은 화살
					
					createNewItem3(pc, 9100, 1, 0); // 가이아의 격노

					createNewItem3(pc, 20082, 1, 5); // 수련자의 티셔츠
					createNewItem3(pc, 21108, 1, 7); // 수련자의 신성한 장갑
					createNewItem3(pc, 21109, 1, 7); // 수련자의 신성한 투구
					createNewItem3(pc, 21112, 1, 5); // 수련자의 신성한 망토
					createNewItem3(pc, 21111, 1, 7); // 수련자의 신성한 갑옷
					createNewItem3(pc, 21110, 1, 7); // 수련자의 신성한 부츠
					
					createNewItem3(pc, 500220, 1, 5); // 수련자의 각반
					
					createNewItem3(pc, 21100, 1, 0); // 수련자의 반지
					createNewItem3(pc, 21100, 1, 0); // 수련자의 반지
					createNewItem3(pc, 21101, 1, 0); // 수련자의 목걸이
					createNewItem3(pc, 21099, 1, 0); // 수련자의 벨트
					createNewItem3(pc, 420010, 1, 0); // 수련자의 귀걸이
					pc.sendPackets(new S_SystemMessage("요정 보급품이 지급 되었습니다."));
				}
				
		
			
		/*	if (pc.getInventory().getSize() >= 50) {
					pc.sendPackets(new S_SystemMessage("아이템 사용 실패 : 인벤토리의 갯수를 줄인후 사용해주세요."));
					return;
				}
				if (pc.getInventory().calcWeightpercent() >= 50) {
					pc.sendPackets(new S_SystemMessage("아이템 사용 실패 : 무게 게이지 90% 이상 사용 불가"));
					return;
				}
				pc.setRingSlotLevel(1);
				pc.setShoulder70SlotLevel(1);
				pc.setEarringSlotLevel(1);
				pc.sendPackets(new S_ReturnedStat(S_ReturnedStat.RING_RUNE_SLOT, S_ReturnedStat.SUBTYPE_RING, 16));
				pc.sendPackets(new S_ReturnedStat(S_ReturnedStat.RING_RUNE_SLOT, S_ReturnedStat.SUBTYPE_RING, 128));
				pc.sendPackets(new S_ReturnedStat(S_ReturnedStat.RING_RUNE_SLOT, S_ReturnedStat.SUBTYPE_RING, pc.getRingSlotLevel()));

	
				
				L1Teleport.teleport(pc, 33432, 32800, (short) 4, 6, true);
				
				pc.sendPackets(new S_CloseList(pc.getId()), true);
				
				if(pc.getLevel() < 70){
				pc.addExp((ExpTable.getExpByLevel(70) - 1) - pc.getExp() + ((ExpTable.getExpByLevel(70) - 1) / 100)); // 아크 프리패스 모드
				}
							
				createNewItem(pc, 40308, 300000);
				
				createNewItem(pc, 40021, 300); // 고대의 체력 회복제
				createNewItem(pc, 40024, 100); // 고대의 체력 회복제
				createNewItem(pc, 550000, 10); // 강화 속도 향상 물약
				createNewItem(pc, 55004, 10); // 마력 회복 물약
				createNewItem(pc, 41246, 1000); // 결정체
				createNewItem(pc, 60360, 10);
				createNewItem(pc, 40126, 10);
				createNewItem(pc, 40081, 20);
				createNewItem(pc, 40100, 100);
				createNewItem(pc, 40088, 10);
				createNewItem(pc, 140100, 10);
				createNewItem(pc, 430005, 1);
				
				createNewItem(pc, 60080, 1);
				createNewItem(pc, 60381, 1);
				createNewItem(pc, 60382, 1);
				createNewItem(pc, 500209, 35);
			//	createNewItem(pc, 5560, 5);
			//	createNewItem(pc, 6130, 1);
			//	createNewItem(pc, 600225, 1);
				
				if (pc.isWarrior()) {
					createNewItem(pc, 40014, 10); // 용기의 물약
					
					createNewItem3(pc, 72290, 1, 8); // 강철 도끼
					createNewItem3(pc, 72290, 1, 8); // 강철 도끼
					
					createNewItem3(pc, 20085, 1, 5); // 티셔츠
					createNewItem3(pc, 20168, 1, 7); // 무관의 장갑
					createNewItem3(pc, 20020, 1, 7); // 무관의 투구
					createNewItem3(pc, 20058, 1, 5); // 무관의 망토
					createNewItem3(pc, 20113, 1, 7); // 무관의 갑옷
					createNewItem3(pc, 20201, 1, 7); // 무관의 부츠
					
					createNewItem3(pc, 500220, 1, 4); // 철각반
					
					createNewItem3(pc, 20280, 1, 0); // 멸마의 반지
					createNewItem3(pc, 20280, 1, 0); // 멸마의 반지
					createNewItem3(pc, 20264, 1, 0); // 완력의 목걸이
					createNewItem3(pc, 20309, 1, 0); // 빛나는 신체의 벨트
					createNewItem3(pc, 21013, 1, 0); // 관용의 귀걸이
					pc.sendPackets(new S_SystemMessage(pc, "전사 보급품이 지급 되었습니다."));
				}
				if (pc.isKnight()) {
					createNewItem(pc, 40014, 10); // 용기의 물약
					
					createNewItem3(pc, 56, 1, 8); // 데스 블레이드
					createNewItem3(pc, 20231, 1, 5); // 사각 방패
					
					createNewItem3(pc, 20085, 1, 5); // 티셔츠
					createNewItem3(pc, 20168, 1, 7); // 무관의 장갑
					createNewItem3(pc, 20020, 1, 7); // 무관의 투구
					createNewItem3(pc, 20058, 1, 5); // 무관의 망토
					createNewItem3(pc, 20113, 1, 7); // 무관의 갑옷
					createNewItem3(pc, 20201, 1, 7); // 무관의 부츠
					
					createNewItem3(pc, 500220, 1, 4); // 철각반
					
					createNewItem3(pc, 20280, 1, 0); // 멸마의 반지
					createNewItem3(pc, 20280, 1, 0); // 멸마의 반지
					createNewItem3(pc, 20264, 1, 0); // 완력의 목걸이
					createNewItem3(pc, 20309, 1, 0); // 빛나는 신체의 벨트
					createNewItem3(pc, 21013, 1, 0); // 관용의 귀걸이
					pc.sendPackets(new S_SystemMessage(pc, "기사 보급품이 지급 되었습니다."));
				}
				if (pc.isDragonknight()) {
					createNewItem(pc, 430007, 20); // 각인의 뼈조각
					
					createNewItem3(pc, 410000, 1, 8); // 소멸자의 체인소드
					createNewItem3(pc, 420001, 1, 2); // 용비늘 가더
					
					createNewItem3(pc, 20085, 1, 5); // 티셔츠
					createNewItem3(pc, 20168, 1, 7); // 무관의 장갑
					createNewItem3(pc, 20020, 1, 7); // 무관의 투구
					createNewItem3(pc, 20058, 1, 5); // 무관의 망토
					createNewItem3(pc, 20113, 1, 7); // 무관의 갑옷
					createNewItem3(pc, 20201, 1, 7); // 무관의 부츠
					
					createNewItem3(pc, 500220, 1, 4); // 철각반
					
					createNewItem3(pc, 20280, 1, 0); // 멸마의 반지
					createNewItem3(pc, 20280, 1, 0); // 멸마의 반지
					createNewItem3(pc, 20264, 1, 0); // 완력의 목걸이
					createNewItem3(pc, 20309, 1, 0); // 빛나는 신체의 벨트
					createNewItem3(pc, 21013, 1, 0); // 관용의 귀걸이
					pc.sendPackets(new S_SystemMessage("용기사 보급품이 지급 되었습니다."));
				}
				if (pc.isCrown()) {
					createNewItem(pc, 40031, 10); // 악마의 피
					
					
					createNewItem3(pc, 51, 1, 8); // 황금 지휘봉
					createNewItem3(pc, 20229, 1, 5); // 반사 방패
					
					createNewItem3(pc, 20085, 1, 5); // 티셔츠
					createNewItem3(pc, 20168, 1, 7); // 무관의 장갑
					createNewItem3(pc, 20020, 1, 7); // 무관의 투구
					createNewItem3(pc, 20058, 1, 5); // 무관의 망토
					createNewItem3(pc, 20113, 1, 7); // 무관의 갑옷
					createNewItem3(pc, 20201, 1, 7); // 무관의 부츠
					
					createNewItem3(pc, 500220, 1, 4); // 철각반
					
					createNewItem3(pc, 20280, 1, 0); // 멸마의 반지
					createNewItem3(pc, 20280, 1, 0); // 멸마의 반지
					createNewItem3(pc, 20264, 1, 0); // 완력의 목걸이
					createNewItem3(pc, 20309, 1, 0); // 빛나는 신체의 벨트
					createNewItem3(pc, 21013, 1, 0); // 관용의 귀걸이
					pc.sendPackets(new S_SystemMessage("군주 보급품이 지급 되었습니다."));
				}
				if (pc.isWizard()) {
					createNewItem(pc, 40318, 30); // 마력의 돌
					
					createNewItem3(pc, 126, 1, 8); // 마나의 지팡이

					createNewItem3(pc, 20085, 1, 5); // 캠프 티셔츠
					createNewItem3(pc, 20176, 1, 7); // 신관의 장갑
					createNewItem3(pc, 20030, 1, 7); // 신관의 투구
					createNewItem3(pc, 20067, 1, 5); // 신관의 망토
					createNewItem3(pc, 20129, 1, 7); // 신관의 갑옷
					createNewItem3(pc, 20208, 1, 7); // 신관의 부츠
					createNewItem3(pc, 20233, 1, 7); // 신관의 마력서
					
					createNewItem3(pc, 500220, 1, 4); // 철각반

					createNewItem3(pc, 20280, 1, 0); // 멸마의 반지
					createNewItem3(pc, 20280, 1, 0); // 멸마의 반지
					createNewItem3(pc, 20266, 1, 0); // 지식의 목걸이
					createNewItem3(pc, 20311, 1, 0); // 빛나는 정신의 벨트
					createNewItem3(pc, 21017, 1, 0); // 지배의 귀걸이
					pc.sendPackets(new S_SystemMessage("마법사 보급품이 지급 되었습니다."));
				}
				if (pc.isIllusionist()) {
					createNewItem(pc, 430006, 10); // 유그드라열매
					
					createNewItem3(pc, 410003, 1, 8); // 사파이어 키링크

					createNewItem3(pc, 20085, 1, 5); // 캠프 티셔츠
					createNewItem3(pc, 20176, 1, 7); // 신관의 장갑
					createNewItem3(pc, 20030, 1, 7); // 신관의 투구
					createNewItem3(pc, 20067, 1, 5); // 신관의 망토
					createNewItem3(pc, 20129, 1, 7); // 신관의 갑옷
					createNewItem3(pc, 20208, 1, 7); // 신관의 부츠
					createNewItem3(pc, 20233, 1, 7); // 신관의 마력서
					
					createNewItem3(pc, 500220, 1, 4); // 철각반

					createNewItem3(pc, 20280, 1, 0); // 멸마의 반지
					createNewItem3(pc, 20280, 1, 0); // 멸마의 반지
					createNewItem3(pc, 20266, 1, 0); // 지식의 목걸이
					createNewItem3(pc, 20311, 1, 0); // 빛나는 정신의 벨트
					createNewItem3(pc, 21017, 1, 0); // 지배의 귀걸이
					pc.sendPackets(new S_SystemMessage("환술사 보급품이 지급 되었습니다."));
				}
				if (pc.isElf()) {
					createNewItem(pc, 40068, 10); // 엘븐 와퍼
					createNewItem(pc, 8601, 1000); // 사냥꾼의 은 화살
					
					createNewItem3(pc, 184, 1, 8); // 화염의 활

					createNewItem3(pc, 20084, 1, 7); // 요정족 티셔츠
					createNewItem3(pc, 20176, 1, 7); // 신관의 장갑
					createNewItem3(pc, 20030, 1, 7); // 신관의 투구
					createNewItem3(pc, 20067, 1, 5); // 신관의 망토
					createNewItem3(pc, 20129, 1, 7); // 신관의 갑옷
					createNewItem3(pc, 20208, 1, 7); // 신관의 부츠
					createNewItem3(pc, 420000, 1, 2); // 고대 명궁 가더
					
					createNewItem3(pc, 500220, 1, 4); // 철각반

					createNewItem3(pc, 20280, 1, 0); // 멸마의 반지
					createNewItem3(pc, 20280, 1, 0); // 멸마의 반지
					createNewItem3(pc, 20256, 1, 0); // 민첩의 목걸이
					createNewItem3(pc, 20310, 1, 0); // 빛나는 영혼의 벨트
					createNewItem3(pc, 21011, 1, 0); // 불사의 귀걸이
					pc.sendPackets(new S_SystemMessage("요정 보급품이 지급 되었습니다."));
				}
				if (pc.isDarkelf()) {
					createNewItem(pc, 40321, 30); // 흑요석
					
					createNewItem3(pc, 13, 1, 8); // 핑거오브데스
					
					createNewItem3(pc, 20085, 1, 5); // 티셔츠
					createNewItem3(pc, 20168, 1, 7); // 무관의 장갑
					createNewItem3(pc, 20020, 1, 7); // 무관의 투구
					createNewItem3(pc, 20058, 1, 5); // 무관의 망토
					createNewItem3(pc, 20113, 1, 7); // 무관의 갑옷
					createNewItem3(pc, 20201, 1, 7); // 무관의 부츠
					createNewItem3(pc, 420003, 1, 2); // 고대 투사 가더
					
					createNewItem3(pc, 500220, 1, 4); // 철각반
					
					createNewItem3(pc, 20280, 1, 0); // 멸마의 반지
					createNewItem3(pc, 20280, 1, 0); // 멸마의 반지
					createNewItem3(pc, 20264, 1, 0); // 완력의 목걸이
					createNewItem3(pc, 20309, 1, 0); // 빛나는 신체의 벨트
					createNewItem3(pc, 21013, 1, 0); // 관용의 귀걸이
					pc.sendPackets(new S_SystemMessage("다크엘프 보급품이 지급 되었습니다."));
				}*/
		/*		
				if (pc.getInventory().getSize() >= 50) {
					pc.sendPackets(new S_SystemMessage("아이템 사용 실패 : 인벤토리의 갯수를 줄인후 사용해주세요."));
					return;
				}
				if (pc.getInventory().calcWeightpercent() >= 50) {
					pc.sendPackets(new S_SystemMessage("아이템 사용 실패 : 무게 게이지 90% 이상 사용 불가"));
					return;
				}
				
				L1Teleport.teleport(pc, 33432, 32800, (short) 4, 6, true);
				
				pc.sendPackets(new S_CloseList(pc.getId()), true);
				
				pc.addExp((ExpTable.getExpByLevel(80) - 1) - pc.getExp() + ((ExpTable.getExpByLevel(80) - 1) / 100)); // 군터 프리패스 모드
	
				createNewItem(pc, 40021, 300); // 고대의 체력 회복제
				createNewItem(pc, 40024, 100); // 고대의 체력 회복제
				createNewItem(pc, 550000, 10); // 강화 속도 향상 물약
				createNewItem(pc, 55004, 10); // 마력 회복 물약
				createNewItem(pc, 41246, 1000); // 결정체
				createNewItem(pc, 60360, 10);
				createNewItem(pc, 40126, 10);
				createNewItem(pc, 40081, 20);
				createNewItem(pc, 40100, 100);
				createNewItem(pc, 40088, 10);
				createNewItem(pc, 140100, 10);
				createNewItem(pc, 40308, 300000);
				createNewItem(pc, 430005, 1);
				createNewItem(pc, 60080, 1);
				createNewItem(pc, 60381, 1);
				createNewItem(pc, 60382, 1);
				createNewItem(pc, 500209, 1);
			//	createNewItem(pc, 5560, 5);
			//	createNewItem(pc, 6130, 1);
			//	createNewItem(pc, 600225, 1);
				
				if (pc.isWarrior()) {
					createNewItem(pc, 40014, 10); // 용기의 물약
				    createNewItem3(pc, 7227, 1, 7, 129, 3);// 혈풍 도끼
					createNewItem3(pc, 7227, 1, 7, 129, 3);// 혈풍 도끼
					createNewItem3(pc, 9204, 1, 7, 129, 0); // 멸마의 판금 갑옷
					createNewItem3(pc, 7247, 1, 7, 129, 0); // 신성한 마법방어 투구
					createNewItem3(pc, 20074, 1, 7, 129, 0); // 신성보망
					createNewItem3(pc, 20085, 1, 8, 129, 0); // 티셔츠
					createNewItem3(pc, 21259, 1, 7, 129, 0); // 완력의 부츠
					createNewItem3(pc, 30219, 1, 7, 129, 0); // 돌 장갑
					createNewItem3(pc, 500214, 1, 7, 129, 0); // 마법방어 각반
					createNewItem3(pc, 9207, 1, 4, 129, 0); // 신성한 완력의 목걸이
					createNewItem3(pc, 502010, 1, 4, 129, 0); // 축검귀
					createNewItem3(pc, 502007, 1, 4, 129, 0); // 축붉귀
					createNewItem3(pc, 9410, 1, 0, 129, 0); // 신성한 보호벨트
					createNewItem3(pc, 21252, 1, 4, 129, 0); // 축복받은 스냅퍼의 마저반지
					createNewItem3(pc, 21252, 1, 4, 129, 0); // 축복받은 스냅퍼의 마저반지
					pc.sendPackets(new S_SystemMessage("[프리 패스] 전사전용 아이템이 지급 되었습니다."));
				}
				if (pc.isKnight()) {
					createNewItem(pc, 40014, 10); // 용기의 물약
				    createNewItem3(pc, 9, 1, 9, 129, 3);// 오리하루콘 단검
					createNewItem3(pc, 9106, 1, 8, 129, 3);// 진싸
					createNewItem3(pc, 21093, 1, 7, 129, 0);// 반역자방패
					createNewItem3(pc, 9204, 1, 7, 129, 0); // 신요판
					createNewItem3(pc, 220011, 1, 7, 129, 0); // 신성한 마법방어 투구
					createNewItem3(pc, 20074, 1, 7, 129, 0); // 신성보망
					createNewItem3(pc, 20085, 1, 8, 129, 0); // 티셔츠
					createNewItem3(pc, 21259, 1, 7, 129, 0); // 완력의 부츠
					createNewItem3(pc, 30219, 1, 7, 129, 0); // 돌 장갑
					createNewItem3(pc, 500214, 1, 7, 129, 0); // 마법방어 각반
					createNewItem3(pc, 9207, 1, 4, 129, 0); // 신성한 완력의 목걸이
					createNewItem3(pc, 502010, 1, 4, 129, 0); // 축검귀
					createNewItem3(pc, 502007, 1, 4, 129, 0); // 축붉귀
					createNewItem3(pc, 9410, 1, 0, 129, 0); // 신성한 보호벨트
					createNewItem3(pc, 21252, 1, 4, 129, 0); // 축복받은 스냅퍼의 마저반지
					createNewItem3(pc, 21252, 1, 4, 129, 0); // 축복받은 스냅퍼의 마저반지
					
					pc.sendPackets(new S_SystemMessage("[프리 패스] 기사전용 아이템이 지급 되었습니다."));
				}
				if (pc.isDragonknight()) {
					createNewItem(pc, 430007, 20); // 각인의 뼈조각
				    createNewItem3(pc, 6000, 1, 7, 129, 3);// 극한 체인소드
					createNewItem3(pc, 420001, 1, 5, 129, 0); // 용비늘가더
					createNewItem3(pc, 6004, 1, 6, 129, 0); // 극한 갑옷
					createNewItem3(pc, 6003, 1, 6, 129, 0); // 극한 투구
					createNewItem3(pc, 20074, 1, 7, 129, 0); // 신성보망
					createNewItem3(pc, 20085, 1, 8, 129, 0); // 티셔츠
					createNewItem3(pc, 6005, 1, 6, 129, 0); // 극한부츠
					createNewItem3(pc, 30219, 1, 7, 129, 0); // 돌 장갑
					createNewItem3(pc, 500214, 1, 7, 129, 0); // 마법방어 각반
					createNewItem3(pc, 9207, 1, 4, 129, 0); // 신성한 완력의 목걸이
					createNewItem3(pc, 502010, 1, 4, 129, 0); // 축검귀
					createNewItem3(pc, 502007, 1, 4, 129, 0); // 축붉귀
					createNewItem3(pc, 9410, 1, 0, 129, 0); // 신성한 보호벨트
					createNewItem3(pc, 21252, 1, 4, 129, 0); // 축복받은 스냅퍼의 마저반지
					createNewItem3(pc, 21252, 1, 4, 129, 0); // 축복받은 스냅퍼의 마저반지
					pc.sendPackets(new S_SystemMessage("[프리 패스] 용기사전용 아이템이 지급 되었습니다."));
				}
				if (pc.isCrown()) {
					createNewItem(pc, 40031, 10); // 악마의 피
					
				    createNewItem3(pc, 9, 1, 9, 129, 3);// 오리하루콘 단검
					createNewItem3(pc, 51, 1, 8, 129, 3);// 황금 지휘봉
					createNewItem3(pc, 21093, 1, 7, 129, 0);// 반역자방패
					createNewItem3(pc, 6004, 1, 6, 129, 0); // 극한 갑옷
					createNewItem3(pc, 6003, 1, 6, 129, 0); // 극한 투구
					createNewItem3(pc, 20074, 1, 7, 129, 0); // 신성보망
					createNewItem3(pc, 20085, 1, 8, 129, 0); // 티셔츠
					createNewItem3(pc, 6005, 1, 6, 129, 0); // 극한부츠
					createNewItem3(pc, 30219, 1, 7, 129, 0); // 돌 장갑
					createNewItem3(pc, 500214, 1, 7, 129, 0); // 마법방어 각반
					createNewItem3(pc, 9207, 1, 4, 129, 0); // 신성한 완력의 목걸이
					createNewItem3(pc, 502010, 1, 4, 129, 0); // 축검귀
					createNewItem3(pc, 502007, 1, 4, 129, 0); // 축붉귀
					createNewItem3(pc, 9410, 1, 0, 129, 0); // 신성한 보호벨트
					createNewItem3(pc, 21252, 1, 4, 129, 0); // 축복받은 스냅퍼의 마저반지
					createNewItem3(pc, 21252, 1, 4, 129, 0); // 축복받은 스냅퍼의 마저반지
					pc.sendPackets(new S_SystemMessage("[프리 패스] 군주전용 아이템이 지급 되었습니다."));
				}
				
				if (pc.isWizard()) {
					createNewItem(pc, 40318, 30); // 마력의 돌
				    createNewItem3(pc, 127, 1, 9, 129, 3);// 강철 마나의 지팡이
					createNewItem3(pc, 119, 1, 0, 129, 3);// 데몬의 지팡이
					
					createNewItem3(pc, 20160, 1, 5, 129, 0); // 흑장로의 로브
					createNewItem3(pc, 220011, 1, 7, 129, 0); // 신성한 마법방어 투구
					createNewItem3(pc, 20074, 1, 7, 129, 0); // 신성보망
					createNewItem3(pc, 20085, 1, 8, 129, 0); // 티셔츠
					createNewItem3(pc, 20218, 1, 5, 129, 0); // 흑장로의 샌달
					createNewItem3(pc, 30219, 1, 7, 129, 0); // 돌 장갑
					createNewItem3(pc, 500214, 1, 7, 129, 0); // 마법방어 각반
					createNewItem3(pc, 9208, 1, 4, 129, 0); // 신성한 지식의 목걸이
					createNewItem3(pc, 502009, 1, 4, 129, 0); // 축보귀
					createNewItem3(pc, 502007, 1, 4, 129, 0); // 축붉귀
					createNewItem3(pc, 9410, 1, 0, 129, 0); // 신성한 보호벨트
					createNewItem3(pc, 21252, 1, 4, 129, 0); // 축복받은 스냅퍼의 마저반지
					createNewItem3(pc, 21252, 1, 4, 129, 0); // 축복받은 스냅퍼의 마저반지
					pc.sendPackets(new S_SystemMessage("[프리 패스] 마법사전용 아이템이 지급 되었습니다."));
				}
				if (pc.isIllusionist()) {
					createNewItem(pc, 430006, 10); // 유그드라열매
				    createNewItem3(pc, 6001, 1, 7, 129, 3);// 냉한의 키링크
					createNewItem3(pc, 20160, 1, 5, 129, 0); // 흑장로의 로브
					createNewItem3(pc, 220011, 1, 7, 129, 0); // 신성한 마법방어 투구
					createNewItem3(pc, 20074, 1, 7, 129, 0); // 신성보망
					createNewItem3(pc, 20085, 1, 8, 129, 0); // 티셔츠
					createNewItem3(pc, 21266, 1, 7, 129, 0); // 지식의 부츠
					createNewItem3(pc, 30219, 1, 7, 129, 0); // 돌 장갑
					createNewItem3(pc, 500214, 1, 7, 129, 0); // 마법방어 각반
					createNewItem3(pc, 9208, 1, 4, 129, 0); // 신성한 지식의 목걸이
					createNewItem3(pc, 502009, 1, 4, 129, 0); // 축검귀
					createNewItem3(pc, 502007, 1, 4, 129, 0); // 축붉귀
					createNewItem3(pc, 9410, 1, 0, 129, 0); // 신성한 보호벨트
					createNewItem3(pc, 21252, 1, 4, 129, 0); // 축복받은 스냅퍼의 마저반지
					createNewItem3(pc, 21252, 1, 4, 129, 0); // 축복받은 스냅퍼의 마저반지
					
					pc.sendPackets(new S_SystemMessage("[프리 패스] 환술사전용 아이템이 지급 되었습니다."));
				}
				if (pc.isElf()) {
					createNewItem(pc, 40068, 10); // 엘븐 와퍼
					createNewItem(pc, 8602, 1000); // 화살
				    createNewItem3(pc, 189, 1, 8, 129, 3);// 흑왕궁
					createNewItem3(pc, 9204, 1, 7, 129, 0); // 신요판
					createNewItem3(pc, 220011, 1, 7, 129, 0); // 신성한 마법방어 투구
					createNewItem3(pc, 20074, 1, 7, 129, 0); // 신성보망
					createNewItem3(pc, 20085, 1, 8, 129, 0); // 티셔츠
					createNewItem3(pc, 21265, 1, 7, 129, 0); // 민첩의 부츠
					createNewItem3(pc, 30219, 1, 7, 129, 0); // 돌 장갑
					createNewItem3(pc, 500214, 1, 7, 129, 0); // 마법방어 각반
					createNewItem3(pc, 9209, 1, 4, 129, 0); // 신성한 민첩의 목걸이
					createNewItem3(pc, 502010, 1, 4, 129, 0); // 축검귀
					createNewItem3(pc, 502007, 1, 4, 129, 0); // 축붉귀
					createNewItem3(pc, 9410, 1, 0, 129, 0); // 신성한 보호벨트
					createNewItem3(pc, 21252, 1, 4, 129, 0); // 축복받은 스냅퍼의 마저반지
					createNewItem3(pc, 21252, 1, 4, 129, 0); // 축복받은 스냅퍼의 마저반지

					pc.sendPackets(new S_SystemMessage("[프리 패스] 요정전용 아이템이 지급 되었습니다."));
				}
				if (pc.isDarkelf()) {
					createNewItem(pc, 40321, 30); // 흑요석
				    createNewItem3(pc, 76, 1, 8, 129, 3);// 론드의 이도류
					createNewItem3(pc, 9204, 1, 7, 129, 0); // 신요판
					createNewItem3(pc, 220011, 1, 7, 129, 0); // 신성한 마법방어 투구
					createNewItem3(pc, 20074, 1, 7, 129, 0); // 신성보망
					createNewItem3(pc, 20085, 1, 8, 129, 0); // 티셔츠
					createNewItem3(pc, 21259, 1, 7, 129, 0); // 완력의 부츠
					createNewItem3(pc, 30219, 1, 7, 129, 0); // 돌 장갑
					createNewItem3(pc, 500214, 1, 7, 129, 0); // 마법방어 각반
					createNewItem3(pc, 9207, 1, 4, 129, 0); // 신성한 완력의 목걸이
					createNewItem3(pc, 502010, 1, 4, 129, 0); // 축검귀
					createNewItem3(pc, 502007, 1, 4, 129, 0); // 축붉귀
					createNewItem3(pc, 9410, 1, 0, 129, 0); // 신성한 보호벨트
					createNewItem3(pc, 21252, 1, 4, 129, 0); // 축복받은 스냅퍼의 마저반지
					createNewItem3(pc, 21252, 1, 4, 129, 0); // 축복받은 스냅퍼의 마저반지
					pc.sendPackets(new S_SystemMessage("[프리 패스] 다크엘프전용 아이템이 지급 되었습니다."));
				}*/
				try{
					pc.save();
					pc.saveInventory();
				} catch (Exception e) {
					e.printStackTrace();
				}
				S_EffectLocation packet = new S_EffectLocation(pc.getX(), pc.getY(), 13561);
				pc.sendPackets(packet);
		//		pc.sendPackets(new S_NPCTalkReturn(pc.getId(),"clending"));
				//pc.setLawful(32767);
				pc.getQuest().set_end(L1Quest.QUEST_LEVEL1);
			}
		
		break;

	
		case 4212015: { // 드래곤 포탈
			// pc.system=1;
			pc.dragonmapid = (short) moveMapId;
			// pc.sendPackets(new S_Message_YN(622,
			// "안타라스 드래곤 포탈에 진입 하시겠습니까?(Y/N)"), true);
			pc.system = -1;// 초기화
			int count = 0;
			int trcount = 0;
			AntarasRaid ar = AntarasRaidSystem.getInstance().getAR(pc.dragonmapid);
			int count1 = ar.countLairUser();
			if(pc.getLevel() < Config.RAID_LEVEL){
				pc.sendPackets(new S_SystemMessage("입장 불가: "+Config.RAID_LEVEL+"레벨 이상 입장가능"), true);
				return;
			}
			if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.DRAGONRAID_BUFF)) {
				pc.sendPackets(new S_SystemMessage("드래곤 레이드 마법으로 인해 드래곤 포탈에 입장 할 수 없습니다."));
				pc.sendPackets(new S_SystemMessage(ss.format(pc.getNetConnection().getAccount().getDragonRaid()) + " 이후에 입장 가능합니다."), true);
				return;
			}
			if (count1 > 0) {
				for (L1PcInstance player : L1World.getInstance().getAllPlayers()) {
					if (player.getMapId() == pc.dragonmapid) {
						trcount++;
					}
				}
				if (trcount == 0) {
					for (L1Object npc : L1World.getInstance().getObject()) {
						if (npc instanceof L1MonsterInstance) {
							if (npc.getMapId() == pc.dragonmapid) {
								L1MonsterInstance _npc = (L1MonsterInstance) npc;
								_npc.deleteMe();
							}
						}
					}
					ar.clLairUser();
					ar.setAntaras(false);
					ar.setanta(null);
				}
			}
			for (L1PcInstance player : L1World.getInstance().getAllPlayers()) {
				if (player.getMapId() == pc.dragonmapid) {
					count += 1;
					if (count > 16) {
						pc.sendPackets(new S_SystemMessage("입장 가능 인원수를 초과 하였습니다."));
						return;
					}
				}
			}
			L1Teleport.teleport(pc, 32668, 32675, (short) pc.dragonmapid, 5, true);
		}
			break;
		case 4212016: { // 드래곤 포탈
			// pc.system =2 ;
			pc.dragonmapid = (short) moveMapId;
			// pc.sendPackets(new S_Message_YN(622,
			// "파푸리온 드래곤 포탈에 진입 하시겠습니까?(Y/N)"), true);
			pc.system = -1;// 초기화
			int count = 0;
			int trcount = 0;
			PaPooRaid ar = PaPooRaidSystem.getInstance().getAR(pc.dragonmapid);
			int count1 = ar.countLairUser();
			if(pc.getLevel() < Config.RAID_LEVEL){
				pc.sendPackets(new S_SystemMessage("입장 불가: "+Config.RAID_LEVEL+"레벨 이상 입장가능"), true);
				return;
			}
			if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.DRAGONRAID_BUFF)) {
				pc.sendPackets(new S_SystemMessage("드래곤 레이드 마법으로 인해 드래곤 포탈에 입장 할 수 없습니다."));
				pc.sendPackets(new S_SystemMessage(ss.format(pc.getNetConnection().getAccount().getDragonRaid()) + " 이후에 입장 가능합니다."), true);
				return;
			}
			if (count1 > 0) {
				for (L1PcInstance player : L1World.getInstance().getAllPlayers()) {
					if (player.getMapId() == pc.dragonmapid) {
						trcount++;
					}
				}
				if (trcount == 0) {
					for (L1Object npc : L1World.getInstance().getObject()) {
						if (npc instanceof L1MonsterInstance) {
							if (npc.getMapId() == pc.dragonmapid) {
								L1MonsterInstance _npc = (L1MonsterInstance) npc;
								_npc.deleteMe();
							}
						}
					}
					ar.clLairUser();
					ar.setPapoo(false);
					ar.setPaPoo(null);
				}
			}
			for (L1PcInstance player : L1World.getInstance().getAllPlayers()) {
				if (player.getMapId() == pc.dragonmapid) {
					count += 1;
					if (count > 16) {
						pc.sendPackets(new S_SystemMessage("입장 가능 인원수를 초과 하였습니다."));
						return;
					}
				}
			}
			L1Teleport.teleport(pc, 32920, 32672, (short) pc.dragonmapid, 5, true);
		}
			break;
		case 4500102: // 안타 대기방 -> 안타레어
			안타레이드시작(pc, moveMapId);
			break;
		case 4500107: // 파푸 대기방 -> 파푸레어
			파푸레이드시작(pc, moveMapId);
			break;

		case 4500101: // 안타시작->동굴
			tel4room(pc, moveMapId);
			break;
		case 4500103: // 파푸시작 -> 동굴
			tel4room2(pc, moveMapId);
			break;
		case 100011: { // 드래곤 포탈 (린드)
			pc.dragonmapid = (short) moveMapId;
			pc.system = -1;// 초기화
			if(pc.getLevel() < Config.RAID_LEVEL){
				pc.sendPackets(new S_SystemMessage("입장 불가: "+Config.RAID_LEVEL+"레벨 이상 입장가능"), true);
				return;
			}
			if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.DRAGONRAID_BUFF)) {
				pc.sendPackets(new S_SystemMessage("드래곤 레이드 마법으로 인해 드래곤 포탈에 입장 할 수 없습니다."));
				pc.sendPackets(new S_SystemMessage(ss.format(pc.getNetConnection().getAccount().getDragonRaid()) + " 이후에 입장 가능합니다."), true);
				return;
			}
			int count = 0;
			for (L1PcInstance player : L1World.getInstance().getAllPlayers()) {
				if (player.getMapId() == pc.dragonmapid) {
					count += 1;
					if (count > 16) {
						pc.sendPackets(new S_SystemMessage("입장 가능 인원수를 초과 하였습니다."));
						return;
					}
				}
			}
			LindRaid.get().in(pc);
		}
			break;
		case 100010: // 린드 레어입구 -> 린드레어
			LindRare_in(pc);
			break;
			/** 발라 리뉴얼 **/
		case 4038060:  // 드래곤 포탈 (발라)
			pc.dragonmapid = (short) moveMapId;
			pc.system = -1;// 초기화
			if(pc.getLevel() < Config.RAID_LEVEL){
				pc.sendPackets(new S_SystemMessage("입장 불가: "+Config.RAID_LEVEL+"레벨 이상 입장가능"), true);
				return;
			}
			if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.DRAGONRAID_BUFF)) {
				pc.sendPackets(new S_SystemMessage("드래곤 레이드 마법으로 인해 드래곤 포탈에 입장 할 수 없습니다."));
				pc.sendPackets(new S_SystemMessage(ss.format(pc.getNetConnection().getAccount().getDragonRaid()) + " 이후에 입장 가능합니다."), true);
				return;
			} else {
				pc.dx = 32733;
				pc.dy = 32927;
				pc.dm = (short) pc.dragonmapid;
				pc.dh = 4;
				pc.setTelType(7);
				pc.sendPackets(new S_SabuTell(pc));
			}
				break;
		case 4038061:
			ValaRaid vala = ValaRaidSystem.getInstance().getVala(pc.dragonmapid);
			if (!vala.isReady()){
				vala.setReady(true);
				vala.Start();
			}
			pc.dx = 32761;
			pc.dy = 32885;
			pc.dm = (short) pc.dragonmapid;
			pc.dh = 4;
			pc.setTelType(7);
			pc.sendPackets(new S_SabuTell(pc));
			break;
		default:
			break;
		}
	}

	private synchronized void LindRare_in(L1PcInstance pc) {
		Lind lind = LindThread.get().getLind(getMapId());
		if (lind == null)
			return;
		if (lind.Step > 1) {
			pc.sendPackets(new S_ServerMessage(1537), true);// 드래곤이 깨서 진입 못한다
			return;
		}
		pc.dx = 32846;
		pc.dy = 32886;
		pc.dm = (short) getMapId();
		pc.dh = 5;
		pc.setTelType(7);
		pc.sendPackets(new S_SabuTell(pc), true);
		lind.Step = 1;
	}

	@Override
	public void deleteMe() {
		_destroyed = true;
		if (getInventory() != null) {
			getInventory().clearItems();
		}
		L1World.getInstance().removeVisibleObject(this);
		L1World.getInstance().removeObject(this);
		ArrayList<L1PcInstance> list = null;
		list = L1World.getInstance().getRecognizePlayer(this);
		for (L1PcInstance pc : list) {
			if (pc == null)
				continue;
			pc.getNearObjects().removeKnownObject(this);
			pc.sendPackets(new S_RemoveObject(this), true);
		}
		getNearObjects().removeAllKnownObjects();
	}

	/**
	 * 지정된 맵의 32명이 넘는지 체크해서 텔시킨다
	 * 
	 * @param pc
	 * @param mapid
	 */
	private synchronized void 안타레이드시작(L1PcInstance pc, int mapid) {
		int count = 0;
		AntarasRaid ar = AntarasRaidSystem.getInstance().getAR(mapid);
		count = ar.countLairUser();
		if (ar.isAntaras()) {
			pc.sendPackets(new S_ServerMessage(1537));// 드래곤이 깨서 진입 못한다
			return;
		} else if (count == 0) {
			for (L1Object npc : L1World.getInstance().getObject()) {
				if (npc instanceof L1MonsterInstance) {
					if (npc.getMapId() == mapid) {
						L1MonsterInstance _npc = (L1MonsterInstance) npc;
						_npc.deleteMe();
					}
				}
			}
			ar.clLairUser();
			ar.addLairUser(pc);
			ar.setAntaras(false);
			ar.setanta(null);
			if (ar.art != null) {
				ar.art.cancel();
				ar.art = null;
			}
			AntarasRaidTimer antastart = new AntarasRaidTimer(null, ar, 5, 60000);// 1분 체크
			antastart.begin();
			// 몹소환 안타 깨어나서 접근금지.
			AntarasRaidTimer antaendtime = new AntarasRaidTimer(null, ar, 6, 2700000);// 45분 체크
			ar.art = antaendtime;
			antaendtime.begin();
			// 안타 잡기 실패 모두 텔.
		}
		ar.addLairUser(pc);
		pc.dx = 32796;
		pc.dy = 32664;
		pc.dm = (short) mapid;
		pc.dh = 5;
		pc.setTelType(7);
		pc.sendPackets(new S_SabuTell(pc), true);
	}

	/**
	 * 지정된 맵의 32명이 넘는지 체크해서 텔시킨다
	 * 
	 * @param pc
	 * @param mapid
	 */
	private synchronized void 파푸레이드시작(L1PcInstance pc, int mapid) {

		int count = 0;
		PaPooRaid ar = PaPooRaidSystem.getInstance().getAR(mapid);
		count = ar.countLairUser();
		if (ar.isPaPoo()) {
			pc.sendPackets(new S_ServerMessage(1537));// 드래곤이 깨서 진입 못한다
			return;
		} else if (count == 0) {
			for (L1Object npc : L1World.getInstance().getObject()) {
				if (npc instanceof L1MonsterInstance) {
					if (npc.getMapId() == mapid) {
						L1MonsterInstance _npc = (L1MonsterInstance) npc;
						_npc.deleteMe();
					}
				}
			}
			ar.clLairUser();
			ar.addLairUser(pc);
			ar.setPapoo(false);
			ar.setPaPoo(null);

			PaPooTimer antastart = new PaPooTimer(null, ar, 4, 60000);// 1분  체크
			antastart.begin();
			// 몹소환 안타 깨어나서 접근금지.
			PaPooTimer antaendtime = new PaPooTimer(null, ar, 0, 2700000);// 45분 체크
			antaendtime.begin();
			// 안타 잡기 실패 모두 텔.
		}
		ar.addLairUser(pc);
		pc.dx = 32989;
		pc.dy = 32843;
		pc.dm = (short) mapid;
		pc.dh = 5;
		pc.setTelType(7);
		pc.sendPackets(new S_SabuTell(pc), true);
	}

	/**
	 * 이동할 맵을 설정한다.
	 * 
	 * @param id
	 */
	public void setMoveMapId(int id) {
		moveMapId = id;
	}

	/***/
	private void tel4room(L1PcInstance pc, int mapid) {
		/*
		 * L1Party pcpt = pc.getParty(); if(pcpt == null){ pc.sendPackets(new
		 * S_SystemMessage("당신은 파티 중이 아닙니다."), true); return; }
		 * if(pcpt.getLeader().getName() != pc.getName()){ pc.sendPackets(new
		 * S_SystemMessage("파티장만 입장을 할 수 있습니다."), true); return; }
		 */
		pc.system = 3;
		pc.dragonmapid = (short) moveMapId;
		pc.sendPackets(new S_Message_YN(622, "안타라스 단계형 동굴에 진입 하시겠습니까?(Y/N)"),
				true);
	}

	private void tel4room2(L1PcInstance pc, int mapid) {
		/*
		 * L1Party pcpt = pc.getParty(); if(pcpt == null){ pc.sendPackets(new
		 * S_SystemMessage("당신은 파티 중이 아닙니다."), true); return; }
		 * if(pcpt.getLeader().getName() != pc.getName()){ pc.sendPackets(new
		 * S_SystemMessage("파티장만 입장을 할 수 있습니다."), true); return; }
		 */
		pc.system = 4;
		pc.dragonmapid = (short) moveMapId;
		pc.sendPackets(new S_Message_YN(622, "파푸리온 단계형 동굴에 진입 하시겠습니까?(Y/N)"),
				true);
	}

	class 개미지옥 implements Runnable {

		@Override
		public void run() {
			try {

				// TODO 자동 생성된 메소드 스텁
				if (개미지옥몹스폰가능) { // 개미굴
					for (L1PcInstance pc : L1World.getInstance()
							.getVisiblePlayer(L1FieldObjectInstance.this, 1)) {
						if ((Math.abs(getX() - pc.getX()) + Math.abs(getY()
								- pc.getY())) <= 1) {

							개미지옥몹스폰();
						}
					}
					GeneralThreadPool.getInstance().schedule(this, 2000);
				} else
					GeneralThreadPool.getInstance().schedule(this, 60000);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	public void 개미지옥몹스폰() {
		if (!개미지옥몹스폰가능)
			return;
		개미지옥몹스폰가능 = false;
		L1SpawnUtil.spawn2(getX(), getY(), getMapId(), 100343, 2, 0, 0);
		L1SpawnUtil.spawn2(getX(), getY(), getMapId(), 100344, 2, 0, 0);
		GeneralThreadPool.getInstance().schedule(new Runnable() {
			@Override
			public void run() {
				// TODO 자동 생성된 메소드 스텁
				개미지옥몹스폰가능 = true;
			}

		}, 60000 * 10);
	}
	private boolean createNewItem(L1PcInstance pc, int item_id, int count) {
		L1ItemInstance item = ItemTable.getInstance().createItem(item_id);
		if (item != null) {
			item.setCount(count);
			if (pc.getInventory().checkAddItem(item, count) == L1Inventory.OK) {
				pc.getInventory().storeItem(item);
			} else { // 가질 수 없는 경우는 지면에 떨어뜨리는 처리의 캔슬은 하지 않는다(부정 방지)
				L1World.getInstance().getInventory(pc.getX(), pc.getY(), pc.getMapId()).storeItem(item);
			}
			pc.sendPackets(new S_ServerMessage(403, item.getLogName()), true); // %0를
																				// 손에
																				// 넣었습니다.
			return true;
		} else {
			return false;
		}
	}
	private boolean createNewItem3(L1PcInstance pc, int item_id, int count, int EnchantLevel) {
		return createNewItem3(pc, item_id, count, EnchantLevel, 1, 0);
	}
	
	private boolean createNewItem3(L1PcInstance pc, int item_id, int count, int EnchantLevel, int bless, int SpiritIn) {
		
		L1ItemInstance item = ItemTable.getInstance().createItem(item_id);

			item.setCount(count);
			item.setEnchantLevel(EnchantLevel);
			item.setIdentified(true);
			item.setRegistLevel(SpiritIn);
			item.setLock(1);
			if (item != null) {
				if (pc.getInventory().checkAddItem(item, count) == L1Inventory.OK) {
					pc.getInventory().storeItem(item);
					item.setBless(bless);
					pc.getInventory().updateItem(item, L1PcInventory.COL_BLESS);
					pc.getInventory().saveItem(item, L1PcInventory.COL_BLESS);
				} else { // 가질 수 없는 경우는 지면에 떨어뜨리는 처리의 캔슬은 하지 않는다(부정 방지)
					L1World.getInstance().getInventory(pc.getX(), pc.getY(), pc.getMapId()).storeItem(item);
				}
			
			pc.sendPackets(new S_ServerMessage(403, item.getLogName()), true); // %0를 손에 넣었습니다.
			return true;
		} else {
			return false;
		}
	}
}
