/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package l1j.server.server.model.Instance;

import java.util.Random;

import l1j.server.server.ObjectIdFactory;
import l1j.server.server.model.L1World;
import l1j.server.server.serverpackets.S_WorldPutObject;
import l1j.server.server.templates.L1Npc;

public class L1LittleBugInstance extends L1NpcInstance {
	private static final long serialVersionUID = 1L;

	private static final Random _random = new Random();

	private static final int FIRST_NAMEID = 1213;
	public String[] Name = { "ī��", "���", "����", "���", "��ī", "����", "���", "����", "����", "�Ƹ�", "�þ�", "����", "����", "��ġ", "�ٰ�", "�ھ�", "����", "���", "����", "���" };
	private static final int[] gfxId = { 16081, 16082, 16083, 16084, 16085, 16086, 16087, 16088, 16089, 16090, 16091, 16092, 16093, 16094, 16095, 16096, 16097, 16098, 16099, 16100 };

	public static final int GOOD = 0;
	public static final int NORMAL = 1;
	public static final int BAD = 2;

	// private static final int[] gfxId = { 5919, 6096, 6100, 6443, 6449, 6452,
	// 6477, 6480, 8650, 7047, 7050, 7053, 7257, 8539, 8549, 6483, 8741, 8745,
	// 8751, 10256};

	public L1LittleBugInstance(L1Npc template, int num, int x, int y) {
		super(template);
		setId(ObjectIdFactory.getInstance().nextId());
		setName("$" + Name[num]);
		setNameId("#" + (num + 1) + " $" + Name[num]);
		gfx.setTempCharGfx(gfxId[num]);
		gfx.setGfxId(gfxId[num]);
		setLocation(x, y, 4);
		getMoveState().setHeading(6);
		setCondition(_random.nextInt(3));
		setNumber(num);
		getMoveState().setMoveSpeed(1); // �ͱ�
		L1World.getInstance().storeObject(this);
		L1World.getInstance().addVisibleObject(this);
		for (L1PcInstance pc : L1World.getInstance().getRecognizePlayer(this)) {
			onPerceive(pc);
		}
	}

	@Override
	public void onPerceive(L1PcInstance perceivedFrom) {
		perceivedFrom.getNearObjects().addKnownObject(this);
		perceivedFrom.sendPackets(new S_WorldPutObject(this), true);
		//perceivedFrom.sendPackets(new S_NPCPack(this), true);
	}

	private int _number; // ������ȣ

	public void setNumber(int i) {
		_number = i;
	}

	public int getNumber() {
		return _number;
	}

	private int _rain; // ���ι�ȣ

	public void setRainNum(int i) {
		_rain = i;
	}

	public int getRainNum() {
		return _rain;
	}

	private int _condition; // ����

	public void setCondition(int i) {
		_condition = i;
	}

	public int getCondition() {
		return _condition;
	}

	private int _win; // �¸� Ƚ��

	public void setWin(int i) {
		_win = i;
	}

	public int getWin() {
		return _win;
	}

	private int _lose; // �� Ƚ��

	public void setLose(int i) {
		_lose = i;
	}

	public int getLose() {
		return _lose;
	}

	private String _winPoint; // �·�

	public void setWinPoint(String i) {
		_winPoint = i;
	}

	public String getWinPoint() {
		return _winPoint;
	}

	private double _dividend; // ���

	public void setDividend(double i) {
		_dividend = i;
	}

	public double getDividend() {
		return _dividend;
	}

}
