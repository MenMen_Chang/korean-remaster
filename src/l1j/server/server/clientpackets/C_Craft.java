package l1j.server.server.clientpackets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Logger;

import kr.PeterMonk.GameSystem.SupportSystem.L1SupportMap;
import kr.PeterMonk.GameSystem.SupportSystem.SupportMapTable;
import l1j.server.Config;
import l1j.server.server.ActionCodes;
import l1j.server.server.datatables.ItemTable;
import l1j.server.server.datatables.MonsterBookTable;
import l1j.server.server.model.Broadcaster;
import l1j.server.server.model.L1Cooking;
import l1j.server.server.model.L1PolyMorph;
import l1j.server.server.model.L1Teleport;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1DollInstance;
import l1j.server.server.model.Instance.L1ItemInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.model.Instance.L1PetInstance;
import l1j.server.server.model.skill.L1SkillId;
import l1j.server.server.serverpackets.S_ChangeShape;
import l1j.server.server.serverpackets.S_CharVisualUpdate;
import l1j.server.server.serverpackets.S_Disconnect;
import l1j.server.server.serverpackets.S_DoActionGFX;
import l1j.server.server.serverpackets.S_DoActionShop;
import l1j.server.server.serverpackets.S_MonsterUi;
import l1j.server.server.serverpackets.S_PacketBox;
import l1j.server.server.serverpackets.S_Paralysis;
import l1j.server.server.serverpackets.S_ServerMessage;
import l1j.server.server.serverpackets.S_ServerVersion;
import l1j.server.server.serverpackets.S_SupportSystem;
import l1j.server.server.serverpackets.S_SystemMessage;
import l1j.server.server.templates.L1PrivateShopBuyList;
import l1j.server.server.templates.L1PrivateShopSellList;
import server.LineageClient;
import server.message.ServerMessage;

public class C_Craft extends ClientBasePacket {

	private static Logger _log = Logger.getLogger(C_Craft.class.getName());


	private static final int MONSTER_CLEAR 			= 0x33;
	private static final int MONSTERBOOK_TEL 		= 530; // 몬스터 북 텔레포트

	public C_Craft(byte[] data, LineageClient client) throws IOException {
		super(data);
		if (client == null) {
			return;
		}
		L1PcInstance pc = client.getActiveChar();
		int type = readC();
		
		switch (type) {
		case 52:
			client.sendPacket(new S_ServerVersion());
			break;
		case 49:
			if (pc == null) {
				return;
			}
			if (pc.isGhost() || pc.isDead()) {
				return;
			}
			if (pc.isInvisble()) {
				pc.sendPackets(new S_ServerMessage(755), true);
				return;
			}
			int mapId = pc.getMapId();
			if (mapId != 800) {
				pc.sendPackets(new S_ServerMessage(876), true);
				return;
			}

		/*	if(pc.TamTime() > 0){
				pc.sendPackets(new S_SystemMessage(pc,"개설 불가: 탐 열매 복용중. [/상점]으로 해제"));
				break;
			}*/
			if(Config.STANDBY_SERVER ){
				pc.sendPackets(new S_SystemMessage(pc, "오픈대기중에는 사용할 수 없습니다."), true);
				pc.sendPackets(new S_DoActionGFX(pc.getId(),ActionCodes.ACTION_Idle), true);
				Broadcaster.broadcastPacket(pc, new S_DoActionGFX(pc.getId(),ActionCodes.ACTION_Idle), true);
				break;
			}
			
			if (Config.ALT_PRIVATE_SHOP_LEVEL != 0 && pc.getLevel() < Config.ALT_PRIVATE_SHOP_LEVEL) {
				pc.sendPackets(new S_SystemMessage(pc, "무인상점은 " + Config.ALT_PRIVATE_SHOP_LEVEL + " 레벨이상 만 가능합니다."), true);
				return;
			}
			
			if (!pc.getInventory().checkItem(40308, 1000)) {
				pc.sendPackets(new S_SystemMessage(pc,"상점 개설에 필요한 아데나가 부족합니다."));
				return;
			}
			
			for (L1PcInstance target : L1World.getInstance().getAllPlayers()) {
				if (target.getId() != pc.getId() && target.getAccountName().toLowerCase().equals(pc.getAccountName().toLowerCase())&& target.isPrivateShop()) {
					pc.sendPackets(new S_SystemMessage("무인상점은 한개의 캐릭터만 가능합니다."));
					return;
				}
			}

			ArrayList<L1PrivateShopSellList> sellList = pc.getSellList();
			ArrayList<L1PrivateShopBuyList> buyList = pc.getBuyList();
			L1ItemInstance checkItem;
			boolean tradable = true;
			
			readC();
			int length = readBit();
			
			readC();
			readC();
			int shoptype = readC(); // 0 or 1

			if (shoptype == 0) { // 개시
				int sellTotalCount = 0;
				int buyTotalCount = 0;
				int sellObjectId = 0;
				int sellPrice = 0;
				int sellCount = 0;
				L1ItemInstance sellitem = null;
				Object[] petlist = null;
				L1ItemInstance buyitem = null;
				for (int i = 0; i < length; i++) {
					int code = readC();
					if(code == 0x12 || code == 0x1a){
						int l1 = readC();
						for(int i2=0;i2<3;i2++ ){
							int code2 = readC();
							if(code2 == 0x08)
								sellObjectId = readBit();
							else if(code2 == 0x10)
								sellPrice = readBit();
							else if(code2 == 0x18)
								sellCount = readBit();							
						}
						// 거래 가능한 아이템이나 체크
						checkItem = pc.getInventory().getItem(sellObjectId);

						if (checkItem == null) {
							continue;
						}

						if (sellObjectId != checkItem.getId()) {
							tradable = false;
							pc.sendPackets(new S_SystemMessage("비정상 아이템 입니다. 다시 시도해주세요."), true);
						}
						if (!checkItem.isStackable() && sellCount != 1) {
							tradable = false;
							pc.sendPackets(new S_SystemMessage("비정상 아이템 입니다. 다시 시도해주세요."), true);
						}
						if (sellCount > checkItem.getCount()) {
							sellCount = checkItem.getCount();
						}
						if (checkItem.getCount() < sellCount || checkItem.getCount() <= 0 || sellCount <= 0) {
							tradable = false;
							pc.sendPackets(new S_SystemMessage("비정상 아이템 입니다. 다시 시도해주세요."), true);
						}
						if (checkItem.getBless() >= 128) {
							tradable = false;
							pc.sendPackets(new S_ServerMessage(ServerMessage.CANNOT_DROP_OR_TRADE, checkItem.getItem().getName()));
							// return;
						}
						L1DollInstance 인형 = null;
						for (Object 인형오브젝트 : pc.getDollList()) {
							if (인형오브젝트 instanceof L1DollInstance) {
								인형 = (L1DollInstance) 인형오브젝트;
								if (checkItem.getId() == 인형.getItemObjId()) {
									// \f1%0은 버리거나 또는 타인에게 양일을 할 수 없습니다.
									tradable = false;
									pc.sendPackets(new S_SystemMessage("소환중인 인형은 상점에 올릴 수 없습니다."), true);
									// return;
								}
							}
						}
						
						if (!checkItem.getItem().isTradable()) {
							tradable = false;
							pc.sendPackets(new S_ServerMessage(166, checkItem.getItem().getName(), "거래 불가능합니다. "), true);
						}
						
						if (checkItem.getLock() != 0) {
							tradable = false;
							pc.sendPackets(new S_SystemMessage("각인된 아이템은 거래할 수 없습니다."), true);
						}

						for (Object petObject : pc.getPetList()) {
							if (petObject instanceof L1PetInstance) {
								L1PetInstance pet = (L1PetInstance) petObject;
								if (checkItem.getId() == pet.getItemObjId()) {
									tradable = false;
									pc.sendPackets(new S_ServerMessage(166, checkItem.getItem().getName(), "거래 불가능합니다. "), true);
									break;
								}
							}
						}
						if(code == 0x12){
							if(sellTotalCount > 7){ 
								pc.sendPackets(new S_SystemMessage("물품등록은 8개까지만 가능합니다.")); 
								return;
							}
							
							L1PrivateShopSellList pssl = new L1PrivateShopSellList();
							pssl.setItemObjectId(sellObjectId);
							pssl.setSellPrice(sellPrice);
							pssl.setSellTotalCount(sellCount);
							sellList.add(pssl);
							sellTotalCount++;
						}else if(code == 0x1a){
							if(buyTotalCount > 7){ 
								pc.sendPackets(new S_SystemMessage("물품등록은 8개까지만 가능합니다.")); 
								return;
							}
							L1PrivateShopBuyList psbl = new L1PrivateShopBuyList();
							psbl.setItemObjectId(sellObjectId);
							psbl.setBuyPrice(sellPrice);
							psbl.setBuyTotalCount(sellCount);
							buyList.add(psbl);
							buyTotalCount++;
						}
					}else{
						break;
					}
				}

				if (buyTotalCount > 0) {
					try {
						pc.상점아이템삭제(pc.getId());
					} catch (Exception e) {
						e.printStackTrace();
					}
					pc.sendPackets(new S_SystemMessage("일시적으로 구입 물품을 등록할 수 없습니다."), true);
					sellList.clear();
					buyList.clear();
					pc.setPrivateShop(false);
					pc.sendPackets(new S_DoActionGFX(pc.getId(),ActionCodes.ACTION_Idle), true);
					Broadcaster.broadcastPacket(pc, new S_DoActionGFX(pc.getId(),ActionCodes.ACTION_Idle), true);
					return;
				}

				if (sellTotalCount == 0 && buyTotalCount == 0) {
					pc.sendPackets(new S_ServerMessage(908), true);
					pc.setPrivateShop(false);
					pc.sendPackets(new S_DoActionGFX(pc.getId(), ActionCodes.ACTION_Idle), true);
					Broadcaster.broadcastPacket(pc, new S_DoActionGFX(pc.getId(), ActionCodes.ACTION_Idle), true);
					return;
				}

				if (!tradable) { // 거래 불가능한 아이템이 포함되어 있는 경우, 개인 상점 종료
					sellList.clear();
					buyList.clear();
					pc.setPrivateShop(false);
					pc.sendPackets(new S_DoActionGFX(pc.getId(), ActionCodes.ACTION_Idle), true);
					Broadcaster.broadcastPacket(pc, new S_DoActionGFX(pc.getId(), ActionCodes.ACTION_Idle), true);
					return;
				}
				int l1 = readC();
		        byte[] chat = readByte(l1); 
		        
		        readC();
		        int l2 = readC();
		        String polynum = readS(l2); 
				
				pc.setShopChat(chat);
				
				pc.setPrivateShop(true);
				
				pc.sendPackets(new S_DoActionShop(pc.getId(), ActionCodes.ACTION_Shop, chat), true);
				Broadcaster.broadcastPacket(pc, new S_DoActionShop(pc.getId(), ActionCodes.ACTION_Shop, chat), true);

				try {
					for (L1PrivateShopSellList pss : pc.getSellList()) {
						int sellp = pss.getSellPrice();
						int sellc = pss.getSellTotalCount();
						sellitem = pc.getInventory().getItem(pss.getItemObjectId());
						if (sellitem == null)
							continue;
						pc.SaveShop(pc, sellitem, sellp, sellc, 1);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}

				try {
					for (L1PrivateShopBuyList psb : pc.getBuyList()) {
						int buyp = psb.getBuyPrice();
						int buyc = psb.getBuyTotalCount();
						buyitem = pc.getInventory().getItem(psb.getItemObjectId());
						if (buyitem == null)
							continue;
						pc.SaveShop(pc, buyitem, buyp, buyc, 0);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}

				try {
					int polyId = 0;
					if (polynum.equalsIgnoreCase("tradezone1"))
						polyId = 11326;
					else if (polynum.equalsIgnoreCase("tradezone2"))
						polyId = 11427;
					else if (polynum.equalsIgnoreCase("tradezone3"))
						polyId = 10047;
					else if (polynum.equalsIgnoreCase("tradezone4"))
						polyId = 9688;
					else if (polynum.equalsIgnoreCase("tradezone5"))
						polyId = 11322;
					else if (polynum.equalsIgnoreCase("tradezone6"))
						polyId = 10069;
					else if (polynum.equalsIgnoreCase("tradezone7"))
						polyId = 10034;
					else if (polynum.equalsIgnoreCase("tradezone8"))
						polyId = 10032;

					if (polyId != 0) {
						pc.getSkillEffectTimerSet().killSkillEffectTimer(L1SkillId.SHAPE_CHANGE);
						pc.getSkillEffectTimerSet().killSkillEffectTimer(L1SkillId.SHAPE_CHANGE_JIBAE);
						L1PolyMorph.undoPoly(pc);
						L1ItemInstance weapon = pc.getWeapon();
						if (weapon != null)
							pc.getInventory().setEquipped(weapon, false, false, false);
						pc.getGfxId().setTempCharGfx(polyId);
						pc.sendPackets(new S_ChangeShape(pc.getId(), polyId, pc.getCurrentWeapon()));
						if (!pc.isGmInvis() && !pc.isInvisble()) {
							Broadcaster.broadcastPacket(pc, new S_ChangeShape(pc.getId(), polyId));
						}
						S_CharVisualUpdate charVisual = new S_CharVisualUpdate(pc, 0x46);
						pc.sendPackets(charVisual);
						Broadcaster.broadcastPacket(pc, charVisual);
					}
					pc.getInventory().consumeItem(40308, 1000);
					pc.sendPackets(new S_SystemMessage("[.무인상점] 입력시 자동리스 후 상점 유지"));
					pc.sendPackets(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, 
							"\\f7[.무인상점] 커맨드 입력시 캐릭터 리스 후 개인상점이 유지됩니다."));
				} catch (Exception e) {
					pc.상점아이템삭제(pc.getId());
					sellList.clear();
					buyList.clear();
					pc.setPrivateShop(false);
					pc.sendPackets(new S_DoActionGFX(pc.getId(), ActionCodes.ACTION_Idle), true);
					Broadcaster.broadcastPacket(pc, new S_DoActionGFX(pc.getId(), ActionCodes.ACTION_Idle), true);
					return;
				}
				petlist = null;
			} else if (shoptype == 1) { // 종료
				if (isTwoLogin(pc)) {
					pc.sendPackets(new S_Disconnect());
				}
				sellList.clear();
				buyList.clear();
				pc.setPrivateShop(false);
				int classId = pc.getClassId();
				pc.getGfxId().setTempCharGfx(classId);
				pc.sendPackets(new S_ChangeShape(pc.getId(), classId));
				Broadcaster.broadcastPacket(pc, new S_ChangeShape(pc.getId(), classId));
				L1ItemInstance weapon = pc.getWeapon();
				if (weapon != null)
				pc.getInventory().setEquipped(weapon, false, false, false);
				S_CharVisualUpdate charVisual = new S_CharVisualUpdate(pc);
				pc.sendPackets(charVisual);
				Broadcaster.broadcastPacket(pc, charVisual);
				try {
					pc.상점아이템삭제(pc.getId());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			break;
		case MONSTER_CLEAR: {
			readH();
			readH();
			int monNum = read4(read_size());
			int value = 0;
			if (monNum == 1)
				value = 1;
			else if (monNum == 2)
				value = 2;
			if (monNum >= 3) {
				value = monNum % 3;
			}
			switch (value) {//도감 1~3단계별로 아이템지급
			case 1:
				pc.addExp(50000);
				L1Cooking.newEatCooking(pc, L1SkillId.천하장사버프, 1800);
				break;
			case 2:
				pc.getInventory().storeItem(40308, 5000);
				pc.addExp(500000);
				break;
			case 3:
				pc.getInventory().storeItem(5548, 1);
				pc.addExp(5000000);
				break;
				}
			pc.sendPackets(new S_MonsterUi(S_MonsterUi.MONSTER_END, monNum));
			MonsterBookTable.getInstace().setMon_Quest(pc.getId(), monNum, 1);
			MonsterBookTable.getInstace().saveMonsterQuest(pc.getId());
			}
			break;
		/*case MONSTERBOOK_TEL:
			readH();
			readH();
			int monsternumber = read4(read_size()) / 3 + 1;
			MonsterBookTable Mui_gi = MonsterBookTable.getInstace();
			int mn = Mui_gi.getMonNum(monsternumber);
			if(pc.getMapId() == 6202){
				pc.sendPackets(new S_SystemMessage(pc,"이 곳에서는 사용할 수 없습니다."), true);
				return;
			}
			if (mn != 0) {
				int itemId = Mui_gi.getMarterial(monsternumber);
				String itemName = ItemTable.getInstance().findItemIdByName(itemId);
				if (itemName != null) {
					int locx = Mui_gi.getLocX(monsternumber);
					int locy = Mui_gi.getLocY(monsternumber);
					int mapid = Mui_gi.getMapId(monsternumber);
					if (pc.getMap().isEscapable()) {
						if (pc.getInventory().consumeItem(itemId, 1)){
							pc.sendPackets(new S_SystemMessage("5초후 텔레포트됩니다."), true);
							L1Teleport.teleport(pc, locx, locy, (short) mapid, 5, true);
						}else {
							pc.sendPackets(new S_ServerMessage(4692, itemName));
							return;
						}
					} else {
						pc.sendPackets(new S_Paralysis(S_Paralysis.TYPE_TELEPORT_UNLOCK, false));
						pc.sendPackets(new S_ServerMessage(4726));
					}
				}
			}
			break;*/
		default:
			break;
		}
	}
	private boolean isTwoLogin(L1PcInstance c) {
		boolean bool = false;
		L1PcInstance[] list = L1World.getInstance().getAllPlayersToArray();
		for (L1PcInstance target : list) {
			// 무인PC 는 제외
			if (target == null)
				continue;
			if (target.noPlayerCK)
				continue;
			if (target.샌드백)
				continue;
			//
			if (c.getId() != target.getId() && !target.isPrivateShop()) {
				if (c.getNetConnection() != null
						&& target.getNetConnection() != null) {
					if (c.getNetConnection()
							.getAccountName()
							.equalsIgnoreCase(
									target.getNetConnection().getAccountName())) {
						bool = true;
						break;
					}
				}
			}
		}
		list = null;
		return bool;
	}

	public String getType() {
		return "[C] C_Craft";
	}
}
