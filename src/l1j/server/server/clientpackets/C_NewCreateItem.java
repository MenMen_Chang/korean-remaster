/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */

package l1j.server.server.clientpackets;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import kr.PeterMonk.ClanBuffSystem.ClanBuffTable;
import kr.PeterMonk.ClanBuffSystem.S_EinhasadClanBuff;
import kr.PeterMonk.ClanBuffSystem.ClanBuffTable.ClanBuff;
import kr.PeterMonk.GameSystem.SupportSystem.L1SupportMap;
import kr.PeterMonk.GameSystem.SupportSystem.SupportMapTable;
import l1j.server.Config;
import l1j.server.L1DatabaseFactory;
import l1j.server.GameSystem.UserRanking.UserRankingController;
import l1j.server.server.TimeController.InvSwapController;
import l1j.server.server.TimeController.WarTimeController;
import l1j.server.server.datatables.ClanTable;
import l1j.server.server.datatables.EnchantWeaponChance;
import l1j.server.server.datatables.ExcludeLetterTable;
import l1j.server.server.datatables.ExcludeTable;
import l1j.server.server.datatables.ItemTable;
import l1j.server.server.datatables.NpcActionTable;
import l1j.server.server.datatables.PetTable;
import l1j.server.server.datatables.PetsSkillsTable;
import l1j.server.server.model.Broadcaster;
import l1j.server.server.model.L1CastleLocation;
import l1j.server.server.model.L1Clan;
import l1j.server.server.model.L1ExcludingLetterList;
import l1j.server.server.model.L1ExcludingList;
import l1j.server.server.model.L1Inventory;
import l1j.server.server.model.L1Object;
import l1j.server.server.model.L1Teleport;
import l1j.server.server.model.L1War;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1ItemInstance;
import l1j.server.server.model.Instance.L1NpcInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.model.Instance.L1PetInstance;
import l1j.server.server.model.npc.L1NpcHtml;
import l1j.server.server.model.npc.action.L1NpcAction;
import l1j.server.server.serverpackets.S_ACTION_UI;
import l1j.server.server.serverpackets.S_Message_YN;
import l1j.server.server.serverpackets.S_NPCTalkReturn;
import l1j.server.server.serverpackets.S_NewCreateItem;
import l1j.server.server.serverpackets.S_NewUI;
import l1j.server.server.serverpackets.S_PacketBox;
import l1j.server.server.serverpackets.S_Paralysis;
import l1j.server.server.serverpackets.S_Party;
import l1j.server.server.serverpackets.S_PetWindow;
import l1j.server.server.serverpackets.S_ServerMessage;
import l1j.server.server.serverpackets.S_SkillSound;
import l1j.server.server.serverpackets.S_SupportSystem;
import l1j.server.server.serverpackets.S_SystemMessage;
import l1j.server.server.templates.L1EnchantWeaponChance;
import l1j.server.server.templates.L1PetSkill;
import l1j.server.server.templates.L1UserRanking;
import l1j.server.server.utils.SQLUtil;
import server.LineageClient;
import server.manager.eva;

public class C_NewCreateItem extends ClientBasePacket {

	private static final String C_EXTENDED_PROTOBUF = "[C] C_NewCreateItem";
	Random _Random = new Random(System.nanoTime());
	
	private static final int START_PLAY_SUPPORT 	= 2101;
	private static final int FINISH_PLAY_SUPPORT 	= 2103;
	private static final int CLAN_BUFF_CHANGE 		= 1017; // 파티초대
	private static final int CLAN_BUFF_CHOICE 		= 1016; // 파티초대
	
	private static final int DOLL_MAKING 			= 0x7a; // 인형제작창 띄우기
	private static final int DOLL_MAKING_RESULT 	= 0x7c; // 인형제작 결과 요청.

	private static final int PetCommand 			= 0x07d4;
	private static final int Petstat 				= 0x07d3;
	private static final int PetName 				= 0x07d1;
	private static final int PetSkill 				= 0x07d8;
	private static final int PetSkillLevel  		= 0x07d7;
	
	public C_NewCreateItem(byte[] decrypt, LineageClient client) {
		super(decrypt);
		try {
			int type = readH();

			L1PcInstance pc = client.getActiveChar();
			int objectId = 0, itemtype = 0, itemcount = 0;

			L1Object obj = null;
			String s = null;
			L1NpcInstance npc;
			L1NpcAction action;
			L1PetInstance Pet;
			// System.out.println(type);
			if (type != 0x01e4) {
				if (pc == null)
					return;
			}
			switch (type) {
			case PetSkill:
				readH();
				readC();
				int NameNumber = readC();
				Pet = (L1PetInstance) pc.getPet();
				int SkillLevel = 0;
				int[] Amount = null;
				if(NameNumber == 1 || NameNumber == 7 || NameNumber == 15 ||  NameNumber == 18 ||
				   NameNumber == 19 || NameNumber == 20 || NameNumber == 21 ||  NameNumber == 22){
					SkillLevel = 1;
					Amount = new int[]{ 3, 4, 5, 7, 10, 15, 25, 50, 100, 200 };
				}else if(NameNumber == 2 || NameNumber == 8 || NameNumber == 16){
					SkillLevel = 2;
					Amount = new int[]{ 13, 24, 35, 47, 60, 75, 95, 130, 190, 300};
				}else if(NameNumber == 24 || NameNumber == 14 || NameNumber == 12 ||
					NameNumber == 23 || NameNumber == 10 || NameNumber == 17){
					SkillLevel = 3;
					Amount = new int[]{ 28, 54, 80, 107, 135, 165, 200, 250, 325, 450};
				}else if(NameNumber == 4 || NameNumber == 9 || NameNumber == 11 || 
					NameNumber == 5 || NameNumber == 13){
					SkillLevel = 4;
					Amount = new int[]{ 48, 94, 140, 187, 235, 285, 340, 410, 505, 650};
				}else if(NameNumber == 3){
					SkillLevel = 5;
					Amount = new int[]{ 73, 144, 215, 287, 360, 435, 515, 610, 730, 900};
				}
				
				L1PetSkill PetSkill = Pet.getPetSkills(NameNumber);
				if (PetSkill != null) {
					pc.sendPackets(new S_PetWindow(S_PetWindow.Friendship, Pet), true);
					if (PetSkill.getSkillLevel() < 4) {
						if (pc.getInventory().consumeItem(43204, 1)
								&& Pet.getFriendship() >= Amount[PetSkill.getSkillLevel()]) {
							Pet.setFriendship(Pet.getFriendship() - Amount[PetSkill.getSkillLevel()]);
							PetSkill.setSkillLevel(PetSkill.getSkillLevel() + 1);
							Pet.SkillsUpdate(PetSkill.getSkillNumber(), PetSkill.getSkillLevel());
							pc.sendPackets(new S_PetWindow(PetSkill), true);
							pc.sendPackets(new S_PetWindow(NameNumber, true), true);
							PetsSkillsTable.SaveSkills(Pet, false);
							pc.sendPackets(new S_PetWindow(Pet, false), true);
						} else {
							pc.sendPackets(new S_PetWindow(NameNumber, false, true), true);
						}
					} else {
						int Chance = _Random.nextInt(100) + 1;
						if (pc.getInventory().consumeItem(43204, 1)
								&& Pet.getFriendship() >= Amount[PetSkill.getSkillLevel()]) {
							Pet.setFriendship(Pet.getFriendship() - Amount[PetSkill.getSkillLevel()]);
							if (50 - (PetSkill.getSkillLevel() * SkillLevel) >= Chance) {
								PetSkill.setSkillLevel(PetSkill.getSkillLevel() + 1);
								Pet.SkillsUpdate(PetSkill.getSkillNumber(), PetSkill.getSkillLevel());
								pc.sendPackets(new S_PetWindow(PetSkill), true);
								pc.sendPackets(new S_PetWindow(NameNumber, true), true);
								PetsSkillsTable.SaveSkills(Pet, false);
								pc.sendPackets(new S_PetWindow(Pet, false), true);
							} else {
								pc.sendPackets(new S_PetWindow(NameNumber, false), true);
							}
						}
						pc.sendPackets(new S_PetWindow(NameNumber, false, true), true);
					}
				}
				break;
			case PetSkillLevel:
				Pet = (L1PetInstance) pc.getPet();
				if (Pet != null) {
					if (Pet.getPetSkills(Pet.getPetType().getSkillOneStep())) {
						pc.sendPackets(new S_ServerMessage(5314), true); //아데나가 부족 합니다.
						Pet.addPetSkills(Pet.getPetType().getSkillOneStep());
						pc.sendPackets(new S_PetWindow(Pet.ArrayPetSkills()), true);
						PetsSkillsTable.SaveSkills(Pet, false);
					} else if (Pet.getPetSkills(Pet.getPetType().getSkillTwoStep())) {
						if (!pc.getInventory().consumeItem(40308, 100000)) {
							pc.sendPackets(new S_ServerMessage(3559), true);
							return;
						}
						pc.sendPackets(new S_ServerMessage(5314), true);
						Pet.addPetSkills(Pet.getPetType().getSkillTwoStep());
						pc.sendPackets(new S_PetWindow(Pet.ArrayPetSkills()), true);
						PetsSkillsTable.SaveSkills(Pet, false);
					} else if (Pet.getPetSkills(Pet.getPetType().getSkillThreeStep())) {
						if (!pc.getInventory().consumeItem(40308, 500000)) {
							pc.sendPackets(new S_ServerMessage(3559), true);
							return;
						}
						pc.sendPackets(new S_ServerMessage(5314), true);
						Pet.addPetSkills(Pet.getPetType().getSkillThreeStep());
						pc.sendPackets(new S_PetWindow(Pet.ArrayPetSkills()), true);
						PetsSkillsTable.SaveSkills(Pet, false);
					} else if (Pet.getPetSkills(Pet.getPetType().getSkillFourStep())) {
						if (!pc.getInventory().consumeItem(40308, 1000000)) {
							pc.sendPackets(new S_ServerMessage(3559), true);
							return;
						}
						pc.sendPackets(new S_ServerMessage(5314), true);
						Pet.addPetSkills(Pet.getPetType().getSkillFourStep());
						pc.sendPackets(new S_PetWindow(Pet.ArrayPetSkills()), true);
						PetsSkillsTable.SaveSkills(Pet, false);
					} else if (Pet.getPetSkills(Pet.getPetType().getSkillFiveStep())) {
						if (!pc.getInventory().consumeItem(40308, 5000000)) {
							pc.sendPackets(new S_ServerMessage(3559), true);
							return;
						}
						pc.sendPackets(new S_ServerMessage(5314), true);
						Pet.addPetSkills(Pet.getPetType().getSkillFiveStep());
						pc.sendPackets(new S_PetWindow(Pet.ArrayPetSkills()), true);
						PetsSkillsTable.SaveSkills(Pet, false);
					} else {
						pc.sendPackets(new S_ServerMessage(312), true);
					}
				}
				break;
			case PetName:
				readH(); // size;
				readC(); // size;
				int NameLength = readC();
				String Name = readS(NameLength);
				Pet = (L1PetInstance) pc.getPet();
				if(Pet != null){
					if(Pet.isInvalidName(Name)){
						pc.sendPackets(new S_SystemMessage("사용할수 없는 이름 입니다."), true);
						return;
					}
					if (PetTable.isNameExists(Name)) {
						pc.sendPackets(new S_ServerMessage(327), true);
						return;
					}
					Pet.PetName(Name);
				}
				break;
			case Petstat:
				readH(); 
				int Temp[] = new int[3];
				Pet = (L1PetInstance) pc.getPet();
				if(Pet != null && Pet.getBonusPoint() > 0){
					for (int i = 0; i < Temp.length; i++) {
						readC();
						Temp[i] = readC(); 
					}
					Pet.BonusPoint(Temp);
				}
				break;
			case PetCommand:
				readH(); 
				readC(); 
				int Command = readC();
				Pet = (L1PetInstance) pc.getPet();
				if(Pet != null){
					switch (Command) {	
						case 2:
							Pet.onFinalAction(1, null);
							break;
						case 3:
							Pet.onFinalAction(2, null);
							break;
						case 6:
							Pet.onFinalAction(8, null);
							break;
						case 7:
							readC();
							L1Object targetobj = L1World.getInstance().findObject(read4(read_size()));
							Pet.onFinalAction(9, targetobj);
							break;
						case 9:
							Pet.onFinalAction(10, null);
							break;
						case 101:
							Pet.onFinalAction(67, null);
							break;
					}
				}
				break;
			case DOLL_MAKING: {// 인형제작
				if (pc.getInventory().calcWeightpercent() >= 90) {
					pc.sendPackets(new S_SystemMessage("무게 게이지가 가득차서 합성을 진행할 수 없습니다."));
					return;
				}
				if (client.인형합성패킷전송중)
					return;

				readH();// length
				readC();
				readC();
				int unknown = readD();
				pc.sendPackets(new S_NewCreateItem(0x7b, "08 03 00 00"));
				break;
			}
			case DOLL_MAKING_RESULT: {// 인형제작
				int bytelen = readH();// 길이임
				readC();
				int typelevel = readC();
				byte[] BYTE = readByte();

				int a_len = 0;
				int _off = 0;
				a_len = BYTE[_off + 1];
				StringBuffer sb = null;
				StringBuffer sb2 = null;
				byte[] temp = null;
				ArrayList<L1ItemInstance> _usedoll = new ArrayList<L1ItemInstance>();
				L1ItemInstance item = null;
				int saveid = 0;
				boolean max = false;
				while (bytelen > 7) {
					temp = new byte[a_len - 6];
					System.arraycopy(BYTE, _off + 8, temp, 0, a_len - 6);
					bytelen -= temp.length + 8;
					_off += temp.length + 8;
					sb = new StringBuffer();
					sb2 = new StringBuffer();
					for (byte zzz : temp) {
						sb.append(HexToDex(zzz & 0xff, 2) + " ");
						sb2.append(String.valueOf(zzz));
					}
					item = pc.getInventory().findEncobj(sb2.toString());

					if (item == null) {
						pc.sendPackets(new S_SystemMessage(
								"정상적인 방법으로만 이용해 주세요."));
						return;
					}
					if(saveid == 0) {
						saveid = item_doll_code(item.getItemId());
					}else {
						if(saveid != item_doll_code(item.getItemId())) 
							max = true;
					}
					_usedoll.add(item);
				}
				try {
					String lll = "08";
					if (temp.length == 5) {
						lll = "09";
					} else if (temp.length == 4) {
						lll = "08";
					} else if (temp.length == 3) {
						lll = "07";
					} else if (temp.length == 2) {
						lll = "06";
					} else if (temp.length == 1) {
						lll = "05";
					}
					int rnd = _Random.nextInt(100) + 1;
					int suc = 1;// 실패
					boolean 월드메세지 = false;
					int failditemid = _usedoll.get(0).getItemId();
					Collections.shuffle(_usedoll);
					int itemid = _usedoll.get(0).getItemId();
					int dollid = itemid;
					L1ItemInstance sucitem = null;
					if (typelevel == 1) {
						if (rnd < Config.DOLL_1) // 성공
							suc = 0;
						if (suc == 0) {
							dollid = lv2doll[_Random.nextInt(lv2doll.length)];
							sucitem = ItemTable.getInstance().createItem(dollid);
						}
					} else if (typelevel == 2) {
						if (rnd < Config.DOLL_2) // 성공
							suc = 0;
						if (suc == 0) {
							dollid = lv3doll[_Random.nextInt(lv3doll.length)];
							sucitem = ItemTable.getInstance().createItem(dollid);
						}
					} else if (typelevel == 3) {
						if (rnd < Config.DOLL_3) // 성공
							suc = 0;
						if (suc == 0) {
							dollid = lv4doll[_Random.nextInt(lv4doll.length)];
							sucitem = ItemTable.getInstance().createItem(dollid);
						}
					} else if (typelevel == 4) {
						

							if (rnd < Config.DOLL_4) // 성공
								suc = 0;
	
						if (suc == 0) {
							dollid = lv5doll[_Random.nextInt(lv5doll.length)];
							sucitem = ItemTable.getInstance().createItem(dollid);
						}
					}else if (typelevel == 5) { //축5단계 및 드래곤
						if(max) {
							if(rnd < Config.DOLL_5)
								suc = 0;
						}else {
							if(rnd < 10)
								suc = 0;
						}
						if (suc == 0) {
							dollid = lv5dollDragon[_Random.nextInt(lv5dollDragon.length)];
							sucitem = ItemTable.getInstance().createItem(dollid);
						}
						/** 1이라면 실패효과**/
					else {
							dollid = lv5doll[_Random.nextInt(lv5doll.length)];
							sucitem = ItemTable.getInstance().createItem(dollid);
							/** 실패 해도 5단계가 나오기 때문에 메세지 출력 */
					}
					}
					
					else if (typelevel == 6) { //축 3단계
						
						if(max) {
							if(rnd < Config.DOLL_6)
								suc = 0;
						}else {
							if(rnd < 10)
								suc = 0;
						}
						if (suc == 0) {
							dollid = lv6doll[_Random.nextInt(lv6doll.length)];
							sucitem = ItemTable.getInstance().createItem(dollid);
						}
						/** 1이라면 실패효과**/
					else {
							dollid = lv3doll[_Random.nextInt(lv3doll.length)];
							sucitem = ItemTable.getInstance().createItem(dollid);
							/** 실패 해도 3단계가 나오기 때문에 메세지 출력 */

					}
					}
					else if (typelevel == 7) { //축 4단계
						
						if(max) {
							if(rnd < Config.DOLL_7)
								suc = 0;
						}else {
							if(rnd < 10)
								suc = 0;
						}
						if (suc == 0) {
							dollid = lv7doll[_Random.nextInt(lv7doll.length)];
							sucitem = ItemTable.getInstance().createItem(dollid);
							/** 1이라면 실패 효과 */
						}else {
							dollid = lv4doll[_Random.nextInt(lv4doll.length)];
							sucitem = ItemTable.getInstance().createItem(dollid);
							/** 실패 해도 4단계가 나오기 때문에 메세지 출력 */
						
						}
					}
					for (L1ItemInstance zzz : _usedoll) {
						pc.getInventory().removeItem(zzz);
					}
					if (sucitem == null) {
						sucitem = ItemTable.getInstance().createItem(failditemid);
					}
					String ss = HexToDex(suc & 0xff, 2);
					sucitem.setCount(1);
					boolean success = false;
					if (suc == 0)
						success = true;
					else
						success = false;
					pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.DOLL_MAKING_RESULT, success, sucitem));
					if (suc == 0) {
						if (typelevel >= 3)
							월드메세지 = true;
						pc.getInventory().storeItem(sucitem, sucitem.getBless());
						if (월드메세지) {
							Thread.sleep(8700L);
							L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE,"누군가가 " + sucitem.getName() + " 합성에 성공 하였습니다."));
						}
					} else {
						pc.getInventory().storeItem(sucitem, sucitem.getBless());
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				break;
			}
			case CLAN_BUFF_CHANGE: {
				L1Clan clan = pc.getClan();
				if(clan == null || !pc.getInventory().checkItem(40308, 300000)) return;
				pc.getInventory().consumeItem(40308, 300000);
				clan.setEinhasadBlessBuff(0);
				clan.setBuffFirst(ClanBuffTable.getRandomBuff(clan));
				clan.setBuffSecond(ClanBuffTable.getRandomBuff(clan));
				clan.setBuffThird(ClanBuffTable.getRandomBuff(clan));
				pc.sendPackets(new S_EinhasadClanBuff(pc), true);	
				ClanTable.getInstance().updateClan(clan);
				break;
			}
			case CLAN_BUFF_CHOICE: {
				readH();
				readC();
				readBit();
				readC();
				int buffnum = readBit();
				readC();
				int BuffCheck = readBit();
				L1Clan clan = pc.getClan();
				if(clan == null) return;
				switch (BuffCheck) {
					case 1:
						clan.setEinhasadBlessBuff(buffnum);
						pc.sendPackets(new S_EinhasadClanBuff(pc), true);
						break;
						
					case 2:
						if (!pc.getMap().isEscapable()) {
							pc.sendPackets(new S_SystemMessage("이곳에서는 사용할수 없습니다."), true);
							pc.sendPackets(new S_Paralysis(S_Paralysis.TYPE_TELEPORT_UNLOCK, false));
							return;
						}
						
						ClanBuff Buff = ClanBuffTable.getBuffList(buffnum);
						if(Buff == null || !pc.getInventory().checkItem(40308, 1000)) return;
						pc.getInventory().consumeItem(40308, 1000);
						pc.sendPackets(new S_Paralysis(S_Paralysis.TYPE_TELEPORT_UNLOCK, false));
						L1Teleport.teleport(pc, Buff.teleportX, Buff.teleportY, (short) Buff.teleportM, pc.getMoveState().getHeading(), true);
						break;
						
					case 3:
						if(!pc.getInventory().checkItem(40308, 500000)) return;
						pc.getInventory().consumeItem(40308, 500000);
						clan.setEinhasadBlessBuff(buffnum);
						pc.sendPackets(new S_EinhasadClanBuff(pc), true);
						break;
				}
				ClanTable.getInstance().updateClan(clan);
				break;
			}
			case START_PLAY_SUPPORT:
				if (SupportMapTable.getInstance().isSupportMap(pc.getMapId())
						&& !pc.getMap().isSafetyZone(pc.getLocation())) {
					L1SupportMap SM = SupportMapTable.getInstance().getSupportMap(pc.getMapId());
					if (SM != null) {
						pc.setAutoPlay(true);
						pc.sendPackets(new S_SupportSystem(pc, S_SupportSystem.SC_START_PLAY_SUPPORT_ACK), true);
					}
				} else {
					pc.sendPackets(new S_SystemMessage("이 곳에서는 사용할 수 없습니다."), true);
				}
				break;
			case FINISH_PLAY_SUPPORT:
				pc.setAutoPlay(false);
				pc.sendPackets(new S_SupportSystem(pc, S_SupportSystem.SC_FINISH_PLAY_SUPPORT_ACK), true);
				break;
			default:
				break;
			}

			if (type == 0x87) { // 유저랭킹시스템
				readH();
				readC();
				int classId = readC();
				ArrayList<L1UserRanking> list = UserRankingController.getInstance().getList(classId);

				if (list.size() > 200) {
					List<L1UserRanking> cutlist = list.subList(0, 200);
					List<L1UserRanking> cutlist2 = list.subList(200, list.size());
					pc.sendPackets(new S_NewCreateItem(S_NewCreateItem.유저랭킹, cutlist, classId, 2, 1));
					pc.sendPackets(new S_NewCreateItem(S_NewCreateItem.유저랭킹, cutlist2, classId, 2, 2));
				} else {
					pc.sendPackets(new S_NewCreateItem(S_NewCreateItem.유저랭킹, list, classId, 1, 1));
				}
			} else if (type == 0x033c) {
				try {
					readH(); // 사이즈
					readC();
					int size = 0;
					int code = readBit();
					byte[] name = null;
					String target_name = null;
					if(code == 0 || code == 1 || code == 3 || code == 6){
						readC();
						objectId = readBit();
						readC();
						size = readBit();
						name = readByte(size);
						target_name = new String(name, "MS949");
						
						obj = L1World.getInstance().findObject(objectId);
						
					} else if(code == 4 || code == 5 || code == 2 || code == 7) {
						readC();
						size = readBit();
						name = readByte(size);
						target_name = new String(name, "MS949");
						
						obj = L1World.getInstance().getPlayer(target_name);
					}
					
					if (obj == null)
						return;

					if (!(obj instanceof L1PcInstance))
						return;

					L1PcInstance target = (L1PcInstance) obj;

					if (target.getId() == pc.getId())
						return;
					if (target.noPlayerCK)
						return;

					if (code == 0 || code == 1 || code == 4 || code == 5) {
						if (target.isInParty()) {
							pc.sendPackets(new S_ServerMessage(415));
							return;
						}

						if (pc.isInParty()) {
							if (pc.getParty().isLeader(pc)) {
								target.setPartyID(pc.getId());
								switch (code) {
								case 0:
								case 4:
									pc.setPartyType(0);
									target.sendPackets(new S_Message_YN(953, pc.getName()));
									break;
								case 1:
								case 5:
									pc.setPartyType(1);
									target.sendPackets(new S_Message_YN(954, pc.getName()));
								case 2:
								case 3:
								}
							} else {
								pc.sendPackets(new S_ServerMessage(416));
							}
						} else {
							target.setPartyID(pc.getId());

							switch (code) {
							case 0:
							case 4:
								pc.setPartyType(0);
								target.sendPackets(new S_Message_YN(953, pc.getName()));
								break;
							case 1:
							case 5:
								pc.setPartyType(1);
								target.sendPackets(new S_Message_YN(954, pc.getName()));
							case 2:
							case 3:
							}
						}
					} else if (code == 2) { // 채팅 파티
						if (target.isInChatParty()) {
							pc.sendPackets(new S_ServerMessage(415));
							return;
						}

						if (pc.isInChatParty()) {
							if (pc.getChatParty().isLeader(pc)) {
								target.setPartyID(pc.getId());
								target.sendPackets(new S_Message_YN(951, pc.getName()));
							} else {
								pc.sendPackets(new S_ServerMessage(416));
							}
						} else {
							target.setPartyID(pc.getId());
							target.sendPackets(new S_Message_YN(951, pc.getName()));
						}
					} else if (code == 3) { // 파티장 위임
						if (pc.getParty() == null || !pc.getParty().isLeader(pc)) {
							pc.sendPackets(new S_ServerMessage(1697));
							return;
						}

						if (!target.isInParty()) {
							pc.sendPackets(new S_ServerMessage(1696));
							return;
						}

						pc.getParty().passLeader(target);
					} else if (code == 6) { // 마크변경
						readC();
						int mark = readBit();
						
						if (!pc.getParty().isLeader(pc)) {
							return;
						}

						target.setPartySign(mark);
						for (L1PcInstance member : pc.getParty().getMembers()) {
							member.sendPackets(new S_Party(S_Party.OPCODE_TYPE_PARTY_MEMBER_STATUS, S_Party.MEMBER_MARK_CHANGE, target));
						}

					} else if (code == 7) { // 파티추방
						if (!pc.getParty().isLeader(pc)) {
							// 파티 리더가 아닌 경우
							pc.sendPackets(new S_ServerMessage(427));
							// 파티의 리더만을 추방할 수 있습니다.
							return;
						}

						
						target.sendPackets(new S_ServerMessage(419)); // 파티로부터 추방되었습니다.
						for (L1PcInstance member : pc.getParty().getMembers()) {
							if (member.getName().toLowerCase().equals(target_name.toLowerCase())) {
								pc.getParty().leaveMember(member);
								return;
							}
						}
						// 발견되지 않았다
						pc.sendPackets(new S_ServerMessage(426, s)); // %0는 파티 멤버가 아닙니다.
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
		/*	} else if (type == 0x03ee) {
				readC();
				readH();
				int check_id = readBit();
				readC();
				int Normal_PC = readBit();

				ArrayList<Attendtemp> itemlist = null;

				if (pc.getLevel() < Config.AttendLevel) {
					pc.sendPackets(new S_SystemMessage(pc, "출석 체크 완료 실패: "+Config.AttendLevel+"레벨 미만 수령불가"), true);
					return;
				}

				if (Normal_PC == 0) {
					itemlist = AttenDanceTable.getInstance().getNormalList();
					for (idTemp temp : pc.attendTemp.Nomarlist) {
						if (temp.id == check_id) {
							temp.state++;
							if (temp.state >= 2) {
								temp.state = 2;
							}
							break;
						}
					}
				} else {
					itemlist = AttenDanceTable.getInstance().getPcRoomList();
					for (idTemp temp : pc.attendTemp.PcRoomlist) {
						if (temp.id == check_id) {
							temp.state++;
							if (temp.state >= 2) {
								temp.state = 2;
							}
							break;
						}
					}
				}
				
				for (Attendtemp temp : itemlist) {
					if (check_id == temp.id) {
						pc.getInventory().storeItem(temp.item_id, temp.item_count);
						break;
					}
				}
				pc.sendPackets(new S_AttenDance(S_AttenDance.WatchStoreItem, check_id, Normal_PC));
				pc.sendPackets(new S_SystemMessage(pc, "보상 아이템 미지급시 리스타트 후 클릭하세요."), true);
				return;*/
			} else if (type == 802) {
				pc.sendPackets(new S_NewUI(35, mapOrders(), mapNames(), mapTimes(pc), maxmapTimes()));
			} else if (type == 801) {
				readH();
				int index = readC();
				int code = readC();
				if (index == 0x08) {
					InvSwapController.getInstance().toSaveSet(pc, code);
				} else if (index == 0x10) {
					InvSwapController.getInstance().toChangeSet(pc, code);
				}
			} else if (type == 0x021F) {
				int length = readH();
				readC();
				int excludeType = readC();
				readC();
				int subType = readC();
				int nameFlag = readC();
				String name = "";
				if (nameFlag == 0x1A) {
					int nameLength = readC();
					name = readS(nameLength);
				}
				try {
					if (name.isEmpty()) {
						pc.sendPackets(new S_PacketBox(S_PacketBox.ADD_EXCLUDE2, pc.getExcludingList().getList(), 0),
								true);
						pc.sendPackets(
								new S_PacketBox(S_PacketBox.ADD_EXCLUDE2, pc.getExcludingLetterList().getList(), 1),
								true);
						return;
					}

					if (excludeType == 1) {// 추가
						L1ExcludingList exList = pc.getExcludingList();
						L1ExcludingLetterList exletterList = pc.getExcludingLetterList();
						switch (subType) {// 일반 0 편지 1
						case 0:
							if (exList.contains(name)) {
								/*
								 * String temp = exList.remove(name);
								 * S_PacketBox pb = new
								 * S_PacketBox(S_PacketBox.REM_EXCLUDE, temp,
								 * type); pc.sendPackets(pb, true);
								 * ExcludeTable.getInstance().delete(pc.getName(
								 * ), name);
								 */
							} else {
								if (exList.isFull()) {
									S_SystemMessage sm = new S_SystemMessage("차단된 사용자가 너무 많습니다.");
									pc.sendPackets(sm, true);
									return;
								}
								exList.add(name);
								S_PacketBox pb = new S_PacketBox(S_PacketBox.ADD_EXCLUDE, name, 0);
								pc.sendPackets(pb, true);
								ExcludeTable.getInstance().add(pc.getName(), name);
							}
							break;
						case 1:
							if (exletterList.contains(name)) {

							} else {
								if (exletterList.isFull()) {
									S_SystemMessage sm = new S_SystemMessage("차단된 사용자가 너무 많습니다.");
									pc.sendPackets(sm, true);
									return;
								}
								exletterList.add(name);
								S_PacketBox pb = new S_PacketBox(S_PacketBox.ADD_EXCLUDE, name, 1);
								pc.sendPackets(pb, true);
								ExcludeLetterTable.getInstance().add(pc.getName(), name);
							}
							break;
						default:
							break;
						}
					} else if (excludeType == 2) {// 삭제
						L1ExcludingList exList = pc.getExcludingList();
						L1ExcludingLetterList exletterList = pc.getExcludingLetterList();
						switch (subType) {// 일반 0 편지 1
						case 0:
							if (exList.contains(name)) {
								String temp = exList.remove(name);
								S_PacketBox pb = new S_PacketBox(S_PacketBox.REM_EXCLUDE, temp, 0);
								pc.sendPackets(pb, true);
								ExcludeTable.getInstance().delete(pc.getName(), name);
							}
							break;
						case 1:
							if (exletterList.contains(name)) {
								String temp = exletterList.remove(name);
								S_PacketBox pb = new S_PacketBox(S_PacketBox.REM_EXCLUDE, temp, 1);
								pc.sendPackets(pb, true);
								ExcludeLetterTable.getInstance().delete(pc.getName(), name);
							}
							break;
						default:
							break;
						}
					}
				} catch (Exception e) {

				}
			} else if (type == 143) {
				readH();
				readC();
				int num = readC();
				readC();
				switch (num) {
				case 1:// 샌드웜
					if (!pc.getInventory().checkItem(40308, 10000)) {
						pc.sendPackets(new S_SystemMessage("아데나가 부족합니다."));
						return;
					}
					pc.getInventory().consumeItem(40308, 10000);
					pc.sendPackets(new S_SystemMessage(pc, "5초뒤 에르자베 인근 지역으로 이동합니다.."));
					L1Teleport.teleport(pc, 32908, 33233, (short) 4, 5, true, true, 5000);
					break;
				case 2:
					if (!pc.getInventory().checkItem(40308, 10000)) {
						pc.sendPackets(new S_SystemMessage("아데나가 부족합니다."));
						return;
					}
					pc.getInventory().consumeItem(40308, 10000);
					pc.sendPackets(new S_SystemMessage(pc, "5초뒤 샌드 웜 인근 지역으로 이동합니다.."));
					L1Teleport.teleport(pc, 32774, 33164, (short) 4, 5, true, true, 5000);
					break;
				case 3:
					if (!pc.getInventory().checkItem(40308, 10000)) {
						pc.sendPackets(new S_SystemMessage("아데나가 부족합니다."));
						return;
					}
					pc.getInventory().consumeItem(40308, 10000);
					pc.sendPackets(new S_SystemMessage(pc, "5초뒤 켄트성 인근 지역으로 이동합니다.."));
					L1Teleport.teleport(pc, 33086, 32766, (short) 4, 5, true, true, 500);
					break;
				}
			} else if (type == 0x44) {
				pc.sendPackets(new S_NewCreateItem(S_NewCreateItem.CLAN_JOIN_WAIT, true), true);
			} else if (type == 0x36) { // CRAFT_ITEM
				pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.CRAFT_ITEM));
			} else if (type == 0x38) { // CRAFT_ITEMLIST
				readH(); // size;
				readC(); // dummy
				objectId = read4(read_size());
				obj = L1World.getInstance().findObject(objectId);
				if (obj instanceof L1NpcInstance) {
					npc = (L1NpcInstance) obj;
					pc.sendPackets(new S_ACTION_UI(npc));
				}
			} else if (type == 0x013D) { // 세금 관련 맵 표시
			} else if (type == 0x013F) {// 소셜액션
				readD();
				readC();
				int action1 = readC();
				if (action1 >= 1 && action1 <= 11) {
					pc.sendPackets(new S_NewCreateItem(S_NewCreateItem.EMOTICON, action1, pc.getId()), true);
					Broadcaster.broadcastPacket(pc, new S_NewCreateItem(S_NewCreateItem.EMOTICON, action1, pc.getId()),
							true);
				}

			} else if (type == 0x45) {
				try {
					readH();
					readC();
					int castleType = readC();
					// 1켄트 2기란 4오크요새
					String s1 = "";
					for (L1Clan cc : L1World.getInstance().getAllClans()) {
						if (castleType == cc.getCastleId()) {
							s1 = cc.getClanName();
							break;
						}
					}

					if (s1.equalsIgnoreCase("")) {
						return;
					}

					L1PcInstance player = pc;
					String playerName = player.getName();
					String clanName = player.getClanname();
					int clanId = player.getClanid();

					if (!player.isCrown()) { // 군주 이외
						S_ServerMessage sm = new S_ServerMessage(478);
						player.sendPackets(sm, true);
						return;
					}
					if (clanId == 0) { // 크란미소속
						S_ServerMessage sm = new S_ServerMessage(272);
						player.sendPackets(sm, true);
						return;
					}
					L1Clan clan = L1World.getInstance().getClan(clanName);
					if (clan == null) { // 자크란이 발견되지 않는다
						S_SystemMessage sm = new S_SystemMessage("대상 혈맹이 발견되지 않았습니다.");
						player.sendPackets(sm, true);
						return;
					}
					if (player.getId() != clan.getLeaderId()) { // 혈맹주
						S_ServerMessage sm = new S_ServerMessage(478);
						player.sendPackets(sm, true);
						return;
					}
					if (clanName.toLowerCase().equals(s1.toLowerCase())) { // 자크란을
																			// 지정
						S_SystemMessage sm = new S_SystemMessage("자신의 혈에 공성 선포는 불가능합니다.");
						player.sendPackets(sm, true);
						return;
					}
					L1Clan enemyClan = null;
					String enemyClanName = null;
					for (L1Clan checkClan : L1World.getInstance().getAllClans()) { // 크란명을
																					// 체크
						if (checkClan.getClanName().toLowerCase().equals(s1.toLowerCase())) {
							enemyClan = checkClan;
							enemyClanName = checkClan.getClanName();
							break;
						}
					}
					if (enemyClan == null) { // 상대 크란이 발견되지 않았다
						S_SystemMessage sm = new S_SystemMessage("대상 혈맹이 발견되지 않았습니다.");
						player.sendPackets(sm, true);
						return;
					}
					if (clan.getAlliance(enemyClan.getClanId()) == enemyClan) {
						S_ServerMessage sm = new S_ServerMessage(1205);
						player.sendPackets(sm, true);
						return;
					}
					List<L1War> warList = L1World.getInstance().getWarList(); // 전쟁
																				// 리스트를
																				// 취득
					if (clan.getCastleId() != 0) { // 자크란이 성주
						S_ServerMessage sm = new S_ServerMessage(474);
						player.sendPackets(sm, true);
						return;
					}
					if (enemyClan.getCastleId() != 0 && player.getLevel() < 25) {
						S_ServerMessage sm = new S_ServerMessage(475);
						player.sendPackets(sm, true); // 공성전을 선언하려면 레벨 25에 이르지
														// 않으면 안됩니다.
						return;
					}

					int onLineMemberSize = 0;
					for (L1PcInstance onlineMember : clan.getOnlineClanMember()) {
						if (onlineMember.isPrivateShop())
							continue;
						onLineMemberSize++;
					}

					if (onLineMemberSize < 5) {
						player.sendPackets(new S_SystemMessage("접속중인 혈맹 구성원이 5명 이상 되어야 선포가 가능합니다."), true);
						return;
					}

					if (enemyClan.getCastleId() != 0) { // 상대 크란이 성주
						int castle_id = enemyClan.getCastleId();
						if (WarTimeController.getInstance().isNowWar(castle_id)) { // 전쟁
																					// 시간내
							L1PcInstance clanMember[] = clan.getOnlineClanMember();
							for (int k = 0; k < clanMember.length; k++) {
								if (L1CastleLocation.checkInWarArea(castle_id, clanMember[k])) {
									// S_ServerMessage sm = new
									// S_ServerMessage(477);
									// player.sendPackets(sm, true); // 당신을 포함한
									// 모든 혈맹원이 성의 밖에 나오지 않으면 공성전은 선언할 수 없습니다.
									int[] loc = new int[3];
									Random _rnd = new Random(System.nanoTime());
									loc = L1CastleLocation.getGetBackLoc(castle_id);
									int locx = loc[0] + (_rnd.nextInt(4) - 2);
									int locy = loc[1] + (_rnd.nextInt(4) - 2);
									short mapid = (short) loc[2];
									L1Teleport.teleport(clanMember[k], locx, locy, mapid,
											clanMember[k].getMoveState().getHeading(), true);
								}
							}
							boolean enemyInWar = false;
							for (L1War war : warList) {
								if (war.CheckClanInWar(enemyClanName)) { // 상대
																			// 크란이
																			// 이미
																			// 전쟁중
									war.DeclareWar(clanName, enemyClanName);
									war.AddAttackClan(clanName);
									enemyInWar = true;
									eva.BugLogAppend(" [공성 선포] (" + player.getClanname() + ")");
									break;
								}
							}
							if (!enemyInWar) { // 상대 크란이 전쟁중 이외로, 선전포고
								L1War war = new L1War();
								war.handleCommands(1, clanName, enemyClanName); // 공성전
																				// 개시
								eva.BugLogAppend(" [공성 선포] (" + player.getClanname() + ")");
							}
						} else { // 전쟁 시간외
							S_ServerMessage sm = new S_ServerMessage(476);
							player.sendPackets(sm, true); // 아직 공성전의 시간이 아닙니다.
						}
					} else { // 상대 크란이 성주는 아니다
						return;
					}
				} catch (Exception e) {
				}
			} else if (type == 0x0142) {// /혈맹가입
				readC();
				readH();
				int length = readC();
				byte[] BYTE = readByte();
				// 0 가입완료
				// 1 가입을 신청하였습니다.
				// 3 입력한 암호가 정확하지 않습니다.
				// 4 존재하지 않는 혈맹입니다.
				// 5 가입신청은 최대 3개의 혈맹에 할 수 있습니다.
				// 6 혈맹은 현재 가입할 수 없습니다.
				// 7 혈맹은 현재 가입할 수 없습니다.
				// 8 위와 동일
				// 9 이미 혈맹에 가입한 상태입니다.
				// 10 11 12 혈맹은 현재 가입할 수 없습니다.
				// 13 군주를 만나 가입해주세요.

				if (pc.isCrown()) {
					pc.sendPackets(new S_NewCreateItem(S_NewCreateItem.CLAN_JOIN_MESSAGE, 13), true);
					return;
				}

				if (pc.getClanid() != 0) {
					pc.sendPackets(new S_NewCreateItem(S_NewCreateItem.CLAN_JOIN_MESSAGE, 9), true);
					return;
				}

				String clanname = new String(BYTE, 0, length, "EUC-KR");
				L1Clan clan = L1World.getInstance().getClan(clanname);
				if (clan == null) {
					pc.sendPackets(new S_NewCreateItem(S_NewCreateItem.CLAN_JOIN_MESSAGE, 4), true);
					return;
				}
				L1PcInstance crown = clan.getonline간부();// L1World.getInstance().getPlayer(clan.getLeaderName());
				if (crown == null) {
					pc.sendPackets(new S_NewCreateItem(S_NewCreateItem.CLAN_JOIN_MESSAGE, 11), true);
					return;
				}
				if (pc.getClanid() == 0) { // 혈에 가입하지 않았으면서 현재캐릭이 클란에 가입 하려하면
					boolean Checking = pc.clanid_search1(pc.getNetConnection().getAccountName(), crown.getClanid());
					if (Checking == true) { // 상대의 클랜아디이와 다르면 가입을 거부시킨다.
						pc.sendPackets(new S_SystemMessage("당신의 계정내 한캐릭은 이미 다른 혈맹에 가입중입니다. "));
						pc.sendPackets(new S_SystemMessage("한 계정내 캐릭들은  같은 혈맹만 가입 가능합니다. "));
						pc.sendPackets(new S_SystemMessage("상대방의 계정에 캐릭터가 다른 혈맹에 가입중입니다."));
						return;
					}
				}
				if (clan.getJoinSetting() == 0) {
					pc.sendPackets(new S_NewCreateItem(S_NewCreateItem.CLAN_JOIN_MESSAGE, 8), true);
					return;

				} else if (clan.getJoinType() == 0) {
					C_Attr.혈맹가입(crown, pc, clan);
					pc.sendPackets(new S_NewCreateItem(S_NewCreateItem.CLAN_JOIN_MESSAGE, 0), true);
					return;
				} else {
					crown.setTempID(pc.getId()); // 상대의 오브젝트 ID를 보존해 둔다
					S_Message_YN myn = new S_Message_YN(97, pc.getName());
					crown.sendPackets(myn, true);
					pc.sendPackets(new S_NewCreateItem(S_NewCreateItem.CLAN_JOIN_MESSAGE, 1), true);
				}

			} else if (type == 0x0146) { // 혈맹 가입신청 받기 설정
				if (pc.getClanid() == 0 || (!pc.isCrown() && pc.getClanRank() != L1Clan.CLAN_RANK_GUARDIAN))
					return;
				readC();
				readH();
				int setting = readC();
				readC();
				int setting2 = readC();
				if (setting2 == 2) {
					pc.sendPackets(new S_SystemMessage("현재 암호 가입 유형으로 설정할 수 없습니다."), true);
					setting2 = 1;
				}

				pc.getClan().setJoinSetting(setting);
				pc.getClan().setJoinType(setting2);
				pc.sendPackets(new S_NewCreateItem(S_NewCreateItem.CLAN_JOIN_SETTING, setting, setting2), true);
				ClanTable.getInstance().updateClan(pc.getClan());
				pc.sendPackets(new S_ServerMessage(3980), true);
			} else if (type == 0x014C) { // 혈맹 모집 셋팅
				if (pc.getClanid() == 0)
					return;
				pc.sendPackets(new S_NewCreateItem(S_NewCreateItem.CLAN_JOIN_SETTING, pc.getClan().getJoinSetting(),
						pc.getClan().getJoinType()), true);
			} else if (type == 0x0152) { // 표식설정
				try {

					int length = readH();

					byte[] BYTE = readByte();
					byte[] objid = new byte[length - 3];
					byte[] subtype = new byte[1];

					if (pc.getParty() == null)
						return;
					if (!pc.getParty().isLeader(pc))
						return;

					System.arraycopy(BYTE, 1, objid, 0, objid.length);
					System.arraycopy(BYTE, length - 1, subtype, 0, 1);

					StringBuffer sb = new StringBuffer();

					for (byte zzz : objid) {
						sb.append(String.valueOf(zzz));
					}

					String s2 = sb.toString();

					L1PcInstance 표식pc = null;

					// System.out.println(s);

					for (L1PcInstance player : pc.getParty().getMembers()) {
						// System.out.println(player.encobjid);
						if (s2.equals(player.encobjid)) {
							player.표식 = subtype[0];
							표식pc = player;
						}
					}

					if (표식pc != null) {
						for (L1PcInstance player : pc.getParty().getMembers()) {
							player.sendPackets(new S_NewUI(0x53, 표식pc));
						}
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			} else if (type == 0x01e0) { // 탐 예약취소
				readC();
				readH();
				byte[] BYTE = readByte();
				byte[] temp = new byte[BYTE.length - 1];
				for (int i = 0; i < temp.length; i++) {
					temp[i] = BYTE[i];
				}
				StringBuffer sb = new StringBuffer();
				for (byte zzz : temp) {
					sb.append(String.valueOf(zzz));
				}
				int day = Nexttam(sb.toString());
				int charobjid = TamCharid(sb.toString());
				if (charobjid != pc.getId()) {
					pc.sendPackets(new S_SystemMessage("해당 케릭터만 취소를 할 수 있습니다."));
					return;
				}
				int itemid = 0;
				if (day != 0) {
					if (day == 1) {
						itemid = 5559;
					} else if (day == 3) {
						itemid = 5560;
					}
					L1ItemInstance item = pc.getInventory().storeItem(itemid, 1);
					if (item != null) {
						pc.sendPackets(new S_ServerMessage(403, item.getName() + " (1)"), true);
						tamcancle(sb.toString());
						pc.sendPackets(new S_NewCreateItem(pc.getAccountName(), 0xcd));
					}
				}
			} else if (type == 0x01cc) { // 혈맹 모집 셋팅
				pc.sendPackets(new S_NewCreateItem(pc.getAccountName(), S_NewCreateItem.TamPage));
			} else if (type == 0x84) { // 수상한 하늘정원
				if(pc.getMapId() >= 12852 && pc.getMapId() <= 12862){
					pc.sendPackets(new S_SystemMessage("지배의 탑에서는 사용할 수 없습니다."));
					return;
				}
				if (!pc.PC방_버프 && !pc.getInventory().checkItem(4192100)) {
					pc.sendPackets(new S_SystemMessage("PC방 이용권을 사용중에만 사용 가능한 행동입니다."), true);
					return;
				}
				if (pc.getMapId() == 99 || pc.getMapId() == 6202|| pc.getMapId() == 2004) {
					pc.sendPackets(new S_SystemMessage("주위의 마력에의해 순간이동을 사용할 수 없습니다."), true);
					return;
				}

				if (!pc.getMap().isEscapable()) {
					pc.sendPackets(new S_SystemMessage("주위의 마력에의해 순간이동을 사용할 수 없습니다."), true);
					return;
				}
				
				if (!pc.PC방_버프 && pc.getInventory().checkItem(4192100)) {
					pc.getInventory().consumeItem(4192100, 1);
				}

				int ran = _Random.nextInt(4);

				if (ran == 0) {
					L1Teleport.teleport(pc, 32779, 32825, (short) 622, pc.getMoveState().getHeading(), true);
				} else if (ran == 1) {
					L1Teleport.teleport(pc, 32761, 32819, (short) 622, pc.getMoveState().getHeading(), true);
				} else if (ran == 2) {
					L1Teleport.teleport(pc, 32756, 32837, (short) 622, pc.getMoveState().getHeading(), true);
				} else {
					L1Teleport.teleport(pc, 32770, 32839, (short) 622, pc.getMoveState().getHeading(), true);
				}

			} else if (type == 0x0202) {
				int totallen = readH();// 전체길이
				패킷위치변경((byte) 0x10);// 위치이동
				int chattype = readC();// 채팅타입
				패킷위치변경((byte) 0x1a);// 위치이동
				int chatlen = readC();// 채팅길이

				String chattext = "";
				String name = "";
				if (chatlen != 0) {
					chattext = readS(chatlen);
				}

				if (chattype != 1) {
					pc.chat(chattype, chattext, client);
					return;
				}

				패킷위치변경((byte) 0x2a);// 위치이동
				int namelen = readC();// 이름길이
				if (namelen != 0) {
					name = readS(namelen);
				}
				pc.whisper(name, chattext);
			} else if (type == 0x01e4) { // 캐릭터 생성
				try {
					/*
					 * b7 e4 01
					 * 
					 * 08 00
					 * 
					 * 08 63 10 03 18 10 40 27
					 * 
					 * b6 c8 4d
					 */
					int length = readH();// 길이
					ArrayList<byte[]> arrb = new ArrayList<byte[]>();
					for (int i = 0; i < length / 2; i++) {
						arrb.add(readByte(2));
					}
					int addstat, level, classtype = 0, status = 0, unknown2, unknown3 = 0, str = 0, cha = 0, inte = 0,
							dex = 0, con = 0, wis = 0;

					/*
					 * 0000: b7 e4 01 0e 00
					 * 
					 * 08 5f 10 07 18 10 20 01 28 10 30 1f 50 10 5a
					 * 
					 * b7 e4 01 0e 00 08 5f 10 07 18 10 20 01 28 01 30 1e 50 11
					 * 00
					 */
					for (byte[] b : arrb) {
						switch (b[0]) {
						case 0x08:
							level = b[1] & 0xff;
							break;// 모름
						case 0x10:
							classtype = b[1] & 0xff;
							break;// 클래스 타입
						case 0x18:
							status = b[1] & 0xff;
							break;// 초기상태 = 1 / 스탯변경상태 = 8

						case 0x20:
							unknown2 = b[1] & 0xff;
							break;// 모름
						case 0x28:
							unknown3 = b[1] & 0xff;
							break;// 모름

						case 0x30:
							str = b[1] & 0xff;
							break;// 힘
						case 0x38:
							inte = b[1] & 0xff;
							break;// 인트
						case 0x40:
							wis = b[1] & 0xff;
							break;// 위즈
						case 0x48:
							dex = b[1] & 0xff;
							break;// 덱
						case 0x50:
							con = b[1] & 0xff;
							break;// 콘
						case 0x58:
							cha = b[1] & 0xff;
							break;// 카리

						default:
							int i = 0;
							try {
								i = b[0] & 0xff;
							} catch (Exception e) {
							}
							System.out.println("[스탯관련 정의되지 않은 패킷] op : " + i);
							break;
						}
					}

					if (str != 0 && unknown3 != 1) {
						client.sendPacket(new S_NewCreateItem(0x01e3, status, str, con, "힘", classtype, null));
					}
					if (dex != 0) {
						client.sendPacket(new S_NewCreateItem(0x01e3, status, dex, 0, "덱", classtype, null));
					}
					if (con != 0 && unknown3 != 16) {
						client.sendPacket(new S_NewCreateItem(0x01e3, status, con, str, "콘", classtype, null));
					}
					if (inte != 0) {
						client.sendPacket(new S_NewCreateItem(0x01e3, status, inte, 0, "인트", classtype, null));
					}
					if (wis != 0) {
						client.sendPacket(new S_NewCreateItem(0x01e3, status, wis, 0, "위즈", classtype, null));
					}
					if (cha != 0) {
						client.sendPacket(new S_NewCreateItem(0x01e3, status, cha, 0, "카리", classtype, null));
					}

				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		} catch (Exception e) {

		} finally {
			clear();
		}
	}

	public static final int[] hextable = { 0x80, 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89, 0x8a, 0x8b, 0x8c,
			0x8d, 0x8e, 0x8f, 0x90, 0x91, 0x92, 0x93, 0x94, 0x95, 0x96, 0x97, 0x98, 0x99, 0x9a, 0x9b, 0x9c, 0x9d, 0x9e,
			0x9f, 0xa0, 0xa1, 0xa2, 0xa3, 0xa4, 0xa5, 0xa6, 0xa7, 0xa8, 0xa9, 0xaa, 0xab, 0xac, 0xad, 0xae, 0xaf, 0xb0,
			0xb1, 0xb2, 0xb3, 0xb4, 0xb5, 0xb6, 0xb7, 0xb8, 0xb9, 0xba, 0xbb, 0xbc, 0xbd, 0xbe, 0xbf, 0xc0, 0xc1, 0xc2,
			0xc3, 0xc4, 0xc5, 0xc6, 0xc7, 0xc8, 0xc9, 0xca, 0xcb, 0xcc, 0xcd, 0xce, 0xcf, 0xd0, 0xd1, 0xd2, 0xd3, 0xd4,
			0xd5, 0xd6, 0xd7, 0xd8, 0xd9, 0xda, 0xdb, 0xdc, 0xdd, 0xde, 0xdf, 0xe0, 0xe1, 0xe2, 0xe3, 0xe4, 0xe5, 0xe6,
			0xe7, 0xe8, 0xe9, 0xea, 0xeb, 0xec, 0xed, 0xee, 0xef, 0xf0, 0xf1, 0xf2, 0xf3, 0xf4, 0xf5, 0xf6, 0xf7, 0xf8,
			0xf9, 0xfa, 0xfb, 0xfc, 0xfd, 0xfe, 0xff };

	private String byteWrite1(long value) {
		long temp = value / 128;
		StringBuffer sb = new StringBuffer();
		if (temp > 0) {

			sb.append(hextable[(int) value % 128] + " ");
			while (temp >= 128) {
				// System.out.println(temp);
				sb.append(hextable[(int) temp % 128] + " ");
				temp = temp / 128;
			}
			if (temp > 0)
				sb.append((int) temp + " ");
		} else {
			if (value == 0) {
				sb.append(0);
			} else {
				sb.append(hextable[(int) value] + " ");
				sb.append(0);
			}
		}
		System.out.println(sb.toString());
		return sb.toString();
	}

	private static final int lv1doll[] = { 41248, 41250, 430000, 430002, 430004, 600241 };
	private static final int lv2doll[] = { 430001, 41249, 430500, 500108, 500109, 600242 };
	private static final int lv3doll[] = { 500205, 500204, 500203, 60324, 500110, 600243 };
	private static final int lv4doll[] = { 500202, 5000035, 600244, 600245, 41551, 41552, 19006 };
	private static final int lv5doll[] = { 600246, 600247, 41553, 19005, 19007, 19008, 19009 };
	private static final int lv5dollDragon[] = {73001,73002,73003,73004,950014,950015,950016,950017,950018,950019};
	private static final int lv6doll[]= {950000,950001,950002,950003,950004,950005};
	private static final int lv7doll[] = {950006,950007,950008,950009,950010,950011,950012};


	public int item_doll_code(int item) {
		int doll = 0;
		switch (item) {
		/*
		 * 41248 버그베어 41250 늑대인간 430000 돌골렘 430002 크러스트시안 430004 에티
		 */
		case 41248:
			doll = 1;
			break;
		case 41250:
			doll = 1;
			break;
		case 430000:
			doll = 1;
			break;
		case 430002:
			doll = 1;
			break;
		case 430004:
			doll = 1;
			break;
		case 600241:
			doll = 1;
			break;// 목각
		/*
		 * 430001 장로 41249 서큐 430500 코카 500108 인어 500109 눈사람
		 */
		case 430001:
			doll = 2;
			break;
		case 41249:
			doll = 2;
			break;
		case 430500:
			doll = 2;
			break;
		case 500108:
			doll = 2;
			break;
		case 500109:
			doll = 2;
			break;
		case 600242:
			doll = 2;
			break;// 라바골렘
		/*
		 * 500205 서큐퀸 500204 흑장로 500203 자이언트 60324 드레이크
		 */
		case 500205:
			doll = 3;
			break;
		case 500204:
			doll = 3;
			break;
		case 500203:
			doll = 3;
			break;
		case 60324:
			doll = 3;
			break;
		case 500110:
			doll = 3;
			break;
		case 600243:
			doll = 3;
			break;// 다이아골렘
		/*
		 * 500202 싸이클롭스 5000035 리치
		 */
		case 500202:
			doll = 4;
			break;
		case 5000035:
			doll = 4;
			break;
		case 600244:
			doll = 4;
			break;// 시어
		case 600245:
			doll = 4;
			break;// 나발
		case 41551:
			doll = 4;
			break;
		case 41552:
			doll = 4;
			break;
		case 19006:
			doll = 4;
			break;

		case 19005:
			doll = 5;
			break;
		case 41553:
			doll = 5;
			break;
		case 600246:
			doll = 5;
			break;// 데몬
		case 600247:
			doll = 5;
			break;// 데스
		case 19007:
			doll = 5;
			break;
		case 19008:
			doll = 5;
			break;
		case 19009:
			doll = 5;
			break;
		default:
			break;
		}
		return doll;
	}

	public int doll_item_code(int doll) {
		int item = 0;
		switch (doll) {
		/*
		 * 150 버그베어 151 늑대인간 152 돌골렘 153 크러스트시안 154 에티
		 */
		case 150:
			item = 41248;
			break;
		case 151:
			item = 41250;
			break;
		case 152:
			item = 430000;
			break;
		case 153:
			item = 430002;
			break;
		case 154:
			item = 430004;
			break;

		/*
		 * 155 장로 156 서큐 157 코카 158 인어 159 눈사람
		 */
		case 155:
			item = 430001;
			break;
		case 156:
			item = 41249;
			break;
		case 157:
			item = 430500;
			break;
		case 158:
			item = 500108;
			break;
		case 159:
			item = 500109;
			break;

		/*
		 * 160 서큐퀸 161 흑장로 162 자이언트 163 드레이크
		 */
		case 160:
			item = 500205;
			break;
		case 161:
			item = 500204;
			break;
		case 162:
			item = 500203;
			break;
		case 163:
			item = 60324;
			break;
		case 176:
			item = 500110;
			break;
		/*
		 * 164 싸이클롭스 165 리치
		 */
		case 164:
			item = 500202;
			break;
		case 165:
			item = 5000035;
			break;
		default:
			break;
		}
		return item;
	}

	static final int div1 = 128 * 128 * 128 * 128;
	static final int div2 = 128 * 128 * 128;
	static final int div3 = 128 * 128;
	static final int div4 = 128;

	private static String HexToDex(int data, int digits) {
		String number = Integer.toHexString(data);
		for (int i = number.length(); i < digits; i++)
			number = "0" + number;
		return number;
	}

	private long Decbyte(byte[] data) {
		long[] temp = new long[data.length];
		for (int i = 0; i < data.length; i++) {
			if (data[i] > 128) {
				temp[i] = ((data[i] - 128));
			} else if (data[i] == 128) {
				temp[i] = 128;
			} else {
				temp[i] = data[i];
			}
		}

		if (temp.length == 4) {
			temp[0] *= div1;
			temp[1] *= div2;
			temp[2] *= div3;
			temp[3] *= div4;
		}
		if (temp.length == 3) {
			temp[0] *= div1;
			temp[1] *= div2;
			temp[2] *= div3;
		}
		if (temp.length == 2) {
			temp[0] *= div1;
			temp[1] *= div2;
		}
		if (temp.length == 1) {
			temp[0] *= div1;
		}
		long yes = 0;
		for (int i = 0; i < temp.length; i++) {
			yes += temp[i];
		}

		System.out.println(yes);
		return 0;
	}

	public int Nexttam(String encobj) {
		Connection con = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		int day = 0;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("SELECT day FROM `tam` WHERE encobjid = ? order by id asc limit 1"); // 케릭터
																												// 테이블에서
																												// 군주만
																												// 골라와서
			pstm.setString(1, encobj);
			rs = pstm.executeQuery();
			while (rs.next()) {
				day = rs.getInt("Day");
			}
		} catch (SQLException e) {
			// _log.log(Level.SEVERE, e.getLocalizedMessage(), e);
		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
		return day;
	}

	public int TamCharid(String encobj) {
		Connection con = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		int objid = 0;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("SELECT objid FROM `tam` WHERE encobjid = ? order by id asc limit 1"); // 케릭터
																												// 테이블에서
																												// 군주만
																												// 골라와서
			pstm.setString(1, encobj);
			rs = pstm.executeQuery();
			while (rs.next()) {
				objid = rs.getInt("objid");
			}
		} catch (SQLException e) {
			// _log.log(Level.SEVERE, e.getLocalizedMessage(), e);
		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
		return objid;
	}

	public void tamcancle(String objectId) {
		Connection con = null;
		PreparedStatement pstm = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("delete from tam where encobjid = ? order by id asc limit 1");
			pstm.setString(1, objectId);
			pstm.executeUpdate();
		} catch (SQLException e) {
			// _log.log(Level.SEVERE, e.getLocalizedMessage(), e);
		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	public void commit(String com, String name, int count) {
		Connection con = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("SELECT * FROM request_log WHERE command=?");
			pstm.setString(1, com);
			rs = pstm.executeQuery();
			Connection con2 = null;
			PreparedStatement pstm2 = null;
			try {
				con2 = L1DatabaseFactory.getInstance().getConnection();
				if (rs.next()) {
					int amount = rs.getInt("count");
					pstm2 = con2.prepareStatement("UPDATE request_log SET count=? WHERE command=?");
					pstm2.setInt(1, amount + count);
					pstm2.setString(2, com);
				} else {
					pstm2 = con2.prepareStatement("INSERT INTO request_log SET command=?, count=?");
					pstm2.setString(1, com);
					pstm2.setInt(2, count);
				}
				pstm2.executeUpdate();
			} catch (SQLException e) {
			} finally {
				SQLUtil.close(pstm2);
				SQLUtil.close(con2);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(rs);
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	public static int[] stat = new int[] { 0, 0, 0, 0, 0 };

	private int[] mapOrders() {
		return new int[] { 1, 2, 15, 500 };
		// { 1, 2, 15, 41, 45, 100, 500 };
	}

	private String[] mapNames() {
		return new String[] { "기란 감옥/지배의결계", "말하는섬 던전", "라스타바드 폐허", "상아탑: 발록 진영" };
	}

	private int[] mapTimes(L1PcInstance pc) {
		int[] mapRemainTimes = new int[mapNames().length];
		int i = 0;
		mapRemainTimes[(i++)] = (60 * 60 * 3) - pc.getgirantime();
		mapRemainTimes[(i++)] = 3600 - pc.get말던time();
		mapRemainTimes[(i++)] = 7200 - pc.getravatime();
		mapRemainTimes[(i++)] = 3600 - pc.getivorytime();
		return mapRemainTimes;
	}

	private int[] maxmapTimes() {
		int[] mapRemainTimes = new int[mapNames().length];
		int i = 0;
		mapRemainTimes[(i++)] = 60 * 60 * 3;
		mapRemainTimes[(i++)] = 3600;
		mapRemainTimes[(i++)] = 7200;
		mapRemainTimes[(i++)] = 3600;
		return mapRemainTimes;
	}

	@Override
	public String getType() {
		return C_EXTENDED_PROTOBUF;
	}
}
