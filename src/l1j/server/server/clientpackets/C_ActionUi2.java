package l1j.server.server.clientpackets;

import java.util.Random;

import server.LineageClient;
import l1j.server.Config;
import l1j.server.server.datatables.EnchantWeaponChance;
import l1j.server.server.datatables.ItemTable;
import l1j.server.server.datatables.NpcActionTable;
import l1j.server.server.model.L1Inventory;
import l1j.server.server.model.L1Object;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1ItemInstance;
import l1j.server.server.model.Instance.L1NpcInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.model.npc.L1NpcHtml;
import l1j.server.server.model.npc.action.L1NpcAction;
import l1j.server.server.serverpackets.S_ACTION_UI;
import l1j.server.server.serverpackets.S_NPCTalkReturn;
import l1j.server.server.serverpackets.S_ServerMessage;
import l1j.server.server.serverpackets.S_SkillSound;
import l1j.server.server.serverpackets.S_SystemMessage;
import l1j.server.server.templates.L1EnchantWeaponChance;

public class C_ActionUi2 extends ClientBasePacket {

	Random _Random = new Random(System.nanoTime());

	private static final String C_ACTION_UI2 = "[C] C_ActionUi2";
	
	private static final int CRAFT_OK = 0x3a; // 첫번째 타입

	public C_ActionUi2(byte abyte0[], LineageClient client) {
		super(abyte0);
		int type = readH();
		L1PcInstance pc = client.getActiveChar();
		if (pc == null || pc.isGhost())
			return;
		int objectId = 0, itemtype = 0, itemcount = 0;
		L1Object obj;
		String s = null;
		L1NpcInstance npc;
		L1NpcAction action;
		//System.out.println("액션 > " + type);
		switch (type) {

		case CRAFT_OK:
		case 92:
			readH(); // size;
			readC(); // dummy
			objectId = read4(read_size());
			readC(); // dummy
			itemtype = read4(read_size());
			readC(); // dummy
			itemcount = read4(read_size());
			if (itemcount < 1 || itemcount > 99) {
				return;
			}

			s = Integer.toString(itemtype);
			obj = L1World.getInstance().findObject(objectId);
			int Chance = _Random.nextInt(100) + 1;
			if (obj instanceof L1NpcInstance) {
				npc = (L1NpcInstance) obj;
				if(itemtype == 5020){
					if(pc.getInventory().checkItem(57000, 8)){
						pc.getInventory().consumeItem(57000, 8);
						인첸트지급(pc, 40222, 1, 0);
						return;
					}
				} else if(itemtype == 5021){
					if(pc.getInventory().checkItem(57001, 8)){
						pc.getInventory().consumeItem(57001, 8);
						인첸트지급(pc, 41148, 1, 0);
						return;
					}
				} else if(itemtype == 5022){
					if(pc.getInventory().checkItem(57002, 8)){
						pc.getInventory().consumeItem(57002, 8);
						인첸트지급(pc, 41149, 1, 0);
						return;
					}
				} else if(itemtype == 5023){
					if(pc.getInventory().checkItem(57003, 8)){
						pc.getInventory().consumeItem(57003, 8);
						인첸트지급(pc, 60199, 1, 0);
						return;
					}
				} else if(itemtype == 5024){
					if(pc.getInventory().checkItem(57004, 8)){
						pc.getInventory().consumeItem(57004, 8);
						인첸트지급(pc, 210125, 1, 0);
						return;
					}
				} else if(itemtype == 5025){
					if(pc.getInventory().checkItem(57005, 8)){
						pc.getInventory().consumeItem(57005, 8);
						인첸트지급(pc, 40219, 1, 0);
						return;
					}
				} else if(itemtype == 5026){
					if(pc.getInventory().checkItem(57006, 8)){
						pc.getInventory().consumeItem(57006, 8);
						인첸트지급(pc, 7304, 1, 0);
						return;
					}
				} else if(itemtype == 5027){
					if(pc.getInventory().checkItem(57007, 8)){
						pc.getInventory().consumeItem(57007, 8);
						인첸트지급(pc, 7306, 1, 0);
						return;
					}
				} else if(itemtype == 5028){
					if(pc.getInventory().checkItem(57008, 8)){
						pc.getInventory().consumeItem(57008, 8);
						인첸트지급(pc, 7305, 1, 0);
						return;
					}
				} else if(itemtype == 5029){
					if(pc.getInventory().checkItem(57009, 8)){
						pc.getInventory().consumeItem(57009, 8);
						인첸트지급(pc, 72011, 1, 0);
						return;
					}
				} else if(itemtype == 5030){
					if(pc.getInventory().checkItem(57010, 8)){
						pc.getInventory().consumeItem(57010, 8);
						인첸트지급(pc, 72014, 1, 0);
						return;
					}
				} else if(itemtype == 5031){
					if(pc.getInventory().checkItem(57011, 8)){
						pc.getInventory().consumeItem(57011, 8);
						인첸트지급(pc, 72017, 1, 0);
						return;
					}
				} else if(itemtype == 5032){
					if(pc.getInventory().checkItem(57012, 8)){
						pc.getInventory().consumeItem(57012, 8);
						인첸트지급(pc, 72022, 1, 0);
						return;
					}
				} else if(itemtype == 5050){
					if(pc.getInventory().checkItem(57015, 8)){
						pc.getInventory().consumeItem(57015, 8);
						인첸트지급(pc, 9, 1, 0);
						return;
					}
				} else if(itemtype == 5051){
					if(pc.getInventory().checkItem(57016, 8)){
						pc.getInventory().consumeItem(57016, 8);
						인첸트지급(pc, 59, 1, 0);
						return;
					}
				} else if(itemtype == 5052){
					if(pc.getInventory().checkItem(57017, 8)){
						pc.getInventory().consumeItem(57017, 8);
						인첸트지급(pc, 76, 1, 0);
						return;
					}
				} else if(itemtype == 5053){
					if(pc.getInventory().checkItem(57018, 8)){
						pc.getInventory().consumeItem(57018, 8);
						인첸트지급(pc, 124, 1, 0);
						return;
					}
				} else if(itemtype == 5054){
					if(pc.getInventory().checkItem(57019, 8)){
						pc.getInventory().consumeItem(57019, 8);
						인첸트지급(pc, 121, 1, 0);
						return;
					}
				} else if(itemtype == 5055){
					if(pc.getInventory().checkItem(57020, 8)){
						pc.getInventory().consumeItem(57020, 8);
						인첸트지급(pc, 119, 1, 0);
						return;
					}
				} else if(itemtype == 5056){
					if(pc.getInventory().checkItem(57021, 8)){
						pc.getInventory().consumeItem(57021, 8);
						인첸트지급(pc, 262, 1, 0);
						return;
					}
				} else if(itemtype == 5057){
					if(pc.getInventory().checkItem(57022, 8)){
						pc.getInventory().consumeItem(57022, 8);
						인첸트지급(pc, 6000, 1, 0);
						return;
					}
				} else if(itemtype == 5058){
					if(pc.getInventory().checkItem(57023, 8)){
						pc.getInventory().consumeItem(57023, 8);
						인첸트지급(pc, 6001, 1, 0);
						return;
					}
				} else if(itemtype == 5059){
					if(pc.getInventory().checkItem(57024, 8)){
						pc.getInventory().consumeItem(57024, 8);
						인첸트지급(pc, 58, 1, 0);
						return;
					}
				} else if(itemtype == 5060){
					if(pc.getInventory().checkItem(57025, 8)){
						pc.getInventory().consumeItem(57025, 8);
						인첸트지급(pc, 54, 1, 0);
						return;
					}
				} else if(itemtype == 5061){
					if(pc.getInventory().checkItem(57026, 8)){
						pc.getInventory().consumeItem(57026, 8);
						인첸트지급(pc, 291, 1, 0);
						return;
					}
				} else if(itemtype == 5062){
					if(pc.getInventory().checkItem(57027, 8)){
						pc.getInventory().consumeItem(57027, 8);
						인첸트지급(pc, 6101, 1, 0);
						return;
					}
				} else if(itemtype == 5075){ //리치로브
					if(pc.getInventory().checkItem(57028, 8)){
						pc.getInventory().consumeItem(57028, 8);
						인첸트지급(pc, 20107, 1, 0);
						return;
					}
				} else if(itemtype == 5076){
					if(pc.getInventory().checkItem(57029, 8)){
						pc.getInventory().consumeItem(57029, 8);
						인첸트지급(pc, 20100, 1, 0);
						return;
					}
				} else if(itemtype == 5077){
					if(pc.getInventory().checkItem(57030, 8)){
						pc.getInventory().consumeItem(57030, 8);
						인첸트지급(pc, 20160, 1, 0);
						return;
					}
				} else if(itemtype == 5078){
					if(pc.getInventory().checkItem(57031, 8)){
						pc.getInventory().consumeItem(57031, 8);
						인첸트지급(pc, 20150, 1, 0);
						return;
					}
				} else if(itemtype == 5079){
					if(pc.getInventory().checkItem(57032, 8)){
						pc.getInventory().consumeItem(57032, 8);
						인첸트지급(pc, 20049, 1, 0);
						return;
					}
				} else if(itemtype == 5080){
					if(pc.getInventory().checkItem(57033, 8)){
						pc.getInventory().consumeItem(57033, 8);
						인첸트지급(pc, 20050, 1, 0);
						return;
					}
				} else if(itemtype == 5081){
					if(pc.getInventory().checkItem(57034, 8)){
						pc.getInventory().consumeItem(57034, 8);
						인첸트지급(pc, 20079, 1, 0);
						return;
					}
				} else if(itemtype == 5082){
					if(pc.getInventory().checkItem(57035, 8)){
						pc.getInventory().consumeItem(57035, 8);
						인첸트지급(pc, 20077, 1, 0);
						return;
					}
				} else if(itemtype == 5083){
					if(pc.getInventory().checkItem(57036, 8)){
						pc.getInventory().consumeItem(57036, 8);
						인첸트지급(pc, 20074, 1, 0);
						return;
					}
				} else if(itemtype == 5084){
					if(pc.getInventory().checkItem(57037, 8)){
						pc.getInventory().consumeItem(57037, 8);
						인첸트지급(pc, 22009, 1, 0);
						return;
					}
				} else if(itemtype == 5085){
					if(pc.getInventory().checkItem(57038, 8)){
						pc.getInventory().consumeItem(57038, 8);
						인첸트지급(pc, 21093, 1, 0);
						return;
					}
				} else if(itemtype == 5086){
					if(pc.getInventory().checkItem(57039, 8)){
						pc.getInventory().consumeItem(57039, 8);
						인첸트지급(pc, 20198, 1, 0);
						return;
					}
				} else if(itemtype == 5087){
					if(pc.getInventory().checkItem(57040, 8)){
						pc.getInventory().consumeItem(57040, 8);
						인첸트지급(pc, 20214, 1, 0);
						return;
					}
				} else if(itemtype == 5088){
					if(pc.getInventory().checkItem(57041, 8)){
						pc.getInventory().consumeItem(57041, 8);
						인첸트지급(pc, 20216, 1, 0);
						return;
					}
				} else if(itemtype == 5089){
					if(pc.getInventory().checkItem(57042, 8)){
						pc.getInventory().consumeItem(57042, 8);
						인첸트지급(pc, 30219, 1, 0);
						return;
					}
				} else if(itemtype == 5090){
					if(pc.getInventory().checkItem(57043, 8)){
						pc.getInventory().consumeItem(57043, 8);
						인첸트지급(pc, 20166, 1, 0);
						return;
					}
				} else if(itemtype == 5091){
					if(pc.getInventory().checkItem(57044, 8)){
						pc.getInventory().consumeItem(57044, 8);
						인첸트지급(pc, 20184, 1, 0);
						return;
					}
				} else if(itemtype == 5092){
					if(pc.getInventory().checkItem(57045, 8)){
						pc.getInventory().consumeItem(57045, 8);
						인첸트지급(pc, 20186, 1, 0);
						return;
					}
				} else if(itemtype == 5093){
					if(pc.getInventory().checkItem(57046, 8)){
						pc.getInventory().consumeItem(57046, 8);
						인첸트지급(pc, 20017, 1, 0);
						return;
					}
				} else if(itemtype == 5094){
					if(pc.getInventory().checkItem(57047, 8)){
						pc.getInventory().consumeItem(57047, 8);
						인첸트지급(pc, 21122, 1, 0);
						return;
					}
				} else if(itemtype == 5095){
					if(pc.getInventory().checkItem(57048, 8)){
						pc.getInventory().consumeItem(57048, 8);
						인첸트지급(pc, 20010, 1, 0);
						return;
					}
				} else if(itemtype == 5096){
					if(pc.getInventory().checkItem(57049, 8)){
						pc.getInventory().consumeItem(57049, 8);
						인첸트지급(pc, 20025, 1, 0);
						return;
					}
				} else if(itemtype == 5097){
					if(pc.getInventory().checkItem(57050, 8)){
						pc.getInventory().consumeItem(57050, 8);
						인첸트지급(pc, 20040, 1, 0);
						return;
					}
				} else if(itemtype == 5098){
					if(pc.getInventory().checkItem(57051, 8)){
						pc.getInventory().consumeItem(57051, 8);
						인첸트지급(pc, 20018, 1, 0);
						return;
					}
				} else if(itemtype == 5099){
					if(pc.getInventory().checkItem(57052, 8)){
						pc.getInventory().consumeItem(57052, 8);
						인첸트지급(pc, 20029, 1, 0);
						return;
					}
				} else if(itemtype == 5100){
					if(pc.getInventory().checkItem(57053, 8)){
						pc.getInventory().consumeItem(57053, 8);
						인첸트지급(pc, 20041, 1, 0);
						return;
					}
				} else if(itemtype == 5101){
					if(pc.getInventory().checkItem(57054, 8)){
						pc.getInventory().consumeItem(57054, 8);
						인첸트지급(pc, 500214, 1, 0);
						return;
					}
				} else if(itemtype == 5102){
					if(pc.getInventory().checkItem(57055, 8)){
						pc.getInventory().consumeItem(57055, 8);
						인첸트지급(pc, 20218, 1, 0);
						return;
					}
				} else if(itemtype == 5103){
					if(pc.getInventory().checkItem(57056, 8)){
						pc.getInventory().consumeItem(57056, 8);
						인첸트지급(pc, 20048, 1, 0);
						return;
					}
				} else if(itemtype == 5104){
					if(pc.getInventory().checkItem(57057, 8)){
						pc.getInventory().consumeItem(57057, 8);
						인첸트지급(pc, 9113, 1, 0);
						return;
					}
				} else if(itemtype == 5105){
					if(pc.getInventory().checkItem(57058, 8)){
						pc.getInventory().consumeItem(57058, 8);
						인첸트지급(pc, 10100, 1, 0);
						return;
					}
				} else if(itemtype == 5106){
					if(pc.getInventory().checkItem(57059, 8)){
						pc.getInventory().consumeItem(57059, 8);
						인첸트지급(pc, 10101, 1, 0);
						return;
					}
				} else if(itemtype == 5107){
					if(pc.getInventory().checkItem(57060, 8)){
						pc.getInventory().consumeItem(57060, 8);
						인첸트지급(pc, 10102, 1, 0);
						return;
					}
				} else if(itemtype == 5108){
					if(pc.getInventory().checkItem(57061, 8)){
						pc.getInventory().consumeItem(57061, 8);
						인첸트지급(pc, 10103, 1, 0);
						return;
					}
				} else if(itemtype == 5109){
					if(pc.getInventory().checkItem(57062, 8)){
						pc.getInventory().consumeItem(57062, 8);
						인첸트지급(pc, 500221, 1, 0);
						return;
					}
				} else if(itemtype == 5110){
					if(pc.getInventory().checkItem(57063, 8)){
						pc.getInventory().consumeItem(57063, 8);
						인첸트지급(pc, 500222, 1, 0);
						return;
					}
				} else if(itemtype == 5111){
					if(pc.getInventory().checkItem(57064, 8)){
						pc.getInventory().consumeItem(57064, 8);
						인첸트지급(pc, 500223, 1, 0);
						return;
					}
				} else if(itemtype == 5112){ //격분
					if(pc.getInventory().checkItem(57065, 8)){
						pc.getInventory().consumeItem(57065, 8);
						인첸트지급(pc, 130220, 1, 0);
						return;
					}
				} else if(itemtype == 5130){ //도펠목
					if(pc.getInventory().checkItem(57066, 8)){
						pc.getInventory().consumeItem(57066, 8);
						인첸트지급(pc, 20250, 1, 0);
						return;
					}
				} else if(itemtype == 5131){
					if(pc.getInventory().checkItem(57067, 8)){
						pc.getInventory().consumeItem(57067, 8);
						인첸트지급(pc, 21258, 1, 0);
						return;
					}
				} else if(itemtype == 5132){
					if(pc.getInventory().checkItem(57068, 8)){
						pc.getInventory().consumeItem(57068, 8);
						인첸트지급(pc, 21260, 1, 0);
						return;
					}
				} else if(itemtype == 5133){
					if(pc.getInventory().checkItem(57069, 8)){
						pc.getInventory().consumeItem(57069, 8);
						인첸트지급(pc, 21268, 1, 0);
						return;
					}
				} else if(itemtype == 5134){
					if(pc.getInventory().checkItem(57070, 8)){
						pc.getInventory().consumeItem(57070, 8);
						인첸트지급(pc, 21167, 1, 0);
						return;
					}
				} else if(itemtype == 5135){
					if(pc.getInventory().checkItem(57071, 8)){
						pc.getInventory().consumeItem(57071, 8);
						인첸트지급(pc, 20278, 1, 0);
						return;
					}
				} else if(itemtype == 5136){
					if(pc.getInventory().checkItem(57072, 8)){
						pc.getInventory().consumeItem(57072, 8);
						인첸트지급(pc, 20277, 1, 0);
						return;
					}
				} else if(itemtype == 5137){
					if(pc.getInventory().checkItem(57073, 8)){
						pc.getInventory().consumeItem(57073, 8);
						인첸트지급(pc, 20279, 1, 0);
						return;
					}
				} else if(itemtype == 5138){
					if(pc.getInventory().checkItem(57074, 8)){
						pc.getInventory().consumeItem(57074, 8);
						인첸트지급(pc, 22267, 1, 0);
						return;
					}
				} else if(itemtype == 5139){
					if(pc.getInventory().checkItem(57075, 8)){
						pc.getInventory().consumeItem(57075, 8);
						인첸트지급(pc, 20298, 1, 0);
						return;
					}
				} else if(itemtype == 5140){
					if(pc.getInventory().checkItem(57076, 8)){
						pc.getInventory().consumeItem(57076, 8);
						인첸트지급(pc, 420008, 1, 0);
						return;
					}
				} else if(itemtype == 5141){
					if(pc.getInventory().checkItem(57077, 8)){
						pc.getInventory().consumeItem(57077, 8);
						인첸트지급(pc, 420009, 1, 0);
						return;
					}
				} else if(itemtype == 5142){
					if(pc.getInventory().checkItem(57078, 8)){
						pc.getInventory().consumeItem(57078, 8);
						인첸트지급(pc, 21168, 1, 0);
						return;
					}
				} else if(itemtype == 5143){
					if(pc.getInventory().checkItem(57079, 8)){
						pc.getInventory().consumeItem(57079, 8);
						인첸트지급(pc, 30229, 1, 0);
						return;
					}
				} else if(itemtype == 5144){
					if(pc.getInventory().checkItem(57080, 8)){
						pc.getInventory().consumeItem(57080, 8);
						인첸트지급(pc, 7214, 1, 0);
						return;
					}
				} else if(itemtype == 5145){
					if(pc.getInventory().checkItem(57081, 8)){
						pc.getInventory().consumeItem(57081, 8);
						인첸트지급(pc, 7215, 1, 0);
						return;
					}
				} else if(itemtype == 5146){
					if(pc.getInventory().checkItem(57082, 8)){
						pc.getInventory().consumeItem(57082, 8);
						인첸트지급(pc, 20314, 1, 0);
						return;
					}
				} else if(itemtype == 5147){
					if(pc.getInventory().checkItem(57083, 8)){
						pc.getInventory().consumeItem(57083, 8);
						인첸트지급(pc, 22002, 1, 0);
						return;
					}
				} else if(itemtype == 5148){
					if(pc.getInventory().checkItem(57084, 8)){
						pc.getInventory().consumeItem(57084, 8);
						인첸트지급(pc, 20320, 1, 0);
						return;
					}
				} else if(itemtype == 5149){
					if(pc.getInventory().checkItem(57085, 8)){
						pc.getInventory().consumeItem(57085, 8);
						인첸트지급(pc, 21281, 1, 0);
						return;
					}
				} else if(itemtype == 5150){
					if(pc.getInventory().checkItem(57086, 8)){
						pc.getInventory().consumeItem(57086, 8);
						인첸트지급(pc, 21267, 1, 0);
						return;
					}
				} else if(itemtype == 5151){
					if(pc.getInventory().checkItem(57087, 8)){
						pc.getInventory().consumeItem(57087, 8);
						인첸트지급(pc, 10104, 1, 0);
						return;
					}
				} else if(itemtype == 5152){
					if(pc.getInventory().checkItem(57088, 8)){
						pc.getInventory().consumeItem(57088, 8);
						인첸트지급(pc, 10105, 1, 0);
						return;
					}
				} else if(itemtype == 5171){ //대법관 사념조각
					if(pc.getInventory().checkItem(57089, 8)){
						pc.getInventory().consumeItem(57089, 8);
						인첸트지급(pc, 73010, 1, 0);
						return;
					}
				} else if(itemtype == 5172){
					if(pc.getInventory().checkItem(57090, 8)){
						pc.getInventory().consumeItem(57090, 8);
						인첸트지급(pc, 73011, 1, 0);
						return;
					}
				} else if(itemtype == 5173){
					if(pc.getInventory().checkItem(57091, 8)){
						pc.getInventory().consumeItem(57091, 8);
						인첸트지급(pc, 40466, 1, 0);
						return;
					}
				} else if(itemtype == 5174){
					if(pc.getInventory().checkItem(57092, 8)){
						pc.getInventory().consumeItem(57092, 8);
						인첸트지급(pc, 60201, 1, 0);
						return;
					}
				} else if(itemtype == 5175){
					if(pc.getInventory().checkItem(57093, 8)){
						pc.getInventory().consumeItem(57093, 8);
						인첸트지급(pc, 40280, 1, 0);
						return;
					}
				} else if(itemtype == 5176){
					if(pc.getInventory().checkItem(57094, 8)){
						pc.getInventory().consumeItem(57094, 8);
						인첸트지급(pc, 40281, 1, 0);
						return;
					}
				} else if(itemtype == 5177){
					if(pc.getInventory().checkItem(57095, 8)){
						pc.getInventory().consumeItem(57095, 8);
						인첸트지급(pc, 40282, 1, 0);
						return;
					}
				} else if(itemtype == 5178){
					if(pc.getInventory().checkItem(57096, 8)){
						pc.getInventory().consumeItem(57096, 8);
						인첸트지급(pc, 40283, 1, 0);
						return;
					}
				} else if(itemtype == 5179){
					if(pc.getInventory().checkItem(57097, 8)){
						pc.getInventory().consumeItem(57097, 8);
						인첸트지급(pc, 40284, 1, 0);
						return;
					}
				} else if(itemtype == 5180){
					if(pc.getInventory().checkItem(57098, 8)){
						pc.getInventory().consumeItem(57098, 8);
						인첸트지급(pc, 40285, 1, 0);
						return;
					}
				} else if(itemtype == 5181){
					if(pc.getInventory().checkItem(57099, 8)){
						pc.getInventory().consumeItem(57099, 8);
						인첸트지급(pc, 40286, 1, 0);
						return;
					}
				} else if(itemtype == 5182){
					if(pc.getInventory().checkItem(57100, 8)){
						pc.getInventory().consumeItem(57100, 8);
						인첸트지급(pc, 40287, 1, 0);
						return;
					}
				} else if(itemtype == 5183){
					if(pc.getInventory().checkItem(57101, 8)){
						pc.getInventory().consumeItem(57101, 8);
						인첸트지급(pc, 40288, 1, 0);
						return;
					}
				} else if(itemtype == 5184){
					if(pc.getInventory().checkItem(57102, 8)){
						pc.getInventory().consumeItem(57102, 8);
						인첸트지급(pc, 40346, 1, 0);
						return;
					}
				} else if(itemtype == 5185){
					if(pc.getInventory().checkItem(57103, 8)){
						pc.getInventory().consumeItem(57103, 8);
						인첸트지급(pc, 40354, 1, 0);
						return;
					}
				} else if(itemtype == 5186){
					if(pc.getInventory().checkItem(57104, 8)){
						pc.getInventory().consumeItem(57104, 8);
						인첸트지급(pc, 40362, 1, 0);
						return;
					}
				} else if(itemtype == 5187){
					if(pc.getInventory().checkItem(57105, 8)){
						pc.getInventory().consumeItem(57105, 8);
						인첸트지급(pc, 40370, 1, 0);
						return;
					}
				} else if(itemtype == 5188){
					if(pc.getInventory().checkItem(57106, 8)){
						pc.getInventory().consumeItem(57106, 8);
						인첸트지급(pc, 75007, 1, 0);
						return;
					}
				} else if(itemtype == 5189){
					if(pc.getInventory().checkItem(57107, 8)){
						pc.getInventory().consumeItem(57107, 8);
						인첸트지급(pc, 75006, 1, 0);
						return;
					}
				} else if(itemtype == 5190){
					if(pc.getInventory().checkItem(57108, 8)){
						pc.getInventory().consumeItem(57108, 8);
						인첸트지급(pc, 400109, 1, 0);
						return;
					}
				} else if(itemtype == 5193){
					if(pc.getInventory().checkItem(57013, 8)){
						pc.getInventory().consumeItem(57013, 8);
						인첸트지급(pc, 72028, 1, 0);
						return;
					}
				} else if(itemtype == 5194){
					if(pc.getInventory().checkItem(57014, 8)){
						pc.getInventory().consumeItem(57014, 8);
						인첸트지급(pc, 72029, 1, 0);
						return;
					}
				} else if (itemtype == 1805 || itemtype == 1806 || itemtype == 1807 || itemtype == 1808 || itemtype == 1809) {
					if ((pc.getInventory().checkEnchantItem(63, 7, 1)
							|| pc.getInventory().checkEnchantItem(85, 7, 1)
							|| pc.getInventory().checkEnchantItem(165, 7, 1)
							|| pc.getInventory().checkEnchantItem(185, 7, 1)
							|| pc.getInventory().checkEnchantItem(63, 8, 1)
							|| pc.getInventory().checkEnchantItem(85, 8, 1)
							|| pc.getInventory().checkEnchantItem(165, 8, 1)
							|| pc.getInventory().checkEnchantItem(185, 8, 1)
							|| pc.getInventory().checkEnchantItem(63, 9, 1)
							|| pc.getInventory().checkEnchantItem(85, 9, 1)
							|| pc.getInventory().checkEnchantItem(165, 9, 1)
							|| pc.getInventory().checkEnchantItem(185, 9, 1)
							|| pc.getInventory().checkEnchantItem(63, 10, 1)
							|| pc.getInventory().checkEnchantItem(85, 10, 1)
							|| pc.getInventory().checkEnchantItem(165, 10, 1)
							|| pc.getInventory().checkEnchantItem(185, 10, 1)
							|| pc.getInventory().checkEnchantItem(63, 11, 1)
							|| pc.getInventory().checkEnchantItem(85, 11, 1)
							|| pc.getInventory().checkEnchantItem(165, 11, 1)
							|| pc.getInventory().checkEnchantItem(185, 11, 1))
							&& pc.getInventory().checkItem(40669, 1) && pc.getInventory().checkItem(40719, 1)
							&& pc.getInventory().checkItem(40707, 1) && pc.getInventory().checkItem(40445, 5)
							&& pc.getInventory().checkItem(40678, 1000) && pc.getInventory().checkItem(40677, 50)) {
						if ((pc.getInventory().consumeEnchantItem(63, 7, 1)
								|| pc.getInventory().consumeEnchantItem(85, 7, 1)
								|| pc.getInventory().consumeEnchantItem(165, 7, 1)
								|| pc.getInventory().consumeEnchantItem(185, 7, 1)
								|| pc.getInventory().consumeEnchantItem(63, 8, 1)
								|| pc.getInventory().consumeEnchantItem(85, 8, 1)
								|| pc.getInventory().consumeEnchantItem(165, 8, 1)
								|| pc.getInventory().consumeEnchantItem(185, 8, 1)
								|| pc.getInventory().consumeEnchantItem(63, 9, 1)
								|| pc.getInventory().consumeEnchantItem(85, 9, 1)
								|| pc.getInventory().consumeEnchantItem(165, 9, 1)
								|| pc.getInventory().consumeEnchantItem(185, 9, 1)
								|| pc.getInventory().consumeEnchantItem(63, 10, 1)
								|| pc.getInventory().consumeEnchantItem(85, 10, 1)
								|| pc.getInventory().consumeEnchantItem(165, 10, 1)
								|| pc.getInventory().consumeEnchantItem(185, 10, 1)
								|| pc.getInventory().consumeEnchantItem(63, 11, 1)
								|| pc.getInventory().consumeEnchantItem(85, 11, 1)
								|| pc.getInventory().consumeEnchantItem(165, 11, 1)
								|| pc.getInventory().consumeEnchantItem(185, 11, 1))) {
							;
						}
						int dwnum = 0;
						if (itemtype == 1805)
							dwnum = 413101;
						if (itemtype == 1806)
							dwnum = 413102;
						if (itemtype == 1807)
							dwnum = 413103;
						if (itemtype == 1808)
							dwnum = 413104;
						if (itemtype == 1809)
							dwnum = 413105;
						pc.getInventory().consumeItem(40669, 1);
						pc.getInventory().consumeItem(40719, 1);
						pc.getInventory().consumeItem(40707, 1);
						pc.getInventory().consumeItem(40445, 5);
						pc.getInventory().consumeItem(40678, 1000);
						pc.getInventory().consumeItem(40677, 50);
						if (Chance < 50) {
							인첸트지급(pc, dwnum, 1, 0);
						} else {
							pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패했습니다."), true);
						}
					}
				} else if (itemtype == 116) {
					if ((pc.getInventory().checkEnchantItem(5, 8, 1) // [+7]마력의
																		// 단검
							|| pc.getInventory().checkEnchantItem(6, 8, 1)
							|| pc.getInventory().checkEnchantItem(32, 8, 1)
							|| pc.getInventory().checkEnchantItem(37, 8, 1)
							|| pc.getInventory().checkEnchantItem(41, 8, 1)
							|| pc.getInventory().checkEnchantItem(42, 8, 1)
							|| pc.getInventory().checkEnchantItem(52, 8, 1)
							|| pc.getInventory().checkEnchantItem(64, 8, 1)
							|| pc.getInventory().checkEnchantItem(99, 8, 1)
							|| pc.getInventory().checkEnchantItem(104, 8, 1)
							|| pc.getInventory().checkEnchantItem(125, 8, 1)
							|| pc.getInventory().checkEnchantItem(129, 8, 1)
							|| pc.getInventory().checkEnchantItem(131, 8, 1)
							|| pc.getInventory().checkEnchantItem(145, 8, 1)
							|| pc.getInventory().checkEnchantItem(148, 8, 1)
							|| pc.getInventory().checkEnchantItem(180, 8, 1)
							|| pc.getInventory().checkEnchantItem(181, 8, 1))
							&& pc.getInventory().checkItem(40308, 5000000)) {
						if (pc.getInventory().consumeEnchantItem(5, 8, 1)
								|| pc.getInventory().consumeEnchantItem(6, 8, 1)
								|| pc.getInventory().consumeEnchantItem(32, 8, 1)
								|| pc.getInventory().consumeEnchantItem(37, 8, 1)
								|| pc.getInventory().consumeEnchantItem(41, 8, 1)
								|| pc.getInventory().consumeEnchantItem(42, 8, 1)
								|| pc.getInventory().consumeEnchantItem(52, 8, 1)
								|| pc.getInventory().consumeEnchantItem(64, 8, 1)
								|| pc.getInventory().consumeEnchantItem(99, 8, 1)
								|| pc.getInventory().consumeEnchantItem(104, 8, 1)
								|| pc.getInventory().consumeEnchantItem(125, 8, 1)
								|| pc.getInventory().consumeEnchantItem(129, 8, 1)
								|| pc.getInventory().consumeEnchantItem(131, 8, 1)
								|| pc.getInventory().consumeEnchantItem(145, 8, 1)
								|| pc.getInventory().consumeEnchantItem(148, 8, 1)
								|| pc.getInventory().consumeEnchantItem(180, 8, 1)
								|| pc.getInventory().consumeEnchantItem(181, 8, 1)) {
							;
						}
						pc.getInventory().consumeItem(40308, 5000000);
						인첸트지급(pc, 412002, 1, 7);
					}
				} else if (itemtype == 123) {
					if ((pc.getInventory().checkEnchantItem(5, 9, 1) // [+8]마력의
																		// 단검
							|| pc.getInventory().checkEnchantItem(6, 9, 1)
							|| pc.getInventory().checkEnchantItem(32, 9, 1)
							|| pc.getInventory().checkEnchantItem(37, 9, 1)
							|| pc.getInventory().checkEnchantItem(41, 9, 1)
							|| pc.getInventory().checkEnchantItem(42, 9, 1)
							|| pc.getInventory().checkEnchantItem(52, 9, 1)
							|| pc.getInventory().checkEnchantItem(64, 9, 1)
							|| pc.getInventory().checkEnchantItem(99, 9, 1)
							|| pc.getInventory().checkEnchantItem(104, 9, 1)
							|| pc.getInventory().checkEnchantItem(125, 9, 1)
							|| pc.getInventory().checkEnchantItem(129, 9, 1)
							|| pc.getInventory().checkEnchantItem(131, 9, 1)
							|| pc.getInventory().checkEnchantItem(145, 9, 1)
							|| pc.getInventory().checkEnchantItem(148, 9, 1)
							|| pc.getInventory().checkEnchantItem(180, 9, 1)
							|| pc.getInventory().checkEnchantItem(181, 9, 1))
							&& pc.getInventory().checkItem(40308, 10000000)) {
						if (pc.getInventory().consumeEnchantItem(5, 9, 1)
								|| pc.getInventory().consumeEnchantItem(6, 9, 1)
								|| pc.getInventory().consumeEnchantItem(32, 9, 1)
								|| pc.getInventory().consumeEnchantItem(37, 9, 1)
								|| pc.getInventory().consumeEnchantItem(41, 9, 1)
								|| pc.getInventory().consumeEnchantItem(42, 9, 1)
								|| pc.getInventory().consumeEnchantItem(52, 9, 1)
								|| pc.getInventory().consumeEnchantItem(64, 9, 1)
								|| pc.getInventory().consumeEnchantItem(99, 9, 1)
								|| pc.getInventory().consumeEnchantItem(104, 9, 1)
								|| pc.getInventory().consumeEnchantItem(125, 9, 1)
								|| pc.getInventory().consumeEnchantItem(129, 9, 1)
								|| pc.getInventory().consumeEnchantItem(131, 9, 1)
								|| pc.getInventory().consumeEnchantItem(145, 9, 1)
								|| pc.getInventory().consumeEnchantItem(148, 9, 1)
								|| pc.getInventory().consumeEnchantItem(180, 9, 1)
								|| pc.getInventory().consumeEnchantItem(181, 9, 1)) {
							;
						}
						pc.getInventory().consumeItem(40308, 10000000);
						인첸트지급(pc, 412002, 1, 8);
					}
				} else if (itemtype == 159) {
					if ((pc.getInventory().checkEnchantItem(410000, 8, 1) // [+7]환영의
																			// 체인소드
							|| pc.getInventory().checkEnchantItem(410001, 8, 1))
							&& pc.getInventory().checkItem(40308, 5000000)) {
						if (pc.getInventory().consumeEnchantItem(410000, 8, 1)
								|| pc.getInventory().consumeEnchantItem(410001, 8, 1)) {
							;
						}
						pc.getInventory().consumeItem(40308, 5000000);
						인첸트지급(pc, 275, 1, 7);
					}
				} else if (itemtype == 160) {
					if ((pc.getInventory().checkEnchantItem(410000, 9, 1) // [+8]환영의
																			// 체인소드
							|| pc.getInventory().checkEnchantItem(410001, 9, 1))
							&& pc.getInventory().checkItem(40308, 10000000)) {
						if (pc.getInventory().consumeEnchantItem(410000, 9, 1)
								|| pc.getInventory().consumeEnchantItem(410001, 9, 1)) {
							;
						}
						pc.getInventory().consumeItem(40308, 10000000);
						인첸트지급(pc, 275, 1, 8);
					}
				} else if (itemtype == 161) {
					if ((pc.getInventory().checkEnchantItem(410003, 8, 1) // [+7]공명의
																			// 키링크
							|| pc.getInventory().checkEnchantItem(410004, 8, 1))
							&& pc.getInventory().checkItem(40308, 5000000)) {
						if (pc.getInventory().consumeEnchantItem(410003, 8, 1)
								|| pc.getInventory().consumeEnchantItem(410004, 8, 1)) {
							;
						}
						pc.getInventory().consumeItem(40308, 5000000);
						인첸트지급(pc, 266, 1, 7);
					}
				} else if (itemtype == 162) {
					if ((pc.getInventory().checkEnchantItem(410003, 9, 1) // [+8]공명의
																			// 키링크
							|| pc.getInventory().checkEnchantItem(410004, 9, 1))
							&& pc.getInventory().checkItem(40308, 10000000)) {
						if (pc.getInventory().consumeEnchantItem(410003, 9, 1)
								|| pc.getInventory().consumeEnchantItem(410004, 9, 1)) {
							;
						}
						pc.getInventory().consumeItem(40308, 10000000);
						인첸트지급(pc, 266, 1, 8);
					}
				} else if (itemtype == 287) {
					if ((pc.getInventory().checkEnchantItem(81, 8, 1) // [+7]파괴의
																		// 크로우
							|| pc.getInventory().checkEnchantItem(162, 8, 1)
							|| pc.getInventory().checkEnchantItem(177, 8, 1)
							|| pc.getInventory().checkEnchantItem(194, 8, 1)
							|| pc.getInventory().checkEnchantItem(13, 8, 1))
							&& pc.getInventory().checkItem(40308, 5000000)) {
						if (pc.getInventory().consumeEnchantItem(81, 8, 1)
								|| pc.getInventory().consumeEnchantItem(162, 8, 1)
								|| pc.getInventory().consumeEnchantItem(177, 8, 1)
								|| pc.getInventory().consumeEnchantItem(194, 8, 1)
								|| pc.getInventory().consumeEnchantItem(13, 8, 1)) {
							;
						}
						pc.getInventory().consumeItem(40308, 5000000);
						인첸트지급(pc, 259, 1, 7);
					}
				} else if (itemtype == 288) {
					if ((pc.getInventory().checkEnchantItem(81, 9, 1) // [+8]파괴의
																		// 크로우
							|| pc.getInventory().consumeEnchantItem(162, 8, 1)
							|| pc.getInventory().checkEnchantItem(177, 9, 1)
							|| pc.getInventory().checkEnchantItem(194, 9, 1)
							|| pc.getInventory().checkEnchantItem(13, 9, 1))
							&& pc.getInventory().checkItem(40308, 10000000)) {
						if (pc.getInventory().consumeEnchantItem(81, 9, 1)
								|| pc.getInventory().consumeEnchantItem(162, 9, 1)
								|| pc.getInventory().consumeEnchantItem(177, 9, 1)
								|| pc.getInventory().consumeEnchantItem(194, 9, 1)
								|| pc.getInventory().consumeEnchantItem(13, 9, 1)) {
							;
						}
						pc.getInventory().consumeItem(40308, 10000000);
						인첸트지급(pc, 259, 1, 8);
					}
				} else if (itemtype == 289) {
					if ((pc.getInventory().checkEnchantItem(81, 8, 1) // [+7]파괴의
																		// 이도류
							|| pc.getInventory().checkEnchantItem(162, 8, 1)
							|| pc.getInventory().checkEnchantItem(177, 8, 1)
							|| pc.getInventory().checkEnchantItem(194, 8, 1)
							|| pc.getInventory().checkEnchantItem(13, 8, 1))
							&& pc.getInventory().checkItem(40308, 5000000)) {
						if (pc.getInventory().consumeEnchantItem(81, 8, 1)
								|| pc.getInventory().consumeEnchantItem(162, 8, 1)
								|| pc.getInventory().consumeEnchantItem(177, 8, 1)
								|| pc.getInventory().consumeEnchantItem(194, 8, 1)
								|| pc.getInventory().consumeEnchantItem(13, 8, 1)) {
							;
						}
						pc.getInventory().consumeItem(40308, 5000000);
						인첸트지급(pc, 260, 1, 7);
					}
				} else if (itemtype == 290) {
					if ((pc.getInventory().checkEnchantItem(81, 9, 1) // [+8]파괴의
																		// 이도류
							|| pc.getInventory().checkEnchantItem(162, 9, 1)
							|| pc.getInventory().checkEnchantItem(177, 9, 1)
							|| pc.getInventory().checkEnchantItem(194, 9, 1)
							|| pc.getInventory().checkEnchantItem(13, 9, 1))
							&& pc.getInventory().checkItem(40308, 10000000)) {
						if (pc.getInventory().consumeEnchantItem(81, 9, 1)
								|| pc.getInventory().consumeEnchantItem(162, 9, 1)
								|| pc.getInventory().consumeEnchantItem(177, 9, 1)
								|| pc.getInventory().consumeEnchantItem(194, 9, 1)
								|| pc.getInventory().consumeEnchantItem(13, 9, 1)) {
							;
						}
						pc.getInventory().consumeItem(40308, 10000000);
						인첸트지급(pc, 260, 1, 8);
					}
				} else if (itemtype == 2182) {
					if (pc.getInventory().checkEnchantItem(126, 9, 1)
							&& pc.getInventory().checkEnchantItem(131, 9, 1)) {
						if (pc.getInventory().consumeEnchantItem(126, 9, 1)
								&& pc.getInventory().consumeEnchantItem(131, 9, 1)) {
							;
						}
						인첸트지급(pc, 127, 1, 0);
					}
				} else if (itemtype == 2183) {
					if (pc.getInventory().checkEnchantItem(131, 10, 1)) {
						if (pc.getInventory().consumeEnchantItem(131, 10, 1)) {
							;
						}
						인첸트지급(pc, 127, 1, 0);
					}
				} else if (itemtype == 2227) {
					if (pc.getInventory().checkEnchantItem(126, 10, 1)) {
						if (pc.getInventory().consumeEnchantItem(126, 10, 1)) {
							;
						}
						인첸트지급(pc, 127, 1, 0);
						인첸트지급(pc, 127, 1, 0);
					}
				} else if (itemtype == 2184) {
					if (pc.getInventory().checkEnchantItem(131, 11, 1)) {
						if (pc.getInventory().consumeEnchantItem(131, 11, 1)) {
							;
						}
						인첸트지급(pc, 127, 1, 0);
						인첸트지급(pc, 127, 1, 0);
						인첸트지급(pc, 127, 1, 0);
					}
				} else if (itemtype == 2185) {
					if (pc.getInventory().checkEnchantItem(126, 11, 1)) {
						if (pc.getInventory().consumeEnchantItem(126, 11, 1)) {
							;
						}
						인첸트지급(pc, 127, 1, 0);
						인첸트지급(pc, 127, 1, 0);
						인첸트지급(pc, 127, 1, 0);
					}
				} else if (itemtype == 577) { // 힘의룬주머니
					if (pc.getInventory().checkItem(60384, 1) && pc.getInventory().checkItem(60385, 1)
							&& pc.getInventory().checkItem(149027, 50)) {
						pc.getInventory().consumeItem(60384, 1);
						pc.getInventory().consumeItem(60385, 1);
						pc.getInventory().consumeItem(149027, 50);
					}
					인첸트지급(pc, 7252, 1, 0);
				} else if (itemtype == 578) { // 민첩의룬주머니
					if (pc.getInventory().checkItem(60384, 1) && pc.getInventory().checkItem(60385, 1)
							&& pc.getInventory().checkItem(149027, 50)) {
						pc.getInventory().consumeItem(60384, 1);
						pc.getInventory().consumeItem(60385, 1);
						pc.getInventory().consumeItem(149027, 50);
					}
					인첸트지급(pc, 7253, 1, 0);
				} else if (itemtype == 579) { // 체력의룬주머니
					if (pc.getInventory().checkItem(60384, 1) && pc.getInventory().checkItem(60385, 1)
							&& pc.getInventory().checkItem(149027, 50)) {
						pc.getInventory().consumeItem(60384, 1);
						pc.getInventory().consumeItem(60385, 1);
						pc.getInventory().consumeItem(149027, 50);
					}
					인첸트지급(pc, 7254, 1, 0);
				} else if (itemtype == 580) { // 지식의의룬주머니
					if (pc.getInventory().checkItem(60384, 1) && pc.getInventory().checkItem(60385, 1)
							&& pc.getInventory().checkItem(149027, 50)) {
						pc.getInventory().consumeItem(60384, 1);
						pc.getInventory().consumeItem(60385, 1);
						pc.getInventory().consumeItem(149027, 50);
					}
					인첸트지급(pc, 7255, 1, 0);
				} else if (itemtype == 581) { // 지혜의의룬주머니
					if (pc.getInventory().checkItem(60384, 1) && pc.getInventory().checkItem(60385, 1)
							&& pc.getInventory().checkItem(149027, 50)) {
						pc.getInventory().consumeItem(60384, 1);
						pc.getInventory().consumeItem(60385, 1);
						pc.getInventory().consumeItem(149027, 50);
					}
					인첸트지급(pc, 7256, 1, 0);
				} else if (itemtype == 46) { // 집행
					if (pc.getInventory().checkItem(73005, 1) // 광분 양검
							&& pc.getInventory().checkItem(73011, 1) // 비법서
							&& pc.getInventory().checkItem(6670, 500) // 환보
							&& pc.getInventory().checkItem(73010, 20)) { // 사념조각
						pc.getInventory().consumeItem(73005, 1); // 광분양검
						pc.getInventory().consumeItem(73011, 1); // 비법서
						pc.getInventory().consumeItem(6670, 500); // 환보
						pc.getInventory().consumeItem(73010, 20); // 사념조각
						인첸트지급(pc, 61, 1, 0);
					}
					if (pc.getInventory().checkEnchantItem(59, 10, 1) && pc.getInventory().checkItem(73011, 1)
							&& pc.getInventory().checkItem(6670, 500) && pc.getInventory().checkItem(73010, 20)) { // 흑마법가루
						pc.getInventory().consumeEnchantItem(59, 10, 1);
						pc.getInventory().consumeItem(73011, 1);
						pc.getInventory().consumeItem(6670, 500);
						pc.getInventory().consumeItem(73010, 20);
						인첸트지급(pc, 61, 1, 0);
					}
				} else if (itemtype == 47) { // 바단, 바람칼날 단검
					if (pc.getInventory().checkItem(73006, 1) // 광분 양검
							&& pc.getInventory().checkItem(73011, 1) // 비법서
							&& pc.getInventory().checkItem(6670, 500) // 환보
							&& pc.getInventory().checkItem(73010, 20)) { // 사념조각
						pc.getInventory().consumeItem(73006, 1); // 광분양검
						pc.getInventory().consumeItem(73011, 1); // 비법서
						pc.getInventory().consumeItem(6670, 500); // 환보
						pc.getInventory().consumeItem(73010, 20); // 사념조각
						인첸트지급(pc, 12, 1, 0);
					}
				} else if (itemtype == 48) { // 붉은 그림자의 이도류,붉이
					if (pc.getInventory().checkItem(73008, 1) // 광분 양검
							&& pc.getInventory().checkItem(73011, 1) // 비법서
							&& pc.getInventory().checkItem(6670, 500) // 환보
							&& pc.getInventory().checkItem(73010, 20)) { // 사념조각
						pc.getInventory().consumeItem(73008, 1); // 광분양검
						pc.getInventory().consumeItem(73011, 1); // 비법서
						pc.getInventory().consumeItem(6670, 500); // 환보
						pc.getInventory().consumeItem(73010, 20); // 사념조각
						인첸트지급(pc, 86, 1, 0);
					}
				} else if (itemtype == 49) { // 수정 결정의 지팡이, 수결지
					if (pc.getInventory().checkItem(73007, 1) // 광분 양검
							&& pc.getInventory().checkItem(73011, 1) // 비법서
							&& pc.getInventory().checkItem(6670, 500) // 환보
							&& pc.getInventory().checkItem(73010, 20)) { // 사념조각
						pc.getInventory().consumeItem(73007, 1); // 광분양검
						pc.getInventory().consumeItem(73011, 1); // 비법서
						pc.getInventory().consumeItem(6670, 500); // 환보
						pc.getInventory().consumeItem(73010, 20); // 사념조각
						인첸트지급(pc, 134, 1, 0);
					}
				} else if (itemtype == 198) {
					if (pc.getInventory().checkItem(111, 1) && pc.getInventory().checkItem(40318, 100)
							&& pc.getInventory().checkItem(40090, 2) && pc.getInventory().checkItem(40091, 2)
							&& pc.getInventory().checkItem(40092, 2) && pc.getInventory().checkItem(40093, 2)
							&& pc.getInventory().checkItem(40094, 2)) {
						pc.getInventory().consumeItem(111, 1);
						pc.getInventory().consumeItem(40318, 100);
						pc.getInventory().consumeItem(40090, 2);
						pc.getInventory().consumeItem(40091, 2);
						pc.getInventory().consumeItem(40092, 2);
						pc.getInventory().consumeItem(40093, 2);
						pc.getInventory().consumeItem(40094, 2);
					}
					인첸트지급(pc, 121, 1, 0);
				} else if (itemtype == 49) {
					if (pc.getInventory().checkItem(111, 1) && pc.getInventory().checkItem(40318, 100)
							&& pc.getInventory().checkItem(40090, 2) && pc.getInventory().checkItem(40091, 2)
							&& pc.getInventory().checkItem(40092, 2) && pc.getInventory().checkItem(40093, 2)
							&& pc.getInventory().checkItem(40094, 2)) {
						pc.getInventory().consumeItem(111, 1);
						pc.getInventory().consumeItem(40318, 100);
						pc.getInventory().consumeItem(40090, 2);
						pc.getInventory().consumeItem(40091, 2);
						pc.getInventory().consumeItem(40092, 2);
						pc.getInventory().consumeItem(40093, 2);
						pc.getInventory().consumeItem(40094, 2);
					}
					인첸트지급(pc, 134, 1, 0);
				} else if (itemtype == 35) { // 악마의 크로스보우
					if (pc.getInventory().checkItem(40482, 1) && pc.getInventory().checkItem(40461, 35)) {
						pc.getInventory().consumeItem(40482, 1);
						pc.getInventory().consumeItem(40461, 35);
					}
					인첸트지급(pc, 185, 1, 0);
				} else if (itemtype == 36) { // 악마의 칼
					if (pc.getInventory().checkItem(40474, 1) && pc.getInventory().checkItem(40461, 35)) {
						pc.getInventory().consumeItem(40474, 1);
						pc.getInventory().consumeItem(40461, 35);
					}
					인첸트지급(pc, 63, 1, 0);
				} else if (itemtype == 37) { // 악마의 이도류
					if (pc.getInventory().checkItem(40481, 1) && pc.getInventory().checkItem(40461, 35)) {
						pc.getInventory().consumeItem(40481, 1);
						pc.getInventory().consumeItem(40461, 35);
					}
					인첸트지급(pc, 85, 1, 0);
				} else if (itemtype == 38) { // 악마의 크로우
					if (pc.getInventory().checkItem(40476, 1) && pc.getInventory().checkItem(40461, 35)) {
						pc.getInventory().consumeItem(40476, 1);
						pc.getInventory().consumeItem(40461, 35);
					}
					인첸트지급(pc, 165, 1, 0);
					// 95, 96, 97, 98, 99, 1960, 1961,
				} else if (itemtype >= 2571 && itemtype <= 2618) {
					int newEnchant = 0;
					int oldEnchant = 0;
					int itemnum = 0;
					int metNum = 0;
					int succChan = 0;
					if ((itemtype == 2583) || (itemtype == 2584) || (itemtype == 2585) || (itemtype == 2586)
							|| (itemtype == 2595) || (itemtype == 2596) || (itemtype == 2597) || (itemtype == 2598)
							|| (itemtype == 2571) || (itemtype == 2572) || (itemtype == 2573) || (itemtype == 2574)
							|| (itemtype == 2607) || (itemtype == 2608) || (itemtype == 2609)
							|| (itemtype == 2610)) {
						newEnchant = 7;
						oldEnchant = 6;
						succChan = 30;
					}
					if ((itemtype == 2587) || (itemtype == 2588) || (itemtype == 2589) || (itemtype == 2590)
							|| (itemtype == 2599) || (itemtype == 2600) || (itemtype == 2601) || (itemtype == 2602)
							|| (itemtype == 2575) || (itemtype == 2576) || (itemtype == 2577) || (itemtype == 2578)
							|| (itemtype == 2611) || (itemtype == 2612) || (itemtype == 2613)
							|| (itemtype == 2614)) {
						newEnchant = 8;
						oldEnchant = 7;
						succChan = 20;
					}
					if ((itemtype == 2591) || (itemtype == 2592) || (itemtype == 2593) || (itemtype == 2594)
							|| (itemtype == 2603) || (itemtype == 2604) || (itemtype == 2605) || (itemtype == 2606)
							|| (itemtype == 2579) || (itemtype == 2580) || (itemtype == 2581) || (itemtype == 2582)
							|| (itemtype == 2615) || (itemtype == 2616) || (itemtype == 2617)
							|| (itemtype == 2618)) {
						newEnchant = 9;
						oldEnchant = 8;
						succChan = 10;
					}
					if ((itemtype == 2583) || (itemtype == 2595) || (itemtype == 2571) || (itemtype == 2607)
							|| (itemtype == 2587) || (itemtype == 2599) || (itemtype == 2575) || (itemtype == 2611)
							|| (itemtype == 2591) || (itemtype == 2603) || (itemtype == 2579)
							|| (itemtype == 2615)) {
						metNum = 40346;
					}
					if ((itemtype == 2584) || (itemtype == 2596) || (itemtype == 2572) || (itemtype == 2608)
							|| (itemtype == 2588) || (itemtype == 2600) || (itemtype == 2576) || (itemtype == 2612)
							|| (itemtype == 2592) || (itemtype == 2604) || (itemtype == 2580)
							|| (itemtype == 2616)) {
						metNum = 40362;
					}
					if ((itemtype == 2585) || (itemtype == 2597) || (itemtype == 2573) || (itemtype == 2609)
							|| (itemtype == 2589) || (itemtype == 2601) || (itemtype == 2577) || (itemtype == 2613)
							|| (itemtype == 2593) || (itemtype == 2605) || (itemtype == 2581)
							|| (itemtype == 2617)) {
						metNum = 40370;
					}
					if ((itemtype == 2586) || (itemtype == 2598) || (itemtype == 2574) || (itemtype == 2610)
							|| (itemtype == 2590) || (itemtype == 2602) || (itemtype == 2578) || (itemtype == 2614)
							|| (itemtype == 2594) || (itemtype == 2606) || (itemtype == 2582)
							|| (itemtype == 2618)) {
						metNum = 40354;
					}
					if ((itemtype == 2583) || (itemtype == 2587) || (itemtype == 2591)) {
						itemnum = 420103;
					}
					if ((itemtype == 2595) || (itemtype == 2599) || (itemtype == 2603)) {
						itemnum = 420102;
					}
					if ((itemtype == 2571) || (itemtype == 2575) || (itemtype == 2579)) {
						itemnum = 420100;
					}
					if ((itemtype == 2607) || (itemtype == 2611) || (itemtype == 2615)) {
						itemnum = 420101;
					}
					if ((itemtype == 2584) || (itemtype == 2588) || (itemtype == 2596)) {
						itemnum = 420107;
					}
					if ((itemtype == 2596) || (itemtype == 2600) || (itemtype == 2604)) {
						itemnum = 420106;
					}
					if ((itemtype == 2572) || (itemtype == 2576) || (itemtype == 2580)) {
						itemnum = 420104;
					}
					if ((itemtype == 2608) || (itemtype == 2612) || (itemtype == 2616)) {
						itemnum = 420105;
					}
					if ((itemtype == 2585) || (itemtype == 2589) || (itemtype == 2593)) {
						itemnum = 420111;
					}
					if ((itemtype == 2597) || (itemtype == 2601) || (itemtype == 2605)) {
						itemnum = 420110;
					}
					if ((itemtype == 2573) || (itemtype == 2577) || (itemtype == 2581)) {
						itemnum = 420108;
					}
					if ((itemtype == 2609) || (itemtype == 2613) || (itemtype == 2617)) {
						itemnum = 420109;
					}
					if ((itemtype == 2574) || (itemtype == 2578) || (itemtype == 2582)) {
						itemnum = 420112; // 발라완력
					}
					if ((itemtype == 2586) || (itemtype == 2590) || (itemtype == 2594)) {
						itemnum = 420115; // 발라마력
					}
					if ((itemtype == 2598) || (itemtype == 2602) || (itemtype == 2606)) {
						itemnum = 420114; // 발라인내
					}
					if ((itemtype == 2610) || (itemtype == 2614) || (itemtype == 2618)) {
						itemnum = 420113; // 발라예지
					}
					if ((pc.getInventory().checkEnchantItem(itemnum, oldEnchant, 1))
							&& (pc.getInventory().checkItem(metNum, 1))) {
						if (Chance < succChan) {
							pc.getInventory().consumeEnchantItem(itemnum, oldEnchant, 1);
							pc.getInventory().consumeItem(metNum, 1);
							인첸트지급(pc, itemnum, 1, newEnchant);
						} else {
							pc.getInventory().consumeItem(metNum, 1);
							pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패하였습니다."), true);
						}
					}
					// 4564,4565,4566,4567,4568, 신완벨 5,6,7,8,9
					// 4569,4570,4571,4572,4573, 신민벨 5,6,7,8,9
					// 4574,4575,4576,4577,4578, 신지벨 5,6,7,8,9
					// 4579,4580,4581,4582,4583, 신혜벨 5,6,7,8,9
				} else if (itemtype == 4564 || itemtype == 4565 || itemtype == 4566 || itemtype == 4567
						|| itemtype == 4568) {
					if (itemtype == 4564) {
						if (pc.getInventory().checkEnchantItem(21261, 5, 1)
								&& pc.getInventory().checkItem(6670, 180)
								&& pc.getInventory().checkItem(60492, 17000000)) {
							pc.getInventory().consumeEnchantItem(21261, 5, 1);
							pc.getInventory().consumeItem(6670, 180);
							pc.getInventory().consumeItem(60492, 17000000);
							인첸트지급(pc, 33910, 1, 5);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21261, 5, 1)
								&& pc.getInventory().checkItem(6670, 180)
								&& pc.getInventory().checkItem(40308, 21000000)) {
							pc.getInventory().consumeEnchantItem(21261, 5, 1);
							pc.getInventory().consumeItem(6670, 180);
							pc.getInventory().consumeItem(40308, 21000000);
							인첸트지급(pc, 33910, 1, 5);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21261, 5, 1)
								&& pc.getInventory().checkItem(66710, 140)
								&& pc.getInventory().checkItem(40308, 21000000)) {
							pc.getInventory().consumeEnchantItem(21261, 5, 1);
							pc.getInventory().consumeItem(66710, 140);
							pc.getInventory().consumeItem(40308, 21000000);
							인첸트지급(pc, 33910, 1, 5);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21261, 5, 1)
								&& pc.getInventory().checkItem(66710, 180)
								&& pc.getInventory().checkItem(60492, 17000000)) {
							pc.getInventory().consumeEnchantItem(21261, 5, 1);
							pc.getInventory().consumeItem(66710, 180);
							pc.getInventory().consumeItem(60492, 17000000);
							인첸트지급(pc, 33910, 1, 5);
							return;
						}
					}
					if (itemtype == 4565) {
						if (pc.getInventory().checkEnchantItem(21261, 6, 1)
								&& pc.getInventory().checkItem(6670, 310)
								&& pc.getInventory().checkItem(60492, 19000000)) {
							pc.getInventory().consumeEnchantItem(21261, 6, 1);
							pc.getInventory().consumeItem(6670, 310);
							pc.getInventory().consumeItem(60492, 19000000);
							인첸트지급(pc, 33910, 1, 6);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21261, 6, 1)
								&& pc.getInventory().checkItem(6670, 310)
								&& pc.getInventory().checkItem(40308, 26000000)) {
							pc.getInventory().consumeEnchantItem(21261, 6, 1);
							pc.getInventory().consumeItem(6670, 310);
							pc.getInventory().consumeItem(40308, 26000000);
							인첸트지급(pc, 33910, 1, 6);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21261, 6, 1)
								&& pc.getInventory().checkItem(66710, 240)
								&& pc.getInventory().checkItem(40308, 26000000)) {
							pc.getInventory().consumeEnchantItem(21261, 6, 1);
							pc.getInventory().consumeItem(66710, 240);
							pc.getInventory().consumeItem(40308, 26000000);
							인첸트지급(pc, 33910, 1, 6);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21261, 6, 1)
								&& pc.getInventory().checkItem(66710, 240)
								&& pc.getInventory().checkItem(60492, 19000000)) {
							pc.getInventory().consumeEnchantItem(21261, 6, 1);
							pc.getInventory().consumeItem(66710, 240);
							pc.getInventory().consumeItem(60492, 19000000);
							인첸트지급(pc, 33910, 1, 6);
							return;
						}
					}
					if (itemtype == 4566) {
						if (pc.getInventory().checkEnchantItem(21261, 7, 1)
								&& pc.getInventory().checkItem(6670, 440)
								&& pc.getInventory().checkItem(60492, 21000000)) {
							pc.getInventory().consumeEnchantItem(21261, 7, 1);
							pc.getInventory().consumeItem(6670, 440);
							pc.getInventory().consumeItem(60492, 21000000);
							인첸트지급(pc, 33910, 1, 7);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21261, 7, 1)
								&& pc.getInventory().checkItem(6670, 440)
								&& pc.getInventory().checkItem(40308, 29000000)) {
							pc.getInventory().consumeEnchantItem(21261, 7, 1);
							pc.getInventory().consumeItem(6670, 440);
							pc.getInventory().consumeItem(40308, 29000000);
							인첸트지급(pc, 33910, 1, 7);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21261, 7, 1)
								&& pc.getInventory().checkItem(66710, 340)
								&& pc.getInventory().checkItem(40308, 29000000)) {
							pc.getInventory().consumeEnchantItem(21261, 7, 1);
							pc.getInventory().consumeItem(66710, 340);
							pc.getInventory().consumeItem(40308, 29000000);
							인첸트지급(pc, 33910, 1, 7);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21261, 7, 1)
								&& pc.getInventory().checkItem(66710, 340)
								&& pc.getInventory().checkItem(60492, 21000000)) {
							pc.getInventory().consumeEnchantItem(21261, 7, 1);
							pc.getInventory().consumeItem(66710, 340);
							pc.getInventory().consumeItem(60492, 21000000);
							인첸트지급(pc, 33910, 1, 7);
							return;
						}
					}
					if (itemtype == 4567) {
						if (pc.getInventory().checkEnchantItem(21261, 8, 1)
								&& pc.getInventory().checkItem(6670, 570)
								&& pc.getInventory().checkItem(60492, 23000000)) {
							pc.getInventory().consumeEnchantItem(21261, 8, 1);
							pc.getInventory().consumeItem(6670, 570);
							pc.getInventory().consumeItem(60492, 23000000);
							인첸트지급(pc, 33910, 1, 8);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21261, 8, 1)
								&& pc.getInventory().checkItem(6670, 570)
								&& pc.getInventory().checkItem(40308, 32000000)) {
							pc.getInventory().consumeEnchantItem(21261, 8, 1);
							pc.getInventory().consumeItem(6670, 570);
							pc.getInventory().consumeItem(40308, 32000000);
							인첸트지급(pc, 33910, 1, 8);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21261, 8, 1)
								&& pc.getInventory().checkItem(66710, 440)
								&& pc.getInventory().checkItem(40308, 32000000)) {
							pc.getInventory().consumeEnchantItem(21261, 8, 1);
							pc.getInventory().consumeItem(66710, 440);
							pc.getInventory().consumeItem(40308, 32000000);
							인첸트지급(pc, 33910, 1, 8);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21261, 8, 1)
								&& pc.getInventory().checkItem(66710, 440)
								&& pc.getInventory().checkItem(60492, 23000000)) {
							pc.getInventory().consumeEnchantItem(21261, 8, 1);
							pc.getInventory().consumeItem(66710, 440);
							pc.getInventory().consumeItem(60492, 23000000);
							인첸트지급(pc, 33910, 1, 8);
							return;
						}
					}
					if (itemtype == 4568) {
						if (pc.getInventory().checkEnchantItem(21261, 9, 1)
								&& pc.getInventory().checkItem(6670, 700)
								&& pc.getInventory().checkItem(60492, 25000000)) {
							pc.getInventory().consumeEnchantItem(21261, 9, 1);
							pc.getInventory().consumeItem(6670, 700);
							pc.getInventory().consumeItem(60492, 25000000);
							인첸트지급(pc, 33910, 1, 9);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21261, 9, 1)
								&& pc.getInventory().checkItem(6670, 700)
								&& pc.getInventory().checkItem(40308, 35000000)) {
							pc.getInventory().consumeEnchantItem(21261, 9, 1);
							pc.getInventory().consumeItem(6670, 700);
							pc.getInventory().consumeItem(40308, 35000000);
							인첸트지급(pc, 33910, 1, 9);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21261, 9, 1)
								&& pc.getInventory().checkItem(66710, 540)
								&& pc.getInventory().checkItem(40308, 35000000)) {
							pc.getInventory().consumeEnchantItem(21261, 9, 1);
							pc.getInventory().consumeItem(66710, 540);
							pc.getInventory().consumeItem(40308, 35000000);
							인첸트지급(pc, 33910, 1, 9);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21261, 9, 1)
								&& pc.getInventory().checkItem(66710, 540)
								&& pc.getInventory().checkItem(60492, 25000000)) {
							pc.getInventory().consumeEnchantItem(21261, 9, 1);
							pc.getInventory().consumeItem(66710, 540);
							pc.getInventory().consumeItem(60492, 25000000);
							인첸트지급(pc, 33910, 1, 9);
							return;
						}
					}
				} else if (itemtype == 4569 || itemtype == 4570 || itemtype == 4571 || itemtype == 4572
						|| itemtype == 4573) {
					if (itemtype == 4569) {
						if (pc.getInventory().checkEnchantItem(21263, 5, 1)
								&& pc.getInventory().checkItem(6670, 180)
								&& pc.getInventory().checkItem(60492, 17000000)) {
							pc.getInventory().consumeEnchantItem(21263, 5, 1);
							pc.getInventory().consumeItem(6670, 180);
							pc.getInventory().consumeItem(60492, 17000000);
							인첸트지급(pc, 33911, 1, 5);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21263, 5, 1)
								&& pc.getInventory().checkItem(6670, 180)
								&& pc.getInventory().checkItem(40308, 21000000)) {
							pc.getInventory().consumeEnchantItem(21263, 5, 1);
							pc.getInventory().consumeItem(6670, 180);
							pc.getInventory().consumeItem(40308, 21000000);
							인첸트지급(pc, 33911, 1, 5);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21263, 5, 1)
								&& pc.getInventory().checkItem(66710, 140)
								&& pc.getInventory().checkItem(40308, 21000000)) {
							pc.getInventory().consumeEnchantItem(21263, 5, 1);
							pc.getInventory().consumeItem(66710, 140);
							pc.getInventory().consumeItem(40308, 21000000);
							인첸트지급(pc, 33911, 1, 5);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21263, 5, 1)
								&& pc.getInventory().checkItem(66710, 180)
								&& pc.getInventory().checkItem(60492, 17000000)) {
							pc.getInventory().consumeEnchantItem(21263, 5, 1);
							pc.getInventory().consumeItem(66710, 180);
							pc.getInventory().consumeItem(60492, 17000000);
							인첸트지급(pc, 33911, 1, 5);
							return;
						}
					}
					if (itemtype == 4570) {
						if (pc.getInventory().checkEnchantItem(21263, 6, 1)
								&& pc.getInventory().checkItem(6670, 310)
								&& pc.getInventory().checkItem(60492, 19000000)) {
							pc.getInventory().consumeEnchantItem(21263, 6, 1);
							pc.getInventory().consumeItem(6670, 310);
							pc.getInventory().consumeItem(60492, 19000000);
							인첸트지급(pc, 33911, 1, 6);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21263, 6, 1)
								&& pc.getInventory().checkItem(6670, 310)
								&& pc.getInventory().checkItem(40308, 26000000)) {
							pc.getInventory().consumeEnchantItem(21263, 6, 1);
							pc.getInventory().consumeItem(6670, 310);
							pc.getInventory().consumeItem(40308, 26000000);
							인첸트지급(pc, 33911, 1, 6);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21263, 6, 1)
								&& pc.getInventory().checkItem(66710, 240)
								&& pc.getInventory().checkItem(40308, 26000000)) {
							pc.getInventory().consumeEnchantItem(21263, 6, 1);
							pc.getInventory().consumeItem(66710, 240);
							pc.getInventory().consumeItem(40308, 26000000);
							인첸트지급(pc, 33911, 1, 6);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21263, 6, 1)
								&& pc.getInventory().checkItem(66710, 240)
								&& pc.getInventory().checkItem(60492, 19000000)) {
							pc.getInventory().consumeEnchantItem(21263, 6, 1);
							pc.getInventory().consumeItem(66710, 240);
							pc.getInventory().consumeItem(60492, 19000000);
							인첸트지급(pc, 33911, 1, 6);
							return;
						}
					}
					if (itemtype == 4571) {
						if (pc.getInventory().checkEnchantItem(21263, 7, 1)
								&& pc.getInventory().checkItem(6670, 440)
								&& pc.getInventory().checkItem(60492, 21000000)) {
							pc.getInventory().consumeEnchantItem(21263, 7, 1);
							pc.getInventory().consumeItem(6670, 440);
							pc.getInventory().consumeItem(60492, 21000000);
							인첸트지급(pc, 33911, 1, 7);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21263, 7, 1)
								&& pc.getInventory().checkItem(6670, 440)
								&& pc.getInventory().checkItem(40308, 29000000)) {
							pc.getInventory().consumeEnchantItem(21263, 7, 1);
							pc.getInventory().consumeItem(6670, 440);
							pc.getInventory().consumeItem(40308, 29000000);
							인첸트지급(pc, 33911, 1, 7);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21263, 7, 1)
								&& pc.getInventory().checkItem(66710, 340)
								&& pc.getInventory().checkItem(40308, 29000000)) {
							pc.getInventory().consumeEnchantItem(21263, 7, 1);
							pc.getInventory().consumeItem(66710, 340);
							pc.getInventory().consumeItem(40308, 29000000);
							인첸트지급(pc, 33911, 1, 7);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21263, 7, 1)
								&& pc.getInventory().checkItem(66710, 340)
								&& pc.getInventory().checkItem(60492, 21000000)) {
							pc.getInventory().consumeEnchantItem(21263, 7, 1);
							pc.getInventory().consumeItem(66710, 340);
							pc.getInventory().consumeItem(60492, 21000000);
							인첸트지급(pc, 33911, 1, 7);
							return;
						}
					}
					if (itemtype == 4572) {
						if (pc.getInventory().checkEnchantItem(21263, 8, 1)
								&& pc.getInventory().checkItem(6670, 570)
								&& pc.getInventory().checkItem(60492, 23000000)) {
							pc.getInventory().consumeEnchantItem(21263, 8, 1);
							pc.getInventory().consumeItem(6670, 570);
							pc.getInventory().consumeItem(60492, 23000000);
							인첸트지급(pc, 33911, 1, 8);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21263, 8, 1)
								&& pc.getInventory().checkItem(6670, 570)
								&& pc.getInventory().checkItem(40308, 32000000)) {
							pc.getInventory().consumeEnchantItem(21263, 8, 1);
							pc.getInventory().consumeItem(6670, 570);
							pc.getInventory().consumeItem(40308, 32000000);
							인첸트지급(pc, 33911, 1, 8);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21263, 8, 1)
								&& pc.getInventory().checkItem(66710, 440)
								&& pc.getInventory().checkItem(40308, 32000000)) {
							pc.getInventory().consumeEnchantItem(21263, 8, 1);
							pc.getInventory().consumeItem(66710, 440);
							pc.getInventory().consumeItem(40308, 32000000);
							인첸트지급(pc, 33911, 1, 8);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21263, 8, 1)
								&& pc.getInventory().checkItem(66710, 440)
								&& pc.getInventory().checkItem(60492, 23000000)) {
							pc.getInventory().consumeEnchantItem(21263, 8, 1);
							pc.getInventory().consumeItem(66710, 440);
							pc.getInventory().consumeItem(60492, 23000000);
							인첸트지급(pc, 33911, 1, 8);
							return;
						}
					}
					if (itemtype == 4573) {
						if (pc.getInventory().checkEnchantItem(21263, 9, 1)
								&& pc.getInventory().checkItem(6670, 700)
								&& pc.getInventory().checkItem(60492, 25000000)) {
							pc.getInventory().consumeEnchantItem(21263, 9, 1);
							pc.getInventory().consumeItem(6670, 700);
							pc.getInventory().consumeItem(60492, 25000000);
							인첸트지급(pc, 33911, 1, 9);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21263, 9, 1)
								&& pc.getInventory().checkItem(6670, 700)
								&& pc.getInventory().checkItem(40308, 35000000)) {
							pc.getInventory().consumeEnchantItem(21263, 9, 1);
							pc.getInventory().consumeItem(6670, 700);
							pc.getInventory().consumeItem(40308, 35000000);
							인첸트지급(pc, 33911, 1, 9);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21263, 9, 1)
								&& pc.getInventory().checkItem(66710, 540)
								&& pc.getInventory().checkItem(40308, 35000000)) {
							pc.getInventory().consumeEnchantItem(21263, 9, 1);
							pc.getInventory().consumeItem(66710, 540);
							pc.getInventory().consumeItem(40308, 35000000);
							인첸트지급(pc, 33911, 1, 9);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21263, 9, 1)
								&& pc.getInventory().checkItem(66710, 540)
								&& pc.getInventory().checkItem(60492, 25000000)) {
							pc.getInventory().consumeEnchantItem(21263, 9, 1);
							pc.getInventory().consumeItem(66710, 540);
							pc.getInventory().consumeItem(60492, 25000000);
							인첸트지급(pc, 33911, 1, 9);
							return;
						}
					}
				} else if (itemtype == 4574 || itemtype == 4575 || itemtype == 4576 || itemtype == 4577
						|| itemtype == 4578) {
					if (itemtype == 4574) {
						if (pc.getInventory().checkEnchantItem(21262, 5, 1)
								&& pc.getInventory().checkItem(6670, 180)
								&& pc.getInventory().checkItem(60492, 17000000)) {
							pc.getInventory().consumeEnchantItem(21262, 5, 1);
							pc.getInventory().consumeItem(6670, 180);
							pc.getInventory().consumeItem(60492, 17000000);
							인첸트지급(pc, 33912, 1, 5);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21262, 5, 1)
								&& pc.getInventory().checkItem(6670, 180)
								&& pc.getInventory().checkItem(40308, 21000000)) {
							pc.getInventory().consumeEnchantItem(21262, 5, 1);
							pc.getInventory().consumeItem(6670, 180);
							pc.getInventory().consumeItem(40308, 21000000);
							인첸트지급(pc, 33912, 1, 5);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21262, 5, 1)
								&& pc.getInventory().checkItem(66710, 140)
								&& pc.getInventory().checkItem(40308, 21000000)) {
							pc.getInventory().consumeEnchantItem(21262, 5, 1);
							pc.getInventory().consumeItem(66710, 140);
							pc.getInventory().consumeItem(40308, 21000000);
							인첸트지급(pc, 33912, 1, 5);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21262, 5, 1)
								&& pc.getInventory().checkItem(66710, 180)
								&& pc.getInventory().checkItem(60492, 17000000)) {
							pc.getInventory().consumeEnchantItem(21262, 5, 1);
							pc.getInventory().consumeItem(66710, 180);
							pc.getInventory().consumeItem(60492, 17000000);
							인첸트지급(pc, 33912, 1, 5);
							return;
						}
					}
					if (itemtype == 4575) {
						if (pc.getInventory().checkEnchantItem(21262, 6, 1)
								&& pc.getInventory().checkItem(6670, 310)
								&& pc.getInventory().checkItem(60492, 19000000)) {
							pc.getInventory().consumeEnchantItem(21262, 6, 1);
							pc.getInventory().consumeItem(6670, 310);
							pc.getInventory().consumeItem(60492, 19000000);
							인첸트지급(pc, 33912, 1, 6);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21262, 6, 1)
								&& pc.getInventory().checkItem(6670, 310)
								&& pc.getInventory().checkItem(40308, 26000000)) {
							pc.getInventory().consumeEnchantItem(21262, 6, 1);
							pc.getInventory().consumeItem(6670, 310);
							pc.getInventory().consumeItem(40308, 26000000);
							인첸트지급(pc, 33912, 1, 6);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21262, 6, 1)
								&& pc.getInventory().checkItem(66710, 240)
								&& pc.getInventory().checkItem(40308, 26000000)) {
							pc.getInventory().consumeEnchantItem(21262, 6, 1);
							pc.getInventory().consumeItem(66710, 240);
							pc.getInventory().consumeItem(40308, 26000000);
							인첸트지급(pc, 33912, 1, 6);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21262, 6, 1)
								&& pc.getInventory().checkItem(66710, 240)
								&& pc.getInventory().checkItem(60492, 19000000)) {
							pc.getInventory().consumeEnchantItem(21262, 6, 1);
							pc.getInventory().consumeItem(66710, 240);
							pc.getInventory().consumeItem(60492, 19000000);
							인첸트지급(pc, 33912, 1, 6);
							return;
						}
					}
					if (itemtype == 4576) {
						if (pc.getInventory().checkEnchantItem(21262, 7, 1)
								&& pc.getInventory().checkItem(6670, 440)
								&& pc.getInventory().checkItem(60492, 21000000)) {
							pc.getInventory().consumeEnchantItem(21262, 7, 1);
							pc.getInventory().consumeItem(6670, 440);
							pc.getInventory().consumeItem(60492, 21000000);
							인첸트지급(pc, 33912, 1, 7);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21262, 7, 1)
								&& pc.getInventory().checkItem(6670, 440)
								&& pc.getInventory().checkItem(40308, 29000000)) {
							pc.getInventory().consumeEnchantItem(21262, 7, 1);
							pc.getInventory().consumeItem(6670, 440);
							pc.getInventory().consumeItem(40308, 29000000);
							인첸트지급(pc, 33912, 1, 7);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21262, 7, 1)
								&& pc.getInventory().checkItem(66710, 340)
								&& pc.getInventory().checkItem(40308, 29000000)) {
							pc.getInventory().consumeEnchantItem(21262, 7, 1);
							pc.getInventory().consumeItem(66710, 340);
							pc.getInventory().consumeItem(40308, 29000000);
							인첸트지급(pc, 33912, 1, 7);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21262, 7, 1)
								&& pc.getInventory().checkItem(66710, 340)
								&& pc.getInventory().checkItem(60492, 21000000)) {
							pc.getInventory().consumeEnchantItem(21262, 7, 1);
							pc.getInventory().consumeItem(66710, 340);
							pc.getInventory().consumeItem(60492, 21000000);
							인첸트지급(pc, 33912, 1, 7);
							return;
						}
					}
					if (itemtype == 4577) {
						if (pc.getInventory().checkEnchantItem(21262, 8, 1)
								&& pc.getInventory().checkItem(6670, 570)
								&& pc.getInventory().checkItem(60492, 23000000)) {
							pc.getInventory().consumeEnchantItem(21262, 8, 1);
							pc.getInventory().consumeItem(6670, 570);
							pc.getInventory().consumeItem(60492, 23000000);
							인첸트지급(pc, 33912, 1, 8);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21262, 8, 1)
								&& pc.getInventory().checkItem(6670, 570)
								&& pc.getInventory().checkItem(40308, 32000000)) {
							pc.getInventory().consumeEnchantItem(21262, 8, 1);
							pc.getInventory().consumeItem(6670, 570);
							pc.getInventory().consumeItem(40308, 32000000);
							인첸트지급(pc, 33912, 1, 8);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21262, 8, 1)
								&& pc.getInventory().checkItem(66710, 440)
								&& pc.getInventory().checkItem(40308, 32000000)) {
							pc.getInventory().consumeEnchantItem(21262, 8, 1);
							pc.getInventory().consumeItem(66710, 440);
							pc.getInventory().consumeItem(40308, 32000000);
							인첸트지급(pc, 33912, 1, 8);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21262, 8, 1)
								&& pc.getInventory().checkItem(66710, 440)
								&& pc.getInventory().checkItem(60492, 23000000)) {
							pc.getInventory().consumeEnchantItem(21262, 8, 1);
							pc.getInventory().consumeItem(66710, 440);
							pc.getInventory().consumeItem(60492, 23000000);
							인첸트지급(pc, 33912, 1, 8);
							return;
						}
					}
					if (itemtype == 4578) {
						if (pc.getInventory().checkEnchantItem(21262, 9, 1)
								&& pc.getInventory().checkItem(6670, 700)
								&& pc.getInventory().checkItem(60492, 25000000)) {
							pc.getInventory().consumeEnchantItem(21262, 9, 1);
							pc.getInventory().consumeItem(6670, 700);
							pc.getInventory().consumeItem(60492, 25000000);
							인첸트지급(pc, 33912, 1, 9);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21262, 9, 1)
								&& pc.getInventory().checkItem(6670, 700)
								&& pc.getInventory().checkItem(40308, 35000000)) {
							pc.getInventory().consumeEnchantItem(21262, 9, 1);
							pc.getInventory().consumeItem(6670, 700);
							pc.getInventory().consumeItem(40308, 35000000);
							인첸트지급(pc, 33912, 1, 9);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21262, 9, 1)
								&& pc.getInventory().checkItem(66710, 540)
								&& pc.getInventory().checkItem(40308, 35000000)) {
							pc.getInventory().consumeEnchantItem(21262, 9, 1);
							pc.getInventory().consumeItem(66710, 540);
							pc.getInventory().consumeItem(40308, 35000000);
							인첸트지급(pc, 33912, 1, 9);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21262, 9, 1)
								&& pc.getInventory().checkItem(66710, 540)
								&& pc.getInventory().checkItem(60492, 25000000)) {
							pc.getInventory().consumeEnchantItem(21262, 9, 1);
							pc.getInventory().consumeItem(66710, 540);
							pc.getInventory().consumeItem(60492, 25000000);
							인첸트지급(pc, 33912, 1, 9);
							return;
						}
					}
				} else if (itemtype == 4579 || itemtype == 4580 || itemtype == 4581 || itemtype == 4582
						|| itemtype == 4583) {
					if (itemtype == 4579) {
						if (pc.getInventory().checkEnchantItem(21264, 5, 1)
								&& pc.getInventory().checkItem(6670, 180)
								&& pc.getInventory().checkItem(60492, 17000000)) {
							pc.getInventory().consumeEnchantItem(21264, 5, 1);
							pc.getInventory().consumeItem(6670, 180);
							pc.getInventory().consumeItem(60492, 17000000);
							인첸트지급(pc, 33913, 1, 5);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21264, 5, 1)
								&& pc.getInventory().checkItem(6670, 180)
								&& pc.getInventory().checkItem(40308, 21000000)) {
							pc.getInventory().consumeEnchantItem(21264, 5, 1);
							pc.getInventory().consumeItem(6670, 180);
							pc.getInventory().consumeItem(40308, 21000000);
							인첸트지급(pc, 33913, 1, 5);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21264, 5, 1)
								&& pc.getInventory().checkItem(66710, 140)
								&& pc.getInventory().checkItem(40308, 21000000)) {
							pc.getInventory().consumeEnchantItem(21264, 5, 1);
							pc.getInventory().consumeItem(66710, 140);
							pc.getInventory().consumeItem(40308, 21000000);
							인첸트지급(pc, 33913, 1, 5);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21264, 5, 1)
								&& pc.getInventory().checkItem(66710, 180)
								&& pc.getInventory().checkItem(60492, 17000000)) {
							pc.getInventory().consumeEnchantItem(21264, 5, 1);
							pc.getInventory().consumeItem(66710, 180);
							pc.getInventory().consumeItem(60492, 17000000);
							인첸트지급(pc, 33913, 1, 5);
							return;
						}
					}
					if (itemtype == 4580) {
						if (pc.getInventory().checkEnchantItem(21264, 6, 1)
								&& pc.getInventory().checkItem(6670, 310)
								&& pc.getInventory().checkItem(60492, 19000000)) {
							pc.getInventory().consumeEnchantItem(21264, 6, 1);
							pc.getInventory().consumeItem(6670, 310);
							pc.getInventory().consumeItem(60492, 19000000);
							인첸트지급(pc, 33913, 1, 6);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21264, 6, 1)
								&& pc.getInventory().checkItem(6670, 310)
								&& pc.getInventory().checkItem(40308, 26000000)) {
							pc.getInventory().consumeEnchantItem(21264, 6, 1);
							pc.getInventory().consumeItem(6670, 310);
							pc.getInventory().consumeItem(40308, 26000000);
							인첸트지급(pc, 33913, 1, 6);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21264, 6, 1)
								&& pc.getInventory().checkItem(66710, 240)
								&& pc.getInventory().checkItem(40308, 26000000)) {
							pc.getInventory().consumeEnchantItem(21264, 6, 1);
							pc.getInventory().consumeItem(66710, 240);
							pc.getInventory().consumeItem(40308, 26000000);
							인첸트지급(pc, 33913, 1, 6);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21264, 6, 1)
								&& pc.getInventory().checkItem(66710, 240)
								&& pc.getInventory().checkItem(60492, 19000000)) {
							pc.getInventory().consumeEnchantItem(21264, 6, 1);
							pc.getInventory().consumeItem(66710, 240);
							pc.getInventory().consumeItem(60492, 19000000);
							인첸트지급(pc, 33913, 1, 6);
							return;
						}
					}
					if (itemtype == 4581) {
						if (pc.getInventory().checkEnchantItem(21264, 7, 1)
								&& pc.getInventory().checkItem(6670, 440)
								&& pc.getInventory().checkItem(60492, 21000000)) {
							pc.getInventory().consumeEnchantItem(21264, 7, 1);
							pc.getInventory().consumeItem(6670, 440);
							pc.getInventory().consumeItem(60492, 21000000);
							인첸트지급(pc, 33913, 1, 7);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21264, 7, 1)
								&& pc.getInventory().checkItem(6670, 440)
								&& pc.getInventory().checkItem(40308, 29000000)) {
							pc.getInventory().consumeEnchantItem(21264, 7, 1);
							pc.getInventory().consumeItem(6670, 440);
							pc.getInventory().consumeItem(40308, 29000000);
							인첸트지급(pc, 33913, 1, 7);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21264, 7, 1)
								&& pc.getInventory().checkItem(66710, 340)
								&& pc.getInventory().checkItem(40308, 29000000)) {
							pc.getInventory().consumeEnchantItem(21264, 7, 1);
							pc.getInventory().consumeItem(66710, 340);
							pc.getInventory().consumeItem(40308, 29000000);
							인첸트지급(pc, 33913, 1, 7);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21264, 7, 1)
								&& pc.getInventory().checkItem(66710, 340)
								&& pc.getInventory().checkItem(60492, 21000000)) {
							pc.getInventory().consumeEnchantItem(21264, 7, 1);
							pc.getInventory().consumeItem(66710, 340);
							pc.getInventory().consumeItem(60492, 21000000);
							인첸트지급(pc, 33913, 1, 7);
							return;
						}
					}
					if (itemtype == 4582) {
						if (pc.getInventory().checkEnchantItem(21264, 8, 1)
								&& pc.getInventory().checkItem(6670, 570)
								&& pc.getInventory().checkItem(60492, 23000000)) {
							pc.getInventory().consumeEnchantItem(21264, 8, 1);
							pc.getInventory().consumeItem(6670, 570);
							pc.getInventory().consumeItem(60492, 23000000);
							인첸트지급(pc, 33913, 1, 8);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21264, 8, 1)
								&& pc.getInventory().checkItem(6670, 570)
								&& pc.getInventory().checkItem(40308, 32000000)) {
							pc.getInventory().consumeEnchantItem(21264, 8, 1);
							pc.getInventory().consumeItem(6670, 570);
							pc.getInventory().consumeItem(40308, 32000000);
							인첸트지급(pc, 33913, 1, 8);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21264, 8, 1)
								&& pc.getInventory().checkItem(66710, 440)
								&& pc.getInventory().checkItem(40308, 32000000)) {
							pc.getInventory().consumeEnchantItem(21264, 8, 1);
							pc.getInventory().consumeItem(66710, 440);
							pc.getInventory().consumeItem(40308, 32000000);
							인첸트지급(pc, 33913, 1, 8);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21264, 8, 1)
								&& pc.getInventory().checkItem(66710, 440)
								&& pc.getInventory().checkItem(60492, 23000000)) {
							pc.getInventory().consumeEnchantItem(21264, 8, 1);
							pc.getInventory().consumeItem(66710, 440);
							pc.getInventory().consumeItem(60492, 23000000);
							인첸트지급(pc, 33913, 1, 8);
							return;
						}
					}
					if (itemtype == 4583) {
						if (pc.getInventory().checkEnchantItem(21264, 9, 1)
								&& pc.getInventory().checkItem(6670, 700)
								&& pc.getInventory().checkItem(60492, 25000000)) {
							pc.getInventory().consumeEnchantItem(21264, 9, 1);
							pc.getInventory().consumeItem(6670, 700);
							pc.getInventory().consumeItem(60492, 25000000);
							인첸트지급(pc, 33913, 1, 9);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21264, 9, 1)
								&& pc.getInventory().checkItem(6670, 700)
								&& pc.getInventory().checkItem(40308, 35000000)) {
							pc.getInventory().consumeEnchantItem(21264, 9, 1);
							pc.getInventory().consumeItem(6670, 700);
							pc.getInventory().consumeItem(40308, 35000000);
							인첸트지급(pc, 33913, 1, 9);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21264, 9, 1)
								&& pc.getInventory().checkItem(66710, 540)
								&& pc.getInventory().checkItem(40308, 35000000)) {
							pc.getInventory().consumeEnchantItem(21264, 9, 1);
							pc.getInventory().consumeItem(66710, 540);
							pc.getInventory().consumeItem(40308, 35000000);
							인첸트지급(pc, 33913, 1, 9);
							return;
						}
						if (pc.getInventory().checkEnchantItem(21264, 9, 1)
								&& pc.getInventory().checkItem(66710, 540)
								&& pc.getInventory().checkItem(60492, 25000000)) {
							pc.getInventory().consumeEnchantItem(21264, 9, 1);
							pc.getInventory().consumeItem(66710, 540);
							pc.getInventory().consumeItem(60492, 25000000);
							인첸트지급(pc, 33913, 1, 9);
							return;
						}
					}
				} else if (itemtype == 2775 || itemtype == 2776 || itemtype == 2777) {
					int 지급템번호 = 0;
					if (itemtype == 2775) {
						지급템번호 = 9207;
					}
					if (itemtype == 2776) {
						지급템번호 = 9209;
					}
					if (itemtype == 2777) {
						지급템번호 = 9208;
					}

					if (pc.getInventory().checkItem(6670, 30) && pc.getInventory().checkItem(60492, 1000000)) {
						pc.getInventory().consumeItem(6670, 30);
						pc.getInventory().consumeItem(60492, 1000000);
						인첸트지급(pc, 지급템번호, 1, 0);
						return;
					}
					if (pc.getInventory().checkItem(6670, 30) && pc.getInventory().checkItem(40308, 1500000)) {
						pc.getInventory().consumeItem(6670, 30);
						pc.getInventory().consumeItem(40308, 1500000);
						인첸트지급(pc, 지급템번호, 1, 0);
						return;
					}
					if (pc.getInventory().checkItem(66710, 20) && pc.getInventory().checkItem(60492, 1000000)) {
						pc.getInventory().consumeItem(66710, 20);
						pc.getInventory().consumeItem(60492, 1000000);
						인첸트지급(pc, 지급템번호, 1, 0);
						return;
					}
					if (pc.getInventory().checkItem(66710, 20) && pc.getInventory().checkItem(40308, 1500000)) {
						pc.getInventory().consumeItem(66710, 20);
						pc.getInventory().consumeItem(40308, 1500000);
						인첸트지급(pc, 지급템번호, 1, 0);
						return;
					}
				} else if (itemtype == 2778) {

					if (pc.getInventory().checkItem(6670, 40) && pc.getInventory().checkItem(60492, 4000000)) {
						pc.getInventory().consumeItem(6670, 40);
						pc.getInventory().consumeItem(60492, 4000000);
						인첸트지급(pc, 9210, 1, 0);
						return;
					}
					if (pc.getInventory().checkItem(6670, 40) && pc.getInventory().checkItem(40308, 4000000)) {
						pc.getInventory().consumeItem(6670, 40);
						pc.getInventory().consumeItem(40308, 4000000);
						인첸트지급(pc, 9210, 1, 0);
						return;
					}
					if (pc.getInventory().checkItem(66710, 30) && pc.getInventory().checkItem(60492, 3000000)) {
						pc.getInventory().consumeItem(66710, 30);
						pc.getInventory().consumeItem(60492, 1000000);
						인첸트지급(pc, 9210, 1, 0);
						return;
					}
					if (pc.getInventory().checkItem(66710, 30) && pc.getInventory().checkItem(40308, 4000000)) {
						pc.getInventory().consumeItem(66710, 30);
						pc.getInventory().consumeItem(40308, 4000000);
						인첸트지급(pc, 9210, 1, 0);
						return;
					}
					// 4396,4397,4398,4399, 수호의 투사문장 5,6,7,8
					// 4400,4401,4402,4403, 수호의 명궁문장 5,6,7,8
					// 4404,4405,4406,4407, 수호의 현자문장 5,6,7,8
				} else if (itemtype == 4396 || itemtype == 4397 || itemtype == 4398 || itemtype == 4399) {
					int enchant = 0;
					if (itemtype == 4396) {
						enchant = 5;
					}
					if (itemtype == 4397) {
						enchant = 6;
					}
					if (itemtype == 4398) {
						enchant = 7;
					}
					if (itemtype == 4399) {
						enchant = 8;
					}
					if (pc.getInventory().checkEnchantItem(31910, enchant, 1)
							&& pc.getInventory().checkEnchantItem(31913, enchant, 1)) {
						pc.getInventory().consumeEnchantItem(31910, enchant, 1);
						pc.getInventory().consumeEnchantItem(31913, enchant, 1);
						인첸트지급(pc, 32910, 1, enchant);
					}
				} else if (itemtype == 4400 || itemtype == 4401 || itemtype == 4402 || itemtype == 4403) {
					int enchant = 0;
					if (itemtype == 4400) {
						enchant = 5;
					}
					if (itemtype == 4401) {
						enchant = 6;
					}
					if (itemtype == 4402) {
						enchant = 7;
					}
					if (itemtype == 4403) {
						enchant = 8;
					}
					if (pc.getInventory().checkEnchantItem(31911, enchant, 1)
							&& pc.getInventory().checkEnchantItem(31913, enchant, 1)) {
						pc.getInventory().consumeEnchantItem(31911, enchant, 1);
						pc.getInventory().consumeEnchantItem(31913, enchant, 1);
						인첸트지급(pc, 32911, 1, enchant);
					}
				} else if (itemtype == 4404 || itemtype == 4405 || itemtype == 4406 || itemtype == 4407) {
					int enchant = 0;
					if (itemtype == 4404) {
						enchant = 5;
					}
					if (itemtype == 4405) {
						enchant = 6;
					}
					if (itemtype == 4406) {
						enchant = 7;
					}
					if (itemtype == 4407) {
						enchant = 8;
					}
					if (pc.getInventory().checkEnchantItem(31912, enchant, 1)
							&& pc.getInventory().checkEnchantItem(31913, enchant, 1)) {
						pc.getInventory().consumeEnchantItem(31912, enchant, 1);
						pc.getInventory().consumeEnchantItem(31913, enchant, 1);
						인첸트지급(pc, 32912, 1, enchant);
					}

				} else if (itemtype == 3456 || itemtype == 3457 || itemtype == 3458 || itemtype == 3459
						|| itemtype == 3460 || itemtype == 3461 || itemtype == 3462 || itemtype == 3463
						|| itemtype == 3464 || itemtype == 3465 || itemtype == 3466 || itemtype == 3467
						|| itemtype == 3468 || itemtype == 3469 || itemtype == 3470) { // 신기백
																						// 제작
					int enchant = 0;
					int 환보카운트 = 0;
					int 조각카운트 = 0;
					int 베리카운트 = 0;
					int 아덴카운트 = 0;
					int 회수템번호 = 0;
					int 지급템번호 = 0;
					if (itemtype == 3456) { // 5신기백 지급
						enchant = 5;
						환보카운트 = 180;
						조각카운트 = 140;
						베리카운트 = 17000000;
						아덴카운트 = 21000000;
						회수템번호 = 420009;
						지급템번호 = 422209;
					}
					if (itemtype == 3457) { // 6신기백 지급
						enchant = 6;
						환보카운트 = 310;
						조각카운트 = 240;
						베리카운트 = 19000000;
						아덴카운트 = 26000000;
						회수템번호 = 420009;
						지급템번호 = 422209;
					}
					if (itemtype == 3458) { // 7신기백 지급
						enchant = 7;
						환보카운트 = 440;
						조각카운트 = 340;
						베리카운트 = 21000000;
						아덴카운트 = 29000000;
						회수템번호 = 420009;
						지급템번호 = 422209;
					}
					if (itemtype == 3459) { // 8신기백 지급
						enchant = 8;
						환보카운트 = 570;
						조각카운트 = 440;
						베리카운트 = 23000000;
						아덴카운트 = 32000000;
						회수템번호 = 420009;
						지급템번호 = 422209;
					}
					if (itemtype == 3460) { // 9신기백 지급
						enchant = 9;
						환보카운트 = 700;
						조각카운트 = 540;
						베리카운트 = 25000000;
						아덴카운트 = 35000000;
						회수템번호 = 420009;
						지급템번호 = 422209;
					}
					if (itemtype == 3461) { // 5신마왕 지급
						enchant = 5;
						환보카운트 = 180;
						조각카운트 = 140;
						베리카운트 = 17000000;
						아덴카운트 = 21000000;
						회수템번호 = 420008;
						지급템번호 = 422208;
					}
					if (itemtype == 3462) { // 6신마왕 지급
						enchant = 6;
						환보카운트 = 310;
						조각카운트 = 240;
						베리카운트 = 19000000;
						아덴카운트 = 26000000;
						회수템번호 = 420008;
						지급템번호 = 422208;
					}
					if (itemtype == 3463) { // 7신마왕 지급
						enchant = 7;
						환보카운트 = 440;
						조각카운트 = 340;
						베리카운트 = 21000000;
						아덴카운트 = 29000000;
						회수템번호 = 420008;
						지급템번호 = 422208;
					}
					if (itemtype == 3464) { // 8신마왕 지급
						enchant = 8;
						환보카운트 = 570;
						조각카운트 = 440;
						베리카운트 = 23000000;
						아덴카운트 = 32000000;
						회수템번호 = 420008;
						지급템번호 = 422208;
					}
					if (itemtype == 3465) { // 9신마왕 지급
						enchant = 9;
						환보카운트 = 700;
						조각카운트 = 540;
						베리카운트 = 25000000;
						아덴카운트 = 35000000;
						회수템번호 = 420008;
						지급템번호 = 422208;
					}
					if (itemtype == 3466) { // 5신명궁 지급
						enchant = 5;
						환보카운트 = 180;
						조각카운트 = 140;
						베리카운트 = 17000000;
						아덴카운트 = 21000000;
						회수템번호 = 21267;
						지급템번호 = 22267;
					}
					if (itemtype == 3467) { // 6신명궁 지급
						enchant = 6;
						환보카운트 = 310;
						조각카운트 = 240;
						베리카운트 = 19000000;
						아덴카운트 = 26000000;
						회수템번호 = 21267;
						지급템번호 = 22267;
					}
					if (itemtype == 3468) { // 7신명궁 지급
						enchant = 7;
						환보카운트 = 440;
						조각카운트 = 340;
						베리카운트 = 21000000;
						아덴카운트 = 29000000;
						회수템번호 = 21267;
						지급템번호 = 22267;
					}
					if (itemtype == 3469) { // 8신명궁 지급
						enchant = 8;
						환보카운트 = 570;
						조각카운트 = 440;
						베리카운트 = 23000000;
						아덴카운트 = 32000000;
						회수템번호 = 21267;
						지급템번호 = 22267;
					}
					if (itemtype == 3470) { // 9신명궁 지급
						enchant = 9;
						환보카운트 = 700;
						조각카운트 = 540;
						베리카운트 = 25000000;
						아덴카운트 = 35000000;
						회수템번호 = 21267;
						지급템번호 = 22267;
					}

					if (pc.getInventory().checkEnchantItem(회수템번호, enchant, 1)
							&& pc.getInventory().checkItem(6670, 환보카운트)
							&& pc.getInventory().checkItem(60492, 베리카운트)) {
						pc.getInventory().consumeEnchantItem(회수템번호, enchant, 1);
						pc.getInventory().consumeItem(6670, 환보카운트);
						pc.getInventory().consumeItem(60492, 베리카운트);
						인첸트지급(pc, 지급템번호, 1, enchant);
						return;
					}
					if (pc.getInventory().checkEnchantItem(회수템번호, enchant, 1)
							&& pc.getInventory().checkItem(6670, 환보카운트)
							&& pc.getInventory().checkItem(40308, 아덴카운트)) {
						pc.getInventory().consumeEnchantItem(회수템번호, enchant, 1);
						pc.getInventory().consumeItem(6670, 환보카운트);
						pc.getInventory().consumeItem(40308, 아덴카운트);
						인첸트지급(pc, 지급템번호, 1, enchant);
						return;
					}
					if (pc.getInventory().checkEnchantItem(회수템번호, enchant, 1)
							&& pc.getInventory().checkItem(66710, 조각카운트)
							&& pc.getInventory().checkItem(60492, 베리카운트)) {
						pc.getInventory().consumeEnchantItem(회수템번호, enchant, 1);
						pc.getInventory().consumeItem(66710, 조각카운트);
						pc.getInventory().consumeItem(60492, 베리카운트);
						인첸트지급(pc, 지급템번호, 1, enchant);
						return;
					}
					if (pc.getInventory().checkEnchantItem(회수템번호, enchant, 1)
							&& pc.getInventory().checkItem(66710, 조각카운트)
							&& pc.getInventory().checkItem(40308, 아덴카운트)) {
						pc.getInventory().consumeEnchantItem(회수템번호, enchant, 1);
						pc.getInventory().consumeItem(66710, 조각카운트);
						pc.getInventory().consumeItem(40308, 아덴카운트);
						인첸트지급(pc, 지급템번호, 1, enchant);
						return;
					}
				} else if (itemtype == 3393 || itemtype == 3394 || itemtype == 3395 || itemtype == 3396
						|| itemtype == 3397 || itemtype == 3398 || itemtype == 3399 || itemtype == 3400
						|| itemtype == 3401 || itemtype == 3402 || itemtype == 3403 || itemtype == 3404
						|| itemtype == 3405 || itemtype == 3406 || itemtype == 3407) {
					int enchant = 0;
					int 환보카운트 = 0;
					int 조각카운트 = 0;
					int 베리카운트 = 0;
					int 아덴카운트 = 0;
					int 지급템번호 = 0;
					if (itemtype == 3393) { // 5신완반 지급
						enchant = 5;
						환보카운트 = 130;
						조각카운트 = 100;
						베리카운트 = 5000000;
						아덴카운트 = 7000000;
						지급템번호 = 9400;
					}
					if (itemtype == 3394) { // 5신민반 지급
						enchant = 5;
						환보카운트 = 130;
						조각카운트 = 100;
						베리카운트 = 5000000;
						아덴카운트 = 7000000;
						지급템번호 = 9401;
					}
					if (itemtype == 3395) { // 5신지반 지급
						enchant = 5;
						환보카운트 = 130;
						조각카운트 = 100;
						베리카운트 = 5000000;
						아덴카운트 = 7000000;
						지급템번호 = 9402;
					}
					if (itemtype == 3396) { // 6신완반 지급
						enchant = 6;
						환보카운트 = 260;
						조각카운트 = 200;
						베리카운트 = 10000000;
						아덴카운트 = 14000000;
						지급템번호 = 9400;
					}
					if (itemtype == 3397) { // 6신민반 지급
						enchant = 6;
						환보카운트 = 260;
						조각카운트 = 200;
						베리카운트 = 10000000;
						아덴카운트 = 14000000;
						지급템번호 = 9401;
					}
					if (itemtype == 3398) { // 6신지반 지급
						enchant = 6;
						환보카운트 = 260;
						조각카운트 = 200;
						베리카운트 = 10000000;
						아덴카운트 = 14000000;
						지급템번호 = 9402;
					}
					if (itemtype == 3399) { // 7신완반 지급
						enchant = 7;
						환보카운트 = 390;
						조각카운트 = 300;
						베리카운트 = 15000000;
						아덴카운트 = 21000000;
						지급템번호 = 9400;
					}
					if (itemtype == 3400) { // 7신민반 지급
						enchant = 7;
						환보카운트 = 390;
						조각카운트 = 300;
						베리카운트 = 15000000;
						아덴카운트 = 21000000;
						지급템번호 = 9401;
					}
					if (itemtype == 3401) { // 7신지반 지급
						enchant = 7;
						환보카운트 = 390;
						조각카운트 = 300;
						베리카운트 = 15000000;
						아덴카운트 = 21000000;
						지급템번호 = 9402;
					}
					if (itemtype == 3402) { // 8신완반 지급
						enchant = 8;
						환보카운트 = 520;
						조각카운트 = 400;
						베리카운트 = 20000000;
						아덴카운트 = 28000000;
						지급템번호 = 9400;
					}
					if (itemtype == 3403) { // 8신민반 지급
						enchant = 8;
						환보카운트 = 520;
						조각카운트 = 400;
						베리카운트 = 20000000;
						아덴카운트 = 28000000;
						지급템번호 = 9401;
					}
					if (itemtype == 3404) { // 8신지반 지급
						enchant = 8;
						환보카운트 = 520;
						조각카운트 = 400;
						베리카운트 = 20000000;
						아덴카운트 = 28000000;
						지급템번호 = 9402;
					}
					if (itemtype == 3405) { // 9신완반 지급
						enchant = 9;
						환보카운트 = 650;
						조각카운트 = 500;
						베리카운트 = 25000000;
						아덴카운트 = 35000000;
						지급템번호 = 9400;
					}
					if (itemtype == 3406) { // 9신민반 지급
						enchant = 9;
						환보카운트 = 650;
						조각카운트 = 500;
						베리카운트 = 25000000;
						아덴카운트 = 35000000;
						지급템번호 = 9401;
					}
					if (itemtype == 3407) { // 9신지반 지급
						enchant = 9;
						환보카운트 = 650;
						조각카운트 = 500;
						베리카운트 = 25000000;
						아덴카운트 = 35000000;
						지급템번호 = 9402;
					}
					if (pc.getInventory().checkEnchantItem(7214, enchant, 1)
							&& pc.getInventory().checkItem(6670, 환보카운트)
							&& pc.getInventory().checkItem(60492, 베리카운트)) {
						pc.getInventory().consumeEnchantItem(7214, enchant, 1);
						pc.getInventory().consumeItem(6670, 환보카운트);
						pc.getInventory().consumeItem(60492, 베리카운트);
						인첸트지급(pc, 지급템번호, 1, enchant);
						return;
					}
					if (pc.getInventory().checkEnchantItem(7214, enchant, 1)
							&& pc.getInventory().checkItem(6670, 환보카운트)
							&& pc.getInventory().checkItem(40308, 아덴카운트)) {
						pc.getInventory().consumeEnchantItem(7214, enchant, 1);
						pc.getInventory().consumeItem(6670, 환보카운트);
						pc.getInventory().consumeItem(40308, 아덴카운트);
						인첸트지급(pc, 지급템번호, 1, enchant);
						return;
					}
					if (pc.getInventory().checkEnchantItem(7214, enchant, 1)
							&& pc.getInventory().checkItem(66710, 조각카운트)
							&& pc.getInventory().checkItem(60492, 베리카운트)) {
						pc.getInventory().consumeEnchantItem(7214, enchant, 1);
						pc.getInventory().consumeItem(66710, 조각카운트);
						pc.getInventory().consumeItem(60492, 베리카운트);
						인첸트지급(pc, 지급템번호, 1, enchant);
						return;
					}
					if (pc.getInventory().checkEnchantItem(7214, enchant, 1)
							&& pc.getInventory().checkItem(66710, 조각카운트)
							&& pc.getInventory().checkItem(40308, 아덴카운트)) {
						pc.getInventory().consumeEnchantItem(7214, enchant, 1);
						pc.getInventory().consumeItem(66710, 조각카운트);
						pc.getInventory().consumeItem(40308, 아덴카운트);
						인첸트지급(pc, 지급템번호, 1, enchant);
						return;
					}
					if (pc.getInventory().checkEnchantItem(7215, enchant, 1)
							&& pc.getInventory().checkItem(6670, 환보카운트)
							&& pc.getInventory().checkItem(60492, 베리카운트)) {
						pc.getInventory().consumeEnchantItem(7215, enchant, 1);
						pc.getInventory().consumeItem(6670, 환보카운트);
						pc.getInventory().consumeItem(60492, 베리카운트);
						인첸트지급(pc, 지급템번호, 1, enchant);
						return;
					}
					if (pc.getInventory().checkEnchantItem(7215, enchant, 1)
							&& pc.getInventory().checkItem(6670, 환보카운트)
							&& pc.getInventory().checkItem(40308, 아덴카운트)) {
						pc.getInventory().consumeEnchantItem(7215, enchant, 1);
						pc.getInventory().consumeItem(6670, 환보카운트);
						pc.getInventory().consumeItem(40308, 아덴카운트);
						인첸트지급(pc, 지급템번호, 1, enchant);
						return;
					}
					if (pc.getInventory().checkEnchantItem(7215, enchant, 1)
							&& pc.getInventory().checkItem(66710, 조각카운트)
							&& pc.getInventory().checkItem(60492, 베리카운트)) {
						pc.getInventory().consumeEnchantItem(7215, enchant, 1);
						pc.getInventory().consumeItem(66710, 조각카운트);
						pc.getInventory().consumeItem(60492, 베리카운트);
						인첸트지급(pc, 지급템번호, 1, enchant);
						return;
					}
					if (pc.getInventory().checkEnchantItem(7215, enchant, 1)
							&& pc.getInventory().checkItem(66710, 조각카운트)
							&& pc.getInventory().checkItem(40308, 아덴카운트)) {
						pc.getInventory().consumeEnchantItem(7215, enchant, 1);
						pc.getInventory().consumeItem(66710, 조각카운트);
						pc.getInventory().consumeItem(40308, 아덴카운트);
						인첸트지급(pc, 지급템번호, 1, enchant);
						return;
					}
					if (pc.getInventory().checkEnchantItem(30229, enchant, 1)
							&& pc.getInventory().checkItem(6670, 환보카운트)
							&& pc.getInventory().checkItem(60492, 베리카운트)) {
						pc.getInventory().consumeEnchantItem(30229, enchant, 1);
						pc.getInventory().consumeItem(6670, 환보카운트);
						pc.getInventory().consumeItem(60492, 베리카운트);
						인첸트지급(pc, 지급템번호, 1, enchant);
						return;
					}
					if (pc.getInventory().checkEnchantItem(30229, enchant, 1)
							&& pc.getInventory().checkItem(6670, 환보카운트)
							&& pc.getInventory().checkItem(40308, 아덴카운트)) {
						pc.getInventory().consumeEnchantItem(30229, enchant, 1);
						pc.getInventory().consumeItem(6670, 환보카운트);
						pc.getInventory().consumeItem(40308, 아덴카운트);
						인첸트지급(pc, 지급템번호, 1, enchant);
						return;
					}
					if (pc.getInventory().checkEnchantItem(30229, enchant, 1)
							&& pc.getInventory().checkItem(66710, 조각카운트)
							&& pc.getInventory().checkItem(60492, 베리카운트)) {
						pc.getInventory().consumeEnchantItem(30229, enchant, 1);
						pc.getInventory().consumeItem(66710, 조각카운트);
						pc.getInventory().consumeItem(60492, 베리카운트);
						인첸트지급(pc, 지급템번호, 1, enchant);
						return;
					}
					if (pc.getInventory().checkEnchantItem(30229, enchant, 1)
							&& pc.getInventory().checkItem(66710, 조각카운트)
							&& pc.getInventory().checkItem(40308, 아덴카운트)) {
						pc.getInventory().consumeEnchantItem(30229, enchant, 1);
						pc.getInventory().consumeItem(66710, 조각카운트);
						pc.getInventory().consumeItem(40308, 아덴카운트);
						인첸트지급(pc, 지급템번호, 1, enchant);
						return;
					}
				} else if (itemtype == 2619 || itemtype == 2620 || itemtype == 2621) { // 속성
																						// 3단
																						// 10나양
																						// >
																						// 속성
																						// 3단
																						// 0드슬
					int Attrlvl1 = 0;
					int Attrlvl2 = 0;
					int Attrlvl3 = 0;
					int Attrlvl4 = 0;
					if (itemtype == 2619) {
						Attrlvl1 = 3;
						Attrlvl2 = 6;
						Attrlvl3 = 9;
						Attrlvl4 = 12;
					}
					if (itemtype == 2620) {
						Attrlvl1 = 33;
						Attrlvl2 = 35;
						Attrlvl3 = 37;
						Attrlvl4 = 39;
					}
					if (itemtype == 2621) {
						Attrlvl1 = 34;
						Attrlvl2 = 36;
						Attrlvl3 = 38;
						Attrlvl4 = 40;
					}
					if (pc.getInventory().checkAttrEnchantItem(59, 10, 1, Attrlvl1)
							&& pc.getInventory().checkItem(40346, 3) && pc.getInventory().checkItem(40354, 3)
							&& pc.getInventory().checkItem(40362, 3) && pc.getInventory().checkItem(40370, 3)
							&& pc.getInventory().checkItem(40308, 10000000)) {
						pc.getInventory().consumeAttrEnchantItem(59, 10, 1, Attrlvl1);
						pc.getInventory().consumeItem(40346, 3);
						pc.getInventory().consumeItem(40354, 3);
						pc.getInventory().consumeItem(40362, 3);
						pc.getInventory().consumeItem(40370, 3);
						pc.getInventory().consumeItem(40308, 10000000);
						속성인첸트지급(pc, 66, 1, 0, Attrlvl1);
					}
					if (pc.getInventory().checkAttrEnchantItem(59, 10, 1, Attrlvl2)
							&& pc.getInventory().checkItem(40346, 3) && pc.getInventory().checkItem(40354, 3)
							&& pc.getInventory().checkItem(40362, 3) && pc.getInventory().checkItem(40370, 3)
							&& pc.getInventory().checkItem(40308, 10000000)) {
						pc.getInventory().consumeAttrEnchantItem(59, 10, 1, Attrlvl2);
						pc.getInventory().consumeItem(40346, 3);
						pc.getInventory().consumeItem(40354, 3);
						pc.getInventory().consumeItem(40362, 3);
						pc.getInventory().consumeItem(40370, 3);
						pc.getInventory().consumeItem(40308, 10000000);
						속성인첸트지급(pc, 66, 1, 0, Attrlvl2);
					}
					if (pc.getInventory().checkAttrEnchantItem(59, 10, 1, Attrlvl3)
							&& pc.getInventory().checkItem(40346, 3) && pc.getInventory().checkItem(40354, 3)
							&& pc.getInventory().checkItem(40362, 3) && pc.getInventory().checkItem(40370, 3)
							&& pc.getInventory().checkItem(40308, 10000000)) {
						pc.getInventory().consumeAttrEnchantItem(59, 10, 1, Attrlvl3);
						pc.getInventory().consumeItem(40346, 3);
						pc.getInventory().consumeItem(40354, 3);
						pc.getInventory().consumeItem(40362, 3);
						pc.getInventory().consumeItem(40370, 3);
						pc.getInventory().consumeItem(40308, 10000000);
						속성인첸트지급(pc, 66, 1, 0, Attrlvl3);
					}
					if (pc.getInventory().checkAttrEnchantItem(59, 10, 1, Attrlvl4)
							&& pc.getInventory().checkItem(40346, 3) && pc.getInventory().checkItem(40354, 3)
							&& pc.getInventory().checkItem(40362, 3) && pc.getInventory().checkItem(40370, 3)
							&& pc.getInventory().checkItem(40308, 10000000)) {
						pc.getInventory().consumeAttrEnchantItem(59, 10, 1, Attrlvl4);
						pc.getInventory().consumeItem(40346, 3);
						pc.getInventory().consumeItem(40354, 3);
						pc.getInventory().consumeItem(40362, 3);
						pc.getInventory().consumeItem(40370, 3);
						pc.getInventory().consumeItem(40308, 10000000);
						속성인첸트지급(pc, 66, 1, 0, Attrlvl4);
					}

				} else if (itemtype >= 3715 && itemtype <= 3724) {
					int oElvl = itemtype - 3715;
					int nElvl = oElvl + 1;
					int Lchance = 0;
					/** 무기별 인첸당 확률 **/
					L1EnchantWeaponChance l1enchantchance = EnchantWeaponChance.getInstance().getTemplate(61);
					int e0 = l1enchantchance.get0();
					int e1 = l1enchantchance.get1();
					int e2 = l1enchantchance.get2();
					int e3 = l1enchantchance.get3();
					int e4 = l1enchantchance.get4();
					int e5 = l1enchantchance.get5();
					int e6 = l1enchantchance.get6();
					int e7 = l1enchantchance.get7();
					int e8 = l1enchantchance.get8();
					int e9 = l1enchantchance.get9();
					int e10 = l1enchantchance.get10();
					int e11 = l1enchantchance.get11();
					int e12 = l1enchantchance.get12();
					int e13 = l1enchantchance.get13();
					int e14 = l1enchantchance.get14();
					try {
						if (l1enchantchance != null) {
							int enchant = oElvl;
							switch (enchant) {
							case 0:
								Lchance += e0;
								break;
							case 1:
								Lchance += e1;
								break;
							case 2:
								Lchance += e2;
								break;
							case 3:
								Lchance += e3;
								break;
							case 4:
								Lchance += e4;
								break;
							case 5:
								Lchance += e5;
								break;
							case 6:
								Lchance += e6;
								break;
							case 7:
								Lchance += e7;
								break;
							case 8:
								Lchance += e8;
								break;
							case 9:
								Lchance += e9;
								break;
							case 10:
								Lchance += e10;
								break;
							case 11:
								Lchance += e11;
								break;
							case 12:
								Lchance += e12;
								break;
							case 13:
								Lchance += e13;
								break;
							case 14:
								Lchance += e14;
								break;
							default:
								break;
							}
						}
					} catch (Exception e) {
						System.out.println("Character Enchant Weapon Chance Load Error");
					}
					if (pc.getInventory().checkAttrEnchantItem(61, oElvl, 1, 34)
							&& pc.getInventory().checkItem(40346, 1) && pc.getInventory().checkItem(40354, 1)
							&& pc.getInventory().checkItem(40362, 1) && pc.getInventory().checkItem(40370, 1)
							&& pc.getInventory().checkItem(40087, 500)) {
						if (Chance > Lchance) { // 인첸 성공률
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패하였습니다."), true);
						} else {
							pc.getInventory().consumeAttrEnchantItem(61, oElvl, 1, 34);
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							속성인첸트지급(pc, 61, 1, nElvl, 34);
						}
					}
					if (pc.getInventory().checkAttrEnchantItem(61, oElvl, 1, 36)
							&& pc.getInventory().checkItem(40346, 1) && pc.getInventory().checkItem(40354, 1)
							&& pc.getInventory().checkItem(40362, 1) && pc.getInventory().checkItem(40370, 1)
							&& pc.getInventory().checkItem(40087, 500)) {
						if (Chance > Lchance) { // 인첸 성공률
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패하였습니다."), true);
						} else {
							pc.getInventory().consumeAttrEnchantItem(61, oElvl, 1, 36);
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							속성인첸트지급(pc, 61, 1, nElvl, 36);
						}
					}
					if (pc.getInventory().checkAttrEnchantItem(61, oElvl, 1, 38)
							&& pc.getInventory().checkItem(40346, 1) && pc.getInventory().checkItem(40354, 1)
							&& pc.getInventory().checkItem(40362, 1) && pc.getInventory().checkItem(40370, 1)
							&& pc.getInventory().checkItem(40087, 500)) {
						if (Chance > Lchance) { // 인첸 성공률
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패하였습니다."), true);
						} else {
							pc.getInventory().consumeAttrEnchantItem(61, oElvl, 1, 38);
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							속성인첸트지급(pc, 61, 1, nElvl, 38);
						}
					}
					if (pc.getInventory().checkAttrEnchantItem(61, oElvl, 1, 40)
							&& pc.getInventory().checkItem(40346, 1) && pc.getInventory().checkItem(40354, 1)
							&& pc.getInventory().checkItem(40362, 1) && pc.getInventory().checkItem(40370, 1)
							&& pc.getInventory().checkItem(40087, 500)) {
						if (Chance > Lchance) { // 인첸 성공률
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패하였습니다."), true);
						} else {
							pc.getInventory().consumeAttrEnchantItem(61, oElvl, 1, 40);
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							속성인첸트지급(pc, 61, 1, nElvl, 40);
						}
					}
				} else if (itemtype >= 3728 && itemtype <= 3737) {
					int oElvl = itemtype - 3728;
					int nElvl = oElvl + 1;
					int Lchance = 0;
					/** 무기별 인첸당 확률 **/
					L1EnchantWeaponChance l1enchantchance = EnchantWeaponChance.getInstance().getTemplate(12);
					int e0 = l1enchantchance.get0();
					int e1 = l1enchantchance.get1();
					int e2 = l1enchantchance.get2();
					int e3 = l1enchantchance.get3();
					int e4 = l1enchantchance.get4();
					int e5 = l1enchantchance.get5();
					int e6 = l1enchantchance.get6();
					int e7 = l1enchantchance.get7();
					int e8 = l1enchantchance.get8();
					int e9 = l1enchantchance.get9();
					int e10 = l1enchantchance.get10();
					int e11 = l1enchantchance.get11();
					int e12 = l1enchantchance.get12();
					int e13 = l1enchantchance.get13();
					int e14 = l1enchantchance.get14();
					try {
						if (l1enchantchance != null) {
							int enchant = oElvl;
							switch (enchant) {
							case 0:
								Lchance += e0;
								break;
							case 1:
								Lchance += e1;
								break;
							case 2:
								Lchance += e2;
								break;
							case 3:
								Lchance += e3;
								break;
							case 4:
								Lchance += e4;
								break;
							case 5:
								Lchance += e5;
								break;
							case 6:
								Lchance += e6;
								break;
							case 7:
								Lchance += e7;
								break;
							case 8:
								Lchance += e8;
								break;
							case 9:
								Lchance += e9;
								break;
							case 10:
								Lchance += e10;
								break;
							case 11:
								Lchance += e11;
								break;
							case 12:
								Lchance += e12;
								break;
							case 13:
								Lchance += e13;
								break;
							case 14:
								Lchance += e14;
								break;
							default:
								break;
							}
						}
					} catch (Exception e) {
						System.out.println("Character Enchant Weapon Chance Load Error");
					}
					if (pc.getInventory().checkAttrEnchantItem(12, oElvl, 1, 34)
							&& pc.getInventory().checkItem(40346, 1) && pc.getInventory().checkItem(40354, 1)
							&& pc.getInventory().checkItem(40362, 1) && pc.getInventory().checkItem(40370, 1)
							&& pc.getInventory().checkItem(40087, 500)) {
						if (Chance > Lchance) { // 인첸 성공률
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패하였습니다."), true);
						} else {
							pc.getInventory().consumeAttrEnchantItem(12, oElvl, 1, 34);
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							속성인첸트지급(pc, 12, 1, nElvl, 34);
						}
					}
					if (pc.getInventory().checkAttrEnchantItem(12, oElvl, 1, 36)
							&& pc.getInventory().checkItem(40346, 1) && pc.getInventory().checkItem(40354, 1)
							&& pc.getInventory().checkItem(40362, 1) && pc.getInventory().checkItem(40370, 1)
							&& pc.getInventory().checkItem(40087, 500)) {
						if (Chance > Lchance) { // 인첸 성공률
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패하였습니다."), true);
						} else {
							pc.getInventory().consumeAttrEnchantItem(12, oElvl, 1, 36);
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							속성인첸트지급(pc, 12, 1, nElvl, 36);
						}
					}
					if (pc.getInventory().checkAttrEnchantItem(12, oElvl, 1, 38)
							&& pc.getInventory().checkItem(40346, 1) && pc.getInventory().checkItem(40354, 1)
							&& pc.getInventory().checkItem(40362, 1) && pc.getInventory().checkItem(40370, 1)
							&& pc.getInventory().checkItem(40087, 500)) {
						if (Chance > Lchance) { // 인첸 성공률
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패하였습니다."), true);
						} else {
							pc.getInventory().consumeAttrEnchantItem(12, oElvl, 1, 38);
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							속성인첸트지급(pc, 12, 1, nElvl, 38);
						}
					}
					if (pc.getInventory().checkAttrEnchantItem(12, oElvl, 1, 40)
							&& pc.getInventory().checkItem(40346, 1) && pc.getInventory().checkItem(40354, 1)
							&& pc.getInventory().checkItem(40362, 1) && pc.getInventory().checkItem(40370, 1)
							&& pc.getInventory().checkItem(40087, 500)) {
						if (Chance > Lchance) { // 인첸 성공률
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패하였습니다."), true);
						} else {
							pc.getInventory().consumeAttrEnchantItem(12, oElvl, 1, 40);
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							속성인첸트지급(pc, 12, 1, nElvl, 40);
						}
					}
				} else if (itemtype >= 3741 && itemtype <= 3750) {
					int oElvl = itemtype - 3741;
					int nElvl = oElvl + 1;
					int Lchance = 0;
					/** 무기별 인첸당 확률 **/
					L1EnchantWeaponChance l1enchantchance = EnchantWeaponChance.getInstance().getTemplate(134);
					int e0 = l1enchantchance.get0();
					int e1 = l1enchantchance.get1();
					int e2 = l1enchantchance.get2();
					int e3 = l1enchantchance.get3();
					int e4 = l1enchantchance.get4();
					int e5 = l1enchantchance.get5();
					int e6 = l1enchantchance.get6();
					int e7 = l1enchantchance.get7();
					int e8 = l1enchantchance.get8();
					int e9 = l1enchantchance.get9();
					int e10 = l1enchantchance.get10();
					int e11 = l1enchantchance.get11();
					int e12 = l1enchantchance.get12();
					int e13 = l1enchantchance.get13();
					int e14 = l1enchantchance.get14();
					try {
						if (l1enchantchance != null) {
							int enchant = oElvl;
							switch (enchant) {
							case 0:
								Lchance += e0;
								break;
							case 1:
								Lchance += e1;
								break;
							case 2:
								Lchance += e2;
								break;
							case 3:
								Lchance += e3;
								break;
							case 4:
								Lchance += e4;
								break;
							case 5:
								Lchance += e5;
								break;
							case 6:
								Lchance += e6;
								break;
							case 7:
								Lchance += e7;
								break;
							case 8:
								Lchance += e8;
								break;
							case 9:
								Lchance += e9;
								break;
							case 10:
								Lchance += e10;
								break;
							case 11:
								Lchance += e11;
								break;
							case 12:
								Lchance += e12;
								break;
							case 13:
								Lchance += e13;
								break;
							case 14:
								Lchance += e14;
								break;
							default:
								break;
							}
						}
					} catch (Exception e) {
						System.out.println("Character Enchant Weapon Chance Load Error");
					}
					if (pc.getInventory().checkAttrEnchantItem(134, oElvl, 1, 34)
							&& pc.getInventory().checkItem(40346, 1) && pc.getInventory().checkItem(40354, 1)
							&& pc.getInventory().checkItem(40362, 1) && pc.getInventory().checkItem(40370, 1)
							&& pc.getInventory().checkItem(40087, 500)) {
						if (Chance > Lchance) { // 인첸 성공률
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패하였습니다."), true);
						} else {
							pc.getInventory().consumeAttrEnchantItem(134, oElvl, 1, 34);
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							속성인첸트지급(pc, 134, 1, nElvl, 34);
						}
					}
					if (pc.getInventory().checkAttrEnchantItem(134, oElvl, 1, 36)
							&& pc.getInventory().checkItem(40346, 1) && pc.getInventory().checkItem(40354, 1)
							&& pc.getInventory().checkItem(40362, 1) && pc.getInventory().checkItem(40370, 1)
							&& pc.getInventory().checkItem(40087, 500)) {
						if (Chance > Lchance) { // 인첸 성공률
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패하였습니다."), true);
						} else {
							pc.getInventory().consumeAttrEnchantItem(134, oElvl, 1, 36);
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							속성인첸트지급(pc, 134, 1, nElvl, 36);
						}
					}
					if (pc.getInventory().checkAttrEnchantItem(134, oElvl, 1, 38)
							&& pc.getInventory().checkItem(40346, 1) && pc.getInventory().checkItem(40354, 1)
							&& pc.getInventory().checkItem(40362, 1) && pc.getInventory().checkItem(40370, 1)
							&& pc.getInventory().checkItem(40087, 500)) {
						if (Chance > Lchance) { // 인첸 성공률
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패하였습니다."), true);
						} else {
							pc.getInventory().consumeAttrEnchantItem(134, oElvl, 1, 38);
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							속성인첸트지급(pc, 134, 1, nElvl, 38);
						}
					}
					if (pc.getInventory().checkAttrEnchantItem(134, oElvl, 1, 40)
							&& pc.getInventory().checkItem(40346, 1) && pc.getInventory().checkItem(40354, 1)
							&& pc.getInventory().checkItem(40362, 1) && pc.getInventory().checkItem(40370, 1)
							&& pc.getInventory().checkItem(40087, 500)) {
						if (Chance > Lchance) { // 인첸 성공률
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패하였습니다."), true);
						} else {
							pc.getInventory().consumeAttrEnchantItem(134, oElvl, 1, 40);
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							속성인첸트지급(pc, 134, 1, nElvl, 40);
						}
					}
				} else if (itemtype >= 3754 && itemtype <= 3763) {

					int oElvl = itemtype - 3754;
					int nElvl = oElvl + 1;
					int Lchance = 0;
					/** 무기별 인첸당 확률 **/
					L1EnchantWeaponChance l1enchantchance = EnchantWeaponChance.getInstance().getTemplate(86);
					int e0 = l1enchantchance.get0();
					int e1 = l1enchantchance.get1();
					int e2 = l1enchantchance.get2();
					int e3 = l1enchantchance.get3();
					int e4 = l1enchantchance.get4();
					int e5 = l1enchantchance.get5();
					int e6 = l1enchantchance.get6();
					int e7 = l1enchantchance.get7();
					int e8 = l1enchantchance.get8();
					int e9 = l1enchantchance.get9();
					int e10 = l1enchantchance.get10();
					int e11 = l1enchantchance.get11();
					int e12 = l1enchantchance.get12();
					int e13 = l1enchantchance.get13();
					int e14 = l1enchantchance.get14();
					try {
						if (l1enchantchance != null) {
							int enchant = oElvl;
							switch (enchant) {
							case 0:
								Lchance += e0;
								break;
							case 1:
								Lchance += e1;
								break;
							case 2:
								Lchance += e2;
								break;
							case 3:
								Lchance += e3;
								break;
							case 4:
								Lchance += e4;
								break;
							case 5:
								Lchance += e5;
								break;
							case 6:
								Lchance += e6;
								break;
							case 7:
								Lchance += e7;
								break;
							case 8:
								Lchance += e8;
								break;
							case 9:
								Lchance += e9;
								break;
							case 10:
								Lchance += e10;
								break;
							case 11:
								Lchance += e11;
								break;
							case 12:
								Lchance += e12;
								break;
							case 13:
								Lchance += e13;
								break;
							case 14:
								Lchance += e14;
								break;
							default:
								break;
							}
						}
					} catch (Exception e) {
						System.out.println("Character Enchant Weapon Chance Load Error");
					}
					if (pc.getInventory().checkAttrEnchantItem(86, oElvl, 1, 34)
							&& pc.getInventory().checkItem(40346, 1) && pc.getInventory().checkItem(40354, 1)
							&& pc.getInventory().checkItem(40362, 1) && pc.getInventory().checkItem(40370, 1)
							&& pc.getInventory().checkItem(40087, 500)) {
						if (Chance > Lchance) { // 인첸 성공률
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패하였습니다."), true);
						} else {
							pc.getInventory().consumeAttrEnchantItem(86, oElvl, 1, 34);
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							속성인첸트지급(pc, 86, 1, nElvl, 34);
						}
					}
					if (pc.getInventory().checkAttrEnchantItem(86, oElvl, 1, 36)
							&& pc.getInventory().checkItem(40346, 1) && pc.getInventory().checkItem(40354, 1)
							&& pc.getInventory().checkItem(40362, 1) && pc.getInventory().checkItem(40370, 1)
							&& pc.getInventory().checkItem(40087, 500)) {
						if (Chance > Lchance) { // 인첸 성공률
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패하였습니다."), true);
						} else {
							pc.getInventory().consumeAttrEnchantItem(86, oElvl, 1, 36);
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							속성인첸트지급(pc, 86, 1, nElvl, 36);
						}
					}
					if (pc.getInventory().checkAttrEnchantItem(86, oElvl, 1, 38)
							&& pc.getInventory().checkItem(40346, 1) && pc.getInventory().checkItem(40354, 1)
							&& pc.getInventory().checkItem(40362, 1) && pc.getInventory().checkItem(40370, 1)
							&& pc.getInventory().checkItem(40087, 500)) {
						if (Chance > Lchance) { // 인첸 성공률
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패하였습니다."), true);
						} else {
							pc.getInventory().consumeAttrEnchantItem(86, oElvl, 1, 38);
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							속성인첸트지급(pc, 86, 1, nElvl, 38);
						}
					}
					if (pc.getInventory().checkAttrEnchantItem(86, oElvl, 1, 40)
							&& pc.getInventory().checkItem(40346, 1) && pc.getInventory().checkItem(40354, 1)
							&& pc.getInventory().checkItem(40362, 1) && pc.getInventory().checkItem(40370, 1)
							&& pc.getInventory().checkItem(40087, 500)) {
						if (Chance > Lchance) { // 인첸 성공률
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패하였습니다."), true);
						} else {
							pc.getInventory().consumeAttrEnchantItem(86, oElvl, 1, 40);
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							속성인첸트지급(pc, 86, 1, nElvl, 40);
						}
					}
				} else if (itemtype >= 3767 && itemtype <= 3776) {
					int oElvl = itemtype - 3767;
					int nElvl = oElvl + 1;
					int Lchance = 0;
					/** 무기별 인첸당 확률 **/
					L1EnchantWeaponChance l1enchantchance = EnchantWeaponChance.getInstance().getTemplate(9100);
					int e0 = l1enchantchance.get0();
					int e1 = l1enchantchance.get1();
					int e2 = l1enchantchance.get2();
					int e3 = l1enchantchance.get3();
					int e4 = l1enchantchance.get4();
					int e5 = l1enchantchance.get5();
					int e6 = l1enchantchance.get6();
					int e7 = l1enchantchance.get7();
					int e8 = l1enchantchance.get8();
					int e9 = l1enchantchance.get9();
					int e10 = l1enchantchance.get10();
					int e11 = l1enchantchance.get11();
					int e12 = l1enchantchance.get12();
					int e13 = l1enchantchance.get13();
					int e14 = l1enchantchance.get14();
					try {
						if (l1enchantchance != null) {
							int enchant = oElvl;
							switch (enchant) {
							case 0:
								Lchance += e0;
								break;
							case 1:
								Lchance += e1;
								break;
							case 2:
								Lchance += e2;
								break;
							case 3:
								Lchance += e3;
								break;
							case 4:
								Lchance += e4;
								break;
							case 5:
								Lchance += e5;
								break;
							case 6:
								Lchance += e6;
								break;
							case 7:
								Lchance += e7;
								break;
							case 8:
								Lchance += e8;
								break;
							case 9:
								Lchance += e9;
								break;
							case 10:
								Lchance += e10;
								break;
							case 11:
								Lchance += e11;
								break;
							case 12:
								Lchance += e12;
								break;
							case 13:
								Lchance += e13;
								break;
							case 14:
								Lchance += e14;
								break;
							default:
								break;
							}
						}
					} catch (Exception e) {
						System.out.println("Character Enchant Weapon Chance Load Error");
					}
					if (pc.getInventory().checkAttrEnchantItem(9100, oElvl, 1, 34)
							&& pc.getInventory().checkItem(40346, 1) && pc.getInventory().checkItem(40354, 1)
							&& pc.getInventory().checkItem(40362, 1) && pc.getInventory().checkItem(40370, 1)
							&& pc.getInventory().checkItem(40087, 500)) {
						if (Chance > Lchance) { // 인첸 성공률
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패하였습니다."), true);
						} else {
							pc.getInventory().consumeAttrEnchantItem(9100, oElvl, 1, 34);
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							속성인첸트지급(pc, 9100, 1, nElvl, 34);
						}
					}
					if (pc.getInventory().checkAttrEnchantItem(9100, oElvl, 1, 36)
							&& pc.getInventory().checkItem(40346, 1) && pc.getInventory().checkItem(40354, 1)
							&& pc.getInventory().checkItem(40362, 1) && pc.getInventory().checkItem(40370, 1)
							&& pc.getInventory().checkItem(40087, 500)) {
						if (Chance > Lchance) { // 인첸 성공률
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패하였습니다."), true);
						} else {
							pc.getInventory().consumeAttrEnchantItem(9100, oElvl, 1, 36);
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							속성인첸트지급(pc, 9100, 1, nElvl, 36);
						}
					}
					if (pc.getInventory().checkAttrEnchantItem(9100, oElvl, 1, 38)
							&& pc.getInventory().checkItem(40346, 1) && pc.getInventory().checkItem(40354, 1)
							&& pc.getInventory().checkItem(40362, 1) && pc.getInventory().checkItem(40370, 1)
							&& pc.getInventory().checkItem(40087, 500)) {
						if (Chance > Lchance) { // 인첸 성공률
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패하였습니다."), true);
						} else {
							pc.getInventory().consumeAttrEnchantItem(9100, oElvl, 1, 38);
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							속성인첸트지급(pc, 9100, 1, nElvl, 38);
						}
					}
					if (pc.getInventory().checkAttrEnchantItem(9100, oElvl, 1, 40)
							&& pc.getInventory().checkItem(40346, 1) && pc.getInventory().checkItem(40354, 1)
							&& pc.getInventory().checkItem(40362, 1) && pc.getInventory().checkItem(40370, 1)
							&& pc.getInventory().checkItem(40087, 500)) {
						if (Chance > Lchance) { // 인첸 성공률
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패하였습니다."), true);
						} else {
							pc.getInventory().consumeAttrEnchantItem(9100, oElvl, 1, 40);
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							속성인첸트지급(pc, 9100, 1, nElvl, 40);
						}
					}
				} else if (itemtype >= 3780 && itemtype <= 3789) {
					int oElvl = itemtype - 3780;
					int nElvl = oElvl + 1;
					int Lchance = 0;
					/** 무기별 인첸당 확률 **/
					L1EnchantWeaponChance l1enchantchance = EnchantWeaponChance.getInstance().getTemplate(9101);
					int e0 = l1enchantchance.get0();
					int e1 = l1enchantchance.get1();
					int e2 = l1enchantchance.get2();
					int e3 = l1enchantchance.get3();
					int e4 = l1enchantchance.get4();
					int e5 = l1enchantchance.get5();
					int e6 = l1enchantchance.get6();
					int e7 = l1enchantchance.get7();
					int e8 = l1enchantchance.get8();
					int e9 = l1enchantchance.get9();
					int e10 = l1enchantchance.get10();
					int e11 = l1enchantchance.get11();
					int e12 = l1enchantchance.get12();
					int e13 = l1enchantchance.get13();
					int e14 = l1enchantchance.get14();
					try {
						if (l1enchantchance != null) {
							int enchant = oElvl;
							switch (enchant) {
							case 0:
								Lchance += e0;
								break;
							case 1:
								Lchance += e1;
								break;
							case 2:
								Lchance += e2;
								break;
							case 3:
								Lchance += e3;
								break;
							case 4:
								Lchance += e4;
								break;
							case 5:
								Lchance += e5;
								break;
							case 6:
								Lchance += e6;
								break;
							case 7:
								Lchance += e7;
								break;
							case 8:
								Lchance += e8;
								break;
							case 9:
								Lchance += e9;
								break;
							case 10:
								Lchance += e10;
								break;
							case 11:
								Lchance += e11;
								break;
							case 12:
								Lchance += e12;
								break;
							case 13:
								Lchance += e13;
								break;
							case 14:
								Lchance += e14;
								break;
							default:
								break;
							}
						}
					} catch (Exception e) {
						System.out.println("Character Enchant Weapon Chance Load Error");
					}
					if (pc.getInventory().checkAttrEnchantItem(9101, oElvl, 1, 34)
							&& pc.getInventory().checkItem(40346, 1) && pc.getInventory().checkItem(40354, 1)
							&& pc.getInventory().checkItem(40362, 1) && pc.getInventory().checkItem(40370, 1)
							&& pc.getInventory().checkItem(40087, 500)) {
						if (Chance > Lchance) { // 인첸 성공률
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패하였습니다."), true);
						} else {
							pc.getInventory().consumeAttrEnchantItem(9101, oElvl, 1, 34);
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							속성인첸트지급(pc, 9101, 1, nElvl, 34);
						}
					}
					if (pc.getInventory().checkAttrEnchantItem(9101, oElvl, 1, 36)
							&& pc.getInventory().checkItem(40346, 1) && pc.getInventory().checkItem(40354, 1)
							&& pc.getInventory().checkItem(40362, 1) && pc.getInventory().checkItem(40370, 1)
							&& pc.getInventory().checkItem(40087, 500)) {
						if (Chance > Lchance) { // 인첸 성공률
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패하였습니다."), true);
						} else {
							pc.getInventory().consumeAttrEnchantItem(9101, oElvl, 1, 36);
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							속성인첸트지급(pc, 9101, 1, nElvl, 36);
						}
					}
					if (pc.getInventory().checkAttrEnchantItem(9101, oElvl, 1, 38)
							&& pc.getInventory().checkItem(40346, 1) && pc.getInventory().checkItem(40354, 1)
							&& pc.getInventory().checkItem(40362, 1) && pc.getInventory().checkItem(40370, 1)
							&& pc.getInventory().checkItem(40087, 500)) {
						if (Chance > Lchance) { // 인첸 성공률
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패하였습니다."), true);
						} else {
							pc.getInventory().consumeAttrEnchantItem(9101, oElvl, 1, 38);
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							속성인첸트지급(pc, 9101, 1, nElvl, 38);
						}
					}
					if (pc.getInventory().checkAttrEnchantItem(9101, oElvl, 1, 40)
							&& pc.getInventory().checkItem(40346, 1) && pc.getInventory().checkItem(40354, 1)
							&& pc.getInventory().checkItem(40362, 1) && pc.getInventory().checkItem(40370, 1)
							&& pc.getInventory().checkItem(40087, 500)) {
						if (Chance > Lchance) { // 인첸 성공률
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패하였습니다."), true);
						} else {
							pc.getInventory().consumeAttrEnchantItem(9101, oElvl, 1, 40);
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							속성인첸트지급(pc, 9101, 1, nElvl, 40);
						}
					}
				} else if (itemtype >= 3793 && itemtype <= 3802) {
					int oElvl = itemtype - 3793;
					int nElvl = oElvl + 1;
					int Lchance = 0;
					/** 무기별 인첸당 확률 **/
					L1EnchantWeaponChance l1enchantchance = EnchantWeaponChance.getInstance().getTemplate(9102);
					int e0 = l1enchantchance.get0();
					int e1 = l1enchantchance.get1();
					int e2 = l1enchantchance.get2();
					int e3 = l1enchantchance.get3();
					int e4 = l1enchantchance.get4();
					int e5 = l1enchantchance.get5();
					int e6 = l1enchantchance.get6();
					int e7 = l1enchantchance.get7();
					int e8 = l1enchantchance.get8();
					int e9 = l1enchantchance.get9();
					int e10 = l1enchantchance.get10();
					int e11 = l1enchantchance.get11();
					int e12 = l1enchantchance.get12();
					int e13 = l1enchantchance.get13();
					int e14 = l1enchantchance.get14();
					try {
						if (l1enchantchance != null) {
							int enchant = oElvl;
							switch (enchant) {
							case 0:
								Lchance += e0;
								break;
							case 1:
								Lchance += e1;
								break;
							case 2:
								Lchance += e2;
								break;
							case 3:
								Lchance += e3;
								break;
							case 4:
								Lchance += e4;
								break;
							case 5:
								Lchance += e5;
								break;
							case 6:
								Lchance += e6;
								break;
							case 7:
								Lchance += e7;
								break;
							case 8:
								Lchance += e8;
								break;
							case 9:
								Lchance += e9;
								break;
							case 10:
								Lchance += e10;
								break;
							case 11:
								Lchance += e11;
								break;
							case 12:
								Lchance += e12;
								break;
							case 13:
								Lchance += e13;
								break;
							case 14:
								Lchance += e14;
								break;
							default:
								break;
							}
						}
					} catch (Exception e) {
						System.out.println("Character Enchant Weapon Chance Load Error");
					}
					if (pc.getInventory().checkAttrEnchantItem(9102, oElvl, 1, 34)
							&& pc.getInventory().checkItem(40346, 1) && pc.getInventory().checkItem(40354, 1)
							&& pc.getInventory().checkItem(40362, 1) && pc.getInventory().checkItem(40370, 1)
							&& pc.getInventory().checkItem(40087, 500)) {
						if (Chance > Lchance) { // 인첸 성공률
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패하였습니다."), true);
						} else {
							pc.getInventory().consumeAttrEnchantItem(9102, oElvl, 1, 34);
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							속성인첸트지급(pc, 9102, 1, nElvl, 34);
						}
					}
					if (pc.getInventory().checkAttrEnchantItem(9102, oElvl, 1, 36)
							&& pc.getInventory().checkItem(40346, 1) && pc.getInventory().checkItem(40354, 1)
							&& pc.getInventory().checkItem(40362, 1) && pc.getInventory().checkItem(40370, 1)
							&& pc.getInventory().checkItem(40087, 500)) {
						if (Chance > Lchance) { // 인첸 성공률
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패하였습니다."), true);
						} else {
							pc.getInventory().consumeAttrEnchantItem(9102, oElvl, 1, 36);
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							속성인첸트지급(pc, 9102, 1, nElvl, 36);
						}
					}
					if (pc.getInventory().checkAttrEnchantItem(9102, oElvl, 1, 38)
							&& pc.getInventory().checkItem(40346, 1) && pc.getInventory().checkItem(40354, 1)
							&& pc.getInventory().checkItem(40362, 1) && pc.getInventory().checkItem(40370, 1)
							&& pc.getInventory().checkItem(40087, 500)) {
						if (Chance > Lchance) { // 인첸 성공률
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패하였습니다."), true);
						} else {
							pc.getInventory().consumeAttrEnchantItem(9102, oElvl, 1, 38);
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							속성인첸트지급(pc, 9102, 1, nElvl, 38);
						}
					}
					if (pc.getInventory().checkAttrEnchantItem(9102, oElvl, 1, 40)
							&& pc.getInventory().checkItem(40346, 1) && pc.getInventory().checkItem(40354, 1)
							&& pc.getInventory().checkItem(40362, 1) && pc.getInventory().checkItem(40370, 1)
							&& pc.getInventory().checkItem(40087, 500)) {
						if (Chance > Lchance) { // 인첸 성공률
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패하였습니다."), true);
						} else {
							pc.getInventory().consumeAttrEnchantItem(9102, oElvl, 1, 40);
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							속성인첸트지급(pc, 9102, 1, nElvl, 40);
						}
					}
				} else if (itemtype >= 3806 && itemtype <= 3815) {
					int oElvl = itemtype - 3806;
					int nElvl = oElvl + 1;
					int Lchance = 0;
					/** 무기별 인첸당 확률 **/
					L1EnchantWeaponChance l1enchantchance = EnchantWeaponChance.getInstance().getTemplate(9103);
					int e0 = l1enchantchance.get0();
					int e1 = l1enchantchance.get1();
					int e2 = l1enchantchance.get2();
					int e3 = l1enchantchance.get3();
					int e4 = l1enchantchance.get4();
					int e5 = l1enchantchance.get5();
					int e6 = l1enchantchance.get6();
					int e7 = l1enchantchance.get7();
					int e8 = l1enchantchance.get8();
					int e9 = l1enchantchance.get9();
					int e10 = l1enchantchance.get10();
					int e11 = l1enchantchance.get11();
					int e12 = l1enchantchance.get12();
					int e13 = l1enchantchance.get13();
					int e14 = l1enchantchance.get14();
					try {
						if (l1enchantchance != null) {
							int enchant = oElvl;
							switch (enchant) {
							case 0:
								Lchance += e0;
								break;
							case 1:
								Lchance += e1;
								break;
							case 2:
								Lchance += e2;
								break;
							case 3:
								Lchance += e3;
								break;
							case 4:
								Lchance += e4;
								break;
							case 5:
								Lchance += e5;
								break;
							case 6:
								Lchance += e6;
								break;
							case 7:
								Lchance += e7;
								break;
							case 8:
								Lchance += e8;
								break;
							case 9:
								Lchance += e9;
								break;
							case 10:
								Lchance += e10;
								break;
							case 11:
								Lchance += e11;
								break;
							case 12:
								Lchance += e12;
								break;
							case 13:
								Lchance += e13;
								break;
							case 14:
								Lchance += e14;
								break;
							default:
								break;
							}
						}
					} catch (Exception e) {
						System.out.println("Character Enchant Weapon Chance Load Error");
					}
					if (pc.getInventory().checkAttrEnchantItem(9103, oElvl, 1, 34)
							&& pc.getInventory().checkItem(40346, 1) && pc.getInventory().checkItem(40354, 1)
							&& pc.getInventory().checkItem(40362, 1) && pc.getInventory().checkItem(40370, 1)
							&& pc.getInventory().checkItem(40087, 500)) {
						if (Chance > Lchance) { // 인첸 성공률
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패하였습니다."), true);
						} else {
							pc.getInventory().consumeAttrEnchantItem(9103, oElvl, 1, 34);
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							속성인첸트지급(pc, 9103, 1, nElvl, 34);
						}
					}
					if (pc.getInventory().checkAttrEnchantItem(9103, oElvl, 1, 36)
							&& pc.getInventory().checkItem(40346, 1) && pc.getInventory().checkItem(40354, 1)
							&& pc.getInventory().checkItem(40362, 1) && pc.getInventory().checkItem(40370, 1)
							&& pc.getInventory().checkItem(40087, 500)) {
						if (Chance > Lchance) { // 인첸 성공률
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패하였습니다."), true);
						} else {
							pc.getInventory().consumeAttrEnchantItem(9103, oElvl, 1, 36);
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							속성인첸트지급(pc, 9103, 1, nElvl, 36);
						}
					}
					if (pc.getInventory().checkAttrEnchantItem(9103, oElvl, 1, 38)
							&& pc.getInventory().checkItem(40346, 1) && pc.getInventory().checkItem(40354, 1)
							&& pc.getInventory().checkItem(40362, 1) && pc.getInventory().checkItem(40370, 1)
							&& pc.getInventory().checkItem(40087, 500)) {
						if (Chance > Lchance) { // 인첸 성공률
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패하였습니다."), true);
						} else {
							pc.getInventory().consumeAttrEnchantItem(9103, oElvl, 1, 38);
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							속성인첸트지급(pc, 9103, 1, nElvl, 38);
						}
					}
					if (pc.getInventory().checkAttrEnchantItem(9103, oElvl, 1, 40)
							&& pc.getInventory().checkItem(40346, 1) && pc.getInventory().checkItem(40354, 1)
							&& pc.getInventory().checkItem(40362, 1) && pc.getInventory().checkItem(40370, 1)
							&& pc.getInventory().checkItem(40087, 500)) {
						if (Chance > Lchance) { // 인첸 성공률
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패하였습니다."), true);
						} else {
							pc.getInventory().consumeAttrEnchantItem(9103, oElvl, 1, 40);
							pc.getInventory().consumeItem(40346, 1);
							pc.getInventory().consumeItem(40354, 1);
							pc.getInventory().consumeItem(40362, 1);
							pc.getInventory().consumeItem(40370, 1);
							pc.getInventory().consumeItem(40087, 500);
							속성인첸트지급(pc, 9103, 1, nElvl, 40);
						}
					}
				} else if (itemtype >= 2622 && itemtype <= 2651) {
					int oElvl = 0;
					int nElvl = 0;
					int Echance = 0;
					int Attrlvl1 = 0;
					int Attrlvl2 = 0;
					int Attrlvl3 = 0;
					int Attrlvl4 = 0;

					if (itemtype >= 2622 && itemtype <= 2631) {
						Attrlvl1 = 3;
						Attrlvl2 = 6;
						Attrlvl3 = 9;
						Attrlvl4 = 12;
					}
					if (itemtype >= 2632 && itemtype <= 2641) {
						Attrlvl1 = 33;
						Attrlvl2 = 35;
						Attrlvl3 = 37;
						Attrlvl4 = 39;
					}
					if (itemtype >= 2642 && itemtype <= 2651) {
						Attrlvl1 = 34;
						Attrlvl2 = 36;
						Attrlvl3 = 38;
						Attrlvl4 = 40;
					}

					if (itemtype == 2622 || itemtype == 2632 || itemtype == 2642) {
						oElvl = 0;
						nElvl = 1;
						Echance = 50;
					}
					if (itemtype == 2623 || itemtype == 2633 || itemtype == 2643) {
						oElvl = 1;
						nElvl = 2;
						Echance = 40;
					}
					if (itemtype == 2624 || itemtype == 2634 || itemtype == 2644) {
						oElvl = 2;
						nElvl = 3;
						Echance = 30;
					}
					if (itemtype == 2625 || itemtype == 2635 || itemtype == 2645) {
						oElvl = 3;
						nElvl = 4;
						Echance = 20;
					}
					if (itemtype == 2626 || itemtype == 2636 || itemtype == 2646) {
						oElvl = 4;
						nElvl = 5;
						Echance = 20;
					}
					if (itemtype == 2627 || itemtype == 2637 || itemtype == 2647) {
						oElvl = 5;
						nElvl = 6;
						Echance = 20;
					}
					if (itemtype == 2628 || itemtype == 2638 || itemtype == 2648) {
						oElvl = 6;
						nElvl = 7;
						Echance = 15;
					}
					if (itemtype == 2629 || itemtype == 2639 || itemtype == 2649) {
						oElvl = 7;
						nElvl = 8;
						Echance = 10;
					}
					if (itemtype == 2630 || itemtype == 2640 || itemtype == 2650) {
						oElvl = 8;
						nElvl = 9;
						Echance = 5;
					}
					if (itemtype == 2631 || itemtype == 2641 || itemtype == 2651) {
						oElvl = 9;
						nElvl = 10;
						Echance = 5;
					}

					if (pc.getInventory().checkAttrEnchantItem(66, oElvl, 1, Attrlvl1)
							&& pc.getInventory().checkItem(40346, 1) && pc.getInventory().checkItem(40354, 1)
							&& pc.getInventory().checkItem(40362, 1) && pc.getInventory().checkItem(40370, 1)) {
						pc.getInventory().consumeItem(40346, 1);
						pc.getInventory().consumeItem(40354, 1);
						pc.getInventory().consumeItem(40362, 1);
						pc.getInventory().consumeItem(40370, 1);
						if (Chance < Echance) {
							pc.getInventory().consumeAttrEnchantItem(66, oElvl, 1, Attrlvl1);
							속성인첸트지급(pc, 66, 1, nElvl, Attrlvl1);
						} else {
							pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패하였습니다."), true);
						}
					}
					if (pc.getInventory().checkAttrEnchantItem(66, oElvl, 1, Attrlvl2)
							&& pc.getInventory().checkItem(40346, 1) && pc.getInventory().checkItem(40354, 1)
							&& pc.getInventory().checkItem(40362, 1) && pc.getInventory().checkItem(40370, 1)) {
						pc.getInventory().consumeItem(40346, 1);
						pc.getInventory().consumeItem(40354, 1);
						pc.getInventory().consumeItem(40362, 1);
						pc.getInventory().consumeItem(40370, 1);
						if (Chance < Echance) {
							pc.getInventory().consumeAttrEnchantItem(66, oElvl, 1, Attrlvl2);
							속성인첸트지급(pc, 66, 1, nElvl, Attrlvl2);
						} else {
							pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패하였습니다."), true);
						}
					}
					if (pc.getInventory().checkAttrEnchantItem(66, oElvl, 1, Attrlvl3)
							&& pc.getInventory().checkItem(40346, 1) && pc.getInventory().checkItem(40354, 1)
							&& pc.getInventory().checkItem(40362, 1) && pc.getInventory().checkItem(40370, 1)) {
						pc.getInventory().consumeItem(40346, 1);
						pc.getInventory().consumeItem(40354, 1);
						pc.getInventory().consumeItem(40362, 1);
						pc.getInventory().consumeItem(40370, 1);
						if (Chance < Echance) {
							pc.getInventory().consumeAttrEnchantItem(66, oElvl, 1, Attrlvl3);
							속성인첸트지급(pc, 66, 1, nElvl, Attrlvl3);
						} else {
							pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패하였습니다."), true);
						}
					}
					if (pc.getInventory().checkAttrEnchantItem(66, oElvl, 1, Attrlvl4)
							&& pc.getInventory().checkItem(40346, 1) && pc.getInventory().checkItem(40354, 1)
							&& pc.getInventory().checkItem(40362, 1) && pc.getInventory().checkItem(40370, 1)) {
						pc.getInventory().consumeItem(40346, 1);
						pc.getInventory().consumeItem(40354, 1);
						pc.getInventory().consumeItem(40362, 1);
						pc.getInventory().consumeItem(40370, 1);
						if (Chance < Echance) {
							pc.getInventory().consumeAttrEnchantItem(66, oElvl, 1, Attrlvl4);
							속성인첸트지급(pc, 66, 1, nElvl, Attrlvl4);
						} else {
							pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패하였습니다."), true);
						}
					}
				} else if ((itemtype >= 103) && (itemtype <= 106)) {
					int newEnchant = 0;
					int oldEnchant = 0;
					int itemnum = 0;
					if (itemtype == 103) {
						newEnchant = 430102;
						oldEnchant = 5000064;
						itemnum = 430106;
					}
					if (itemtype == 104) {
						newEnchant = 430100;
						oldEnchant = 5000066;
						itemnum = 430104;
					}
					if (itemtype == 105) {
						newEnchant = 430103;
						oldEnchant = 5000063;
						itemnum = 430107;
					}
					if (itemtype == 106) {
						newEnchant = 430101;
						oldEnchant = 5000065;
						itemnum = 430105;
					}
					if (pc.getInventory().checkItem(newEnchant, 1) && pc.getInventory().checkItem(40308, 100000)) {
						pc.getInventory().consumeItem(newEnchant, 1);
						pc.getInventory().consumeItem(40308, 100000);
						if (Chance < 50) {
							인첸트지급(pc, itemnum, 1, 0);
							return;
						} else {
							pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패하였습니다."), true);
							return;
						}
					} else if (pc.getInventory().checkItem(oldEnchant, 32)
							&& pc.getInventory().checkItem(40308, 100000)) {
						pc.getInventory().consumeItem(oldEnchant, 32);
						pc.getInventory().consumeItem(40308, 100000);
						if (Chance < 50) {
							인첸트지급(pc, itemnum, 1, 0);
							return;
						} else {
							pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패하였습니다."), true);
							return;
						}
					}
				} else if (itemtype == 107) {
					if (pc.getInventory().checkItem(430106, 1) && pc.getInventory().checkItem(430104, 1)
							&& pc.getInventory().checkItem(40308, 200000)) {
						pc.getInventory().consumeItem(430106, 1);
						pc.getInventory().consumeItem(430104, 1);
						pc.getInventory().consumeItem(40308, 200000);
					}
					if (Chance < 30) {
						인첸트지급(pc, 430108, 1, 0);
						return;
					} else {
						pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패하였습니다."), true);
						return;
					}
				} else if (itemtype == 108) {
					if (pc.getInventory().checkItem(430108, 1) && pc.getInventory().checkItem(430105, 1)
							&& pc.getInventory().checkItem(40308, 200000)) {
						pc.getInventory().consumeItem(430105, 1);
						pc.getInventory().consumeItem(40308, 200000);
					}
					if (Chance < 30) {
						pc.getInventory().consumeItem(430108, 1);
						인첸트지급(pc, 430109, 1, 0);
						return;
					} else {
						pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패하였습니다."), true);
						return;
					}
				} else if (itemtype == 109) {
					if (pc.getInventory().checkItem(430109, 1) && pc.getInventory().checkItem(430107, 1)
							&& pc.getInventory().checkItem(40308, 200000)) {
						pc.getInventory().consumeItem(430107, 1);
						pc.getInventory().consumeItem(40308, 200000);
					}
					if (Chance < 10) {
						pc.getInventory().consumeItem(430109, 1);
						인첸트지급(pc, 430110, 1, 0);
						return;
					} else {
						pc.sendPackets(new S_SystemMessage(pc, "아이템 지급에 실패하였습니다."), true);
						return;
					}
				} else if (itemtype == 1729) { // 복원된 고대 목걸이
					if (pc.getInventory().checkItem(41139, 1) && pc.getInventory().checkItem(49028, 1)
							&& pc.getInventory().checkItem(49029, 1) && pc.getInventory().checkItem(49030, 1)) {
						pc.getInventory().consumeItem(41139, 1);
						pc.getInventory().consumeItem(49028, 1);
						pc.getInventory().consumeItem(49029, 1);
						pc.getInventory().consumeItem(49030, 1);
					}
					if (Chance < 50) {
						인첸트지급(pc, 41140, 1, 0);
						return;
					} else {
						pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패했습니다."), true);
						return;
					}
				} else if (itemtype == 1730) { // 빛나는 고대 목걸이
					if (pc.getInventory().checkItem(41140, 1) && pc.getInventory().checkItem(49027, 1)) {
						pc.getInventory().consumeItem(41140, 1);
						pc.getInventory().consumeItem(49027, 1);
					}
					if (Chance < 30) {
						인첸트지급(pc, 20422, 1, 0);
						return;
					} else {
						pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패했습니다."), true);
						return;
					}
				} else if (itemtype == 156) {
					if (pc.getInventory().checkItem(40718, 2000) && pc.getInventory().checkItem(40991, 1)) {
						pc.getInventory().consumeItem(40718, 2000);
						pc.getInventory().consumeItem(40991, 1);
						if (Chance < 10) {
							인첸트지급(pc, 203, 1, 0);
							return;
						} else {
							pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패했습니다."), true);
							return;
						}
					}
					if (pc.getInventory().checkItem(60062, 20) && pc.getInventory().checkItem(40991, 1)) {
						pc.getInventory().consumeItem(60062, 20);
						pc.getInventory().consumeItem(40991, 1);
						if (Chance < 10) {
							인첸트지급(pc, 203, 1, 0);
							return;
						} else {
							pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패했습니다."), true);
							return;
						}
					}
				} else if (itemtype == 3599) {
					if (pc.getInventory().checkItem(20288, 2) && pc.getInventory().checkItem(40393, 5)
							&& pc.getInventory().checkItem(6670, 50)) {
						pc.getInventory().consumeItem(20288, 2);
						pc.getInventory().consumeItem(40393, 5);
						pc.getInventory().consumeItem(6670, 50);
						if (Chance < Config.JJE_SUCC) {
							인첸트지급(pc, 21288, 1, 0);
							return;
						} else {
							pc.getInventory().storeItem(20288, 1);
							pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패했습니다."), true);
							return;
						}
					}
					if (pc.getInventory().checkItem(20288, 2) && pc.getInventory().checkItem(40394, 5)
							&& pc.getInventory().checkItem(6670, 50)) {
						pc.getInventory().consumeItem(20288, 2);
						pc.getInventory().consumeItem(40394, 5);
						pc.getInventory().consumeItem(6670, 50);
						if (Chance < 80) {
							인첸트지급(pc, 21288, 1, 0);
							return;
						} else {
							pc.getInventory().storeItem(20288, 1);
							pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패했습니다."), true);
							return;
						}
					}
					if (pc.getInventory().checkItem(20288, 2) && pc.getInventory().checkItem(40395, 5)
							&& pc.getInventory().checkItem(6670, 50)) {
						pc.getInventory().consumeItem(20288, 2);
						pc.getInventory().consumeItem(40395, 5);
						pc.getInventory().consumeItem(6670, 50);
						if (Chance < 80) {
							인첸트지급(pc, 21288, 1, 0);
							return;
						} else {
							pc.getInventory().storeItem(20288, 1);
							pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패했습니다."), true);
							return;
						}
					}
					if (pc.getInventory().checkItem(20288, 2) && pc.getInventory().checkItem(40396, 5)
							&& pc.getInventory().checkItem(6670, 50)) {
						pc.getInventory().consumeItem(20288, 2);
						pc.getInventory().consumeItem(40396, 5);
						pc.getInventory().consumeItem(6670, 50);
						if (Chance < 80) {
							인첸트지급(pc, 21288, 1, 0);
							return;
						} else {
							pc.getInventory().storeItem(20288, 1);
							pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패했습니다."), true);
							return;
						}
					}
				} else if (itemtype == 3503) {
					if (pc.getInventory().checkItem(60476, 5) && pc.getInventory().checkItem(149027, 5)) {
						pc.getInventory().consumeItem(60476, 5);
						pc.getInventory().consumeItem(149027, 5);
						인첸트지급(pc, 600228, 1, 0);
						return;
					} else if (pc.getInventory().checkItem(60476, 5)
							&& pc.getInventory().checkItem(40308, 250000)) {
						pc.getInventory().consumeItem(60476, 5);
						pc.getInventory().consumeItem(40308, 250000);
						인첸트지급(pc, 600228, 1, 0);
						return;
					}
				} else if (itemtype == 2036) {
					if (pc.getInventory().checkItem(40678, 25000) && pc.getInventory().checkItem(40678, 100)) {
						pc.getInventory().consumeItem(40678, 25000);
						pc.getInventory().consumeItem(40678, 100);
						if (Chance < 30) {
							인첸트지급(pc, 21027, 1, 0);
							return;
						} else {
							pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패했습니다."), true);
							return;
						}
					}
					if (pc.getInventory().checkItem(40678, 25000) && pc.getInventory().checkItem(60063, 1)) {
						pc.getInventory().consumeItem(40678, 25000);
						pc.getInventory().consumeItem(60063, 1);
						if (Chance < 30) {
							인첸트지급(pc, 21027, 1, 0);
							return;
						} else {
							pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패했습니다."), true);
							return;
						}
					}
					if (pc.getInventory().checkItem(60063, 250) && pc.getInventory().checkItem(60063, 1)) {
						pc.getInventory().consumeItem(60063, 250);
						pc.getInventory().consumeItem(60063, 1);
						if (Chance < 30) {
							인첸트지급(pc, 21027, 1, 0);
							return;
						} else {
							pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패했습니다."), true);
							return;
						}
					}
					if (pc.getInventory().checkItem(60063, 250) && pc.getInventory().checkItem(40678, 100)) {
						pc.getInventory().consumeItem(60062, 250);
						pc.getInventory().consumeItem(40678, 100);
						if (Chance < 30) {
							인첸트지급(pc, 21027, 1, 0);
							return;
						} else {
							pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패했습니다."), true);
							return;
						}
						
					}
				} else if (itemtype == 2044) {
					if (pc.getInventory().checkItem(40718, 25000) && pc.getInventory().checkItem(40718, 100)) {
						pc.getInventory().consumeItem(40718, 25000);
						pc.getInventory().consumeItem(40718, 100);
						if (Chance < 30) {
							인첸트지급(pc, 20365, 1, 0);
							return;
						} else {
							pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패했습니다."), true);
							return;
						}
						
					}
					if (pc.getInventory().checkItem(40718, 25000) && pc.getInventory().checkItem(60062, 1)) {
						pc.getInventory().consumeItem(40718, 25000);
						pc.getInventory().consumeItem(60062, 1);
						if (Chance < 30) {
							인첸트지급(pc, 20365, 1, 0);
							return;
						} else {
							pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패했습니다."), true);
							return;
						}
					}
					if (pc.getInventory().checkItem(60062, 250) && pc.getInventory().checkItem(60062, 1)) {
						pc.getInventory().consumeItem(60062, 250);
						pc.getInventory().consumeItem(60062, 1);
						if (Chance < 30) {
							인첸트지급(pc, 20365, 1, 0);
							return;
						} else {
							pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패했습니다."), true);
							return;
						}
					}
					if (pc.getInventory().checkItem(60062, 250) && pc.getInventory().checkItem(40718, 100)) {
						pc.getInventory().consumeItem(60062, 250);
						pc.getInventory().consumeItem(40718, 100);
						if (Chance < 30) {
							인첸트지급(pc, 20365, 1, 0);
							return;
						} else {
							pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패했습니다."), true);
							return;
						}
					}
				} else if (itemtype == 2747) {
					if (pc.getInventory().checkItem(7323, 3)) {
						pc.getInventory().consumeItem(7323, 3);
					}
					if (Chance < 30) {
						인첸트지급(pc, 7324, 1, 0);
					} else {
						pc.getInventory().storeItem(7323, 1);
						pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패했습니다."), true);
					}
				} else if (itemtype == 1763 || itemtype == 1764 || itemtype == 1765 || itemtype == 1766) { // 고대
																											// 암석
					if (pc.getInventory().checkItem(6673, 1) && pc.getInventory().checkItem(6671, 1)) {
						pc.getInventory().consumeItem(6673, 1);
					}
					int itemNum = 0;
					if (itemtype == 1763) { // 각반
						itemNum = 4640;
					}
					if (itemtype == 1764) { // 부츠
						itemNum = 4641;
					}
					if (itemtype == 1765) { // 망토
						itemNum = 4643;
					}
					if (itemtype == 1766) { // 장갑
						itemNum = 4642;
					}
					if (Chance < Config.FI_ARMOR) {
						인첸트지급(pc, itemNum, 1, 0);
						pc.getInventory().consumeItem(6671, 1);
					} else {
						pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패했습니다."), true);
					}
				} else if (itemtype == 1767 || itemtype == 1768 || itemtype == 1769 || itemtype == 1770) { // 고대
																											// 마물
					if (pc.getInventory().checkItem(6673, 1) && pc.getInventory().checkItem(6672, 1)) {
						pc.getInventory().consumeItem(6673, 1);
					}
					int itemNum = 0;
					if (itemtype == 1767) { // 각반
						itemNum = 4540;
					}
					if (itemtype == 1768) { // 부츠
						itemNum = 4541;
					}
					if (itemtype == 1769) { // 망토
						itemNum = 4543;
					}
					if (itemtype == 1770) { // 장갑
						itemNum = 4542;
					}
					if (Chance < Config.FI_ARMOR) {
						인첸트지급(pc, itemNum, 1, 0);
						pc.getInventory().consumeItem(6672, 1);
					} else {
						pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패했습니다."), true);
					}
				} else if (itemtype == 1771 || itemtype == 1772 || itemtype == 1773 || itemtype == 1774
						|| itemtype == 1775 || itemtype == 1776 || itemtype == 1777 || itemtype == 1778) { // 고대
																											// 법서
					if (pc.getInventory().checkItem(6673, 1)) {
						pc.getInventory().consumeItem(6673, 1);
					}
					int itemNum = 0;
					if (itemtype == 1771) { // 각반
						itemNum = 6674;
					}
					if (itemtype == 1772) { // 부츠
						itemNum = 6675;
					}
					if (itemtype == 1773) { // 망토
						itemNum = 6676;
					}
					if (itemtype == 1774) { // 장갑
						itemNum = 6677;
					}
					if (itemtype == 1775) { // 장갑
						itemNum = 6678;
					}
					if (itemtype == 1776) { // 장갑
						itemNum = 6679;
					}
					if (itemtype == 1777) { // 장갑
						itemNum = 6680;
					}
					if (itemtype == 1778) { // 장갑
						itemNum = 6681;
					}
					if (Chance < Config.FI_MAGIC) {
						인첸트지급(pc, itemNum, 1, 0);
					} else {
						pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 실패했습니다."), true);
					}
				} else if (itemtype == 4354 || itemtype == 4355 || itemtype == 4356 || itemtype == 4357 || itemtype == 4358){
					int 지급아이템 = 0;
					if (pc.getInventory().checkItem(40308, 10000000) && pc.getInventory().checkItem(7257, 1) && pc.getInventory().checkItem(7658, 1)) {
						pc.getInventory().consumeItem(40308, 10000000);
						pc.getInventory().consumeItem(7257, 1);
						pc.getInventory().consumeItem(7658, 1);
					}
					if(itemtype == 4354)
						지급아이템 = 7352;
					if(itemtype == 4355)
						지급아이템 = 7353;
					if(itemtype == 4356)
						지급아이템 = 7354;
					if(itemtype == 4357)
						지급아이템 = 7355;
					if(itemtype == 4358)
						지급아이템 = 7356;
					인첸트지급(pc, 지급아이템, 1, 0);
				} else if (itemtype == 4359 || itemtype == 4360 || itemtype == 4361 || itemtype == 4362 || itemtype == 4363){
					int 지급아이템 = 0;
					if (pc.getInventory().checkItem(40308, 30000000) && pc.getInventory().checkItem(7357, 1) && pc.getInventory().checkItem(7358, 1)) {
						pc.getInventory().consumeItem(40308, 30000000);
						pc.getInventory().consumeItem(7357, 1);
						pc.getInventory().consumeItem(7358, 1);
					}
					
					if(itemtype == 4359)
						지급아이템 = 7452;
					if(itemtype == 4360)
						지급아이템 = 7453;
					if(itemtype == 4361)
						지급아이템 = 7454;
					if(itemtype == 4362)
						지급아이템 = 7455;
					if(itemtype == 4363)
						지급아이템 = 7456;
					인첸트지급(pc, 지급아이템, 1, 0);
				} else if (itemtype == 4364 || itemtype == 4365 || itemtype == 4366 || itemtype == 4367 || itemtype == 4368){
					int 지급아이템 = 0;
					if (pc.getInventory().checkItem(40308, 50000000) && pc.getInventory().checkItem(7457, 1) && pc.getInventory().checkItem(7458, 1)) {
						pc.getInventory().consumeItem(40308, 50000000);
						pc.getInventory().consumeItem(7457, 1);
						pc.getInventory().consumeItem(7458, 1);
					}
					if(itemtype == 4364)
						지급아이템 = 7552;
					if(itemtype == 4365)
						지급아이템 = 7553;
					if(itemtype == 4366)
						지급아이템 = 7554;
					if(itemtype == 4367)
						지급아이템 = 7555;
					if(itemtype == 4368)
						지급아이템 = 7556;
					인첸트지급(pc, 지급아이템, 1, 0);
				} else if (itemtype == 3870) {
					if (pc.getInventory().checkItem(40308, 3000000) && pc.getInventory().checkItem(437008, 10)) {
						pc.getInventory().consumeItem(40308, 3000000);
						pc.getInventory().consumeItem(437008, 10);
						인첸트지급(pc, 37000, 1, 0);
						return;
					} else if (pc.getInventory().checkItem(40308, 3000000)
							&& pc.getInventory().checkItem(37001, 1)) {
						pc.getInventory().consumeItem(40308, 3000000);
						pc.getInventory().consumeItem(37001, 1);
						인첸트지급(pc, 37000, 1, 0);
					} else if (pc.getInventory().checkItem(40308, 3000000)
							&& pc.getInventory().checkItem(6670, 10)) {
						pc.getInventory().consumeItem(40308, 3000000);
						pc.getInventory().consumeItem(6670, 10);
						인첸트지급(pc, 37000, 1, 0);
					}
				} else if (itemtype == 2559){
					if (pc.getInventory().checkItem(38002, 30)
						&& pc.getInventory().checkItem(40087, 3)) {
						pc.getInventory().consumeItem(38002, 30);
						pc.getInventory().consumeItem(40087, 3);
						인첸트지급(pc, 38000, 1, 0);
					}
				} else if (itemtype == 2560){
					if (pc.getInventory().checkItem(38002, 30)
						&& pc.getInventory().checkItem(40074, 3)) {
						pc.getInventory().consumeItem(38002, 30);
						pc.getInventory().consumeItem(40074, 3);
						인첸트지급(pc, 38001, 1, 0);
					}
				} else if (itemtype == 1862){
					if (pc.getInventory().checkItem(37004, 21)) {
						pc.getInventory().consumeItem(37004, 21);
						인첸트지급(pc, 37005, 1, 0);
					}
				} else if (itemtype == 1863){
					if (pc.getInventory().checkItem(37004, 70)) {
						pc.getInventory().consumeItem(37004, 70);
						인첸트지급(pc, 37006, 1, 0);
					}
				} else if (itemtype == 1959){
					if (pc.getInventory().checkItem(37004, 90)) {
						pc.getInventory().consumeItem(37004, 90);
						인첸트지급(pc, 37007, 1, 0);
					}
				} else if (itemtype == 4589){
					if (pc.getInventory().checkItem(60203) && pc.getInventory().checkItem(40308, 500000000) && pc.getInventory().checkItem(40113, 200)) {
						pc.getInventory().consumeItem(40308, 500000000) ;
						pc.getInventory().consumeItem(40113, 200);
						인첸트지급(pc, 39100, 1, 0);
					}
				} else if (itemtype == 481){
					if (pc.getInventory().checkItem(149027, 50) && pc.getInventory().checkItem(40053, 10) && pc.getInventory().checkItem(40393, 5)) {
						pc.getInventory().consumeItem(149027, 50) ;
						pc.getInventory().consumeItem(40053, 10);
						pc.getInventory().consumeItem(40393, 5);
						인첸트지급(pc, 21259, 1, 0);
						return;
					}	else if (pc.getInventory().checkItem(149027, 50) && pc.getInventory().checkItem(40053, 10) && pc.getInventory().checkEnchantItem(20194, 7, 1)) {
						pc.getInventory().consumeItem(149027, 50) ;
						pc.getInventory().consumeItem(40053, 10);
						pc.getInventory().consumeEnchantItem(20194, 7, 1);
						인첸트지급(pc, 21259, 1, 0);
						return;
					}	
				} else if (itemtype == 482){
						if (pc.getInventory().checkItem(149027, 50) && pc.getInventory().checkItem(40052, 10) && pc.getInventory().checkItem(40396, 5)) {
							pc.getInventory().consumeItem(149027, 50) ;
							pc.getInventory().consumeItem(40052, 10);
							pc.getInventory().consumeItem(40396, 5);
							인첸트지급(pc, 30218, 1, 0);
							return;
						} else if (pc.getInventory().checkItem(149027, 50) && pc.getInventory().checkItem(40052, 10) &&  pc.getInventory().checkEnchantItem(20194, 7, 1)) {
							pc.getInventory().consumeItem(149027, 50) ;
							pc.getInventory().consumeItem(40052, 10);
							pc.getInventory().consumeEnchantItem(20194, 7, 1);
							인첸트지급(pc, 30218, 1, 0);
							return;
						}
				} else if (itemtype == 483){
					if (pc.getInventory().checkItem(149027, 50) && pc.getInventory().checkItem(40055, 10) && pc.getInventory().checkItem(40394, 5)) {
						pc.getInventory().consumeItem(149027, 50) ;
						pc.getInventory().consumeItem(40055, 10);
						pc.getInventory().consumeItem(40394, 5);
						인첸트지급(pc, 21265, 1, 0);
						return;
					} else if (pc.getInventory().checkItem(149027, 50) && pc.getInventory().checkItem(40055, 10) && pc.getInventory().checkEnchantItem(20194, 7, 1)) {
						pc.getInventory().consumeItem(149027, 50) ;
						pc.getInventory().consumeItem(40055, 10);
						pc.getInventory().consumeEnchantItem(20194, 7, 1);
						인첸트지급(pc, 21265, 1, 0);
						return;
					}
				} else if (itemtype ==484){
					if (pc.getInventory().checkItem(149027, 50) && pc.getInventory().checkItem(40054, 10) && pc.getInventory().checkItem(40395, 5)) {
						pc.getInventory().consumeItem(149027, 50) ;
						pc.getInventory().consumeItem(40054, 10);
						pc.getInventory().consumeItem(40395, 5);
						인첸트지급(pc, 21266, 1, 0);
						return;
					} else if (pc.getInventory().checkItem(149027, 50) && pc.getInventory().checkItem(40054, 10) && pc.getInventory().checkEnchantItem(20194, 7, 1)) {
						pc.getInventory().consumeItem(149027, 50) ;
						pc.getInventory().consumeItem(40054, 10);
						pc.getInventory().consumeEnchantItem(20194, 7, 1);
						인첸트지급(pc, 21266, 1, 0);
						return;
					}
					
				} else if(itemtype == 4934){
					if(pc.getInventory().checkEnchantItem(9114, 7, 2)){
						pc.getInventory().consumeEnchantItem(9114, 7, 1);
						pc.getInventory().consumeEnchantItem(9114, 7, 1);
						인첸트지급(pc, 91140, 1, 7);
						return;
					}
				} else if(itemtype == 4935){
					if(pc.getInventory().checkEnchantItem(9115, 7, 2)){
						pc.getInventory().consumeEnchantItem(9115, 7, 1);
						pc.getInventory().consumeEnchantItem(9115, 7, 1);
						인첸트지급(pc, 91150, 1, 7);
						return;
					}
				} else if(itemtype == 4936){
					if(pc.getInventory().checkEnchantItem(9116, 7, 2)){
						pc.getInventory().consumeEnchantItem(9116, 7, 1);
						pc.getInventory().consumeEnchantItem(9116, 7, 1);
						인첸트지급(pc, 91160, 1, 7);
						return;
					}
				} else if(itemtype == 4937){
					if(pc.getInventory().checkEnchantItem(9117, 7, 2)){
						pc.getInventory().consumeEnchantItem(9117, 7, 1);
						pc.getInventory().consumeEnchantItem(9117, 7, 1);
						인첸트지급(pc, 91170, 1, 7);
						return;
					}
				} else if(itemtype == 4938){
					if(pc.getInventory().checkEnchantItem(9114, 8, 2)){
						pc.getInventory().consumeEnchantItem(9114, 8, 1);
						pc.getInventory().consumeEnchantItem(9114, 8, 1);
						인첸트지급(pc, 91140, 1, 8);
						return;
					}
				} else if(itemtype == 4939){
					if(pc.getInventory().checkEnchantItem(9115, 8, 2)){
						pc.getInventory().consumeEnchantItem(9115, 8, 1);
						pc.getInventory().consumeEnchantItem(9115, 8, 1);
						인첸트지급(pc, 91150, 1, 8);
						return;
					}
				} else if(itemtype == 4940){
					if(pc.getInventory().checkEnchantItem(9116, 8, 2)){
						pc.getInventory().consumeEnchantItem(9116, 8, 1);
						pc.getInventory().consumeEnchantItem(9116, 8, 1);
						인첸트지급(pc, 91160, 1, 8);
						return;
					}
				} else if(itemtype == 4941){
					if(pc.getInventory().checkEnchantItem(9117, 8, 2)){
						pc.getInventory().consumeEnchantItem(9117, 8, 1);
						pc.getInventory().consumeEnchantItem(9117, 8, 1);
						인첸트지급(pc, 91170, 1, 8);
						return;
					}
				} else if(itemtype == 4942){
					if(pc.getInventory().checkEnchantItem(9114, 9, 2)){
						pc.getInventory().consumeEnchantItem(9114, 9, 1);
						pc.getInventory().consumeEnchantItem(9114, 9, 1);
						인첸트지급(pc, 91140, 1, 9);
						return;
					}
				} else if(itemtype == 4943){
					if(pc.getInventory().checkEnchantItem(9115, 9, 2)){
						pc.getInventory().consumeEnchantItem(9115, 9, 1);
						pc.getInventory().consumeEnchantItem(9115, 9, 1);
						인첸트지급(pc, 91150, 1, 9);
						return;
					}
				} else if(itemtype == 4944){
					if(pc.getInventory().checkEnchantItem(9116, 9, 2)){
						pc.getInventory().consumeEnchantItem(9116, 9, 1);
						pc.getInventory().consumeEnchantItem(9116, 9, 1);
						인첸트지급(pc, 91160, 1, 9);
						return;
					}
				} else if(itemtype == 4945){
					if(pc.getInventory().checkEnchantItem(9117, 9, 2)){
						pc.getInventory().consumeEnchantItem(9117, 9, 1);
						pc.getInventory().consumeEnchantItem(9117, 9, 1);
						인첸트지급(pc, 91170, 1, 9);
						return;
					}
				} else if(itemtype == 4946){
					if(pc.getInventory().checkEnchantItem(9114, 10, 2)){
						pc.getInventory().consumeEnchantItem(9114, 10, 1);
						pc.getInventory().consumeEnchantItem(9114, 10, 1);
						인첸트지급(pc, 91140, 1, 10);
						return;
					}
				} else if(itemtype == 4947){
					if(pc.getInventory().checkEnchantItem(9115, 10, 2)){
						pc.getInventory().consumeEnchantItem(9115, 10, 1);
						pc.getInventory().consumeEnchantItem(9115, 10, 1);
						인첸트지급(pc, 91150, 1, 10);
						return;
					}
				} else if(itemtype == 4948){
					if(pc.getInventory().checkEnchantItem(9116, 10, 2)){
						pc.getInventory().consumeEnchantItem(9116, 10, 1);
						pc.getInventory().consumeEnchantItem(9116, 10, 1);
						인첸트지급(pc, 91160, 1, 10);
						return;
					}
				} else if(itemtype == 4949){
					if(pc.getInventory().checkEnchantItem(9117, 10, 2)){
						pc.getInventory().consumeEnchantItem(9117, 10, 1);
						pc.getInventory().consumeEnchantItem(9117, 10, 1);
						인첸트지급(pc, 91170, 1, 10);
						return;
					}
				} 
			} else {
				pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.CRAFT_OK));
				return;
			}
			action = NpcActionTable.getInstance().get(s, pc, npc);
			if (action != null) {
				L1NpcHtml result = action.executeWithAmount(s, pc, npc, itemcount);	
				if (result != null) {
					pc.sendPackets(new S_NPCTalkReturn(npc.getId(), result));
					pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.CRAFT_OK));
					pc.sendPackets(new S_SkillSound(pc.getId(), 7976)); 
					pc.broadcastPacket(new S_SkillSound(pc.getId(), 7976));	
				} else {
					pc.sendPackets(new S_SystemMessage("미구현 상태: 메티스에게 편지로 요청바랍니다."));
				}	
			} 
			break;
		default:
			// System.out.println(type);
			break;
		}
	}

	private boolean 인첸트지급(L1PcInstance pc, int item_id, int count, int EnchantLevel) {
		L1ItemInstance item = ItemTable.getInstance().createItem(item_id);
		if (item != null) {
			item.setCount(count);
			item.setEnchantLevel(EnchantLevel);
			item.setIdentified(true);
			if (pc.getInventory().checkAddItem(item, count) == L1Inventory.OK) {
				pc.getInventory().storeItem(item);
			} else {
				pc.sendPackets(new S_ServerMessage(82));
				// 무게 게이지가 부족하거나 인벤토리가 꽉차서 더 들 수 없습니다.
				return false;
			}
			pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 성공했습니다."));
			pc.sendPackets(new S_ServerMessage(143, item.getLogName())); // %0를
																			// 손에
																			// 넣었습니다.
			pc.sendPackets(new S_SkillSound(pc.getId(), 7976));
			pc.broadcastPacket(new S_SkillSound(pc.getId(), 7976));
			return true;
		} else {
			return false;
		}
	}

	private boolean 속성인첸트지급(L1PcInstance pc, int item_id, int count, int EnchantLevel, int Attr) {
		L1ItemInstance item = ItemTable.getInstance().createItem(item_id);
		if (item != null) {
			item.setCount(count);
			item.setEnchantLevel(EnchantLevel);
			item.setAttrEnchantLevel(Attr);
			item.setIdentified(true);
			if (pc.getInventory().checkAddItem(item, count) == L1Inventory.OK) {
				pc.getInventory().storeItem(item);
			} else {
				pc.sendPackets(new S_ServerMessage(82));
				// 무게 게이지가 부족하거나 인벤토리가 꽉차서 더 들 수 없습니다.
				return false;
			}
			pc.sendPackets(new S_SystemMessage(pc, "아이템 제작에 성공했습니다."));
			pc.sendPackets(new S_ServerMessage(143, item.getLogName())); // %0를
																			// 손에
																			// 넣었습니다.
			pc.sendPackets(new S_SkillSound(pc.getId(), 7976));
			pc.broadcastPacket(new S_SkillSound(pc.getId(), 7976));
			return true;
		} else {
			return false;
		}
	}

	
	@Override
	public String getType() {
		return C_ACTION_UI2;
	}

}
		
		
		
