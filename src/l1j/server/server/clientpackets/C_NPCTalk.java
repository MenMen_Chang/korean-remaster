/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */

package l1j.server.server.clientpackets;

import java.util.logging.Logger;

import l1j.server.server.datatables.NpcActionTable;
import l1j.server.server.model.L1Object;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1NpcInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.model.npc.L1NpcHtml;
import l1j.server.server.model.npc.action.L1NpcAction;
import l1j.server.server.serverpackets.S_NPCTalkReturn;
import l1j.server.server.serverpackets.S_NewUI;
import server.LineageClient;

//Referenced classes of package l1j.server.server.clientpackets:
//ClientBasePacket, C_NPCTalk

public class C_NPCTalk extends ClientBasePacket {

	private static final String C_NPC_TALK = "[C] C_NPCTalk";
	private static Logger _log = Logger.getLogger(C_NPCTalk.class.getName());

	public C_NPCTalk(byte abyte0[], LineageClient client) throws Exception {
		super(abyte0);
		try {
			int objid = readD();
			L1Object obj = L1World.getInstance().findObject(objid);
			L1PcInstance pc = client.getActiveChar();
			if (obj != null && pc != null) {
				if (obj instanceof L1NpcInstance)
					pc.talkingNpcObjid = objid;
				L1NpcAction action = NpcActionTable.getInstance().get(pc, obj);
				if (action != null) {
					L1NpcHtml html = action.execute("", pc, obj, new byte[0]);
					if (html != null) {
						S_NPCTalkReturn snt = new S_NPCTalkReturn(obj.getId(), html);
						pc.sendPackets(snt, true);
					}
					return;
				}
				if (obj instanceof L1NpcInstance) {
					L1NpcInstance npc = (L1NpcInstance) obj;
					if (npc.getNpcTemplate().get_npcId() == 50036
						|| npc.getNpcTemplate().get_npcId() == 50020
						|| npc.getNpcTemplate().get_npcId() == 50024
						|| npc.getNpcTemplate().get_npcId() == 50054
						|| npc.getNpcTemplate().get_npcId() == 50066
						|| npc.getNpcTemplate().get_npcId() == 50039
						|| npc.getNpcTemplate().get_npcId() == 50051
						|| npc.getNpcTemplate().get_npcId() == 50044
						|| npc.getNpcTemplate().get_npcId() == 50046
						|| npc.getNpcTemplate().get_npcId() == 50068
						|| npc.getNpcTemplate().get_npcId() == 4919000
						|| npc.getNpcTemplate().get_npcId() == 4918000
						|| npc.getNpcTemplate().get_npcId() == 100287
						|| npc.getNpcTemplate().get_npcId() == 100573
						|| npc.getNpcTemplate().get_npcId() == 50015) {
						pc.sendPackets(new S_NewUI(S_NewUI.텔녀이동패킷), true);
					}
				}
				obj.onTalkAction(pc);
			} else {
				// _log.severe("오브젝트가 발견되지 않습니다 시도 캐릭터:"+pc==null ? "PC없음":
				// (pc.getName()+" x:"+pc.getX()+" y:"+pc.getY()+" map:"+pc.getMapId())+" objid="
				// + objid);
			}
		} catch (Exception e) {

		} finally {
			clear();
		}
	}

	@Override
	public String getType() {
		return C_NPC_TALK;
	}
}