/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package l1j.server.server.clientpackets;

import static l1j.server.server.model.skill.L1SkillId.ADVANCE_SPIRIT;
import static l1j.server.server.model.skill.L1SkillId.AQUA_PROTECTER;
import static l1j.server.server.model.skill.L1SkillId.LIFE_MAAN;
import static l1j.server.server.model.skill.L1SkillId.SHINING_ARMOR;
import static l1j.server.server.model.skill.L1SkillId.DRAGON_SKIN;
import static l1j.server.server.model.skill.L1SkillId.ARMOR_BREAK;
import static l1j.server.server.model.skill.L1SkillId.BLESS_WEAPON;
import static l1j.server.server.model.skill.L1SkillId.BONE_BREAK;
import static l1j.server.server.model.skill.L1SkillId.CONCENTRATION;
import static l1j.server.server.model.skill.L1SkillId.COUNTER_MAGIC;
import static l1j.server.server.model.skill.L1SkillId.CURSE_PARALYZE;
import static l1j.server.server.model.skill.L1SkillId.CURSE_PARALYZE2;
import static l1j.server.server.model.skill.L1SkillId.DECAY_POTION;
import static l1j.server.server.model.skill.L1SkillId.DRAGON_PUPLE;
import static l1j.server.server.model.skill.L1SkillId.DRAGON_TOPAZ;
import static l1j.server.server.model.skill.L1SkillId.DRESS_DEXTERITY;
import static l1j.server.server.model.skill.L1SkillId.DRESS_MIGHTY;
import static l1j.server.server.model.skill.L1SkillId.EARTH_BIND;
import static l1j.server.server.model.skill.L1SkillId.ERASE_MAGIC;
import static l1j.server.server.model.skill.L1SkillId.EXP_POTION;
import static l1j.server.server.model.skill.L1SkillId.EXP_POTION2;
import static l1j.server.server.model.skill.L1SkillId.EXP_POTION3;
import static l1j.server.server.model.skill.L1SkillId.EXP_POTION_cash;
import static l1j.server.server.model.skill.L1SkillId.FOG_OF_SLEEPING;
import static l1j.server.server.model.skill.L1SkillId.FREEZING_BREATH;
import static l1j.server.server.model.skill.L1SkillId.ICE_LANCE;
import static l1j.server.server.model.skill.L1SkillId.INSIGHT;
import static l1j.server.server.model.skill.L1SkillId.IRON_SKIN;
import static l1j.server.server.model.skill.L1SkillId.MOB_BASILL;
import static l1j.server.server.model.skill.L1SkillId.MOB_COCA;
import static l1j.server.server.model.skill.L1SkillId.MOB_RANGESTUN_18;
import static l1j.server.server.model.skill.L1SkillId.MOB_RANGESTUN_19;
import static l1j.server.server.model.skill.L1SkillId.MOB_RANGESTUN_30;
import static l1j.server.server.model.skill.L1SkillId.MOB_SHOCKSTUN_30;
import static l1j.server.server.model.skill.L1SkillId.PATIENCE;
import static l1j.server.server.model.skill.L1SkillId.PHANTASM;
import static l1j.server.server.model.skill.L1SkillId.PHYSICAL_ENCHANT_DEX;
import static l1j.server.server.model.skill.L1SkillId.PHYSICAL_ENCHANT_STR;
import static l1j.server.server.model.skill.L1SkillId.POLLUTE_WATER;
import static l1j.server.server.model.skill.L1SkillId.SHINING_AURA;
import static l1j.server.server.model.skill.L1SkillId.SHOCK_STUN; //버프물약
import static l1j.server.server.model.skill.L1SkillId.STATUS_CASHSCROLL;
import static l1j.server.server.model.skill.L1SkillId.STATUS_CASHSCROLL2;
import static l1j.server.server.model.skill.L1SkillId.STATUS_CASHSCROLL3;
import static l1j.server.server.model.skill.L1SkillId.STATUS_COMA_5;
import static l1j.server.server.model.skill.L1SkillId.STATUS_HOLY_MITHRIL_POWDER;
import static l1j.server.server.model.skill.L1SkillId.STATUS_HOLY_WATER;
import static l1j.server.server.model.skill.L1SkillId.STATUS_HOLY_WATER_OF_EVA;
import static l1j.server.server.model.skill.L1SkillId.STATUS_덱업6;
import static l1j.server.server.model.skill.L1SkillId.STATUS_덱업7;
import static l1j.server.server.model.skill.L1SkillId.STATUS_시원한얼음조각;
import static l1j.server.server.model.skill.L1SkillId.STATUS_힘업6;
import static l1j.server.server.model.skill.L1SkillId.STATUS_힘업7;
import static l1j.server.server.model.skill.L1SkillId.TRUE_TARGET;
import static l1j.server.server.model.skill.L1SkillId.Tam_Fruit1;
import static l1j.server.server.model.skill.L1SkillId.Tam_Fruit2;
import static l1j.server.server.model.skill.L1SkillId.Tam_Fruit3;
import static l1j.server.server.model.skill.L1SkillId.Tam_Fruit4;
import static l1j.server.server.model.skill.L1SkillId.Tam_Fruit5;
import static l1j.server.server.model.skill.L1SkillId.아쿠아샷;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import javolution.util.FastMap;
import l1j.server.Config;
import l1j.server.L1DatabaseFactory;
import l1j.server.GameSystem.Antaras.AntarasRaidSystem;
import l1j.server.GameSystem.Delivery.Delivery;
import l1j.server.GameSystem.Delivery.DeliverySystem;
import l1j.server.GameSystem.Lind.LindRaid;
import l1j.server.GameSystem.Papoo.PaPooRaidSystem;
import l1j.server.GameSystem.valakas.ValaRaidSystem;
import l1j.server.server.ActionCodes;
import l1j.server.server.GeneralThreadPool;
import l1j.server.server.Goras1Controller;
import l1j.server.server.Jibae11;
import l1j.server.server.Opcodes;
import l1j.server.server.TimeController.FishingTimeController;
import l1j.server.server.datatables.ExpTable;
import l1j.server.server.datatables.ForceItem;
import l1j.server.server.datatables.ItemTable;
import l1j.server.server.datatables.LogTable;
import l1j.server.server.datatables.MapFixKeyTable;
import l1j.server.server.datatables.NpcTable;
import l1j.server.server.datatables.PetTable;
import l1j.server.server.datatables.ReviveItemTable;
import l1j.server.server.datatables.SkillsTable;
import l1j.server.server.datatables.ReviveItemTable.L1ReviveItem;
import l1j.server.server.model.Beginner;
import l1j.server.server.model.Broadcaster;
import l1j.server.server.model.CharPosUtil;
import l1j.server.server.model.L1CastleLocation;
import l1j.server.server.model.L1Character;
import l1j.server.server.model.L1Clan;
import l1j.server.server.model.L1EffectSpawn;
import l1j.server.server.model.L1Inventory;
import l1j.server.server.model.L1ItemDelay;
import l1j.server.server.model.L1Location;
import l1j.server.server.model.L1Object;
import l1j.server.server.model.L1PcInventory;
import l1j.server.server.model.L1PolyMorph;
import l1j.server.server.model.L1Quest;
import l1j.server.server.model.L1Teleport;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1BoardInstance;
import l1j.server.server.model.Instance.L1DollInstance;
import l1j.server.server.model.Instance.L1DoorInstance;
import l1j.server.server.model.Instance.L1EffectInstance;
import l1j.server.server.model.Instance.L1GuardianInstance;
import l1j.server.server.model.Instance.L1ItemInstance;
import l1j.server.server.model.Instance.L1MonsterInstance;
import l1j.server.server.model.Instance.L1NpcInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.model.Instance.L1PetInstance;
import l1j.server.server.model.Instance.L1SummonInstance;
import l1j.server.server.model.item.L1ItemId;
import l1j.server.server.model.item.function.ItemUtil;
import l1j.server.server.model.item.function.PassiveBook;
import l1j.server.server.model.item.function.PetSummons;
import l1j.server.server.model.item.function.TeleportScroll;
import l1j.server.server.model.map.L1Map;
import l1j.server.server.model.map.L1WorldMap;
import l1j.server.server.model.poison.L1DamagePoison;
import l1j.server.server.model.skill.L1SkillDelay;
import l1j.server.server.model.skill.L1SkillId;
import l1j.server.server.model.skill.L1SkillUse;
import l1j.server.server.serverpackets.S_ACTION_UI;
import l1j.server.server.serverpackets.S_AddItem;
import l1j.server.server.serverpackets.S_AddSkill;
import l1j.server.server.serverpackets.S_AttackPacket;
import l1j.server.server.serverpackets.S_AttenDance;
import l1j.server.server.serverpackets.S_ChangeName;
import l1j.server.server.serverpackets.S_ChangeShape;
import l1j.server.server.serverpackets.S_CharVisualUpdate;
import l1j.server.server.serverpackets.S_ChatPacket;
import l1j.server.server.serverpackets.S_DRAGONPERL;
import l1j.server.server.serverpackets.S_DeleteInventoryItem;
import l1j.server.server.serverpackets.S_Dexup;
import l1j.server.server.serverpackets.S_DoActionGFX;
import l1j.server.server.serverpackets.S_EffectLocation;
import l1j.server.server.serverpackets.S_HPUpdate;
import l1j.server.server.serverpackets.S_ItemName;
import l1j.server.server.serverpackets.S_Liquor;
import l1j.server.server.serverpackets.S_MPUpdate;
import l1j.server.server.serverpackets.S_Message_YN;
import l1j.server.server.serverpackets.S_NPCTalkReturn;
import l1j.server.server.serverpackets.S_NewCreateItem;
import l1j.server.server.serverpackets.S_NewUI;
import l1j.server.server.serverpackets.S_OwnCharAttrDef;
import l1j.server.server.serverpackets.S_OwnCharStatus;
import l1j.server.server.serverpackets.S_OwnCharStatus2;
import l1j.server.server.serverpackets.S_PacketBox;
import l1j.server.server.serverpackets.S_Paralysis;
import l1j.server.server.serverpackets.S_Ranking;
import l1j.server.server.serverpackets.S_ReturnedStat;
import l1j.server.server.serverpackets.S_SPMR;
import l1j.server.server.serverpackets.S_SabuTell;
import l1j.server.server.serverpackets.S_Serchdrop2;
import l1j.server.server.serverpackets.S_ServerMessage;
import l1j.server.server.serverpackets.S_SkillIconAura;
import l1j.server.server.serverpackets.S_SkillSound;
import l1j.server.server.serverpackets.S_Sound;
import l1j.server.server.serverpackets.S_Strup;
import l1j.server.server.serverpackets.S_SystemMessage;
import l1j.server.server.serverpackets.S_UseMap;
import l1j.server.server.serverpackets.S_UserCommands;//서버정보 편지지
import l1j.server.server.serverpackets.S_UserCommands1;//무인pc티켓 설명
import l1j.server.server.serverpackets.S_UserCommands2;//캐릭터교환증표설명
import l1j.server.server.serverpackets.S_UserStatus;
import l1j.server.server.storage.CharactersItemStorage;
import l1j.server.server.templates.L1BookMark;
import l1j.server.server.templates.L1EtcItem;
import l1j.server.server.templates.L1Item;
import l1j.server.server.templates.L1Npc;
import l1j.server.server.templates.L1Pet;
import l1j.server.server.templates.L1Skills;
import l1j.server.server.utils.L1SpawnUtil;
import l1j.server.server.utils.SQLUtil;
import server.LineageClient;
import server.manager.eva;

//Referenced classes of package l1j.server.server.clientpackets:
//ClientBasePacket

public class C_ItemUSe extends ClientBasePacket {

	private static final String C_ITEM_USE = "[C] C_ItemUSe";
	private static Logger _log = Logger.getLogger(C_ItemUSe.class.getName());

	private static Random _random = new Random(System.nanoTime());

	Calendar currentDate = Calendar.getInstance();
	// SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd h:mm:ss a");
	// String time = dateFormat.format(currentDate.getTime());
	private static final int[] MALE_LIST = new int[] { 0, 61, 138, 734, 2786,
			6658, 6671, 12490 };
	private static final int[] FEMALE_LIST = new int[] { 1, 48, 37, 1186, 2796,
			6661, 6650, 12494 };


	public C_ItemUSe(byte abyte0[], LineageClient client) throws Exception {
		super(abyte0);
		try {
			int itemObjid = readD();
			L1PcInstance pc = client.getActiveChar();

			if (pc == null || pc.isGhost()) {
				return;
			}
			L1ItemInstance useItem = pc.getInventory().getItem(itemObjid);
			if (itemObjid == 0 || useItem == null || pc.isDead() == true) {
				return;
			}
			if (useItem.getItemId() == 20383 && useItem.getRemainingTime() <= 0) {// 기마용 투구
				pc.sendPackets(new S_ServerMessage(1195), true); // 사용 시간이 다 되었습니다.
				return;
			}
			if (pc.getMapId() == 5166) {
				return;
			}
			if (pc.isPrivateShop())
				return;
			
			
			if (useItem.getItem().getType2() == 0 && useItem.getItem().getType() == 17) {
				PassiveBook.getIn().toUse(pc, useItem, this);
				return;
			}
			
			if (useItem.getItem().getUseType() == -1) { // none:사용할 수 없는 아이템
				pc.sendPackets(new S_ServerMessage(74, useItem.getLogName()),
						true); // \f1%0은 사용할 수 없습니다.
				return;
			}
			/** 봉인된 룬, 유물 사용 불가능하게 by 케인 **/
			if (useItem.getItemId() >= 427123 && useItem.getItemId() <= 427140) {
				pc.sendPackets(new S_ServerMessage(74, useItem.getLogName()), true); // \f1%0은 사용할 수 없습니다.
				return;
			}
			if (pc.isTeleport()) { // 텔레포트 처리중
				if (useItem instanceof TeleportScroll) {
					pc.sendPackets(new S_Paralysis(
							S_Paralysis.TYPE_TELEPORT_UNLOCK, false), true);
				}
				return;
			}
			if (useItem.getItem().getType2() == 0) {
				if (useItem.getItem().getType() != 6
						&& useItem.getItem().getType() != 7
						&& useItem.getItem().getType() != 8
						&& useItem.getItem().getItemId() != 41159
						&& useItem.getItem().getItemId() != 60203
								&& useItem.getItem().getItemId() != 39100
										&& useItem.getItem().getItemId() != 391026
						&& useItem.getItem().getItemId() != 5588) {
					if (pc.getInventory().calcWeightpercent() >= 90) {
						pc.sendPackets(new S_SystemMessage("아이템 사용 실패: 무게 게이지 90% 이상 사용 불가."));
						// pc.sendPackets(new S_ServerMessage(3561)); // 무게 게이지가
						// 가득찼습니다.
						return;
					}
					if (pc.getInventory().getSize() >= 175) {
						pc.sendPackets(new S_SystemMessage("아이템 사용 실패: 인벤토리의 갯수를 줄인후 사용해주세요."));
						return;
					}

				}
			}

			// 존재버그 관련 추가
			L1PcInstance jonje = L1World.getInstance().getPlayer(pc.getName());
			if (jonje == null && pc.getAccessLevel() != 200) {
				pc.sendPackets(new S_SystemMessage("존재버그 강제종료! 재접속하세요"), true);
				client.kick();
				return;
			}
			/** 제어 스킬 중에 임의로 아이템 사용 못하게 **/
			if (pc.getSkillEffectTimerSet().hasSkillEffect(SHOCK_STUN)
					|| pc.getSkillEffectTimerSet().hasSkillEffect(MOB_SHOCKSTUN_30)
					|| pc.getSkillEffectTimerSet().hasSkillEffect(MOB_RANGESTUN_19)
					|| pc.getSkillEffectTimerSet().hasSkillEffect(MOB_RANGESTUN_18)
					|| pc.getSkillEffectTimerSet().hasSkillEffect(MOB_RANGESTUN_30)
					|| pc.getSkillEffectTimerSet().hasSkillEffect(EARTH_BIND)
					|| pc.getSkillEffectTimerSet().hasSkillEffect(MOB_COCA)
					|| pc.getSkillEffectTimerSet().hasSkillEffect(MOB_BASILL)
					|| pc.getSkillEffectTimerSet().hasSkillEffect(ICE_LANCE)
					|| pc.getSkillEffectTimerSet().hasSkillEffect(FREEZING_BREATH)
					|| pc.getSkillEffectTimerSet().hasSkillEffect(BONE_BREAK)
					|| pc.getSkillEffectTimerSet().hasSkillEffect(PHANTASM)
					|| pc.getSkillEffectTimerSet().hasSkillEffect(FOG_OF_SLEEPING)
					|| pc.getSkillEffectTimerSet().hasSkillEffect(CURSE_PARALYZE)
					|| pc.getSkillEffectTimerSet().hasSkillEffect(CURSE_PARALYZE2)) {
				return;
			}

			if (!pc.getMap().isUsableItem() && !pc.isGm()) {
				pc.sendPackets(new S_SystemMessage("여기에서는 사용할 수 없습니다."), true); // \f1 여기에서는 사용할 수 없습니다.
				if (useItem instanceof TeleportScroll) {
					pc.sendPackets(new S_Paralysis(S_Paralysis.TYPE_TELEPORT_UNLOCK, false), true);
				}
				return;
			}
			int itemId;
			try {
				itemId = useItem.getItem().getItemId();
			} catch (Exception e) {
				return;
			}

			if (pc.getSkillEffectTimerSet().hasSkillEffect(
					L1SkillId.STATUS_안전모드)) {
				pc.getSkillEffectTimerSet().killSkillEffectTimer(
						L1SkillId.STATUS_안전모드);
			}
			
			if (useItem.getItem().getType() == 21) {
				ItemUtil.getIn().toUse(pc, useItem, this);
				return;
			}
			
			if (useItem.isWorking()) {
				if (pc.getCurrentHp() > 0) {
					if (useItem.getItem().getType2() == 0) { // 종별：그 외의 아이템
						int item_minlvl = ((L1EtcItem) useItem.getItem())
								.getMinLevel();
						int item_maxlvl = ((L1EtcItem) useItem.getItem())
								.getMaxLevel();

						if (item_minlvl != 0 && item_minlvl > pc.getLevel()
								&& !pc.isGm()) {
							// pc.sendPackets(new S_ServerMessage(318,
							// String.valueOf(item_minlvl)), true);
							pc.sendPackets(new S_SystemMessage("이 아이템은 "
									+ item_minlvl + "레벨 이상이 되어야 사용할 수 있습니다."),
									true);
							// 이 아이템은%0레벨 이상이 되지 않으면 사용할 수 없습니다.
							if (useItem instanceof TeleportScroll) {
								pc.sendPackets(
										new S_Paralysis(
												S_Paralysis.TYPE_TELEPORT_UNLOCK,
												false), true);
							}
							return;
						} else if (item_maxlvl != 0
								&& item_maxlvl < pc.getLevel() && !pc.isGm()) {
							pc.sendPackets(new S_SystemMessage("이 아이템은 "
									+ item_maxlvl + "레벨 이하일 때만 사용할 수 있습니다."),
									true);
							// pc.sendPackets(new S_ServerMessage(673,
							// /*String.valueOf(item_maxlvl)*/), true);
							// 이 아이템은%d레벨 이상만 사용할 수 있습니다.
							if (useItem instanceof TeleportScroll) {
								pc.sendPackets(
										new S_Paralysis(
												S_Paralysis.TYPE_TELEPORT_UNLOCK,
												false), true);
							}
							return;
						}
					}
					useItem.clickItem(pc, this);
				}
				return;
			}

			int l = 0;
			int spellsc_objid = 0;
			int use_objid = 0;
			int spellsc_x = 0;
			int spellsc_y = 0;

			int use_type = useItem.getItem().getUseType();

			if (itemId >= 40859 && itemId <= 40898) {
				switch (itemId) {
				
				case 40866:
				case 40867:
				case 40877:
				case 40884:
				case 40895:// 버프
				case 40870:
				case 40879:
					spellsc_objid = readD();
					break;
				case 40871:
				case 40872:
				case 40890:
				case 40863:
				case 40889:
				case 40861:
				case 40859:
				case 40893:
				case 40860:// 즉시실행
					break;
				case 40896:
				case 40894:
				case 40891:
				case 40892:
				case 40864:
				case 40898:
				case 40888:
				case 40887:
				case 40865:
				case 40869:
				case 40873:
				case 40874:
				case 40875:
				case 40876:
				case 40878:
				case 40880:
				case 40881:
				case 40883:
				case 40885:
				case 40862:
				case 40897:
				case 40868:
				case 40886:// 지정
					spellsc_objid = readD();
					spellsc_x = readH();
					spellsc_y = readH();
				default:
					break;
				}
			} else if (itemId == 41029 // 소환공의 조각
					|| itemId == 600233
					|| itemId == 40317
					|| itemId == 60155
					|| itemId == 41036
					|| itemId == L1ItemId.LOWER_OSIRIS_PRESENT_PIECE_DOWN
					|| itemId == 7322
					|| itemId == 7240
					|| itemId == L1ItemId.HIGHER_OSIRIS_PRESENT_PIECE_DOWN
					|| itemId == L1ItemId.LOWER_TIKAL_PRESENT_PIECE_DOWN
					|| itemId == L1ItemId.HIGHER_TIKAL_PRESENT_PIECE_DOWN
					|| itemId == L1ItemId.TIMECRACK_CORE
					|| itemId == 55220 || itemId == 55221
					|| itemId == 141917
					|| itemId == 40964
					|| itemId == 41030
					|| itemId == 40925
					|| itemId == 40926
					|| itemId == 5589
					|| itemId == 40927 // 정화·신비적인 일부
					|| itemId == 40928 || itemId == 40929
					|| itemId == 500231
					|| itemId == 60025 || itemId == 60026
					|| itemId == 60104
					|| itemId == 6351
					|| itemId == 6352
					|| itemId == 60197 || itemId == 5900
					|| (itemId >= 60218 && itemId <= 60232)
					|| (itemId >= 60235 && itemId <= 60246)
					|| itemId == 60247
					|| (itemId >= 60273 && itemId <= 60283)
					|| itemId == 60333
					|| itemId == 7702
					|| itemId == 73323
					|| itemId == 57200
					|| itemId == 1999
					|| itemId == 2999
					|| itemId == 8275
					|| itemId == 8276
					|| itemId == 600228 || itemId == 60476
					|| itemId == 60477 || itemId == 6673  || itemId == 16673
					|| itemId == 60383 || itemId == 9095) {
				l = readD();
			} else if (use_type == 30 || itemId == 40870 || itemId == 40879) { // spell_buff
				spellsc_objid = readD();
			} else if (itemId == 5559 || itemId == 5560) {
				use_objid = readD();

			} else if (use_type == 5 || use_type == 17) { // spell_long
															// spell_short
				spellsc_objid = readD();
				spellsc_x = readH();
				spellsc_y = readH();
			} else {
				l = readC();
			}

			if (pc.getCurrentHp() > 0) {
				int delay_id = 0;
				if (useItem.getItem().getType2() == 0) { // 종별：그 외의 아이템
					delay_id = ((L1EtcItem) useItem.getItem()).get_delayid();
				}
				if (delay_id != 0) { // 지연 설정 있어
					if (pc.hasItemDelay(delay_id) == true) {
						// System.out.println("딜레이");
						return;
					}
				}

				L1ItemInstance l1iteminstance1 = pc.getInventory().getItem(l);
				_log.finest("request item use (obj) = " + itemObjid
						+ " action = " + l);
				if (useItem.getItem().getType2() == 0) { // 종별：그 외의 아이템
					int item_minlvl = ((L1EtcItem) useItem.getItem())
							.getMinLevel();
					int item_maxlvl = ((L1EtcItem) useItem.getItem())
							.getMaxLevel();

					if (item_minlvl != 0 && item_minlvl > pc.getLevel()
							&& !pc.isGm()) {
						pc.sendPackets(new S_SystemMessage("이 아이템은 "
								+ item_minlvl + "레벨 이상이 되어야 사용할 수 있습니다."), true);
						// pc.sendPackets(new S_ServerMessage(318,
						// String.valueOf(item_minlvl)), true);
						// 이 아이템은%0레벨 이상이 되지 않으면 사용할 수 없습니다.
						return;
					} else if (item_maxlvl != 0 && item_maxlvl < pc.getLevel()
							&& !pc.isGm()) {
						pc.sendPackets(new S_SystemMessage("이 아이템은 "
								+ item_maxlvl + "레벨 이하일 때만 사용할 수 있습니다."), true);
						// pc.sendPackets(new S_ServerMessage(673,
						// String.valueOf(item_maxlvl)), true);
						// 이 아이템은%d레벨 이상만 사용할 수 있습니다.
						return;
					}
					if ((itemId == 40576 && !pc.isElf())
							|| (itemId == 40577 && !pc.isWizard()) // 영혼의 결정의
																	// 파편(흑)
							|| (itemId == 40578 && !pc.isKnight())) { // 영혼의 결정의
																		// 파편(빨강)
						pc.sendPackets(new S_SystemMessage(
								"당신의 클래스는 이 아이템을 사용할 수 없습니다."), true); // \f1당신의
																		// 클래스에서는
																		// 이
																		// 아이템은
																		// 사용할 수
																		// 없습니다.
						return;
					}

					if (itemId == 40003) {
						for (L1ItemInstance lightItem : pc.getInventory().getItems()) {
							if (lightItem.getItem().getItemId() == 40002) {
								lightItem.setRemainingTime(useItem.getItem().getLightFuel());
								pc.sendPackets(new S_ItemName(lightItem), true);
								pc.sendPackets(new S_ServerMessage(230), true);
								break;
							}
						}
						pc.getInventory().removeItem(useItem, 1);
					} 
					/*
					else if (useItem.getItem().getType2() == 0 && useItem.getItem().getType() == 21) {
						ItemUtil.getIn().toUse(pc, useItem, this);
					}
					*/
					else if (itemId == 600258) { // 정상의 가호
						정상의가호(pc, useItem);
					} else if (itemId == 60517) { // 벚꽃 상자
						pc.getInventory().removeItem(useItem, 1);
						벚꽃상자(pc);
					} else if (itemId == 9057) { // 그렘린의선물상자
						pc.getInventory().removeItem(useItem, 1);
						그렘린의선물상자(pc);
					} else if (itemId == 40722) { // 금호박
						pc.getInventory().removeItem(useItem, 1);
						금호박(pc);
					} else if (itemId == 140722) { // 바루의 선물 상자
						pc.getInventory().removeItem(useItem, 1);
						바루의선물상자(pc);
					} else if (itemId == 60518) { // 랜덤 버프 1회 코인
						pc.getInventory().removeItem(useItem, 1);
						랜덤버프코인(pc);
					} else if (itemId == 60516) { // 탐 포인트 상자
						pc.getInventory().removeItem(useItem, 1);
						탐포인트상자(pc);
					} else if (itemId == 6013) {// 폭풍막대
						폭풍막대(pc, itemId, useItem);
					} else if (itemId == 60512 || itemId == 60513) {// 마법막대 파톰, 정화
						마법막대_파톰_정화(pc, itemId, useItem);
					} else if (itemId == 6673 || itemId == 16673) {
						int chance = _random.nextInt(100)+1;
						int succChance = Config.FI_WEAPON;
						if(itemId == 16673){
							succChance *= 2;
						}
						if(l1iteminstance1.getItem().getItemId() != 6682 && l1iteminstance1.getItem().getItemId() != 6683
							&& l1iteminstance1.getItem().getItemId() != 6684 && l1iteminstance1.getItem().getItemId() != 6685
							&& l1iteminstance1.getItem().getItemId() != 6686 && l1iteminstance1.getItem().getItemId() != 6687
							&& l1iteminstance1.getItem().getItemId() != 6688 && l1iteminstance1.getItem().getItemId() != 6689){
							pc.sendPackets(new S_SystemMessage("해당무기에는 사용할 수 없습니다."), true);
							return;
						}
						if(succChance >= chance){
							if(l1iteminstance1.getItem().getItemId() == 6682){
								pc.getInventory().storeItem(9100, 1);	
								pc.getInventory().removeItem(l1iteminstance1, 1);
								pc.getInventory().removeItem(useItem, 1);
								pc.sendPackets(new S_SystemMessage("제작 성공: 가이아의 격노 획득"), true);
								L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE,"누군가가 "+l1iteminstance1.getName()+"에 영혼을 부여 하였습니다."), true);
							}
							if(l1iteminstance1.getItem().getItemId() == 6683){
								pc.getInventory().storeItem(9101, 1);	
								pc.getInventory().removeItem(l1iteminstance1, 1);
								pc.getInventory().removeItem(useItem, 1);
								pc.sendPackets(new S_SystemMessage("제작 성공: 크로노스의 공포 획득"), true);
								L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE,"누군가가 "+l1iteminstance1.getName()+"에 영혼을 부여 하였습니다."), true);
							}
							if(l1iteminstance1.getItem().getItemId() == 6684){
								pc.getInventory().storeItem(9102, 1);	
								pc.getInventory().removeItem(l1iteminstance1, 1);
								pc.getInventory().removeItem(useItem, 1);
								pc.sendPackets(new S_SystemMessage("제작 성공: 히페리온의 절망 획득"), true);
								L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE,"누군가가 "+l1iteminstance1.getName()+"에 영혼을 부여 하였습니다."), true);
							}
							if(l1iteminstance1.getItem().getItemId() == 6685){
								pc.getInventory().storeItem(9103, 1);	
								pc.getInventory().removeItem(l1iteminstance1, 1);
								pc.getInventory().removeItem(useItem, 1);
								pc.sendPackets(new S_SystemMessage("제작 성공: 타이탄의 분노 획득"), true);
								L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE,"누군가가 "+l1iteminstance1.getName()+"에 영혼을 부여 하였습니다."), true);
							}
							if(l1iteminstance1.getItem().getItemId() == 6686){
								pc.getInventory().storeItem(61, 1);	
								pc.getInventory().removeItem(l1iteminstance1, 1);
								pc.getInventory().removeItem(useItem, 1);
								pc.sendPackets(new S_SystemMessage("제작 성공: 진명황의 집행검 획득"), true);
								L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE,"누군가가 "+l1iteminstance1.getName()+"에 영혼을 부여 하였습니다."), true);
							}
							if(l1iteminstance1.getItem().getItemId() == 6687){
								pc.getInventory().storeItem(12, 1);	
								pc.getInventory().removeItem(l1iteminstance1, 1);
								pc.getInventory().removeItem(useItem, 1);
								pc.sendPackets(new S_SystemMessage("제작 성공: 바람 칼날의 단검 획득"), true);
								L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE,"누군가가 "+l1iteminstance1.getName()+"에 영혼을 부여 하였습니다."), true);
							}
							if(l1iteminstance1.getItem().getItemId() == 6688){
								pc.getInventory().storeItem(134, 1);	
								pc.getInventory().removeItem(l1iteminstance1, 1);
								pc.getInventory().removeItem(useItem, 1);
								pc.sendPackets(new S_SystemMessage("제작 성공: 수정 결정체의 지팡이 획득"), true);
								L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE,"누군가가 "+l1iteminstance1.getName()+"에 영혼을 부여 하였습니다."), true);
							}
							if(l1iteminstance1.getItem().getItemId() == 6689){
								pc.getInventory().storeItem(86, 1);	
								pc.getInventory().removeItem(l1iteminstance1, 1);
								pc.getInventory().removeItem(useItem, 1);
								pc.sendPackets(new S_SystemMessage("제작 성공: 붉은 그림자의 이도류 획득"), true);
								L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE,"누군가가 "+l1iteminstance1.getName()+"에 영혼을 부여 하였습니다."), true);
							}
						} else {
							pc.getInventory().removeItem(l1iteminstance1, 1);
							pc.getInventory().removeItem(useItem, 1);
							pc.sendPackets(new S_SystemMessage("제작 실패: 제작 재료 소멸"), true);
							return;
						}
					} else if (itemId == 73009) { // 대법관의 상자
						L1ItemInstance item = pc.getInventory().storeItem(8200, 1); // 기운 사념 조각
						pc.sendPackets(new S_ServerMessage(403, item.getName()), true);
						int chance = _random.nextInt(100)+1;
						if(20 > chance){
						L1ItemInstance item2 = pc.getInventory().storeItem(8201, 1); // 기운 비법서
						pc.sendPackets(new S_ServerMessage(403, item2.getName()), true);
						}
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 39200) {
						pc.sendPackets(new S_SystemMessage(pc, "2018년 1월 1일 개봉가능, +1~+9 무기 아이템 획득"), true);
					} else if (itemId == 39201) {
						pc.sendPackets(new S_SystemMessage(pc, "2018년 1월 1일 개봉가능, +1~+8 방어구 아이템 획득"), true);
					} else if (itemId == 39202) {
						pc.sendPackets(new S_SystemMessage(pc, "2018년 1월 1일 개봉가능, +1~+7 장신구 아이템 획득"), true);
					} else if (itemId == 57200) {
						L1ReviveItem reviveItem = ReviveItemTable.getInstance().getReviveItem(l1iteminstance1.getItem().getItemId());
						if (reviveItem != null) {
							소생주문서(pc, reviveItem, useItem, l1iteminstance1);
						} else {
							pc.sendPackets(new S_ServerMessage(79));
						}
					} else if (itemId == 1999) {
						int percent = ForceItem.getInstance().getSuccPercent(l1iteminstance1.getItem().getItemId());
						int SuccItem = ForceItem.getInstance().getSuccItem(l1iteminstance1.getItem().getItemId());
						int chance = _random.nextInt(100)+1;
						if(pc.기운축복){
							percent = 100;
							pc.기운축복 = false;
						}
						if(l1iteminstance1.getItem().getItemId() >= 8100 && l1iteminstance1.getItem().getItemId() <= 8217){
							if(percent > chance){
							L1ItemInstance SuccItem2 = pc.getInventory().storeItem(SuccItem, 1);
							pc.getInventory().removeItem(l1iteminstance1, 1);
							pc.getInventory().removeItem(useItem, 1);
							pc.sendPackets(new S_SystemMessage(""+SuccItem2.getName()+"획득"), true);
							pc.sendPackets(new S_SystemMessage(""+SuccItem2.getName()+"은(는) 새 생명이 부여되었습니다."), true);
							L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE,"누군가가 "+l1iteminstance1.getName()+"에 새 생명을 부여 하였습니다."), true);
							eva.LogEnchantAppend("성공[기운템]: ", pc.getName(), Integer.toString(l1iteminstance1.getEnchantLevel()), l1iteminstance1.getName(), l1iteminstance1.getId());
							} else {
								pc.getInventory().removeItem(l1iteminstance1, 1);
								pc.getInventory().removeItem(useItem, 1);
								pc.sendPackets(new S_SystemMessage(""+l1iteminstance1.getName()+"은(는) 기운을 흡수하지 못하고 소멸하였습니다."), true);
							}
						} else {
							pc.sendPackets(new S_SystemMessage("해당 아이템에는 사용할 수 없습니다."), true);
						}
					} else if (itemId == 2999) { // 축 나뭇잎
						int percent = ForceItem.getInstance().getSuccPercent(l1iteminstance1.getItem().getItemId());
						int SuccItem = ForceItem.getInstance().getSuccItem(l1iteminstance1.getItem().getItemId());
						int chance = _random.nextInt(100)+1;
						int chance2 = _random.nextInt(100)+1;
						if(pc.기운축복){
							percent = 100;
							pc.기운축복 = false;
						}
						if(l1iteminstance1.getItem().getItemId() >= 8100 && l1iteminstance1.getItem().getItemId() <= 8217){
							if(percent > chance){
							L1ItemInstance SuccItem2 = pc.getInventory().storeItem(SuccItem, 1);
							pc.getInventory().removeItem(l1iteminstance1, 1);
							pc.getInventory().removeItem(useItem, 1);
							pc.sendPackets(new S_SystemMessage(""+SuccItem2.getName()+"획득"), true);
							pc.sendPackets(new S_SystemMessage(""+SuccItem2.getName()+"은(는) 새 생명이 부여되었습니다."), true);
							L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE,"누군가가 "+l1iteminstance1.getName()+"에 새 생명을 부여 하였습니다."), true);
							eva.LogEnchantAppend("성공[기운템]: ", pc.getName(), Integer.toString(l1iteminstance1.getEnchantLevel()), l1iteminstance1.getName(), l1iteminstance1.getId());
							} else {
								if (20 > chance2){
								pc.getInventory().removeItem(useItem, 1);
								pc.sendPackets(new S_SystemMessage(""+l1iteminstance1.getName()+"은(는) 기운을 흡수하지 못하였으나 다행히 소멸하지 않았습니다."), true);
								} else {
								pc.getInventory().removeItem(l1iteminstance1, 1);
								pc.getInventory().removeItem(useItem, 1);
								pc.sendPackets(new S_SystemMessage(""+l1iteminstance1.getName()+"은(는) 기운을 흡수하지 못하고 소멸하였습니다."), true);
							}
							}
						} else {
							pc.sendPackets(new S_SystemMessage("해당 아이템에는 사용할 수 없습니다."), true);
						}
					} else if (itemId == 60486) {// 포노스 포상 20만
						L1ItemInstance item = pc.getInventory().storeItem(L1ItemId.ADENA, 200000);
						pc.sendPackets(new S_ServerMessage(403, item.getName() + " (200000)"), true);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 60487) {// 포노스 포상 75만
						L1ItemInstance item = pc.getInventory().storeItem(L1ItemId.ADENA, 750000);
						pc.sendPackets(new S_ServerMessage(403, item.getName()	+ " (750000)"), true);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 60488) {// 포노스 포상 300만
						L1ItemInstance item = pc.getInventory().storeItem(
								L1ItemId.ADENA, 3000000);
						pc.sendPackets(new S_ServerMessage(403, item.getName()
								+ " (3000000)"), true);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 60489) {// 포노스 포상 1000만
						L1ItemInstance item = pc.getInventory().storeItem(
								L1ItemId.ADENA, 10000000);
						pc.sendPackets(new S_ServerMessage(403, item.getName()
								+ " (10000000)"), true);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 60491) {// 금화주머니
						금화주머니(pc);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 160491) {// 금화주머니
						황금주머니(pc);
						pc.getInventory().removeItem(useItem, 1);
					} 
					else if (itemId == 391038 ) {// 52레벨 퀘스트
						
						if (pc.isWarrior()) {
							createNewItem3(pc, 90090, 1, 11); // 타이탄의 분노
						}
						if (pc.isKnight()) {
							createNewItem3(pc, 90086, 1, 11); // 타이탄의 분노
						}
						if (pc.isDragonknight()) {
							createNewItem3(pc, 90091, 1, 11); // 공포
						}
						if (pc.isCrown()) {
							createNewItem3(pc, 90092, 1, 11); // 칼날
						}
						if (pc.isDarkelf()) {
							createNewItem3(pc, 90087, 1, 11); // 붉이
						}
						if (pc.isWizard()) {
							createNewItem3(pc, 90085, 1, 11); // 수결지
						}
						if (pc.isIllusionist()) {
							createNewItem3(pc, 90089, 1, 11); // 절망
						}
						if (pc.isElf()) {
							createNewItem3(pc, 90088, 1, 11); // 격노
						}
							pc.getInventory().removeItem(useItem, 1);
							
	} else if (itemId == 391039 ) {// 52레벨 퀘스트		
						if (pc.isWarrior()) {
							createNewItem3(pc, 90090, 1, 12); // 타이탄의 분노
						}
						if (pc.isKnight()) {
							createNewItem3(pc, 90086, 1, 12); // 타이탄의 분노
						}
						if (pc.isDragonknight()) {
							createNewItem3(pc, 90091, 1, 12); // 공포
						}
						if (pc.isCrown()) {
							createNewItem3(pc, 90092, 1, 12); // 칼날
						}
						if (pc.isDarkelf()) {
							createNewItem3(pc, 90087, 1, 12); // 붉이
						}
						if (pc.isWizard()) {
							createNewItem3(pc, 90085, 1, 12); // 수결지
						}
						if (pc.isIllusionist()) {
							createNewItem3(pc, 90089, 1, 12); // 절망
						}
						if (pc.isElf()) {
							createNewItem3(pc, 90088, 1, 12); // 격노
						}
							pc.getInventory().removeItem(useItem, 1);
				
					} else if(itemId == 66732){ // 고대의 금빛 베리아나
						L1ItemInstance item = pc.getInventory().storeItem(60492, 30000);
						pc.sendPackets(new S_ServerMessage(403, item.getName() + "(30000)"), true);
						int giveItem = 0;
						int chance = _random.nextInt(100) + 1;
						if(chance < 20){
							giveItem  = 40393;
						} else if(chance >= 20 && chance < 35){
							giveItem  = 40394;
						} else if(chance >= 35 && chance < 50){ 
							giveItem  = 40395;
						} else if(chance >= 50 && chance < 65){ 
							giveItem  = 40396;
						} else if(chance >= 65 && chance < 80){
							giveItem  = 40466;
						} else if(chance >= 80 && chance < 85){
							giveItem  = 8156;
						} else if(chance >= 85 && chance < 90){
							giveItem  = 8193;
						} else if(chance >= 90 && chance < 95){
							giveItem  = 8194;
						} else if(chance >= 95 && chance < 97){
							giveItem  = 8195;
						} else {
							giveItem  = 8196;
						}
						L1ItemInstance item2 = pc.getInventory().storeItem(giveItem, 1);
						pc.sendPackets(new S_ServerMessage(403, item2.getName() + "(1)"), true);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 60482) {// 작은 금빛 베리아나
						int chance = _random.nextInt(100) + 1;
						int count = 0;
						if(chance < 50){
							count = 3000000;
						} else if  (chance >= 50 && chance < 60){
							count = 3200000;
						} else if  (chance >= 60 && chance < 70){
							count = 3500000;
						} else if  (chance >= 70 && chance < 80){
							count = 3800000;
						} else if  (chance >= 80 && chance < 90){
							count = 4000000;
						} else {
							count = 5000000;
						}
						L1ItemInstance item = pc.getInventory().storeItem(60492, count);
						pc.sendPackets(new S_ServerMessage(403, item.getName() + count), true);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 60483) {// 작은 은빛 베리아나
						L1ItemInstance item = pc.getInventory().storeItem(40308, 500000);
						pc.sendPackets(new S_ServerMessage(403, item.getName() + " (500000)"), true);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 60480) {// 큰 금빛 베리아나
						큰금빛베리아나(pc);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 60481) {// 큰 은빛 베리아나
						큰은빛베리아나(pc);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 60475) { // 기본 장비 상자
						기본장비상자(pc);
						pc.getInventory().removeItem(useItem, 1);

					} else if (itemId == 7258) { // 클래스 스킬북
						클래스스킬북(pc);
						pc.getInventory().removeItem(useItem, 1);

					} else if (itemId >= 7325 && itemId <= 7334) { // 페어리 상자
						페어리선물상자(pc, itemId);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 60471) { // 민티스의 마법인형 선물 상자
						민티스마법인형선물상자(pc);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 60472) { // 크리스마스 쿠키 주머니
						L1ItemInstance item = pc.getInventory().storeItem(
								60443, 1);
						pc.sendPackets(
								new S_ServerMessage(403, item.getName()), true);
						item = pc.getInventory().storeItem(
								60427 + _random.nextInt(12), 1);
						pc.sendPackets(
								new S_ServerMessage(403, item.getName()), true);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 60446) { // 크리스마스 솔로 파티 초대장
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),"15th_invite"), true);
					} else if (itemId == 39105){
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),"jindeath2017"), true);
						pc.픽시아이템사용id = useItem.getId();
					} else if (itemId == 5000046){
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),"dpoly"), true);
						pc.픽시아이템사용id = useItem.getId();
					} else if (itemId == 5000047){
						pc.픽시아이템사용id = useItem.getId();
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),"spoly"), true);
					} else if (itemId == 60424) { // 마녀 마법 코인 (어밴)
						L1SkillUse su = new L1SkillUse();
						su.handleCommands(pc, L1SkillId.ADVANCE_SPIRIT,
								pc.getId(), pc.getX(), pc.getY(), null, 0,
								L1SkillUse.TYPE_GMBUFF);
						su = null;
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 60425) { // 마녀 마법 코인 (드래곤 스킨)
						L1SkillUse su = new L1SkillUse();
						su.handleCommands(pc, L1SkillId.DRAGON_SKIN,
								pc.getId(), pc.getX(), pc.getY(), null, 0,
								L1SkillUse.TYPE_GMBUFF);
						su = null;
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 60426) { // 마녀 마법 코인 (아이언 스킨)
						L1SkillUse su = new L1SkillUse();
						su.handleCommands(pc, L1SkillId.IRON_SKIN, pc.getId(),
								pc.getX(), pc.getY(), null, 0,
								L1SkillUse.TYPE_GMBUFF);
						su = null;
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 60422) { // 드래곤 키 복구함
						pc.getInventory().removeItem(useItem, 1);
						L1ItemInstance item = pc.getInventory().storeItem(
								40308, 10000000);
						pc.sendPackets(new S_ServerMessage(403, item.getName()
								+ " (10000000)"), true);
					} else if (itemId == 60411) { // 군주의 포상
						군주의포상(pc, useItem);
					} else if (itemId == 60391) { // 붉은 기사단의 지령서 주머니
						붉은기사단의지령서주머니(pc, useItem);
					} else if (itemId == 60392) {// 붉은 기사단의 지령서
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
								"ev_siege"), true);
					} else if (itemId == 60383) { // 룬 마력 제거제
						룬마력제거제(pc, useItem, l1iteminstance1);
					} else if (itemId == 9095) { // 각인 해제
						각인해제(pc, useItem, l1iteminstance1);
					} else if (itemId == 60380) { // 기본 지급 장비함
						pc.getInventory().removeItem(useItem, 1);
						Beginner.getInstance().GiveItem장비함(pc);
					} else if (itemId == 60378) { // 기억 확장 구슬
						if (기억확장구슬(pc))
							pc.getInventory().removeItem(useItem, 1);
					} else if (itemId >= 60373 && itemId <= 60377) {
						신기한반지4개묶음(pc, itemId);
					} else if ((itemId >= 60413 && itemId <= 60416)
							|| (itemId >= 60418 && itemId <= 60421)) {
						스냅퍼의반지(pc, itemId, useItem);
					} else if (itemId >= 60361 && itemId <= 60372) {
						투옵티셔츠(pc, itemId, useItem);
					} else if (itemId == 60353) { // 시원한 얼음조각 상자
						L1ItemInstance item = pc.getInventory().storeItem(
								60354, 15);
						pc.sendPackets(new S_ServerMessage(403, item.getName()
								+ " (15)"), true);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 60354) { // 시원한 얼음 조각
						useCashScroll(pc, 60354, false);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 60350) {
						if (AntarasRaidSystem.getInstance().startRaid(pc)) {
							L1World.getInstance().broadcastServerMessage(
							"강철 길드 난쟁이: 으...드래곤의 울부짖음이 여기까지 들리오. 필시 누군가 드래곤 포탈을 연 것이 확실하오! 준비된 드래곤 슬레이어에게 영광과 축복을!");
							pc.getInventory().removeItem(useItem, 1);
						}
					} else if (itemId == 60351) {
						if (PaPooRaidSystem.getInstance().startRaid(pc)) {
							L1World.getInstance().broadcastServerMessage(
							"강철 길드 난쟁이: 으...드래곤의 울부짖음이 여기까지 들리오. 필시 누군가 드래곤 포탈을 연 것이 확실하오! 준비된 드래곤 슬레이어에게 영광과 축복을!");
							pc.getInventory().removeItem(useItem, 1);
						}
					} else if (itemId == 60352) {
						if (LindRaid.get().start(pc)) {
							pc.getInventory().removeItem(useItem, 1);
						}
						/** 발라 리뉴얼 **/
					} else if (itemId == 61000) {
						if (ValaRaidSystem.getInstance().countVala() >= 5) {
							pc.sendPackets(new S_SystemMessage("시스템 메세지 : 아직 드래곤이 모두 깨어 있습니다."));
							return;
						}
						ValaRaidSystem.getInstance().startValakas(pc);
						pc.getInventory().removeItem(useItem, 1);
						L1World.getInstance().broadcastServerMessage(
						"강철 길드 난쟁이: 으...드래곤의 울부짖음이 여기까지 들리오. 필시 누군가 드래곤 포탈을 연 것이 확실하오! 준비된 드래곤 슬레이어에게 영광과 축복을!");
						return;
					} else if (itemId == 49013) {// 마족의 주문서
						try {
							pc.setCurrentHp(0);
							pc.death(null);
							L1ItemInstance item = pc.getInventory().storeItem(
									49014, 1);
							pc.sendPackets(
									new S_ServerMessage(403, item.getName()),
									true);
						} catch (Exception e) {
						}
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 40312 || itemId == 49312) {// 여관 열쇠
						여관키(pc, useItem);
					} else if (itemId == 60333 || itemId == 60476 || itemId == 60477) {// 폴의 쾌속 릴
						폴의쾌속릴(pc, useItem, l1iteminstance1);
					} else if(itemId == 8300) {
						if(pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.드래곤의성장버프)){
							pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.드래곤의성장버프);
						}
						
						pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.신스킬, 6072), true);
						pc.sendPackets(new S_ACTION_UI(6072, 1800, 8382, 5087), true);
						
						pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.드래곤의성장버프, 1800 * 1000);
						
						pc.sendPackets(new S_SkillSound(pc.getId(), 15247), true);
						Broadcaster.broadcastPacket(pc, new S_SkillSound(pc.getId(), 15247), true);
						pc.getInventory().removeItem(useItem, 1);
						
					} else if (itemId == 600228) {// 폴의 쾌속 릴
						성장의릴(pc, useItem, l1iteminstance1);
					} else if (itemId == 60311 || itemId == 60312) { // 기르타스의 사념
						기르타스의사념TOTAL(pc, itemId);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 60306) { // 단테스의 유물 상자
						L1ItemInstance item = pc.getInventory().storeItem(60307, 1);
						pc.sendPackets(new S_ServerMessage(403, item.getName()));
						int i = 단테스상자(pc);
						if (i != 0) {
							int count = 1;
							if (i >= 60068 && i <= 60071)
								count = 3 + _random.nextInt(7);
							item = pc.getInventory().storeItem(i, count);
							pc.sendPackets(new S_ServerMessage(403, item
									.getName()));
						}
						pc.getInventory().removeItem(useItem, 1);

					} else if (itemId == 600218) { // 리퍼의유물
						룸티스선물(pc);
						pc.getInventory().removeItem(useItem, 1);

					} else if (itemId == 600232) { // 룸티지원
						룸티스지원(pc);
						pc.getInventory().removeItem(useItem, 1);

					} else if (itemId == 500211) { // 리퍼의유물
						산타양말(pc);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 500208) { // 리퍼의유물상자
						L1ItemInstance item = pc.getInventory().storeItem(
								500206, 1);
						pc.sendPackets(new S_ServerMessage(403, item.getName()));
						리퍼상자(pc);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 600221) { // 리퍼 선물 상자
						L1ItemInstance item = pc.getInventory().storeItem(
								500206, 1);
						pc.sendPackets(new S_ServerMessage(403, item.getName()));
						리퍼선물상자(pc);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 60307) { // 단테스의 유물 주머니
						if (pc.getInventory().checkItem(21157)) {
							pc.sendPackets(new S_SystemMessage(
									"특정 아이템이 인벤토리에 이미 있어 더 받을 수 없습니다."));
							return;
						}
						L1ItemInstance item = pc.getInventory().storeItem(
								21157, 1);
						pc.sendPackets(new S_ServerMessage(403, item.getName()));
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 60257) {// 드래곤의 자수정 상자
						L1ItemInstance item = pc.getInventory().storeItem(
								60256, 3);
						pc.sendPackets(
								new S_ServerMessage(403, item.getName()), true);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 60256) {// 드래곤의 자수정 단지
						if (pc.getInventory().checkItem(60255)) {
							pc.sendPackets(new S_ServerMessage(2887), true);
							return;
						}
						L1ItemInstance item = pc.getInventory().storeItem(
								60255, 1);
						pc.sendPackets(
								new S_ServerMessage(403, item.getName()), true);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId >= 60273 && itemId <= 60282) {
						순백의인장and비누(pc, useItem, l1iteminstance1);
					} else if (itemId == 60247) {// 천연 비누(판도라 향기티)
						판도라향수and문양(pc, useItem, l1iteminstance1);
					}  else if (itemId == 9094) {// 폭풍의 지원 상자
						pc.getInventory().removeItem(useItem, 1);
						폭풍의지원상자(pc);
					} else if (itemId >= 60235 && itemId <= 60246
							|| itemId == 60283) {
						티변환석(pc, useItem, l1iteminstance1);
					} else if (itemId >= 60218 && itemId <= 60232) {
						판도라향수and문양(pc, useItem, l1iteminstance1);
					} else if (itemId == 5588){
						if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.데스페라도)) {
							return;
						}
						if (pc.getMapId() == 1708 || pc.getMapId() == 1703){
							L1Teleport.teleport(pc, 32776, 32757, (short) 1710, pc.getMoveState().getHeading(), true, true);
							pc.getInventory().removeItem(useItem, 1);
						} else {
							pc.sendPackets(new S_SystemMessage("이곳에서 사용할 수 없습니다."), true);
						}
					} else if (itemId == 60217) { // 코마의 축복 코인
						L1SkillUse su = new L1SkillUse();
						su.handleCommands(pc, L1SkillId.STATUS_COMA_5,
								pc.getId(), pc.getX(), pc.getY(), null, 0,
								L1SkillUse.TYPE_GMBUFF);
						su = null;
						pc.getInventory().removeItem(useItem, 1);
						
					} else if (itemId == 391034) { // 미소성
						  if (pc.getSkillEffectTimerSet().hasSkillEffect(7600)) {
			                    pc.getSkillEffectTimerSet().killSkillEffectTimer(7600);
			                }
			                pc.getSkillEffectTimerSet().removeSkillEffect(7600);
			                pc.getSkillEffectTimerSet().setSkillEffect(7600, 1800000);
			                pc.sendPackets(new S_웃픈버프(1, 1800L));
			                pc.sendPackets(new S_SystemMessage("가이아의 성장의 기운이 느껴집니다."));
			                pc.getInventory().removeItem(useItem, 1);
			                
					} else if (itemId == 391035) { // 미소성
						   if (pc.getSkillEffectTimerSet().hasSkillEffect(7601)) {
			                    pc.getSkillEffectTimerSet().killSkillEffectTimer(7601);
			                    pc.getResistance().addMr(-10);
			                    pc.addDamageReductionByArmor(-2);
			                    pc.addMaxHp(-100);
			                    pc.addHpr(-2);
			                    pc.sendPackets(new S_HPUpdate(pc));
			                    pc.sendPackets(new S_MPUpdate(pc.getCurrentMp(), pc.getMaxMp()));
			                    pc.sendPackets(new S_SPMR(pc));
			                }
			                pc.getSkillEffectTimerSet().removeSkillEffect(7601);
			                pc.getSkillEffectTimerSet().setSkillEffect(7601, 1800000);
			                pc.getResistance().addMr(10);
			                pc.addDamageReductionByArmor(2);
			                pc.addMaxHp(100);
			                pc.addHpr(2);
			                pc.sendPackets(new S_HPUpdate(pc));
			                pc.sendPackets(new S_MPUpdate(pc.getCurrentMp(), pc.getMaxMp()));
			                pc.sendPackets(new S_SPMR(pc));
			                pc.sendPackets(new S_웃픈버프(2, 1800L));
			                pc.sendPackets(new S_SystemMessage("가이아의 방어의 기운이 느껴집니다."));
			                pc.getInventory().removeItem(useItem, 1);
			                
					} else if (itemId == 391036) { // 미소성
						  if (pc.getSkillEffectTimerSet().hasSkillEffect(7602)) {
			                    pc.getSkillEffectTimerSet().killSkillEffectTimer(7602);
			                    pc.addDmgup(-3);
			                    pc.addBowDmgup(-3);
			                    pc.getAbility().addSp(-3);
			                    pc.addMaxMp(-50);
			                    pc.addMpr(-2);
			                    pc.sendPackets(new S_HPUpdate(pc));
			                    pc.sendPackets(new S_MPUpdate(pc.getCurrentMp(), pc.getMaxMp()));
			                    pc.sendPackets(new S_SPMR(pc));
			                }
			                pc.getSkillEffectTimerSet().removeSkillEffect(7602);
			                pc.getSkillEffectTimerSet().setSkillEffect(7602, 1800000);
			                pc.addDmgup(3);
			                pc.addBowDmgup(3);
			                pc.getAbility().addSp(3);
			                pc.addMaxMp(50);
			                pc.addMpr(2);
			                pc.sendPackets(new S_HPUpdate(pc));
			                pc.sendPackets(new S_MPUpdate(pc.getCurrentMp(), pc.getMaxMp()));
			                pc.sendPackets(new S_SPMR(pc));
			                pc.sendPackets(new S_웃픈버프(3, 1800L));
			                pc.sendPackets(new S_SystemMessage("가이아의 공격의 기운이 느껴집니다."));
			                pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 60208) { // 완력의 체리빙수
						if (pc.getSkillEffectTimerSet().hasSkillEffect(
								L1SkillId.완력빙수))
							pc.getSkillEffectTimerSet().removeSkillEffect(
									L1SkillId.완력빙수);
						else if (pc.getSkillEffectTimerSet().hasSkillEffect(
								L1SkillId.민첩빙수))
							pc.getSkillEffectTimerSet().removeSkillEffect(
									L1SkillId.민첩빙수);
						else if (pc.getSkillEffectTimerSet().hasSkillEffect(
								L1SkillId.지식빙수))
							pc.getSkillEffectTimerSet().removeSkillEffect(
									L1SkillId.지식빙수);

						pc.getSkillEffectTimerSet().setSkillEffect(
								L1SkillId.완력빙수, 900 * 1000);
						pc.addHitup(5);
						pc.addDmgup(3);
						pc.getAbility().addAddedStr((byte) 1);
						pc.sendPackets(new S_OwnCharStatus2(pc), true);
						pc.sendPackets(new S_SkillSound(pc.getId(), 7954), true);
						Broadcaster.broadcastPacket(pc,
								new S_SkillSound(pc.getId(), 7954), true);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 60209) { // 민첩의 녹차빙수
						if (pc.getSkillEffectTimerSet().hasSkillEffect(
								L1SkillId.완력빙수))
							pc.getSkillEffectTimerSet().removeSkillEffect(
									L1SkillId.완력빙수);
						else if (pc.getSkillEffectTimerSet().hasSkillEffect(
								L1SkillId.민첩빙수))
							pc.getSkillEffectTimerSet().removeSkillEffect(
									L1SkillId.민첩빙수);
						else if (pc.getSkillEffectTimerSet().hasSkillEffect(
								L1SkillId.지식빙수))
							pc.getSkillEffectTimerSet().removeSkillEffect(
									L1SkillId.지식빙수);

						pc.getSkillEffectTimerSet().setSkillEffect(
								L1SkillId.민첩빙수, 900 * 1000);
						pc.addBowHitup(5);
						pc.addBowDmgup(3);
						pc.getAbility().addAddedDex((byte) 1);
						pc.sendPackets(new S_OwnCharStatus2(pc), true);
						pc.sendPackets(
								new S_PacketBox(S_PacketBox.char_ER, pc
										.get_PlusEr()), true);
						pc.sendPackets(new S_OwnCharAttrDef(pc));
						pc.sendPackets(new S_SkillSound(pc.getId(), 7952), true);
						Broadcaster.broadcastPacket(pc,
								new S_SkillSound(pc.getId(), 7952), true);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 60210) { // 지식의 단팥빙수
						if (pc.getSkillEffectTimerSet().hasSkillEffect(
								L1SkillId.완력빙수))
							pc.getSkillEffectTimerSet().removeSkillEffect(
									L1SkillId.완력빙수);
						else if (pc.getSkillEffectTimerSet().hasSkillEffect(
								L1SkillId.민첩빙수))
							pc.getSkillEffectTimerSet().removeSkillEffect(
									L1SkillId.민첩빙수);
						else if (pc.getSkillEffectTimerSet().hasSkillEffect(
								L1SkillId.지식빙수))
							pc.getSkillEffectTimerSet().removeSkillEffect(
									L1SkillId.지식빙수);

						pc.getSkillEffectTimerSet().setSkillEffect(
								L1SkillId.지식빙수, 900 * 1000);
						pc.addMaxMp(50);
						pc.getAbility().addAddedInt((byte) 1);
						pc.sendPackets(new S_OwnCharStatus2(pc), true);
						pc.sendPackets(
								new S_MPUpdate(pc.getCurrentMp(), pc.getMaxMp()),
								true);
						pc.sendPackets(new S_SkillSound(pc.getId(), 7956), true);
						Broadcaster.broadcastPacket(pc,
								new S_SkillSound(pc.getId(), 7956), true);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 60211) { // 서리 당한 통수박
						int rnd = (new Random(System.nanoTime())).nextInt(3) + 1;
						L1ItemInstance item = pc.getInventory().storeItem(
								60212, rnd);
						pc.sendPackets(new S_ServerMessage(403, item.getName()
								+ " (" + rnd + ")"), true);
						// pc.sendPackets(new
						// S_SystemMessage(item.getName()+" ("+rnd+")을 얻었습니다."),
						// true);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 60201) { // 봉인된 오만의 탑 1층 이동 부적
						L1ItemInstance item = pc.getInventory().storeItem(
								60202, 1);
						pc.sendPackets(
								new S_ServerMessage(403, item.getName()), true);
						// pc.sendPackets(new
						// S_SystemMessage(item.getName()+"을 얻었습니다."), true);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 60185) { // 드래곤의 큐브
						Timestamp lastUsed = useItem.getLastUsed();
						if (lastUsed == null || currentDate.getTimeInMillis() > lastUsed.getTime() + (60 * 60000 * 24)) {
							int i = 드래곤큐브(pc);
							L1ItemInstance item = pc.getInventory().storeItem(i, 1);
							pc.sendPackets(new S_ServerMessage(403, item.getName()), true);
							useItem.setLastUsed(new Timestamp(currentDate.getTimeInMillis()));
							useItem.setChargeCount(useItem.getChargeCount() - 1);
							pc.getInventory().updateItem(useItem, L1PcInventory.COL_CHARGE_COUNT);
							if (useItem.getChargeCount() <= 0)
								pc.getInventory().removeItem(useItem, 1);
						} else {
							long i = (lastUsed.getTime() + (60 * 60000 * 24)) - currentDate.getTimeInMillis();
							Calendar cal = (Calendar) currentDate.clone();
							cal.setTimeInMillis(cal.getTimeInMillis() + i);
							pc.sendPackets(new S_SystemMessage(i / 60000
									+ "분 동안(" + cal.getTime().getHours() + ":"
									+ cal.getTime().getMinutes()
									+ " 까지)은 사용할 수 없습니다."), true);
						}
					} else if (itemId == 60167) { // 픽시의 변신 막대
						pc.픽시아이템사용id = useItem.getId();
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(), "pixies"), true);
					} else if (itemId == 60308) { // 선수 변신 주문서
						pc.선수아이템사용id = useItem.getId();
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),"dionsevent"), true);
					} else if (itemId == 60325) { // 영웅 변신 주문서 (80렙변신)
						pc.영웅80변신아이템사용id = useItem.getId();
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(), "hero80"), true);
					} else if (itemId == 8000) {// 82히어로
						L1PolyMorph.doPoly(pc, 13153, 1800,L1PolyMorph.MORPH_BY_ITEMMAGIC);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 8001) {// 85히어로
						L1PolyMorph.doPoly(pc, 13152, 1800,
								L1PolyMorph.MORPH_BY_ITEMMAGIC);
						pc.getInventory().removeItem(useItem, 1);

					} else if (itemId == 50757) { // 쵸파의뿔
						int itemi = 쵸파의뿔(pc);
						int itemcount = 1;
						L1ItemInstance temptem2 = pc.getInventory().storeItem(
								itemi, itemcount);
						if (temptem2.isStackable())
							pc.sendPackets(
									new S_ServerMessage(403, temptem2.getName()
											+ " (" + itemcount + ")"), true);
						else
							pc.sendPackets(
									new S_ServerMessage(403, temptem2
											.getLogName()), true);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 60171) { // 픽시나무의 환영상자
						int count = _random.nextInt(29) + 1;
						pc.getInventory().storeItem(41159, count); // 신비한 날개깃털
						pc.sendPackets(new S_SystemMessage("신비한 날개깃털 (" + count
								+ ")이 생성되었습니다."), true);
						if (_random.nextInt(100) + 1 < 20) {
							pc.getInventory().storeItem(60167, 5); // 픽시 변신막대
							pc.sendPackets(new S_SystemMessage(
									"픽시 변신막대 (5)이 생성되었습니다."), true);
						}
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 40304) { // 마프르 유산
						int count = _random.nextInt(6) + 5;
						pc.getInventory().storeItem(40318, count); // 마돌
						pc.sendPackets(new S_SystemMessage("마력의 돌 (" + count
								+ ")이 생성되었습니다."), true);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 40305) { // 파아그리오의 유산
						int count = _random.nextInt(6) + 5;
						pc.getInventory().storeItem(40320, count); // 흑마석
						pc.sendPackets(new S_SystemMessage("흑마석 (" + count
								+ ")이 생성되었습니다."), true);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 40306) { // 에바의 유산
						int count = _random.nextInt(6) + 5;
						pc.getInventory().storeItem(40319, count); // 정령옥
						pc.sendPackets(new S_SystemMessage("정령옥 (" + count
								+ ")이 생성되었습니다."), true);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 40307) { // 사이하의 유산
						int count = _random.nextInt(20) + 1;
						pc.getInventory().storeItem(40318, count); // 마돌
						pc.sendPackets(new S_SystemMessage("마력의 돌 (" + count
								+ ")이 생성되었습니다."), true);
						count = _random.nextInt(30) + 1;
						pc.getInventory().storeItem(40319, count); // 정령옥
						pc.sendPackets(new S_SystemMessage("정령옥 (" + count
								+ ")이 생성되었습니다."), true);
						count = _random.nextInt(20) + 1;
						pc.getInventory().storeItem(40320, count); // 흑마석
						pc.sendPackets(new S_SystemMessage("흑마석 (" + count
								+ ")이 생성되었습니다."), true);
						count = _random.nextInt(5) + 1;
						pc.getInventory().storeItem(40031, count); // 악마의피
						pc.sendPackets(new S_SystemMessage("악마의 피 (" + count
								+ ")이 생성되었습니다."), true);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 60149) { // 토벌대원 주머니
						Timestamp lastUsed = useItem.getLastUsed();
						if (lastUsed == null
								|| currentDate.getTimeInMillis() > lastUsed
										.getTime() + (20 * 60000)) {
							pc.getInventory().storeItem(60150, 1);
							pc.sendPackets(new S_SystemMessage(
									"빛나는 구슬이 생성되었습니다."), true);
							useItem.setLastUsed(new Timestamp(currentDate
									.getTimeInMillis()));
							pc.getInventory().removeItem(useItem, 1);
						} else {
							/*
							 * SimpleDateFormat dateFormat = new
							 * SimpleDateFormat("HH시간 mm분"); String time =
							 * dateFormat.format(new
							 * Timestamp(((lastUsed.getTime() + (20 * 60000)) -
							 * currentDate.getTimeInMillis()) + (60 * 1000 * 60
							 * * 15))); pc.sendPackets(new
							 * S_SystemMessage(time+" 후에 사용 할수 있습니다."), true);
							 * dateFormat = null;
							 */
							long i = (lastUsed.getTime() + (20 * 60000))
									- currentDate.getTimeInMillis();
							Calendar cal = (Calendar) currentDate.clone();
							cal.setTimeInMillis(cal.getTimeInMillis() + i);
							pc.sendPackets(new S_SystemMessage(i / 60000
									+ "분 동안(" + cal.getTime().getHours() + ":"
									+ cal.getTime().getMinutes()
									+ " 까지)은 사용할 수 없습니다."), true);
						}
					} else if (itemId == 60159) { // 드래곤뼈 수집꾼의 주머니
						Timestamp lastUsed = useItem.getLastUsed();
						if (lastUsed == null
								|| currentDate.getTimeInMillis() > lastUsed
										.getTime() + (60 * 60000 * 1)) {
							pc.getInventory().storeItem(60160, 1);
							pc.sendPackets(new S_SystemMessage(
									"영롱한 구슬이 생성되었습니다."), true);
							useItem.setLastUsed(new Timestamp(currentDate
									.getTimeInMillis()));
							pc.getInventory().removeItem(useItem, 1);
						} else {
							/*
							 * SimpleDateFormat dateFormat = new
							 * SimpleDateFormat("HH시간 mm분"); String time =
							 * dateFormat.format(new
							 * Timestamp(((lastUsed.getTime() + (60 * 60000 *
							 * 1)) - currentDate.getTimeInMillis()) + (60 * 1000
							 * * 60 * 15))); pc.sendPackets(new
							 * S_SystemMessage(time+" 후에 사용 할수 있습니다."), true);
							 * dateFormat = null;
							 */
							long i = (lastUsed.getTime() + (60 * 60000 * 1))
									- currentDate.getTimeInMillis();
							Calendar cal = (Calendar) currentDate.clone();
							cal.setTimeInMillis(cal.getTimeInMillis() + i);
							pc.sendPackets(new S_SystemMessage(i / 60000
									+ "분 동안(" + cal.getTime().getHours() + ":"
									+ cal.getTime().getMinutes()
									+ " 까지)은 사용할 수 없습니다."), true);
						}
					} else if (itemId == 39101) { // by.코봉 
						영코부적2(useItem, pc);
					} else if (itemId == 99991) { // by.코봉 
						지배탑정상(useItem, pc);
					} else if (itemId == 391011) { // 숨겨진 계곡 부적
						영코부적(useItem, pc);
					} else if (itemId == 391025) { // 숨겨진 계곡 부적
						행복부적(useItem, pc);
					} else if (itemId == 60156) { // 숨겨진 계곡 부적
						숨계부적(useItem, pc);
					} else if (itemId == 601561) { // 보스이동 부적
						보스부적(useItem, pc);
					} else if (itemId == 391037) { // 보스이동 부적
						기르무기부적(useItem, pc);
					} else if (itemId == 601562) { // 보스이동 부적
						기감부적(useItem, pc);
					} else if (itemId == 60126) {// 봉인7파크
						pc.getInventory().removeItem(useItem, 1);
						createNewItem2(pc, 259, 1, 7);
					} else if (itemId == 60127) {// 봉인8파크
						pc.getInventory().removeItem(useItem, 1);
						createNewItem2(pc, 259, 1, 8);
					} else if (itemId == 60128) {// 봉인7파이
						pc.getInventory().removeItem(useItem, 1);
						createNewItem2(pc, 260, 1, 7);
					} else if (itemId == 60129) {// 봉인8파이
						pc.getInventory().removeItem(useItem, 1);
						createNewItem2(pc, 260, 1, 8);
					} else if (itemId == 60124) { // 아머 브레이크
						아머브레이크(pc, useItem, spellsc_objid);
					} else if (itemId == 60105) {
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
								"j_ep0p220"), true);
					} else if (itemId == 60106) {
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
								"j_ep0p221"), true);
					} else if (itemId == 60107) {
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
								"j_ep0p222"), true);
					} else if (itemId == 60108) {
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
								"j_ep0p223"), true);
					} else if (itemId == 60109) {
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
								"j_ep0p224"), true);
					} else if (itemId == 60110) {
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
								"j_ep0p225"), true);
					} else if (itemId == 60111) {
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
								"j_ep0p226"), true);
					} else if (itemId == 60112) {
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
								"j_ep0p227"), true);
					} else if (itemId == 60113) {
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
								"j_ep0p228"), true);
					} else if (itemId == 60114) {
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
								"j_ep0p229"), true);
					} else if (itemId == 60115) {
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
								"j_ep0p230"), true);
					} else if (itemId == 60116) {
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
								"j_ep0p231"), true);
					} else if (itemId == 60117) {
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
								"j_ep0p232"), true);
					} else if (itemId == 60118) {
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
								"j_ep0p233"), true);
					} else if (itemId == 60119) {
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
								"j_ep0p234"), true);
					} else if (itemId == 60120) {
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
								"j_ep0p235"), true);
					} else if (itemId == 60121) {
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
								"j_ep0p236"), true);
					} else if (itemId == 60122) {
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
								"j_ep0p237"), true);
					} else if (itemId == 60123) {
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
								"j_ep0p200"), true);
					} else if (itemId == 8275){ // 스냅퍼 강화 보호 주문서
						if (l1iteminstance1 != null && (l1iteminstance1.getItemId() >= 21246 && l1iteminstance1.getItemId() <= 21253)) {
							if (l1iteminstance1.getEnchantLevel() >= 0 && l1iteminstance1.getEnchantLevel() <= 8) {
								l1iteminstance1.setDemonBongin(true);
								pc.getInventory().removeItem(useItem, 1);
								pc.sendPackets(new S_SystemMessage(l1iteminstance1.getName() + "에 보호가 깃들었습니다."), true);
							} else
								pc.sendPackets(new S_SystemMessage("스냅퍼 장신구에만 사용 가능합니다."), true);
						} else {
							pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
							return;
						}
					} else if (itemId == 8276){ // 룸티스 강화 보호 주문서
						if (l1iteminstance1 != null && (l1iteminstance1.getItemId() >= 500007 && l1iteminstance1.getItemId() <= 500010) 
								||(l1iteminstance1.getItemId() >= 502007 && l1iteminstance1.getItemId() <= 502010)) {
							if (l1iteminstance1.getEnchantLevel() >= 0 && l1iteminstance1.getEnchantLevel() <= 8) {
								l1iteminstance1.setDemonBongin(true);
								pc.getInventory().removeItem(useItem, 1);
								pc.sendPackets(new S_SystemMessage(l1iteminstance1.getName() + "에 보호가 깃들었습니다."), true);
							} else
								pc.sendPackets(new S_SystemMessage("룸티스 장신구에만 사용 가능합니다."), true);
						} else {
							pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
							return;
						}
					} else if (itemId == 60104) { // 마족무기보호주문서
						if (l1iteminstance1 != null && (l1iteminstance1.getItemId() >= 450008 && l1iteminstance1.getItemId() <= 450013) || l1iteminstance1.getItemId() == 7233) {
							if (l1iteminstance1.getEnchantLevel() >= 0 && l1iteminstance1.getEnchantLevel() <= 12) {
								l1iteminstance1.setDemonBongin(true);
								pc.getInventory().removeItem(useItem, 1);
								pc.sendPackets(new S_SystemMessage(l1iteminstance1.getName() + "에 마족의 보호가 깃들었습니다."), true);
							} else
								pc.sendPackets(new S_SystemMessage("강화 수치가 0이상 12이하의 마족 무기에만 사용 가능합니다."), true);
						} else {
							pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
							return;
						}
					} else if (itemId == 6351) { // 무기 강화 보호 주문서
						if (l1iteminstance1 != null && l1iteminstance1.getItem().get_safeenchant() > 0 && l1iteminstance1.getItem().getType2() == 1){
							if (l1iteminstance1.getEnchantLevel() >= 0 && l1iteminstance1.getEnchantLevel() < 9) {
								l1iteminstance1.setDemonBongin(true);
								pc.getInventory().removeItem(useItem, 1);
								pc.sendPackets(new S_SystemMessage(l1iteminstance1.getName() + "에 신성한 보호가 깃들었습니다."), true);
							} else
								pc.sendPackets(new S_SystemMessage("+9 이상의 무기에는 사용할 수 없습니다."), true);
						} else {
							pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
							return;
						}
					} else if (itemId == 6352) { // 방어구 강화 보호 주문서
						if (l1iteminstance1 != null && l1iteminstance1.getItem().get_safeenchant() > 0 && l1iteminstance1.getItem().getType2() == 2){
							if (l1iteminstance1.getEnchantLevel() >= 0 && l1iteminstance1.getEnchantLevel() < 8) {
								l1iteminstance1.setDemonBongin(true);
								pc.getInventory().removeItem(useItem, 1);
								pc.sendPackets(new S_SystemMessage(l1iteminstance1.getName() + "에 신성한 보호가 깃들었습니다."), true);
							} else
								pc.sendPackets(new S_SystemMessage("+8 이상의 방어구에는 사용할 수 없습니다."), true);
						} else {
							pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
							return;
						}
					} else if (itemId >= 60088 && itemId <= 60103) { // 1~16단계
																		// 보상 상자
						// 해상전보상상자(pc, itemId);
						// pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 37005) { // 군터의 상자
						군터의상자(pc, itemId);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 37006) { // 군터의 백금 상자
						군터의백금상자(pc, itemId);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 37007) { // 군터의 황금 상자
						군터의황금상자(pc, itemId);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 39103) { // 산타의양말
						산타의양말(pc, itemId);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 39104) { // 산타의 황금 양말
						산타의황금양말(pc, itemId);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 38000) { // 군터의 상자
						시원한고대무기상자(pc, itemId);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 38001) { // 군터의 상자
						시원한고대방어구상자(pc, itemId);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 600231) { // 오림 보물상자
						세뱃돈(pc, itemId);
						pc.getInventory().removeItem(useItem, 1);

					} else if (itemId == 600226) { // 오림 보물상자
						오림보물상자(pc, itemId);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 60080) { // 희미한 기억의 구슬
						int size = pc.getBookMarkSize();
						if (size + 35 > pc.getBookmarkMax()) {
							pc.sendPackets(new S_ServerMessage(2961, "" + (size - pc.getBookmarkMax() + 35)), true);
							return;
						}
						pc.구슬아이템 = useItem.getId();
						pc.sendPackets(new S_Message_YN(2936, ""), true);
					} else if (itemId == 60081) { // 기억의 구슬 상자
						L1ItemInstance temptem3 = pc.getInventory().storeItem(
								60082, 1);
						pc.sendPackets(
								new S_ServerMessage(403, temptem3.getName()
										+ " (" + 1 + ")"), true);
						temptem3 = pc.getInventory().storeItem(60083, 1);
						pc.sendPackets(
								new S_ServerMessage(403, temptem3.getName()
										+ " (" + 1 + ")"), true);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 60082) { // 신비한 기억의 구슬
						int size = pc.getBookMarkSize();
						if (size + 8 > pc.getBookmarkMax()) {
							pc.sendPackets(new S_ServerMessage(2961, ""
									+ (size - pc.getBookmarkMax() + 8)), true);
							return;
						}
						pc.구슬아이템 = useItem.getId();
						pc.sendPackets(new S_Message_YN(3016, ""), true);
					} else if (itemId == 60083) { // 케플리샤의 기억 저장 구슬
						int size = pc.getBookMarkSize();
						if (size <= 0) {
							pc.sendPackets(new S_ServerMessage(2963, ""), true);
							return;
						}
						// pc.구슬아이템 = useItem.getId();
						pc.sendPackets(new S_Message_YN(2935, ""), true);
					} else if (itemId == 60084) { // 기억의 구슬
						int size = pc.getBookMarkSize();
						int itemsize = L1BookMark.ItemBookmarkChehck(useItem.getId());
						if (size + itemsize > pc.getBookmarkMax()) {
							pc.sendPackets(new S_ServerMessage(2961, ""+ (size - pc.getBookmarkMax() + itemsize)), true);
							return;
						}
						pc.구슬아이템 = useItem.getId();
						pc.sendPackets(new S_Message_YN(2936, ""), true);
					} else if (itemId == 60076) { // 완력의 도시락 바구니
						int rnd = _random.nextInt(100) + 1;
						int count = 0;
						if (rnd <= 80)
							count = 1;
						else
							count = 2;
						L1ItemInstance temptem3 = pc.getInventory().storeItem(
								60072, count);
						pc.sendPackets(
								new S_ServerMessage(403, temptem3.getName()
										+ " (" + count + ")"), true);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 60077) { // 민첩의 도시락 바구니
						int rnd = _random.nextInt(100) + 1;
						int count = 0;
						if (rnd <= 80)
							count = 1;
						else
							count = 2;
						L1ItemInstance temptem3 = pc.getInventory().storeItem(
								60073, count);
						pc.sendPackets(
								new S_ServerMessage(403, temptem3.getName()
										+ " (" + count + ")"), true);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 60078) { // 지식의 도시락 바구니
						int rnd = _random.nextInt(100) + 1;
						int count = 0;
						if (rnd <= 80)
							count = 1;
						else
							count = 2;
						L1ItemInstance temptem3 = pc.getInventory().storeItem(
								60074, count);
						pc.sendPackets(
								new S_ServerMessage(403, temptem3.getName()
										+ " (" + count + ")"), true);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 60079) { // 성장의 도시락 바구니
						int rnd = _random.nextInt(100) + 1;
						int count = 0;
						if (rnd <= 80)
							count = 1;
						else
							count = 2;
						L1ItemInstance temptem3 = pc.getInventory().storeItem(
								60075, count);
						pc.sendPackets(
								new S_ServerMessage(403, temptem3.getName()
										+ " (" + count + ")"), true);
						pc.getInventory().removeItem(useItem, 1);

					} else if (itemId == 500210) { // 위대한 전사의 주문
						if (pc.getSkillEffectTimerSet().hasSkillEffect(DRESS_MIGHTY))
							pc.getSkillEffectTimerSet().removeSkillEffect(DRESS_MIGHTY);
						if (pc.getSkillEffectTimerSet().hasSkillEffect(PHYSICAL_ENCHANT_STR))
							pc.getSkillEffectTimerSet().removeSkillEffect(PHYSICAL_ENCHANT_STR);
						if (pc.getSkillEffectTimerSet().hasSkillEffect(DRESS_DEXTERITY))
							pc.getSkillEffectTimerSet().removeSkillEffect(DRESS_DEXTERITY);
						if (pc.getSkillEffectTimerSet().hasSkillEffect(PHYSICAL_ENCHANT_DEX))
							pc.getSkillEffectTimerSet().removeSkillEffect(PHYSICAL_ENCHANT_DEX);
						if (pc.getSkillEffectTimerSet().hasSkillEffect(STATUS_힘업7))
							pc.getSkillEffectTimerSet().removeSkillEffect(STATUS_힘업7);
						if (pc.getSkillEffectTimerSet().hasSkillEffect(STATUS_힘업6))
							pc.getSkillEffectTimerSet().removeSkillEffect(STATUS_힘업6);
						if (pc.getSkillEffectTimerSet().hasSkillEffect(STATUS_덱업6))
							pc.getSkillEffectTimerSet().removeSkillEffect(STATUS_덱업6);
						if (pc.getSkillEffectTimerSet().hasSkillEffect(STATUS_덱업7))
							pc.getSkillEffectTimerSet().removeSkillEffect(STATUS_덱업7);
							pc.getAbility().addAddedStr((byte) 7);
							pc.getSkillEffectTimerSet().setSkillEffect(STATUS_힘업7, 1000 * 1800);
							pc.sendPackets(new S_Strup(pc, 7, 1800), true);
							pc.getSkillEffectTimerSet().setSkillEffect(STATUS_덱업7, 1000 * 1800);
							pc.getAbility().addAddedDex((byte) 7);
							pc.sendPackets(new S_Dexup(pc, 7, 1800), true);
							pc.sendPackets(new S_PacketBox(S_PacketBox.char_ER, pc.get_PlusEr()), true);
							pc.sendPackets(new S_OwnCharAttrDef(pc));
						pc.sendPackets(new S_SkillSound(pc.getId(), 9736), true);
						Broadcaster.broadcastPacket(pc, new S_SkillSound(pc.getId(), 9736), true);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 60068) { // 위대한 용사의 주문
						if (pc.getSkillEffectTimerSet().hasSkillEffect(
								DRESS_MIGHTY))
							pc.getSkillEffectTimerSet().removeSkillEffect(
									DRESS_MIGHTY);
						if (pc.getSkillEffectTimerSet().hasSkillEffect(
								PHYSICAL_ENCHANT_STR))
							pc.getSkillEffectTimerSet().removeSkillEffect(
									PHYSICAL_ENCHANT_STR);
						if (pc.getLevel() >= 65) {
							if (pc.getSkillEffectTimerSet().hasSkillEffect(
									STATUS_힘업7))
								pc.getSkillEffectTimerSet().removeSkillEffect(
										STATUS_힘업7);
							if (pc.getSkillEffectTimerSet().hasSkillEffect(
									STATUS_힘업6)) {
								pc.getSkillEffectTimerSet()
										.killSkillEffectTimer(STATUS_힘업6);
							} else
								pc.getAbility().addAddedStr((byte) 6);
							pc.getSkillEffectTimerSet().setSkillEffect(
									STATUS_힘업6, 1000 * 1200);
							pc.sendPackets(new S_Strup(pc, 6, 1200), true);
						} else {
							if (pc.getSkillEffectTimerSet().hasSkillEffect(
									STATUS_힘업6))
								pc.getSkillEffectTimerSet().removeSkillEffect(
										STATUS_힘업6);
							if (pc.getSkillEffectTimerSet().hasSkillEffect(
									STATUS_힘업7)) {
								pc.getSkillEffectTimerSet()
										.killSkillEffectTimer(STATUS_힘업7);
							} else
								pc.getAbility().addAddedStr((byte) 7);
							pc.getSkillEffectTimerSet().setSkillEffect(
									STATUS_힘업7, 1000 * 1200);

							pc.sendPackets(new S_Strup(pc, 7, 1200), true);
						}
						pc.sendPackets(new S_SkillSound(pc.getId(), 191), true);
						Broadcaster.broadcastPacket(pc,
								new S_SkillSound(pc.getId(), 191), true);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 60069) { // 위대한 궁수의 주문
						if (pc.getSkillEffectTimerSet().hasSkillEffect(
								DRESS_DEXTERITY))
							pc.getSkillEffectTimerSet().removeSkillEffect(
									DRESS_DEXTERITY);
						if (pc.getSkillEffectTimerSet().hasSkillEffect(
								PHYSICAL_ENCHANT_DEX))
							pc.getSkillEffectTimerSet().removeSkillEffect(
									PHYSICAL_ENCHANT_DEX);
						if (pc.getSkillEffectTimerSet().hasSkillEffect(
								STATUS_덱업6))
							pc.getSkillEffectTimerSet().removeSkillEffect(
									STATUS_덱업6);
						if (pc.getSkillEffectTimerSet().hasSkillEffect(
								STATUS_덱업7))
							pc.getSkillEffectTimerSet().removeSkillEffect(
									STATUS_덱업7);
						if (pc.getLevel() >= 65) {
							pc.getSkillEffectTimerSet().setSkillEffect(
									STATUS_덱업6, 1000 * 1200);
							pc.getAbility().addAddedDex((byte) 6);
							pc.sendPackets(new S_Dexup(pc, 6, 1200), true);
							pc.sendPackets(new S_PacketBox(S_PacketBox.char_ER,
									pc.get_PlusEr()), true);
							pc.sendPackets(new S_OwnCharAttrDef(pc));
						} else {
							pc.getSkillEffectTimerSet().setSkillEffect(
									STATUS_덱업7, 1000 * 1200);
							pc.getAbility().addAddedDex((byte) 7);
							pc.sendPackets(new S_Dexup(pc, 7, 1200), true);
							pc.sendPackets(new S_PacketBox(S_PacketBox.char_ER,
									pc.get_PlusEr()), true);
							pc.sendPackets(new S_OwnCharAttrDef(pc));
						}
						pc.sendPackets(new S_SkillSound(pc.getId(), 191), true);
						Broadcaster.broadcastPacket(pc,
								new S_SkillSound(pc.getId(), 191), true);
						pc.sendPackets(new S_ServerMessage(294), true);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 60070) { // 위대한 법사의 주문
						if (pc.getSkillEffectTimerSet().hasSkillEffect(
								DECAY_POTION)) { // 디케이포션 상태
							pc.sendPackets(new S_ServerMessage(698), true); // \f1마력에
																			// 의해
																			// 아무것도
																			// 마실
																			// 수가
																			// 없습니다.
							return;
						}
						// 아브소르트바리아의 해제
						pc.cancelAbsoluteBarrier();
						pc.setCurrentMp(pc.getCurrentMp()
								+ (_random.nextInt(5) + 8));
						pc.sendPackets(new S_SkillSound(pc.getId(), 190), true);
						Broadcaster.broadcastPacket(pc,
								new S_SkillSound(pc.getId(), 190), true);
						pc.sendPackets(new S_ServerMessage(77), true);
						pc.getInventory().removeItem(useItem, 1);
						} else if (itemId == 60067) { // 위대한 자의 유물 주머니
						if (pc.getInventory().checkItem(21094)) {
							pc.sendPackets(new S_SystemMessage(
									"특정 아이템이 인벤토리에 이미 있어 더 받을 수 없습니다."));
						} else {
							L1ItemInstance temptem = pc.getInventory()
									.storeItem(21094, 1);
							pc.sendPackets(
									new S_ServerMessage(403, temptem
											.getLogName()), true);
							pc.getInventory().removeItem(useItem, 1);
						}
					} else if(itemId == 73323) { // 축 룸티스 강화 주문서
						int enchant_level = l1iteminstance1.getEnchantLevel();
						if (enchant_level >= 8) {
							pc.sendPackets(new S_SystemMessage("더이상 사용할 수 없습니다."), true);
							return;
						}
						if(!((l1iteminstance1.getItem().getItemId() >= 500007 && 
							l1iteminstance1.getItem().getItemId() <= 500010) || 
							(l1iteminstance1.getItem().getItemId() >= 502007 && 
							l1iteminstance1.getItem().getItemId() <= 502010))){
							pc.sendPackets(new S_SystemMessage("사용 실패: 룸티스의 빛나는 귀걸이에만 사용할 수 있습니다"));
							return;
						}
						int rate = _random.nextInt(100)+1;
						if(rate <= Config.축룸티스성공률){
							l1iteminstance1.setEnchantLevel(l1iteminstance1.getEnchantLevel()+1);
							CharactersItemStorage storage = CharactersItemStorage.create();
					        storage.updateItemEnchantLevel(l1iteminstance1);
					        pc.getInventory().updateItem(l1iteminstance1, L1PcInventory.COL_ENCHANTLVL);
							pc.sendPackets(new S_SystemMessage("인첸트 성공: "+l1iteminstance1.getLogName()+"가 한순간 은색으로 빛납니다."), true);
						} else if(rate > Config.축룸티스성공률 && rate <= (Config.축룸티스성공률 + 20)) {
							l1iteminstance1.setEnchantLevel(l1iteminstance1.getEnchantLevel()-1);
							CharactersItemStorage storage = CharactersItemStorage.create();
					        storage.updateItemEnchantLevel(l1iteminstance1);
					        pc.getInventory().updateItem(l1iteminstance1, L1PcInventory.COL_ENCHANTLVL);
							pc.sendPackets(new S_SystemMessage("인첸트 실패: "+l1iteminstance1.getLogName()+"가 한순간 검게 빛납니다."), true);
						} else {
							l1iteminstance1.setEnchantLevel(l1iteminstance1.getEnchantLevel()+0);
							CharactersItemStorage storage = CharactersItemStorage.create();
					        storage.updateItemEnchantLevel(l1iteminstance1);
					        pc.getInventory().updateItem(l1iteminstance1, L1PcInventory.COL_ENCHANTLVL);
							pc.sendPackets(new S_SystemMessage("인첸트 실패: "+l1iteminstance1.getLogName()+"가 소멸의 굴레를 극복하였습니다."), true);
						}

						pc.getInventory().removeItem(useItem, 1);
					} else if(itemId == 55220) { // 무기 PVP 강화 주문서
						if(l1iteminstance1.getItem().getType2() != 1){
							pc.sendPackets(new S_SystemMessage("사용 실패: 무기에만 사용할 수 있습니다."));
							return;
						}
						if(l1iteminstance1.getPvPLevel() > 4){
							pc.sendPackets(new S_SystemMessage("PVP 5단계 이상 강화할 수 없습니다."));
							return;
						}
						int rate = _random.nextInt(100)+1;
						if(rate <= 20){
				        l1iteminstance1.setPvPLevel(l1iteminstance1.getPvPLevel() + 1);
				        CharactersItemStorage storage = CharactersItemStorage.create();
				        storage.updateItemPvPLevel(l1iteminstance1);
				        pc.getInventory().updateItem(l1iteminstance1, L1PcInventory.COL_PVPLVL);
						pc.getInventory().removeItem(useItem, 1);
						pc.sendPackets(new S_SystemMessage("PVP강화: 강화 성공"), true);
						}else{
						pc.sendPackets(new S_SystemMessage("PVP강화: 강화 실패"), true);	
						pc.getInventory().removeItem(useItem, 1);
						}
					} else if(itemId == 55221) { // 100% 무기 PVP 강화 주문서
						if(l1iteminstance1.getItem().getType2() != 1){
							pc.sendPackets(new S_SystemMessage("사용 실패: 무기에만 사용할 수 있습니다."));
							return;
						}
				        l1iteminstance1.setPvPLevel(l1iteminstance1.getPvPLevel() + 1);
				        CharactersItemStorage storage = CharactersItemStorage.create();
				        storage.updateItemPvPLevel(l1iteminstance1);
				        pc.getInventory().updateItem(l1iteminstance1,L1PcInventory.COL_PVPLVL);
						pc.getInventory().removeItem(useItem, 1);
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(), "pvpmenual"));
						pc.sendPackets(new S_SystemMessage("PVP강화 레벨 변경 완료"));
					
					} else if (itemId == 60064) { // 마녀의 마력 회복제
						if (pc.isWitchPotion()) {
							pc.setWitchPotion();
							pc.getInventory().updateItem(useItem,
									L1PcInventory.COL_DELAY_EFFECT);
							pc.getInventory().saveItem(useItem,
									L1PcInventory.COL_DELAY_EFFECT);
							pc.sendPackets(new S_ServerMessage(338, "$1084")); // 당신의%0가
																				// 회복해
																				// 갈
																				// 것입니다.
							pc.setCurrentMp(pc.getCurrentMp() + 1000); // 엠1000
																		// 채운다
							pc.getInventory().removeItem(useItem, 1);
						} else {
							pc.sendPackets(new S_SystemMessage(
									"아직은 사용할 수 없습니다."));
						}
					} else if (itemId == 60381 || itemId == 60384 || itemId == 7257) {
						고서(pc, useItem);
					} else if (itemId >= 7252 && itemId <= 7257) {
						룬주머니(pc, useItem);
					} else if (itemId >= 7352 && itemId <= 7357) {
						룬주머니80(pc, useItem);
					} else if (itemId >= 7452 && itemId <= 7457) {
						룬주머니85(pc, useItem);
					} else if (itemId >= 7552 && itemId <= 7557) {
						룬주머니90(pc, useItem);
					} else if (itemId == 60057) { // 옛 말하는섬의 진귀한 주머니
						L1ItemInstance item1 = pc.getInventory().storeItem(
								60055, 3);
						L1ItemInstance item2 = pc.getInventory().storeItem(
								60056, 2);
						pc.sendPackets(new S_ServerMessage(403, item1.getName()
								+ " (" + 3 + ")"), true);
						pc.sendPackets(new S_ServerMessage(403, item2.getName()
								+ " (" + 2 + ")"), true);
						int tempid = 진귀한아이템(pc);
						if (tempid != 0) {
							L1ItemInstance temptem = pc.getInventory()
									.storeItem(tempid, 1);
							pc.sendPackets(
									new S_ServerMessage(403, temptem
											.getLogName()), true);
						} else {
							pc.sendPackets(new S_SystemMessage(
									"진귀한 아이템을 얻지 못하였습니다."), true);
						}
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 60058) { // 옛 말하는섬의 주머니
						L1ItemInstance item1 = pc.getInventory().storeItem(
								60055, 2);
						L1ItemInstance item2 = pc.getInventory().storeItem(
								60056, 1);
						pc.sendPackets(new S_ServerMessage(403, item1.getName()
								+ " (" + 2 + ")"), true);
						pc.sendPackets(new S_ServerMessage(403, item2.getName()
								+ " (" + 1 + ")"), true);
						int tempid = 진귀한아이템(pc);
						if (tempid != 0) {
							L1ItemInstance temptem = pc.getInventory()
									.storeItem(tempid, 1);
							pc.sendPackets(
									new S_ServerMessage(403, temptem
											.getLogName()), true);
						} else {
							pc.sendPackets(new S_SystemMessage(
									"진귀한 아이템을 얻지 못하였습니다."), true);
						}
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 60056 || itemId == 60212
							|| itemId == 60398) { // 진귀한 식량, 잘익은 수박, 붉은기사단의물약
						if (pc.getSkillEffectTimerSet().hasSkillEffect(
								L1SkillId.STATUS_진귀한식량)) {
							pc.getSkillEffectTimerSet().killSkillEffectTimer(
									L1SkillId.STATUS_진귀한식량);
						}
						int time = 1800 * 1000;
						pc.getSkillEffectTimerSet().setSkillEffect(
								L1SkillId.STATUS_진귀한식량, time);
						pc.getInventory().removeItem(useItem, 1);

						pc.sendPackets(new S_SkillSound(pc.getId(), 7541), true);
						Broadcaster.broadcastPacket(pc,
								new S_SkillSound(pc.getId(), 7541), true);
						// pc.sendPackets(new S_ServerMessage(1542));
						/** 봉인 조우 무기 **/
					} else if (itemId == 60041) { // 봉인7마단
						pc.getInventory().removeItem(useItem, 1);
						createNewItem2(pc, 412002, 1, 7);
					} else if (itemId == 60042) { // 봉인7광풍
						pc.getInventory().removeItem(useItem, 1);
						createNewItem2(pc, 412005, 1, 7);
					} else if (itemId == 60043) { // 봉인7파대
						pc.getInventory().removeItem(useItem, 1);
						createNewItem2(pc, 412001, 1, 7);
					} else if (itemId == 60044) { // 봉인7천사
						pc.getInventory().removeItem(useItem, 1);
						createNewItem2(pc, 412003, 1, 7);
					} else if (itemId == 60045) { // 봉인7혹한
						pc.getInventory().removeItem(useItem, 1);
						createNewItem2(pc, 412004, 1, 7);
					} else if (itemId == 60046) { // 봉인7뇌신검
						pc.getInventory().removeItem(useItem, 1);
						createNewItem2(pc, 412000, 1, 7);
					} else if (itemId == 60047) { // 봉인7살천
						pc.getInventory().removeItem(useItem, 1);
						createNewItem2(pc, 191, 1, 7);
					} else if (itemId == 60048) { // 봉인8마단
						pc.getInventory().removeItem(useItem, 1);
						createNewItem2(pc, 412002, 1, 8);
					} else if (itemId == 60049) { // 봉인8광풍
						pc.getInventory().removeItem(useItem, 1);
						createNewItem2(pc, 412005, 1, 8);
					} else if (itemId == 60050) { // 봉인8파대
						pc.getInventory().removeItem(useItem, 1);
						createNewItem2(pc, 412001, 1, 8);
					} else if (itemId == 60051) { // 봉인8천사
						pc.getInventory().removeItem(useItem, 1);
						createNewItem2(pc, 412003, 1, 8);
					} else if (itemId == 60052) { // 봉인8혹한
						pc.getInventory().removeItem(useItem, 1);
						createNewItem2(pc, 412004, 1, 8);
					} else if (itemId == 60053) { // 봉인8뇌신검
						pc.getInventory().removeItem(useItem, 1);
						createNewItem2(pc, 412000, 1, 8);
					} else if (itemId == 60054) { // 봉인8살천
						pc.getInventory().removeItem(useItem, 1);
						createNewItem2(pc, 191, 1, 8);
						/** 배송 미믹 피리 **/
					} else if (itemId == 60017) {
						pc.getInventory().storeItem(60027, 1);
						pc.getInventory().removeItem(useItem, 1);
						pc.sendPackets(new S_SkillSound(pc.getId(), 8473), true);
					} else if (itemId == 60018) {
						pc.getInventory().storeItem(60028, 1);
						pc.getInventory().removeItem(useItem, 1);
						pc.sendPackets(new S_SkillSound(pc.getId(), 8473), true);
					} else if (itemId == 60035) {
						pc.getInventory().storeItem(60029, 100);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 60036) {
						pc.getInventory().storeItem(60029, 200);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 60037) {
						pc.getInventory().storeItem(60029, 300);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 60038) {
						pc.getInventory().storeItem(60030, 100);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 60039) {
						pc.getInventory().storeItem(60030, 200);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 60040) {
						pc.getInventory().storeItem(60030, 300);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 60019) {
						pc.getInventory().storeItem(60035, 1);
						pc.getInventory().removeItem(useItem, 1);
						pc.sendPackets(new S_SkillSound(pc.getId(), 8473), true);
					} else if (itemId == 60020) {
						pc.getInventory().storeItem(60036, 1);
						pc.getInventory().removeItem(useItem, 1);
						pc.sendPackets(new S_SkillSound(pc.getId(), 8473), true);
					} else if (itemId == 60021) {
						pc.getInventory().storeItem(60037, 1);
						pc.getInventory().removeItem(useItem, 1);
						pc.sendPackets(new S_SkillSound(pc.getId(), 8473), true);
					} else if (itemId == 60022) {
						pc.getInventory().storeItem(60038, 1);
						pc.getInventory().removeItem(useItem, 1);
						pc.sendPackets(new S_SkillSound(pc.getId(), 8473), true);
					} else if (itemId == 60023) {
						pc.getInventory().storeItem(60039, 1);
						pc.getInventory().removeItem(useItem, 1);
						pc.sendPackets(new S_SkillSound(pc.getId(), 8473), true);
					} else if (itemId == 60024) {
						pc.getInventory().storeItem(60040, 1);
						pc.getInventory().removeItem(useItem, 1);
						pc.sendPackets(new S_SkillSound(pc.getId(), 8473), true);
					} else if (itemId == 60027) {
						L1SpawnUtil.spawn(pc, 45711, 0, 0, false); // 아기 진돗개
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 60028) {
						L1SpawnUtil.spawn(pc, 45313, 0, 0, false); // 호랑이
						pc.getInventory().removeItem(useItem, 1);
					} else if(itemId == 7702) { // 러블리 캔디 포장지
						if(l1iteminstance1.getItemId() != 3341){
							pc.sendPackets(new S_SystemMessage("러블리 캔디에만 사용할 수 있습니다."));
							return;
						}
						pc.getInventory().removeItem(useItem, 1);
						pc.getInventory().removeItem(l1iteminstance1, 1);
						pc.getInventory().storeItem(3339, 1);
						pc.sendPackets(new S_SystemMessage("아아템 합성: 러블리 캔디 박스 획득"));
					} else if (itemId == 60025 || itemId == 60026) {// 엉뚱한시계
																	// 2시간, 4시간
						try {
							if (l1iteminstance1 != null) {
								if (l1iteminstance1.getItemId() >= 60011
										&& l1iteminstance1.getItemId() <= 60016) {
									Delivery del = DeliverySystem.getInstance()
											.get(l1iteminstance1.getId());
									if (del != null) {
										if (del.getClockCount() <= 0) {
											pc.sendPackets(new S_SystemMessage(
													"더 이상 사용할 수 없습니다."), true);
											return;
										}
										del.setClockCount(del.getClockCount() - 1);
										Timestamp ts = del.getTime();
										if (itemId == 60026)
											ts.setTime(ts.getTime()
													- (3600000 * 4));
										else
											ts.setTime(ts.getTime()
													- (3600000 * 2));
										del.setTime(ts);
										l1iteminstance1.setEndTime(del
												.getTime());
										pc.getInventory().updateItem(
												l1iteminstance1,
												L1PcInventory.COL_ENDTIME);
										pc.getInventory().saveItem(
												l1iteminstance1,
												L1PcInventory.COL_ENDTIME);
										DeliverySystem.UpdateTime(
												del.getItemObjId(),
												del.getTime(),
												del.getClockCount());
									}
								} else {
									pc.sendPackets(new S_SystemMessage(
											"아무일도 일어나지 않았습니다."), true); // \f1
																		// 아무것도
																		// 일어나지
																		// 않았습니다.
									return;
								}
							}
							pc.getInventory().removeItem(useItem, 1);
						} catch (Exception e) {
							e.printStackTrace();
						}
						/** 상아탑의 마법 주머니 **/
					} else if (itemId == 41505) {// 데스 삭감 주문서
						if (pc.getDeaths() == 0) {
							pc.sendPackets(new S_SystemMessage("데스 수치가 0이므로 사용할 수 없습니다."));
							return;
						}
						pc.setDeaths(pc.getDeaths() - 1);
						pc.sendPackets(new S_SystemMessage(pc.getName() + "님의 데스 수치가 1 감소했습니다"));
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 75000) {
						if(useItem.getCreaterName() != null){
						String _name = useItem.getCreaterName();
						if(_name.equalsIgnoreCase(pc.getName())) {
							if (pc.getDeaths() == 0) {
								pc.sendPackets(new S_SystemMessage(pc, "사망 전적이 없으므로 사용할 수 없습니다."));
								return;
							}
							pc.setDeaths(pc.getDeaths() - 1);
							pc.sendPackets(new S_SystemMessage(pc, pc.getName() + "님의 데스 수치가 1 감소했습니다"));
							pc.getInventory().removeItem(useItem, 1);
						} else {
							pc.sendPackets(new S_SystemMessage(pc, "희생자가 사용시 데스 1감소, 매입시 2,000,000아데나"));
						}
						} else{
							pc.sendPackets(new S_SystemMessage(pc, "희생자가 사용시 데스 1감소, 매입시 2,000,000아데나"));
						}
					} else if (itemId == 6116) {
						Timestamp lastUsed = useItem.getLastUsed();
						if (lastUsed == null || currentDate.getTimeInMillis() > lastUsed.getTime() + 60000) {
							serchdroplist2(pc, spellsc_objid, l1iteminstance1);
						} else {
							long i = (lastUsed.getTime() + 60000)
									- currentDate.getTimeInMillis();
							Calendar cal = (Calendar) currentDate.clone();
							cal.setTimeInMillis(cal.getTimeInMillis() + i);
							pc.sendPackets(new S_SystemMessage(i / 3600000
									+ "초 동안(" + cal.getTime().getHours() + ":"
									+ cal.getTime().getMinutes()
									+ " 까지)은 사용할 수 없습니다."), true);
						}
					} else if (itemId >= 600212 && itemId <= 600217) {
						강화버프(pc, itemId, useItem);
					} else if (itemId >= 600198 && itemId <= 600211) {
						강화버프2(pc, itemId, useItem);
					} else if (itemId == 600223) {
						피씨방코인(pc, itemId, useItem, 1);
					} else if (itemId == 600225) {
						피씨방코인(pc, itemId, useItem, 3);
					} else if (itemId == 60001) {
						Timestamp lastUsed = useItem.getLastUsed();
						if (lastUsed == null || currentDate.getTimeInMillis() > lastUsed.getTime() + (60 * 60000 * 1)) {
							pc.getInventory().storeItem(60002, 1);
							pc.sendPackets(new S_SystemMessage(
									"상아탑의 보급품 전송 주문서 가 생성되었습니다."), true);
							useItem.setLastUsed(new Timestamp(currentDate
									.getTimeInMillis()));
						} else {
							long i = (lastUsed.getTime() + (60 * 60000 * 1))
									- currentDate.getTimeInMillis();
							Calendar cal = (Calendar) currentDate.clone();
							cal.setTimeInMillis(cal.getTimeInMillis() + i);
							pc.sendPackets(new S_SystemMessage(i / 60000
									+ "분 동안(" + cal.getTime().getHours() + ":"
									+ cal.getTime().getMinutes()
									+ " 까지)은 사용할 수 없습니다."), true);
						}
						/** 상아탑의 보급품 전송 **/
					} else if (itemId == 60002) {
						pc.getInventory().storeItem(40029, 100);
						pc.getInventory().storeItem(40030, 6);
						pc.getInventory().storeItem(40095, 2);
						pc.getInventory().storeItem(40096, 2);
						pc.getInventory().removeItem(useItem, 1);
						/** 얼음 큐브 **/
					} else if (itemId == 90099) {
						L1PolyMorph.doPoly(pc, 5346, 30,
								L1PolyMorph.MORPH_BY_GM);
						pc.sendPackets(new S_SkillSound(pc.getId(), 2059), true);
						Broadcaster.broadcastPacket(pc,
								new S_SkillSound(pc.getId(), 2059), true);
						String S[] = { "쭈쭈니~~", "흐규흐규ㅜ_ㅜ", "쭌쭈니!!", "쭈니쭈니~",
								"눈알볼 맛있다~^^*" + "뿌잉뿌잉!", "헤헤~" };
						int ran = _random.nextInt(6);
						pc.sendPackets(new S_ChatPacket(pc, S[ran],
								Opcodes.S_SAY, 0), true);
						Broadcaster.broadcastPacket(pc, new S_ChatPacket(pc,
								S[ran], Opcodes.S_SAY, 0), true);
						pc.getInventory().removeItem(useItem, 1);
						S = null;
						/** 얼음 큐브 **/
					} else if (itemId == 6020) {
						int rnd = _random.nextInt(100) + 1;
						int itemid = 0;
						int count = 0;
						if (rnd <= 12) {
							itemid = 40021;
							count = 5;
						} else if (rnd <= 24) {
							itemid = 40024;
							count = 5;
						} else if (rnd <= 36) {
							itemid = 40088;
							count = 5;
						} else if (rnd <= 48) {
							itemid = 40018;
							count = 3;
						} else if (rnd <= 60) {
							itemid = 40068;
							count = 3;
						} else if (rnd <= 72) {
							itemid = 40031;
							count = 2;
						} else if (rnd <= 84) {
							itemid = 40308;
							count = 3000;
						} else if (rnd <= 92) {
							itemid = 40238;
							count = 1;
						} else {
							itemid = 6022;
							count = 1;
						}
						L1ItemInstance 아이템 = pc.getInventory().storeItem(
								itemid, count);// 기타아이템
						pc.sendPackets(new S_ServerMessage(403, 아이템.getName()
								+ "(" + count + ")"), true);
						pc.getInventory().removeItem(useItem, 1);

						/** 반짝이는 얼음 큐브 **/
					} else if (itemId == 6021) {
						L1ItemInstance 아이템 = pc.getInventory().storeItem(6022,
								2);// 화염의 기운 2
						pc.sendPackets(new S_ServerMessage(403, 아이템.getName()
								+ "(2)"), true);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 7242) {
						햄의주머니(pc, useItem);
						/** 마빈의 주머니 **/
					} else if (itemId == 50751) {
						Timestamp lastUsed = useItem.getLastUsed();
						if (lastUsed == null
								|| currentDate.getTimeInMillis() > lastUsed
										.getTime() + (1000 * 60 * 60 * 1)) {// 22시간
							if (useItem.getChargeCount() >= 1) {
								pc.getInventory().storeItem(50752, 200);
								pc.sendPackets(new S_ServerMessage(403,
										"전사의 체력 회복제 (200)"), true);
							}
							useItem.setChargeCount(useItem.getChargeCount() - 1);
							pc.getInventory().updateItem(useItem,
									L1PcInventory.COL_CHARGE_COUNT);
							if (useItem.getChargeCount() == 0) {
								pc.getInventory().removeItem(useItem);
							}
							useItem.setLastUsed(new Timestamp(currentDate
									.getTimeInMillis()));
						} else {
							long i = (lastUsed.getTime() + (60 * 60000 * 1))
									- currentDate.getTimeInMillis();
							Calendar cal = (Calendar) currentDate.clone();
							cal.setTimeInMillis(cal.getTimeInMillis() + i);
							StringBuffer sb = new StringBuffer();
							sb.append(i / 60000).append("분 후(");
							if (cal.getTime().getHours() < 10) {
								sb.append("0").append(cal.getTime().getHours())
										.append(":");
							} else {
								sb.append(cal.getTime().getHours()).append(":");
							}

							if (cal.getTime().getMinutes() < 10) {
								sb.append("0")
										.append(cal.getTime().getMinutes())
										.append(")에 사용할 수 있습니다.");
							} else {
								sb.append(cal.getTime().getMinutes()).append(
										")에 사용할 수 있습니다.");
							}

							pc.sendPackets(new S_SystemMessage(sb.toString()),
									true);

						}
					} else if (itemId == 600251) {// 시공의 구슬 큐브
						시공의구슬큐브(pc, useItem);
					} else if (itemId == 600252) {// 축드다 큐브
						축드다큐브(pc, useItem);
					} else if (itemId == 600253) {// 드래곤 토파즈 큐브
						토파즈큐브(pc, useItem);
					} else if (itemId == 600254) {// 마빈의 큐브
						마빈의큐브(pc, useItem);
					} else if (itemId == 600255) {// 오만의 보물 상자
						오만의보물상자(pc, useItem);
					} else if (itemId == 6015) {
						Timestamp lastUsed = useItem.getLastUsed();
						if (lastUsed == null || currentDate.getTimeInMillis() > lastUsed.getTime() + (1000 * 60 * 60 * 12)) {// 22시간
							if (!get마빈주머니_계정횟수(pc.getAccountName())) {
								pc.sendPackets(new S_SystemMessage("사용 계정의 마빈의 주머니 이용횟수를 초과했습니다. (매일 밤 12시 초기화)"),
										true);
								// pc.sendPackets(new S_ServerMessage(3693,
								// "마빈의 주머니"), true);
								return;
							}
							if (useItem.getChargeCount() == 1) {
								pc.getInventory().storeItem(6017, 1);
								pc.sendPackets(new S_ServerMessage(403,
										"불완전한 마법 구슬"), true);
								// pc.sendPackets(new
								// S_SystemMessage("불완전한 마법 구슬 1개를 얻었습니다."),
								// true);
							} else {
								pc.getInventory().storeItem(6016, 1);
								pc.sendPackets(new S_ServerMessage(403,
										"불완전한 마법 구슬 조각"), true);
								// pc.sendPackets(new
								// S_SystemMessage("불완전한 마법 구슬 조각 1개를 얻었습니다."),
								// true);
							}
							useItem.setChargeCount(useItem.getChargeCount() - 1);
							pc.getInventory().updateItem(useItem,
									L1PcInventory.COL_CHARGE_COUNT);
							if (useItem.getChargeCount() == 0) {
								pc.getInventory().removeItem(useItem);
							}
							useItem.setLastUsed(new Timestamp(currentDate
									.getTimeInMillis()));
						} else {
							long i = (lastUsed.getTime() + (60 * 60000 * 12))
									- currentDate.getTimeInMillis();
							Calendar cal = (Calendar) currentDate.clone();
							cal.setTimeInMillis(cal.getTimeInMillis() + i);
							StringBuffer sb = new StringBuffer();
							sb.append(i / 60000).append("분 후(");
							if (cal.getTime().getHours() < 10) {
								sb.append("0").append(cal.getTime().getHours())
										.append(":");
							} else {
								sb.append(cal.getTime().getHours()).append(":");
							}

							if (cal.getTime().getMinutes() < 10) {
								sb.append("0")
										.append(cal.getTime().getMinutes())
										.append(")에 사용할 수 있습니다.");
							} else {
								sb.append(cal.getTime().getMinutes()).append(
										")에 사용할 수 있습니다.");
							}

							pc.sendPackets(new S_SystemMessage(sb.toString()),
									true);

						}
						/** 시공의 항아리 **/
					} else if (itemId == 500016) {
						Timestamp lastUsed = useItem.getLastUsed();
						if (lastUsed == null || currentDate.getTimeInMillis() > lastUsed.getTime() + (60 * 60000 * 22)) { 
							if (pc.getInventory().checkItem(500017, 2)) {
								pc.sendPackets(new S_SystemMessage("시공의 구슬은 2개 이상 소지할 수 없습니다."), true);
								return;
							}
							pc.getInventory().storeItem(500017, 1);
							pc.sendPackets(new S_SystemMessage(
									"시공의 구슬 (1) 이 생성되었습니다."), true);
							/*
							 * pc.getInventory().storeItem(60164, 1);
							 * pc.sendPackets(new
							 * S_SystemMessage("오림의 시공 구슬 (1) 이 생성되었습니다."),
							 * true);
							 */
							useItem.setLastUsed(new Timestamp(currentDate
									.getTimeInMillis()));
						} else {
							/*
							 * SimpleDateFormat dateFormat = new
							 * SimpleDateFormat("HH시간 mm분"); String time =
							 * dateFormat.format(new
							 * Timestamp(((lastUsed.getTime() + (60 * 60000 *
							 * 5)) - currentDate.getTimeInMillis()) + (60 * 1000
							 * * 60 * 15))); pc.sendPackets(new
							 * S_SystemMessage(time+" 후에 사용 할수 있습니다."), true);
							 * dateFormat = null;
							 */
							long i = (lastUsed.getTime() + (60 * 60000 * 22))
									- currentDate.getTimeInMillis();
							Calendar cal = (Calendar) currentDate.clone();
							cal.setTimeInMillis(cal.getTimeInMillis() + i);
							StringBuffer sb = new StringBuffer();
							sb.append(i / 60000).append("분 후(");
							if (cal.getTime().getHours() < 10) {
								sb.append("0").append(cal.getTime().getHours())
										.append(":");
							} else {
								sb.append(cal.getTime().getHours()).append(":");
							}
							if (cal.getTime().getMinutes() < 10) {
								sb.append("0")
										.append(cal.getTime().getMinutes())
										.append(")에 사용할 수 있습니다.");
							} else {
								sb.append(cal.getTime().getMinutes()).append(
										")에 사용할 수 있습니다.");
							}
							pc.sendPackets(new S_SystemMessage(sb.toString()),
									true);
						}
					} else if (itemId == 5001000) {
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),"pbook001"));
					} else if (itemId == 500209) {
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),"pureelixir"));
					} else if (itemId == 41506) {
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
								"QuickBook"), true);
					} else if (itemId == 500022) {
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
								"j_ep0s10"), true);
					} else if (itemId == 500023) {
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
								"j_ep0s00"), true);
					} else if (itemId == 500024) {
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
								"j_ep0s01"), true);
					} else if (itemId == 500025) {
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
								"j_ep0s02"), true);
					} else if (itemId == 500026) {
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
								"j_ep0s03"), true);
					} else if (itemId == 500027) {
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
								"j_ep0s04"), true);
					} else if (itemId == 500028) {
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
								"j_ep0s05"), true);
					} else if (itemId == 500029) {
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
								"j_ep0s06"), true);
					} else if (itemId == 500030) {
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
								"j_ep0s07"), true);
					} else if (itemId == 500031) {
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
								"j_ep0s08"), true);
					} else if (itemId == 500032) {
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
								"j_ep0s09"), true);
					} else if (itemId == L1ItemId.INCRESE_HP_SCROLL
							|| itemId == L1ItemId.INCRESE_MP_SCROLL
							|| itemId == L1ItemId.INCRESE_ATTACK_SCROLL
							|| itemId == L1ItemId.CHUNSANG_HP_SCROLL
							|| itemId == L1ItemId.CHUNSANG_MP_SCROLL
							|| itemId == L1ItemId.CHUNSANG_ATTACK_SCROLL
							|| itemId == 66712 || itemId == 66713 || itemId == 66714) {
						useCashScroll(pc, itemId, false);
						pc.getInventory().removeItem(useItem, 1);

					} else if (itemId == 40858) { // liquor(술)
						pc.setDrink(true);
						pc.sendPackets(new S_Liquor(pc.getId()), true);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == L1ItemId.EXP_POTION
							|| itemId == L1ItemId.EXP_POTION2
							|| itemId == L1ItemId.EXP_POTION_fairly
							|| itemId == L1ItemId.EXP_POTION_cash) { // 천상의물약
						UseExpPotion(pc, itemId);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == L1ItemId.EXP_POTION3) { // 게렝전투
						UseExpPotion2(pc, itemId);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == L1ItemId.EXP_POTION4) { // 게렝행운
						UseExpPotion3(pc, itemId);
						pc.getInventory().removeItem(useItem, 1);

						/** 마안 7종 추가 **/
					} else if (itemId >= 430104 && itemId <= 430110) { // 마안 7종
						pc.cancelAbsoluteBarrier(); // 아브소르트바리아의 해제
						int skillid = itemId - 40858;
						Timestamp lastUsed = useItem.getLastUsed();
						
						if (!pc.getInventory().checkItem(41246, 1000)) {
							pc.sendPackets(new S_SystemMessage(pc,  "결정체가 부족합니다."));
							return;
						}
						if (lastUsed != null && currentDate.getTimeInMillis() -  lastUsed.getTime() < 3600 * 1000) {
							long time = 3600 - ((currentDate.getTimeInMillis() - lastUsed.getTime()) / 1000);
							long minute = time / 60;
							long second = time % 60;
							pc.sendPackets(new S_SystemMessage(pc,  "해당 아이템은 " + minute + "분 " + second + "초 후에 사용 가능합니다."));
							return;
						}

						if (!CheckEffects(pc)) {
							return;
						}
						if (itemId == L1ItemId.DRAGONMAAN_EARTH) { // 지룡의 마안
							if (CheckEffects(pc)) {
								skillid = 7671;
							}
						} else if (itemId == L1ItemId.DRAGONMAAN_WATER) { // 수룡의  마안
							if (CheckEffects(pc)) {
								skillid = 7672;
							}
						} else if (itemId == L1ItemId.DRAGONMAAN_FIRE) { // 화룡의 마안
							if (CheckEffects(pc)) {
								skillid = 7673;
							}
						} else if (itemId == L1ItemId.DRAGONMAAN_WIND) { // 풍룡의 마안
							if (CheckEffects(pc)) {
								skillid = 7674;
							}
						} else if (itemId == L1ItemId.DRAGONMAAN_BIRTH) { // 탄생의 마안
							if (CheckEffects(pc)) {
								skillid = 7675;
							}
						} else if (itemId == L1ItemId.DRAGONMAAN_SHAPE) { // 형상의 마안
							if (CheckEffects(pc)) {
								skillid = 7676;
							}
						} else if (itemId == L1ItemId.DRAGONMAAN_LIFE) { // 생명의  마안
							if (CheckEffects(pc)) {
								skillid = 7677;
							}
						}
						pc.getInventory().consumeItem(41246, 1000);
						L1SkillUse l1skilluse = new L1SkillUse();
						l1skilluse.handleCommands(client.getActiveChar(), skillid, spellsc_objid, spellsc_x, spellsc_y, null, 0, L1SkillUse.TYPE_SPELLSC);
						l1skilluse = null;
						useItem.setLastUsed(new Timestamp(currentDate.getTimeInMillis()));
						try {
							pc.save();
						} catch (Exception e) {
						}
					
					} else if (itemId == L1ItemId.POTION_OF_CURE_POISON
							|| itemId == 40507 || itemId == 60153) { // 시션 일부, 엔트의 가지
						if (pc.getSkillEffectTimerSet().hasSkillEffect(71) == true) { // 디케이포션
																						// 상태
							pc.sendPackets(new S_ServerMessage(698), true); // 마력에
																			// 의해
																			// 아무것도
																			// 마실
																			// 수가
																			// 없습니다.
						} else {
							pc.cancelAbsoluteBarrier(); // 아브소르트바리아의 해제
							pc.sendPackets(new S_SkillSound(pc.getId(), 192),
									true);
							Broadcaster.broadcastPacket(pc,
									new S_SkillSound(pc.getId(), 192), true);
							if (itemId == L1ItemId.POTION_OF_CURE_POISON
									|| itemId == 60153) {
								pc.getInventory().removeItem(useItem, 1);
							} else if (itemId == 40507) {
								pc.getInventory().removeItem(useItem, 1);
							}
							pc.curePoison();
						}

					} else if (itemId == 40066 ) { 
						pc.sendPackets(new S_SystemMessage("당신의 MP가 회복되었습니다.")); // 당신의%0가
						pc.setCurrentMp(pc.getCurrentMp()
								+ (7 + _random.nextInt(6))); 
						pc.getInventory().removeItem(useItem, 1);
					
					
					} else if (itemId == 40067 || itemId == 41413 || itemId == 3341) { // 쑥떡, 복떡
						pc.sendPackets(new S_SystemMessage("당신의 MP가 소량 회복되었습니다.")); // 당신의%0가
					pc.setCurrentMp(pc.getCurrentMp()
								+ (15 + _random.nextInt(16))); // 15~30
						pc.getInventory().removeItem(useItem, 1);
						
					} else if (itemId == 41414) { 
						pc.sendPackets(new S_SystemMessage("당신의 MP가 대량 회복되었습니다.")); // 당신의%0가
					pc.setCurrentMp(pc.getCurrentMp()
								+ (25 + _random.nextInt(15))); // 15~30
						pc.getInventory().removeItem(useItem, 1);
					
						
					} else if (itemId == 676767) { 
						pc.sendPackets(new S_SystemMessage("당신의 MP가 가득히 채워졌습니다.")); // 당신의%0가
					pc.setCurrentMp(pc.getCurrentMp()
								+ 1000); // 15~30
						pc.getInventory().removeItem(useItem, 1);
						
					
					} else if (itemId == 435001) { // 빛나는 나뭇잎
						pc.sendPackets(new S_ServerMessage(338, "$1084"), true); // 당신의%0가
						pc.setCurrentMp(pc.getCurrentMp() + 44);
						pc.getInventory().removeItem(useItem, 1);
					
					
					} else if (itemId == 40735) { // 용기의 코인
						pc.sendPackets(new S_ServerMessage(338, "$1084"), true); // 당신의%0가
																					// 회복해
																					// 갈
																					// 것입니다.
						pc.setCurrentMp(pc.getCurrentMp() + 60);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 40042) { // 스피릿 일부
						pc.sendPackets(new S_ServerMessage(338, "$1084"), true); // 당신의%0가
																					// 회복해
																					// 갈
																					// 것입니다.
						pc.setCurrentMp(pc.getCurrentMp() + 50);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 41404) { // 쿠쟈크의 영약
						pc.sendPackets(new S_ServerMessage(338, "$1084"), true); // 당신의%0가
																					// 회복해
																					// 갈
																					// 것입니다.
						pc.setCurrentMp(pc.getCurrentMp()
								+ (80 + _random.nextInt(21))); // 80~100
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 41412) { // 금의 톨즈
						pc.sendPackets(new S_ServerMessage(338, "$1084"), true); // 당신의%0가
																					// 회복해
																					// 갈
																					// 것입니다.
						pc.setCurrentMp(pc.getCurrentMp()
								+ (5 + _random.nextInt(16))); // 5~20
						pc.getInventory().removeItem(useItem, 1);

					} else if (itemId == 141917) {// 이벤트 인형 변경 케플리샤의 증표
						int dollid = l1iteminstance1.getItemId();
						if (dollid == 430000 || dollid == 41248
								|| dollid == 430002 || dollid == 141918
								|| dollid == 41249 || dollid == 430001
								|| dollid == 430004 || dollid == 430500
								|| dollid == 141919 || dollid == 141920
								|| dollid == 141922 || dollid == 141921
								|| dollid == 500203 || dollid == 5000035
								|| dollid == 500202 || dollid == 41250) {
							케플리샤인형(pc, useItem, l1iteminstance1);
						} else {
							pc.sendPackets(new S_SystemMessage(
									"아무일도 일어나지 않았습니다."));
						}
					} else if (itemId == 500231) {
						int i = l1iteminstance1.getItem().getItemId();
						boolean isAppear = true;
						L1DollInstance doll = null;
						for (Object dollObject : pc.getDollList()) {
							doll = (L1DollInstance) dollObject;
							if (doll.getItemObjId() == itemId) {
								isAppear = false;
								break;
							}
						}
						if (isAppear) {
							if (pc.getDollListSize() >= 1) {
								pc.sendPackets(new S_SystemMessage(
										"사용 중인 인형은 변경 할 수 없습니다."), true);
								return;
							}
							if ((i == 41248) || (i == 41249) || (i == 41250)
									|| (i == 430000) || (i == 430001)
									|| (i == 430002) || (i == 430003)
									|| (i == 430004) || (i == 430500)
									|| (i == 430505) || (i == 430506)) {
								Random _random = new Random();
								int i50 = _random.nextInt(100);
								// if (i50 <= 6){
								// pc.sendPackets(new
								// S_SystemMessage("인형이 증발되어 사라집니다.ㅠ_ㅠ"));
								// }
								if ((i50 >= 7) && (i50 <= 16)) {
									pc.getInventory().storeItem(41249, 1);
									pc.sendPackets(new S_ServerMessage(403,
											"마법인형 : 서큐버스"), true);
								}
								if ((i50 >= 17) && (i50 <= 34)) {
									pc.getInventory().storeItem(41250, 1);
									pc.sendPackets(new S_ServerMessage(403,
											"마법인형 : 늑대인간"), true);
								}
								if ((i50 >= 35) && (i50 <= 36)) {
									pc.getInventory().storeItem(430000, 1);
									pc.sendPackets(new S_ServerMessage(403,
											"마법인형 : 돌골렘"), true);
								}
								if ((i50 >= 37) && (i50 <= 45)) {
									pc.getInventory().storeItem(430001, 1);
									pc.sendPackets(new S_ServerMessage(403,
											"마법인형 : 장로"), true);
								}
								if ((i50 >= 46) && (i50 <= 55)) {
									pc.getInventory().storeItem(430002, 1);
									pc.sendPackets(new S_ServerMessage(403,
											"마법인형 : 크러스트시안"), true);
								}
								if ((i50 >= 56) && (i50 <= 65)) {
									pc.getInventory().storeItem(430003, 1);
									pc.sendPackets(new S_ServerMessage(403,
											"마법인형 : 시댄서"), true);
								}
								if ((i50 >= 66) && (i50 <= 86)) {
									pc.getInventory().storeItem(430004, 1);
									pc.sendPackets(new S_ServerMessage(403,
											"마법인형 : 에티"), true);
								}
								/*
								 * if ((i50 >= 87) && (i50 <= 89)){
								 * pc.getInventory().storeItem(5000034, 1);
								 * pc.sendPackets(new
								 * S_SystemMessage("마법인형 : 에틴을 얻었습니다.")); }
								 */
								if ((i50 >= 90) && (i50 <= 92)) {
									pc.getInventory().storeItem(430500, 1);
									pc.sendPackets(new S_ServerMessage(403,
											"마법인형 : 코카트리스"), true);
								}
								if ((i50 >= 93) && (i50 <= 95)) {
									pc.getInventory().storeItem(430505, 1);
									pc.sendPackets(new S_ServerMessage(403,
											"마법인형 : 라미아"), true);
								}
								if ((i50 >= 96) && (i50 <= 97)) {
									pc.getInventory().storeItem(41915, 1);
									pc.sendPackets(new S_ServerMessage(403,
											"마법인형 : 허수아비"), true);
								}
								if ((i50 >= 98) && (i50 <= 99)) {
									pc.getInventory().storeItem(430506, 1);
									pc.sendPackets(new S_ServerMessage(403,
											"마법인형 : 스파토이"), true);
								}
								pc.getInventory().removeItem(useItem, 1);
								pc.getInventory()
										.removeItem(l1iteminstance1, 1);
								_random = null;
							}
						}
					} else if (itemId == 467009) { // 캐릭명변경주문서
						pc.캐릭명변경 = true;
						pc.sendPackets(new S_SystemMessage("변경할 케릭명을 입력하세요"),
								true);
					} else if (itemId == 437008) { // 속죄의 성서 사용법
						pc.sendPackets(new S_SystemMessage(
								"이아이템은 신녀 유리스에게 가져가면 라우풀은 3천씩 받을수 있습니다.."),
								true);
					} else if (itemId == 500034) { // 기란던전 시간충전 int
						Timestamp nowday = new Timestamp(
								System.currentTimeMillis());
						String chat = "";
						pc.setgirantime(1);
						pc.setgiranday(nowday);
						chat = "기란감옥 과 글로디오 던전의 이용시간이 초기화 되었습니다.";
						pc.getNetConnection().getAccount().updateDGTime();
						pc.sendPackets(new S_SystemMessage(chat));
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 5000030) { // 뽕데스물약~!
						pc.getInventory().removeItem(useItem, 1);
						// int polyId = pc.getGfxId().getTempCharGfx();
						L1PolyMorph.doPoly(pc, 5641, 600,
								L1PolyMorph.MORPH_BY_ITEMMAGIC);

					} else if (itemId == 400074) { // 1억 아데나 통장
						if (pc.getInventory().checkItem(40308, 100000000)) {
							pc.getInventory().consumeItem(40308, 100000000);
							pc.getInventory().storeItem(400075, 1);
							pc.sendPackets(new S_SystemMessage(
									"1억 아데나가 1억 수표로 변환되었습니다."), true);
						} else {
							pc.sendPackets(new S_SystemMessage(
									"1억 아데나가 있어야 가능합니다."), true);
						}
					} else if (itemId == 400075) { // 1억 아데나 수표
						if (pc.getInventory().checkItem(40308, 1900000000)) {
							pc.sendPackets(new S_SystemMessage(
									"인벤에 소지하고 계신 아데나 량을 19억 이하로 만들어주세요."), true);
						} else {
							pc.getInventory().storeItem(40308, 100000000);
							pc.getInventory().consumeItem(400075, 1);
							pc.sendPackets(new S_SystemMessage(
									"1억 아데나로 변환되었습니다."), true);
						}
					} else if (itemId == 42098) { // 라우풀 물약
						pc.setLawful(32000);
						pc.save();
						pc.getInventory().removeItem(useItem, 1);
						pc.sendPackets(new S_SystemMessage("\\fS성향치가 라우풀로 변화 됩니다."), true);
					} else if (itemId == 42099) { // 카오틱물약
						pc.setLawful(-32000);
						pc.save();
						pc.getInventory().removeItem(useItem, 1);
						pc.sendPackets(new S_SystemMessage(
								"\\fY성향치가 카오틱으로 변화 됩니다."), true);

					} else if (itemId == 4443) { // 라우풀 물약
						pc.setLawful(2000);
						pc.save();
						pc.getInventory().removeItem(useItem, 1);
						pc.sendPackets(new S_SystemMessage(
								"\\fS성향치가 라우풀로 변화 됩니다."), true);

					} else if (itemId == 4444) { // 카오틱물약
						pc.setLawful(-2000);
						pc.save();
						pc.getInventory().removeItem(useItem, 1);
						pc.sendPackets(new S_SystemMessage(
								"\\fY성향치가 카오틱으로 변화 됩니다."), true);

					} else if (itemId == 100903 || itemId == 100904) { // 캐릭터교환증표
																		// 설명서
						pc.sendPackets(new S_UserCommands2(1), true);

					} else if (itemId == 999999) { // 무인PC설명편지지
						pc.sendPackets(new S_UserCommands1(1), true);

					} else if (itemId == 9999) { // 서버정보편지지
						pc.sendPackets(new S_UserCommands(1), true);

					} else if (itemId == 42198) { // 킬뎃 초기화 주문서
						pc.setKills(0);
						pc.setDeaths(0);
						pc.sendPackets(new S_SystemMessage(pc.getName()
								+ "님의 킬,데스 수치가 초기화되었습니다."), true);
						pc.getInventory().removeItem(useItem, 1);

						// ////////경험치물약 ---------------------------------
					} else if (itemId == 550009) { // 경험치물약
						if (pc.getLevel() < 51) {
							pc.addExp((ExpTable.getExpByLevel(51) - 1) - pc.getExp() + ((ExpTable.getExpByLevel(51) - 1) / 100));
						} else {
							pc.addExp(360650L);
							pc.getInventory().removeItem(useItem, 1);
						}
					} else if (itemId == 391015) { // 경험치물약
						if (pc.getLevel() < 51) {
							pc.addExp((ExpTable.getExpByLevel(51) - 1) - pc.getExp() + ((ExpTable.getExpByLevel(51) - 1) / 100));
						} else {
							pc.addExp(180325L);
							pc.getInventory().removeItem(useItem, 1);
						}
					} else if (itemId == 391016) { // 경험치물약
						if (pc.getLevel() < 51) {
							pc.addExp((ExpTable.getExpByLevel(51) - 1) - pc.getExp() + ((ExpTable.getExpByLevel(51) - 1) / 100));
						} else {
							pc.addExp(90162L);
							pc.getInventory().removeItem(useItem, 1);
						}

					
						// /////////경험치물약
						// ----------------------------------------

						// 자기 계정 군주 혈맹에 가입하기
					} else if (itemId == 42085) {
						if (pc.getInventory().checkItem(42085, 1)) { // 인벤에 있나
																		// 체크
							pc.getInventory().consumeItem(42085, 1); // 소모
							if (pc.isCrown()) { // 군주라면
								if (pc.get_sex() == 0) { // 왕자라면
									pc.sendPackets(new S_SystemMessage(
											"다른 혈맹에 가입하다니요! 당신은 왕자입니다!"), true); // 당신은
																					// 왕자입니다
								} else { // 공주라면
									pc.sendPackets(new S_SystemMessage(
											"다른 혈맹에 가입하다니요! 당신은 공주입니다!"), true); // 당신은
																					// 공주입니다
								}
								return;
							}
							if (pc.getClanid() != 0) { // 혈맹이 있다면
								pc.sendPackets(new S_SystemMessage(
										"이미 혈맹에 가입했습니다."), true); // 이미 혈맹이 있습니다
								return;
							}
							Connection con = null;
							Statement pstm2 = null;
							ResultSet rs2 = null;
							try {
								con = L1DatabaseFactory.getInstance()
										.getConnection();
								pstm2 = con.createStatement();
								rs2 = pstm2
										.executeQuery("SELECT `account_name`, `char_name`, `ClanID`, `Clanname` FROM `characters` WHERE Type = 0"); // 케릭터
																																					// 테이블에서
																																					// 군주만
																																					// 골라와서
								while (rs2.next()) {
									if (pc.getNetConnection()
											.getAccountName()
											.equalsIgnoreCase(
													rs2.getString("account_name"))) { // 현재
																						// 접속한
																						// 계정과
																						// 계정을
																						// 비교해서
																						// 동일하면
										if (rs2.getInt("ClanID") != 0) { // 군주의
																			// 혈맹이
																			// 있다면
											L1Clan clan = L1World
													.getInstance()
													.getClan(
															rs2.getString("Clanname")); // 군주의
																						// 혈맹으로
																						// 가입
											L1PcInstance clanMember[] = clan
													.getOnlineClanMember();
											for (int cnt = 0; cnt < clanMember.length; cnt++) { // 접속한
																								// 혈맹원에게
																								// 메세지
																								// 뿌리고
												clanMember[cnt].sendPackets(
														new S_ServerMessage(94,
																pc.getName()),
														true); // \f1%0이 혈맹의
																// 일원으로서
																// 받아들여졌습니다.
											}
											pc.setClanid(rs2.getInt("ClanID"));
											pc.setClanRank(2);
											pc.setClanname(rs2
													.getString("Clanname"));
											pc.save(); // DB에 캐릭터 정보를 기입한다
											clan.addClanMember(pc.getName(),
													pc.getClanRank(),
													pc.getLevel(),
													pc.getType(), pc.getMemo(),
													pc.getOnlineStatus(), pc);
											pc.sendPackets(
													new S_ServerMessage(
															95,
															rs2.getString("Clanname")),
													true); // \f1%0 혈맹에 가입했습니다.
															// // 메세지 보내고
											pc.getInventory().removeItem(
													useItem, 1);
											clanMember = null;
											break;
										}
									}
								}
							} catch (Exception e) {

							} finally {
								rs2.first(); // 쿼리를 처음으로 되돌리고
								SQLUtil.close(rs2, pstm2, con);
							}
							if (pc.getClanid() == 0) { // 혈맹이 있다면
								pc.sendPackets(new S_SystemMessage(
										"\\fY계정내에 군주가 없거나 혈맹이 창설되지 않았습니다."),
										true); // 메세지 보내고
							}
						}
						/**
						 * 기사 8파대,8오단 7셋 - +8 파멸의대검(412001).+8 오리하루콘단검(9),+7
						 * 마법방어투구(20011),+7 마법망토(120056),+7 티셔츠(20085),+7
						 * 마수군왕의부츠(20200),+7 암살군왕의장갑(20178),+7 발라카스의마갑주(20119)
						 * +0 고대투사가더(420003) 노예의귀걸이(21027),빛나는
						 * 고대의목걸이(20422),타이탄의벨트(20320)
						 */
						/*
						 * } else if (itemId == 500094){ //붉은빛상자
						 * pc.getInventory().consumeItem(500094, 1); // 삭제되는
						 * 아이템과 수량 노확인생성템(pc, 500007, 1, 0); 노확인생성템(pc, 5000500,
						 * 8, 0); //룸티스강화줌서
						 */} else if (itemId == 600220) { // 붉은빛상자
						pc.getInventory().consumeItem(600220, 1); // 삭제되는 아이템과
																	// 수량
						노확인생성템(pc, 500007, 1, 0);
						노확인생성템(pc, 5000500, 8, 0); // 룸티스강화줌서
					} else if (itemId == 600219) { // 보라상자
						pc.getInventory().consumeItem(600219, 1); // 삭제되는 아이템과
																	// 수량
						노확인생성템(pc, 500009, 1, 0);
						노확인생성템(pc, 5000500, 8, 0); // 룸티스강화줌서
					} else if (itemId == 500095) { // 푸른빛상자
						pc.getInventory().consumeItem(500095, 1); // 삭제되는 아이템과
																	// 수량
						노확인생성템(pc, 500008, 1, 0);
						노확인생성템(pc, 5000500, 8, 0); // 룸티스강화줌서
					} else if (itemId == 500100) { // 검은상자
						pc.getInventory().consumeItem(500100, 1); // 삭제되는 아이템과
																	// 수량
						노확인생성템(pc, 500010, 1, 0);
						노확인생성템(pc, 5000500, 8, 0); // 룸티스강화줌서

						/*
						 * } else if (itemId == 500096){ //보랏빛상자
						 * pc.getInventory().consumeItem(500096, 1); // 삭제되는
						 * 아이템과 수량 노확인생성템(pc, 500009, 1, 0); 노확인생성템(pc, 5000500,
						 * 8, 0); //룸티스강화줌서
						 *//**
						 * 525109 신기한 회복 반지 525110 신기한 집중 반지 525111 신기한 체력 반지
						 * 525112 신기한 마나 반지 525113 신기한 마법저항 반지
						 * 
						 * 625109 신기한 특제 회복 반지 625110 신기한 특제 집중 반지 625111 신기한 특제
						 * 체력 반지 625112 신기한 특제 마나 반지 625113 신기한 특제 마법저항 반지
						 **/

					} else if (itemId == 600094) { // 마법저항
						pc.getInventory().consumeItem(600094, 1); // 삭제되는 아이템과
																	// 수량
						int ran = _random.nextInt(100) + 1;
						if (ran <= 5) {
							createNewItem2(pc, 625113, 1, 0); // 룸티스강화줌서
						} else {
							createNewItem2(pc, 525113, 1, 0); // 룸티스강화줌서
						}
						createNewItem2(pc, 530040, 8, 0); // 룸티스강화줌서
					} else if (itemId == 600095) { // 마나
						pc.getInventory().consumeItem(600095, 1); // 삭제되는 아이템과
																	// 수량
						int ran = _random.nextInt(100) + 1;
						if (ran <= 5) {
							createNewItem2(pc, 625112, 1, 0); // 룸티스강화줌서
						} else {
							createNewItem2(pc, 525112, 1, 0); // 룸티스강화줌서
						}
						createNewItem2(pc, 530040, 8, 0); // 룸티스강화줌서
					} else if (itemId == 600096) { // 체력
						pc.getInventory().consumeItem(600096, 1); // 삭제되는 아이템과
																	// 수량
						int ran = _random.nextInt(100) + 1;
						if (ran <= 5) {
							createNewItem2(pc, 625111, 1, 0); // 룸티스강화줌서
						} else {
							createNewItem2(pc, 525111, 1, 0); // 룸티스강화줌서
						}
						createNewItem2(pc, 530040, 8, 0); // 룸티스강화줌서
					} else if (itemId == 600097) { // 회복
						pc.getInventory().consumeItem(600097, 1); // 삭제되는 아이템과
																	// 수량
						int ran = _random.nextInt(100) + 1;
						if (ran <= 5) {
							createNewItem2(pc, 625109, 1, 0); // 룸티스강화줌서
						} else {
							createNewItem2(pc, 525109, 1, 0); // 룸티스강화줌서
						}
						createNewItem2(pc, 530040, 8, 0); // 룸티스강화줌서
					} else if (itemId == 600098) { // 집중
						pc.getInventory().consumeItem(600098, 1); // 삭제되는 아이템과
																	// 수량
						int ran = _random.nextInt(100) + 1;
						if (ran <= 5) {
							createNewItem2(pc, 625110, 1, 0); // 룸티스강화줌서
						} else {
							createNewItem2(pc, 525110, 1, 0); // 룸티스강화줌서
						}
						createNewItem2(pc, 530040, 8, 0); // 룸티스강화줌서

					} else if (itemId == 500097) { // 붉은빛패키지
						노확인생성템(pc, 500094, 4, 0);
						pc.getInventory().consumeItem(500097, 1); // 삭제되는 아이템과
																	// 수량

					} else if (itemId == 500098) { // 푸른빛패키지
						노확인생성템(pc, 500095, 4, 0);
						pc.getInventory().consumeItem(500098, 1); // 삭제되는 아이템과
																	// 수량

					} else if (itemId == 500099) { // 보랏빛패키지
						노확인생성템(pc, 500096, 4, 0);
						pc.getInventory().consumeItem(500099, 1); // 삭제되는 아이템과
																	// 수량
					} else if (itemId == 500101) { // 검은빛패키지
						노확인생성템(pc, 500100, 4, 0);
						pc.getInventory().consumeItem(500101, 1); // 삭제되는 아이템과 수량
					} else if (itemId == 7001) {
						if (pc.getInventory().checkItem(7001, 1)) {
							pc.getInventory().consumeItem(7001, 1);
							if (pc.isKnight() || pc.isDragonknight()) {
								createNewItem2(pc, 61, 1, 4); // 진명황의 집행검
							}
							if (pc.isCrown()) {
								createNewItem2(pc, 114, 1, 8); // 전설군주 검
							}
							if (pc.isWizard()) {
								createNewItem2(pc, 134, 1, 4); // 수정 결정체 지팡이
							}
							if (pc.isElf()) {
								createNewItem2(pc, 205, 1, 8); // 살천의 활
							}
							if (pc.isDarkelf()) {
								createNewItem2(pc, 86, 1, 4); // 붉은 그림자의 이도류
							}
							if (pc.isIllusionist()) {
								createNewItem2(pc, 450013, 1, 8); // 챔피언 키링크
							}
						}
						/** 아이템 상자 (방어구) **/
					} else if (itemId == 7002) {
						if (pc.getInventory().checkItem(7002, 1)) {
							pc.getInventory().consumeItem(7002, 1);
							if (pc.isKnight()) {
								createNewItem2(pc, 120011, 1, 7); // 마법 방어 투구(축)
								createNewItem2(pc, 20048, 1, 5); // 혼돈의 투구
								createNewItem2(pc, 20078, 1, 7); // 혼돈의 망토
								createNewItem2(pc, 21028, 1, 7); // 힘의 티셔츠
								createNewItem2(pc, 425106, 1, 7); // 스턴 내성의 티셔츠
								createNewItem2(pc, 425108, 1, 7); // 마법 저항의 티셔츠
								createNewItem2(pc, 20200, 1, 7); // 베레스의 부츠
								createNewItem2(pc, 20183, 1, 7); // 암살군왕의 장갑
								createNewItem2(pc, 420002, 1, 5); // 크리스탈 가더
								createNewItem(pc, 420104, 1, 7); // 안타라스의 완력
							}
							if (pc.isCrown()) {
								createNewItem2(pc, 120011, 1, 7); // 마법 방어 투구(축)
								createNewItem2(pc, 20048, 1, 5); // 혼돈의 투구
								createNewItem2(pc, 20078, 1, 7); // 혼돈의 망토
								createNewItem2(pc, 21028, 1, 7); // 힘의 티셔츠
								createNewItem2(pc, 425106, 1, 7); // 스턴 내성의 티셔츠
								createNewItem2(pc, 425108, 1, 7); // 마법 저항의 티셔츠
								createNewItem2(pc, 20178, 1, 7); // 베레스의 부츠
								createNewItem2(pc, 20183, 1, 7); // 암살군왕의 장갑
								createNewItem2(pc, 420002, 1, 5); // 크리스탈 가더
								createNewItem2(pc, 420105, 1, 7); // 안타라스의 완력
							}
							if (pc.isWizard()) {
								createNewItem2(pc, 120011, 1, 7); // 마법 방어 투구(축)
								createNewItem2(pc, 20048, 1, 5); // 혼돈의 투구
								createNewItem2(pc, 20078, 1, 7); // 혼돈의 망토
								createNewItem2(pc, 21028, 1, 7); // 힘의 티셔츠
								createNewItem2(pc, 425106, 1, 7); // 스턴 내성의 티셔츠
								createNewItem2(pc, 425108, 1, 7); // 마법 저항의 티셔츠
								createNewItem2(pc, 20218, 1, 7); // 베레스의 부츠
								createNewItem2(pc, 20183, 1, 7); // 암살군왕의 장갑
								createNewItem2(pc, 420002, 1, 5); // 크리스탈 가더
								createNewItem2(pc, 420107, 1, 7); // 안타라스의 완력
							}
							if (pc.isElf()) {
								createNewItem2(pc, 120011, 1, 7); // 마법 방어 투구(축)
								createNewItem2(pc, 20048, 1, 5); // 혼돈의 투구
								createNewItem2(pc, 20078, 1, 7); // 혼돈의 망토
								createNewItem2(pc, 21028, 1, 7); // 힘의 티셔츠
								createNewItem2(pc, 425106, 1, 7); // 스턴 내성의 티셔츠
								createNewItem2(pc, 425108, 1, 7); // 마법 저항의 티셔츠
								createNewItem2(pc, 20216, 1, 7); // 베레스의 부츠
								createNewItem2(pc, 20190, 1, 7); // 암살군왕의 장갑
								createNewItem2(pc, 420002, 1, 5); // 크리스탈 가더
								createNewItem2(pc, 420106, 1, 7); // 안타라스의 완력
							}
							if (pc.isDarkelf()) {
								createNewItem2(pc, 120011, 1, 7); // 마법 방어 투구(축)
								createNewItem2(pc, 20048, 1, 5); // 혼돈의 투구
								createNewItem2(pc, 20078, 1, 7); // 혼돈의 망토
								createNewItem2(pc, 21028, 1, 7); // 힘의 티셔츠
								createNewItem2(pc, 425106, 1, 7); // 스턴 내성의 티셔츠
								createNewItem2(pc, 425108, 1, 7); // 마법 저항의 티셔츠
								createNewItem2(pc, 20200, 1, 7); // 베레스의 부츠
								createNewItem2(pc, 20183, 1, 7); // 암살군왕의 장갑
								createNewItem2(pc, 420002, 1, 5); // 크리스탈 가더
								createNewItem2(pc, 420106, 1, 7); // 안타라스의 완력
							}
							if (pc.isDragonknight()) {
								createNewItem2(pc, 120011, 1, 7); // 마법 방어 투구(축)
								createNewItem2(pc, 20048, 1, 5); // 혼돈의 투구
								createNewItem2(pc, 20078, 1, 7); // 혼돈의 망토
								createNewItem2(pc, 21028, 1, 7); // 힘의 티셔츠
								createNewItem2(pc, 425106, 1, 7); // 스턴 내성의 티셔츠
								createNewItem2(pc, 425108, 1, 7); // 마법 저항의 티셔츠
								createNewItem2(pc, 20200, 1, 7); // 베레스의 부츠
								createNewItem2(pc, 20183, 1, 7); // 죽비
								createNewItem2(pc, 420002, 1, 5); // 크리스탈 가더
								createNewItem2(pc, 420105, 1, 7); // 안타라스의 완력
							}
							if (pc.isIllusionist()) {
								createNewItem2(pc, 120011, 1, 7); // 마법 방어 투구(축)
								createNewItem2(pc, 20048, 1, 5); // 혼돈의 투구
								createNewItem2(pc, 20078, 1, 7); // 혼돈의 망토
								createNewItem2(pc, 21028, 1, 7); // 힘의 티셔츠
								createNewItem2(pc, 425106, 1, 7); // 스턴 내성의 티셔츠
								createNewItem2(pc, 425108, 1, 7); // 마법 저항의 티셔츠
								createNewItem2(pc, 20200, 1, 7); // 베레스의 부츠
								createNewItem2(pc, 20183, 1, 7); // 암살군왕의 장갑
								createNewItem2(pc, 420002, 1, 5); // 크리스탈 가더
								createNewItem2(pc, 420107, 1, 7); // 안타라스의 완력
							}
						}

						/** 아이템 상자 (악세) **/
					} else if (itemId == 7003) {
						if (pc.getInventory().checkItem(7003, 1)) {
							pc.getInventory().consumeItem(7003, 1);
							if (pc.isKnight()) {
								createNewItem2(pc, 101010, 1, 0); // 2차목걸이
								createNewItem2(pc, 202020, 1, 0); // 2차반지
								createNewItem2(pc, 303030, 1, 0); // 2차반지
								createNewItem2(pc, 404040, 1, 0); // 2차벨트
								createNewItem2(pc, 202020, 1, 0); // 2차귀걸이

							}
							if (pc.isCrown()) {
								createNewItem2(pc, 101010, 1, 0); // 2차목걸이
								createNewItem2(pc, 202020, 1, 0); // 2차반지
								createNewItem2(pc, 303030, 1, 0); // 2차반지
								createNewItem2(pc, 404040, 1, 0); // 2차벨트
								createNewItem2(pc, 202020, 1, 0); // 2차귀걸이
							}
							if (pc.isWizard()) {
								createNewItem2(pc, 101010, 1, 0); // 2차목걸이
								createNewItem2(pc, 202020, 1, 0); // 2차반지
								createNewItem2(pc, 303030, 1, 0); // 2차반지
								createNewItem2(pc, 404040, 1, 0); // 2차벨트
								createNewItem2(pc, 202020, 1, 0); // 2차귀걸이
							}
							if (pc.isElf()) {
								createNewItem2(pc, 101010, 1, 0); // 2차목걸이
								createNewItem2(pc, 202020, 1, 0); // 2차반지
								createNewItem2(pc, 303030, 1, 0); // 2차반지
								createNewItem2(pc, 404040, 1, 0); // 2차벨트
								createNewItem2(pc, 202020, 1, 0); // 2차귀걸이
							}
							if (pc.isDarkelf()) {
								createNewItem2(pc, 101010, 1, 0); // 2차목걸이
								createNewItem2(pc, 202020, 1, 0); // 2차반지
								createNewItem2(pc, 303030, 1, 0); // 2차반지
								createNewItem2(pc, 404040, 1, 0); // 2차벨트
								createNewItem2(pc, 202020, 1, 0); // 2차귀걸이
							}
							if (pc.isDragonknight()) {
								createNewItem2(pc, 101010, 1, 0); // 2차목걸이
								createNewItem2(pc, 202020, 1, 0); // 2차반지
								createNewItem2(pc, 303030, 1, 0); // 2차반지
								createNewItem2(pc, 404040, 1, 0); // 2차벨트
								createNewItem2(pc, 202020, 1, 0); // 2차귀걸이
							}
							if (pc.isIllusionist()) {
								createNewItem2(pc, 101010, 1, 0); // 2차목걸이
								createNewItem2(pc, 202020, 1, 0); // 2차반지
								createNewItem2(pc, 303030, 1, 0); // 2차반지
								createNewItem2(pc, 404040, 1, 0); // 2차벨트
								createNewItem2(pc, 202020, 1, 0); // 2차귀걸이
							}
						}

						/** 오만이동책 **/
						// by사부

					} else if (itemId == 5000137) {
						if (pc.getInventory().checkItem(5000137, 1)) {
							int ran = _random.nextInt(100) + 1;
							pc.getInventory().consumeItem(5000137, 1);
							if (ran == 77) {
								createNewItem2(pc, 6000032, 1, 0); // 100추타
							} else {
								int[] 템번호 = { 40308, 41159, 140074, 140087,
										40074, 40087, 5000031 };
								int k3 = _random.nextInt(7);
								int k = _random.nextInt(9);
								int k2 = _random.nextInt(5);
								switch (k3) {
								case 0:
									int[] 갯수 = { 1000000, 1500000, 2000000,
											2500000, 3000000, 3500000, 4000000,
											4500000, 5000000 };
									createNewItem2(pc, 템번호[k3], 갯수[k], 0);
									갯수 = null;
									break;
								case 1:
									int[] 갯수1 = { 100, 150, 200, 250, 300, 350,
											400, 450, 500 };
									createNewItem2(pc, 템번호[k3], 갯수1[k], 0);
									갯수1 = null;
									break;
								case 2:
								case 3:
								case 4:
								case 5:
								case 6:
									int[] 갯수2 = { 1, 2, 3, 4, 5 };
									createNewItem2(pc, 템번호[k3], 갯수2[k2], 0);
									갯수2 = null;
									break;
								}
								템번호 = null;
							}
						}
					} else if (itemId == 40079) { // 일반 귀환 스크롤
						if (pc.getMap().isEscapable() || pc.isGm()) {
							if (pc.Sabutelok()) {
								if(pc.getMapId() >= 12852 && pc.getMapId() <= 12862){
									pc.setTelType(pc.getMapId());
									pc.sendPackets(new S_SabuTell(pc), true);
									pc.getInventory().removeItem(useItem, 1);
								}else{
								pc.setTelType(5);
								pc.sendPackets(new S_SabuTell(pc), true);
								pc.getInventory().removeItem(useItem, 1);
								}
							}
						} else {
							pc.sendPackets(new S_ServerMessage(647), true);
						}
					} else if (itemId == 140056) {// 정보
						pc.sendPackets(new S_UserStatus(pc, 1), true);
					} else if (itemId == 40081 || itemId == 40095) { // 기란 귀환 스크롤
						if (pc.getMap().isEscapable() || pc.isGm()) {
							
							if (pc.Sabutelok()) {
								if(pc.getMapId() >= 12852 && pc.getMapId() <= 12862){
									pc.setTelType(pc.getMapId());
									pc.sendPackets(new S_SabuTell(pc), true);
									pc.getInventory().removeItem(useItem, 1);
								}else{
									pc.setTelType(11);
									pc.sendPackets(new S_SabuTell(pc), true);
									pc.getInventory().removeItem(useItem, 1);
								}
							}
						} else {
							pc.sendPackets(new S_ServerMessage(647), true);
						}
					} else if (itemId == 41159) { // 깃털
						if (pc.getMap().isEscapable() || pc.isGm()) {
							if (pc.Sabutelok()) {
								if(pc.getMapId() >= 12852 && pc.getMapId() <= 12862){
									pc.setTelType(pc.getMapId());
									pc.sendPackets(new S_SabuTell(pc), true);
									pc.getInventory().removeItem(useItem, 1);
								}else{
								pc.setTelType(77);
								pc.sendPackets(new S_SabuTell(pc), true);
								pc.getInventory().removeItem(useItem, 1);
								}
							}
						} else {
							pc.sendPackets(new S_ServerMessage(647), true);
						}
					} else if (itemId == 40124) { // 혈맹 귀환 스크롤
						if (pc.getMap().isEscapable() || pc.isGm()) {
							int castle_id = 0;
							int house_id = 0;
							
							if(pc.getMapId() >= 12852 && pc.getMapId() <= 12862){
								pc.setTelType(pc.getMapId());
								pc.sendPackets(new S_SabuTell(pc), true);
								pc.getInventory().removeItem(useItem, 1);
								return;
							}
							
							if (pc.getClanid() != 0) { // 크란 소속
								L1Clan clan = L1World.getInstance().getClan(pc.getClanname());
								if (clan != null) {
									castle_id = clan.getCastleId();
									house_id = clan.getHouseId();
								}
							}
							if (castle_id != 0) { // 성주 크란원
								if (pc.getMap().isEscapable() || pc.isGm()) {
									if (pc.Sabutelok()) {
										pc.setTelType(2);
										pc.sendPackets(new S_SabuTell(pc), true);
										pc.getInventory().removeItem(useItem, 1);
									}
								} else {
									pc.sendPackets(new S_ServerMessage(647), true);
								}
							} else if (house_id != 0) { // 아지트 소유 크란원
								if (pc.getMap().isEscapable() || pc.isGm()) {
									if (pc.Sabutelok()) {
										pc.setTelType(3);
										pc.sendPackets(new S_SabuTell(pc), true);
										pc.getInventory().removeItem(useItem, 1);
									}
								} else {
									pc.sendPackets(new S_ServerMessage(647), true);
								}
							} else {
								if (pc.Sabutelok()) {
									pc.setTelType(6);
									pc.sendPackets(new S_SabuTell(pc), true);
									pc.getInventory().removeItem(useItem, 1);
								}
							}
						} else {
							pc.sendPackets(new S_ServerMessage(647), true);
						}

					} else if(itemId == 391019) { //아이템 아이디 //상자 by.황교수
						/** 기르타스의 무기 상자 패턴 정리 */
						/** 랜덤 확율 100으로 계산 */
						int Random = _random.nextInt(1000) + 1 ;
						int Itemid = 0;
						/** 아이템 있는지 체크 */
						if (pc.getInventory().checkItem(391019, 1)) { //체크하는 아이템
							pc.getInventory().consumeItem(391019, 1);//없어질 아이템
							/** 클래스별로 획득 아이템 변경 */
							if (pc.isKnight()) Itemid = 90086;
							if (pc.isDragonknight()) Itemid = 90091;
							if (pc.isCrown()) Itemid = 90092;
							if (pc.isWizard()) Itemid = 90085;
							if (pc.isElf()) Itemid = 90088;
							if (pc.isIllusionist()) Itemid = 90089;
							if (pc.isWarrior())	Itemid = 90090;
							if (pc.isDarkelf()) Itemid =90087;
							/** 아이템 인스 만들어서 그정보 보관 */
							L1ItemInstance Itme;
							/** 확율 계산 전체적으로 통합
							 확율 계산방법 1000 의 수를 뽑음 그수를
							 확율로 계산 수가 높은지 체크 높은수부터 낮은수로 계산
							 예제 : Random >= 800 랜덤수가 800보다 높은지 체크
							 높다면 그아이템 생성 멍청한 코봉이를 위해서 설명첨주 */
							if(Random >= 980) { // 0.9 %
								Itme = createNewItem(pc, Itemid, 9, 1);
							} else if (Random >= 800) { //4%
								Itme = createNewItem(pc, Itemid, 8, 1);
							} else if (Random >= 500) {//2.9%
								Itme = createNewItem(pc, Itemid, 7, 1);
							/** 남은 아이템은 모두 6으로 계산해서 정리 */
							} else Itme = createNewItem(pc, Itemid, 6, 1);
							/** 획득 멘트 등록 */
							if(Itme != null)
								pc.sendPackets(new S_ServerMessage(403, Itme.getLogName()), true);
						}
			
					/** 아이템 상자 (무기) **/
					} else if (itemId == 391019) {
						if (pc.getInventory().checkItem(391019, 1)) {
							pc.getInventory().consumeItem(391019, 1);
							if (pc.isKnight() ) {
								createNewItem2(pc, 90086, 1 ,7); // 기르타스의 절대검
							}
							if ( pc.isDragonknight()) {
								createNewItem2(pc, 90091, 1, 7); // 기르타스의 절풍검
							}
							if (pc.isCrown()) {
								createNewItem2(pc, 90092, 1, 7); // 전설군주 검
							}
							if (pc.isWizard()) {
								createNewItem2(pc, 90085, 1, 7); // 기르타스의 지팡이
							}
							if (pc.isElf()) {
								createNewItem2(pc, 90088, 1, 7); // 기르타스의 척살궁
							}
							if (pc.isDarkelf()) {
								createNewItem2(pc, 90087, 1, 7); // 기르타스의 파멸도
							}
							if (pc.isIllusionist()) {
								createNewItem2(pc, 90089, 1, 7); // 기르타스의 키링크
							
							}
							if (pc.isWarrior()) {
								createNewItem2(pc, 90090, 1, 7); // 기르타스의 섬광도
								
							}
						}
						/** 아이템 상자 (무기) **/
					} else if (itemId == 7011) {
						if (pc.getInventory().checkItem(7011, 1)) {
							pc.getInventory().consumeItem(7011, 1);
							if (pc.isKnight() || pc.isDragonknight()) {
								createNewItem2(pc, 61, 1, 7); // 진명황의 집행검
							}
							if (pc.isCrown()) {
								createNewItem2(pc, 114, 1, 10); // 전설군주 검
							}
							if (pc.isWizard()) {
								createNewItem2(pc, 134, 1, 7); // 수정 결정체 지팡이
							}
							if (pc.isElf()) {
								createNewItem2(pc, 205, 1, 9); // 살천의 활
							}
							if (pc.isDarkelf()) {
								createNewItem2(pc, 86, 1, 7); // 붉은 그림자의 이도류
							}
							if (pc.isIllusionist()) {
								createNewItem2(pc, 450013, 1, 9); // 챔피언 키링크
							}
						}
						/** 아이템 상자 (방어구) **/
					} else if (itemId == 7022) {
						씨발(pc);
					} else if (itemId == 447091) {// 지원상자
						if (pc.getInventory().checkItem(447091, 1)) { // 체크 되는
																		// 아이템과
																		// 수량
							pc.getInventory().consumeItem(447091, 1); // 삭제되는
																		// 아이템과
																		// 수량
							createNewItem2(pc, 425111, 1, 1); // 순백체력반지
							createNewItem2(pc, 425111, 1, 1); // 순백체력반지
							createNewItem2(pc, 20317, 1, 0); // 순백체력반지
							if (pc.isKnight()) {
								createNewItem2(pc, 20011, 1, 6); // 마투
								createNewItem2(pc, 20056, 1, 6); // 마망
								createNewItem2(pc, 20194, 1, 6); // 강부
								createNewItem2(pc, 20095, 1, 0); // 고판
								createNewItem2(pc, 62, 1, 8); // 무양
								createNewItem2(pc, 20187, 1, 6); // 파글
								createNewItem2(pc, 425106, 1, 6); // 스티
								createNewItem2(pc, 20422, 1, 0); // 빛고목
								L1World.getInstance()
										.broadcastPacketToAll(
												new S_SystemMessage(
														"\\fU1:1 대질 및 대화 또는 소환은 절대 해드리지 않습니다."),
												true);
								pc.sendPackets(
										new S_SystemMessage(
												"\\fW#####################################################"),
										true);
								pc.sendPackets(new S_SystemMessage(
										"1차 지원아이템 지급 완료 열심히활동부탁드려요^^."), true);
								pc.sendPackets(
										new S_SystemMessage(
												"\\fW#####################################################"),
										true);
							}
							if (pc.isDragonknight()) {
								createNewItem2(pc, 20011, 1, 6); // 마투
								createNewItem2(pc, 20056, 1, 6); // 마망
								createNewItem2(pc, 20194, 1, 6); // 강부
								createNewItem2(pc, 21060, 1, 8); // 고판
								createNewItem2(pc, 54, 1, 9); // 무양
								createNewItem2(pc, 20187, 1, 6); // 파글
								createNewItem2(pc, 425106, 1, 6); // 스티
								createNewItem2(pc, 20422, 1, 0); // 빛고목
								L1World.getInstance()
										.broadcastPacketToAll(
												new S_SystemMessage(
														"\\fU건의 이외에 문의 불만들은 편지로 답변드리지 않습니다."),
												true);
								pc.sendPackets(
										new S_SystemMessage(
												"\\fW#####################################################"),
										true);
								pc.sendPackets(new S_SystemMessage(
										"1차 지원아이템 지급 완료 열심히활동부탁드려요^^."), true);
								pc.sendPackets(
										new S_SystemMessage(
												"\\fW#####################################################"),
										true);
							}
							if (pc.isCrown()) {
								createNewItem2(pc, 20011, 1, 6); // 마투
								createNewItem2(pc, 20056, 1, 6); // 마망
								createNewItem2(pc, 20194, 1, 6); // 강부
								createNewItem2(pc, 20094, 1, 0); // 고판
								createNewItem2(pc, 62, 1, 8); // 무양
								createNewItem2(pc, 20187, 1, 6); // 파글
								createNewItem2(pc, 425106, 1, 6); // 스티
								createNewItem2(pc, 20422, 1, 0); // 빛고목
								L1World.getInstance()
										.broadcastPacketToAll(
												new S_SystemMessage(
														"\\fU서버정보 게시판을 안읽고 당하는 불이익에 대해서는 절대 책임 지지않습니다."),
												true);
								pc.sendPackets(
										new S_SystemMessage(
												"\\fW#####################################################"),
										true);
								pc.sendPackets(new S_SystemMessage(
										"1차 지원아이템 지급 완료 열심히활동부탁드려요^^."), true);
								pc.sendPackets(
										new S_SystemMessage(
												"\\fW#####################################################"),
										true);
							}
							if (pc.isWizard()) {
								createNewItem2(pc, 20018, 1, 5); // 메르모
								createNewItem2(pc, 20025, 1, 5); // 발터모
								createNewItem2(pc, 20218, 1, 5); // 흑샌
								createNewItem2(pc, 20187, 1, 6); // 파글
								createNewItem2(pc, 20093, 1, 0); // 고판
								createNewItem2(pc, 415013, 1, 8); // 무양
								createNewItem2(pc, 126, 1, 8); // 무양
								createNewItem2(pc, 21031, 1, 6); // 파글
								createNewItem2(pc, 20253, 1, 0); // 빛고목
								L1World.getInstance()
										.broadcastPacketToAll(
												new S_SystemMessage(
														"\\fU제작 아이템관련은 서버정보 게시판을 필독해주세요."),
												true);
								pc.sendPackets(
										new S_SystemMessage(
												"\\fW#####################################################"),
										true);
								pc.sendPackets(new S_SystemMessage(
										"1차 지원아이템 지급 완료 열심히활동부탁드려요^^."), true);
								pc.sendPackets(
										new S_SystemMessage(
												"\\fW#####################################################"),
										true);
							}
							if (pc.isIllusionist()) {
								createNewItem2(pc, 20018, 1, 5); // 메르모
								createNewItem2(pc, 20025, 1, 5); // 발터모
								createNewItem2(pc, 20218, 1, 5); // 흑샌
								createNewItem2(pc, 20187, 1, 6); // 파글
								createNewItem2(pc, 20093, 1, 0); // 고판
								createNewItem2(pc, 415013, 1, 8); // 무양
								createNewItem2(pc, 126, 1, 8); // 무양
								createNewItem2(pc, 21031, 1, 6); // 파글
								createNewItem2(pc, 20253, 1, 0); // 빛고목
								L1World.getInstance()
										.broadcastPacketToAll(
												new S_SystemMessage(
														"\\fU공성은 최초시작후 2일마다 1시간씩 진행 됩니다."),
												true);
								pc.sendPackets(
										new S_SystemMessage(
												"\\fW#####################################################"),
										true);
								pc.sendPackets(new S_SystemMessage(
										"1차 지원아이템 지급 완료 열심히활동부탁드려요^^."), true);
								pc.sendPackets(
										new S_SystemMessage(
												"\\fW#####################################################"),
										true);
							}
							if (pc.isElf()) {
								createNewItem2(pc, 20011, 1, 6); // 마투
								createNewItem2(pc, 20056, 1, 6); // 마망
								createNewItem2(pc, 20194, 1, 6); // 강부
								createNewItem2(pc, 21060, 1, 8); // 고판
								createNewItem2(pc, 415012, 1, 9); // 무양
								createNewItem2(pc, 20191, 1, 6); // 파글
								createNewItem2(pc, 425106, 1, 6); // 스티
								createNewItem2(pc, 120256, 1, 0); // 빛고목
								L1World.getInstance()
										.broadcastPacketToAll(
												new S_SystemMessage(
														"\\fU운영자 비방, 서버 비방, 스핵등의 프로그램 사용시 경고없이 강퇴입니다."),
												true);
								pc.sendPackets(
										new S_SystemMessage(
												"\\fW#####################################################"),
										true);
								pc.sendPackets(new S_SystemMessage(
										"1차 지원아이템 지급 완료 열심히활동부탁드려요^^."), true);
								pc.sendPackets(
										new S_SystemMessage(
												"\\fW#####################################################"),
										true);
							}
							if (pc.isDarkelf()) {
								createNewItem2(pc, 20011, 1, 6); // 마투
								createNewItem2(pc, 20056, 1, 6); // 마망
								createNewItem2(pc, 20195, 1, 6); // 강부
								createNewItem2(pc, 20092, 1, 0); // 고판
								createNewItem2(pc, 81, 1, 9); // 무양
								createNewItem2(pc, 20187, 1, 6); // 파글
								createNewItem2(pc, 425106, 1, 6); // 스티
								createNewItem2(pc, 20422, 1, 0); // 빛고목
								L1World.getInstance()
										.broadcastPacketToAll(
												new S_SystemMessage(
														"\\fU<< 서버 정보 게시판 >>은 꼭 확인하세요."),
												true);
								pc.sendPackets(
										new S_SystemMessage(
												"\\fW#####################################################"),
										true);
								pc.sendPackets(new S_SystemMessage(
										"1차 지원아이템 지급 완료 열심히활동부탁드려요^^."), true);
								pc.sendPackets(
										new S_SystemMessage(
												"\\fW#####################################################"),
										true);
							}
						}
						// *******************지원상자(2단계)************************//
					} else if (itemId == 447090) {// 지원상자
						if (pc.getInventory().checkItem(447090, 1)) { // 체크 되는
																		// 아이템과
																		// 수량
							pc.getInventory().consumeItem(447090, 1); // 삭제되는
																		// 아이템과
																		// 수량

							if (pc.isKnight()) {
								createNewItem2(pc, 425111, 1, 2); // 순백체력반지
								createNewItem2(pc, 425111, 1, 2); // 순백체력반지
								createNewItem2(pc, 20320, 1, 0); // 순백체력반지
								createNewItem2(pc, 20011, 1, 7); // 마투
								createNewItem2(pc, 20056, 1, 7); // 마망
								createNewItem2(pc, 20194, 1, 7); // 강부
								createNewItem2(pc, 20133, 1, 1); // 고판
								createNewItem2(pc, 62, 1, 9); // 무양
								createNewItem2(pc, 20187, 1, 7); // 파글
								createNewItem2(pc, 425106, 1, 7); // 스티
								createNewItem2(pc, 20422, 1, 0); // 빛고목

								L1World.getInstance()
										.broadcastPacketToAll(
												new S_SystemMessage(
														"\\fU1:1 대질 및 대화 또는 소환은 절대 해드리지 않습니다."),
												true);
								pc.sendPackets(
										new S_SystemMessage(
												"\\fW#####################################################"),
										true);
								pc.sendPackets(new S_SystemMessage(
										"2차 지원아이템 지급 완료 열심히 활동부탁드려요^^ ."), true);
								pc.sendPackets(
										new S_SystemMessage(
												"\\fW#####################################################"),
										true);
							}
							if (pc.isDragonknight()) {
								createNewItem2(pc, 425111, 1, 2); // 순백체력반지
								createNewItem2(pc, 425111, 1, 2); // 순백체력반지
								createNewItem2(pc, 20320, 1, 0); // 순백체력반지
								createNewItem2(pc, 20011, 1, 7); // 마투
								createNewItem2(pc, 20056, 1, 7); // 마망
								createNewItem2(pc, 20194, 1, 7); // 강부
								createNewItem2(pc, 21060, 1, 9); // 고판
								createNewItem2(pc, 58, 1, 7); // 무양
								createNewItem2(pc, 20187, 1, 7); // 파글
								createNewItem2(pc, 425106, 1, 7); // 스티
								createNewItem2(pc, 20422, 1, 0); // 빛고목
								L1World.getInstance()
										.broadcastPacketToAll(
												new S_SystemMessage(
														"\\fU건의 이외에 문의 불만들은 편지로 답변드리지 않습니다."),
												true);
								pc.sendPackets(
										new S_SystemMessage(
												"\\fW#####################################################"),
										true);
								pc.sendPackets(new S_SystemMessage(
										"2차 지원아이템 지급 완료 열심히 활동부탁드려요^^ ."), true);
								pc.sendPackets(
										new S_SystemMessage(
												"\\fW#####################################################"),
										true);
							}
							if (pc.isCrown()) {
								createNewItem2(pc, 425111, 1, 2); // 순백체력반지
								createNewItem2(pc, 425111, 1, 2); // 순백체력반지
								createNewItem2(pc, 20320, 1, 0); // 순백체력반지
								createNewItem2(pc, 20011, 1, 7); // 마투
								createNewItem2(pc, 20056, 1, 7); // 마망
								createNewItem2(pc, 20194, 1, 7); // 강부
								createNewItem2(pc, 21060, 1, 9); // 고판
								createNewItem2(pc, 58, 1, 7); // 무양
								createNewItem2(pc, 20187, 1, 7); // 파글
								createNewItem2(pc, 425106, 1, 7); // 스티
								createNewItem2(pc, 20422, 1, 0); // 빛고목
								L1World.getInstance()
										.broadcastPacketToAll(
												new S_SystemMessage(
														"\\fU서버정보 게시판을 안읽고 당하는 불이익에 대해서는 절대 책임 지지않습니다."),
												true);
								pc.sendPackets(
										new S_SystemMessage(
												"\\fW#####################################################"),
										true);
								pc.sendPackets(new S_SystemMessage(
										"2차 지원아이템 지급 완료 열심히 활동부탁드려요^^ ."), true);
								pc.sendPackets(
										new S_SystemMessage(
												"\\fW#####################################################"),
										true);
							}
							if (pc.isWizard()) {
								createNewItem2(pc, 420008, 1, 0); // 순백체력반지
								createNewItem2(pc, 420008, 1, 0); // 순백체력반지
								createNewItem2(pc, 20317, 1, 0); // 순백체력반지
								createNewItem2(pc, 20018, 1, 6); // 메르모
								createNewItem2(pc, 20025, 1, 6); // 발터모
								createNewItem2(pc, 20218, 1, 6); // 흑샌
								createNewItem2(pc, 20187, 1, 7); // 파글
								createNewItem2(pc, 20093, 1, 0); // 고판
								createNewItem2(pc, 20257, 1, 0); // 블목
								createNewItem2(pc, 415013, 1, 9); // 무양
								createNewItem2(pc, 126, 1, 9); // 무양
								createNewItem2(pc, 21031, 1, 7); // 파글
								createNewItem2(pc, 20253, 1, 0); // 빛고목
								L1World.getInstance()
										.broadcastPacketToAll(
												new S_SystemMessage(
														"\\fU제작 아이템관련은 서버정보 게시판을 필독해주세요."),
												true);
								pc.sendPackets(
										new S_SystemMessage(
												"\\fW#####################################################"),
										true);
								pc.sendPackets(new S_SystemMessage(
										"2차 지원아이템 지급 완료 열심히 활동부탁드려요^^ ."), true);
								pc.sendPackets(
										new S_SystemMessage(
												"\\fW#####################################################"),
										true);
							}
							if (pc.isIllusionist()) {
								createNewItem2(pc, 425111, 1, 2); // 순백체력반지
								createNewItem2(pc, 425111, 1, 2); // 순백체력반지
								createNewItem2(pc, 20317, 1, 0); // 순백체력반지
								createNewItem2(pc, 20018, 1, 6); // 메르모
								createNewItem2(pc, 20025, 1, 6); // 발터모
								createNewItem2(pc, 20218, 1, 6); // 흑샌
								createNewItem2(pc, 20187, 1, 7); // 파글
								createNewItem2(pc, 20093, 1, 0); // 고판
								createNewItem2(pc, 20257, 1, 0); // 블목
								createNewItem2(pc, 415013, 1, 9); // 무양
								createNewItem2(pc, 126, 1, 9); // 무양
								createNewItem2(pc, 21031, 1, 7); // 파글
								createNewItem2(pc, 20253, 1, 0); // 빛고목
								L1World.getInstance()
										.broadcastPacketToAll(
												new S_SystemMessage(
														"\\fU공성은 최초시작후 2일마다 1시간씩 진행 됩니다."),
												true);
								pc.sendPackets(
										new S_SystemMessage(
												"\\fW#####################################################"),
										true);
								pc.sendPackets(new S_SystemMessage(
										"2차 지원아이템 지급 완료 열심히 활동부탁드려요^^ ."), true);
								pc.sendPackets(
										new S_SystemMessage(
												"\\fW#####################################################"),
										true);
							}
							if (pc.isElf()) {
								createNewItem2(pc, 425111, 1, 2); // 순백체력반지
								createNewItem2(pc, 425111, 1, 2); // 순백체력반지
								createNewItem2(pc, 20317, 1, 0); // 순백체력반지
								createNewItem2(pc, 20011, 1, 7); // 마투
								createNewItem2(pc, 20056, 1, 7); // 마망
								createNewItem2(pc, 20194, 1, 7); // 강부
								createNewItem2(pc, 21060, 1, 9); // 고판
								createNewItem2(pc, 205, 1, 8); // 무양
								createNewItem2(pc, 20191, 1, 7); // 파글
								createNewItem2(pc, 425106, 1, 7); // 스티
								createNewItem2(pc, 120256, 1, 0); // 빛고목
								L1World.getInstance()
										.broadcastPacketToAll(
												new S_SystemMessage(
														"\\fU운영자 비방, 서버 비방, 스핵등의 프로그램 사용시 경고없이 강퇴입니다."),
												true);
								pc.sendPackets(
										new S_SystemMessage(
												"\\fW#####################################################"),
										true);
								pc.sendPackets(new S_SystemMessage(
										"2차 지원아이템 지급 완료 열심히 활동부탁드려요^^ ."), true);
								pc.sendPackets(
										new S_SystemMessage(
												"\\fW#####################################################"),
										true);
							}
							if (pc.isDarkelf()) {
								createNewItem2(pc, 425111, 1, 2); // 순백체력반지
								createNewItem2(pc, 425111, 1, 2); // 순백체력반지
								createNewItem2(pc, 20317, 1, 0); // 순백체력반지
								createNewItem2(pc, 20011, 1, 7); // 마투
								createNewItem2(pc, 20056, 1, 7); // 마망
								createNewItem2(pc, 20195, 1, 7); // 강부
								createNewItem2(pc, 21060, 1, 9); // 고판
								createNewItem2(pc, 84, 1, 7); // 무양
								createNewItem2(pc, 20187, 1, 7); // 파글
								createNewItem2(pc, 425106, 1, 7); // 스티
								createNewItem2(pc, 20422, 1, 0); // 빛고목
								L1World.getInstance()
										.broadcastPacketToAll(
												new S_SystemMessage(
														"\\fU<< 서버 정보 게시판 >>은 꼭 확인하세요."),
												true);
								pc.sendPackets(
										new S_SystemMessage(
												"\\fW#####################################################"),
										true);
								pc.sendPackets(new S_SystemMessage(
										"2차 지원아이템 지급 완료 열심히 활동부탁드려요^^ ."), true);
								pc.sendPackets(
										new S_SystemMessage(
												"\\fW#####################################################"),
										true);
							}
						}
						// *******************지원상자(3단계)************************//

					} else if (itemId == 447093) {// 지원상자
						if (pc.getInventory().checkItem(447093, 1)) { // 체크 되는
																		// 아이템과
																		// 수량
							pc.getInventory().consumeItem(447093, 1); // 삭제되는
																		// 아이템과
																		// 수량
							if (pc.isKnight()) {
								createNewItem2(pc, 425111, 1, 4); // 순백체력반지
								createNewItem2(pc, 425111, 1, 4); // 순백체력반지
								createNewItem2(pc, 20320, 1, 0); // 타벨
								createNewItem2(pc, 20011, 1, 8); // 마투
								createNewItem2(pc, 20056, 1, 8); // 마망
								createNewItem2(pc, 20194, 1, 8); // 강부
								createNewItem2(pc, 20133, 1, 3); // 고판
								createNewItem2(pc, 412001, 1, 9); // 무양
								createNewItem2(pc, 20187, 1, 8); // 파글
								createNewItem2(pc, 425106, 1, 8); // 스티
								createNewItem2(pc, 20422, 1, 0); // 빛고목
								L1World.getInstance()
										.broadcastPacketToAll(
												new S_SystemMessage(
														"\\fU1:1 대질 및 대화 또는 소환은 절대 해드리지 않습니다."),
												true);
								pc.sendPackets(
										new S_SystemMessage(
												"\\fW#####################################################"),
										true);
								pc.sendPackets(new S_SystemMessage(
										"3차 지원아이템 지급 완료 열심히 활동부탁드려요^^ ."), true);
								pc.sendPackets(
										new S_SystemMessage(
												"\\fW#####################################################"),
										true);
							}
							if (pc.isDragonknight()) {
								createNewItem2(pc, 425111, 1, 4); // 순백체력반지
								createNewItem2(pc, 425111, 1, 4); // 순백체력반지
								createNewItem2(pc, 20320, 1, 0); // 순백체력반지
								createNewItem2(pc, 20011, 1, 8); // 마투
								createNewItem2(pc, 20056, 1, 8); // 마망
								createNewItem2(pc, 20194, 1, 8); // 강부
								createNewItem2(pc, 21060, 1, 9); // 고판
								createNewItem2(pc, 58, 1, 9); // 무양
								createNewItem2(pc, 20187, 1, 8); // 파글
								createNewItem2(pc, 425106, 1, 8); // 스티
								createNewItem2(pc, 420013, 1, 0); // 빛고목
								L1World.getInstance()
										.broadcastPacketToAll(
												new S_SystemMessage(
														"\\fU건의 이외에 문의 불만들은 편지로 답변드리지 않습니다."),
												true);
								pc.sendPackets(
										new S_SystemMessage(
												"\\fW#####################################################"),
										true);
								pc.sendPackets(new S_SystemMessage(
										"3차 지원아이템 지급 완료 열심히 활동부탁드려요^^ ."), true);
								pc.sendPackets(
										new S_SystemMessage(
												"\\fW#####################################################"),
										true);
							}
							if (pc.isCrown()) {
								createNewItem2(pc, 420105, 1, 8); // 파푸완력
								createNewItem2(pc, 20049, 1, 8); // 금날
								createNewItem2(pc, 20200, 1, 9); // 마부
								createNewItem2(pc, 420013, 1, 5); // 송곳니
								createNewItem2(pc, 294, 1, 10); // 집행
								L1World.getInstance()
										.broadcastPacketToAll(
												new S_SystemMessage(
														"\\fU서버정보 게시판을 안읽고 당하는 불이익에 대해서는 절대 책임 지지않습니다."),
												true);
								pc.sendPackets(
										new S_SystemMessage(
												"\\fW#####################################################"),
										true);
								pc.sendPackets(new S_SystemMessage(
										"3차 지원아이템 지급 완료 열심히 활동부탁드려요^^ ."), true);
								pc.sendPackets(
										new S_SystemMessage(
												"\\fW#####################################################"),
										true);
							}
							if (pc.isWizard()) {
								createNewItem2(pc, 420008, 1, 0); // 순백체력반지
								createNewItem2(pc, 420008, 1, 0); // 순백체력반지
								createNewItem2(pc, 20317, 1, 0); // 순백체력반지
								createNewItem2(pc, 20018, 1, 7); // 메르모
								createNewItem2(pc, 20025, 1, 7); // 발터모
								createNewItem2(pc, 20218, 1, 7); // 흑샌
								createNewItem2(pc, 20187, 1, 7); // 파글
								createNewItem2(pc, 20093, 1, 0); // 고판
								createNewItem2(pc, 20257, 1, 0); // 블목
								createNewItem2(pc, 121, 1, 8); // 무양
								createNewItem2(pc, 119, 1, 0); // 무양
								createNewItem2(pc, 21031, 1, 7); // 파글
								createNewItem2(pc, 20253, 1, 0); // 빛고목
								L1World.getInstance()
										.broadcastPacketToAll(
												new S_SystemMessage(
														"\\fU제작 아이템관련은 서버정보 게시판을 필독해주세요."),
												true);
								pc.sendPackets(
										new S_SystemMessage(
												"\\fW#####################################################"),
										true);
								pc.sendPackets(new S_SystemMessage(
										"3차 지원아이템 지급 완료 열심히 활동부탁드려요^^ ."), true);
								pc.sendPackets(
										new S_SystemMessage(
												"\\fW#####################################################"),
										true);
							}
							if (pc.isIllusionist()) {
								createNewItem2(pc, 420008, 1, 0); // 순백체력반지
								createNewItem2(pc, 420008, 1, 0); // 순백체력반지
								createNewItem2(pc, 20317, 1, 0); // 순백체력반지
								createNewItem2(pc, 20018, 1, 7); // 메르모
								createNewItem2(pc, 20025, 1, 7); // 발터모
								createNewItem2(pc, 20218, 1, 7); // 흑샌
								createNewItem2(pc, 20187, 1, 7); // 파글
								createNewItem2(pc, 20093, 1, 0); // 고판
								createNewItem2(pc, 20257, 1, 0); // 블목
								createNewItem2(pc, 121, 1, 8); // 무양
								createNewItem2(pc, 119, 1, 0); // 무양
								createNewItem2(pc, 21031, 1, 7); // 파글
								createNewItem2(pc, 20253, 1, 0); // 빛고목
								L1World.getInstance()
										.broadcastPacketToAll(
												new S_SystemMessage(
														"\\fU공성은 최초시작후 2일마다 1시간씩 진행 됩니다."),
												true);
								pc.sendPackets(
										new S_SystemMessage(
												"\\fW#####################################################"),
										true);
								pc.sendPackets(new S_SystemMessage(
										"3차 지원아이템 지급 완료 열심히 활동부탁드려요^^ ."), true);
								pc.sendPackets(
										new S_SystemMessage(
												"\\fW#####################################################"),
										true);
							}
							if (pc.isElf()) {
								createNewItem2(pc, 425111, 1, 4); // 순백체력반지
								createNewItem2(pc, 425111, 1, 4); // 순백체력반지
								createNewItem2(pc, 20317, 1, 0); // 순백체력반지
								createNewItem2(pc, 20011, 1, 8); // 마투
								createNewItem2(pc, 20056, 1, 8); // 마망
								createNewItem2(pc, 20194, 1, 8); // 강부
								createNewItem2(pc, 21060, 1, 9); // 고판
								createNewItem2(pc, 205, 1, 9); // 무양
								createNewItem2(pc, 20191, 1, 8); // 파글
								createNewItem2(pc, 425106, 1, 8); // 스티
								createNewItem2(pc, 420013, 1, 0); // 빛고목
								L1World.getInstance()
										.broadcastPacketToAll(
												new S_SystemMessage(
														"\\fU운영자 비방, 서버 비방, 스핵등의 프로그램 사용시 경고없이 강퇴입니다."),
												true);
								pc.sendPackets(
										new S_SystemMessage(
												"\\fW#####################################################"),
										true);
								pc.sendPackets(new S_SystemMessage(
										"3차 지원아이템 지급 완료 열심히 활동부탁드려요^^ ."), true);
								pc.sendPackets(
										new S_SystemMessage(
												"\\fW#####################################################"),
										true);
							}
							if (pc.isDarkelf()) {
								createNewItem2(pc, 425111, 1, 4); // 순백체력반지
								createNewItem2(pc, 425111, 1, 4); // 순백체력반지
								createNewItem2(pc, 20317, 1, 0); // 순백체력반지
								createNewItem2(pc, 20011, 1, 8); // 마투
								createNewItem2(pc, 20056, 1, 8); // 마망
								createNewItem2(pc, 20195, 1, 8); // 강부
								createNewItem2(pc, 21060, 1, 9); // 고판
								createNewItem2(pc, 84, 1, 8); // 무양
								createNewItem2(pc, 20187, 1, 8); // 파글
								createNewItem2(pc, 425106, 1, 8); // 스티
								createNewItem2(pc, 20422, 1, 0); // 빛고목
								L1World.getInstance()
										.broadcastPacketToAll(
												new S_SystemMessage(
														"\\fU<< 서버 정보 게시판 >>은 꼭 확인하세요."),
												true);
								pc.sendPackets(
										new S_SystemMessage(
												"\\fW#####################################################"),
										true);
								pc.sendPackets(new S_SystemMessage(
										"3차 지원아이템 지급 완료 열심히 활동부탁드려요^^ ."), true);
								pc.sendPackets(
										new S_SystemMessage(
												"\\fW#####################################################"),
										true);
							}
						}
					} else if (itemId == 600233) { // 숫돌
						강화이벤트주문서(pc, useItem, l1iteminstance1);
					} else if (itemId == 600240) { // 상자
						/*
						 * if(l1iteminstance1.getEnchantLevel() == 0){
						 * 강화상자0(pc); }else if(l1iteminstance1.getEnchantLevel()
						 * == 1){ 강화상자1(pc); }else
						 * if(l1iteminstance1.getEnchantLevel() == 2){
						 * 강화상자2(pc); }else if(l1iteminstance1.getEnchantLevel()
						 * == 3){ 강화상자3(pc); }
						 */
					} else if (itemId == 40317 || itemId == 60155) { // 숫돌
						// 무기나 방어용 기구의 경우만
						if (l1iteminstance1.getItem().getType2() != 0 && l1iteminstance1.get_durability() > 0) {
							String msg0;
							pc.getInventory().recoveryDamage(l1iteminstance1);
							msg0 = l1iteminstance1.getLogName();
							if (l1iteminstance1.get_durability() == 0) {
								if(l1iteminstance1.getItem().getType2() == 2 && l1iteminstance1.getItem().getType() == 2 && l1iteminstance1.isEquipped()){
								pc.getAC().addAc(-1);
								pc.sendPackets(new S_OwnCharAttrDef(pc), true);
								pc.sendPackets(new S_OwnCharStatus(pc), true);
								pc.sendPackets(new S_SPMR(pc), true);
								}
								pc.sendPackets(new S_ServerMessage(464, msg0), true); // %0%s는 신품 같은 상태가 되었습니다.
							} else {
								if(l1iteminstance1.getItem().getType2() == 2 && l1iteminstance1.getItem().getType() == 2 && l1iteminstance1.isEquipped()){
								pc.getAC().addAc(-1);
								pc.sendPackets(new S_OwnCharAttrDef(pc), true);
								pc.sendPackets(new S_OwnCharStatus(pc), true);
								pc.sendPackets(new S_SPMR(pc), true);
								}
								pc.sendPackets(new S_ServerMessage(463, msg0), true); // %0 상태가 좋아졌습니다.
							}
							pc.getInventory().removeItem(useItem, 1);
						} else {
							pc.sendPackets(new S_SystemMessage(
									"아무일도 일어나지 않았습니다."), true); // \f1 아무것도 일어나지 않았습니다.
						}
					} else if (itemId == 7322 || itemId == 7240) { // 드래곤의 연금술
																	// 용액
						용액(pc, useItem, l1iteminstance1);
					} else if (itemId == L1ItemId.LOWER_OSIRIS_PRESENT_PIECE_DOWN
							|| itemId == L1ItemId.HIGHER_OSIRIS_PRESENT_PIECE_DOWN) { // 오시리스의
																						// 조각
																						// (하)
						int itemId2 = l1iteminstance1.getItem().getItemId();
						if (itemId == L1ItemId.LOWER_OSIRIS_PRESENT_PIECE_DOWN
								&& itemId2 == L1ItemId.LOWER_OSIRIS_PRESENT_PIECE_UP) {
							if (pc.getInventory().checkItem(
									L1ItemId.LOWER_OSIRIS_PRESENT_PIECE_UP)) {
								pc.getInventory()
										.removeItem(l1iteminstance1, 1);
								pc.getInventory().removeItem(useItem, 1);
								pc.getInventory().storeItem(
										L1ItemId.CLOSE_LOWER_OSIRIS_PRESENT, 1);
							}
						} else if (itemId == L1ItemId.HIGHER_OSIRIS_PRESENT_PIECE_DOWN
								&& itemId2 == L1ItemId.HIGHER_OSIRIS_PRESENT_PIECE_UP) {
							if (pc.getInventory().checkItem(
									L1ItemId.HIGHER_OSIRIS_PRESENT_PIECE_UP)) {
								pc.getInventory()
										.removeItem(l1iteminstance1, 1);
								pc.getInventory().removeItem(useItem, 1);
								pc.getInventory()
										.storeItem(
												L1ItemId.CLOSE_HIGHER_OSIRIS_PRESENT,
												1);
							}
						} else {
							pc.sendPackets(new S_SystemMessage(
									"아무일도 일어나지 않았습니다."), true); // \f1 아무것도 일어나지
																// 않았습니다.
						}
					} else if (itemId == L1ItemId.LOWER_TIKAL_PRESENT_PIECE_DOWN
							|| itemId == L1ItemId.HIGHER_TIKAL_PRESENT_PIECE_DOWN) { // 오시리스의
																						// 조각
																						// (하)
						int itemId2 = l1iteminstance1.getItem().getItemId();
						if (itemId == L1ItemId.LOWER_TIKAL_PRESENT_PIECE_DOWN
								&& itemId2 == L1ItemId.LOWER_TIKAL_PRESENT_PIECE_UP) {
							if (pc.getInventory().checkItem(
									L1ItemId.LOWER_TIKAL_PRESENT_PIECE_UP)) {
								pc.getInventory()
										.removeItem(l1iteminstance1, 1);
								pc.getInventory().removeItem(useItem, 1);
								pc.getInventory().storeItem(
										L1ItemId.CLOSE_LOWER_TIKAL_PRESENT, 1);
							}
						} else if (itemId == L1ItemId.HIGHER_TIKAL_PRESENT_PIECE_DOWN
								&& itemId2 == L1ItemId.HIGHER_TIKAL_PRESENT_PIECE_UP) {
							if (pc.getInventory().checkItem(
									L1ItemId.HIGHER_TIKAL_PRESENT_PIECE_UP)) {
								pc.getInventory()
										.removeItem(l1iteminstance1, 1);
								pc.getInventory().removeItem(useItem, 1);
								pc.getInventory().storeItem(
										L1ItemId.CLOSE_HIGHER_TIKAL_PRESENT, 1);
							}
						} else {
							pc.sendPackets(new S_SystemMessage(
									"아무일도 일어나지 않았습니다."), true); // \f1 아무것도 일어나지
																// 않았습니다.
						}
					} else if (itemId == L1ItemId.ANCIENT_ROYALSEAL) { // 태고의 옥쇄
						if (client.getAccount().getCharSlot() < 8) {
							client.getAccount().setCharSlot(client,
									client.getAccount().getCharSlot() + 1);
							pc.getInventory().removeItem(useItem, 1);
						} else {
							pc.sendPackets(new S_SystemMessage(
									"아무일도 일어나지 않았습니다."), true); // \f1 아무것도 일어나지
																// 않았습니다.
						}
					} else if (itemId == L1ItemId.TIMECRACK_CORE) { // 균열의 핵
						int itemId2 = l1iteminstance1.getItem().getItemId();
						if (itemId2 == L1ItemId.CLOSE_LOWER_OSIRIS_PRESENT) {
							if (pc.getInventory().checkItem(
									L1ItemId.CLOSE_LOWER_OSIRIS_PRESENT)) {
								pc.getInventory()
										.removeItem(l1iteminstance1, 1);
								pc.getInventory().removeItem(useItem, 1);
								pc.getInventory().storeItem(
										L1ItemId.OPEN_LOWER_OSIRIS_PRESENT, 1);
							}
						} else if (itemId2 == L1ItemId.CLOSE_HIGHER_OSIRIS_PRESENT) {
							if (pc.getInventory().checkItem(
									L1ItemId.CLOSE_HIGHER_OSIRIS_PRESENT)) {
								pc.getInventory()
										.removeItem(l1iteminstance1, 1);
								pc.getInventory().removeItem(useItem, 1);
								pc.getInventory().storeItem(
										L1ItemId.OPEN_HIGHER_OSIRIS_PRESENT, 1);
							}
						} else if (itemId2 == L1ItemId.CLOSE_LOWER_TIKAL_PRESENT) {
							if (pc.getInventory().checkItem(
									L1ItemId.CLOSE_LOWER_TIKAL_PRESENT)) {
								pc.getInventory()
										.removeItem(l1iteminstance1, 1);
								pc.getInventory().removeItem(useItem, 1);
								pc.getInventory().storeItem(
										L1ItemId.OPEN_LOWER_TIKAL_PRESENT, 1);
							}
						} else if (itemId2 == L1ItemId.CLOSE_HIGHER_TIKAL_PRESENT) {
							if (pc.getInventory().checkItem(
									L1ItemId.CLOSE_HIGHER_TIKAL_PRESENT)) {
								pc.getInventory()
										.removeItem(l1iteminstance1, 1);
								pc.getInventory().removeItem(useItem, 1);
								pc.getInventory().storeItem(
										L1ItemId.OPEN_HIGHER_TIKAL_PRESENT, 1);
							}
						} else {
							pc.sendPackets(new S_SystemMessage(
									"아무일도 일어나지 않았습니다."), true); // \f1 아무것도 일어나지
																// 않았습니다.
						}
					} else if (itemId == 40097 || itemId == 40119
							|| itemId == 140119 || itemId == 140329) { // 해주스크롤,
																		// 원주민의
																		// 토템
						L1Item template = null;
						for (L1ItemInstance eachItem : pc.getInventory()
								.getItems()) {
							if (eachItem.getItem().getBless() != 2) {
								continue;
							}
							if (!eachItem.isEquipped()
									&& (itemId == 40119 || itemId == 40097)) {
								// n해주는 장비 하고 있는 것 밖에 해주 하지 않는다
								continue;
							}
							int id_normal = eachItem.getItemId() - 200000;
							template = ItemTable.getInstance().getTemplate(
									id_normal);
							if (template == null) {
								continue;
							}
							if (pc.getInventory().checkItem(id_normal)
									&& template.isStackable()) {
								pc.getInventory().storeItem(id_normal,
										eachItem.getCount());
								pc.getInventory().removeItem(eachItem,
										eachItem.getCount());
							} else {
								eachItem.setItem(template);
								pc.getInventory().updateItem(eachItem,
										L1PcInventory.COL_ITEMID);
								pc.getInventory().saveItem(eachItem,
										L1PcInventory.COL_ITEMID);
								eachItem.setBless(eachItem.getBless() - 1);
								pc.getInventory().updateItem(eachItem,
										L1PcInventory.COL_BLESS);
								pc.getInventory().saveItem(eachItem,
										L1PcInventory.COL_BLESS);
							}
						}
						pc.getInventory().removeItem(useItem, 1);
						pc.sendPackets(new S_SystemMessage("누군가가 도와주는 것같습니다."),
								true); // \f1누군가가 도와 준 것 같습니다.
					} else if (itemId == 41036) { // 풀
						int diaryId = l1iteminstance1.getItem().getItemId();
						if (diaryId >= 41038 && 41047 >= diaryId) {
							if ((_random.nextInt(99) + 1) <= Config.CREATE_CHANCE_DIARY) {
								createNewItem(pc, diaryId + 10, 1);
							} else {
								pc.sendPackets(new S_ServerMessage(158,
										l1iteminstance1.getName()), true); // \f1%0이
																			// 증발하고
																			// 있지
																			// 않게
																			// 되었습니다.
							}
							pc.getInventory().removeItem(l1iteminstance1, 1);
							pc.getInventory().removeItem(useItem, 1);
						} else {
							pc.sendPackets(new S_SystemMessage(
									"아무일도 일어나지 않았습니다."), true); // \f1 아무것도 일어나지
																// 않았습니다.
						}
					} else if (itemId == 40964) { // 흑마법 가루
						int historybookId = l1iteminstance1.getItem().getItemId();
						if (historybookId >= 41011 && 41018 >= historybookId) {
							if ((_random.nextInt(99) + 1) <= Config.CREATE_CHANCE_HISTORY_BOOK) {
								createNewItem(pc, historybookId + 8, 1);
							} else {
								pc.sendPackets(new S_ServerMessage(158,
										l1iteminstance1.getName()), true);
							}
							pc.getInventory().removeItem(l1iteminstance1, 1);
							pc.getInventory().removeItem(useItem, 1);
						} else {
							pc.sendPackets(new S_SystemMessage(
									"아무일도 일어나지 않았습니다."), true);
						}
					} else if (itemId == 40925) { // 정화의 일부
						int earingId = l1iteminstance1.getItem().getItemId();
						if (earingId >= 40987 && 40989 >= earingId) { // 저주해진 블랙
																		// 귀 링
							if (_random.nextInt(100) < Config.CREATE_CHANCE_RECOLLECTION) {
								createNewItem(pc, earingId + 186, 1);
							} else {
								pc.sendPackets(new S_ServerMessage(158,
										l1iteminstance1.getName()), true); // \f1%0이
																			// 증발하고
																			// 있지
																			// 않게
																			// 되었습니다.
							}
							pc.getInventory().removeItem(l1iteminstance1, 1);
							pc.getInventory().removeItem(useItem, 1);
						} else {
							pc.sendPackets(new S_SystemMessage(
									"아무일도 일어나지 않았습니다."), true); // \f1 아무것도 일어나지
																// 않았습니다.
						}
					} else if (itemId >= 40926 && 40929 >= itemId) {
						// 　신비적인 일부(1~4 단계)
						int earing2Id = l1iteminstance1.getItem().getItemId();
						int potion1 = 0;
						int potion2 = 0;
						if (earing2Id >= 41173 && 41184 >= earing2Id) {
							// 귀 링류
							if (itemId == 40926) {
								potion1 = 247;
								potion2 = 249;
							} else if (itemId == 40927) {
								potion1 = 249;
								potion2 = 251;
							} else if (itemId == 40928) {
								potion1 = 251;
								potion2 = 253;
							} else if (itemId == 40929) {
								potion1 = 253;
								potion2 = 255;
							}
							if (earing2Id >= (itemId + potion1)
									&& (itemId + potion2) >= earing2Id) {
								if ((_random.nextInt(99) + 1) < Config.CREATE_CHANCE_MYSTERIOUS) {
									createNewItem(pc, (earing2Id - 12), 1);
									pc.getInventory().removeItem(
											l1iteminstance1, 1);
									pc.getInventory().removeItem(useItem, 1);
								} else {
									pc.sendPackets(new S_ServerMessage(160,
											l1iteminstance1.getName()), true);
									// \f1%0이%2 강렬하게%1 빛났습니다만, 다행히 무사하게 살았습니다.
									pc.getInventory().removeItem(useItem, 1);
								}
							} else {
								pc.sendPackets(new S_SystemMessage(
										"아무일도 일어나지 않았습니다."), true); // \f1 아무것도
																	// 일어나지
																	// 않았습니다.
							}
						} else {
							pc.sendPackets(new S_SystemMessage(
									"아무일도 일어나지 않았습니다."), true); // \f1 아무것도 일어나지
																// 않았습니다.
						}
					} else if (itemId == 41029) { // 소환구 조각
						int dantesId = l1iteminstance1.getItem().getItemId();
						if (dantesId >= 41030 && 41034 >= dantesId) { // 소환공의
																		// 코어· 각
																		// 단계
							if ((_random.nextInt(99) + 1) < Config.CREATE_CHANCE_DANTES) {
								createNewItem(pc, dantesId + 1, 1);
							} else {
								pc.sendPackets(new S_ServerMessage(158,
										l1iteminstance1.getName()), true); // \f1%0이
																			// 증발하고
																			// 있지
																			// 않게
																			// 되었습니다.
							}
							pc.getInventory().removeItem(l1iteminstance1, 1);
							pc.getInventory().removeItem(useItem, 1);
						} else {
							pc.sendPackets(new S_SystemMessage(
									"아무일도 일어나지 않았습니다."), true); // \f1 아무것도 일어나지
																// 않았습니다.
						}
						// 스펠 스크롤
					} else if (((itemId >= 40859 && itemId <= 40898) && itemId != 40863) || (itemId >= 60003 && itemId <= 60008)) {
						if (pc.isSkillDelay()) {
							return;
						}
						if (pc.getInventory().calcWeightpercent() >= 90) {
							pc.sendPackets(new S_SystemMessage("아이템 사용 실패: 무게 게이지 90% 이상 사용 불가."));
							return;
						}
						if (pc.isTeleport() || pc.isDead()) {
							pc.sendPackets(new S_ServerMessage(281), true); // \f1마법이 무효가 되었습니다.
							return;
						}

						pc.getInventory().removeItem(useItem, 1);
						pc.cancelAbsoluteBarrier(); // 아브소르트바리아의 해제
						int skillid = itemId - 40858;
						
						if (itemId == 60003)
							skillid = 42;
						else if (itemId == 60004)
							skillid = 48;
						else if (itemId == 60005)
							skillid = 52;
						else if (itemId == 60006)
							skillid = 57;
						else if (itemId == 60007)
							skillid = 54;
						else if (itemId == 60008)
							skillid = 49;

						L1Skills _skill = SkillsTable.getInstance().getTemplate(skillid);

						double delay_rate = 0.900;// 874;
						int delay = _skill.getScrollReuseDelay();

						if (pc.isHaste()) {
							delay *= delay_rate;
						}

						if (pc.isBrave()) {
							delay *= delay_rate;
						}

						if (delay > 0) {
							pc.setSkillDelay(true);
							GeneralThreadPool.getInstance().schedule(new L1SkillDelay(pc, delay), delay);
						}

						pc.플레이어상태 = pc.공격_상태;
						pc.상태시간 = System.currentTimeMillis() + 2000;

						L1SkillUse l1skilluse = new L1SkillUse();
						l1skilluse.handleCommands(client.getActiveChar(), skillid, spellsc_objid, spellsc_x, spellsc_y, null, 0, L1SkillUse.TYPE_SPELLSC);
						l1skilluse = null;
					} else if (itemId >= 40373 && itemId <= 40384 || itemId >= 40385 && itemId <= 40390) {
						pc.sendPackets(new S_UseMap(pc, useItem.getId(), useItem.getItem().getItemId()), true);
					} else if (itemId == 40493) { // 매직 플룻
						pc.sendPackets(new S_Sound(165));
						Broadcaster.broadcastPacket(pc, new S_Sound(165), true);
						L1GuardianInstance guardian = null;
						for (L1Object visible : pc.getNearObjects()
								.getKnownObjects()) {
							if (visible instanceof L1GuardianInstance) {
								guardian = (L1GuardianInstance) visible;
								if (guardian.getNpcTemplate().get_npcId() == 70850) { // 빵
									if (createNewItem(pc, 88, 1)) {
										pc.getInventory()
												.removeItem(useItem, 1);
									}
								}
							}
						}
					} else if (itemId == 40325) {
						if (pc.getInventory().checkItem(40318, 1)) {
							int gfxid = 3237 + _random.nextInt(2);
							pc.sendPackets(new S_SkillSound(pc.getId(), gfxid),
									true);
							Broadcaster.broadcastPacket(pc,
									new S_SkillSound(pc.getId(), gfxid), true);
							pc.getInventory().consumeItem(40318, 1);
						} else {
							pc.sendPackets(new S_SystemMessage(
									"아무일도 일어나지 않았습니다.")); // \f1 아무것도 일어나지
															// 않았습니다.
						}
					} else if (itemId == 40326) {
						if (pc.getInventory().checkItem(40318, 1)) {
							int gfxid = 3229 + _random.nextInt(3);
							pc.sendPackets(new S_SkillSound(pc.getId(), gfxid),
									true);
							Broadcaster.broadcastPacket(pc,
									new S_SkillSound(pc.getId(), gfxid), true);
							pc.getInventory().consumeItem(40318, 1);
						} else {
							pc.sendPackets(new S_SystemMessage(
									"아무일도 일어나지 않았습니다."), true); // \f1 아무것도 일어나지
																// 않았습니다.
						}
					} else if (itemId == 40327) {
						if (pc.getInventory().checkItem(40318, 1)) {
							int gfxid = 3241 + _random.nextInt(4);
							pc.sendPackets(new S_SkillSound(pc.getId(), gfxid),
									true);
							Broadcaster.broadcastPacket(pc,
									new S_SkillSound(pc.getId(), gfxid), true);
							pc.getInventory().consumeItem(40318, 1);
						} else {
							pc.sendPackets(new S_SystemMessage(
									"아무일도 일어나지 않았습니다."), true); // \f1 아무것도 일어나지
																// 않았습니다.
						}
					} else if (itemId == 40328) {
						if (pc.getInventory().checkItem(40318, 1)) {
							int gfxid = 3204 + _random.nextInt(6);
							pc.sendPackets(new S_SkillSound(pc.getId(), gfxid),
									true);
							Broadcaster.broadcastPacket(pc,
									new S_SkillSound(pc.getId(), gfxid), true);
							pc.getInventory().consumeItem(40318, 1);
						} else {
							// \f1 아무것도 일어나지 않았습니다.
							pc.sendPackets(new S_SystemMessage(
									"아무일도 일어나지 않았습니다."), true);
						}

					} else if (itemId == 5000085) {
						EventT(pc, useItem);
						int special = _random.nextInt(20);
						switch (special) {
						case 0:
							pc.getInventory().storeItem(490009, 1);
							break;
						default:
							pc.getInventory().storeItem(490000, 1);
							break;
						}

					} else if (itemId == 600250) {
						아르카마법인형상자(pc, useItem);
					} else if (itemId == 75008) {
						랜덤마법인형상자(pc, useItem);
					} else if (itemId == 600257) {
						진명황의상자(pc, useItem);
					} else if (itemId == 600248) {
						아르카마법인형주머니(pc, useItem);
					} else if (itemId == 5000086) {
						EventT(pc, useItem);
						int special = _random.nextInt(20);
						switch (special) {
						case 0:
							pc.getInventory().storeItem(490010, 1);
							break;
						default:
							pc.getInventory().storeItem(490001, 1);
							break;
						}
					} else if (itemId == 5000087) {
						EventT(pc, useItem);
						int special = _random.nextInt(20);
						switch (special) {
						case 0:
							pc.getInventory().storeItem(490011, 1);
							break;
						default:
							pc.getInventory().storeItem(490002, 1);
							break;
						}
					} else if (itemId == 5000088) {
						EventT(pc, useItem);
						int special = _random.nextInt(20);
						switch (special) {
						case 0:
							pc.getInventory().storeItem(490012, 1);
							break;
						default:
							pc.getInventory().storeItem(490003, 1);
							break;
						}
					} else if (itemId == 5000089) {
						EventT(pc, useItem);
						int special = _random.nextInt(20);
						switch (special) {
						case 0:
							pc.getInventory().storeItem(490013, 1);
							break;
						default:
							pc.getInventory().storeItem(490004, 1);
							break;
						}
					} else if (itemId == 5000090) {
						EventT(pc, useItem);
						int special = _random.nextInt(20);
						switch (special) {
						case 0:
							pc.getInventory().storeItem(490014, 1);
							break;
						default:
							pc.getInventory().storeItem(490005, 1);
							break;
						}
					} else if (itemId == 5000091) {
						EventT(pc, useItem);
						int special = _random.nextInt(20);
						switch (special) {
						case 0:
							pc.getInventory().storeItem(490015, 1);
							break;
						default:
							pc.getInventory().storeItem(490006, 1);
							break;
						}
					} else if (itemId == 5000092) {
						EventT(pc, useItem);
						int special = _random.nextInt(20);
						switch (special) {
						case 0:
							pc.getInventory().storeItem(490016, 1);
							break;
						default:
							pc.getInventory().storeItem(490007, 1);
							break;
						}
					} else if (itemId == 5000093) {
						EventT(pc, useItem);
						int special = _random.nextInt(20);
						switch (special) {
						case 0:
							pc.getInventory().storeItem(490017, 1);
							break;
						default:
							pc.getInventory().storeItem(490008, 1);
							break;
						}
					} else if (itemId == 5000094) {
						pc.getInventory().removeItem(useItem, 1);
						pc.getInventory().storeItem(5000098, 5);

					} else if (itemId >= 5001120 && itemId <= 5001129) {
						useToiTeleportAmulets(pc, itemId, useItem);
					} 
					else if (itemId >= 5000100 && itemId <= 5000109) {
						pc.getInventory().removeItem(useItem, 1);
						int chance = _random.nextInt(100);
						if (chance <= 70) {
							L1ItemInstance item = pc.getInventory().storeItem(
									itemId + 1020, 1);
							if (item != null) {
								pc.sendPackets(
										new S_ServerMessage(403, item
												.getLogName()), true);
							}
						} else {
							L1ItemInstance item = null;
							if (itemId == 5000100) {
								item = pc.getInventory().storeItem(60202, 1);
							} else {
								item = pc.getInventory().storeItem(itemId - 4959812, 1);
							}
							if (item != null) {
								pc.sendPackets(new S_ServerMessage(403, item.getLogName()), true);
							}
						}
					} else if (itemId >= 5000110 && itemId <= 5000119) {
						혼돈부적(pc, useItem, itemId);
					} else if (itemId == L1ItemId.CHARACTER_REPAIR_SCROLL) {
						Connection connection = null;
						PreparedStatement preparedstatement = null;
						Connection connection2 = null;
						PreparedStatement preparedstatement2 = null;
						ResultSet rs = null;
						try {
							connection2 = L1DatabaseFactory.getInstance()
									.getConnection();
							preparedstatement2 = connection2
									.prepareStatement("SELECT * FROM characters WHERE account_name = ?");
							preparedstatement2.setString(1,
									client.getAccountName());
							rs = preparedstatement2.executeQuery();
							while (rs.next()) {
								int objid = rs.getInt("objid");
								int mapid = rs.getInt("MapID");
								if (mapid != 99 && mapid != 6202) {
									try {
										connection = L1DatabaseFactory
												.getInstance().getConnection();
										preparedstatement = connection
												.prepareStatement("UPDATE characters SET LocX=33087, LocY=33399, MapID=4 WHERE objid = ?");
										preparedstatement.setInt(1, objid);
										preparedstatement.executeUpdate();
									} catch (Exception e) {
										e.printStackTrace();
									} finally {
										SQLUtil.close(preparedstatement);
										SQLUtil.close(connection);
									}
								}
							}
						} catch (Exception e) {
							e.printStackTrace();
						} finally {
							SQLUtil.close(rs);
							SQLUtil.close(preparedstatement2);
							SQLUtil.close(connection2);
						}
						pc.getInventory().removeItem(useItem, 1);
						pc.sendPackets(new S_SystemMessage(
								"모든 케릭터의 좌표가 정상적으로 복구 되었습니다."), true);
					} else if (itemId == 40903 || itemId == 40904
							|| itemId == 40905 || itemId == 40906
							|| itemId == 40907 || itemId == 40908) { // 각종 약혼 반지
						결혼반(pc, useItem);
					} else if (itemId == 787880) {// 드래곤 돌
						if (pc.isDead()) {
							return;
						}
						if (pc.isParalyzed()) {
							return;
						}
						if (!((pc.getMapId() >= 1005 && pc.getMapId() <= 1016) || (pc.getMapId() >= 1161 && pc.getMapId() <= 1166) || (pc.getMapId() >= 10000 && pc.getMapId() <= 10005)
								|| (pc.getMapId() >= 11000 && pc.getMapId() <= 11005))) {
							pc.sendPackets(new S_SystemMessage(
									"드래곤의 숨결이 깃든 땅에서만 사용할 수 있습니다."), true);
							return;
						}
						pc.getInventory().consumeItem(787880, 1); // 삭제되는 아이템과 수량
						useCashScroll(pc, L1ItemId.INCRESE_ATTACK_SCROLL, true);
					} else if (itemId == 700078) { // 메티스의 가호
						int objid = pc.getId();
						pc.sendPackets(new S_SkillSound(objid, 759));
						Broadcaster.broadcastPacket(pc, new S_SkillSound(objid, 759));
						for (L1PcInstance tg : L1World.getInstance(). getVisiblePlayer(pc)) {
							if (tg.getCurrentHp() == 0 && tg.isDead()) {
								tg.sendPackets(new S_SystemMessage("GM이 부활을 해주었습니다. "));
								Broadcaster.broadcastPacket(tg, new S_SkillSound(tg.getId(), 3944));
								tg.sendPackets(new S_SkillSound(tg.getId(), 3944));
								// 축복된 부활 스크롤과 같은 효과
								tg.setTempID(objid);
								tg.sendPackets(new S_Message_YN(322, "")); // 또 부활하고 싶습니까? (Y/N)
							} else {
								tg.sendPackets(new S_SystemMessage("GM이 HP,MP를 회복해주었습니다."));
								Broadcaster.broadcastPacket(tg, new S_SkillSound(tg.getId(), 832));
								tg.sendPackets(new S_SkillSound(tg.getId(), 832));
								tg.setCurrentHp(tg.getMaxHp());
								tg.setCurrentMp(tg.getMaxMp());
							}
						}

					} else if (itemId == 700079) { // 10월14일몬스터청소기
						for (L1Object obj : L1World.getInstance().getVisibleObjects(pc, 10)) { 
							if (obj instanceof L1MonsterInstance) { // 몬스터라면
								L1NpcInstance npc = (L1NpcInstance) obj;
								npc.receiveDamage(pc, 999999); // 데미지
								if (npc.getCurrentHp() <= 0) {
								} else {
								}
							} else if (obj instanceof L1PcInstance) { // pc라면
								L1PcInstance Player = (L1PcInstance) obj;
								Player.receiveDamage(Player, 0, false); // 데미지
								if (Player.getCurrentHp() <= 0) {
								} else {
								}
							}
						}
					} else if (itemId == 787879) {// 드래곤 눈물
						if (pc.isDead()) {
							return;
						}
						if (pc.isParalyzed()) {
							return;
						}
						if (pc.getSkillEffectTimerSet().hasSkillEffect(71) == true) { // 디케이포션 상태
							pc.sendPackets(new S_ServerMessage(698), true); // 마력에의해 아무것도  마실 수가 없습니다.
							return;
						}
						if (!((pc.getMapId() >= 1005 && pc.getMapId() <= 1016) ||  (pc.getMapId() >= 1161 && pc.getMapId() <= 1166)|| (pc.getMapId() >= 10000 && pc.getMapId() <= 10005) || (pc.getMapId() >= 11000 && pc.getMapId() <= 11005))) {
							pc.sendPackets(new S_SystemMessage("드래곤의 숨결이 깃든 땅에서만 사용할 수 있습니다."), true);
							return;
						}
						pc.getInventory().consumeItem(787879, 1); // 삭제되는 아이템과
																	// 수량
						pc.sendPackets(new S_SkillSound(pc.getId(), 189));
						Broadcaster.broadcastPacket(pc,
								new S_SkillSound(pc.getId(), 189), true);
						int healHP = _random.nextInt(400) + 200;
						pc.setCurrentHp(pc.getCurrentHp() + healHP);

					} else if (itemId == 787878) {// 달아난 드래곤의 흔적
						pc.getInventory().consumeItem(787878, 1); // 삭제되는 아이템과
																	// 수량
						createNewItem2(pc, 787879, 10, 0); // 눈물 10개
						createNewItem2(pc, 787880, 1, 0); // 전강 1개

					} else if (itemId == 430005) {
						if (CharPosUtil.getZoneType(pc) != 1){
							pc.sendPackets(new S_SystemMessage("사용 실패: 사용 불가 지역"), true);
							return;
						}
						if (pc.getMapId() != 6202) {
							if (pc.getLevel() > 50) {
								if (pc.getInventory().checkItem(L1ItemId.REMINISCING_CANDLE)) {
									pc.getInventory().consumeItem(L1ItemId.REMINISCING_CANDLE, 1);
									L1Teleport.teleport(pc, 32723 + _random.nextInt(10), 32851 + _random.nextInt(10), (short) 5166, 5, true);
									eva.RStatLogAppend(" [스텟 초기화] " + pc.getName() + "");
									StatInitialize(pc);
								} else {
									pc.sendPackets(new S_ServerMessage(1290));// 스테이터스 초기화에  필요한 아이템이 없습니다.
								}
							} else {
								pc.sendPackets(new S_SystemMessage(
										"스텟초기화는 51부터 이용하실수 있습니다."));
							}
						} else {
							pc.sendPackets(new S_SystemMessage(
									"감옥에서 사용 불가능 아이템 입니다."));
						}
					} else if (itemId == 40555) { // 비밀의 방의 키
						// 오림 방
						if (pc.isKnight()
								&& (pc.getX() >= 32806 && pc.getX() <= 32814)
								&& (pc.getY() >= 32798 && pc.getY() <= 32807)
								&& pc.getMapId() == 13) {
							if (pc.Sabutelok()) {
								pc.dx = 32815;
								pc.dy = 32810;
								pc.dm = (short) 13;
								pc.dh = pc.getMoveState().getHeading();
								pc.setTelType(7);
								pc.sendPackets(new S_SabuTell(pc), true);
							}
						} else {
							pc.sendPackets(new S_SystemMessage(
									"아무일도 일어나지 않았습니다."), true); // \f1 아무것도 일어나지
																// 않았습니다.
						}
					} else if (itemId == 40417) { // 정령의결정
						if (pc.getMapId() == 440 && pc.getX() > 32667
								&& pc.getX() < 32673 && pc.getY() > 32977
								&& pc.getY() < 32984) {
							if (pc.Sabutelok()) {
								pc.dx = 32734;
								pc.dy = 32806;
								pc.dm = (short) 444;
								pc.dh = pc.getMoveState().getHeading();
								pc.setTelType(7);
								pc.sendPackets(new S_SabuTell(pc), true);
							}
						} else {
							pc.sendPackets(new S_SystemMessage(
									"아무일도 일어나지 않았습니다."), true); // \f1 아무것도 일어나지
																// 않았습니다.
						}
					} else if (itemId == 600222) {
						if (pc.isPinkName()) {
							pc.sendPackets(new S_SystemMessage(
									"전투중이라 사용할 수 없습니다."));
							return;
						}
						if (pc.isFishing() || pc.isFishingReady()
								|| pc.isPrivateShop()) {
							return;
						}
						long curtime = System.currentTimeMillis() / 1000;
						if (pc.getQuizTime() + 20 > curtime) {
							pc.sendPackets(new S_SystemMessage(
									"20초간의 지연시간이 필요합니다."));
							return;
						}
						L1Teleport.teleport(pc, pc.getX(), pc.getY(),
								pc.getMapId(), pc.getMoveState().getHeading(),
								false);
						pc.sendPackets(new S_SystemMessage(
								"주변 오브젝트를 재로딩 하였습니다."));
						pc.setQuizTime(curtime);
						pc.getInventory().consumeItem(600222, 1); // 삭제되는 아이템과
																	// 수량
					} else if (itemId == 40566) { // 신비적인 쉘
						// 상아의 탑의 마을의 남쪽에 있는 매직 스퀘어의 좌표
						if (pc.isElf()
								&& (pc.getX() >= 33971 && pc.getX() <= 33975)
								&& (pc.getY() >= 32324 && pc.getY() <= 32328)
								&& pc.getMapId() == 4
								&& !pc.getInventory().checkItem(40548)) { // 망령의
																			// 봉투
							boolean found = false;
							L1MonsterInstance mob = null;
							for (L1Object obj : L1World.getInstance()
									.getVisibleObjects(4).values()) {
								if (obj instanceof L1MonsterInstance) {
									mob = (L1MonsterInstance) obj;
									if (mob != null) {
										if (mob.getNpcTemplate().get_npcId() == 45300) {
											found = true;
											break;
										}
									}
								}
							}
							if (found) {
								pc.sendPackets(new S_SystemMessage(
										"아무일도 일어나지 않았습니다."), true); // \f1 아무것도
																	// 일어나지
																	// 않았습니다.
							} else {
								L1SpawnUtil.spawn(pc, 45300, 0, 0, false); // 고대인의
																			// 망령
							}
						} else {
							pc.sendPackets(new S_SystemMessage(
									"아무일도 일어나지 않았습니다."), true); // \f1 아무것도 일어나지
																// 않았습니다.
						}
					} else if (itemId == 40557) {
						if (pc.getX() == 32620 && pc.getY() == 32641
								&& pc.getMapId() == 4) {
							L1NpcInstance object = null;
							for (L1Object obj : L1World.getInstance()
									.getObject()) {
								if (obj instanceof L1NpcInstance) {
									object = (L1NpcInstance) obj;
									if (object.getNpcTemplate().get_npcId() == 45883) {
										pc.sendPackets(new S_SystemMessage(
												"아무일도 일어나지 않았습니다."), true);
										return;
									}
								}

							}
							L1SpawnUtil.spawn(pc, 45883, 0, 300000, false);
						} else {
							pc.sendPackets(new S_SystemMessage(
									"아무일도 일어나지 않았습니다."), true);
						}
					} else if (itemId == 40563) {
						if (pc.getX() == 32730 && pc.getY() == 32426
								&& pc.getMapId() == 4) {
							L1NpcInstance object = null;
							for (L1Object obj : L1World.getInstance()
									.getObject()) {
								if (obj instanceof L1NpcInstance) {
									object = (L1NpcInstance) obj;
									if (object.getNpcTemplate().get_npcId() == 45884) {
										pc.sendPackets(new S_SystemMessage(
												"아무일도 일어나지 않았습니다."), true);
										return;
									}
								}

							}
							L1SpawnUtil.spawn(pc, 45884, 0, 300000, false);
						} else {
							pc.sendPackets(new S_SystemMessage(
									"아무일도 일어나지 않았습니다."), true);
						}
					} else if (itemId == 40561) {
						if (pc.getX() == 33046 && pc.getY() == 32806
								&& pc.getMapId() == 4) {
							L1NpcInstance object = null;
							for (L1Object obj : L1World.getInstance()
									.getObject()) {
								if (obj instanceof L1NpcInstance) {
									object = (L1NpcInstance) obj;
									if (object.getNpcTemplate().get_npcId() == 45885) {
										pc.sendPackets(new S_SystemMessage(
												"아무일도 일어나지 않았습니다."), true);
										return;
									}
								}

							}
							L1SpawnUtil.spawn(pc, 45885, 0, 300000, false);
						} else {
							pc.sendPackets(new S_SystemMessage(
									"아무일도 일어나지 않았습니다."), true);
						}

					} else if (itemId == 40560) {
						if (pc.getX() == 32580 && pc.getY() == 33260
								&& pc.getMapId() == 4) {
							L1NpcInstance object = null;
							for (L1Object obj : L1World.getInstance()
									.getObject()) {
								if (obj instanceof L1NpcInstance) {
									object = (L1NpcInstance) obj;
									if (object.getNpcTemplate().get_npcId() == 45886) {
										pc.sendPackets(new S_SystemMessage(
												"아무일도 일어나지 않았습니다."), true);
										return;
									}
								}

							}
							L1SpawnUtil.spawn(pc, 45886, 0, 300000, false);
						} else {
							pc.sendPackets(new S_SystemMessage(
									"아무일도 일어나지 않았습니다."), true);
						}
					} else if (itemId == 40562) {
						if (pc.getX() == 33447 && pc.getY() == 33476
								&& pc.getMapId() == 4) {
							L1NpcInstance object = null;
							for (L1Object obj : L1World.getInstance()
									.getObject()) {
								if (obj instanceof L1NpcInstance) {
									object = (L1NpcInstance) obj;
									if (object.getNpcTemplate().get_npcId() == 45887) {
										pc.sendPackets(new S_SystemMessage(
												"아무일도 일어나지 않았습니다."), true);
										return;
									}
								}

							}
							L1SpawnUtil.spawn(pc, 45887, 0, 300000, false);
						} else {
							pc.sendPackets(new S_SystemMessage(
									"아무일도 일어나지 않았습니다."), true);
						}
					} else if (itemId == 40559) {
						if (pc.getX() == 34215 && pc.getY() == 33195
								&& pc.getMapId() == 4) {
							L1NpcInstance object = null;
							for (L1Object obj : L1World.getInstance()
									.getObject()) {
								if (obj instanceof L1NpcInstance) {
									object = (L1NpcInstance) obj;
									if (object.getNpcTemplate().get_npcId() == 45888) {
										pc.sendPackets(new S_SystemMessage(
												"아무일도 일어나지 않았습니다."), true);
										return;
									}
								}

							}
							L1SpawnUtil.spawn(pc, 45888, 0, 300000, false);
						} else {
							pc.sendPackets(new S_SystemMessage(
									"아무일도 일어나지 않았습니다."), true);
						}
					} else if (itemId == 40558) {
						if (pc.getX() == 33513 && pc.getY() == 32890
								&& pc.getMapId() == 4) {
							L1NpcInstance object = null;
							for (L1Object obj : L1World.getInstance()
									.getObject()) {
								if (obj instanceof L1NpcInstance) {
									object = (L1NpcInstance) obj;
									if (object.getNpcTemplate().get_npcId() == 45889) {
										pc.sendPackets(new S_SystemMessage(
												"아무일도 일어나지 않았습니다."), true);
										return;
									}
								}

							}
							L1SpawnUtil.spawn(pc, 45889, 0, 300000, false);
						} else {
							pc.sendPackets(new S_SystemMessage(
									"아무일도 일어나지 않았습니다."), true);
						}
					} else if (itemId == 40572) {
						if (pc.getX() == 32778 && pc.getY() == 32738
								&& pc.getMapId() == 21) {
							if (pc.Sabutelok()) {
								pc.dx = 32781;
								pc.dy = 32728;
								pc.dm = (short) 21;
								pc.dh = pc.getMoveState().getHeading();
								pc.setTelType(7);
								pc.sendPackets(new S_SabuTell(pc), true);
							}
						} else if (pc.getX() == 32781 && pc.getY() == 32728
								&& pc.getMapId() == 21) {
							if (pc.Sabutelok()) {
								pc.dx = 32778;
								pc.dy = 32738;
								pc.dm = (short) 21;
								pc.dh = pc.getMoveState().getHeading();
								pc.setTelType(7);
								pc.sendPackets(new S_SabuTell(pc), true);
							}
						} else {
							pc.sendPackets(new S_SystemMessage(
									"아무일도 일어나지 않았습니다."), true);
						}
					} else if (itemId == 40009) {// 추방막대
						/*
						 * int chargeCount = useItem.getChargeCount(); if
						 * (chargeCount <= 0) { pc.sendPackets(new
						 * S_SystemMessage("아무일도 일어나지 않았습니다."), true);// \f1
						 * 아무것도 일어나지 않았습니다. return; }
						 */
						L1Object target = L1World.getInstance().findObject(
								spellsc_objid);
						if (target != null && target instanceof L1Character) {
							L1Character cha2 = (L1Character) target;
							if (pc != target) {
								int heding = CharPosUtil.targetDirection(pc,
										spellsc_x, spellsc_y);
								pc.getMoveState().setHeading(heding);
							}
							pc.sendPackets(new S_AttackPacket(pc, 0,
									ActionCodes.ACTION_Wand), true);
							Broadcaster.broadcastPacket(pc, new S_AttackPacket(
									pc, 0, ActionCodes.ACTION_Wand), true);

							if (cha2.getSkillEffectTimerSet().hasSkillEffect(
									COUNTER_MAGIC)) {
								cha2.getSkillEffectTimerSet()
										.removeSkillEffect(COUNTER_MAGIC);
								int castgfx = SkillsTable.getInstance()
										.getTemplate(COUNTER_MAGIC)
										.getCastGfx();
								Broadcaster
										.broadcastPacket(cha2,
												new S_SkillSound(cha2.getId(),
														castgfx), true);
								if (cha2 instanceof L1PcInstance) {
									L1PcInstance pc2 = (L1PcInstance) cha2;
									pc2.sendPackets(
											new S_SkillSound(pc2.getId(),
													castgfx), true);
								}
								return;
							}

							if (target instanceof L1PcInstance) {
								L1PcInstance cha = (L1PcInstance) target;
								if (pc != cha) {
									if (pc.getLevel() > cha.getLevel()
											&& 30 < _random.nextInt(100)) {
										if (!L1CastleLocation
												.checkInAllWarArea(cha.getX(),
														cha.getY(),
														cha.getMapId())
												&& (CharPosUtil
														.getZoneType(cha) == 0 || CharPosUtil
														.getZoneType(cha) == -1)) {
											byte HEADING_TABLE_X[] = { 0, 1, 1,
													1, 0, -1, -1, -1 };
											byte HEADING_TABLE_Y[] = { -1, -1,
													0, 1, 1, 1, 0, -1 };
											int heading = _random.nextInt(8);
											heading = checkObject(cha.getX(),
													cha.getY(), cha.getMapId(),
													heading);
											if (heading != -1) {
												int tempx = HEADING_TABLE_X[heading];
												int tempy = HEADING_TABLE_Y[heading];
												if (cha.Sabutelok()) {
													cha.dx = cha.getX() + tempx;
													cha.dy = cha.getY() + tempy;
													cha.dm = (short) cha
															.getLocation()
															.getMapId();
													cha.dh = cha.getMoveState()
															.getHeading();
													cha.setTelType(10);
													cha.sendPackets(
															new S_SabuTell(cha),
															true);
												}
											}
											HEADING_TABLE_X = null;
											HEADING_TABLE_Y = null;
										}
									}
								}
								if (cha.getSkillEffectTimerSet()
										.hasSkillEffect(ERASE_MAGIC)) {
									cha.getSkillEffectTimerSet()
											.removeSkillEffect(ERASE_MAGIC);
								}
							}
						}

						// useItem.setChargeCount(useItem.getChargeCount() - 1);
						// pc.getInventory().updateItem(useItem,
						// L1PcInventory.COL_CHARGE_COUNT);
						// if (useItem.getChargeCount() == 0){
						pc.getInventory().removeItem(useItem, 1);
						// }

						if (useItem.isIdentified()) {
							useItem.setIdentified(true);
							pc.sendPackets(new S_ItemName(useItem), true);
						}

					} else if (itemId == L1ItemId.ICECAVE_KEY) {
						L1Object t = L1World.getInstance().findObject(
								spellsc_objid);
						L1DoorInstance door = (L1DoorInstance) t;
						if (pc.getLocation().getTileLineDistance(
								door.getLocation()) > 3) {
							return;
						}
						if (door.getDoorId() >= 5000
								&& door.getDoorId() <= 5009) {
							if (door != null
									&& door.getOpenStatus() == ActionCodes.ACTION_Close) {
								door.open();
								pc.getInventory().removeItem(useItem, 1);
							}
						}
					} else if (itemId == 60202 || itemId >= 40289 && itemId <= 40297) { // 오만의 탑11~91층 부적
						useToiTeleportAmulet(pc, itemId, useItem);
					} else if (itemId >= 40280 && itemId <= 40288) {
						// 봉인된 오만의 탑 11~91층 부적
						pc.getInventory().removeItem(useItem, 1);
						L1ItemInstance item = pc.getInventory().storeItem(itemId + 9, 1);
						if (item != null) {
							pc.sendPackets(new S_ServerMessage(403, item.getLogName()), true);
						}
					} else if (itemId == 41301) { // 샤이닝렛드핏슈
						int chance = _random.nextInt(10);
						if (chance >= 0 && chance < 5) {
							UseHeallingPotion(pc, 15, 189);
						} else if (chance >= 5 && chance < 9) {
							createNewItem(pc, 40019, 1);
						} else if (chance >= 9) {
							int gemChance = _random.nextInt(3);
							if (gemChance == 0) {
								createNewItem(pc, 40045, 1);
							} else if (gemChance == 1) {
								createNewItem(pc, 40049, 1);
							} else if (gemChance == 2) {
								createNewItem(pc, 40053, 1);
							}
						}
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 41302) { // 샤이닝그린핏슈
						int chance = _random.nextInt(3);
						if (chance >= 0 && chance < 5) {
							UseHeallingPotion(pc, 15, 189);
						} else if (chance >= 5 && chance < 9) {
							createNewItem(pc, 40018, 1);
						} else if (chance >= 9) {
							int gemChance = _random.nextInt(3);
							if (gemChance == 0) {
								createNewItem(pc, 40047, 1);
							} else if (gemChance == 1) {
								createNewItem(pc, 40051, 1);
							} else if (gemChance == 2) {
								createNewItem(pc, 40055, 1);
							}
						}
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 41303) { // 샤이닝브르핏슈
						int chance = _random.nextInt(3);
						if (chance >= 0 && chance < 5) {
							UseHeallingPotion(pc, 15, 189);
						} else if (chance >= 5 && chance < 9) {
							createNewItem(pc, 40015, 1);
						} else if (chance >= 9) {
							int gemChance = _random.nextInt(3);
							if (gemChance == 0) {
								createNewItem(pc, 40046, 1);
							} else if (gemChance == 1) {
								createNewItem(pc, 40050, 1);
							} else if (gemChance == 2) {
								createNewItem(pc, 40054, 1);
							}
						}
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 41304) { // 샤이닝화이트핏슈
						int chance = _random.nextInt(3);
						if (chance >= 0 && chance < 5) {
							UseHeallingPotion(pc, 15, 189);
						} else if (chance >= 5 && chance < 9) {
							createNewItem(pc, 40021, 1);
						} else if (chance >= 9) {
							int gemChance = _random.nextInt(3);
							if (gemChance == 0) {
								createNewItem(pc, 40044, 1);
							} else if (gemChance == 1) {
								createNewItem(pc, 40048, 1);
							} else if (gemChance == 2) {
								createNewItem(pc, 40052, 1);
							}
						}
						pc.getInventory().removeItem(useItem, 1);
					} else if ((itemId >= 40104 && itemId <= 40112) || (itemId >= 42029 && itemId <= 42039)
							|| (itemId >= 5000163 && itemId <= 5000171) || itemId == 60200) { // 오만...층줌 씨발
						if(pc.getMapId() >= 12852 && pc.getMapId() <= 12862){
							pc.sendPackets(new S_SystemMessage("이곳에서 텔레포트를 할 수 없습니다."));
							return;
						}
						if (((L1EtcItem) useItem.getItem()).get_mapid() == pc.getMapId()) {// 층 랜덤텔
							L1Location loc = L1Location.saburan(pc.getMap());
							if (pc.Sabutelok()) {
								pc.dx = loc.getX();
								pc.dy = loc.getY();
								pc.dm = ((L1EtcItem) useItem.getItem()).get_mapid();
								pc.dh = pc.getMoveState().getHeading();
								pc.setTelType(7);
								pc.sendPackets(new S_SabuTell(pc), true);
							}
						} else {
							if (pc.getMap().isEscapable()) {
								if (pc.Sabutelok()) {
									pc.dx = ((L1EtcItem) useItem.getItem()).get_locx();
									pc.dy = ((L1EtcItem) useItem.getItem()).get_locy();
									pc.dm = ((L1EtcItem) useItem.getItem()).get_mapid();
									pc.dh = pc.getMoveState().getHeading();
									pc.setTelType(7);
									pc.sendPackets(new S_SabuTell(pc), true);
								}
							} else {
								pc.sendPackets(new S_SystemMessage("이곳에서 텔레포트를 할 수 없습니다."));
								return;
							}
						}
						pc.getInventory().removeItem(useItem, 1);

					} else if (itemId == 40615) { // 그림자의 신전 2층의 열쇠
						if (pc.getMapId() == 522 && (pc.getX() >= 32702 && pc.getX() <= 32707 && pc.getY() >= 32893 && pc.getY() <= 32898)) {
							if (pc.getMap().isEscapable()) { // 귀환가능지역인가를 검색한다
								if (pc.Sabutelok()) {
									pc.dx = ((L1EtcItem) useItem.getItem())
											.get_locx();
									pc.dy = ((L1EtcItem) useItem.getItem())
											.get_locy();
									pc.dm = ((L1EtcItem) useItem.getItem())
											.get_mapid();
									pc.dh = pc.getMoveState().getHeading();
									pc.setTelType(7);
									pc.sendPackets(new S_SabuTell(pc), true);
								}
							} else {
								// \f1 아무것도 일어나지 않았습니다.
								pc.sendPackets(new S_SystemMessage(
										"아무일도 일어나지 않았습니다."), true);
							}
						} else {
							// \f1 아무것도 일어나지 않았습니다.
							pc.sendPackets(new S_SystemMessage(
									"아무일도 일어나지 않았습니다."), true);
						}

					} else if (itemId == 437011 || itemId == 60207) { // 드진, 시원
																		// 통쾌한
																		// 물약
						/*
						 * if (pc.getLevel() < 80) { pc.sendPackets(new
						 * S_SystemMessage("\\fY80이하는 드래곤의 진주를 복용 하실 수 없습니다."));
						 * return; }
						 */
						pc.sendPackets(new S_ServerMessage(1065), true); // 드진
																			// 멘트
						진주포션사용(pc, itemId);
						// pc.getInventory().consumeItem(437011, 1);//해당아이템 삭제
						pc.getInventory().removeItem(useItem, 1);

					} else if (itemId == 60055 || itemId == 60250) { // 광분의 물약,
																		// 질주의
																		// 주문서
						pc.sendPackets(new S_ServerMessage(1065), true); // 드진
																			// 멘트
						진주포션사용(pc, itemId);// 7929
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 40616 || itemId == 40782
							|| itemId == 40783) { // 그림자의 신전 3층의 열쇠

						if (pc.getMapId() == 523
								&& (pc.getX() >= 32698 && pc.getX() <= 32704
										&& pc.getY() >= 32892 && pc.getY() <= 32898)) {
							if (pc.getMap().isEscapable()) { // 귀환가능지역인가를 검색한다
								if (pc.Sabutelok()) {
									pc.dx = ((L1EtcItem) useItem.getItem())
											.get_locx();
									pc.dy = ((L1EtcItem) useItem.getItem())
											.get_locy();
									pc.dm = ((L1EtcItem) useItem.getItem())
											.get_mapid();
									pc.dh = pc.getMoveState().getHeading();
									pc.setTelType(7);
									pc.sendPackets(new S_SabuTell(pc), true);
								}
							} else {
								// \f1 아무것도 일어나지 않았습니다.
								pc.sendPackets(new S_SystemMessage(
										"아무일도 일어나지 않았습니다."), true);
							}
						} else {
							pc.sendPackets(new S_SystemMessage(
									"아무일도 일어나지 않았습니다."), true);
						}

					} else if (itemId == 40692) { // 완성된 보물의 지도
						if (pc.getInventory().checkItem(40621)) {
							// \f1 아무것도 일어나지 않았습니다.
							pc.sendPackets(new S_SystemMessage(
									"아무일도 일어나지 않았습니다."), true);
						} else if ((pc.getX() >= 32856 && pc.getX() <= 32858)
								&& (pc.getY() >= 32857 && pc.getY() <= 32858)
								&& pc.getMapId() == 443) { // 해적섬의 지하 감옥 3층
							if (pc.Sabutelok()) {
								pc.dx = ((L1EtcItem) useItem.getItem()).get_locx();
								pc.dy = ((L1EtcItem) useItem.getItem()).get_locy();
								pc.dm = ((L1EtcItem) useItem.getItem()).get_mapid();
								pc.dh = pc.getMoveState().getHeading();
								pc.setTelType(7);
								pc.sendPackets(new S_SabuTell(pc), true);
							}
						} else {
							// \f1 아무것도 일어나지 않았습니다.
							pc.sendPackets(new S_SystemMessage(
									"아무일도 일어나지 않았습니다."), true);
						}
					} else if (itemId == 6105) {
						for (L1Object obj : L1World.getInstance().getObject()) {
							if (obj instanceof L1BoardInstance) {
								L1NpcInstance board = (L1NpcInstance) obj;
								if (board.getNpcTemplate().get_npcId() == 4200012) {
									pc.sendPackets(new S_Ranking(board));
									break;
								}
							}
						}
					} else if (itemId == 41146) { // 드로몬드의 초대장
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(), "ei001"), true);
					} else if (itemId == 41209) { // 포피레아의 의뢰서
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(), "ei002"), true);
					} else if (itemId == 41210) { // 연마재
						pc.sendPackets(
								new S_NPCTalkReturn(pc.getId(), "ei003"), true);
					} else if (itemId == 41211) { // 허브
						pc.sendPackets(
								new S_NPCTalkReturn(pc.getId(), "ei004"), true);
					} else if (itemId == 41212) { // 특제 캔디
						pc.sendPackets(
								new S_NPCTalkReturn(pc.getId(), "ei005"), true);
					} else if (itemId == 41213) { // 티미의 바스켓
						pc.sendPackets(
								new S_NPCTalkReturn(pc.getId(), "ei006"), true);
					} else if (itemId == 41214) { // 운의 증거
						pc.sendPackets(
								new S_NPCTalkReturn(pc.getId(), "ei012"), true);
					} else if (itemId == 41215) { // 지의 증거
						pc.sendPackets(
								new S_NPCTalkReturn(pc.getId(), "ei010"), true);
					} else if (itemId == 41216) { // 력의 증거
						pc.sendPackets(
								new S_NPCTalkReturn(pc.getId(), "ei011"), true);
					} else if (itemId == 41222) { // 마슈르
						pc.sendPackets(
								new S_NPCTalkReturn(pc.getId(), "ei008"), true);
					} else if (itemId == 41223) { // 무기의 파편
						pc.sendPackets(
								new S_NPCTalkReturn(pc.getId(), "ei007"), true);
					} else if (itemId == 41224) { // 배지
						pc.sendPackets(
								new S_NPCTalkReturn(pc.getId(), "ei009"), true);
					} else if (itemId == 41225) { // 케스킨의 발주서
						pc.sendPackets(
								new S_NPCTalkReturn(pc.getId(), "ei013"), true);
					} else if (itemId == 41226) { // 파고의 약
						pc.sendPackets(
								new S_NPCTalkReturn(pc.getId(), "ei014"), true);
					} else if (itemId == 41227) { // 알렉스의 소개장
						pc.sendPackets(
								new S_NPCTalkReturn(pc.getId(), "ei033"), true);
					} else if (itemId == 41228) { // 율법박사의 부적
						pc.sendPackets(
								new S_NPCTalkReturn(pc.getId(), "ei034"), true);
					} else if (itemId == 41229) { // 스켈리턴의 머리
						pc.sendPackets(
								new S_NPCTalkReturn(pc.getId(), "ei025"), true);
					} else if (itemId == 41230) { // 지난에의 편지
						pc.sendPackets(
								new S_NPCTalkReturn(pc.getId(), "ei020"), true);
					} else if (itemId == 41231) { // 맛티에의 편지
						pc.sendPackets(
								new S_NPCTalkReturn(pc.getId(), "ei021"), true);
					} else if (itemId == 41233) { // 케이이에의 편지
						pc.sendPackets(
								new S_NPCTalkReturn(pc.getId(), "ei019"), true);
					} else if (itemId == 41234) { // 뼈가 들어온 봉투
						pc.sendPackets(
								new S_NPCTalkReturn(pc.getId(), "ei023"), true);
					} else if (itemId == 41235) { // 재료표
						pc.sendPackets(
								new S_NPCTalkReturn(pc.getId(), "ei024"), true);
					} else if (itemId == 41236) { // 본아챠의 뼈
						pc.sendPackets(
								new S_NPCTalkReturn(pc.getId(), "ei026"), true);
					} else if (itemId == 41237) { // 스켈리턴 스파이크의 뼈
						pc.sendPackets(
								new S_NPCTalkReturn(pc.getId(), "ei027"), true);
					} else if (itemId == 41239) { // 브트에의 편지
						pc.sendPackets(
								new S_NPCTalkReturn(pc.getId(), "ei018"), true);
					} else if (itemId == 41240) { // 페다에의 편지
						pc.sendPackets(
								new S_NPCTalkReturn(pc.getId(), "ei022"), true);
					} else if (itemId == 41060) { // 노나메의 추천서
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
								"nonames"), true);
					} else if (itemId == 41061) { // 조사단의 증서：에르프 지역 두다마라카메
						pc.sendPackets(
								new S_NPCTalkReturn(pc.getId(), "kames"), true);
					} else if (itemId == 41062) { // 조사단의 증서：인간 지역 네르가바크모
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
								"bakumos"), true);
					} else if (itemId == 41063) { // 조사단의 증서：정령 지역 두다마라브카
						pc.sendPackets(
								new S_NPCTalkReturn(pc.getId(), "bukas"), true);
					} else if (itemId == 41064) { // 조사단의 증서：오크 지역 네르가후우모
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
								"huwoomos"), true);
					} else if (itemId == 41065) { // 조사단의 증서：조사단장 아트바노아
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(), "noas"),
								true);
					} else if (itemId == 41356) { // 파룸의 자원 리스트
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
								"rparum3"), true);
					} else if (itemId == 40701) { // 작은 보물의 지도
						if (pc.getQuest().get_step(L1Quest.QUEST_LUKEIN1) == 1) {
							pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
									"firsttmap"), true);
						} else if (pc.getQuest()
								.get_step(L1Quest.QUEST_LUKEIN1) == 2) {
							pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
									"secondtmapa"), true);
						} else if (pc.getQuest()
								.get_step(L1Quest.QUEST_LUKEIN1) == 3) {
							pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
									"secondtmapb"), true);
						} else if (pc.getQuest()
								.get_step(L1Quest.QUEST_LUKEIN1) == 4) {
							pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
									"secondtmapc"), true);
						} else if (pc.getQuest()
								.get_step(L1Quest.QUEST_LUKEIN1) == 5) {
							pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
									"thirdtmapd"), true);
						} else if (pc.getQuest()
								.get_step(L1Quest.QUEST_LUKEIN1) == 6) {
							pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
									"thirdtmape"), true);
						} else if (pc.getQuest()
								.get_step(L1Quest.QUEST_LUKEIN1) == 7) {
							pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
									"thirdtmapf"), true);
						} else if (pc.getQuest()
								.get_step(L1Quest.QUEST_LUKEIN1) == 8) {
							pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
									"thirdtmapg"), true);
						} else if (pc.getQuest()
								.get_step(L1Quest.QUEST_LUKEIN1) == 9) {
							pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
									"thirdtmaph"), true);
						} else if (pc.getQuest()
								.get_step(L1Quest.QUEST_LUKEIN1) == 10) {
							pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
									"thirdtmapi"), true);
						}
					} else if (itemId == 40663) { // 아들의 편지
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
								"sonsletter"), true);
					} else if (itemId == 40630) { // 디에고의 낡은 일기
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
								"diegodiary"), true);
					} else if (itemId == 41340) { // 용병단장 티온
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(), "tion"),
								true);
					} else if (itemId == 41317) { // 랄슨의 추천장
						pc.sendPackets(
								new S_NPCTalkReturn(pc.getId(), "rarson"), true);
					} else if (itemId == 41318) { // 쿠엔의 메모
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(), "kuen"),
								true);
					} else if (itemId == 41329) { // 박제의 제작 의뢰서
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
								"anirequest"), true);
					} else if (itemId == 41346) { // 로빈훗드의 메모 1
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
								"robinscroll"), true);
					} else if (itemId == 41347) { // 로빈훗드의 메모 2
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
								"robinscroll2"), true);
					} else if (itemId == 41348) { // 로빈훗드의 소개장
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
								"robinhood"), true);
					} else if (itemId == 41007) {
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
								"erisscroll"), true);
					} else if (itemId == 41009) {
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
								"erisscroll2"), true);
					} else if (itemId == 41019) {
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
								"lashistory1"), true);
					} else if (itemId == 41020) {
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
								"lashistory2"), true);
					} else if (itemId == 41021) {
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
								"lashistory3"), true);
					} else if (itemId == 41022) {
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
								"lashistory4"), true);
					} else if (itemId == 41023) {
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
								"lashistory5"), true);
					} else if (itemId == 41024) {
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
								"lashistory6"), true);
					} else if (itemId == 41025) {
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
								"lashistory7"), true);
					} else if (itemId == 41026) {
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
								"lashistory8"), true);
					} else if (itemId == 210087) { // 프로켈의 첫 번째 지령서
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
								"first_p"), true);
					} else if (itemId == 210093) { // 실레인의 첫 번째 편지
						pc.sendPackets(new S_NPCTalkReturn(pc.getId(),
								"silrein1lt"), true);
					} else if (itemId == 41208) { // 져 가는 영혼
						if ((pc.getX() >= 32844 && pc.getX() <= 32845)
								&& (pc.getY() >= 32693 && pc.getY() <= 32694)
								&& pc.getMapId() == 550) { // 배의 묘지:지상층
							if (pc.Sabutelok()) {
								pc.dx = ((L1EtcItem) useItem.getItem())
										.get_locx();
								pc.dy = ((L1EtcItem) useItem.getItem())
										.get_locy();
								pc.dm = ((L1EtcItem) useItem.getItem())
										.get_mapid();
								pc.dh = pc.getMoveState().getHeading();
								pc.setTelType(7);
								pc.sendPackets(new S_SabuTell(pc), true);
							}
						} else {
							// \f1 아무것도 일어나지 않았습니다.
							pc.sendPackets(new S_SystemMessage(
									"아무일도 일어나지 않았습니다."), true);
						}
					} else if (itemId == 40700) { // 실버 플룻
						pc.sendPackets(new S_Sound(10), true);
						Broadcaster.broadcastPacket(pc, new S_Sound(10), true);
						if ((pc.getX() >= 32619 && pc.getX() <= 32623)
								&& (pc.getY() >= 33120 && pc.getY() <= 33124)
								&& pc.getMapId() == 440) { // 해적 시마마에반매직 스퀘어 좌표
							boolean found = false;
							L1MonsterInstance mon = null;
							for (L1Object obj : L1World.getInstance()
									.getObject()) {
								if (obj instanceof L1MonsterInstance) {
									mon = (L1MonsterInstance) obj;
									if (mon != null) {
										if (mon.getNpcTemplate().get_npcId() == 45875) {
											found = true;
											break;
										}
									}
								}

							}
							if (found) {
							} else {
								L1SpawnUtil.spawn(pc, 45875, 0, 0, false);
							}
						}
					} else if (itemId == 41121) {
						if (pc.getQuest().get_step(L1Quest.QUEST_SHADOWS) == L1Quest.QUEST_END
								|| pc.getInventory().checkItem(41122, 1)) {
							pc.sendPackets(new S_SystemMessage(
									"아무일도 일어나지 않았습니다."), true);
						} else {
							createNewItem(pc, 41122, 1);
						}
					} else if (itemId == 41130) {
						if (pc.getQuest().get_step(L1Quest.QUEST_DESIRE) == L1Quest.QUEST_END
								|| pc.getInventory().checkItem(41131, 1)) {
							pc.sendPackets(new S_SystemMessage(
									"아무일도 일어나지 않았습니다."), true);
						} else {
							createNewItem(pc, 41131, 1);
						}

					} else if (itemId == 762676) { // 스톰 워크
						if (pc.getMap().isEscapable() || pc.isGm()) {
							if (pc.Sabutelok()) {
								pc.dx = 32799;
								pc.dy = 32801;
								pc.dm = 6202;
								pc.dh = pc.getMoveState().getHeading();
								pc.setTelType(7);
								pc.sendPackets(new S_SabuTell(pc), true);
							}
						} else {
							pc.sendPackets(new S_ServerMessage(647), true);
						}
					} else if (itemId == 42501) { // 스톰 워크
						// 33495 33103
						// if(!pc.isTeleport() && !pc.텔대기()){
						if (pc.Sabutelok()) {
							pc.dx = spellsc_x;
							pc.dy = spellsc_y;
							pc.dm = pc.getMapId();
							pc.dh = pc.getMoveState().getHeading();

							pc.setTelType(12);
							pc.sendPackets(new S_SabuTell(pc), true);
						}
						// System.out.println("하이네잡밭5.add(new Robot_Location_bean("+spellsc_x+", "+spellsc_y+", 4));");
						// }
						// L1Teleport.CHANGE_POSITION);

					} else if (itemId == 50101) { // 위치막대
						IdentMapWand(pc, spellsc_x, spellsc_y);
					} else if (itemId == 50102) { // 위치변경막대
						MapFixKeyWand(pc, spellsc_x, spellsc_y);
					} else if (itemId == 41260) { // 신
						for (L1Object object : L1World.getInstance()
								.getVisibleObjects(pc, 3)) {
							if (object instanceof L1EffectInstance) {
								if (((L1NpcInstance) object).getNpcTemplate()
										.get_npcId() == 81170) {
									pc.sendPackets(new S_ServerMessage(1162),
											true); // 벌써 주위에 모닥불이 있습니다.
									return;
								}
							}
						}
						int[] loc = new int[2];
						loc = CharPosUtil.getFrontLoc(pc.getX(), pc.getY(), pc
								.getMoveState().getHeading());
						L1EffectSpawn.getInstance().spawnEffect(81170, 600000,
								loc[0], loc[1], pc.getMapId());
						pc.getInventory().removeItem(useItem, 1);
						loc = null;
					} else if (itemId == 41345) { // 산성의 유액
						L1DamagePoison.doInfection(pc, pc, 3000, 5);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 41315) { // 성수
						if (pc.getSkillEffectTimerSet().hasSkillEffect(
								STATUS_HOLY_WATER_OF_EVA)) {
							pc.sendPackets(new S_SystemMessage(
									"아무일도 일어나지 않았습니다."), true); // \f1 아무것도 일어나지
																// 않았습니다.
							return;
						}
						if (pc.getSkillEffectTimerSet().hasSkillEffect(
								STATUS_HOLY_MITHRIL_POWDER)) {
							pc.getSkillEffectTimerSet().removeSkillEffect(
									STATUS_HOLY_MITHRIL_POWDER);
						}
						pc.getSkillEffectTimerSet().setSkillEffect(
								STATUS_HOLY_WATER, 900 * 1000);
						pc.sendPackets(new S_SkillSound(pc.getId(), 190), true);
						Broadcaster.broadcastPacket(pc,
								new S_SkillSound(pc.getId(), 190), true);
						pc.sendPackets(new S_ServerMessage(1141), true);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 41316) { // 신성한 미스리르파우다
						if (pc.getSkillEffectTimerSet().hasSkillEffect(
								STATUS_HOLY_WATER_OF_EVA)) {
							pc.sendPackets(new S_SystemMessage(
									"아무일도 일어나지 않았습니다."), true); // \f1 아무것도 일어나지
																// 않았습니다.
							return;
						}
						if (pc.getSkillEffectTimerSet().hasSkillEffect(
								STATUS_HOLY_WATER)) {
							pc.getSkillEffectTimerSet().removeSkillEffect(
									STATUS_HOLY_WATER);
						}
						pc.getSkillEffectTimerSet().setSkillEffect(
								STATUS_HOLY_MITHRIL_POWDER, 900 * 1000);
						pc.sendPackets(new S_SkillSound(pc.getId(), 190), true);
						Broadcaster.broadcastPacket(pc,
								new S_SkillSound(pc.getId(), 190), true);
						pc.sendPackets(new S_ServerMessage(1142), true);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == 41354) { // 신성한 에바의 물
						if (pc.getSkillEffectTimerSet().hasSkillEffect(
								STATUS_HOLY_WATER)
								|| pc.getSkillEffectTimerSet().hasSkillEffect(
										STATUS_HOLY_MITHRIL_POWDER)) {
							pc.sendPackets(new S_SystemMessage(
									"아무일도 일어나지 않았습니다."), true); // \f1 아무것도 일어나지
																// 않았습니다.
							return;
						}
						pc.getSkillEffectTimerSet().setSkillEffect(
								STATUS_HOLY_WATER_OF_EVA, 900 * 1000);
						pc.sendPackets(new S_SkillSound(pc.getId(), 190), true);
						Broadcaster.broadcastPacket(pc,
								new S_SkillSound(pc.getId(), 190), true);
						pc.sendPackets(new S_ServerMessage(1140), true);
						pc.getInventory().removeItem(useItem, 1);

					} else if (itemId == 500076) { // 올버프 물약 올클래스
						pc.setBuffnoch(1);
						int[] allBuffSkill = { 26, 42, 158,148, 129,151,137,88,79,48};
						L1SkillUse l1skilluse = new L1SkillUse();
						for (int i = 0; i < allBuffSkill.length; i++) {
							l1skilluse.handleCommands(pc, allBuffSkill[i],
									pc.getId(), pc.getX(), pc.getY(), null, 0,
									L1SkillUse.TYPE_GMBUFF);
						}
						l1skilluse = null;
						allBuffSkill = null;
						pc.getInventory().removeItem(useItem, 1);
						// 1Teleport.teleport(pc, pc.getX(), pc.getY(),
						// pc.getMapId(), pc.getHeading(), false);
						pc.sendPackets(new S_SystemMessage(
								"\\fV몸 속에서 뜨거운 기운이 솟구칩니다."), true);
						pc.setBuffnoch(0);
						
					} else if (itemId == 6000035) { //생명의마안 일회용
						pc.setBuffnoch(1);
						int[] allBuffSkill = { 7677};
						L1SkillUse l1skilluse = new L1SkillUse();
						for (int i = 0; i < allBuffSkill.length; i++) {
							l1skilluse.handleCommands(pc, allBuffSkill[i],
									pc.getId(), pc.getX(), pc.getY(), null, 0,
									L1SkillUse.TYPE_GMBUFF);
						}
						l1skilluse = null;
						allBuffSkill = null;
						pc.getInventory().removeItem(useItem, 1);
						// 1Teleport.teleport(pc, pc.getX(), pc.getY(),
						// pc.getMapId(), pc.getHeading(), false);

						pc.setBuffnoch(0);

					} else if (itemId == 500077) { // 올버프 물약 요정법사환술사
						pc.setBuffnoch(1);
						int[] allBuffSkill = { 26, 42, 48, 54, 79, 137, 138,
								129, 160, 166, 206, 209, 151, 158 };
						L1SkillUse l1skilluse = new L1SkillUse();
						for (int i = 0; i < allBuffSkill.length; i++) {
							l1skilluse.handleCommands(pc, allBuffSkill[i],
									pc.getId(), pc.getX(), pc.getY(), null, 0,
									L1SkillUse.TYPE_GMBUFF);
						}
						l1skilluse = null;
						allBuffSkill = null;
						pc.getInventory().removeItem(useItem, 1);
						// 1Teleport.teleport(pc, pc.getX(), pc.getY(),
						// pc.getMapId(), pc.getHeading(), false);
						pc.sendPackets(new S_SystemMessage(
								"\\fV몸 속에서 알수없는 기운이 솟구칩니다."), true);
						pc.setBuffnoch(0);

						/*
						 * } else if (itemId == 5000039 || itemId == 5000040 ||
						 * itemId == 5000041 || itemId == 5000042 || itemId ==
						 * 5000043 || itemId == 5000044 || itemId == 5000045){
						 * //드래곤볼 if (pc.getInventory(). checkItem(5000039, 1)
						 * &&pc.getInventory(). checkItem(5000040, 1) &&
						 * pc.getInventory(). checkItem(5000041, 1) &&//수량체크부분
						 * pc.getInventory(). checkItem(5000042, 1)
						 * &&pc.getInventory(). checkItem(5000043, 1) &&
						 * pc.getInventory(). checkItem(5000044, 1) && //수량체크부분
						 * pc.getInventory(). checkItem(5000045, 1) ){ //수량체크부분
						 * pc.getInventory().consumeItem(5000039, 1);
						 * pc.getInventory().consumeItem(5000040, 1);
						 * pc.getInventory().consumeItem(5000041, 1);
						 * pc.getInventory().consumeItem(5000042, 1);
						 * pc.getInventory().consumeItem(5000043, 1);
						 * pc.getInventory().consumeItem(5000044, 1);
						 * pc.getInventory().consumeItem(5000045, 1);
						 * 
						 * pc.sendPackets(new S_ChatPacket(pc, "드래곤이여...나와라!!" ,
						 * Opcodes.S_SAY, 2), true);
						 * pc.sendPackets(new S_DoActionGFX(pc.getId(), 18),
						 * true); pc.broadcastPacket(new
						 * S_DoActionGFX(pc.getId() , 18), true);
						 * pc.sendPackets(new S_SkillSound(pc.getId() , 7473),
						 * true); pc.broadcastPacket(new S_SkillSound(pc.getId()
						 * , 7473), true); Thread.sleep(3000);
						 * pc.sendPackets(new S_ChatPacket(pc,
						 * "이제..잠에서 깨어나 ...소원을 들어주시오!!" ,
						 * Opcodes.S_SAY, 2), true);
						 * pc.sendPackets(new S_DoActionGFX(pc.getId(), 18),
						 * true); pc.broadcastPacket(new
						 * S_DoActionGFX(pc.getId() , 18), true);
						 * pc.sendPackets(new S_SkillSound(pc.getId() , 7470),
						 * true); pc.broadcastPacket(new S_SkillSound(pc.getId()
						 * , 7470), true); Thread.sleep(4000); if
						 * (pc.getMap().isUsePainwand()) { int[] mobArray = {
						 * 45000172 };//드래곤볼 클릭시 나오는 몹 int rnd =
						 * _random.nextInt(mobArray.length);
						 * L1SpawnUtil.spawn(pc, mobArray[rnd], 5, 60*1000,
						 * true); mobArray = null; }else{ pc.sendPackets(new
						 * S_SystemMessage("아무일도 일어나지 않았습니다."), true);}
						 * pc.getInventory().removeItem(useItem, 1);
						 * Thread.sleep(5000);
						 * L1World.getInstance().broadcastPacketToAll(new
						 * S_PacketBox(S_PacketBox.GREEN_MESSAGE,
						 * "드래곤볼을 모은자여...소원을 ..말하거라 ..어떤 소원이라도 들어주마 ..."),
						 * true); pc.sendPackets(new
						 * S_SystemMessage("\\fT.소원 [아이템이름] 라고 입력. ex).소원 싸울아비장검"
						 * ), true); }else{ }
						 */

						/*
						 * } else if (itemId == 550080){ //미션 임파서블 if
						 * (pc.getInventory(). checkItem(550080, 1)
						 * &&pc.getInventory(). checkItem(550081, 1) &&
						 * pc.getInventory(). checkItem(550082, 1) &&//수량체크부분
						 * pc.getInventory(). checkItem(550083, 1)
						 * &&pc.getInventory(). checkItem(550084, 1) &&
						 * pc.getInventory(). checkItem(550085, 1) && //수량체크부분
						 * pc.getInventory(). checkItem(550086, 1) ){ //수량체크부분
						 * pc.getInventory().consumeItem(550080, 1);
						 * pc.getInventory().consumeItem(550081, 1);
						 * pc.getInventory().consumeItem(550082, 1);
						 * pc.getInventory().consumeItem(550083, 1);
						 * pc.getInventory().consumeItem(550084, 1);
						 * pc.getInventory().consumeItem(550085, 1);
						 * pc.getInventory().consumeItem(550086, 1);
						 * 
						 * pc.getInventory().storeItem(550014, 1);
						 * pc.sendPackets(new S_SkillSound(pc.getId() , 2568),
						 * true); Broadcaster.broadcastPacket(pc, new
						 * S_SkillSound(pc.getId(), 2568), true); // 클릭시 임팩부분
						 * 
						 * pc.sendPackets(new S_SkillSound(pc.getId(), 1249),
						 * true); // 추가했습니다
						 * 
						 * Broadcaster.broadcastPacket(pc, new
						 * S_SkillSound(pc.getId(), 1249), true); // 추가했습니다
						 * pc.sendPackets(new
						 * S_SystemMessage("7개의 조각이 하나로 합쳐져 보물상자가 완성되었습니다."),
						 * true); //성공시 멘트전송
						 * 
						 * } else { pc.sendPackets(new
						 * S_SystemMessage("보물상자 조각 7개가 다 모이지 않았습니다."), true);
						 * //조건이 맞지 않을경우 메세지 전송 }
						 */

					} else if (itemId == L1ItemId.CHANGING_SEX_POTION) { // 성전환
																			// 물약
						if (pc.get_sex() == 0) {
							pc.set_sex(1);
							pc.setClassId(FEMALE_LIST[pc.getType()]);
						} else {
							pc.set_sex(0);
							pc.setClassId(MALE_LIST[pc.getType()]);
						}
						pc.getGfxId().setTempCharGfx(pc.getClassId());
						pc.save();
						pc.sendPackets(
								new S_ChangeShape(pc.getId(), pc.getClassId()),
								true);
						Broadcaster.broadcastPacket(pc,
								new S_ChangeShape(pc.getId(), pc.getClassId()),
								true);
						pc.sendPackets(new S_CharVisualUpdate(pc), true);
						Broadcaster.broadcastPacket(pc, new S_CharVisualUpdate(
								pc), true);
						pc.getInventory().removeItem(useItem, 1);
						/************************** 안타라스 리뉴얼 New System *****************************/
					} else if (itemId == L1ItemId.DRAGON_KEY) {
						int pc_castleId = L1CastleLocation
								.getCastleIdByArea(pc);
						if (pc.getMapId() == 53 || pc.getMapId() == 54
								|| pc.getMapId() == 56 || pc.getMapId() == 55
								|| pc_castleId == 1 || pc_castleId == 2
								|| pc_castleId == 3 || pc_castleId == 4
								|| pc_castleId == 5 || pc_castleId == 6
								|| pc_castleId == 7 || pc_castleId == 8)
							if (useItem.getEndTime().getTime() < System.currentTimeMillis()) {
								pc.getInventory().removeItem(useItem);
								pc.sendPackets(new S_SystemMessage(
										"사용 시간이 지나 삭제 합니다."), true);// 만약의 버그를
																	// 대비
							} else {
								pc.sendPackets(new S_SystemMessage(
										"여기에서는 사용할수 없습니다."), true);

								return;
							}

						pc.sendPackets(new S_PacketBox(S_PacketBox.DRAGONMENU,
								useItem), true);
						// AntarasRaidSystem.getInstance().startRaid(pc);

					} else if (itemId == L1ItemId.DRAGON_EMEBOX) {
						int[] DRAGONSCALE = new int[] { 40393, 40394, 40395,
								40396 };
						int bonus = _random.nextInt(100) + 1;
						int rullet = _random.nextInt(100) + 1;
						L1ItemInstance bonusitem = null;
						pc.getInventory().storeItem(L1ItemId.DRAGON_EME, 1);
						pc.sendPackets(new S_ServerMessage(403, "$11518"), true);
						pc.getInventory().removeItem(useItem, 1);
						if (bonus <= 7) {
							bonusitem = pc.getInventory()
									.storeItem(
											DRAGONSCALE[rullet
													% DRAGONSCALE.length], 1);
							pc.sendPackets(new S_ServerMessage(403, bonusitem
									.getItem().getNameId()), true);
					
						} else if (bonus >= 8 && bonus <= 17) {
							bonusitem = pc.getInventory().storeItem(
									L1ItemId.DRAGON_SAPHIRE, 1);
							pc.sendPackets(new S_ServerMessage(403, bonusitem
									.getItem().getNameId()), true);
						} else if (bonus >= 18 && bonus <= 24) {
							bonusitem = pc.getInventory().storeItem(
									L1ItemId.DRAGON_RUBY, 1);
							pc.sendPackets(new S_ServerMessage(403, bonusitem
									.getItem().getNameId()), true);
						} else {
						}
						DRAGONSCALE = null;
					} else if (itemId == 437009) {
						int[] DRAGONSCALE = new int[] { 40393, 40394, 40395,
								40396 };
						int bonus = _random.nextInt(100) + 1;
						int rullet = _random.nextInt(100) + 1;
						L1ItemInstance bonusitem = null;
						pc.getInventory().storeItem(437010, 1);
						pc.sendPackets(new S_ServerMessage(403, "$7969"), true);
						pc.getInventory().removeItem(useItem, 1);
						if (bonus <= 5) {
							bonusitem = pc.getInventory()
									.storeItem(
											DRAGONSCALE[rullet
													% DRAGONSCALE.length], 1);
							pc.sendPackets(new S_ServerMessage(403, bonusitem
									.getItem().getNameId()), true);
				
						} else if (bonus >= 6 && bonus <= 14) {
							bonusitem = pc.getInventory().storeItem(
									L1ItemId.DRAGON_SAPHIRE, 1);
							pc.sendPackets(new S_ServerMessage(403, bonusitem
									.getItem().getNameId()), true);
						} else if (bonus >= 15 && bonus <= 24) {
							bonusitem = pc.getInventory().storeItem(
									L1ItemId.DRAGON_RUBY, 1);
							pc.sendPackets(new S_ServerMessage(403, bonusitem
									.getItem().getNameId()), true);
						} else {
						}
						DRAGONSCALE = null;
					} else if (itemId == 7249) {
						오림상자(pc, useItem);
					} else if (itemId == 7250) {
						오림상자5(pc, useItem);
					} else if (itemId == 1437009) {
						int[] DRAGONSCALE = new int[] { 40393, 40394, 40395,
								40396 };
						int bonus = _random.nextInt(100) + 1;
						int rullet = _random.nextInt(100) + 1;
						L1ItemInstance bonusitem = null;
						pc.getInventory().storeItem(1437010, 1);
						pc.sendPackets(new S_ServerMessage(403, "축복받은 $7969"),
								true);
						pc.getInventory().removeItem(useItem, 1);
						if (bonus <= 5) {
							bonusitem = pc.getInventory()
									.storeItem(
											DRAGONSCALE[rullet
													% DRAGONSCALE.length], 1);
							pc.sendPackets(new S_ServerMessage(403, bonusitem
									.getItem().getNameId()), true);
					
						} else if (bonus >= 6 && bonus <= 14) {
							bonusitem = pc.getInventory().storeItem(
									L1ItemId.DRAGON_SAPHIRE, 1);
							pc.sendPackets(new S_ServerMessage(403, bonusitem
									.getItem().getNameId()), true);
						} else if (bonus >= 15 && bonus <= 24) {
							bonusitem = pc.getInventory().storeItem(
									L1ItemId.DRAGON_RUBY, 1);
							pc.sendPackets(new S_ServerMessage(403, bonusitem
									.getItem().getNameId()), true);
						} else {
						}
						DRAGONSCALE = null;

					} else if (itemId == 1437010 || itemId == 437010
							|| itemId == 437013 || itemId == 437012
							|| itemId == 5000067 || itemId == 60291
							|| itemId == 3402) {
						드래곤보석(pc, useItem);
					} else if (itemId == 60255 || itemId == 60293) {// 자수정
						자수정(pc, useItem);

					} else if (itemId == 5559 || itemId == 5560) {// 탐열매
						// System.out.println("objid "+use_objid);
						if (use_objid == 0) {
							return;
						}
						int day = 0;
						if (itemId == 5559)
							day = 1;
						if (itemId == 5560)
							day = 3;
						탐열매(pc, use_objid, useItem, day);
					} else if (itemId == 7241) {// 토파즈
						토파즈(pc, useItem);
					} else if (itemId == L1ItemId.DRAGON_DIAMOND) {
						int temphasad = pc.getAinHasad() + 1000000;
						if (temphasad > 49000000) {
							pc.sendPackets(new S_SystemMessage(
									"아인하사드의 축복 : 남아있는 축복지수가 많아 사용 할 수 없습니다."));
							return;
						}
						if(pc.getAinHasad() < 100){
							pc.setAinHasad(1000000);
						} else {
							pc.calAinHasad(1000000);
						}
						pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.EINHASAD, pc), true);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == L1ItemId.DRAGON_SAPHIRE) {
						int temphasad = pc.getAinHasad() + 500000;
						if (temphasad > 49500000) {
							pc.sendPackets(new S_SystemMessage("아인하사드의 축복 : 남아있는 축복지수가 많아 사용 할 수 없습니다."));
							return;
						}
						if(pc.getAinHasad() < 100){
							pc.setAinHasad(500000);
						} else {
							pc.calAinHasad(500000);
						}
						pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.EINHASAD, pc), true);
						pc.getInventory().removeItem(useItem, 1);
					} else if (itemId == L1ItemId.DRAGON_RUBY) {
						int temphasad = pc.getAinHasad() + 300000;
						if (temphasad > 49700000) {
							pc.sendPackets(new S_SystemMessage(
									"아인하사드의 축복 : 남아있는 축복지수가 많아 사용 할 수 없습니다."));
							return;
						}
						if(pc.getAinHasad() < 100){
							pc.setAinHasad(300000);
						}else{
						pc.calAinHasad(300000);
						}
						pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.EINHASAD, pc), true);
						pc.getInventory().removeItem(useItem, 1);
						/** 생일 시스템 by이러버엉 **/
						// 야메 시스템 ㅋ 이런방식인것만...
					} else if (itemId == 46201) {
						L1Object obj = L1World.getInstance().findObject(
								spellsc_objid);
						if (obj == null) {
							return;
						}

						if (obj instanceof L1PcInstance) {
							L1PcInstance pc1 = (L1PcInstance) obj;
							if (pc.getClanid() == pc1.getClanid()) {
								pc.sendPackets(new S_SystemMessage(
										"동일한 혈맹간에는 사용할 수 없습니다."));
								return;
							}
							if (pc.getClanid() != 0) {
								pc1.tt_clanid = pc.getClanid();
							}
							if (pc.getPartyID() != 0) {
								pc1.tt_partyid = pc.getPartyID();
							}
							pc.tt_level = pc.getLevel();
							S_DoActionGFX gfx = new S_DoActionGFX(pc.getId(),
									17);
							pc.sendPackets(gfx);
							Broadcaster.broadcastPacket(pc, gfx, true);
							pc1.getSkillEffectTimerSet().setSkillEffect(
									L1SkillId.ATTACK_TARGET, 5 * 1000);
							// if (pc1.tt_clanid == pc.getClanid() ||
							// pc1.tt_partyid == pc.getPartyID()) {
							// pc.sendPackets(new S_PacketBox(S_PacketBox.이미지스폰,
							// pc1.getId(), 13135, true));
							// }
						} else if (obj instanceof L1MonsterInstance
								|| obj instanceof L1SummonInstance
								|| obj instanceof L1PetInstance
								|| obj instanceof L1NpcInstance) {

							L1NpcInstance npc = (L1NpcInstance) obj;
							if (pc.getClanid() != 0) {
								npc.tt_clanid = pc.getClanid();
							}
							if (pc.getPartyID() != 0) {
								npc.tt_partyid = pc.getPartyID();
							}
							npc.tt_level = pc.getLevel();
							S_DoActionGFX gfx = new S_DoActionGFX(pc.getId(),
									17);
							pc.sendPackets(gfx);
							Broadcaster.broadcastPacket(pc, gfx, true);
							npc.getSkillEffectTimerSet().setSkillEffect(
									L1SkillId.ATTACK_TARGET, 5 * 1000);
							// if (npc.tt_clanid == pc.getClanid() ||
							// npc.tt_partyid == pc.getPartyID()) {
							// pc.sendPackets(new S_PacketBox(S_PacketBox.이미지스폰,
							// npc.getId(), 13135, true));
							// }
						}
					} else if (itemId == 6000034) { // 풀업버프물약
						풀업물약(pc, useItem);
					} else if (itemId == 5000121) {
						if (pc.getInventory().checkItem(5000121, 1)) {
							pc.getInventory().consumeItem(5000121, 1);
							int[] mobArray = { 450001798 };
							int rnd = _random.nextInt(mobArray.length);
							L1SpawnUtil.spawn(pc, mobArray[rnd], 0, 300000,
									true);
							mobArray = null;
						}
					} else if (itemId == L1ItemId.METIS_ONE) {
						int bonus = _random.nextInt(100) + 1;
						pc.getInventory().storeItem(L1ItemId.METIS_THREE, 1);
						pc.sendPackets(new S_ServerMessage(403, "메티스의 케이크 상자"),
								true);
						pc.getInventory().removeItem(useItem, 1);
						if (bonus <= 3) {
							pc.getInventory().storeItem(L1ItemId.weapon_0, 1);
							pc.sendPackets(
									new S_ServerMessage(403, "무기 마법 주문서"), true);
						} else if (bonus >= 4 && bonus <= 7) {
							pc.getInventory().storeItem(L1ItemId.weapon_1, 1);
							pc.sendPackets(
									new S_ServerMessage(403, "무기 마법 주문서"), true);
						} else if (bonus >= 8 && bonus <= 12) {
							pc.getInventory().storeItem(L1ItemId.armor_0, 1);
							pc.sendPackets(
									new S_ServerMessage(403, "갑옷 마법 주문서"), true);
						} else if (bonus >= 13 && bonus <= 17) {
							pc.getInventory().storeItem(L1ItemId.armor_1, 1);
							pc.sendPackets(
									new S_ServerMessage(403, "갑옷 마법 주문서"), true);
						} else if (bonus >= 18 && bonus <= 22) {
							pc.getInventory().storeItem(L1ItemId.potion_0, 5);
							pc.sendPackets(new S_ServerMessage(403,
									"유그드라 열매(5)"), true);
						} else if (bonus >= 23 && bonus <= 27) {
							pc.getInventory().storeItem(L1ItemId.potion_1, 5);
							pc.sendPackets(
									new S_ServerMessage(403, "용기의 물약(5)"), true);
						} else if (bonus >= 28 && bonus <= 32) {
							pc.getInventory().storeItem(L1ItemId.potion_2, 5);
							pc.sendPackets(
									new S_ServerMessage(403, "엘븐 와퍼(5)"), true);
						} else if (bonus >= 33 && bonus <= 37) {
							pc.getInventory().storeItem(L1ItemId.potion_3, 5);
							pc.sendPackets(new S_ServerMessage(403, "악마의피(5)"),
									true);
						}
						/** 생일 시스템 by이러버엉 **/
						// 야메 시스템 ㅋ 이런방식인것만...
					} else if (itemId == L1ItemId.FORTUNE_COOKIE || (itemId >= 437027 && itemId <= 437034)) {
						int count = _random.nextInt(8) + 1;
						pc.getInventory().storeItem(41159, count);
						pc.sendPackets(new S_ServerMessage(403, "픽시의 깃털 (" + count + ")"), true);
						pc.getInventory().removeItem(useItem, 1);
					} else {
						int locX = ((L1EtcItem) useItem.getItem()).get_locx();
						int locY = ((L1EtcItem) useItem.getItem()).get_locy();
						short mapId = ((L1EtcItem) useItem.getItem()).get_mapid();
						if (locX != 0 && locY != 0) {
							if(pc.getMapId() >= 12852 && pc.getMapId() <= 12862){
								pc.setTelType(pc.getMapId());
								pc.sendPackets(new S_SabuTell(pc), true);
								pc.getInventory().removeItem(useItem, 1);
								return;
							}
							if (pc.getMap().isEscapable() || pc.isGm()) {
								if (pc.Sabutelok()) {
									pc.dx = locX;
									pc.dy = locY;
									pc.dm = mapId;
									pc.dh = pc.getMoveState().getHeading();
									pc.setTelType(7);
									pc.sendPackets(new S_SabuTell(pc), true);
									pc.getInventory().removeItem(useItem, 1);
								}
							} else {
								pc.sendPackets(new S_ServerMessage(647), true);
							}
							pc.cancelAbsoluteBarrier();
						} else {
							if (itemId == 40314) {
								PetSummons.UsePetSummons(pc, itemObjid);
							} else if (useItem.getCount() < 1) {
								pc.sendPackets(new S_ServerMessage(329, useItem.getLogName()), true);
							} else {
								pc.sendPackets(new S_ServerMessage(74, useItem.getLogName()), true);
							}
						}
					}
				}
				L1ItemDelay.onItemUse(pc, useItem); // 아이템 지연 개시
			}
		} catch (Exception e) {

		} finally {
			clear();
		}
	}

	private void 정상의가호(L1PcInstance pc, L1ItemInstance useItem) {

		Timestamp lastUsed = useItem.getLastUsed();
		if (lastUsed == null || currentDate.getTimeInMillis() > lastUsed.getTime() + (1000 * 60 * 60 * 1)) {

			pc.sendPackets(new S_SkillSound(pc.getId(), 12536));
			pc.broadcastPacket(new S_SkillSound(pc.getId(), 12536));

			if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.정상의가호)) {
				pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.정상의가호, 600 * 1000);
			} else {
				pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.정상의가호, 600 * 1000);
			}

			useItem.setLastUsed(new Timestamp(currentDate.getTimeInMillis()));
		} else {
			long i = (lastUsed.getTime() + (1000 * 60 * 60 * 1))
					- currentDate.getTimeInMillis();
			Calendar cal = (Calendar) currentDate.clone();
			cal.setTimeInMillis(cal.getTimeInMillis() + i);
			pc.sendPackets(new S_SystemMessage(i / 60000 + "분 동안("
					+ cal.getTime().getHours() + ":"
					+ cal.getTime().getMinutes() + " 까지)은 사용할 수 없습니다."), true);
		}
	}

	private boolean 닥쳐(L1PcInstance pc) {
		if (pc.getMapId() == 4
				&& ((pc.getX() >= 33331 && pc.getX() <= 33341
						&& pc.getY() >= 32430 && pc.getY() <= 32441)
						|| (pc.getX() >= 33258 && pc.getX() <= 33267
								&& pc.getY() >= 32396 && pc.getY() <= 32407)
						|| (pc.getX() >= 33388 && pc.getX() <= 33397
								&& pc.getY() >= 32339 && pc.getY() <= 32350)
						|| (pc.getX() >= 33464 && pc.getX() <= 33531
								&& pc.getY() >= 33168 && pc.getY() <= 33248) || (pc
						.getX() >= 33443
						&& pc.getX() <= 33483
						&& pc.getY() >= 32315 && pc.getY() <= 32357))) {
			return false;
		}

		if (pc.getMapId() == 4
				&& ((pc.getX() >= 33328 && pc.getX() <= 33344
						&& pc.getY() >= 32427 && pc.getY() <= 32444)
						|| (pc.getX() >= 33255 && pc.getX() <= 33272
								&& pc.getY() >= 32393 && pc.getY() <= 32412)
						||

						(pc.getX() >= 34194 && pc.getX() <= 34305
								&& pc.getY() >= 33324 && pc.getY() <= 33535) || // 황혼의산맥
						(pc.getX() >= 33450 && pc.getX() <= 33470
								&& pc.getY() >= 32328 && pc.getY() <= 32344) || // 아덴의한국민

						(pc.getX() >= 33385 && pc.getX() <= 33400
								&& pc.getY() >= 32336 && pc.getY() <= 32353) || (pc
						.getX() >= 33461
						&& pc.getX() <= 33534
						&& pc.getY() >= 33165 && pc.getY() <= 33253))) {
			return false;
		}

		return true;
	}

	private void 풀업물약(L1PcInstance pc, L1ItemInstance useitem) {
		// 서버운영자분이 변경
		pc.setBuffnoch(1);
		int[] allBuffSkill = { CONCENTRATION, PATIENCE, INSIGHT,
				PHYSICAL_ENCHANT_DEX, PHYSICAL_ENCHANT_STR,
				  
				IRON_SKIN, AQUA_PROTECTER, SHINING_ARMOR,DRAGON_SKIN,STATUS_COMA_5,LIFE_MAAN};

		L1SkillUse l1skilluse = new L1SkillUse();

		for (int i = 0; i < allBuffSkill.length; i++) {
			l1skilluse.handleCommands(pc, allBuffSkill[i], pc.getId(),
					pc.getX(), pc.getY(), null, 0, L1SkillUse.TYPE_GMBUFF); // 버프
																			// 모션
																			// 없애기
		}
		l1skilluse = null;
		allBuffSkill = null;
		useCashScroll(pc, L1ItemId.INCRESE_ATTACK_SCROLL, false);
		L1SkillUse su = new L1SkillUse();
		su.handleCommands(pc, STATUS_COMA_5, pc.getId(), pc.getX(), pc.getY(),
				null, 0, L1SkillUse.TYPE_GMBUFF);
		su = null;
		L1SkillUse su2 = new L1SkillUse();
		su2.handleCommands(pc, L1SkillId.FEATHER_BUFF_A, pc.getId(), pc.getX(),
				pc.getY(), null, 0, L1SkillUse.TYPE_SPELLSC);
		su2 = null;
		pc.getInventory().consumeItem(6000034, 1);
		pc.sendPackets(new S_SkillSound(pc.getId(), 4856), true);
		pc.sendPackets(new S_SystemMessage(useitem.getName() + "을 복용 하였습니다!"),
				true);
		pc.setBuffnoch(0);

	}


	private static final int[] 전투벚꽃상자리스트 = { 60217, 437017, 437002, 437003,
			437004, 560025, 560027 };

	private static final int[] 그렘린선물상자리스트 = { 437002, 437003, 437004, 560025,
			560027 };
	//private static final int[] 호박시리즈 = { 256, 4500027, 263, 4500026, 265, 264,
	//		427306 };

	private static final int[] 숨결시리즈 = { 60187, 60188, 60189 };
	private static final int[] 마갑주시리즈 = { 20108, 20119, 20130, 20153 };

	//private static final int[] 엘릭서시리즈 = { 40033, 40034, 40035, 40036, 40037, 40038 };

	//- 이거 정리부터 좀 해야될거 같아요,,,,
	private void 바루의선물상자(L1PcInstance pc) {
		L1ItemInstance item = null;
		int count = 1;
		int rnd = _random.nextInt(10000);
		if (rnd < 10) {// 0.1% 안타라스의숨결
			item = pc.getInventory().storeItem(40346, 1);

		} else if (rnd < 11) {// 0.01%파푸리온의 숨결
			item = pc.getInventory().storeItem(40362, 1);

		} else if (rnd < 12) {// 0.01%린드비오르의숨결
			item = pc.getInventory().storeItem(40370, 1);

		} else if (rnd < 2212) {// 22%화염의기운
			item = pc.getInventory().storeItem(6022, 1);

		} else if (rnd < 4412) {// 22%냉한의기운
			item = pc.getInventory().storeItem(7337, 1);

		} else if (rnd < 6512) {// 21%몽섬탐조각
			item = pc.getInventory().storeItem(60499, 1);

		} else if (rnd < 8512) {// 20%축드다
			item = pc.getInventory().storeItem(1437010, 1);

		} else if (rnd < 8612) {// 1% 봉인된부적1층
			item = pc.getInventory().storeItem(60201, 1);

		} else if (rnd < 8712) {// 1% 봉인된부적2층
			item = pc.getInventory().storeItem(40280, 1);

		} else if (rnd < 8812) {// 1% 봉인된부적3층
			item = pc.getInventory().storeItem(40281, 1);

		} else if (rnd < 8912) {// 1% 봉인된부적4층
			item = pc.getInventory().storeItem(40282, 1);

		} else if (rnd < 9012) {// 1% 봉인된부적5층
			item = pc.getInventory().storeItem(40283, 1);

		} else if (rnd < 9112) {// 1% 봉인된부적6층
			item = pc.getInventory().storeItem(40284, 1);

		} else if (rnd < 9212) {// 1% 봉인된부적7층
			item = pc.getInventory().storeItem(40285, 1);

		} else if (rnd < 9312) {// 1% 봉인된부적8층
			item = pc.getInventory().storeItem(40286, 1);

		} else if (rnd < 9412) {// 1% 봉인된부적9층
			item = pc.getInventory().storeItem(40287, 1);

		} else if (rnd < 9512) {// 1% 봉인된부적10층
			item = pc.getInventory().storeItem(40288, 1);

		} else {// 5% 축변경 주문서
			item = pc.getInventory().storeItem(50022, 1); // 용비늘
		}

		if (item != null)
			pc.sendPackets(new S_ServerMessage(403, item.getName()), true);
	}

	private void 금호박(L1PcInstance pc) {
		L1ItemInstance item = null;
		int count = 1;
		int rnd = _random.nextInt(1020);
		if (rnd < 10) {// 1% 타잉탄락
			item = pc.getInventory().storeItem(7304, 1);

		} else if (rnd < 20) {// 1%타이탄매
			item = pc.getInventory().storeItem(7306, 1);

		} else if (rnd < 91) {// 7%기간틱
			item = pc.getInventory().storeItem(7309, 1);

			/*
			 * }else if(rnd < 91){//0.1%데스페라도 item =
			 * pc.getInventory().storeItem(7310, 1);
			 */
		} else if (rnd < 94) {// 0.3%제로스지팡이
			item = pc.getInventory().storeItem(291, 1);

		} else if (rnd < 95) {// 0.1%커검
			item = pc.getInventory().storeItem(54, 1);

		} else if (rnd < 100) {// 0.5 악몽의 장궁
			item = pc.getInventory().storeItem(293, 1); // 악몽의장궁

		} else if (rnd < 103) {// 0.3 나이트발드의양손검
			item = pc.getInventory().storeItem(59, 1); // 나양

		} else if (rnd < 108) {// 0.5 오우거도끼
			item = pc.getInventory().storeItem(7227, 1);

		} else if (rnd < 178) {// 7%산적도끼
			item = pc.getInventory().storeItem(7225, 1);

		} else if (rnd < 255) {// 7.7 데몬지팡이
			item = pc.getInventory().storeItem(119, 1);

		} else if (rnd < 305) {// 5 축완력부츠
			item = pc.getInventory().storeItem(21259, 1, 0, 0); // 용비늘

		} else if (rnd < 355) {// 5 축지식부츠
			item = pc.getInventory().storeItem(21266, 1, 0, 0); // 용비늘

		} else if (rnd < 405) {// 5 축민첩부츠
			item = pc.getInventory().storeItem(21265, 1, 0, 0); // 용비늘

		} else if (rnd < 455) {// 5 타라스부츠
			item = pc.getInventory().storeItem(120194, 1); // 용비늘

		} else if (rnd < 505) {// 5 돌 장갑
			item = pc.getInventory().storeItem(30219, 1); // 용비늘

		} else if (rnd < 555) {// 5 타라스 장갑
			item = pc.getInventory().storeItem(120187, 1); // 용비늘

		} else if (rnd < 585) {// 3 빛나는 마력 장갑
			item = pc.getInventory().storeItem(7246, 1); // 용비늘

		} else if (rnd < 615) {// 3 혼돈의 손길
			item = pc.getInventory().storeItem(20190, 1); // 용비늘

		} else if (rnd < 650) {// 3 엘릭서힘
			item = pc.getInventory().storeItem(40033, 1); // 용비늘

		} else if (rnd < 685) {// 3 엘릭서덱
			item = pc.getInventory().storeItem(40035, 1); // 용비늘

		} else if (rnd < 720) {// 3 엘릭서인트
			item = pc.getInventory().storeItem(40036, 1); // 용비늘

		} else if (rnd < 755) {// 3 엘릭서위즈
			item = pc.getInventory().storeItem(40037, 1); // 용비늘

		} else if (rnd < 845) {// 9% 봉인된 오우거의도끼
			item = pc.getInventory().storeItem(7335, 1); // 용비늘

		} else if (rnd < 915) {// 7% 은색의 망토
			item = pc.getInventory().storeItem(20074, 1); // 용비늘

		} else if (rnd < 925) {// 1% 금날
			item = pc.getInventory().storeItem(20049, 1); // 용비늘

		} else if (rnd < 935) {// 1% 은날
			item = pc.getInventory().storeItem(20050, 1); // 용비늘

		} else if (rnd < 945) {// 1% 사이하의 목걸이
			item = pc.getInventory().storeItem(21268, 1); // 용비늘

		} else if (rnd < 955) {// 1% 현자의 목걸이
			item = pc.getInventory().storeItem(21260, 1); // 용비늘

		} else if (rnd < 965) {// 1% 포노스 투사의 목걸이
			item = pc.getInventory().storeItem(21258, 1); // 용비늘

		} else if (rnd < 975) {// 1% 빛나는 사이하의 반지
			item = pc.getInventory().storeItem(21267, 1); // 용비늘

		} else if (rnd < 985) {// 1% 완력의 벨트
			item = pc.getInventory().storeItem(21261, 1); // 용비늘

		} else if (rnd < 995) {// 1% 지식의 벨트
			item = pc.getInventory().storeItem(21262, 1); // 용비늘

		} else if (rnd < 1005) {// 1% 민첩의 벨트
			item = pc.getInventory().storeItem(21263, 1); // 용비늘

		} else if (rnd < 1015) {// 1% 지혜의 벨트
			item = pc.getInventory().storeItem(21264, 1); // 용비늘
		} else {// 0.5% 지휘관의 투구
			item = pc.getInventory().storeItem(21122, 1); // 용비늘
		}

		if (item != null)
			pc.sendPackets(new S_ServerMessage(403, item.getName()), true);
	}

	private void 그렘린의선물상자(L1PcInstance pc) {
		L1ItemInstance item = null;
		int count = 1;
		int rnd = _random.nextInt(100000);
		if (rnd < 1) {// 0.001 숨결
			int itemid = 숨결시리즈[_random.nextInt(숨결시리즈.length)];
			item = pc.getInventory().storeItem(itemid, count);
		} else if (rnd < 2) {// 0.001 커검
			item = pc.getInventory().storeItem(54, 1); // 커검
		} else if (rnd < 3) {// 0.001 데불
			item = pc.getInventory().storeItem(58, 1); // 데불
		} else if (rnd < 4) {// 0.001 제로스지팡이
			item = pc.getInventory().storeItem(291, 1); // 제로스
		} else if (rnd < 5) {// 0.001 진노의크로우
			item = pc.getInventory().storeItem(292, 1); // 진노의크로우
		} else if (rnd < 6) {// 0.001 오우거의도끼
			item = pc.getInventory().storeItem(7227, 1); // 오우거의도끼
		} else if (rnd < 7) {// 0.001 냉한의키링크
			item = pc.getInventory().storeItem(6001, 1); // 냉한의
		} else if (rnd < 8) {// 0.001 악몽의 장궁
			item = pc.getInventory().storeItem(293, 1); // 악몽의장궁
		} else if (rnd < 9) {// 0.001 나이트발드의양손검
			item = pc.getInventory().storeItem(59, 1); // 나양
		} else if (rnd < 10) {// 0.001 극한의체인소드
			item = pc.getInventory().storeItem(6000, 1); // 극한의체인소드
		} else if (rnd < 11) {// 0.001 악마의 칼
			item = pc.getInventory().storeItem(63, 1); // 악마의칼
		} else if (rnd < 21) {// 0.01 마갑주
			int itemid = 마갑주시리즈[_random.nextInt(마갑주시리즈.length)];
			item = pc.getInventory().storeItem(itemid, count);
		} else if (rnd < 31) {// 0.01 용비늘
			item = pc.getInventory().storeItem(40394 + _random.nextInt(3), 1); // 용비늘
		} else if (rnd < 10031) {// 10% 드다/성장의물약/코마
			int rndd = _random.nextInt(100);
			if (rndd < 33) {
				item = pc.getInventory().storeItem(437010, 1); // 드다
			} else if (rndd < 66) {
				item = pc.getInventory().storeItem(60217, 1); // 코마버프
			} else {
				item = pc.getInventory().storeItem(437017, 1); // 성장의물약
			}
		} else {
			int itemid = 그렘린선물상자리스트[_random.nextInt(그렘린선물상자리스트.length)];
			item = pc.getInventory().storeItem(itemid, count);
		}

		/*
		 * int rnd1 = _random.nextInt(16)+2011; int rnd2 =
		 * _random.nextInt(20)+2028; pc.sendPackets(new S_SkillSound(pc.getId(),
		 * rnd1)); Broadcaster.broadcastPacket(pc,new S_SkillSound(pc.getId(),
		 * rnd1)); pc.sendPackets(new S_SkillSound(pc.getId(), rnd2));
		 * Broadcaster.broadcastPacket(pc,new S_SkillSound(pc.getId(), rnd2));
		 * 
		 * pc.sendPackets(new S_SkillSound(pc.getId(), 6412));
		 * Broadcaster.broadcastPacket(pc,new S_SkillSound(pc.getId(), 6412));
		 */

		if (item != null)
			pc.sendPackets(new S_ServerMessage(403, item.getName()), true);
	}

	private static final int[] 버프코인리스트 = { 3, 14, 26, 42, 68, 79, 115, 117,
			158, 160, 181, 201, 206, 211, 168, 216, 88, 89 };

	private void 랜덤버프코인(L1PcInstance pc) {
		// TODO 자동 생성된 메소드 스텁
		new L1SkillUse().handleCommands(pc,
				버프코인리스트[_random.nextInt(버프코인리스트.length)], pc.getId(),
				pc.getX(), pc.getY(), null, 0, L1SkillUse.TYPE_GMBUFF);
	}

	private static int[] 호박리스트 = { 256, 4500027, 263, 4500026, 264, 265, 427306 };
	private static int[] 숨결리스트 = { 40346, 40362, 40370 };

	private void 벚꽃상자(L1PcInstance pc) {
		// TODO 자동 생성된 메소드 스텁
		pc.getInventory().storeItem(60518, 1);
		pc.sendPackets(new S_ServerMessage(403, "랜덤 버프 1회 코인"), true);

		L1ItemInstance item = null;
		int count = 1;
		int rnd = _random.nextInt(100000);
		if (rnd < 1) {// 0.001 숨결
			int itemid = 숨결시리즈[_random.nextInt(숨결시리즈.length)];
			item = pc.getInventory().storeItem(itemid, count);
		} else if (rnd < 2) {// 0.001 커검
			item = pc.getInventory().storeItem(54, 1); // 커검
		} else if (rnd < 3) {// 0.001 데불
			item = pc.getInventory().storeItem(58, 1); // 데불
		} else if (rnd < 13) {// 0.01 마갑주
			int itemid = 마갑주시리즈[_random.nextInt(마갑주시리즈.length)];
			item = pc.getInventory().storeItem(itemid, count);
		} else if (rnd < 23) {// 0.01 용비늘
			item = pc.getInventory().storeItem(40394 + _random.nextInt(3), 1); // 용비늘
		} else if (rnd < 2023) {// 2 드다
			item = pc.getInventory().storeItem(437010, 1); // 용비늘
		} else if (rnd < 3023) {// 1 아이디변경권 및 성별 전환 물약
			int rndd = _random.nextInt(100);
			if (rndd < 50) {
				item = pc.getInventory().storeItem(437001, 1); // 성별 전환
			} else {
				item = pc.getInventory().storeItem(467009, 1); // 이름 변경
			}
		} else if (rnd < 13023) {// 10%
			int itemid = 전투벚꽃상자리스트[_random.nextInt(전투벚꽃상자리스트.length)];
			item = pc.getInventory().storeItem(itemid, count);
		}
		int rnd1 = _random.nextInt(16) + 2011;
		int rnd2 = _random.nextInt(20) + 2028;
		pc.sendPackets(new S_SkillSound(pc.getId(), rnd1));
		Broadcaster.broadcastPacket(pc, new S_SkillSound(pc.getId(), rnd1));
		pc.sendPackets(new S_SkillSound(pc.getId(), rnd2));
		Broadcaster.broadcastPacket(pc, new S_SkillSound(pc.getId(), rnd2));

		pc.sendPackets(new S_SkillSound(pc.getId(), 6412));
		Broadcaster.broadcastPacket(pc, new S_SkillSound(pc.getId(), 6412));

		if (item != null)
			pc.sendPackets(new S_ServerMessage(403, item.getName()), true);
	}

	private void 탐포인트상자(L1PcInstance pc) {
		// TODO 자동 생성된 메소드 스텁
		// 탐 포인트 아이템 구매하면 탐이 오르게
		pc.getNetConnection().getAccount().tam_point += 2500;
		try {
			pc.getNetConnection().getAccount().updateTam();
			pc.sendPackets(new S_NewCreateItem(S_NewCreateItem.TAM_POINT, pc.getNetConnection()), true);
		} catch (Exception e) {
		}
	}
	
	private void 소생주문서(L1PcInstance pc, L1ReviveItem reviveItem, L1ItemInstance useItem, L1ItemInstance targetItem) {
		int consumItemId = reviveItem.itemId;
		int storeItemId = reviveItem.storeId;
		int chance = reviveItem.chance;
		if (consumItemId == 0 || storeItemId == 0 || chance == 0) {
			pc.sendPackets(new S_ServerMessage(79)); //아무일도...
			return;
		}
		pc.getInventory().removeItem(useItem, 1); 
		pc.getInventory().removeItem(targetItem, 1);
		int ran = _random.nextInt(100);		
		if (ran < chance){
			pc.getInventory().storeItem(storeItemId, 1);
			pc.sendPackets(new S_ServerMessage(5439, targetItem.getName())); //%0에 소생의 축복의 스며듭니다.
			if (reviveItem.isMent) {
				L1World.getInstance().broadcastPacketToAll(new S_SystemMessage(reviveItem.Ment));
				L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, reviveItem.Ment));
			}
		} else {
			pc.sendPackets(new S_ServerMessage(5440, targetItem.getName()));//%0은 소생하지 못하고 증발하엿습니다.
		}
		if (pc.isGm()) {
			pc.sendPackets(new S_SystemMessage("DB : revive_items 찬스값 : " + chance));
		}
	}

	int[] adencount = { 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140,
			150 };

	private void 황금주머니(L1PcInstance pc) {
		// TODO 자동 생성된 메소드 스텁
		int rnd = adencount[_random.nextInt(adencount.length)] * 10000;
		L1ItemInstance item = pc.getInventory().storeItem(L1ItemId.ADENA, rnd);
		pc.sendPackets(new S_ServerMessage(403, item.getName() + " (" + rnd
				+ ")"), true);
		LogTable.사냥아덴(pc, rnd);
	}

	private void 금화주머니(L1PcInstance pc) {
		// TODO 자동 생성된 메소드 스텁
		int rnd = _random.nextInt(1000);
		if (rnd < 950) {// 95%
			int count = 2500 + _random.nextInt(2000);// 최소 2500~4500 아데나
			L1ItemInstance item = pc.getInventory().storeItem(L1ItemId.ADENA,
					count);
			pc.sendPackets(new S_ServerMessage(403, item.getName() + " ("
					+ count + ")"), true);
			LogTable.사냥아덴(pc, count);
		} else if (rnd < 990) {// 4%
			int count = 77 + _random.nextInt(20000);// 최소 77~20077 아데나
			L1ItemInstance item = pc.getInventory().storeItem(L1ItemId.ADENA,
					count);
			pc.sendPackets(new S_ServerMessage(403, item.getName() + " ("
					+ count + ")"), true);
			LogTable.사냥아덴(pc, count);
		} else if (rnd < 991) {// 0.1%
			int count = 77 + _random.nextInt(777700);// 최소 77~77777 아데나 777,777원
			L1ItemInstance item = pc.getInventory().storeItem(L1ItemId.ADENA,
					count);
			pc.sendPackets(new S_ServerMessage(403, item.getName() + " ("
					+ count + ")"), true);
			LogTable.사냥아덴(pc, count);
		} else {// 0.9%
			int count = 77 + _random.nextInt(77700);// 최소 77~77777 아데나 777,777원
			L1ItemInstance item = pc.getInventory().storeItem(L1ItemId.ADENA,
					count);
			pc.sendPackets(new S_ServerMessage(403, item.getName() + " ("
					+ count + ")"), true);
			LogTable.사냥아덴(pc, count);
		}
	}

	private void 피씨방코인(L1PcInstance pc, int itemId, L1ItemInstance useItem, int day) {
		long sysTime = System.currentTimeMillis();
		Timestamp deleteTime = null;
		deleteTime = new Timestamp(sysTime + (86400000 * (long) day) + 10000);// 7일
		try {
			if (pc.PC방_버프) {
				pc.sendPackets(new S_SystemMessage("이미 PC방 버프 상품이 적용중입니다."));
				return;
			}

			// pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.피씨방_버프,
			// (int)86400000*day);

			pc.sendPackets(new S_PacketBox(S_PacketBox.PC방버프, 1), true);
			pc.PC방_버프 = true;
			String s = "08 01 f1 d5";// 피씨방..
			// pc.sendPackets(new S_NewCreateItem(s ));
			pc.sendPackets(new S_NewCreateItem(126, s));
			if (day == 1) {
				pc.sendPackets(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "[PC방 이용 시간] 1일 동안 PC방 혜택이 적용 됩니다."));
			} else {
				pc.sendPackets(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "[PC방 이용 시간] 3일 동안 PC방 혜택이 적용 됩니다."));
			}
			pc.getNetConnection().getAccount().setBuff_PC방(deleteTime);
			pc.getNetConnection().getAccount().update피씨방();
			pc.getInventory().removeItem(useItem, 1);
	//		pc.sendPackets(new S_AttenDance(pc, S_AttenDance.WatchPcPorfile, 0));
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void 강화버프(L1PcInstance pc, int itemId, L1ItemInstance useItem) {
		String n = "";
		long sysTime = System.currentTimeMillis();
		Timestamp deleteTime = null;
		deleteTime = new Timestamp(sysTime + (86400000) + 10000);// 7일
		try {
			if (itemId == 600212) {
				n = "활력";
				if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.강화버프_활력)) {
					pc.sendPackets(new S_SystemMessage("이미 " + n + " 버프 상품이 적용중입니다."));
					return;
				}
				pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.강화버프_활력, (int) 86400000);
				pc.getNetConnection().getAccount().setBuff_HPMP(deleteTime);
				pc.sendPackets(new S_NewUI(n, (long) 86400000), true);
				pc.addMaxHp(50);
				pc.addMaxMp(50);
				pc.addWeightReduction(3);
				pc.sendPackets(new S_HPUpdate(pc));
				pc.sendPackets(new S_MPUpdate(pc.getCurrentMp(), pc.getMaxMp()));
			} else if (itemId == 600213) {
				n = "공격";
				if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.강화버프_공격)) {
					pc.sendPackets(new S_SystemMessage("이미 " + n + " 버프 상품이 적용중입니다."));
					return;
				}
				pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.강화버프_공격);
				pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.강화버프_공격, (int) 86400000);
				pc.getNetConnection().getAccount().setBuff_DMG(deleteTime);
				pc.sendPackets(new S_NewUI(n, (long) 86400000), true);
				pc.addDmgup(1);
				pc.addBowDmgup(1);
			} else if (itemId == 600214) {
				n = "방어";
				if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.강화버프_방어)) {
					pc.sendPackets(new S_SystemMessage("이미 " + n + " 버프 상품이 적용중입니다."));
					return;
				}
				pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.강화버프_방어);
				pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.강화버프_방어, (int) 86400000);
				pc.getNetConnection().getAccount().setBuff_REDUC(deleteTime);
				pc.sendPackets(new S_NewUI(n, (long) 86400000), true);
				pc.addDamageReductionByArmor(1);
			} else if (itemId == 600215) {
				n = "마법";
				if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.강화버프_마법)) {
					pc.sendPackets(new S_SystemMessage("이미 " + n + " 버프 상품이 적용중입니다."));
					return;
				}
				pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.강화버프_마법);
				pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.강화버프_마법, (int) 86400000);
				pc.getNetConnection().getAccount().setBuff_MAGIC(deleteTime);
				pc.sendPackets(new S_NewUI(n, (long) 86400000), true);
				pc.getAbility().addSp(1);
				pc.sendPackets(new S_SPMR(pc));
			} else if (itemId == 600216) {
				n = "스턴";
				if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.강화버프_스턴)) {
					pc.sendPackets(new S_SystemMessage("이미 " + n + " 버프 상품이 적용중입니다."));
					return;
				}
				pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.강화버프_스턴);
				pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.강화버프_스턴, (int) 86400000);
				pc.getNetConnection().getAccount().setBuff_STUN(deleteTime);
				pc.sendPackets(new S_NewUI(n, (long) 86400000), true);
				pc.getResistance().addTechnique(2);
			} else if (itemId == 600217) {
				n = "홀드";
				if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.강화버프_홀드)) {
					pc.sendPackets(new S_SystemMessage("이미 " + n + " 버프 상품이 적용중입니다."));
					return;
				}
				pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.강화버프_홀드);
				pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.강화버프_홀드, (int) 86400000);
				pc.getNetConnection().getAccount().setBuff_HOLD(deleteTime);
				pc.sendPackets(new S_NewUI(n, (long) 86400000), true);
				pc.getResistance().addSpirit(2);
			}
			pc.getNetConnection().getAccount().updateBUFF();
			pc.getInventory().removeItem(useItem, 1);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void 강화버프2(L1PcInstance pc, int itemId, L1ItemInstance useItem) {
		String n = "";
		long sysTime = System.currentTimeMillis();
		Timestamp deleteTime정방 = null;
		Timestamp deleteTime화방 = null;
		Timestamp deleteTime지방 = null;
		Timestamp deleteTime수방 = null;
		Timestamp deleteTime풍방 = null;
		
		Timestamp deleteTime완력 = null;
		Timestamp deleteTime민첩 = null;
		Timestamp deleteTime지식 = null;
		Timestamp deleteTime지혜 = null;
		
		Timestamp deleteTime화공 = null;
		Timestamp deleteTime지공 = null;
		Timestamp deleteTime수공 = null;
		Timestamp deleteTime풍공 = null;
		Timestamp deleteTimePVP = null;
		
		
		deleteTime정방 = new Timestamp(sysTime + (86400000) + 10000);// 7일
		deleteTime화방 = new Timestamp(sysTime + (86400000) + 10000);// 7일
		deleteTime지방 = new Timestamp(sysTime + (86400000) + 10000);// 7일
		deleteTime수방 = new Timestamp(sysTime + (86400000) + 10000);// 7일
		deleteTime풍방 = new Timestamp(sysTime + (86400000) + 10000);// 7일
		
		deleteTime완력 = new Timestamp(sysTime + (86400000) + 10000);// 7일
		deleteTime민첩 = new Timestamp(sysTime + (86400000) + 10000);// 7일
		deleteTime지식 = new Timestamp(sysTime + (86400000) + 10000);// 7일
		deleteTime지혜 = new Timestamp(sysTime + (86400000) + 10000);// 7일
		
		deleteTime화공 = new Timestamp(sysTime + (86400000) + 10000);// 7일
		deleteTime지공 = new Timestamp(sysTime + (86400000) + 10000);// 7일
		deleteTime수공 = new Timestamp(sysTime + (86400000) + 10000);// 7일
		deleteTime풍공 = new Timestamp(sysTime + (86400000) + 10000);// 7일
		deleteTimePVP = new Timestamp(sysTime + (86400000) + 10000);// 7일
		
		try {
			if (itemId == 600198) {
				n = "화공";
				if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.강화버프_화공)) {
					pc.sendPackets(new S_SystemMessage("이미 " + n + " 버프 상품이 적용중입니다."));
					return;
				}
				pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.강화버프_화공);
				pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.강화버프_화공,(int) 86400000);
				pc.getNetConnection().getAccount().setBuff_FireATTACK(deleteTime화공);
				pc.sendPackets(new S_NewUI(n, (long) 86400000), true);
			} else if (itemId == 600199) {
				n = "지공";
				if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.강화버프_지공)) {
					pc.sendPackets(new S_SystemMessage("이미 " + n + " 버프 상품이 적용중입니다."));
					return;
				}
				pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.강화버프_지공);
				pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.강화버프_지공,(int) 86400000);
				pc.getNetConnection().getAccount().setBuff_EarthATTACK(deleteTime지공);
				pc.sendPackets(new S_NewUI(n, (long) 86400000), true);
			} else if (itemId == 600200) {
				n = "수공";
				if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.강화버프_수공)) {
					pc.sendPackets(new S_SystemMessage("이미 " + n + " 버프 상품이 적용중입니다."));
					return;
				}
				pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.강화버프_수공);
				pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.강화버프_수공,(int) 86400000);
				pc.getNetConnection().getAccount().setBuff_WaterATTACK(deleteTime수공);
				pc.sendPackets(new S_NewUI(n, (long) 86400000), true);
			} else if (itemId == 600201) {
				n = "풍공";
				if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.강화버프_풍공)) {
					pc.sendPackets(new S_SystemMessage("이미 " + n + " 버프 상품이 적용중입니다."));
					return;
				}
				pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.강화버프_풍공);
				pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.강화버프_풍공,(int) 86400000);
				pc.getNetConnection().getAccount().setBuff_WindATTACK(deleteTime풍공);
				pc.sendPackets(new S_NewUI(n, (long) 86400000), true);
			} else if (itemId == 600211) {
				n = "PVP";
				if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.강화버프_PVP)) {
					pc.sendPackets(new S_SystemMessage("이미 " + n + " 버프 상품이 적용중입니다."));
					return;
				}
				pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.강화버프_PVP);
				pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.강화버프_PVP,(int) 86400000);
				pc.getNetConnection().getAccount().setBuff_PVP(deleteTimePVP);
				pc.sendPackets(new S_NewUI(n, (long) 86400000), true);
				pc.addPvPDmgByArmor(1);
				pc.addPvPReductionByArmor(1);
			} else if (itemId == 600205) {
				n = "완력";
				if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.강화버프_완력)) {
					pc.sendPackets(new S_SystemMessage("이미 " + n + " 버프 상품이 적용중입니다."));
					return;
				}
				pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.강화버프_완력);
				pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.강화버프_완력,(int) 86400000);
				pc.getNetConnection().getAccount().setBuff_STR(deleteTime완력);
				pc.sendPackets(new S_NewUI(n, (long) 86400000), true);
				pc.getAbility().addAddedStr((byte) 1);
				pc.sendPackets(new S_OwnCharStatus(pc));
			} else if (itemId == 600204) {
				n = "민첩";
				if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.강화버프_민첩)) {
					pc.sendPackets(new S_SystemMessage("이미 " + n + " 버프 상품이 적용중입니다."));
					return;
				}
				pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.강화버프_민첩);
				pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.강화버프_민첩,(int) 86400000);
				pc.getNetConnection().getAccount().setBuff_DEX(deleteTime민첩);
				pc.sendPackets(new S_NewUI(n, (long) 86400000), true);
				pc.getAbility().addAddedDex((byte) 1);
				pc.sendPackets(new S_OwnCharStatus(pc));
			} else if (itemId == 600203) {
				n = "지식";
				if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.강화버프_지식)) {
					pc.sendPackets(new S_SystemMessage("이미 " + n + " 버프 상품이 적용중입니다."));
					return;
				}
				pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.강화버프_지식);
				pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.강화버프_지식,(int) 86400000);
				pc.getNetConnection().getAccount().setBuff_INT(deleteTime지식);
				pc.sendPackets(new S_NewUI(n, (long) 86400000), true);
				pc.getAbility().addAddedInt((byte) 1);
				pc.sendPackets(new S_SPMR(pc));
				pc.sendPackets(new S_OwnCharStatus(pc));
			} else if (itemId == 600202) {
				n = "지혜";
				if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.강화버프_지혜)) {
					pc.sendPackets(new S_SystemMessage("이미 " + n + " 버프 상품이 적용중입니다."));
					return;
				}
				pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.강화버프_지혜);
				pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.강화버프_지혜,(int) 86400000);
				pc.getNetConnection().getAccount().setBuff_WIS(deleteTime지혜);
				pc.sendPackets(new S_NewUI(n, (long) 86400000), true);
				pc.getAbility().addAddedWis((byte) 1);
				pc.resetBaseMr();
				pc.sendPackets(new S_SPMR(pc));
				pc.sendPackets(new S_OwnCharStatus(pc));
			} else if (itemId == 600206) {
				n = "정방";
				if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.강화버프_정방)) {
					pc.sendPackets(new S_SystemMessage("이미 " + n + " 버프 상품이 적용중입니다."));
					return;
				}
				pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.강화버프_정방);
				pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.강화버프_정방,(int) 86400000);
				pc.getNetConnection().getAccount().setBuff_SoulDEFENCE(deleteTime정방);
				pc.sendPackets(new S_NewUI(n, (long) 86400000), true);
				pc.getResistance().addFire(5);
				pc.getResistance().addEarth(5);
				pc.getResistance().addWater(5);
				pc.getResistance().addWind(5);
				pc.sendPackets(new S_OwnCharAttrDef(pc));
			} else if (itemId == 600207) {
				n = "화방";
				if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.강화버프_화방)) {
					pc.sendPackets(new S_SystemMessage("이미 " + n + " 버프 상품이 적용중입니다."));
					return;
				}
				pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.강화버프_화방);
				pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.강화버프_화방,(int) 86400000);
				pc.getNetConnection().getAccount().setBuff_FireDEFENCE(deleteTime화방);
				pc.sendPackets(new S_NewUI(n, (long) 86400000), true);
				pc.getResistance().addFire(10);
				pc.sendPackets(new S_OwnCharAttrDef(pc));
			} else if (itemId == 600208) {
				n = "지방";
				if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.강화버프_지방)) {
					pc.sendPackets(new S_SystemMessage("이미 " + n + " 버프 상품이 적용중입니다."));
					return;
				}
				pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.강화버프_지방);
				pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.강화버프_지방,(int) 86400000);
				pc.getNetConnection().getAccount().setBuff_EarthDEFENCE(deleteTime지방);
				pc.sendPackets(new S_NewUI(n, (long) 86400000), true);
				pc.getResistance().addEarth(10);
				pc.sendPackets(new S_OwnCharAttrDef(pc));
			} else if (itemId == 600209) {
				n = "풍방";
				if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.강화버프_풍방)) {
					pc.sendPackets(new S_SystemMessage("이미 " + n + " 버프 상품이 적용중입니다."));
					return;
				}
				pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.강화버프_풍방);
				pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.강화버프_풍방,(int) 86400000);
				pc.getNetConnection().getAccount().setBuff_WindDEFENCE(deleteTime풍방);
				pc.sendPackets(new S_NewUI(n, (long) 86400000), true);
				pc.getResistance().addWind(10);
				pc.sendPackets(new S_OwnCharAttrDef(pc));
			} else if (itemId == 600210) {
				n = "수방";
				if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.강화버프_수방)) {
					pc.sendPackets(new S_SystemMessage("이미 " + n + " 버프 상품이 적용중입니다."));
					return;
				}
				pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.강화버프_수방);
				pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.강화버프_수방,(int) 86400000);
				pc.getNetConnection().getAccount().setBuff_WaterDEFENCE(deleteTime수방);
				pc.sendPackets(new S_NewUI(n, (long) 86400000), true);
				pc.getResistance().addWater(10);
				pc.sendPackets(new S_OwnCharAttrDef(pc));
			}
			pc.getNetConnection().getAccount().updateBUFF();
			pc.getInventory().removeItem(useItem, 1);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	// private static final int[] 중앙사원상자리스트 = {412000, 412001, 412005, 412004,
	// 412003, 191, 259, 260, 292, 293, 21261, 21262, 21263, 21264};
	
	private void 폭풍막대(L1PcInstance pc, int itemId, L1ItemInstance useItem) {
		// TODO 자동 생성된 메소드 스텁
		pc.getInventory().removeItem(useItem, 1);
		pc.sendPackets(new S_SkillSound(pc.getId(), 758), true);
		pc.sendPackets(new S_DoActionGFX(pc.getId(), 18), true);
		for (L1Object obj : L1World.getInstance().getVisibleObjects(pc, 3)) {
			if (obj instanceof L1MonsterInstance) {
				L1MonsterInstance mon = (L1MonsterInstance) obj;
				if (mon == null || mon._destroyed || mon.isDead())
					continue;
					mon.receiveDamage(pc, 200 + _random.nextInt(71));
					pc.sendPackets(new S_DoActionGFX(mon.getId(), ActionCodes.ACTION_Damage), true);
				}
			}
		}
	
	private void 마법막대_파톰_정화(L1PcInstance pc, int itemId, L1ItemInstance useItem) {
		// TODO 자동 생성된 메소드 스텁
		pc.getInventory().removeItem(useItem, 1);
		if (itemId == 60512) {// 마법막대 파톰
			pc.sendPackets(new S_SkillSound(pc.getId(), 1819), true);
			pc.sendPackets(new S_DoActionGFX(pc.getId(), 18), true);
			for (L1Object obj : L1World.getInstance().getVisibleObjects(pc, 3)) {
				if (obj instanceof L1MonsterInstance) {
					L1MonsterInstance mon = (L1MonsterInstance) obj;
					if (mon == null || mon._destroyed || mon.isDead())
						continue;
					mon.receiveDamage(pc, 200 + _random.nextInt(71));
					pc.sendPackets(new S_DoActionGFX(mon.getId(),
							ActionCodes.ACTION_Damage), true);
				}
			}
		} else if (itemId == 60513) {// 마법막대 정화
			pc.sendPackets(new S_SkillSound(pc.getId(), 3934), true);
			pc.sendPackets(new S_DoActionGFX(pc.getId(), 18), true);
			for (L1Object obj : L1World.getInstance().getVisibleObjects(pc, 12)) {
				if (obj instanceof L1MonsterInstance) {
					L1MonsterInstance mon = (L1MonsterInstance) obj;
					if (mon == null || mon._destroyed || mon.isDead())
						continue;
					mon.receiveDamage(pc, 700);
					pc.sendPackets(new S_DoActionGFX(mon.getId(),
							ActionCodes.ACTION_Damage), true);
				}
			}
		}
	} 

	private void 큰은빛베리아나(L1PcInstance pc) {
		// TODO 자동 생성된 메소드 스텁
		L1ItemInstance item = pc.getInventory().storeItem(40308, 500000);
		pc.sendPackets(new S_ServerMessage(403, item.getName() + " (500000)"),
				true);
		item = null;
		int rnd = _random.nextInt(10000);
		if (rnd < 20) {// 명가더 0.2%
			item = pc.getInventory().storeItem(420000, 1);
		} else if (rnd < 40) {// 고투사 0.2%
			item = pc.getInventory().storeItem(420003, 1);
		} else if (rnd < 90) {// 영양 허리끈 0.5%
			item = pc.getInventory().storeItem(20315, 1);
		} else if (rnd < 140) {// 영양 목걸이 0.5%
			item = pc.getInventory().storeItem(20262, 1);
		} else if (rnd < 190) {// 영양 반지 0.5%
			item = pc.getInventory().storeItem(20291, 1);
		} else if (rnd < 290) {// 빛고목 1%
			item = pc.getInventory().storeItem(20422, 1);
		} else if (rnd < 590) {// 버그 인형 3%
			item = pc.getInventory().storeItem(41248, 1);
		} else if (rnd < 890) {// 에티 인형 3%
			item = pc.getInventory().storeItem(430004, 1);
		} else if (rnd < 1190) {// 코카 인형 3%
			item = pc.getInventory().storeItem(430500, 1);
		} else if (rnd < 1690) {// 젤 15%
			item = pc.getInventory().storeItem(40074, 1);
		} else if (rnd < 3190) {// 데이 15%
			item = pc.getInventory().storeItem(40087, 1);
		} else if (rnd < 4690) {// 52레벨 퀘스트아이템상자 15%
			item = pc.getInventory().storeItem(60484, 1);
		}
		if (item == null)
			pc.sendPackets(new S_SystemMessage("보너스 아이템 획득에 실패하였습니다."), true);
		else
			pc.sendPackets(new S_ServerMessage(403, item.getName()), true);
	}

	private void 큰금빛베리아나(L1PcInstance pc) {
		// TODO 자동 생성된 메소드 스텁
		L1ItemInstance item = pc.getInventory().storeItem(40308, 5000000);
		pc.sendPackets(new S_ServerMessage(403, item.getName() + " (5000000)"),
				true);
		item = null;
		int rnd = _random.nextInt(10000);
		if (rnd < 10) {// 엘릭서(CHA) 0.1%
			item = pc.getInventory().storeItem(40308, 1);
		} else if (rnd < 60) {// 붉대 0.5%
			item = pc.getInventory().storeItem(276, 1);
		} else if (rnd < 110) {// 흑키 0.5%
			item = pc.getInventory().storeItem(410004, 1);
		} else if (rnd < 160) {// 달장 0.5%
			item = pc.getInventory().storeItem(205, 1);
		} else if (rnd < 260) {// 데몬장갑 1%
			item = pc.getInventory().storeItem(20165, 1);
		} else if (rnd < 360) {// 데몬부츠 1%
			item = pc.getInventory().storeItem(20197, 1);
		} else if (rnd < 460) {// 흑장로브 1%
			item = pc.getInventory().storeItem(20160, 1);
		} else if (rnd < 560) {// 흑장샌달 1%
			item = pc.getInventory().storeItem(20218, 1);
		} else if (rnd < 660) {// 제니스반지 1%
			item = pc.getInventory().storeItem(20298, 1);
		} else if (rnd < 960) {// 마법인형 - 늑인 3%
			item = pc.getInventory().storeItem(41250, 1);
		} else if (rnd < 1260) {// 마법인형 - 돌골렘 3%
			item = pc.getInventory().storeItem(430000, 1);
		} else if (rnd < 1560) {// 마법인형 - 서큐 3%
			item = pc.getInventory().storeItem(41249, 1);
		} else if (rnd < 2260) {// 축데이 7%
			item = pc.getInventory().storeItem(140087, 1);
		} else if (rnd < 2960) {// 축젤 7%
			item = pc.getInventory().storeItem(140074, 1);
		} else if (rnd < 3960) {// 52레벨 퀘스트아이템상자 10%
			item = pc.getInventory().storeItem(60485, 1);
		}
		if (item == null)
			pc.sendPackets(new S_SystemMessage("보너스 아이템 획득에 실패하였습니다."), true);
		else
			pc.sendPackets(new S_ServerMessage(403, item.getName()), true);
	}

	private void 기본장비상자(L1PcInstance pc) {
		// TODO 자동 생성된 메소드 스텁
		createNewItem2(pc, 20028, 1, 4, 0, 129);
		createNewItem2(pc, 21098, 1, 4, 129, 0);
		createNewItem2(pc, 20126, 1, 4, 0, 0);
		createNewItem2(pc, 20173, 1, 4, 0, 0);
		createNewItem2(pc, 20206, 1, 4, 0, 0);
		createNewItem2(pc, 20232, 1, 4, 0, 0);
		createNewItem2(pc, 20082, 1, 4, 0, 0);

		if (pc.isKnight()) {
			createNewItem2(pc, 35, 1, 6, 0, 0); // 상아탑한손검
			createNewItem2(pc, 48, 1, 6, 0, 0); // 상아탑양손검
		} else if (pc.isElf()) {
			createNewItem2(pc, 175, 1, 6, 0, 0); // 상아탑활
			createNewItem2(pc, 174, 1, 6, 0, 0); // 상아탑석궁
		} else if (pc.isWizard()) {
			createNewItem2(pc, 120, 1, 6, 0, 0); // 상아탑지팡이
		} else if (pc.isDarkelf()) {
			createNewItem2(pc, 73, 1, 6, 0, 0); // 상아탑이도류
			createNewItem2(pc, 156, 1, 6, 0, 0); // 상아탑크로우
		} else if (pc.isIllusionist()) {
			createNewItem2(pc, 147, 1, 6, 0, 0); // 상아탑도끼
			createNewItem2(pc, 120, 1, 6, 0, 0); // 상아탑지팡이
		} else if (pc.isDragonknight()) {
			createNewItem2(pc, 48, 1, 6, 0, 0); // 상아탑양손검
			createNewItem2(pc, 147, 1, 6, 0, 0); // 상아탑도끼
		} else if (pc.isCrown()) {
			createNewItem2(pc, 35, 1, 6, 0, 0); // 상아탑한손검
			createNewItem2(pc, 48, 1, 6, 0, 0); // 상아탑양손검
		}
	}

	private void 클래스스킬북(L1PcInstance pc) {
		if (pc.isCrown()) {
			if (pc.getInventory().getSize() < 173) {
				for (int i = 40226; i <= 40231; i++) {
					createNewItem2(pc, i, 1, 0); // 군주 스킬
				}
				createNewItem2(pc, 60348, 1, 0); // 아바타
			} else {
				pc.sendPackets(new S_SystemMessage("인벤토리에 7개의 빈공간이 필요합니다."));
			}
		}

		if (pc.isKnight()) {
			if (pc.getInventory().getSize() < 175) {
				for (int i = 40164; i <= 40166; i++) {
					createNewItem2(pc, i, 1, 0); // 기사 스킬
				}
				createNewItem2(pc, 41147, 1, 0); // 기사 스킬
				// createNewItem2(pc, 41148, 1, 0); //기사 스킬카베
			} else {
				pc.sendPackets(new S_SystemMessage("인벤토리에 4개의 빈공간이 필요합니다."));
				// pc.sendPackets(new S_SystemMessage("인벤토리에 5개의 빈공간이 필요합니다."));
			}
		}

		if (pc.isElf()) {
			if (pc.getInventory().getSize() < 142) {
				for (int i = 40232; i <= 40264; i++) {// 38
					createNewItem2(pc, i, 1, 0); // 요정스킬
				}
				for (int i = 41150; i <= 41153; i++) {
					createNewItem2(pc, i, 1, 0); // 요정스킬
				}
			} else {
				pc.sendPackets(new S_SystemMessage("인벤토리에 38개의 빈공간이 필요합니다."));
			}
		}
		if (pc.isWizard()) {
			if (pc.getInventory().getSize() < 124) {
				for (int i = 40170; i <= 40225; i++) {
					if (i == 40222 || i == 40223 || i == 40212)
						continue;// 디스
					createNewItem2(pc, i, 1, 0); // 법사 스킬
				}
			} else {
				pc.sendPackets(new S_SystemMessage("인벤토리에 55개의 빈공간이 필요합니다."));
			}
		}

		if (pc.isDarkelf()) {
			if (pc.getInventory().getSize() < 164) {
				for (int i = 40265; i <= 40279; i++) {
					createNewItem2(pc, i, 1, 0); // 다엘 스킬
				}
				// createNewItem2(pc, 60199, 1, 0); //다엘 스킬
			} else {
				pc.sendPackets(new S_SystemMessage("인벤토리에 15개의 빈공간이 필요합니다."));
			}
		}

		if (pc.isDragonknight()) {
			if (pc.getInventory().getSize() < 164) {
				for (int i = 439100; i <= 439114; i++) {
					createNewItem2(pc, i, 1, 0); // 용기사 스킬
				}
			} else {
				pc.sendPackets(new S_SystemMessage("인벤토리에 15개의 빈공간이 필요합니다."));
			}
		}
		if (pc.isIllusionist()) {
			if (pc.getInventory().getSize() < 160) {
				for (int i = 439000; i <= 439019; i++) {
					createNewItem2(pc, i, 1, 0); // 용기사 스킬
				}
			} else {
				pc.sendPackets(new S_SystemMessage("인벤토리에 20개의 빈공간이 필요합니다."));
			}
		}
		if (pc.isWarrior()) {
			if (pc.getInventory().getSize() < 168) {
				for (int i = 7300; i <= 7311; i++) {
					if (i == 7310 || i == 7304 || i == 7305 || i == 7306)
						continue;// 데스페라도
					createNewItem2(pc, i, 1, 0); // 전사스킬
				}
			} else {
				pc.sendPackets(new S_SystemMessage("인벤토리에 11개의 빈공간이 필요합니다."));
			}
		}

	}

	private void 씨발(L1PcInstance pc) {
		if (pc.getInventory().checkItem(7022, 1)) {
			pc.getInventory().consumeItem(7022, 1);
			if (pc.isKnight()) {
				createNewItem2(pc, 120011, 1, 9); // 마법 방어 투구(축)
				createNewItem2(pc, 20048, 1, 7); // 혼돈의 투구
				createNewItem2(pc, 20078, 1, 9); // 혼돈의 망토
				createNewItem2(pc, 21028, 1, 9); // 힘의 티셔츠
				createNewItem2(pc, 425106, 1, 9); // 스턴 내성의 티셔츠
				createNewItem2(pc, 425108, 1, 9); // 마법 저항의 티셔츠
				createNewItem2(pc, 20200, 1, 9); // 베레스의 부츠
				createNewItem2(pc, 20183, 1, 9); // 암살군왕의 장갑
				createNewItem2(pc, 420002, 1, 7); // 크리스탈 가더
				createNewItem2(pc, 420104, 1, 9); // 안타라스의 완력
			}
			if (pc.isCrown()) {
				createNewItem2(pc, 120011, 1, 9); // 마법 방어 투구(축)
				createNewItem2(pc, 20048, 1, 7); // 혼돈의 투구
				createNewItem2(pc, 20078, 1, 9); // 혼돈의 망토
				createNewItem2(pc, 21028, 1, 9); // 힘의 티셔츠
				createNewItem2(pc, 425106, 1, 9); // 스턴 내성의 티셔츠
				createNewItem2(pc, 425108, 1, 9); // 마법 저항의 티셔츠
				createNewItem2(pc, 20178, 1, 9); // 베레스의 부츠
				createNewItem2(pc, 20183, 1, 9); // 암살군왕의 장갑
				createNewItem2(pc, 420002, 1, 7); // 크리스탈 가더
				createNewItem2(pc, 420105, 1, 9); // 안타라스의 완력
			}
			if (pc.isWizard()) {
				createNewItem2(pc, 120011, 1, 9); // 마법 방어 투구(축)
				createNewItem2(pc, 20048, 1, 7); // 혼돈의 투구
				createNewItem2(pc, 20078, 1, 9); // 혼돈의 망토
				createNewItem2(pc, 21028, 1, 9); // 힘의 티셔츠
				createNewItem2(pc, 425106, 1, 9); // 스턴 내성의 티셔츠
				createNewItem2(pc, 425108, 1, 9); // 마법 저항의 티셔츠
				createNewItem2(pc, 20218, 1, 9); // 베레스의 부츠
				createNewItem2(pc, 20183, 1, 9); // 암살군왕의 장갑
				createNewItem2(pc, 420002, 1, 7); // 크리스탈 가더
				createNewItem2(pc, 420107, 1, 9); // 안타라스의 완력
			}
			if (pc.isElf()) {
				createNewItem2(pc, 120011, 1, 9); // 마법 방어 투구(축)
				createNewItem2(pc, 20048, 1, 7); // 혼돈의 투구
				createNewItem2(pc, 20078, 1, 9); // 혼돈의 망토
				createNewItem2(pc, 21028, 1, 9); // 힘의 티셔츠
				createNewItem2(pc, 425106, 1, 9); // 스턴 내성의 티셔츠
				createNewItem2(pc, 425108, 1, 9); // 마법 저항의 티셔츠
				createNewItem2(pc, 20216, 1, 9); // 베레스의 부츠
				createNewItem2(pc, 20190, 1, 9); // 암살군왕의 장갑
				createNewItem2(pc, 420002, 1, 7); // 크리스탈 가더
				createNewItem2(pc, 420106, 1, 9); // 안타라스의 완력
			}
			if (pc.isDarkelf()) {
				createNewItem2(pc, 120011, 1, 9); // 마법 방어 투구(축)
				createNewItem2(pc, 20048, 1, 7); // 혼돈의 투구
				createNewItem2(pc, 20078, 1, 9); // 혼돈의 망토
				createNewItem2(pc, 21028, 1, 9); // 힘의 티셔츠
				createNewItem2(pc, 425106, 1, 9); // 스턴 내성의 티셔츠
				createNewItem2(pc, 425108, 1, 9); // 마법 저항의 티셔츠
				createNewItem2(pc, 20200, 1, 9); // 베레스의 부츠
				createNewItem2(pc, 20183, 1, 9); // 암살군왕의 장갑
				createNewItem2(pc, 420002, 1, 7); // 크리스탈 가더
				createNewItem2(pc, 420106, 1, 9); // 안타라스의 완력
			}
			if (pc.isDragonknight()) {
				createNewItem2(pc, 120011, 1, 9); // 마법 방어 투구(축)
				createNewItem2(pc, 20048, 1, 7); // 혼돈의 투구
				createNewItem2(pc, 20078, 1, 9); // 혼돈의 망토
				createNewItem2(pc, 21028, 1, 9); // 힘의 티셔츠
				createNewItem2(pc, 425106, 1, 9); // 스턴 내성의 티셔츠
				createNewItem2(pc, 425108, 1, 9); // 마법 저항의 티셔츠
				createNewItem2(pc, 20200, 1, 9); // 베레스의 부츠
				createNewItem2(pc, 20183, 1, 9); // 죽비
				createNewItem2(pc, 420002, 1, 7); // 크리스탈 가더
				createNewItem2(pc, 420105, 1, 9); // 안타라스의 완력
			}
			if (pc.isIllusionist()) {
				createNewItem2(pc, 120011, 1, 9); // 마법 방어 투구(축)
				createNewItem2(pc, 20048, 1, 7); // 혼돈의 투구
				createNewItem2(pc, 20078, 1, 9); // 혼돈의 망토
				createNewItem2(pc, 21028, 1, 9); // 힘의 티셔츠
				createNewItem2(pc, 425106, 1, 9); // 스턴 내성의 티셔츠
				createNewItem2(pc, 425108, 1, 9); // 마법 저항의 티셔츠
				createNewItem2(pc, 20200, 1, 9); // 베레스의 부츠
				createNewItem2(pc, 20183, 1, 9); // 암살군왕의 장갑
				createNewItem2(pc, 420002, 1, 7); // 크리스탈 가더
				createNewItem2(pc, 420107, 1, 9); // 안타라스의 완력
			}
		}
		// *******************지원상자(1단계)************************//
	}

	private void 페어리선물상자(L1PcInstance pc, int itemid) {
		if (itemid != 7334) {

			if (pc.isCrown()) {
				createNewItem2(pc, 60133, 10, 0); // 악마의피
			}

			if (pc.isWarrior() || pc.isKnight()) {
				createNewItem2(pc, 60134, 10, 0); // 용기
			}

			if (pc.isElf()) {
				createNewItem2(pc, 60135, 10, 0); // 와퍼
			}

			if (pc.isWizard()) {
				createNewItem2(pc, 240016, 10, 0); // 지혜
			}

			if (pc.isDarkelf()) {
				createNewItem2(pc, 60139, 10, 0); // 흑요석
			}

			if (pc.isDragonknight()) {
				createNewItem2(pc, 60140, 10, 0); // 뼈조각
			}

			if (pc.isIllusionist()) {
				createNewItem2(pc, 60136, 10, 0); // 유그드라물약
			}

		}

		if (itemid == 7325) {// 10레벨
			createNewItem2(pc, 7312, 1, 0); // 성장물약
			if (pc.isIllusionist()) {
				createNewItem2(pc, 439004, 1, 0); // 이그니션
				createNewItem2(pc, 439000, 1, 0); // 미러 이미지
				createNewItem2(pc, 439001, 1, 0); // 컨퓨전
			}
			if (pc.isElf()) {
				createNewItem2(pc, 40233, 1, 0); // 바디 투 마인드
				createNewItem2(pc, 40234, 1, 0); // 바디 투 마인드
			}
		} else if (itemid == 7326) {// 15레벨
			createNewItem2(pc, 60255, 1, 0); // 드래곤 자수정
			if (pc.isCrown()) {
				createNewItem2(pc, 40226, 1, 0); // 트루타겟
			}
			if (pc.isDarkelf()) {
				createNewItem2(pc, 40268, 1, 0); // 브링스톤
			}
			if (pc.isDragonknight()) {
				createNewItem2(pc, 439100, 1, 0); // 드래곤스킨
				createNewItem2(pc, 439101, 1, 0); // 버닝슬레이어
			}
			if (pc.isWarrior()) {
				createNewItem2(pc, 7302, 1, 0); // 슬레이어
			}
		} else if (itemid == 7327) {// 20레벨
			createNewItem2(pc, 7312, 1, 0); // 성장물약
			if (pc.isWizard()) {
				createNewItem2(pc, 40170, 1, 0); // 파볼
			}
		} else if (itemid == 7328) {// 25레벨
			createNewItem2(pc, 437004, 1, 0); // 전강
			if (pc.isWizard()) {
				createNewItem2(pc, 40188, 1, 0); // 헤이스트
			}
		} else if (itemid == 7329) {// 30레벨
			if (pc.isCrown()) {
				createNewItem2(pc, 40228, 1, 0); // 콜클랜
			}
			if (pc.isDarkelf()) {
				createNewItem2(pc, 40270, 1, 0); // 무빙 악셀
			}
			if (pc.isDragonknight()) {
				createNewItem2(pc, 439106, 1, 0); // 포우
				createNewItem2(pc, 439105, 1, 0); // 블러드러스트
			}
			if (pc.isIllusionist()) {
				createNewItem2(pc, 439014, 1, 0); // 큐브쇼크
			}
			if (pc.isWarrior()) {
				createNewItem2(pc, 7307, 1, 0); // 하울
			}
		} else if (itemid == 7330) {// 35레벨
			createNewItem2(pc, 40308, 50000, 0); // 아데나
		} else if (itemid == 7331) {// 40레벨
			createNewItem2(pc, 437004, 2, 0); // 전강
			if (pc.isElf()) {
				createNewItem2(pc, 40243, 1, 0); // 서먼레서 엘리멘탈
			}
		} else if (itemid == 7332) {// 45레벨
			createNewItem2(pc, 7312, 1, 0); // 성장물약
			if (pc.isDarkelf()) {
				createNewItem2(pc, 40276, 1, 0); // 언케니닷지
			}
			if (pc.isWarrior()) {
				createNewItem2(pc, 7300, 1, 0); // 크래쉬
			}
		} else if (itemid == 7333) {// 50레벨
			createNewItem2(pc, 60061, 1, 0); // 버프코인
		} else if (itemid == 7334) {// 52레벨
			createNewItem2(pc, 60359, 10, 0); // 조우의 변신 주문서
			createNewItem2(pc, 140100, 10, 0); // 축순
			createNewItem2(pc, 60381, 1, 0); // 낡은고서
			createNewItem2(pc, 140032, 5, 0); // 에바의가호
			createNewItem2(pc, 50751, 1, 0); // 전사의환영물약
			if (pc.isCrown()) {
				createNewItem2(pc, 51, 1, 0); // 황금 지휘봉
				createNewItem2(pc, 20051, 1, 0); // 군주의 위엄
			}
			if (pc.isKnight()) {
				createNewItem2(pc, 56, 1, 0); // 데스 블레이드
				createNewItem2(pc, 20318, 1, 0); // 용기의 벨트
			}
			if (pc.isElf()) {
				createNewItem2(pc, 184, 1, 0); // 화염의 활
				createNewItem2(pc, 50, 1, 0); // 화염의 검
			}
			if (pc.isWizard()) {
				createNewItem2(pc, 20055, 1, 0); // 마나망토
				createNewItem2(pc, 20225, 1, 0); // 마나 수정구
			}
			if (pc.isDarkelf()) {
				createNewItem2(pc, 20195, 1, 0); // 그부
				createNewItem2(pc, 13, 1, 0); // 핑거 오브 데스
			}
			if (pc.isDragonknight()) {
				createNewItem2(pc, 410000, 1, 0); // 소멸자의 체인소드
				createNewItem2(pc, 420001, 1, 0); // 용비늘 가더
			}
			if (pc.isIllusionist()) {
				createNewItem2(pc, 420006, 1, 0); // 환술사의 마법서
				createNewItem2(pc, 410003, 1, 0); // 사파이어 키링크
			}
			if (pc.isWarrior()) {
				createNewItem2(pc, 7231, 1, 0); // 대장장이의도끼
				createNewItem2(pc, 7247, 1, 0); // 전사단의 투구
			}
		}
	}

	private void 폭풍의지원상자(L1PcInstance pc) {
		// TODO 자동 생성된 메소드 스텁
		// pc.getInventory().storeItem(L1ItemId.ADENA, 50000000);
		createNewItem2(pc, 7258, 1, 0, 0, 0); // 스킬북7258
		if (pc.isKnight()) {
			createNewItem2(pc, 9078, 1, 8); // 각인붉대

			createNewItem2(pc, 9091, 1, 6); // 마투
			createNewItem2(pc, 9092, 1, 6); // 마망
			createNewItem2(pc, 9089, 1, 6); // 티셔츠
			createNewItem2(pc, 9088, 1, 6); // 강부
			createNewItem2(pc, 9086, 1, 0); // 고판
			createNewItem2(pc, 9087, 1, 6); // 파글
		} else if (pc.isWarrior()) {
			createNewItem2(pc, 9075, 1, 8); // 질풍의 도끼

			createNewItem2(pc, 9083, 1, 6); // 전사단의 투구
			createNewItem2(pc, 9084, 1, 6); // 은망
			createNewItem2(pc, 9086, 1, 0); // 고판
			createNewItem2(pc, 9087, 1, 6); // 파글
			createNewItem2(pc, 9088, 1, 6); // 강부
			createNewItem2(pc, 9089, 1, 6); // 티셔츠

		} else if (pc.isElf()) {
			createNewItem2(pc, 9080, 1, 8); // 각인살천
			createNewItem2(pc, 9091, 1, 6); // 마투
			createNewItem2(pc, 9092, 1, 6); // 마망
			createNewItem2(pc, 9089, 1, 6); // 티셔츠
			createNewItem2(pc, 9088, 1, 6); // 강부
			createNewItem2(pc, 9090, 1, 0); // 고가죽
			createNewItem2(pc, 9087, 1, 6); // 파글
		} else if (pc.isWizard()) {
			createNewItem2(pc, 9082, 1, 0); // 각인 데몬지
			createNewItem2(pc, 9091, 1, 6); // 마투
			createNewItem2(pc, 9092, 1, 6); // 마망
			createNewItem2(pc, 9089, 1, 6); // 티셔츠
			createNewItem2(pc, 9088, 1, 6); // 강부
			createNewItem2(pc, 9085, 1, 0); // 고롭
			createNewItem2(pc, 9087, 1, 6); // 파글
		} else if (pc.isDarkelf()) {
			createNewItem2(pc, 9081, 1, 8); // 각인 파이
			createNewItem2(pc, 9091, 1, 6); // 마투
			createNewItem2(pc, 9092, 1, 6); // 마망
			createNewItem2(pc, 9089, 1, 6); // 티셔츠
			createNewItem2(pc, 9088, 1, 6); // 강부
			createNewItem2(pc, 9090, 1, 0); // 고가죽
			createNewItem2(pc, 9087, 1, 6); // 파글
		} else if (pc.isIllusionist()) {
			createNewItem2(pc, 9077, 1, 8); // 각인 공명
			createNewItem2(pc, 9091, 1, 6); // 마투
			createNewItem2(pc, 9092, 1, 6); // 마망
			createNewItem2(pc, 9089, 1, 6); // 티셔츠
			createNewItem2(pc, 9088, 1, 6); // 강부
			createNewItem2(pc, 9085, 1, 0); // 고롭
			createNewItem2(pc, 9087, 1, 6); // 파글
		} else if (pc.isDragonknight()) {
			createNewItem2(pc, 9076, 1, 8); // 각인환체
			createNewItem2(pc, 9091, 1, 6); // 마투
			createNewItem2(pc, 9092, 1, 6); // 마망
			createNewItem2(pc, 9089, 1, 6); // 티셔츠
			createNewItem2(pc, 9088, 1, 6); // 강부
			createNewItem2(pc, 9090, 1, 0); // 고가죽
			createNewItem2(pc, 9087, 1, 6); // 파글
		} else if (pc.isCrown()) {
			createNewItem2(pc, 9079, 1, 8); // 각인 뇌신
			createNewItem2(pc, 9091, 1, 6); // 마투
			createNewItem2(pc, 9092, 1, 6); // 마망
			createNewItem2(pc, 9089, 1, 6); // 티셔츠
			createNewItem2(pc, 9088, 1, 6); // 강부
			createNewItem2(pc, 9093, 1, 0); // 고가죽
			createNewItem2(pc, 9087, 1, 6); // 파글
		}
	}
 
	private void 기르타스의사념TOTAL(L1PcInstance pc, int itemId) {
		// TODO 자동 생성된 메소드 스텁
		int i = 0;
		if (itemId == 60311)
			i = 기르타스사념(pc);
		else
			i = 진귀한기르타스사념(pc);
		if (i != 0) {
			int count = 1;
			if (i == 40087 || i == 40074 || i == 140087 || i == 240087
					|| i == 140074 || i == 240074)
				count = 10;
			else if (i >= 40048 && i <= 40055)
				count = 30;
			else if (i == 430024)
				count = 3;
			else if (i >= 40304 && i <= 40307)
				count = 50;
			else if (i == 40498 || i == 40491)
				count = 5;
			L1ItemInstance item = pc.getInventory().storeItem(i, count);
			pc.sendPackets(new S_ServerMessage(403, item.getName()));
		}
	}

	private void 민티스마법인형선물상자(L1PcInstance pc) {
		// TODO 자동 생성된 메소드 스텁
		L1ItemInstance item = null;
		if (pc.isCrown()) {
			if (pc.get_sex() == 0)
				item = pc.getInventory().storeItem(60454, 1);
			else
				item = pc.getInventory().storeItem(60447, 1);
		} else if (pc.isKnight()) {
			if (pc.get_sex() == 0)
				item = pc.getInventory().storeItem(60455, 1);
			else
				item = pc.getInventory().storeItem(60448, 1);
		} else if (pc.isWizard()) {
			if (pc.get_sex() == 0)
				item = pc.getInventory().storeItem(60456, 1);
			else
				item = pc.getInventory().storeItem(60449, 1);
		} else if (pc.isElf()) {
			if (pc.get_sex() == 0)
				item = pc.getInventory().storeItem(60457, 1);
			else
				item = pc.getInventory().storeItem(60450, 1);
		} else if (pc.isDarkelf()) {
			if (pc.get_sex() == 0)
				item = pc.getInventory().storeItem(60458, 1);
			else
				item = pc.getInventory().storeItem(60451, 1);
		} else if (pc.isDragonknight()) {
			if (pc.get_sex() == 0)
				item = pc.getInventory().storeItem(60459, 1);
			else
				item = pc.getInventory().storeItem(60452, 1);
		} else if (pc.isIllusionist()) {
			if (pc.get_sex() == 0)
				item = pc.getInventory().storeItem(60460, 1);
			else
				item = pc.getInventory().storeItem(60453, 1);
		}
		if (item != null)
			pc.sendPackets(new S_ServerMessage(403, item.getName()), true);
	}

	private void 신기한반지4개묶음(L1PcInstance pc, int itemId) {
		// TODO 자동 생성된 메소드 스텁
		if (itemId == 60373) { // 마법저항 4개묶음
			pc.getInventory().consumeItem(60373, 1); // 삭제되는 아이템과 수량
			for (int i = 0; i < 4; i++) {
				int ran = _random.nextInt(100) + 1;
				if (ran <= 5) {
					createNewItem2(pc, 625113, 1, 0); // 룸티스강화줌서
				} else
					createNewItem2(pc, 525113, 1, 0); // 룸티스강화줌서
			}
			createNewItem2(pc, 530040, 32, 0); // 룸티스강화줌서
		} else if (itemId == 60374) { // 마나 4개묶음
			pc.getInventory().consumeItem(60374, 1); // 삭제되는 아이템과 수량
			for (int i = 0; i < 4; i++) {
				int ran = _random.nextInt(100) + 1;
				if (ran <= 5) {
					createNewItem2(pc, 625112, 1, 0); // 룸티스강화줌서
				} else
					createNewItem2(pc, 525112, 1, 0); // 룸티스강화줌서
			}
			createNewItem2(pc, 530040, 32, 0); // 룸티스강화줌서
		} else if (itemId == 60375) { // 체력 4개묶음
			pc.getInventory().consumeItem(60375, 1); // 삭제되는 아이템과 수량
			for (int i = 0; i < 4; i++) {
				int ran = _random.nextInt(100) + 1;
				if (ran <= 5) {
					createNewItem2(pc, 625111, 1, 0); // 룸티스강화줌서
				} else
					createNewItem2(pc, 525111, 1, 0); // 룸티스강화줌서
			}
			createNewItem2(pc, 530040, 32, 0); // 룸티스강화줌서
		} else if (itemId == 60376) { // 회복 4개묶음
			pc.getInventory().consumeItem(60376, 1); // 삭제되는 아이템과 수량
			for (int i = 0; i < 4; i++) {
				int ran = _random.nextInt(100) + 1;
				if (ran <= 5) {
					createNewItem2(pc, 625109, 1, 0); // 룸티스강화줌서
				} else
					createNewItem2(pc, 525109, 1, 0); // 룸티스강화줌서
			}
			createNewItem2(pc, 530040, 32, 0); // 룸티스강화줌서
		} else if (itemId == 60377) { // 집중 4개묶음
			pc.getInventory().consumeItem(60377, 1); // 삭제되는 아이템과 수량
			for (int i = 0; i < 4; i++) {
				int ran = _random.nextInt(100) + 1;
				if (ran <= 5) {
					createNewItem2(pc, 625110, 1, 0); // 룸티스강화줌서
				} else
					createNewItem2(pc, 525110, 1, 0); // 룸티스강화줌서
			}
			createNewItem2(pc, 530040, 32, 0); // 룸티스강화줌서
		}
	}

	private void 스냅퍼의반지(L1PcInstance pc, int itemId, L1ItemInstance useItem) {
		// TODO 자동 생성된 메소드 스텁
		if (itemId == 60413) { // 스냅퍼 지혜 4개묶음
			pc.getInventory().removeItem(useItem, 1);
			for (int i = 0; i < 4; i++) {
				int ran = _random.nextInt(100) + 1;
				if (ran <= 5) {
					createNewItem2(pc, 21250, 1, 0); // 축복
				} else
					createNewItem2(pc, 21246, 1, 0);
			}
			createNewItem2(pc, 60417, 32, 0); // 스냅퍼 반지 강화줌서
		} else if (itemId == 60414) { // 스냅퍼 마법 저항 4개묶음
			pc.getInventory().removeItem(useItem, 1);
			for (int i = 0; i < 4; i++) {
				int ran = _random.nextInt(100) + 1;
				if (ran <= 5) {
					createNewItem2(pc, 21251, 1, 0); // 축복
				} else
					createNewItem2(pc, 21247, 1, 0);
			}
			createNewItem2(pc, 60417, 32, 0); // 스냅퍼 반지 강화줌서
		} else if (itemId == 60415) { // 스냅퍼 체력 4개묶음
			pc.getInventory().removeItem(useItem, 1);
			for (int i = 0; i < 4; i++) {
				int ran = _random.nextInt(100) + 1;
				if (ran <= 5) {
					createNewItem2(pc, 21252, 1, 0); // 축복
				} else
					createNewItem2(pc, 21248, 1, 0);
			}
			createNewItem2(pc, 60417, 32, 0); // 스냅퍼 반지 강화줌서
		} else if (itemId == 60416) { // 스냅퍼 용사 4개묶음
			pc.getInventory().removeItem(useItem, 1);
			for (int i = 0; i < 4; i++) {
				int ran = _random.nextInt(100) + 1;
				if (ran <= 5) {
					createNewItem2(pc, 21253, 1, 0); // 축복
				} else
					createNewItem2(pc, 21249, 1, 0);
			}
			createNewItem2(pc, 60417, 32, 0); // 스냅퍼 반지 강화줌서
		} else if (itemId == 60418) { // 스냅퍼 지혜
			pc.getInventory().removeItem(useItem, 1);
			int ran = _random.nextInt(100) + 1;
			if (ran <= 5) {
				createNewItem2(pc, 21250, 1, 0); // 축복
			} else
				createNewItem2(pc, 21246, 1, 0);
			createNewItem2(pc, 60417, 8, 0); // 스냅퍼 반지 강화줌서
		} else if (itemId == 60419) { // 스냅퍼 마법 저항
			pc.getInventory().removeItem(useItem, 1);
			int ran = _random.nextInt(100) + 1;
			if (ran <= 5) {
				createNewItem2(pc, 21251, 1, 0); // 축복
			} else
				createNewItem2(pc, 21247, 1, 0);
			createNewItem2(pc, 60417, 8, 0); // 스냅퍼 반지 강화줌서
		} else if (itemId == 60420) { // 스냅퍼 체력
			pc.getInventory().removeItem(useItem, 1);
			int ran = _random.nextInt(100) + 1;
			if (ran <= 5) {
				createNewItem2(pc, 21252, 1, 0); // 축복
			} else
				createNewItem2(pc, 21248, 1, 0);
			createNewItem2(pc, 60417, 8, 0); // 스냅퍼 반지 강화줌서
		} else if (itemId == 60421) { // 스냅퍼 용사
			pc.getInventory().removeItem(useItem, 1);
			int ran = _random.nextInt(100) + 1;
			if (ran <= 5) {
				createNewItem2(pc, 21253, 1, 0); // 축복
			} else
				createNewItem2(pc, 21249, 1, 0);
			createNewItem2(pc, 60417, 8, 0); // 스냅퍼 반지 강화줌서
		}
	}

	private void 군주의포상(L1PcInstance pc, L1ItemInstance useItem) {
		// TODO 자동 생성된 메소드 스텁
		Timestamp lastUsed = useItem.getLastUsed();
		if (lastUsed == null || currentDate.getTimeInMillis() > lastUsed.getTime() + (60 * 60000)) {
			useItem.setLastUsed(new Timestamp(currentDate.getTimeInMillis()));
			int count = 200;
			int count1 = 10;
			L1ItemInstance item = pc.getInventory().storeItem(6344, count);
			pc.sendPackets(new S_ServerMessage(403, item.getName() + " ("+ count + ")"), true);
			pc.getInventory().removeItem(useItem, 1);
			L1ItemInstance item1 = pc.getInventory().storeItem(6348, count1);
			pc.sendPackets(new S_ServerMessage(403, "축복받은 "+item1.getName() + " ("+ count1 + ")"), true);
			pc.getInventory().removeItem(useItem, 1);
		} else {
			long i = (lastUsed.getTime() + (60 * 60000)) - currentDate.getTimeInMillis();
			Calendar cal = (Calendar) currentDate.clone();
			cal.setTimeInMillis(cal.getTimeInMillis() + i);
			pc.sendPackets(new S_SystemMessage(i / 60000 + "분 동안("
					+ cal.getTime().getHours() + ":"
					+ cal.getTime().getMinutes() + " 까지)은 사용할 수 없습니다."), true);
		}
	}

	private void 붉은기사단의지령서주머니(L1PcInstance pc, L1ItemInstance useItem) {
		// TODO 자동 생성된 메소드 스텁
		Timestamp lastUsed = useItem.getLastUsed();
		if (lastUsed == null
				|| currentDate.getTimeInMillis() > lastUsed.getTime()
						+ (1000 * 60 * 60 * 22)) {// 22시간
			if (pc.getInventory().checkItem(60392)) {
				pc.sendPackets(new S_SystemMessage(
						"특정 아이템이 인벤토리에 이미 있어 더 받을 수 없습니다."));
				return;
			}
			pc.getInventory().storeItem(60392, 1);
			pc.sendPackets(new S_ServerMessage(403, "붉은 기사단의 지령서"), true);
			pc.getInventory().consumeItem(60391, 1);
			useItem.setLastUsed(new Timestamp(currentDate.getTimeInMillis()));
		} else {
			long i = (lastUsed.getTime() + (60 * 60000 * 22))
					- currentDate.getTimeInMillis();
			Calendar cal = (Calendar) currentDate.clone();
			cal.setTimeInMillis(cal.getTimeInMillis() + i);
			pc.sendPackets(new S_SystemMessage(i / 60000 + "분 동안("
					+ cal.getTime().getHours() + ":"
					+ cal.getTime().getMinutes() + " 까지)은 사용할 수 없습니다."), true);
		}
	}

	private void 룬마력제거제(L1PcInstance pc, L1ItemInstance useItem,
			L1ItemInstance targetItem) {
		if (targetItem == null)
			return;
		if (targetItem.getItemId() >= 21207 && targetItem.getItemId() <= 21241){
			pc.getInventory().removeItem(targetItem, 1);
			pc.getInventory().storeItem(60385, 1, true);
			pc.getInventory().removeItem(useItem, 1);
		} else if((targetItem.getItemId() >= 9000 && targetItem.getItemId() <= 9004)
				|| (targetItem.getItemId() >= 9010 && targetItem.getItemId() <= 9014)
				|| (targetItem.getItemId() >= 9020 && targetItem.getItemId() <= 9024)
				|| (targetItem.getItemId() >= 9030 && targetItem.getItemId() <= 9034)
				|| (targetItem.getItemId() >= 9040 && targetItem.getItemId() <= 9044)
				|| (targetItem.getItemId() >= 9050 && targetItem.getItemId() <= 9054)
				|| (targetItem.getItemId() >= 9060 && targetItem.getItemId() <= 9064)
				|| (targetItem.getItemId() >= 9070 && targetItem.getItemId() <= 9074)) {
			pc.getInventory().removeItem(targetItem, 1);
			pc.getInventory().storeItem(7658, 1, true);
			pc.getInventory().removeItem(useItem, 1);
		} else if ((targetItem.getItemId() >= 9900 && targetItem.getItemId() <= 9904)
				|| (targetItem.getItemId() >= 9910 && targetItem.getItemId() <= 9914)
				|| (targetItem.getItemId() >= 9920 && targetItem.getItemId() <= 9924)
				|| (targetItem.getItemId() >= 9930 && targetItem.getItemId() <= 9934)
				|| (targetItem.getItemId() >= 9940 && targetItem.getItemId() <= 9944)
				|| (targetItem.getItemId() >= 9950 && targetItem.getItemId() <= 9954)
				|| (targetItem.getItemId() >= 9960 && targetItem.getItemId() <= 9964)
				|| (targetItem.getItemId() >= 9970 && targetItem.getItemId() <= 9974)) {
			pc.getInventory().removeItem(targetItem, 1);
			pc.getInventory().storeItem(7358, 1, true);
			pc.getInventory().removeItem(useItem, 1);
		} else if ((targetItem.getItemId() >= 9800 && targetItem.getItemId() <= 9804)
				|| (targetItem.getItemId() >= 9810 && targetItem.getItemId() <= 9814)
				|| (targetItem.getItemId() >= 9820 && targetItem.getItemId() <= 9824)
				|| (targetItem.getItemId() >= 9830 && targetItem.getItemId() <= 9834)
				|| (targetItem.getItemId() >= 9840 && targetItem.getItemId() <= 9844)
				|| (targetItem.getItemId() >= 9850 && targetItem.getItemId() <= 9854)
				|| (targetItem.getItemId() >= 9860 && targetItem.getItemId() <= 9864)
				|| (targetItem.getItemId() >= 9870 && targetItem.getItemId() <= 9874)) {
			pc.getInventory().removeItem(targetItem, 1);
			pc.getInventory().storeItem(7458, 1, true);
			pc.getInventory().removeItem(useItem, 1);
		} else if ((targetItem.getItemId() >= 9700 && targetItem.getItemId() <= 9704)
				|| (targetItem.getItemId() >= 9710 && targetItem.getItemId() <= 9714)
				|| (targetItem.getItemId() >= 9720 && targetItem.getItemId() <= 9724)
				|| (targetItem.getItemId() >= 9730 && targetItem.getItemId() <= 9734)
				|| (targetItem.getItemId() >= 9740 && targetItem.getItemId() <= 9744)
				|| (targetItem.getItemId() >= 9750 && targetItem.getItemId() <= 9754)
				|| (targetItem.getItemId() >= 9760 && targetItem.getItemId() <= 9764)
				|| (targetItem.getItemId() >= 9770 && targetItem.getItemId() <= 9774)) {
			pc.getInventory().removeItem(targetItem, 1);
			pc.getInventory().storeItem(7558, 1, true);
			pc.getInventory().removeItem(useItem, 1);
		} else
			pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
	}

	private void 각인해제(L1PcInstance pc, L1ItemInstance useItem,
			L1ItemInstance targetItem) {
		if (targetItem == null)
			return;
		if (targetItem.getItemId() >= 9075 && targetItem.getItemId() <= 9093) {
			int storeid = 0;
			switch (targetItem.getItemId()) {
			case 9075:
				storeid = 7228;
				break;
			case 9076:
				storeid = 275;
				break;
			case 9077:
				storeid = 266;
				break;
			case 9078:
				storeid = 276;
				break;
			case 9079:
				storeid = 412000;
				break;
			case 9080:
				storeid = 191;
				break;
			case 9081:
				storeid = 259;
				break;
			case 9082:
				storeid = 119;
				break;

			case 9083:
				storeid = 7247;
				break;
			case 9084:
				storeid = 20074;
				break;
			case 9085:
				storeid = 20093;
				break;
			case 9086:
				storeid = 20095;
				break;
			case 9087:
				storeid = 20187;
				break;
			case 9088:
				storeid = 20194;
				break;
			case 9089:
				storeid = 20085;
				break;
			case 9090:
				storeid = 20092;
				break;
			case 9091:
				storeid = 120011;
				break;
			case 9092:
				storeid = 120056;
				break;
			case 9093:
				storeid = 20094;
				break;
			}
			if (storeid != 0) {
				pc.getInventory().removeItem(targetItem, 1);
				createNewItem2(pc, storeid, 1, targetItem.getEnchantLevel()); // 각인
																				// 해제..
				pc.getInventory().removeItem(useItem, 1);
			}
		} else
			pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
	}

	private boolean 기억확장구슬(L1PcInstance pc) {
		// TODO 자동 생성된 메소드 스텁
		if (pc.getBookmarkMax() >= 100) {
			pc.sendPackets(new S_ServerMessage(2962), true);
			return false;
		}
		pc.setBookmarkMax(pc.getBookmarkMax() + 10);
		pc.sendPackets(
				new S_PacketBox(S_PacketBox.기억창_확장, pc.getBookmarkMax()), true);
		return true;
	}

	private void 여관키(L1PcInstance pc, L1ItemInstance useItem) {
		if(pc.getMapId() >= 12852 && pc.getMapId() <= 12862){
			pc.sendPackets(new S_SystemMessage("지배의 탑에서는 사용할 수 없습니다."));
			return;
		}
		if (pc.getMapId() == 6202){
			pc.sendPackets(new S_SystemMessage("지금은 반성할때, 자꾸 이러면 아야아야해!"));
			return;
		}
		if (pc.getMap().isEscapable()) {
			int keymap = 0;
			if (useItem.getEndTime().getTime() > System.currentTimeMillis()) {
				keymap = (short) useItem.getKey();
			}
			if (keymap == 0) {
				pc.sendPackets(new S_SystemMessage("기간이 지난 여관 열쇠 입니다."));
				return;
			}
			if (keymap >= 16384 && keymap <= 16684) {
				L1Teleport.teleport(pc, 32746, 32803, (short) keymap, 5, true);// 말섬
																				// 방
			} else if (keymap >= 16896 && keymap <= 17196) {
				L1Teleport.teleport(pc, 32744, 32808, (short) keymap, 5, true);// 말섬
																				// 홀
			} else if (keymap >= 17408 && keymap <= 17708) {
				L1Teleport.teleport(pc, 32744, 32803, (short) keymap, 5, true); // 글말
																				// 방
			} else if (keymap >= 17920 && keymap <= 18220) {
				L1Teleport.teleport(pc, 32745, 32807, (short) keymap, 5, true);// 글말
																				// 홀
			} else if (keymap >= 18432 && keymap <= 18732) {
				L1Teleport.teleport(pc, 32745, 32803, (short) keymap, 5, true);// 기란
																				// 방
			} else if (keymap >= 18944 && keymap <= 19244) {
				L1Teleport.teleport(pc, 32745, 32807, (short) keymap, 5, true);// 기란
																				// 홀
			} else if (keymap >= 19456 && keymap <= 19756) {
				L1Teleport.teleport(pc, 32745, 32803, (short) keymap, 5, true);// 아덴
																				// 방
			} else if (keymap >= 19968 && keymap <= 20268) {
				L1Teleport.teleport(pc, 32745, 32807, (short) keymap, 5, true);// 아덴
																				// 홀
			} else if (keymap >= 23552 && keymap <= 23852) {
				L1Teleport.teleport(pc, 32745, 32803, (short) keymap, 5, true);// 오렌
																				// 방
			} else if (keymap >= 24064 && keymap <= 24364) {
				L1Teleport.teleport(pc, 32745, 32807, (short) keymap, 5, true);// 오렌
																				// 홀
			} else if (keymap >= 20480 && keymap <= 20780) {
				L1Teleport.teleport(pc, 32745, 32803, (short) keymap, 5, true); // 윈말
																				// 방
			} else if (keymap >= 20992 && keymap <= 21292) {
				L1Teleport.teleport(pc, 32745, 32807, (short) keymap, 5, true);// 윈말
																				// 홀
			} else if (keymap >= 21504 && keymap <= 21804) {
				L1Teleport.teleport(pc, 32745, 32803, (short) keymap, 5, true);// 은기사
																				// 방
			} else if (keymap >= 22016 && keymap <= 22316) {
				L1Teleport.teleport(pc, 32745, 32807, (short) keymap, 5, true);// 은기사
																				// 홀
			} else if (keymap >= 22528 && keymap <= 22828) {
				L1Teleport.teleport(pc, 32745, 32803, (short) keymap, 5, true);// 하이네
																				// 방
			} else if (keymap >= 23040 && keymap <= 23340) {
				L1Teleport.teleport(pc, 32745, 32807, (short) keymap, 5, true);// 하이네
																				// 홀
			} else if (keymap >= 24576 && keymap <= 24876) {
				L1Teleport.teleport(pc, 32745, 32803, (short) keymap, 5, true);// 해적섬
																				// 방
			} else if (keymap >= 25088 && keymap <= 25388) {
				L1Teleport.teleport(pc, 32745, 32807, (short) keymap, 5, true);// 해적섬
																				// 홀
			}
		}
	}

	private void 투옵티셔츠(L1PcInstance pc, int itemId, L1ItemInstance useItem) {
		// TODO 자동 생성된 메소드 스텁
		int itemid = ((itemId - 60361) * 2) + 21183;
		if (_random.nextInt(100) < 20)
			itemid += 1;
		L1ItemInstance item = pc.getInventory().storeItem(itemid, 1);
		pc.sendPackets(new S_ServerMessage(403, item.getName()), true);
		item = pc.getInventory().storeItem(430041, 10);
		pc.sendPackets(new S_ServerMessage(403, item.getName() + " (10)"), true);
		item = pc.getInventory().storeItem(1430041, 10);
		pc.sendPackets(new S_ServerMessage(403, item.getName() + " (10)"), true);
		item = pc.getInventory().storeItem(2430041, 10);
		pc.sendPackets(new S_ServerMessage(403, item.getName() + " (10)"), true);
		pc.getInventory().removeItem(useItem, 1);
	}

	private void 성장의릴(L1PcInstance pc, L1ItemInstance useItem,
			L1ItemInstance targetItem) {
		// TODO 자동 생성된 메소드 스텁
		if (targetItem == null)
			return;
		int targetItemId = targetItem.getItemId();
		int useItemId = useItem.getItemId();
		if ((targetItemId == 600229 && useItemId == 600228)) {
			targetItem.setChargeCount(targetItem.getChargeCount() + 300);
			pc.getInventory().updateItem(targetItem, L1PcInventory.COL_CHARGE_COUNT);
			pc.getInventory().removeItem(useItem, 1);
			return;
		}
		if (targetItemId != 60326)
			return;
		if (pc.isFishing()) {
			pc.setFishingTime(0);
			pc.setFishingReady(false);
			pc.setFishing(false);
			pc.setFishingItem(null);
			pc.sendPackets(new S_CharVisualUpdate(pc), true);
			Broadcaster.broadcastPacket(pc, new S_CharVisualUpdate(pc), true);
			FishingTimeController.getInstance().removeMember(pc);
		}
		pc.getInventory().removeItem(targetItem, 1);
		pc.getInventory().storeItem(600229, 1, true);
		pc.getInventory().removeItem(useItem, 1);
		pc.saveInventory();
	}

	private void 폴의쾌속릴(L1PcInstance pc, L1ItemInstance useItem,
			L1ItemInstance targetItem) {
		// TODO 자동 생성된 메소드 스텁
		if (targetItem == null)
			return;
		int targetItemId = targetItem.getItemId();
		int useItemId = useItem.getItemId();
		if ((targetItemId == 60334 && useItemId == 60333)
				|| (targetItemId == 60478 && useItemId == 60476)
				|| (targetItemId == 60479 && useItemId == 60477)) {
			if (targetItem.getChargeCount() > 400) {
				pc.sendPackets(new S_ServerMessage(3457), true);
				return;
			}
			if (targetItemId == 60478 || targetItemId == 60479)
				targetItem.setChargeCount(targetItem.getChargeCount() + 50);
			else
				targetItem.setChargeCount(targetItem.getChargeCount() + 100);
			pc.getInventory().updateItem(targetItem,
					L1PcInventory.COL_CHARGE_COUNT);
			pc.getInventory().removeItem(useItem, 1);
			return;
		}
		if (targetItemId != 60326)
			return;
		if (pc.isFishing()) {
			pc.setFishingTime(0);
			pc.setFishingReady(false);
			pc.setFishing(false);
			pc.setFishingItem(null);
			pc.sendPackets(new S_CharVisualUpdate(pc), true);
			Broadcaster.broadcastPacket(pc, new S_CharVisualUpdate(pc), true);
			FishingTimeController.getInstance().removeMember(pc);
		}
		pc.getInventory().removeItem(targetItem, 1);
		if (useItemId == 60476)
			pc.getInventory().storeItem(60478, 1, true);
		else if (useItemId == 60477)
			pc.getInventory().storeItem(60479, 1, true);
		else
			pc.getInventory().storeItem(60334, 1, true);
		pc.getInventory().removeItem(useItem, 1);
	}

	private int 진귀한기르타스사념(L1PcInstance pc) {
		// TODO 자동 생성된 메소드 스텁
		int rnd = _random.nextInt(100) + 1;
		int itemid = 0;
		if (rnd <= 1) // 디스
			itemid = 40222;
		else if (rnd <= 2) // 카베
			itemid = 41148;
		else if (rnd <= 10)
			itemid = 사념장비[_random.nextInt(사념장비.length)];
		else if (rnd <= 40)
			itemid = 젤데이[_random.nextInt(젤데이.length)];
		else
			itemid = 사념아이템[_random.nextInt(사념아이템.length)];
		return itemid;
	}

	private static final int[] 젤데이 = { 40087, 140087, 240087, 40074, 140074,
			240074 };
	private static final int[] 사념장비 = { 30229, 20422, 20071, 20059, 20061,
			20054, 20077, 30219, 20187, 30218, 266, 261, 262, 42, 190 };
	private static final int[] 사념아이템 = { 430024, 40304, 40305, 40306, 40307,
			40498, 40491, 40048, 40049, 40050, 40051, 40052, 40053, 40054,
			40055 };

	private int 기르타스사념(L1PcInstance pc) {
		// TODO 자동 생성된 메소드 스텁
		int rnd = _random.nextInt(1000) + 1;
		int itemid = 0;
		if (rnd <= 1) // 디스
			itemid = 40222;
		else if (rnd <= 2) // 카베
			itemid = 41148;
		else if (rnd <= 100)
			itemid = 사념장비[_random.nextInt(사념장비.length)];
		else if (rnd <= 400)
			itemid = 젤데이[_random.nextInt(젤데이.length)];
		else
			itemid = 사념아이템[_random.nextInt(사념아이템.length)];
		return itemid;
	}

	private int 룸티스선물(L1PcInstance pc) {
		// TODO 자동 생성된 메소드 스텁
		int rnd = _random.nextInt(100) + 1;
		int itemid = 0;
		if (rnd <= 10) {// 리퍼유물상자
			itemid = 500208;
		} else if (rnd <= 55) {
			itemid = 600219;
		} else {
			itemid = 600220;
		}
		if (itemid != 0) {
			L1ItemInstance item = pc.getInventory().storeItem(itemid, 1);
			pc.sendPackets(new S_ServerMessage(403, item.getName()));
		}
		return itemid;
	} 
	private int 룸티스지원(L1PcInstance pc) {
		// TODO 자동 생성된 메소드 스텁
		int rnd = _random.nextInt(100) + 1;
		int itemid = 0;
		int count = 1;
		if (rnd <= 25) {// 유물주머니
			itemid = 500206;
		} else if (rnd <= 35) {// 드다
			itemid = 437010;
		} else if (rnd <= 45) {// 빛성장2
			itemid = 600224;
			count = 2;
		} else if (rnd <= 55) {// 전강2
			itemid = 437004;
			count = 2;
		} else if (rnd <= 65) {// 던전기억5
			itemid = 560027;
			count = 5;
		} else if (rnd <= 75) {// 조우이동
			itemid = 60360;
			count = 5;
		} else if (rnd <= 80) {// 돌골렘
			itemid = 430000;
		} else if (rnd <= 85) {// 늑대인간
			itemid = 41250;
		} else if (rnd <= 90) {// 버그베어
			itemid = 41248;
		} else if (rnd <= 95) {// 크러스트시안
			itemid = 430002;
		} else if (rnd <= 100) {// 크러스트시안
			itemid = 430004;
		}
		if (itemid != 0) {
			L1ItemInstance item = pc.getInventory().storeItem(itemid, count);
			pc.sendPackets(new S_ServerMessage(403, item.getName() + " ("
					+ count + ")개"));
		}
		return itemid;
	}

	private int 산타양말(L1PcInstance pc) {
		// TODO 자동 생성된 메소드 스텁
		int rnd = _random.nextInt(1000000) + 1;
		int itemid = 0;
		if (rnd <= 103000) {// 리퍼유물상자
			itemid = 500208;
		} else if (rnd <= 178000) {// 아이디 변경권
			itemid = 467009;
		} else if (rnd <= 253000) {// 성별 전환 물약
			itemid = 437001;
		} else if (rnd <= 613000) {// 케플리샤의 증표
			itemid = 141917;
			/*
			 * 화염의 기운 13% 아이템 아이디 : 6022
			 * 
			 * 냉한의 기운 13% 아이템 아이디 : 7337
			 * 
			 * 변환 그림리퍼의 유물주머니 13.2
			 */

		} else if (rnd <= 743000) {// 화염
			itemid = 6022;
		} else if (rnd <= 873000) {// 냉한
			itemid = 7337;
		} else if (rnd <= 995000) {// 씨발 주머니
			itemid = 500207;

		} else if (rnd <= 999993) {// 퓨어
			itemid = 500209;
		} else if (rnd <= 999994) {// 커검
			itemid = 54;
		} else if (rnd <= 999995) {// 데불
			itemid = 58;
		} else if (rnd <= 999996) {// 악장
			itemid = 293;
		} else if (rnd <= 999997) {// 진노
			itemid = 292;
		} else if (rnd <= 999998) {// 냉키
			itemid = 6001;
		} else if (rnd <= 999999) {// 극체
			itemid = 6000;
		} else if (rnd <= 1000000) {// 제로스
			itemid = 291;
		}
		if (itemid != 0) {
			L1ItemInstance item = pc.getInventory().storeItem(itemid, 1);
			pc.sendPackets(new S_ServerMessage(403, item.getName()));
		}
		return itemid;
	}

	private void 아머브레이크(L1PcInstance pc, L1ItemInstance useItem,
			int spellsc_objid) {
		Timestamp lastUsed = useItem.getLastUsed();
		if (!pc.isSkillDelay()
				&& (lastUsed == null || currentDate.getTimeInMillis() > lastUsed
						.getTime() + (1000 * 5))) {
			L1Object temp = L1World.getInstance().findObject(spellsc_objid);
			if (temp != null && temp != pc && (pc.isDarkelf() || pc.isGm())) {
				if (temp instanceof L1Character) {
					L1Character tempCha = (L1Character) temp;
					if (pc.getCurrentHp() < 30) {
						pc.sendPackets(new S_ServerMessage(279), true);
					} else if (pc.getCurrentMp() < 40) {
						pc.sendPackets(new S_ServerMessage(278), true);
					} else if (!pc.getInventory().checkItem(40321, 2)
							&& !pc.isGm()) {
						pc.sendPackets(new S_ServerMessage(299), true);
					} else {
						pc.setCurrentHp(pc.getCurrentHp() - 30);
						pc.setCurrentMp(pc.getCurrentMp() - 40);
						pc.getInventory().consumeItem(40321, 2);
						int level_dif = pc.getLevel() - tempCha.getLevel();
						int chance = 0;
						if (level_dif <= -10)
							chance = 1;
						else if (level_dif == -9)
							chance = 2;
						else if (level_dif == -8)
							chance = 3;
						else if (level_dif == -7)
							chance = 5;
						else if (level_dif == -6)
							chance = 10;
						else if (level_dif == -5)
							chance = 15;
						else if (level_dif == -4)
							chance = 20;
						else if (level_dif == -3)
							chance = 25;
						else if (level_dif == -2)
							chance = 30;
						else if (level_dif == -1)
							chance = 35;
						else if (level_dif == 0)
							chance = 40;
						else if (level_dif == 1)
							chance = 45;
						else if (level_dif == 2)
							chance = 50;
						else if (level_dif == 3)
							chance = 55;
						else if (level_dif == 4)
							chance = 60;
						else if (level_dif == 5)
							chance = 65;
						else if (level_dif == 6)
							chance = 70;
						else if (level_dif == 7)
							chance = 75;
						else if (level_dif == 8)
							chance = 80;
						else if (level_dif == 9)
							chance = 85;
						else if (level_dif >= 10)
							chance = 90;
						if (chance >= _random.nextInt(100) + 1) {
							if (tempCha.getSkillEffectTimerSet()
									.hasSkillEffect(ARMOR_BREAK)) {
								tempCha.getSkillEffectTimerSet()
										.killSkillEffectTimer(ARMOR_BREAK);
							}
							tempCha.getSkillEffectTimerSet().setSkillEffect(
									ARMOR_BREAK, 8 * 1000);
							if (tempCha instanceof L1PcInstance) {
								((L1PcInstance) tempCha)
										.sendPackets(
												new S_SkillSound(tempCha
														.getId(), 8977), true);
								((L1PcInstance) tempCha).sendPackets(
										new S_SkillIconAura(119, 8), true);
							}
							Broadcaster.broadcastPacket(tempCha,
									new S_SkillSound(tempCha.getId(), 8977),
									true);
						} else
							pc.sendPackets(new S_SystemMessage("마법이 실패하였습니다."),
									true);
					}
				}
				pc.setSkillDelay(true);
				GeneralThreadPool.getInstance().schedule(
						new L1SkillDelay(pc, 5000), 5000);
				useItem.setLastUsed(new Timestamp(currentDate.getTimeInMillis()));
			}
		} else if (pc.isSkillDelay()) {
			if (pc.skilldelayTime > System.currentTimeMillis()) {
				SimpleDateFormat dateFormat = new SimpleDateFormat("ss초");
				String time = dateFormat.format(new Timestamp(
						(pc.skilldelayTime - currentDate.getTimeInMillis())
								+ (60 * 1000 * 60 * 15)));
				pc.sendPackets(new S_SystemMessage(time + " 후에 사용 할수 있습니다."),
						true);
				dateFormat = null;
			}
		} else {
			SimpleDateFormat dateFormat = new SimpleDateFormat("ss초");
			String time = dateFormat.format(new Timestamp(
					((lastUsed.getTime() + (1000 * 8)) - currentDate
							.getTimeInMillis()) + (60 * 1000 * 60 * 15)));
			pc.sendPackets(new S_SystemMessage(time + " 후에 사용 할수 있습니다."), true);
			dateFormat = null;
		}
	}

	private static final int[] 리퍼레어 = { 20107, 90084, 90083, 59, 130220, 20298,
			22009, 20079, 20017, 20260, 7228 };
	private static final int[] 오만층줌 = { 60200, 40104, 40105, 40106, 40107,
			40108, 40109, 401010, 401011 };

	private int 리퍼선물상자(L1PcInstance pc) {
		// TODO 자동 생성된 메소드 스텁
		int rnd = _random.nextInt(10000) + 1;
		int itemid = 0;
		int count = 1;
		if (rnd <= 1) {// 레어템득
			itemid = 리퍼레어[_random.nextInt(리퍼레어.length)];
		} else if (rnd <= 1910) {// 오만층줌
			itemid = 오만층줌[_random.nextInt(오만층줌.length)];
		}
		if (itemid != 0) {
			L1ItemInstance item = pc.getInventory().storeItem(itemid, count);
			pc.sendPackets(new S_ServerMessage(403, item.getName()));
		}
		return itemid;
	}

	private int 리퍼상자(L1PcInstance pc) {
		// TODO 자동 생성된 메소드 스텁
		int rnd = _random.nextInt(1000000) + 1;
		int itemid = 0;
		int count = 1;
		if (rnd <= 50000) {// 위대한전사의주문
			itemid = 500210;
			count = 1;
		} else if (rnd <= 80000) {// 위대한전사의주문
			itemid = 500210;
			count = 3;
		} else if (rnd <= 90000) {// 위대한전사의주문
			itemid = 500210;
			count = 5;
		} else if (rnd <= 140000) {// 위대한 치유의주문
			itemid = 60071;
			count = 1;
		} else if (rnd <= 170000) {// 위대한 치유의주문
			itemid = 60071;
			count = 3;
		} else if (rnd <= 180000) {// 위대한 치유의주문
			itemid = 60071;
			count = 4;
		} else if (rnd <= 181000) {// 퓨어
			itemid = 500209;
		} else if (rnd <= 328769) {// 오만층줌
			itemid = 60200;
		} else if (rnd <= 468769) {// 오만층줌
			itemid = 40104;
		} else if (rnd <= 608769) {// 오만층줌
			itemid = 40105;
		} else if (rnd <= 678769) {// 오만층줌
			itemid = 40106;
		} else if (rnd <= 748769) {// 오만층줌
			itemid = 40107;
		} else if (rnd <= 818769) {// 오만층줌
			itemid = 40108;
		} else if (rnd <= 868769) {// 오만층줌
			itemid = 40109;
		} else if (rnd <= 918769) {// 오만층줌
			itemid = 40110;
		} else if (rnd <= 968769) {// 오만층줌
			itemid = 40111;
		} else if (rnd <= 988769) {// 오만층줌
			itemid = 40112;

		} else if (rnd <= 988779) {// 섬멸자
			itemid = 90084;

		} else if (rnd <= 988789) {// 나양
			itemid = 69;

		} else if (rnd <= 988799) {// 표효
			itemid = 90083;

		} else if (rnd <= 988809) {// 격분
			itemid = 130220;

		} else if (rnd <= 988819) {// 리롭
			itemid = 20107;

		} else if (rnd <= 988829) {// 제반
			itemid = 20298;

		} else if (rnd <= 988839) {// 시어심안
			itemid = 22009;

		} else if (rnd <= 988849) {// 뱀망
			itemid = 20079;

		} else if (rnd <= 988859) {// 머미왕관
			itemid = 20017;

		} else if (rnd <= 988869) {// 아이리스목
			itemid = 20260;

		} else if (rnd <= 988870) {// 데스페라도
			itemid = 7310;

		} else if (rnd <= 988880) {// 블릿
			itemid = 7305;

		} else if (rnd <= 988890) {// 매직
			itemid = 7306;

		} else if (rnd <= 988900) {// 락
			itemid = 7304;

		} else if (rnd <= 989000) {// 봉ㅇ인오우거도끼
			itemid = 7335;
		} else if (rnd <= 990000) {// 봉인산적도끼
			itemid = 7336;
		} else if (rnd <= 995000) {// 질풍도끼
			itemid = 7228;
		} else {// 강철도끼
			itemid = 7229;
		}
		if (itemid != 0) {
			L1ItemInstance item = pc.getInventory().storeItem(itemid, count);
			pc.sendPackets(new S_ServerMessage(403, item.getName()));
		}
		return itemid;
	}

	private static final int[] 무관셋 = { 120020, 120058, 120113, 120168, 120201,
			120228 };
	private static final int[] 신관셋 = { 120233, 120030, 120067, 120129, 120176,
			120208 };

	private int 단테스상자(L1PcInstance pc) {
		// TODO 자동 생성된 메소드 스텁
		int rnd = _random.nextInt(134902) + 1;
		int itemid = 0;
		if (rnd <= 1)
			itemid = 40222;// 디스
		else if (rnd <= 2)
			itemid = 41148;// 카배
		else if (rnd <= 12)
			itemid = 84;// 왕도
		else if (rnd <= 22)
			itemid = 164;// 왕아
		else if (rnd <= 32)
			itemid = 189;// 왕궁
		else if (rnd <= 52)
			itemid = 60199;// 아머
		else if (rnd <= 102)
			itemid = 41149;// 소프
		else if (rnd <= 202)
			itemid = 100259; // 축파크
		else if (rnd <= 302)
			itemid = 100260; // 축파이
		else if (rnd <= 802)
			itemid = 100074; // 축은이
		else if (rnd <= 1302)
			itemid = 100157; // 축은크
		else if (rnd <= 2302)
			itemid = 81; // 흑이
		else if (rnd <= 3302)
			itemid = 162; // 흑크
		else if (rnd <= 4802)
			itemid = 194; // 진건틀렛
		else if (rnd <= 9602)
			itemid = 무관셋[_random.nextInt(무관셋.length)];
		else if (rnd <= 14402)
			itemid = 신관셋[_random.nextInt(신관셋.length)];
		else if (rnd <= 15402)
			itemid = 140087;// 축데이
		else if (rnd <= 16402)
			itemid = 140074;// 축젤
		else if (rnd <= 134902)
			itemid = 60068 + _random.nextInt(4);

		if (itemid == 60070)
			itemid = 60068 + _random.nextInt(4);

		return itemid;
	}

	private void 순백의인장and비누(L1PcInstance pc, L1ItemInstance useItem,
			L1ItemInstance targetItem) {
		// TODO 자동 생성된 메소드 스텁
		if (targetItem == null)
			return;
		if (targetItem.getBless() >= 128) {
			pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
			return;
		}
		if (useItem.getItemId() == 60282) { // 천연 비누 : 순백의 티
			if (targetItem.getItemId() >= 21139
					&& targetItem.getItemId() <= 21156) {
				if (targetItem.getBless() == 0
						&& targetItem.getItemId() < 21131) {
					createNewItem2(pc, 120085, 1, targetItem.getEnchantLevel());
				} else
					createNewItem2(pc, targetItem.getItemId() >= 21148 ? 20084
							: 20085, 1, targetItem.getEnchantLevel());
				pc.getInventory().removeItem(targetItem, 1);
			} else {
				pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
				return;
			}
		} else if (useItem.getItemId() >= 60273 && useItem.getItemId() <= 60281) {// 인장
			if (targetItem.getItemId() == 20084
					|| targetItem.getItemId() == 20085
					|| targetItem.getItemId() == 120085) {
				int itemid = 0;
				if (targetItem.getItemId() == 20084)
					itemid = useItem.getItemId() - 60273 + 21148;
				else if (targetItem.getItemId() == 20085
						|| targetItem.getItemId() == 120085)
					itemid = useItem.getItemId() - 60273 + 21139;

				if (itemid != 0) {// 여기여기
					if (targetItem.getItemId() == 120085) {
						L1ItemInstance citem = createNewItem2(pc, itemid, 1,
								targetItem.getEnchantLevel(), 0, 0, 0);
						citem.setBless(0);
						pc.getInventory().updateItem(citem,
								L1PcInventory.COL_BLESS);
						pc.getInventory().saveItem(citem,
								L1PcInventory.COL_BLESS);
					} else
						createNewItem2(pc, itemid, 1,
								targetItem.getEnchantLevel());
					pc.getInventory().removeItem(targetItem, 1);
				}
			} else {
				pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
				return;
			}
		}
		pc.getInventory().removeItem(useItem, 1);
	}

	private void 티변환석(L1PcInstance pc, L1ItemInstance useItem,
			L1ItemInstance targetItem) {
		// TODO 자동 생성된 메소드 스텁
		if (targetItem == null)
			return;
		if (targetItem.getBless() >= 128) {
			pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
			return;
		}
		// 인나드릴
		if (targetItem.getItemId() >= 490001
				&& targetItem.getItemId() <= 490017) {
			if (useItem.getItemId() == 60283) {
				createNewItem2(pc, 20085, 1, targetItem.getEnchantLevel());
				pc.getInventory().removeItem(targetItem, 1);
				pc.getInventory().removeItem(useItem, 1);
			} else if (useItem.getItemId() - 60235 == targetItem
					.getEnchantLevel()) {
				createNewItem2(pc, 20085, 1, targetItem.getEnchantLevel());
				pc.getInventory().removeItem(targetItem, 1);
				pc.getInventory().removeItem(useItem, 1);
			} else
				pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
		} else
			pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
	}

	private void 판도라향수and문양(L1PcInstance pc, L1ItemInstance useItem,
			L1ItemInstance targetItem) {
		// TODO 자동 생성된 메소드 스텁
		if (targetItem == null)
			return;
		if (targetItem.getBless() >= 128) {
			pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
			return;
		}
		if (useItem.getItemId() == 60247) { // 천연 비누
			if (targetItem.getItemId() >= 21125
					&& targetItem.getItemId() <= 21136) {
				createNewItem2(pc, targetItem.getItemId() >= 21131 ? 20084
						: 20085, 1, targetItem.getEnchantLevel());
				pc.getInventory().removeItem(targetItem, 1);
			} else {
				pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
				return;
			}
		} else if (useItem.getItemId() >= 60218 && useItem.getItemId() <= 60223) {// 향수
			if (targetItem.getItemId() == 20084
					|| targetItem.getItemId() == 20085) {
				int itemid = 0;
				if (targetItem.getItemId() == 20084)
					itemid = useItem.getItemId() - 60218 + 21131;
				else if (targetItem.getItemId() == 20085)
					itemid = useItem.getItemId() - 60218 + 21125;

				if (itemid != 0) {
					createNewItem2(pc, itemid, 1, targetItem.getEnchantLevel());
					pc.getInventory().removeItem(targetItem, 1);
				}
			} else {
				pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
				return;
			}
		} else if (useItem.getItemId() >= 60224 && useItem.getItemId() <= 60232) {// 문양
			if (targetItem.getItemId() >= 21125
					&& targetItem.getItemId() <= 21136) {
				boolean bEquipped = false;
				if (targetItem.isEquipped()) {
					pc.getInventory().setEquipped(targetItem, false);
					bEquipped = true;
				}
				switch (useItem.getItemId()) {
				case 60224:
					targetItem.setRegistLevel(10);
					break;
				case 60225:
					targetItem.setRegistLevel(11);
					break;
				case 60226:
					targetItem.setRegistLevel(12);
					break;
				case 60227:
					targetItem.setRegistLevel(13);
					break;
				case 60228:
					targetItem.setRegistLevel(14);
					break;
				case 60229:
					targetItem.setRegistLevel(15);
					break;
				case 60230:
					targetItem.setRegistLevel(16);
					break;
				case 60231:
					targetItem.setRegistLevel(17);
					break;
				case 60232:
					targetItem.setRegistLevel(18);
					break;
				default:
					break;
				}
				if (bEquipped) {
					pc.getInventory().setEquipped(targetItem, true);
				}
				pc.getInventory().updateItem(targetItem, L1PcInventory.COL_regist);
				pc.getInventory().saveItem(targetItem, L1PcInventory.COL_regist);
				pc.saveInventory();
			} else {
				pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
				return;
			}
		}
		pc.getInventory().removeItem(useItem, 1);
	}

	private int 할로윈체인키링크상자(L1PcInstance pc, int itemId) {
		// TODO 자동 생성된 메소드 스텁
		int rnd = _random.nextInt(1500) + 1;
		int itemid = 0;
		if (rnd <= 50) {
			if (itemId == 60190)// 체인소드
				itemid = 265;
			else
				// 키링크
				itemid = 264;
		} else if (rnd <= 150)
			itemid = 60197; // 호박 무기 보호 주문서
		else if (rnd <= 270)
			itemid = 60192; // 할로윈 딸기 캔디
		else if (rnd <= 390)
			itemid = 60193; // 할로윈 밀크 캔디
		else if (rnd <= 510)
			itemid = 60194; // 할로윈 바나나 캔디
		else if (rnd <= 630)
			itemid = 60195; // 할로윈 초콜릿 캔디
		else if (rnd <= 750)
			itemid = 60196; // 할로윈 호박 캔디
		else if (rnd <= 1500)
			itemid = 435000; // 할로윈 호박 파이

		return itemid;
	}

	private void 케플리샤인형(L1PcInstance pc, L1ItemInstance useItem,
			L1ItemInstance targetItem) {
		boolean isAppear = true;
		L1DollInstance doll = null;
		boolean ck = true;
		for (Object dollObject : pc.getDollList()) {
			doll = (L1DollInstance) dollObject;
			if (doll.getItemObjId() == targetItem.getId()) {
				isAppear = false;
				break;
			}
		}
		if (isAppear) {
			Random _random = new Random();
			int 랜덤 = _random.nextInt(1000);
			int 성공 = _random.nextInt(100);
			if (성공 < 40) {
				if (랜덤 < 70) {
					pc.getInventory().storeItem(430000, 1);
					pc.sendPackets(new S_ServerMessage(403, "마법인형 : 돌골렘"), true);
				} else if (랜덤 < 140) {
					pc.getInventory().storeItem(41248, 1);
					pc.sendPackets(new S_ServerMessage(403, "마법인형 : 버그베어"),
							true);
				} else if (랜덤 < 210) {
					pc.getInventory().storeItem(41249, 1);
					pc.sendPackets(new S_ServerMessage(403, "마법인형 : 서큐버스"),
							true);
				} else if (랜덤 < 280) {
					pc.getInventory().storeItem(430001, 1);
					pc.sendPackets(new S_ServerMessage(403, "마법인형 : 장로"), true);
				} else if (랜덤 < 350) {
					pc.getInventory().storeItem(430002, 1);
					pc.sendPackets(new S_ServerMessage(403, "마법인형 : 크러스트 시안"),
							true);
				} else if (랜덤 < 420) {
					pc.getInventory().storeItem(141918, 1);
					pc.sendPackets(new S_ServerMessage(403, "마법인형 : 시댄서"), true);
				} else if (랜덤 < 490) {
					pc.getInventory().storeItem(430004, 1);
					pc.sendPackets(new S_ServerMessage(403, "마법인형 : 에티"), true);
				} else if (랜덤 < 560) {
					pc.getInventory().storeItem(430500, 1);
					pc.sendPackets(new S_ServerMessage(403, "마법인형 : 코카트리스"),
							true);
				} else if (랜덤 < 630) {
					pc.getInventory().storeItem(141919, 1);
					pc.sendPackets(new S_ServerMessage(403, "마법인형 : 라미아"), true);
				} else if (랜덤 < 700) {
					pc.getInventory().storeItem(141920, 1);
					pc.sendPackets(new S_ServerMessage(403, "마법인형 : 스파토이"),
							true);
				} else if (랜덤 < 770) {
					pc.getInventory().storeItem(141922, 1);
					pc.sendPackets(new S_ServerMessage(403, "마법인형 : 에틴"), true);
				} else if (랜덤 < 841) {
					pc.getInventory().storeItem(141921, 1);
					pc.sendPackets(new S_ServerMessage(403, "마법인형 : 허수아비"),
							true);

				} else if (랜덤 < 842) {
					pc.getInventory().storeItem(500204, 1);
					pc.sendPackets(new S_ServerMessage(403, "마법인형 : 흑장로"), true);

				} else if (랜덤 < 843) {
					pc.getInventory().storeItem(500205, 1);
					pc.sendPackets(new S_ServerMessage(403, "마법인형 : 서큐버스 퀸"),
							true);

				} else if (랜덤 < 844) {
					pc.getInventory().storeItem(500203, 1);
					pc.sendPackets(new S_ServerMessage(403, "마법인형 : 자이언트"),
							true);

				} else if (랜덤 < 846) {
					pc.getInventory().storeItem(5000035, 1);
					pc.sendPackets(new S_ServerMessage(403, "마법인형 : 리치"), true);
				} else if (랜덤 < 849) {
					pc.getInventory().storeItem(500202, 1);
					pc.sendPackets(new S_ServerMessage(403, "마법인형 : 사이클롭스"),
							true);
				} else {
					pc.getInventory().storeItem(41250, 1);
					pc.sendPackets(new S_ServerMessage(403, "마법인형 : 늑대인간"),
							true);
				}

			} else {
				ck = false;
				pc.sendPackets(new S_SystemMessage("케플리샤의 축복이 스며들지 못하였습니다."));
				/*
				 * pc.getInventory().storeItem(41250, 1); pc.sendPackets(new
				 * S_ServerMessage(403, "마법인형 : 늑대인간"), true);
				 */
			}
			if (ck) {
				pc.getInventory().removeItem(useItem, 1);
				pc.getInventory().removeItem(targetItem, 1);
			}
			pc.sendPackets(new S_SkillSound(pc.getId(), 6130));
			Broadcaster.broadcastPacket(pc, new S_SkillSound(pc.getId(), 6130));
			/*
			 * if(변신아이디!=0){ L1PolyMorph.doPoly(pc, 변신아이디, 600,
			 * L1PolyMorph.MORPH_BY_LOGIN); }
			 */

		} else {
			pc.sendPackets(new S_SystemMessage("사용 중인 인형은 변경 할 수 없습니다."), true);
		}

	}

	private int 드래곤큐브(L1PcInstance pc) {
		// TODO 자동 생성된 메소드 스텁
		int rnd = _random.nextInt(12000) + 1;
		int itemid = 0;
		if (rnd <= 10)
			itemid = 60186; // 축용심 0.1
		else if (rnd <= 11)
			itemid = 60187; // 축 안타숨결 0.01
		else if (rnd <= 12)
			itemid = 60188; // 축 파푸숨결
		else if (rnd <= 13)
			itemid = 60189; // 축 린드숨결
		else if (rnd <= 113)
			itemid = 40341; // 안타 비늘 1
		else if (rnd <= 213)
			itemid = 40349; // 발라 비늘
		else if (rnd <= 313)
			itemid = 40357; // 파푸 비늘
		else if (rnd <= 413)
			itemid = 40365; // 린드 비늘
		else if (rnd <= 12000)
			itemid = 437010; // 드다

		return itemid;
	}

	private static final int[] 싸이큐브리스트 = { 40491, 40498, 40651, 40643, 40645,
			40618, 40074, 140074, 40087, 140087, 40488, 40467, 40440, 40068,
			40031, 40014 };

	private int 싸이큐브(L1PcInstance pc) {
		// TODO 자동 생성된 메소드 스텁
		int itemid = 0;
		int rnd = _random.nextInt(100000) + 1;
		if (rnd >= 100 && rnd <= 150) // 훔결
			itemid = 60187;
		else if (rnd >= 150 && rnd <= 200)
			itemid = 60188;
		else if (rnd <= 22)
			itemid = 40513;
		else if (rnd <= 32)
			itemid = 40393;
		else if (rnd <= 42)
			itemid = 40394;
		else if (rnd <= 52)
			itemid = 40395;
		else if (rnd <= 62)
			itemid = 40396;
		else
			itemid = 싸이큐브리스트[_random.nextInt(싸이큐브리스트.length)];

		return itemid;
	}

	/*
	 * 드래곤의 자수정 상자 95& 드래곤의 토파즈 1% 불완전한 구슬 조각 1% 성장의 구슬 조각 : 몽환의 섬 1% 냉한의 기운 1%
	 * 화염의 기운 1% 82 변신 주문서 1% 85 변신 주문서 1%
	 */
	private int 쵸파의뿔(L1PcInstance pc) {
		// TODO 자동 생성된 메소드 스텁
		int rnd = _random.nextInt(1000) + 1;
		int itemid = 0;
		if (rnd <= 20) {
			itemid = 7241; // 토파즈
		} else if (rnd <= 40) {// 불완전한구슬조각
			itemid = 6016;
		} else if (rnd <= 60) {// 성장의구슬조각몽환의섬
			itemid = 60499;
		} else if (rnd <= 80) {// 냉한의기운
			itemid = 7337;
		} else if (rnd <= 100) {// 화염의기운
			itemid = 6022;
		} else if (rnd <= 120) {// 82변신 주문서
			itemid = 8000;
		} else if (rnd <= 140) {// 85변신 주문서
			itemid = 8001;
		} else {
			itemid = 60257;
		}
		return itemid;
	}

	private int _세뱃돈[] = { 41248, 41250, 430000, 430002, 430004 };
	
	private int _군터의상자[] = { 60510,50022,500209,75025,75024,140074,140087,22110,22113,22112,22111,37000 };
	private int _군터의백금상자[] = { 20117,6003,6005,6004,21258,21260,21268,21093,420009,420008,21267,130220,21122,20017,22009,20079,20107,8204,8205,8206,8207,8208,8209,8210,8211,8212 };
	private int _군터의황금상자[] = { 9105,9106,59,189,205,190,293,84,76,90083,119,124,291,6001,6101,6000,90084,7225,7227,54,58,8104,8122,8189,8158 };
	private int _시원한고대무기[] = { 59,293,90083,291,6101,90084,7227,54,58};
	private int _시원한고대방어구[] = { 20117,21093,130220,21122,20017,22009,20079,20107,8104,8122,8189,8158,8204,8205,8206,8207,8208,8209,8210,8211,8212 };
	
	private int _산타의양말[] = { 3402,60204,75008,21288,6670,60201,40280,40281,40282,40283,40284,40285,40286,40287,40288 };
	private int _산타의황금양말[] = { 84,189,164,205,190,54,58,6001,6000,76,9106,121,119,124 };
	
	private void 산타의양말(L1PcInstance pc, int itemId) {
		// TODO 자동 생성된 메소드 스텁
		int itemid = 0;
		int count = 1;
		
		itemid = _산타의양말[_random.nextInt(_산타의양말.length)];
		
		if(itemid == 3402)
			count = 10;
		if(itemid == 6670)
			count = 30;
		
		L1ItemInstance gosu = pc.getInventory().storeItem(itemid, count);
		pc.sendPackets(new S_ServerMessage(403, gosu.getItem().getName() + " (" + count + ")"), true);
	}
	
	private void 산타의황금양말(L1PcInstance pc, int itemId) {
		// TODO 자동 생성된 메소드 스텁
		int rnd = _random.nextInt(100) + 1;
		int itemid = 0;
		int itemid1 = 0;
		int count = 1;
		int count1 = 1;
		itemid = _산타의양말[_random.nextInt(_산타의양말.length)];
		
		if(itemid == 3402)
			count = 10;
		if(itemid == 6670)
			count = 30;
		
		L1ItemInstance gosu = pc.getInventory().storeItem(itemid, count);
		pc.sendPackets(new S_ServerMessage(403, gosu.getItem().getName() + " (" + count + ")"), true);
		if (rnd <= 20) {
			itemid1 = _산타의황금양말[_random.nextInt(_산타의황금양말.length)];
			L1ItemInstance gosu1 = pc.getInventory().storeItem(itemid1, count1);
			pc.sendPackets(new S_ServerMessage(403, gosu1.getItem().getName() + " (" + count1 + ")"), true);
			L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE,"어느 아덴 용사가 산타의 황금양말에서 " + gosu1.getItem().getName() + " 를(을) 획득하였습니다."));
		} 
	}
	
	private void 시원한고대무기상자(L1PcInstance pc, int itemId) {
		// TODO 자동 생성된 메소드 스텁
		int rnd = _random.nextInt(100) + 1;
		int itemid = 0;
		int count = 1;
		int exp = 325000;
		int settingEXP = (int) Config.RATE_XP;
		double exppenalty = ExpTable.getPenaltyRate(pc.getLevel());
		double PobyExp =  exp * settingEXP * exppenalty;
		pc.addExp((int) PobyExp * 900);
		try {	
			pc.save();	
		} catch (Exception e) {
			
		}
		if (rnd <= 30) {// 1단계인형
			itemid = _시원한고대무기[_random.nextInt(_시원한고대무기.length)];
			L1ItemInstance gosu = pc.getInventory().storeItem(itemid, count);
			pc.sendPackets(new S_ServerMessage(403, gosu.getItem().getName() + " (" + count + ")"), true);
		} 
	}
	
	private void 시원한고대방어구상자(L1PcInstance pc, int itemId) {
		// TODO 자동 생성된 메소드 스텁
		int rnd = _random.nextInt(100) + 1;
		int itemid = 0;
		int count = 1;
		int exp = 325000;
		int settingEXP = (int) Config.RATE_XP;
		double exppenalty = ExpTable.getPenaltyRate(pc.getLevel());
		double PobyExp =  exp * settingEXP * exppenalty;
		pc.addExp((int) PobyExp * 900);
		try {	
			pc.save();	
		} catch (Exception e) {
			
		}
		if (rnd <= 30) {// 1단계인형
			itemid = _시원한고대방어구[_random.nextInt(_시원한고대방어구.length)];
			L1ItemInstance gosu = pc.getInventory().storeItem(itemid, count);
			pc.sendPackets(new S_ServerMessage(403, gosu.getItem().getName() + " (" + count + ")"), true);
		} 
	}
	
	private void 군터의상자(L1PcInstance pc, int itemId) {
		// TODO 자동 생성된 메소드 스텁
		int rnd = _random.nextInt(100) + 1;
		int itemid = 0;
		int count = 1;
		int exp = 325000;
		int settingEXP = (int) Config.RATE_XP;
		double exppenalty = ExpTable.getPenaltyRate(pc.getLevel());
		double PobyExp =  exp * settingEXP * exppenalty;
		pc.addExp((int) PobyExp * 100);
		try {	
			pc.save();	
		} catch (Exception e) {
			
		}
		if (rnd <= 30) {// 1단계인형
			itemid = _군터의상자[_random.nextInt(_군터의상자.length)];
			if(itemid == 500209)
				count = 35;
			L1ItemInstance gosu = pc.getInventory().storeItem(itemid, count);
			pc.sendPackets(new S_ServerMessage(403, gosu.getItem().getName() + " (" + count + ")"), true);
		} 
	}
	
	private void 군터의백금상자(L1PcInstance pc, int itemId) {
		// TODO 자동 생성된 메소드 스텁
		int rnd = _random.nextInt(100) + 1;
		int itemid = 0;
		int count = 1;
		int exp = 325000;
		int settingEXP = (int) Config.RATE_XP;
		double exppenalty = ExpTable.getPenaltyRate(pc.getLevel());
		double PobyExp =  exp * settingEXP * exppenalty;
		pc.addExp((int) PobyExp * 165);
		try {	
			pc.save();	
		} catch (Exception e) {
			
		}
		if (rnd <= 30) {// 1단계인형
			itemid = _군터의백금상자[_random.nextInt(_군터의백금상자.length)];
			L1ItemInstance gosu = pc.getInventory().storeItem(itemid, count);
			pc.sendPackets(new S_ServerMessage(403, gosu.getItem().getName() + " (" + count + ")"), true);
		} 
	}
	private void 군터의황금상자(L1PcInstance pc, int itemId) {
		// TODO 자동 생성된 메소드 스텁
		int rnd = _random.nextInt(100) + 1;
		int itemid = 0;
		int count = 1;
		int exp = 325000;
		int settingEXP = (int) Config.RATE_XP;
		double exppenalty = ExpTable.getPenaltyRate(pc.getLevel());
		double PobyExp =  exp * settingEXP * exppenalty;
		pc.addExp((int) PobyExp * 235);
		try {	
			pc.save();	
		} catch (Exception e) {
			
		}
		if (rnd <= 30) {// 1단계인형
			itemid = _군터의황금상자[_random.nextInt(_군터의황금상자.length)];
			L1ItemInstance gosu = pc.getInventory().storeItem(itemid, count);
			pc.sendPackets(new S_ServerMessage(403, gosu.getItem().getName() + " (" + count + ")"), true);
		} 
	}
	
	private void 세뱃돈(L1PcInstance pc, int itemId) {
		// TODO 자동 생성된 메소드 스텁
		int rnd = _random.nextInt(100) + 1;
		int itemid = 0;
		if (rnd <= 1) {// 호박목걸이
			itemid = 21269;
		} else if (rnd <= 2) {// 10주년 귀걸이
			itemid = 423009;
		} else if (rnd <= 3) {// 10주년 목걸이
			itemid = 423010;
		} else if (rnd <= 4) {// 10주년 기념반지
			itemid = 423011;
		} else if (rnd <= 29) {// 1단계인형
			itemid = _세뱃돈[_random.nextInt(_세뱃돈.length)];
		} else {// 랜덤 코인
			itemid = 60518;
		}
		int count = 1;
		L1ItemInstance gosu = pc.getInventory().storeItem(itemid, count);
		pc.sendPackets(new S_ServerMessage(403, gosu.getItem().getName() + " (" + count + ")"), true);
	}

	private int _오림보물상자[] = { 75006, 60105, 60106, 60107, 60108, 60109, 60110, 60111, 60112, 40308 };

	private void 오림보물상자(L1PcInstance pc, int itemId) {
		// TODO 자동 생성된 메소드 스텁
		int itemid = _오림보물상자[_random.nextInt(_오림보물상자.length)];
		int count = 1;
		if (itemid == 40308) {
			count = _random.nextInt(45000) + 5000;
		}
		L1ItemInstance gosu = pc.getInventory().storeItem(itemid, count);
		pc.sendPackets(new S_ServerMessage(403, gosu.getItem().getName() + " (" + count + ")"), true);
	}

	private void 숨계부적(L1ItemInstance useItem, L1PcInstance pc) {
		int locX = ((L1EtcItem) useItem.getItem()).get_locx();
		int locY = ((L1EtcItem) useItem.getItem()).get_locy();
		short mapId = ((L1EtcItem) useItem.getItem()).get_mapid();
		if (locX != 0 && locY != 0) {

			if (pc.getMap().isEscapable() || pc.isGm()) {
				if (pc.Sabutelok()) {
					pc.dx = locX;
					pc.dy = locY;
					pc.dm = mapId;
					pc.dh = pc.getMoveState().getHeading();
					pc.setTelType(7);
					pc.sendPackets(new S_SabuTell(pc), true);
				}
			} else {
				pc.sendPackets(new S_ServerMessage(647), true);
			}
		}
		pc.cancelAbsoluteBarrier();
	}
	private void 기르무기부적(L1ItemInstance useItem, L1PcInstance pc) {
		int locX = ((L1EtcItem) useItem.getItem()).get_locx();
		int locY = ((L1EtcItem) useItem.getItem()).get_locy();
		short mapId = ((L1EtcItem) useItem.getItem()).get_mapid();
		if (locX != 0 && locY != 0) {
			if (!((pc.getMapId() == 4) )) {
				pc.sendPackets(new S_SystemMessage("마을에서만 사용할 수 있습니다."), true);
				return;
			}
			if (pc.getMap().isEscapable() || pc.isGm()) {
				if (pc.Sabutelok()) {
					pc.dx = locX;
					pc.dy = locY;
					pc.dm = mapId;
					pc.dh = pc.getMoveState().getHeading();
					pc.setTelType(7);
					pc.sendPackets(new S_SabuTell(pc), true);
				}
			} else {
				pc.sendPackets(new S_ServerMessage(647), true);
			}
		}
		pc.cancelAbsoluteBarrier();
	}
	private void 기감부적(L1ItemInstance useItem, L1PcInstance pc) {
		int locX = ((L1EtcItem) useItem.getItem()).get_locx();
		int locY = ((L1EtcItem) useItem.getItem()).get_locy();
		short mapId = ((L1EtcItem) useItem.getItem()).get_mapid();
		if (locX != 0 && locY != 0) {
			if (!((pc.getMapId() == 4) )) {
				pc.sendPackets(new S_SystemMessage("마을에서만 사용할 수 있습니다."), true);
				return;
			}
			if (pc.getMap().isEscapable() || pc.isGm()) {
				if (pc.Sabutelok()) {
					pc.dx = locX;
					pc.dy = locY;
					pc.dm = mapId;
					pc.dh = pc.getMoveState().getHeading();
					pc.setTelType(7);
					pc.sendPackets(new S_SabuTell(pc), true);
				}
			} else {
				pc.sendPackets(new S_ServerMessage(647), true);
			}
		}
		pc.cancelAbsoluteBarrier();
	}
	
	private void 보스부적(L1ItemInstance useItem, L1PcInstance pc) {
		int locX = ((L1EtcItem) useItem.getItem()).get_locx();
		int locY = ((L1EtcItem) useItem.getItem()).get_locy();
		short mapId = ((L1EtcItem) useItem.getItem()).get_mapid();
		if (locX != 0 && locY != 0) {
			if (!((pc.getMapId() == 4) )) {
				pc.sendPackets(new S_SystemMessage("마을에서만 사용할 수 있습니다."), true);
				return;
			}
			if (pc.getMap().isEscapable() || pc.isGm()) {
				if (pc.Sabutelok()) {
					pc.dx = locX;
					pc.dy = locY;
					pc.dm = mapId;
					pc.dh = pc.getMoveState().getHeading();
					pc.setTelType(7);
					pc.sendPackets(new S_SabuTell(pc), true);
				}
			} else {
				pc.sendPackets(new S_ServerMessage(647), true);
			}
		}
		pc.cancelAbsoluteBarrier();
	}
	private void 영코부적2(L1ItemInstance useItem, L1PcInstance pc) {
		int locX = ((L1EtcItem) useItem.getItem()).get_locx();
		int locY = ((L1EtcItem) useItem.getItem()).get_locy();
		short mapId = ((L1EtcItem) useItem.getItem()).get_mapid();
		if (locX != 0 && locY != 0) {
			if (!((pc.getMapId() == 4) )) {
				pc.sendPackets(new S_SystemMessage("마을에서만 사용할 수 있습니다."), true);
				return;
			}
			if (pc.getMap().isEscapable() || pc.isGm()) {
				if (pc.Sabutelok()) {
					pc.dx = locX;
					pc.dy = locY;
					pc.dm = mapId;
					pc.dh = pc.getMoveState().getHeading();
					pc.setTelType(7);
					pc.sendPackets(new S_SabuTell(pc), true);
				}
			} else {
				pc.sendPackets(new S_ServerMessage(647), true);
			}
		}
		pc.cancelAbsoluteBarrier();
	}
	private void 지배탑정상(L1ItemInstance useItem, L1PcInstance pc) {
		
		int locX = ((L1EtcItem) useItem.getItem()).get_locx();
		int locY = ((L1EtcItem) useItem.getItem()).get_locy();
		short mapId = ((L1EtcItem) useItem.getItem()).get_mapid();
			if(!Jibae11.getInstance().getJ_B11Start()){
					pc.sendPackets(new S_SystemMessage("지배의탑 정상은 아직 개방되지 않았습니다"), true);
					return  ;
				}
			pc.getInventory().removeItem(useItem, 1);
		if (locX != 0 && locY != 0) {

			if (pc.getMap().isEscapable() || pc.isGm()) {
				if (pc.Sabutelok()) {
					pc.dx = locX;
					pc.dy = locY;
					pc.dm = mapId;
					pc.dh = pc.getMoveState().getHeading();
					pc.setTelType(7);
					pc.sendPackets(new S_SabuTell(pc), true);
				}
			} else {
				pc.sendPackets(new S_ServerMessage(647), true);
			}
		}
		pc.cancelAbsoluteBarrier();
	}
	private void 행복부적(L1ItemInstance useItem, L1PcInstance pc) {
		int locX = ((L1EtcItem) useItem.getItem()).get_locx();
		int locY = ((L1EtcItem) useItem.getItem()).get_locy();
		short mapId = ((L1EtcItem) useItem.getItem()).get_mapid();
		if (locX != 0 && locY != 0) {

			if (pc.getMap().isEscapable() || pc.isGm()) {
				if (pc.Sabutelok()) {
					pc.dx = locX;
					pc.dy = locY;
					pc.dm = mapId;
					pc.dh = pc.getMoveState().getHeading();
					pc.setTelType(7);
					pc.sendPackets(new S_SabuTell(pc), true);
				}
			} else {
				pc.sendPackets(new S_ServerMessage(647), true);
			}
		}
		pc.cancelAbsoluteBarrier();
	}
	private void 영코부적(L1ItemInstance useItem, L1PcInstance pc) {
		int locX = ((L1EtcItem) useItem.getItem()).get_locx();
		int locY = ((L1EtcItem) useItem.getItem()).get_locy();
		short mapId = ((L1EtcItem) useItem.getItem()).get_mapid();
		if (locX != 0 && locY != 0) {
			if (!((pc.getMapId() == 4) )) {
				pc.sendPackets(new S_SystemMessage("마을에서만 사용할 수 있습니다."), true);
				return;
			}
			if (pc.getMap().isEscapable() || pc.isGm()) {
				if (pc.Sabutelok()) {
					pc.dx = locX;
					pc.dy = locY;
					pc.dm = mapId;
					pc.dh = pc.getMoveState().getHeading();
					pc.setTelType(7);
					pc.sendPackets(new S_SabuTell(pc), true);
				}
			} else {
				pc.sendPackets(new S_ServerMessage(647), true);
			}
		}
		pc.cancelAbsoluteBarrier();
	}

	private int 진귀한아이템(L1PcInstance pc) {
		// TODO Auto-generated method stub
		int rnd = _random.nextInt(10000) + 1;
		int itemid = 0;
		if (rnd <= 1) {
			itemid = 30220;// 파괴장궁
		} else if (rnd <= 2) {
			itemid = 20320;// 타벨
		} else if (rnd <= 3) {
			itemid = 30218;// 지혜부츠
		} else if (rnd <= 4) {
			itemid = 30219;// 돌장갑
		} else if (rnd <= 5) {
			itemid = 40223; // 앱솔
		} else if (rnd <= 6) {
			itemid = 157;// 은크
		} else if (rnd <= 7) {
			itemid = 20459;// 붉오귀
		} else if (rnd <= 8) {
			itemid = 20187;// 파글
		} else if (rnd <= 9) {
			itemid = 140074; // 축젤
		} else if (rnd <= 10) {
			itemid = 140087; // 축데이
		} else if (rnd <= 60) {
			itemid = 439017;// 조이수정
		} else if (rnd <= 110) {
			itemid = 439018; // 아바타수정
		} else if (rnd <= 160) {
			itemid = 439016; // 패닉수정
		} else {
			// 꽝
		}
		/**
		 * 꽝 95프로 앱솔,은크,오귀,파글,축젤,축데이,조이수정,아바타수정,패닉수정 0.5프로씩 타벨,지혜부츠,돌장갑 0.1프로
		 * 파괴의장궁 0.01프로
		 */
		return itemid;
	}

	private void 고서(L1PcInstance pc, L1ItemInstance useItem) {
		switch (useItem.getItem().getItemId()) {
		case 60381:
			pc.sendPackets(new S_NPCTalkReturn(pc.getId(), "questbook1"));
			break;
		case 60384:
			pc.sendPackets(new S_NPCTalkReturn(pc.getId(), "questbook2"));
			break;
		case 7257:
			pc.sendPackets(new S_NPCTalkReturn(pc.getId(), "questbook3"));
			break;
		default:
			break;
		}
	}

	private void 룬주머니(L1PcInstance pc, L1ItemInstance useItem) {
		int roonid = 0;
		if (pc.isCrown()) {
			roonid = 0;
		} else if (pc.isKnight()) {
			roonid = 10;
		} else if (pc.isElf()) {
			roonid = 20;
		} else if (pc.isWizard()) {
			roonid = 30;
		} else if (pc.isDarkelf()) {
			roonid = 40;
		} else if (pc.isDragonknight()) {
			roonid = 50;
		} else if (pc.isIllusionist()) {
			roonid = 60;
		} else if (pc.isWarrior()) {
			roonid = 70;
		}
		switch (useItem.getItem().getItemId()) {
		case 7252:
			roonid += 9000;
			break;
		case 7253:
			roonid += 9001;
			break;
		case 7254:
			roonid += 9002;
			break;
		case 7255:
			roonid += 9003;
			break;
		case 7256:
			roonid += 9004;
			break;
		default:
			break;
		}
		L1Quest q = pc.getQuest();
		if (roonid >= 9000) {
			try {
				q.get_step(L1Quest.QUEST_55_Roon);
				q.set_step(L1Quest.QUEST_55_Roon, 1);
				q.get_step(L1Quest.QUEST_70_Roon);
				q.set_step(L1Quest.QUEST_70_Roon, 1);
				L1ItemInstance roon = pc.getInventory().storeItem(roonid, 1);
				L1ItemInstance gosu = pc.getInventory().storeItem(7257, 1);
				pc.sendPackets(new S_ServerMessage(403, roon.getItem().getName()), true);
				pc.sendPackets(new S_ServerMessage(403, gosu.getItem().getName()), true);
				pc.getInventory().removeItem(useItem, 1);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private void 룬주머니80(L1PcInstance pc, L1ItemInstance useItem) {
		int roonid = 0;
		if (pc.isCrown()) {
			roonid = 0;
		} else if (pc.isKnight()) {
			roonid = 10;
		} else if (pc.isElf()) {
			roonid = 20;
		} else if (pc.isWizard()) {
			roonid = 30;
		} else if (pc.isDarkelf()) {
			roonid = 40;
		} else if (pc.isDragonknight()) {
			roonid = 50;
		} else if (pc.isIllusionist()) {
			roonid = 60;
		} else if (pc.isWarrior()) {
			roonid = 70;
		}
		switch (useItem.getItem().getItemId()) {
		case 7352:
			roonid += 9900;
			break;
		case 7353:
			roonid += 9901;
			break;
		case 7354:
			roonid += 9902;
			break;
		case 7355:
			roonid += 9903;
			break;
		case 7356:
			roonid += 9904;
			break;
		default:
			break;
		}
		L1Quest q = pc.getQuest();
		if (roonid >= 9900) {
			try {
				q.get_step(L1Quest.QUEST_55_Roon);
				q.set_step(L1Quest.QUEST_55_Roon, 1);
				q.get_step(L1Quest.QUEST_70_Roon);
				q.set_step(L1Quest.QUEST_70_Roon, 1);
				q.get_step(L1Quest.QUEST_80_Roon);
				q.set_step(L1Quest.QUEST_80_Roon, 1);
				L1ItemInstance roon = pc.getInventory().storeItem(roonid, 1);
				L1ItemInstance gosu = pc.getInventory().storeItem(7357, 1);
				pc.sendPackets(new S_ServerMessage(403, roon.getItem().getName()), true);
				pc.sendPackets(new S_ServerMessage(403, gosu.getItem().getName()), true);
				pc.getInventory().removeItem(useItem, 1);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void 룬주머니85(L1PcInstance pc, L1ItemInstance useItem) {
		int roonid = 0;
		if (pc.isCrown()) {
			roonid = 0;
		} else if (pc.isKnight()) {
			roonid = 10;
		} else if (pc.isElf()) {
			roonid = 20;
		} else if (pc.isWizard()) {
			roonid = 30;
		} else if (pc.isDarkelf()) {
			roonid = 40;
		} else if (pc.isDragonknight()) {
			roonid = 50;
		} else if (pc.isIllusionist()) {
			roonid = 60;
		} else if (pc.isWarrior()) {
			roonid = 70;
		}
		switch (useItem.getItem().getItemId()) {
		case 7452:
			roonid += 9800;
			break;
		case 7453:
			roonid += 9801;
			break;
		case 7454:
			roonid += 9802;
			break;
		case 7455:
			roonid += 9803;
			break;
		case 7456:
			roonid += 9804;
			break;
		default:
			break;
		}
		L1Quest q = pc.getQuest();
		if (roonid >= 9800) {
			try {
				q.get_step(L1Quest.QUEST_55_Roon);
				q.set_step(L1Quest.QUEST_55_Roon, 1);
				q.get_step(L1Quest.QUEST_70_Roon);
				q.set_step(L1Quest.QUEST_70_Roon, 1);
				q.get_step(L1Quest.QUEST_80_Roon);
				q.set_step(L1Quest.QUEST_80_Roon, 1);
				q.get_step(L1Quest.QUEST_85_Roon);
				q.set_step(L1Quest.QUEST_85_Roon, 1);
				L1ItemInstance roon = pc.getInventory().storeItem(roonid, 1);
				L1ItemInstance gosu = pc.getInventory().storeItem(7457, 1);
				pc.sendPackets(new S_ServerMessage(403, roon.getItem().getName()), true);
				pc.sendPackets(new S_ServerMessage(403, gosu.getItem().getName()), true);
				pc.getInventory().removeItem(useItem, 1);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	private void 룬주머니90(L1PcInstance pc, L1ItemInstance useItem) {
		int roonid = 0;
		if (pc.isCrown()) {
			roonid = 0;
		} else if (pc.isKnight()) {
			roonid = 10;
		} else if (pc.isElf()) {
			roonid = 20;
		} else if (pc.isWizard()) {
			roonid = 30;
		} else if (pc.isDarkelf()) {
			roonid = 40;
		} else if (pc.isDragonknight()) {
			roonid = 50;
		} else if (pc.isIllusionist()) {
			roonid = 60;
		} else if (pc.isWarrior()) {
			roonid = 70;
		}
		switch (useItem.getItem().getItemId()) {
		case 7552:
			roonid += 9700;
			break;
		case 7553:
			roonid += 9701;
			break;
		case 7554:
			roonid += 9702;
			break;
		case 7555:
			roonid += 9703;
			break;
		case 7556:
			roonid += 9704;
			break;
		default:
			break;
		}
		L1Quest q = pc.getQuest();
		if (roonid >= 9700) {
			try {
				q.get_step(L1Quest.QUEST_55_Roon);
				q.set_step(L1Quest.QUEST_55_Roon, 1);
				q.get_step(L1Quest.QUEST_70_Roon);
				q.set_step(L1Quest.QUEST_70_Roon, 1);
				q.get_step(L1Quest.QUEST_80_Roon);
				q.set_step(L1Quest.QUEST_80_Roon, 1);
				q.get_step(L1Quest.QUEST_85_Roon);
				q.set_step(L1Quest.QUEST_85_Roon, 1);
				q.get_step(L1Quest.QUEST_90_Roon);
				q.set_step(L1Quest.QUEST_90_Roon, 1);
				L1ItemInstance roon = pc.getInventory().storeItem(roonid, 1);
				L1ItemInstance gosu = pc.getInventory().storeItem(7557, 1);
				pc.sendPackets(new S_ServerMessage(403, roon.getItem().getName()), true);
				pc.sendPackets(new S_ServerMessage(403, gosu.getItem().getName()), true);
				pc.getInventory().removeItem(useItem, 1);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private void 오림상자5(L1PcInstance pc, L1ItemInstance useItem) {
		pc.getInventory().storeItem(7249, 5);
		pc.sendPackets(new S_ServerMessage(403, "오림의 장신구 마법 주문서 상자 (5)"), true);
		pc.getInventory().removeItem(useItem, 1);
	}

	private void 오림상자(L1PcInstance pc, L1ItemInstance useItem) {
		int chan = _random.nextInt(100) + 1;
		if (chan < 10) {
			pc.getInventory().storeItem(7324, 1);
		} else {
			pc.getInventory().storeItem(7323, 1);
		}
		pc.sendPackets(new S_ServerMessage(403, "오림의 장신구 마법 주문서"), true);
		pc.getInventory().removeItem(useItem, 1);
	}

	private void 혼돈부적(L1PcInstance pc, L1ItemInstance useItem, int itemId) {
		pc.getInventory().removeItem(useItem, 1);
		int chance = _random.nextInt(100);
		if (chance <= 70) {
			L1ItemInstance item = pc.getInventory().storeItem(itemId + 1010, 1);
			if (item != null) {
				pc.sendPackets(new S_ServerMessage(403, item.getLogName()),
						true);
			}
		} else {
			L1ItemInstance item = null;
			if (itemId == 5000110) {
				item = pc.getInventory().storeItem(60202, 1);
			} else {
				item = pc.getInventory().storeItem(itemId - 4959822, 1);
			}
			if (item != null) {
				pc.sendPackets(new S_ServerMessage(403, item.getLogName()),
						true);
			}
		}
	}

	private void 강화이벤트주문서(L1PcInstance pc, L1ItemInstance 강화돌, L1ItemInstance 상자) {
		int rnd = _random.nextInt(100) + 1;
		if (상자.getItem().getItemId() == 600240) {
			if (상자.getEnchantLevel() >= 3) {
				pc.sendPackets(new S_SystemMessage("더이상 인챈트가 불가능 합니다."));
				return;
			}
			if (rnd < 10) {// 성공
				int newEnchantLvl = 상자.getEnchantLevel() + 1;
				상자.setEnchantLevel(newEnchantLvl);

				pc.getInventory().updateItem(상자, L1PcInventory.COL_ENCHANTLVL);
				pc.sendPackets(new S_PacketBox(상자, S_PacketBox.인챈변경));
				pc.saveInventory();
				pc.sendPackets(new S_SystemMessage("상자 인첸트 : 인첸트 성공."));
			} else if (rnd < 64) {// 가만히
				pc.sendPackets(new S_SystemMessage("상자 인첸트 : 인첸트 실패(강화수치 보존)"));
			} else {// 하락
				if (상자.getEnchantLevel() == 0) {
					pc.sendPackets(new S_SystemMessage(
							"상자 인첸트 : 인첸트 실패(강화수치 보존)"));
					// pc.sendPackets(new
					// S_SystemMessage("하락해야 하지만 더이상 하락할수없어 리턴됨."));
					pc.getInventory().removeItem(강화돌, 1);
					return;
				}

				int newEnchantLvl = 상자.getEnchantLevel() - 1;
				상자.setEnchantLevel(newEnchantLvl);

				pc.getInventory().updateItem(상자, L1PcInventory.COL_ENCHANTLVL);
				pc.sendPackets(new S_PacketBox(상자, S_PacketBox.인챈변경));
				pc.saveInventory();
				pc.sendPackets(new S_SystemMessage("상자 인첸트 : 인첸트 실패(강화수치 하락)"));
			}

			pc.getInventory().removeItem(강화돌, 1);
		} else {
			pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true); // \f1
																			// 아무것도
																			// 일어나지
																			// 않았습니다.
		}
	}

	private void UseHeallingPotion(L1PcInstance pc, int healHp, int gfxid) {
		if (pc.getSkillEffectTimerSet().hasSkillEffect(71) == true) { // 디케이포션
																		// 상태
			pc.sendPackets(new S_ServerMessage(698), true); // 마력에 의해 아무것도 마실 수가
															// 없습니다.
			return;
		}

		// 앱솔루트베리어의 해제
		pc.cancelAbsoluteBarrier();

		pc.sendPackets(new S_SkillSound(pc.getId(), gfxid), true);
		Broadcaster.broadcastPacket(pc, new S_SkillSound(pc.getId(), gfxid),
				true);
		// pc.sendPackets(new S_ServerMessage(77)); // \f1기분이 좋아졌습니다.
		healHp *= (_random.nextGaussian() / 5.0D) + 1.0D;

		if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.데스페라도)) {
			int atklv = pc.데스페라도공격자레벨;
			int dflv = pc.getLevel();
			double 저하률 = 0.65;
			if (atklv > dflv) {
				저하률 += (atklv - dflv) * 0.05;
			}
			if (저하률 > 0.9) {
				저하률 = 0.9;
			}
			healHp -= (int) ((double) healHp * 저하률);
		} else if (pc.getSkillEffectTimerSet().hasSkillEffect(POLLUTE_WATER)
				|| pc.getSkillEffectTimerSet().hasSkillEffect(10517)) { // 포르트워타중은
																		// 회복량1/2배
			healHp /= 2;
		}

		pc.setCurrentHp(pc.getCurrentHp() + healHp);
	}

	private void 진주포션사용(L1PcInstance pc, int itemid) {
		if (pc.getSkillEffectTimerSet().hasSkillEffect(71) == true) { // 디케이포션
			// 상태
            pc.sendPackets(new S_ServerMessage(698), true);
            return;
        }
        if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.STATUS_DRAGONPERL)) {
            pc.getSkillEffectTimerSet().killSkillEffectTimer(L1SkillId.STATUS_DRAGONPERL);          
            pc.sendPackets(new S_DRAGONPERL(pc.getId(), 0));
            pc.sendPackets(new S_PacketBox(S_PacketBox.DRAGONPERL, 0, 0));
            pc.set진주속도(0);
        }
        pc.cancelAbsoluteBarrier();// 앱솔해제(팩에 이 메소드없으면 무시)
        int time = 600;
        pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.STATUS_DRAGONPERL, time * 1000);
        pc.sendPackets(new S_SkillSound(pc.getId(), 7976));
        pc.broadcastPacket(new S_SkillSound(pc.getId(), 7976));
      //  pc.sendPackets(new S_SkillSound(pc.getId(), 197));// 말갱이 이팩트...
       // pc.broadcastPacket(new S_SkillSound(pc.getId(), 197));
        pc.sendPackets(new S_DRAGONPERL(pc.getId(), 8));
        pc.broadcastPacket(new S_DRAGONPERL(pc.getId(), 8));
        pc.sendPackets(new S_PacketBox(S_PacketBox.DRAGONPERL, 8, time));
        pc.set진주속도(1);
	}

	private void 용액(L1PcInstance pc, L1ItemInstance useItem,
			L1ItemInstance ddItem) {
		int itemId2 = ddItem.getItem().getItemId();
		if (itemId2 == L1ItemId.DRAGON_DIAMOND
				|| itemId2 == L1ItemId.DRAGON_EME) {
			pc.getInventory().removeItem(ddItem, 1);
			pc.getInventory().removeItem(useItem, 1);
			pc.getInventory().storeItem(7241, 2);
			pc.sendPackets(new S_ServerMessage(403, "드래곤의 토파즈" + "(2)"), true);
		} else {
			pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true);
		}

	}

	private void 햄의주머니(L1PcInstance pc, L1ItemInstance useItem) {
		Timestamp lastUsed = useItem.getLastUsed();
		if (lastUsed == null
				|| currentDate.getTimeInMillis() > lastUsed.getTime()
						+ (1000 * 60 * 60 * 22)) {// 22시간
			pc.getInventory().storeItem(7337, 1);
			pc.sendPackets(new S_ServerMessage(403, "냉한의 기운"), true);
			useItem.setChargeCount(useItem.getChargeCount() - 1);
			pc.getInventory().updateItem(useItem,
					L1PcInventory.COL_CHARGE_COUNT);
			if (useItem.getChargeCount() == 0) {
				pc.getInventory().removeItem(useItem);
			}
			useItem.setLastUsed(new Timestamp(currentDate.getTimeInMillis()));
		} else {
			long i = (lastUsed.getTime() + (1000 * 60 * 60 * 22))
					- currentDate.getTimeInMillis();
			Calendar cal = (Calendar) currentDate.clone();
			cal.setTimeInMillis(cal.getTimeInMillis() + i);
			pc.sendPackets(new S_SystemMessage(i / 60000 + "분 동안("
					+ cal.getTime().getHours() + ":"
					+ cal.getTime().getMinutes() + " 까지)은 사용할 수 없습니다."), true);
		}
		/** 시공의 항아리 **/
	}

	private void 자수정(L1PcInstance pc, L1ItemInstance item) {
		if (pc.getAinHasad() > 10000) {
			if (pc.getSkillEffectTimerSet().hasSkillEffect(
					L1SkillId.DRAGON_EME_2) == true) {
				pc.sendPackets(new S_ServerMessage(2147), true);
				return;
			}
			if (pc.getSkillEffectTimerSet().hasSkillEffect(
					L1SkillId.DRAGON_TOPAZ)) {
				pc.getSkillEffectTimerSet().removeSkillEffect(
						L1SkillId.DRAGON_TOPAZ);
			}
			long sysTime = System.currentTimeMillis();

			Timestamp deleteTime = null;
			deleteTime = new Timestamp(sysTime + 1800000);
			pc.setPUPLETime(deleteTime);
			if (pc.getTOPAZTime() != null) {
				pc.setTOPAZTime(null);
			}
			/*
			 * if(!pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.DRAGON_PUPLE
			 * )){ pc.calAinHasad(1000000); pc.sendPackets(new
			 * S_ACTION_UI(S_ACTION_UI.EINHASAD, pc), true); }
			 */
			pc.getSkillEffectTimerSet().setSkillEffect(DRAGON_PUPLE,
					1800 * 1000);
			pc.sendPackets(new S_PacketBox(1800, 1, true, true), true);
			pc.sendPackets(new S_SkillSound(pc.getId(), 197), true);
			Broadcaster.broadcastPacket(pc, new S_SkillSound(pc.getId(), 197),
					true);

			try {
				pc.save();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			pc.getInventory().removeItem(item, 1);
		} else {
			pc.sendPackets(new S_SystemMessage("축복지수가 있어야 사용하실수 있습니다."), true);
		}
	}

	public void tamupdate(int objectId, Timestamp date) {
		Connection con = null;
		PreparedStatement pstm = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con
					.prepareStatement("UPDATE characters SET TamEndTime=? WHERE objid=?");
			pstm.setTimestamp(1, date);
			pstm.setInt(2, objectId);
			pstm.executeUpdate();
		} catch (SQLException e) {
			// _log.log(Level.SEVERE, e.getLocalizedMessage(), e);
		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	public void tamadd(String _name, int objectId, int _day, String _encobjid) {
		Connection con = null;
		PreparedStatement pstm = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con
					.prepareStatement("INSERT INTO Tam SET objid=?, Name=?, Day=? , encobjid=?");
			pstm.setInt(1, objectId);
			pstm.setString(2, _name);
			pstm.setInt(3, _day);
			pstm.setString(4, _encobjid);
			pstm.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			// _log.log(Level.SEVERE, e.getLocalizedMessage(), e);
		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	private void 탐열매(L1PcInstance pc, int _objid, L1ItemInstance item, int day) {
		try {
			Timestamp tamtime = null;
			long time = 0;
			long sysTime = System.currentTimeMillis();
			String _Name = null;
			int tamcount = pc.tamcount();

			Connection con = null;
			PreparedStatement pstm = null;
			ResultSet rs = null;
			try {
				con = L1DatabaseFactory.getInstance().getConnection();
				pstm = con.prepareStatement("SELECT TamEndTime, char_name FROM characters WHERE objid=?");
				pstm.setInt(1, _objid);
				rs = pstm.executeQuery();
				while (rs.next()) {
					_Name = rs.getString("char_name");
					tamtime = rs.getTimestamp("TamEndTime");
					if (tamtime != null) {
						if (sysTime < tamtime.getTime()) {
							time = tamtime.getTime() - sysTime;
						}
					}
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				SQLUtil.close(rs);
				SQLUtil.close(pstm);
				SQLUtil.close(con);
			}
			if (_Name == null) {
				pc.sendPackets(new S_SystemMessage("비정상 접근입니다. 다시 시도해주세요."), true);
				return;
			}
			if (time != 0) {
				tamadd(_Name, _objid, day, byteWrite(_objid));
				pc.sendPackets(new S_NewCreateItem(pc.getAccountName(), 0xcd));
				pc.sendPackets(new S_SystemMessage(pc, "[" + _Name + "] 에 이미 이용중인 상품이 있어 예약 되었습니다."), true);
				pc.getInventory().removeItem(item, 1);
				return;
			} else if (tamcount >= 5) {// 여기에서 계정당 3개먹었는지 체크하면될듯
				pc.sendPackets(new S_SystemMessage(pc, "성장의 고리는 5개의 캐릭터에만 사용 가능합니다."), true);
				return;
			}
			Timestamp deleteTime = null;
			deleteTime = new Timestamp(sysTime + (86400000 * (long) day) + 10000);// 7일

			if (pc.getId() == _objid) {
				pc.setTamTime(deleteTime);
				try {
					pc.save();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				tamupdate(_objid, deleteTime);
			}

			pc.sendPackets(new S_NewCreateItem(pc.getAccountName(), 0xcd));
			int aftertamcount = pc.tamcount();
			int aftertamtime = (int) pc.TamTime();
			// System.out.println(aftertamtime);
			// long aaa = pc.TamTime();
			// System.out.println((int)aaa);

			if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.Tam_Fruit1)) {
				pc.getSkillEffectTimerSet().killSkillEffectTimer(L1SkillId.Tam_Fruit1);
				pc.getAC().addAc(1);
			} else if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.Tam_Fruit2)) {
				pc.getSkillEffectTimerSet().killSkillEffectTimer(L1SkillId.Tam_Fruit2);
				pc.getAC().addAc(2);
			} else if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.Tam_Fruit3)) {
				pc.getSkillEffectTimerSet().killSkillEffectTimer(L1SkillId.Tam_Fruit3);
				pc.getAC().addAc(3);
				pc.addDamageReductionByArmor(-2);
			} else if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.Tam_Fruit4)) {
				pc.getSkillEffectTimerSet().killSkillEffectTimer(L1SkillId.Tam_Fruit4);
				pc.getAC().addAc(4);
				pc.addDamageReductionByArmor(-2);
			} else if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.Tam_Fruit5)) {
				pc.getSkillEffectTimerSet().killSkillEffectTimer(L1SkillId.Tam_Fruit5);
				pc.getAC().addAc(5);
				pc.addDamageReductionByArmor(-2);
			} else {
			}

			if (aftertamtime < 0) {
				aftertamtime = 0;
			}

			if (aftertamcount == 1) {
				pc.getAC().addAc(-1);
				pc.getSkillEffectTimerSet().setSkillEffect(Tam_Fruit1, aftertamtime);
			} else if (aftertamcount == 2) {
				pc.getAC().addAc(-2);
				pc.getSkillEffectTimerSet().setSkillEffect(Tam_Fruit2, aftertamtime);
			} else if (aftertamcount == 3) {
				pc.getAC().addAc(-3);
				pc.addDamageReductionByArmor(2);
				pc.getSkillEffectTimerSet().setSkillEffect(Tam_Fruit3, aftertamtime);
			} else if (aftertamcount == 4) {
				pc.getAC().addAc(-4);
				pc.addDamageReductionByArmor(2);
				pc.getSkillEffectTimerSet().setSkillEffect(Tam_Fruit4, aftertamtime);
			} else if (aftertamcount == 5) {
				pc.getAC().addAc(-5);
				pc.addDamageReductionByArmor(2);
				pc.getSkillEffectTimerSet().setSkillEffect(Tam_Fruit5, aftertamtime);
			}

			// pc.getAC().addAc(-1*pc.tamcount());
			pc.sendPackets(new S_OwnCharStatus(pc));
			pc.sendPackets(new S_NewCreateItem(S_NewCreateItem.버프창, pc.TamTime(), aftertamcount, true), true);
			pc.sendPackets(new S_ServerMessage(3916));
			pc.sendPackets(new S_SkillSound(pc.getId(), 2028), true);
			Broadcaster.broadcastPacket(pc, new S_SkillSound(pc.getId(), 2028), true);

			//

			pc.getInventory().removeItem(item, 1);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static final int[] hextable = { 0x80, 0x81, 0x82, 0x83, 0x84, 0x85,
			0x86, 0x87, 0x88, 0x89, 0x8a, 0x8b, 0x8c, 0x8d, 0x8e, 0x8f, 0x90,
			0x91, 0x92, 0x93, 0x94, 0x95, 0x96, 0x97, 0x98, 0x99, 0x9a, 0x9b,
			0x9c, 0x9d, 0x9e, 0x9f, 0xa0, 0xa1, 0xa2, 0xa3, 0xa4, 0xa5, 0xa6,
			0xa7, 0xa8, 0xa9, 0xaa, 0xab, 0xac, 0xad, 0xae, 0xaf, 0xb0, 0xb1,
			0xb2, 0xb3, 0xb4, 0xb5, 0xb6, 0xb7, 0xb8, 0xb9, 0xba, 0xbb, 0xbc,
			0xbd, 0xbe, 0xbf, 0xc0, 0xc1, 0xc2, 0xc3, 0xc4, 0xc5, 0xc6, 0xc7,
			0xc8, 0xc9, 0xca, 0xcb, 0xcc, 0xcd, 0xce, 0xcf, 0xd0, 0xd1, 0xd2,
			0xd3, 0xd4, 0xd5, 0xd6, 0xd7, 0xd8, 0xd9, 0xda, 0xdb, 0xdc, 0xdd,
			0xde, 0xdf, 0xe0, 0xe1, 0xe2, 0xe3, 0xe4, 0xe5, 0xe6, 0xe7, 0xe8,
			0xe9, 0xea, 0xeb, 0xec, 0xed, 0xee, 0xef, 0xf0, 0xf1, 0xf2, 0xf3,
			0xf4, 0xf5, 0xf6, 0xf7, 0xf8, 0xf9, 0xfa, 0xfb, 0xfc, 0xfd, 0xfe,
			0xff };

	private String byteWrite(long value) {
		long temp = value / 128;
		StringBuffer sb = new StringBuffer();
		if (temp > 0) {
			sb.append((byte) hextable[(int) value % 128]);
			while (temp >= 128) {
				sb.append((byte) hextable[(int) temp % 128]);
				temp = temp / 128;
			}
			if (temp > 0)
				sb.append((int) temp);
		} else {
			if (value == 0) {
				sb.append(0);
			} else {
				sb.append((byte) hextable[(int) value]);
				sb.append(0);
			}
		}
		return sb.toString();
	}
	private boolean createNewItem4(L1PcInstance pc, int item_id, int count, int EnchantLevel) {
		return createNewItem4(pc, item_id, count, EnchantLevel, 1, 0);
	}
	
	private boolean createNewItem4(L1PcInstance pc, int item_id, int count, int EnchantLevel, int bless, int SpiritIn) {
		
		L1ItemInstance item = ItemTable.getInstance().createItem(item_id);

			item.setCount(count);
			item.setEnchantLevel(EnchantLevel);
			item.setIdentified(true);
			item.setRegistLevel(SpiritIn);
			item.setLock(1);
			if (item != null) {
				if (pc.getInventory().checkAddItem(item, count) == L1Inventory.OK) {
					pc.getInventory().storeItem(item);
					item.setBless(bless);
					pc.getInventory().updateItem(item, L1PcInventory.COL_BLESS);
					pc.getInventory().saveItem(item, L1PcInventory.COL_BLESS);
				} else { // 가질 수 없는 경우는 지면에 떨어뜨리는 처리의 캔슬은 하지 않는다(부정 방지)
					L1World.getInstance().getInventory(pc.getX(), pc.getY(), pc.getMapId()).storeItem(item);
				}
			
			pc.sendPackets(new S_ServerMessage(403, item.getLogName()), true); // %0를 손에 넣었습니다.
			return true;
		} else {
			return false;
		}
	}
	
	private boolean createNewItem3(L1PcInstance pc, int item_id, int count, int EnchantLevel) {
		return createNewItem3(pc, item_id, count, EnchantLevel, 1, 0);
	}
	
	private boolean createNewItem3(L1PcInstance pc, int item_id, int count, int EnchantLevel, int bless, int SpiritIn) {
		
		L1ItemInstance item = ItemTable.getInstance().createItem(item_id);

			item.setCount(count);
			item.setEnchantLevel(EnchantLevel);
			item.setIdentified(true);
			item.setRegistLevel(SpiritIn);
			if (item != null) {
				if (pc.getInventory().checkAddItem(item, count) == L1Inventory.OK) {
					pc.getInventory().storeItem(item);
					item.setBless(bless);
					pc.getInventory().updateItem(item, L1PcInventory.COL_BLESS);
					pc.getInventory().saveItem(item, L1PcInventory.COL_BLESS);
				} else { // 가질 수 없는 경우는 지면에 떨어뜨리는 처리의 캔슬은 하지 않는다(부정 방지)
					L1World.getInstance().getInventory(pc.getX(), pc.getY(), pc.getMapId()).storeItem(item);
				}
			
			pc.sendPackets(new S_ServerMessage(403, item.getLogName()), true); // %0를 손에 넣었습니다.
			return true;
		} else {
			return false;
		}
	}

	private void 토파즈(L1PcInstance pc, L1ItemInstance item) {
		if (pc.getAinHasad() > 10000) {
			if (pc.getSkillEffectTimerSet().hasSkillEffect(
					L1SkillId.DRAGON_EME_2) == true) {
				pc.sendPackets(new S_ServerMessage(2147), true);
				return;
			}
			if (pc.getSkillEffectTimerSet().hasSkillEffect(
					L1SkillId.DRAGON_PUPLE)) {
				pc.getSkillEffectTimerSet().removeSkillEffect(
						L1SkillId.DRAGON_PUPLE);
			}
			long sysTime = System.currentTimeMillis();
			Timestamp deleteTime = null;
			deleteTime = new Timestamp(sysTime + 1800000);
			pc.setTOPAZTime(deleteTime);

			if (pc.getPUPLETime() != null) {
				pc.setPUPLETime(null);
			}
			/*
			 * if(!pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.DRAGON_TOPAZ
			 * )){ pc.calAinHasad(1000000); pc.sendPackets(new
			 * S_ACTION_UI(S_ACTION_UI.EINHASAD, pc), true); }
			 */

			pc.getSkillEffectTimerSet().setSkillEffect(DRAGON_TOPAZ,
					1800 * 1000);
			pc.sendPackets(new S_PacketBox(1800, 2, true, true), true);
			pc.sendPackets(new S_SkillSound(pc.getId(), 197), true);
			Broadcaster.broadcastPacket(pc, new S_SkillSound(pc.getId(), 197),
					true);

			try {
				pc.save();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			pc.getInventory().removeItem(item, 1);
		} else {
			pc.sendPackets(new S_SystemMessage("축복지수가 있어야 사용하실수 있습니다."), true);
		}
	}

	private void 드래곤보석(L1PcInstance pc, L1ItemInstance item) {
		int hasad = 0;
		int skill = 0;
		int packet = 0;
		int msg = 0;
		int itemId = item.getItem().getItemId();
		int effect = 197;
		if (itemId == 1437010) {
			hasad = 6000000;
			int temphasad = pc.getAinHasad() + hasad;
			if (temphasad > 44000000) {
				pc.sendPackets(new S_SystemMessage("아인하사드의 축복: 남아있는 축복지수가 많아 사용 할 수 없습니다."));
				return;
			}

			Timestamp deleteTime = null;
			deleteTime = new Timestamp(System.currentTimeMillis() + 3600000 * 3);// 86400000);//
			pc.setDETime2(deleteTime);

			skill = 7785;
			packet = 0x01;
			msg = 2142;
			effect = 198;
			if(pc.getAinHasad() < 10000){
				pc.setAinHasad(hasad);
			}else{
				pc.calAinHasad(hasad);
			}
			pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.EINHASAD, pc), true);
			// 10800), true);
			pc.sendPackets(new S_ServerMessage(msg), true);
			pc.sendPackets(new S_SkillSound(pc.getId(), effect), true);
			Broadcaster.broadcastPacket(pc, new S_SkillSound(pc.getId(), effect), true);
		} else if (itemId == 3402) {
			hasad = 16000000;
			int temphasad = pc.getAinHasad();
			if (temphasad > 34000000) {
				pc.sendPackets(new S_SystemMessage(
						"아인하사드의 축복: 남아있는 축복지수가 많아 사용 할 수 없습니다."));
				return;
			}

			Timestamp deleteTime = null;
			deleteTime = new Timestamp(System.currentTimeMillis() + 3600000 * 3);// 86400000);//
																					// 3일
			pc.setDETime2(deleteTime);

			skill = 7785;
			packet = 0x01;
			msg = 2142;

			if(pc.getAinHasad() < 10000){
				pc.setAinHasad(hasad);
			}else{
				pc.calAinHasad(hasad);
			}
			pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.EINHASAD, pc), true);
			pc.sendPackets(new S_ServerMessage(msg), true);
			pc.sendPackets(new S_SkillSound(pc.getId(), effect), true);
			Broadcaster.broadcastPacket(pc, new S_SkillSound(pc.getId(), effect), true);

		} else if (itemId == L1ItemId.DRAGON_DIAMOND || itemId == 60291) {
			/*
			 * if
			 * (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.DRAGON_EME_2
			 * ) == true) { pc.sendPackets(new S_ServerMessage(2146), true);
			 * return; }
			 */
			hasad = 1000000;
			int temphasad = pc.getAinHasad();
			if (temphasad > 49000000) {
				pc.sendPackets(new S_SystemMessage("아인하사드의 축복: 남아있는 축복지수가 많아 사용 할 수 없습니다."));
				return;
			}

			Timestamp deleteTime = null;
			deleteTime = new Timestamp(System.currentTimeMillis() + 3600000 * 3);// 86400000);//
																					// 3일
			pc.setDETime2(deleteTime);

			skill = 7785;
			packet = 0x01;
			msg = 2142;

			if(pc.getAinHasad() < 10000){
				pc.setAinHasad(1000000);
			}else{
				pc.calAinHasad(hasad);
			}
			pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.EINHASAD, pc), true);
			pc.sendPackets(new S_ServerMessage(msg), true);
			pc.sendPackets(new S_SkillSound(pc.getId(), effect), true);
			Broadcaster.broadcastPacket(pc,
					new S_SkillSound(pc.getId(), effect), true);

		} else if (itemId == L1ItemId.DRAGON_SAPHIRE) {
			/*
			 * if
			 * (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.DRAGON_EME_2
			 * ) == true) { pc.sendPackets(new S_ServerMessage(2146), true);
			 * return; }
			 */

			hasad = 500000;
			int temphasad = pc.getAinHasad();
			if (temphasad > 49500000) {
				pc.sendPackets(new S_SystemMessage("아인하사드의 축복: 남아있는 축복지수가 많아 사용 할 수 없습니다."));
				return;
			}

			Timestamp deleteTime = null;
			deleteTime = new Timestamp(System.currentTimeMillis() + 3600000 * 3);// 86400000);//
																					// 3일
			pc.setDETime2(deleteTime);
			skill = 7785;
			packet = 0x01;
			msg = 2142;
			
			if(pc.getAinHasad() < 10000){
				pc.setAinHasad(500000);
			}else{
				pc.calAinHasad(hasad);
			}
			pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.EINHASAD, pc), true);
			pc.sendPackets(new S_ServerMessage(msg), true);
			pc.sendPackets(new S_SkillSound(pc.getId(), effect), true);
			Broadcaster.broadcastPacket(pc,
					new S_SkillSound(pc.getId(), effect), true);
		} else if (itemId == L1ItemId.DRAGON_RUBY) {
			if (pc.getSkillEffectTimerSet().hasSkillEffect(
					L1SkillId.DRAGON_EME_2) == true) {
				pc.sendPackets(new S_ServerMessage(2146), true);
				return;
			}

			hasad = 300000;
			int temphasad = pc.getAinHasad();
			if (temphasad > 49700000) {
				pc.sendPackets(new S_SystemMessage(
						"아인하사드의 축복: 남아있는 축복지수가 많아 사용 할 수 없습니다."));
				return;
			}

			Timestamp deleteTime = null;
			deleteTime = new Timestamp(System.currentTimeMillis() + 3600000 * 3);// 86400000);//
																					// 3일
			pc.setDETime2(deleteTime);
			skill = 7785;
			packet = 0x01;
			msg = 2142;
			if(pc.getAinHasad() < 10000){
				pc.setAinHasad(300000);
			}else{
				pc.calAinHasad(hasad);
			}
			// pc.getSkillEffectTimerSet().setSkillEffect(skill, 10800 * 1000);
			pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.EINHASAD, pc), true);
			// pc.sendPackets(new S_PacketBox(S_PacketBox.DRAGON_EME, packet,
			// 10800), true);
			pc.sendPackets(new S_ServerMessage(msg), true);
			pc.sendPackets(new S_SkillSound(pc.getId(), effect), true);
			Broadcaster.broadcastPacket(pc,
					new S_SkillSound(pc.getId(), effect), true);
		} else if (itemId == L1ItemId.DRAGON_EME || itemId == 60292) {
			hasad = 1000000;
			int temphasad = pc.getAinHasad();
			if (temphasad > 49000000) {
				pc.sendPackets(new S_SystemMessage(
						"아인하사드의 축복: 남아있는 축복지수가 많아 사용 할 수 없습니다."));
				return;
			}

			Timestamp deleteTime = null;
			deleteTime = new Timestamp(System.currentTimeMillis() + 1800000);
			pc.setDETime(deleteTime);
			skill = 7786;
			packet = 0x02;
			msg = 2140;

			if(pc.getAinHasad() < 10000){
				pc.setAinHasad(1000000);
			}else{
				pc.calAinHasad(hasad);
			}
			pc.getSkillEffectTimerSet().setSkillEffect(skill, 1800 * 1000);
			pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.EINHASAD, pc), true);
			pc.sendPackets(
					new S_PacketBox(S_PacketBox.DRAGON_EME, packet, 1800), true);
			pc.sendPackets(new S_ServerMessage(msg), true);
			pc.sendPackets(new S_SkillSound(pc.getId(), effect), true);
			Broadcaster.broadcastPacket(pc,
					new S_SkillSound(pc.getId(), effect), true);
		}
		try {
			pc.save();
		} catch (Exception e) {
		}
		pc.getInventory().removeItem(item, 1);
	}

	private void useToiTeleportAmulets(L1PcInstance pc, int itemId, L1ItemInstance item) {
		boolean isTeleport = false;
		if (itemId >= 5001120 && itemId <= 5001129) { // 11,51Famulet
			if (pc.getMap().isEscapable()) {
			isTeleport = true;
			}
		}
		
		if (isTeleport) {
			pc.dx = item.getItem().get_locx();
			pc.dy = item.getItem().get_locy();
			pc.dm = item.getItem().get_mapid();
			pc.dh = pc.getMoveState().getHeading();
			pc.setTelType(7);
			pc.sendPackets(new S_SabuTell(pc), true);
		} else {
			pc.sendPackets(new S_SystemMessage("아무일도 일어나지 않았습니다."), true); // \f1 아무것도 일어나지 않았습니다.
		}
	}

	// 천상의 물약
		private void UseExpPotion(L1PcInstance pc, int item_id) {
			if (pc.getSkillEffectTimerSet().hasSkillEffect(71) == true) { // 디케이포션 상태
				pc.sendPackets(new S_ServerMessage(698, ""), true); // 마력에 의해 아무것도  마실 수가 없습니다.
				return;
			}
				pc.cancelAbsoluteBarrier();

				int time = 0;
				int gfx = 0;
				int msg = 0;
				if (item_id == L1ItemId.EXP_POTION || item_id == L1ItemId.EXP_POTION2) { // 경험치 상승 물약
					time = 3600; // 60분
					gfx = 13249;
					msg = 1313;
				} else if (item_id == L1ItemId.EXP_POTION_fairly) {
					time = 1800; // 30분
					gfx = 13249;
					msg = 1313;
				} else if (item_id == L1ItemId.EXP_POTION_cash) {
					time = 1800; // 30분
					gfx = 13249;
					msg = 4358;
				}

		pc.getSkillEffectTimerSet().removeSkillEffect(EXP_POTION);
		pc.getSkillEffectTimerSet().removeSkillEffect(EXP_POTION_cash);

		if (item_id == L1ItemId.EXP_POTION_cash) {
			pc.getSkillEffectTimerSet().setSkillEffect(EXP_POTION_cash, time * 1000);
		} else {
			pc.getSkillEffectTimerSet().setSkillEffect(EXP_POTION, time * 1000);
		}
		
		pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.신스킬, 30080), true);
		pc.sendPackets(new S_ACTION_UI(30080, time, 3069, 4358), true);
		
		pc.sendPackets(new S_SkillSound(pc.getId(), 9915), true);
		Broadcaster.broadcastPacket(pc, new S_SkillSound(pc.getId(), gfx), true);
		
		pc.sendPackets(new S_ServerMessage(msg), true);
	}

	private void UseExpPotion2(L1PcInstance pc, int item_id) {
		if (pc.getSkillEffectTimerSet().hasSkillEffect(71) == true) { // 디케이포션
																		// 상태
			pc.sendPackets(new S_ServerMessage(698, ""), true); // 마력에 의해 아무것도
																// 마실 수가 없습니다.
			return;
		}
		pc.cancelAbsoluteBarrier();

		int time = 0;
		if (item_id == L1ItemId.EXP_POTION3 // 게렝의 전투물약
		) { // 경험치 상승 물약
			time = 3600; // 60분
		}

		pc.getSkillEffectTimerSet().setSkillEffect(EXP_POTION3, time * 1000);
		pc.sendPackets(new S_SkillSound(pc.getId(), 7013), true);
		Broadcaster.broadcastPacket(pc, new S_SkillSound(pc.getId(), 7013),
				true);
		pc.sendPackets(new S_ServerMessage(1313), true);
	}

	private void EventT(L1PcInstance pc, L1ItemInstance useItem) {
		pc.getInventory().removeItem(useItem, 1);
		pc.getInventory().storeItem(L1ItemId.Inadril_T_ScrollA, 10);
		pc.getInventory().storeItem(L1ItemId.Inadril_T_ScrollB, 10);
		pc.getInventory().storeItem(L1ItemId.Inadril_T_ScrollC, 10);
		pc.getInventory().storeItem(L1ItemId.Inadril_T_ScrollD, 5);
	}

	private void 마빈의큐브(L1PcInstance pc, L1ItemInstance useItem) {
		Timestamp lastUsed = useItem.getLastUsed();
		if (lastUsed == null
				|| currentDate.getTimeInMillis() > lastUsed.getTime()
						+ (1000 * 60 * 60 * 22)) {// 22시간
			if (pc.getInventory().checkItem(6016)) {
				pc.sendPackets(new S_SystemMessage(
						"인벤 토리에 불완전한 마법 구슬 조각이 남아 있습니다."));
				return;
			}

			pc.getInventory().storeItem(6016, 1);
			pc.sendPackets(new S_ServerMessage(403, "불완전한 마법 구슬 조각"), true);
			useItem.setChargeCount(useItem.getChargeCount() - 1);
			pc.getInventory().updateItem(useItem,
					L1PcInventory.COL_CHARGE_COUNT);
			if (useItem.getChargeCount() == 0) {
				pc.getInventory().removeItem(useItem);
			}
			useItem.setLastUsed(new Timestamp(currentDate.getTimeInMillis()));
		} else {
			long i = (lastUsed.getTime() + (60 * 60000 * 22))
					- currentDate.getTimeInMillis();
			Calendar cal = (Calendar) currentDate.clone();
			cal.setTimeInMillis(cal.getTimeInMillis() + i);
			StringBuffer sb = new StringBuffer();
			sb.append(i / 60000).append("분 후(");
			if (cal.getTime().getHours() < 10) {
				sb.append("0").append(cal.getTime().getHours()).append(":");
			} else {
				sb.append(cal.getTime().getHours()).append(":");
			}

			if (cal.getTime().getMinutes() < 10) {
				sb.append("0").append(cal.getTime().getMinutes())
						.append(")에 사용할 수 있습니다.");
			} else {
				sb.append(cal.getTime().getMinutes()).append(")에 사용할 수 있습니다.");
			}

			pc.sendPackets(new S_SystemMessage(sb.toString()), true);

		}
	}

	private void 토파즈큐브(L1PcInstance pc, L1ItemInstance useItem) {
		Timestamp lastUsed = useItem.getLastUsed();
		if (lastUsed == null
				|| currentDate.getTimeInMillis() > lastUsed.getTime()
						+ (1000 * 60 * 30)) {// 22시간
			pc.getInventory().storeItem(7241, 1);
			pc.sendPackets(new S_ServerMessage(403, "드래곤의 토파즈"), true);
			useItem.setChargeCount(useItem.getChargeCount() - 1);
			pc.getInventory().updateItem(useItem,
					L1PcInventory.COL_CHARGE_COUNT);
			if (useItem.getChargeCount() == 0) {
				pc.getInventory().removeItem(useItem);
			}
			useItem.setLastUsed(new Timestamp(currentDate.getTimeInMillis()));
		} else {
			long i = (lastUsed.getTime() + (30 * 60000))
					- currentDate.getTimeInMillis();
			Calendar cal = (Calendar) currentDate.clone();
			cal.setTimeInMillis(cal.getTimeInMillis() + i);
			StringBuffer sb = new StringBuffer();
			sb.append(i / 60000).append("분 후(");
			if (cal.getTime().getHours() < 10) {
				sb.append("0").append(cal.getTime().getHours()).append(":");
			} else {
				sb.append(cal.getTime().getHours()).append(":");
			}

			if (cal.getTime().getMinutes() < 10) {
				sb.append("0").append(cal.getTime().getMinutes())
						.append(")에 사용할 수 있습니다.");
			} else {
				sb.append(cal.getTime().getMinutes()).append(")에 사용할 수 있습니다.");
			}

			pc.sendPackets(new S_SystemMessage(sb.toString()), true);

		}
	}

	private void 축드다큐브(L1PcInstance pc, L1ItemInstance useItem) {
		pc.getInventory().storeItem(1437010, 1);
		pc.sendPackets(new S_ServerMessage(403, "축복받은 드래곤의 다이아몬드"), true);
		useItem.setChargeCount(useItem.getChargeCount() - 1);
		pc.getInventory().updateItem(useItem, L1PcInventory.COL_CHARGE_COUNT);
		if (useItem.getChargeCount() == 0) {
			pc.getInventory().removeItem(useItem);
		}
	}

	private void 시공의구슬큐브(L1PcInstance pc, L1ItemInstance useItem) {
		Timestamp lastUsed = useItem.getLastUsed();
		if (lastUsed == null
				|| currentDate.getTimeInMillis() > lastUsed.getTime()
						+ (1000 * 60 * 10)) {// 22시간
			pc.getInventory().storeItem(500017, 1);
			pc.sendPackets(new S_ServerMessage(403, "시공의 구슬"), true);
			useItem.setChargeCount(useItem.getChargeCount() - 1);
			pc.getInventory().updateItem(useItem,
					L1PcInventory.COL_CHARGE_COUNT);
			if (useItem.getChargeCount() == 0) {
				pc.getInventory().removeItem(useItem);
			}
			useItem.setLastUsed(new Timestamp(currentDate.getTimeInMillis()));
		} else {
			long i = (lastUsed.getTime() + (10 * 60000))
					- currentDate.getTimeInMillis();
			Calendar cal = (Calendar) currentDate.clone();
			cal.setTimeInMillis(cal.getTimeInMillis() + i);
			StringBuffer sb = new StringBuffer();
			sb.append(i / 60000).append("분 후(");
			if (cal.getTime().getHours() < 10) {
				sb.append("0").append(cal.getTime().getHours()).append(":");
			} else {
				sb.append(cal.getTime().getHours()).append(":");
			}

			if (cal.getTime().getMinutes() < 10) {
				sb.append("0").append(cal.getTime().getMinutes())
						.append(")에 사용할 수 있습니다.");
			} else {
				sb.append(cal.getTime().getMinutes()).append(")에 사용할 수 있습니다.");
			}

			pc.sendPackets(new S_SystemMessage(sb.toString()), true);

		}
	}

	private void 아르카마법인형상자(L1PcInstance pc, L1ItemInstance useItem) {
		pc.getInventory().removeItem(useItem, 1);
		L1ItemInstance item = null;
		item = pc.getInventory().storeItem(600248, 10);
		pc.sendPackets(new S_ServerMessage(403, item.getName() + " (10)"));

		item = pc.getInventory().storeItem(600249, 3);
		pc.sendPackets(new S_ServerMessage(403, item.getName() + " (3)"));
	}

	// 오만층줌1~10 95
	// 오만부적1~10 5

	// 은망0.1

	// 제니스의반지0.1
	// 뱀망0.01
	// 머미왕관0.01
	// 리치로브0.01
	// 격분의장갑0.01
	// 시어의심안0.01
	// 아이리스목0.01

	// 섬멸자의체인소드0.001
	// 나발양검0.001
	private static final int 랜덤마법인형상자4단[] = { 500202, 5000035, 600244, 600245, 41551, 41552, 19006};
	private static final int 오만의탑주문서[] = { 60200, 40104, 40105, 40106, 40107, 40108, 40109, 40110, 40111, 40112 };
	private static final int 오만의탑이동부적[] = { 40284, 40285, 40286, 40287, 40288, 40283, 40282, 40281, 40280 };
	
	private void 랜덤마법인형상자(L1PcInstance pc, L1ItemInstance useItem) {
		pc.getInventory().removeItem(useItem, 1);
		L1ItemInstance sucitem = null;
		try {
			sucitem = ItemTable.getInstance().createItem(랜덤마법인형상자4단[_random.nextInt(랜덤마법인형상자4단.length)]);
			if (sucitem != null) {
				createNewItem4(pc, sucitem.getItemId(), 1, 0);
				//pc.sendPackets(new S_ServerMessage(403, sucitem.getName()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void 오만의보물상자(L1PcInstance pc, L1ItemInstance useItem) {
		pc.getInventory().removeItem(useItem, 1);
		int rnd = _random.nextInt(100000) + 1;
		// 100000 100%
		// 10000 10%
		// 1000 1%
		// 100 0.1%
		// 10 0.01%
		// 1 0.001%
		L1ItemInstance sucitem = null;
		try {
			if (rnd == 1) {// 0.001%
				sucitem = ItemTable.getInstance().createItem(90084);// 섬멸자의체인소드
			} else if (rnd == 2) {// 0.001%
				sucitem = ItemTable.getInstance().createItem(59);// 나발 양손검
			} else if (rnd <= 12) {
				sucitem = ItemTable.getInstance().createItem(20298);// 제니스의반지
			} else if (rnd <= 22) {
				sucitem = ItemTable.getInstance().createItem(20079);// 뱀파이어의망토
			} else if (rnd <= 32) {
				sucitem = ItemTable.getInstance().createItem(20017);// 머미로드의왕관
			} else if (rnd <= 42) {
				sucitem = ItemTable.getInstance().createItem(20107);// 리치로브
			} else if (rnd <= 52) {
				sucitem = ItemTable.getInstance().createItem(130220);// 격분의장갑
			} else if (rnd <= 62) {
				sucitem = ItemTable.getInstance().createItem(22009);// 시어의심안
			} else if (rnd <= 72) {
				sucitem = ItemTable.getInstance().createItem(20260);// 아이리스의목걸이
			} else if (rnd <= 172) {
				sucitem = ItemTable.getInstance().createItem(20074);// 은색의망토
			} else if (rnd <= 5172) {
				sucitem = ItemTable.getInstance().createItem(오만의탑이동부적[_random.nextInt(오만의탑이동부적.length)]);
			} else {
				sucitem = ItemTable.getInstance().createItem(오만의탑주문서[_random.nextInt(오만의탑주문서.length)]);
			}

			if (sucitem != null) {
				pc.getInventory().storeItem(sucitem, true);
				pc.sendPackets(new S_ServerMessage(403, sucitem.getName()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * 라스타바드 무기 제작 비법서 1개 5% 블랙 미스릴 판금 10개 16% 어둠의 주괴 50개 16% 그랑카인의 눈물 10개 16%
	 * 다크엘프 영혼의 결정체 300개 16% 성지의 유물 100개 15% 흑마법 가루 50개 16%
	 */
	private void 진명황의상자(L1PcInstance pc, L1ItemInstance useItem) {
		pc.getInventory().removeItem(useItem, 1);
		int rnd = _random.nextInt(10000) + 1;
		L1ItemInstance sucitem = null;
		int count = 1;
		try {
			if (rnd <= 500) {
				sucitem = ItemTable.getInstance().createItem(40965);
			} else if (rnd <= 2100) {
				sucitem = ItemTable.getInstance().createItem(40445);
				count = 10;
			} else if (rnd <= 3700) {
				sucitem = ItemTable.getInstance().createItem(40677);
				count = 50;
			} else if (rnd <= 5400) {
				sucitem = ItemTable.getInstance().createItem(40525);
				count = 10;
			} else if (rnd <= 7000) {
				sucitem = ItemTable.getInstance().createItem(40969);
				count = 300;
			} else if (rnd <= 8500) {
				sucitem = ItemTable.getInstance().createItem(40967);
				count = 100;
			} else {
				sucitem = ItemTable.getInstance().createItem(40964);
				count = 50;
			}

			if (sucitem != null) {
				if (count != 1) {
					sucitem.setCount(count);
				}
				pc.getInventory().storeItem(sucitem, true);
				pc.sendPackets(new S_ServerMessage(403, sucitem.getName()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static final int lv1doll[] = { 41248, 41250, 430000, 430002, 430004, 600241 };
	private static final int lv2doll[] = { 430001, 41249, 430500, 500108, 500109, 600242 };
	private static final int lv3doll[] = { 500205, 500204, 500203, 60324, 500110, 600243 };
	private static final int lv4doll[] = { 500202, 5000035, 600244, 600245 };
	private static final int lv5doll[] = { 600246, 600247 };

	private void 아르카마법인형주머니(L1PcInstance pc, L1ItemInstance useItem) {
		pc.getInventory().removeItem(useItem, 1);
		int rnd = _random.nextInt(10000) + 1;
		int rnd2 = _random.nextInt(100) + 1;
		L1ItemInstance sucitem = null;
		try {
			if (rnd <= 8000) {// 80%1단계
				sucitem = ItemTable.getInstance().createItem( lv1doll[_random.nextInt(lv1doll.length)]);
			} else if (rnd <= 9500) {// 15%2단계
				sucitem = ItemTable.getInstance().createItem( lv2doll[_random.nextInt(lv2doll.length)]);
			} else if (rnd <= 9999) {// 0.99%3단계
				sucitem = ItemTable.getInstance().createItem( lv3doll[_random.nextInt(lv3doll.length)]);
			} else {// 4,5단계 0.01%
				if (rnd2 <= 80) {
					sucitem = ItemTable.getInstance().createItem( lv4doll[_random.nextInt(lv4doll.length)]);
				} else {
					sucitem = ItemTable.getInstance().createItem( lv5doll[_random.nextInt(lv5doll.length)]);
				}
			}

			if (sucitem != null) {
				pc.getInventory().storeItem(sucitem, true);
				pc.sendPackets(new S_ServerMessage(403, sucitem.getName()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void UseExpPotion3(L1PcInstance pc, int item_id) {
		if (pc.getSkillEffectTimerSet().hasSkillEffect(71) == true) { // 디케이포션
																		// 상태
			pc.sendPackets(new S_ServerMessage(698, ""), true); // 마력에 의해 아무것도
																// 마실 수가 없습니다.
			return;
		}
		pc.cancelAbsoluteBarrier();

		int time = 0;
		if (item_id == L1ItemId.EXP_POTION4 // 게렝의 행운물약
		) { // 경험치 상승 물약
			time = 3600; // 60분
		}

		pc.getSkillEffectTimerSet().setSkillEffect(EXP_POTION2, time * 1000);
		pc.sendPackets(new S_SkillSound(pc.getId(), 7013), true);
		Broadcaster.broadcastPacket(pc, new S_SkillSound(pc.getId(), 7013),
				true);
		pc.sendPackets(new S_ServerMessage(1313), true);
	}

	private void 결혼반(L1PcInstance pc, L1ItemInstance useItem) {
		L1PcInstance partner = null;
		boolean partner_stat = false;
		if (pc.getPartnerId() != 0) { // 결혼중
			partner = (L1PcInstance) L1World.getInstance().findObject(
					pc.getPartnerId());
			if (partner != null && partner.getPartnerId() != 0
					&& pc.getPartnerId() == partner.getId()
					&& partner.getPartnerId() == pc.getId()) {
				partner_stat = true;
			}
		} else {
			pc.sendPackets(new S_SystemMessage("당신은 현재 결혼한 상태가 아닙니다."), true); // \f1당신은
			// 결혼하지
			// 않았습니다.
			return;
		}

		if (useItem.getChargeCount() <= 0) {
			return;
		}
		if (pc.getMapId() == 666) {
			return;
		}
		if (!닥쳐(partner)) {
			pc.sendPackets(new S_SystemMessage(
					"당신의 파트너는 지금 당신이 갈 수 없는 곳에서 플레이를 하고 있습니다."), true); // \f1당신의
			return;
		}

		if (partner_stat) {
			boolean castle_area = L1CastleLocation.checkInAllWarArea(
					partner.getX(), partner.getY(), partner.getMapId());

			if ((partner.getMapId() == 0 || partner.getMapId() == 4 || partner
					.getMapId() == 304)

			&& castle_area == false) {

				if (pc.getMapId() == 4
						&& ((pc.getX() >= 33331 && pc.getX() <= 33341
								&& pc.getY() >= 32430 && pc.getY() <= 32441)
								|| (pc.getX() >= 33258 && pc.getX() <= 33267
										&& pc.getY() >= 32396 && pc.getY() <= 32407)
								||

								(pc.getX() >= 34197 && pc.getX() <= 34302
										&& pc.getY() >= 33104
										&& pc.getY() <= 33533 && pc.getMap()
										.isNormalZone(pc.getX(), pc.getY()))
								|| // 황혼의산맥

								(pc.getX() >= 33453 && pc.getX() <= 33468
										&& pc.getY() >= 32331 && pc.getY() <= 32341)
								|| // 아덴의한국민

								(pc.getX() >= 33388 && pc.getX() <= 33397
										&& pc.getY() >= 32339 && pc.getY() <= 32350) || (pc
								.getX() >= 33464
								&& pc.getX() <= 33531
								&& pc.getY() >= 33168 && pc.getY() <= 33248) // ||
						)) {
					pc.sendPackets(new S_SystemMessage(
							"당신의 파트너는 지금 당신이 갈 수 없는 곳에서 플레이를 하고 있습니다."), true); // \f1당신의
					// 파트너는
					// 지금
					// 당신이 갈
					// 수 없는
					// 곳에서
					// 플레이중입니다.
				} else {
					if (pc.Sabutelok()) {
						useItem.setChargeCount(useItem.getChargeCount() - 1);
						pc.getInventory().updateItem(useItem, L1PcInventory.COL_CHARGE_COUNT);
						pc.dx = partner.getX();
						pc.dy = partner.getY();
						pc.dm = (short) partner.getMapId();
						pc.dh = pc.getMoveState().getHeading();
						pc.setTelType(7);
						pc.sendPackets(new S_SabuTell(pc), true);
					}
				}
			} else {
				pc.sendPackets(new S_SystemMessage(
						"당신의 파트너는 지금 당신이 갈 수 없는 곳에서 플레이를 하고 있습니다."), true); // \f1당신의
				// 파트너는
				// 지금
				// 당신이 갈
				// 수 없는
				// 곳에서
				// 플레이중입니다.
			}
		} else {
			pc.sendPackets(
					new S_SystemMessage("당신의 파트너는 지금 게임을 플레이하고 있지 않습니다."), true); // \f1당신의
			// 파트너는 지금
			// 플레이를 하고
			// 있지 않습니다.
		}
	}

	private void useCashScroll(L1PcInstance pc, int item_id, boolean ok) {
		int time = 0;
		if (ok) {
			time = 3600;
		} else {
			time = 1800;
		}
		if (pc.getSkillEffectTimerSet().hasSkillEffect(STATUS_CASHSCROLL)) {
			pc.getSkillEffectTimerSet().killSkillEffectTimer(STATUS_CASHSCROLL);
			pc.addHpr(-4);
			pc.addMaxHp(-50);
			pc.sendPackets(new S_HPUpdate(pc.getCurrentHp(), pc.getMaxHp()), true);
			if (pc.isInParty()) {
				pc.getParty().updateMiniHP(pc);
			}
			pc.sendPackets(new S_MPUpdate(pc.getCurrentMp(), pc.getMaxMp()), true);
		}
		if (pc.getSkillEffectTimerSet().hasSkillEffect(STATUS_CASHSCROLL2)) {
			pc.getSkillEffectTimerSet().killSkillEffectTimer(STATUS_CASHSCROLL2);
			pc.addMpr(-4);
			pc.addMaxMp(-40);
			pc.sendPackets(new S_MPUpdate(pc.getCurrentMp(), pc.getMaxMp()), true);
		}
		if (pc.getSkillEffectTimerSet().hasSkillEffect(STATUS_CASHSCROLL3)) {
			pc.getSkillEffectTimerSet().killSkillEffectTimer(STATUS_CASHSCROLL3);
			pc.addDmgup(-3);
			pc.addHitup(-3);
			pc.getAbility().addSp(-3);
			pc.sendPackets(new S_SPMR(pc), true);
		}
		if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.STATUS_투사증강)) {
			pc.getSkillEffectTimerSet().killSkillEffectTimer(L1SkillId.STATUS_투사증강);
			pc.addDmgup(-3);
			pc.addHitup(-5);
			pc.addPvPReductionByArmor(-3);
			pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.신스킬, 8486), true);
			pc.sendPackets(new S_ACTION_UI(8486, 0, 2430, 2160), true);
		}
		if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.STATUS_명궁증강)) {
			pc.getSkillEffectTimerSet().killSkillEffectTimer(L1SkillId.STATUS_명궁증강);
			pc.addBowDmgup(-3);
			pc.addBowHitup(-5);
			pc.addPvPReductionByArmor(-3);
			pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.신스킬, 8487), true);
			pc.sendPackets(new S_ACTION_UI(8487, 0, 2430, 2160), true);
		}
		if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.STATUS_현자증강)) {
			pc.getSkillEffectTimerSet().killSkillEffectTimer(L1SkillId.STATUS_현자증강);
			pc.getAbility().addSp(-3);
			pc.addSuccMagic(-5);
			pc.addPvPReductionByArmor(-3);
			pc.sendPackets(new S_SPMR(pc), true);
			pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.신스킬, 8488), true);
			pc.sendPackets(new S_ACTION_UI(8488, 0, 2430, 2160), true);
		}
		if (pc.getSkillEffectTimerSet().hasSkillEffect(STATUS_시원한얼음조각)) {
			pc.getSkillEffectTimerSet().removeSkillEffect(STATUS_시원한얼음조각);
		}
		if (item_id == L1ItemId.INCRESE_HP_SCROLL || item_id == L1ItemId.CHUNSANG_HP_SCROLL) {
			pc.addHpr(4);
			pc.addMaxHp(50);
			pc.sendPackets(new S_HPUpdate(pc.getCurrentHp(), pc.getMaxHp()), true);
			if (pc.isInParty()) {
				pc.getParty().updateMiniHP(pc);
			}
			pc.sendPackets(new S_MPUpdate(pc.getCurrentMp(), pc.getMaxMp()), true);
			pc.sendPackets(new S_SkillSound(pc.getId(), 7893), true);
			Broadcaster.broadcastPacket(pc, new S_SkillSound(pc.getId(), 7893), true);

		} else if (item_id == L1ItemId.INCRESE_MP_SCROLL || item_id == L1ItemId.CHUNSANG_MP_SCROLL) {
			pc.addMpr(4);
			pc.addMaxMp(40);
			pc.sendPackets(new S_MPUpdate(pc.getCurrentMp(), pc.getMaxMp()), true);
			pc.sendPackets(new S_SkillSound(pc.getId(), 7894), true);
			Broadcaster.broadcastPacket(pc, new S_SkillSound(pc.getId(), 7894), true);
			pc.getSkillEffectTimerSet().setSkillEffect(7894, time * 1000);
		} else if (item_id == L1ItemId.INCRESE_ATTACK_SCROLL || item_id == L1ItemId.CHUNSANG_ATTACK_SCROLL) {
			pc.addDmgup(3);
			pc.addHitup(3);
			pc.getAbility().addSp(3);
			pc.sendPackets(new S_SkillSound(pc.getId(), 7895), true);
			Broadcaster.broadcastPacket(pc, new S_SkillSound(pc.getId(), 7895), true);
			pc.getSkillEffectTimerSet().setSkillEffect(7895, time * 1000);
			pc.sendPackets(new S_SPMR(pc), true);
		} else if (item_id == 60354) {// 시원한얼음조각
			pc.addDmgup(2);
			pc.getAbility().addSp(2);
			pc.addHpr(1);
			pc.addMpr(1);
			pc.sendPackets(new S_SPMR(pc), true);
			pc.sendPackets(new S_SkillSound(pc.getId(), 8485), true);
			Broadcaster.broadcastPacket(pc, new S_SkillSound(pc.getId(), 8485), true);
			pc.getSkillEffectTimerSet().setSkillEffect(8485, time * 1000);
		} else if(item_id == 66712){
			pc.addDmgup(3);
			pc.addHitup(5);
			pc.addPvPReductionByArmor(3);
			pc.getSkillEffectTimerSet().setSkillEffect(8486, time * 1000);
			pc.sendPackets(new S_SkillSound(pc.getId(), 8031), true);
			Broadcaster.broadcastPacket(pc, new S_SkillSound(pc.getId(), 8031), true);
			pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.신스킬, 8486), true);
			pc.sendPackets(new S_ACTION_UI(8486, time, 2430, 5180), true);
		} else if(item_id == 66713){
			pc.addBowDmgup(3);
			pc.addBowHitup(5);
			pc.addPvPReductionByArmor(3);
			pc.getSkillEffectTimerSet().setSkillEffect(8487, time * 1000);
			pc.sendPackets(new S_SkillSound(pc.getId(), 8031), true);
			Broadcaster.broadcastPacket(pc, new S_SkillSound(pc.getId(), 8031), true);
			pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.신스킬, 8487), true);
			pc.sendPackets(new S_ACTION_UI(8487, time, 2430, 5181), true);
		} else if(item_id == 66714){
			pc.getAbility().addSp(3);
			pc.addSuccMagic(5);
			pc.addPvPReductionByArmor(3);
			pc.getSkillEffectTimerSet().setSkillEffect(8488, time * 1000);
			
			pc.sendPackets(new S_SPMR(pc), true);
			pc.sendPackets(new S_SkillSound(pc.getId(), 8031), true);
			Broadcaster.broadcastPacket(pc, new S_SkillSound(pc.getId(), 8031), true);
			pc.sendPackets(new S_ACTION_UI(S_ACTION_UI.신스킬, 8488), true);
			pc.sendPackets(new S_ACTION_UI(8488, time, 2430, 5182), true);
		}
	}

	private boolean createNewItem(L1PcInstance pc, int item_id, int count) {
		L1ItemInstance item = ItemTable.getInstance().createItem(item_id);
		if (item != null) {
			item.setCount(count);
			if (pc.getInventory().checkAddItem(item, count) == L1Inventory.OK) {
				pc.getInventory().storeItem(item);
			} else { // 가질 수 없는 경우는 지면에 떨어뜨리는 처리의 캔슬은 하지 않는다(부정 방지)
				L1World.getInstance()
						.getInventory(pc.getX(), pc.getY(), pc.getMapId())
						.storeItem(item);
			}
			pc.sendPackets(new S_ServerMessage(403, item.getLogName()), true); // %0를// 손에// 넣었습니다.
			return true;
		} else {
			return false;
		}
	}

	private boolean 노확인생성템(L1PcInstance pc, int item_id, int count,
			int EnchantLevel) {
		return 노확인생성템2(pc, item_id, count, EnchantLevel, 0, 0);
	}

	private boolean createNewItem2(L1PcInstance pc, int item_id, int count, int EnchantLevel) {
		return createNewItem2(pc, item_id, count, EnchantLevel, 0, 0);
	}

	private boolean 노확인생성템2(L1PcInstance pc, int item_id, int count,
			int EnchantLevel, int attrEnc, int SpiritIn) {
		L1ItemInstance item = ItemTable.getInstance().createItem(item_id);

		if (item != null) {
			item.setCount(count);
			item.setEnchantLevel(EnchantLevel);
			item.setIdentified(false);
			item.setAttrEnchantLevel(attrEnc);
			item.setRegistLevel(SpiritIn);
			if (pc.getInventory().checkAddItem(item, count) == L1Inventory.OK) {
				pc.getInventory().storeItem(item);
			} else { // 가질 수 없는 경우는 지면에 떨어뜨리는 처리의 캔슬은 하지 않는다(부정 방지)
				L1World.getInstance()
						.getInventory(pc.getX(), pc.getY(), pc.getMapId())
						.storeItem(item);
			}
			pc.sendPackets(new S_ServerMessage(403, item.getLogName()), true); // %0를
																				// 손에
																				// 넣었습니다.
			return true;
		} else {
			return false;
		}
	}

	private boolean createNewItem2(L1PcInstance pc, int item_id, int count,
			int EnchantLevel, int attrEnc, int SpiritIn) {
		L1ItemInstance item = ItemTable.getInstance().createItem(item_id);

		if (item != null) {

			item.setCount(count);
			item.setEnchantLevel(EnchantLevel);
			item.setIdentified(true);
			item.setAttrEnchantLevel(attrEnc);
			item.setRegistLevel(SpiritIn);
			if (pc.getInventory().checkAddItem(item, count) == L1Inventory.OK) {
				pc.getInventory().storeItem(item);
			} else { // 가질 수 없는 경우는 지면에 떨어뜨리는 처리의 캔슬은 하지 않는다(부정 방지)
				L1World.getInstance()
						.getInventory(pc.getX(), pc.getY(), pc.getMapId())
						.storeItem(item);
			}
			pc.sendPackets(new S_ServerMessage(403, item.getLogName()), true); // %0를 손에 넣었습니다.
			return true;
		} else {
			return false;
		}
	}

	private L1ItemInstance createNewItem2(L1PcInstance pc, int item_id, int count, int EnchantLevel, int attrEnc, int SpiritIn, int bless) {
		L1ItemInstance item = ItemTable.getInstance().createItem(item_id);

		if (item != null) {

			item.setCount(count);
			item.setEnchantLevel(EnchantLevel);
			item.setIdentified(true);
			item.setAttrEnchantLevel(attrEnc);
			item.setRegistLevel(SpiritIn);
			item.setBless(bless);
			if (pc.getInventory().checkAddItem(item, count) == L1Inventory.OK) {
				pc.getInventory().storeItem(item);
			} else { // 가질 수 없는 경우는 지면에 떨어뜨리는 처리의 캔슬은 하지 않는다(부정 방지)
				L1World.getInstance()
						.getInventory(pc.getX(), pc.getY(), pc.getMapId())
						.storeItem(item);
			}
			pc.sendPackets(new S_ServerMessage(403, item.getLogName()), true); // %0를
																				// 손에
																				// 넣었습니다.
			return item;
		} else {
			return null;
		}
	}

	/** 아이템 생성 변수 재배열 */
	private L1ItemInstance createNewItem(L1PcInstance pc, int item_id, int EnchantLevel, int bless) {
		L1ItemInstance item = ItemTable.getInstance().createItem(item_id);
		if (item != null) {
			item.setEnchantLevel(EnchantLevel);
			item.setBless(bless);
			item.setIdentified(true);
			if (pc.getInventory().checkAddItem(item, 1) == L1Inventory.OK) {
				pc.getInventory().storeItem(item);
			} else { // 가질 수 없는 경우는 지면에 떨어뜨리는 처리의 캔슬은 하지 않는다(부정 방지)
				L1World.getInstance().getInventory(pc.getX(), pc.getY(), pc.getMapId()).storeItem(item);
			}
			return item;
		} else {
			return null;
		}
	}

	private void useToiTeleportAmulet(L1PcInstance pc, int itemId, L1ItemInstance item) {
		boolean isTeleport = false;
		if (!pc.getMap().isEscapable()) { 
			pc.sendPackets(new S_ServerMessage(3236), true);
			return;
		}
		if (itemId == 60202 || itemId >= 40289 && itemId <= 40297) { // 오만부적11~91층까지
			if (pc.getMap().isEscapable()) { // 귀환가능지역인가를 검색한다
				if (pc.getX() >= 33923 && pc.getX() <= 33934 && pc.getY() >= 33340 && pc.getY() <= 33356 && pc.getMapId() == 4) {
					isTeleport = true;
				}
			}
		}
		if (isTeleport) {
			pc.dx = item.getItem().get_locx();
			pc.dy = item.getItem().get_locy();
			pc.dm = item.getItem().get_mapid();
			pc.dh = pc.getMoveState().getHeading();
			pc.setTelType(7);
			pc.sendPackets(new S_SabuTell(pc), true);
		} else {
			pc.sendPackets(new S_ServerMessage(3236), true); // \f1 아무것도 일어나지 않았습니다.
		}
	}

	private void IdentMapWand(L1PcInstance pc, int locX, int locY) {
		pc.sendPackets(new S_SystemMessage("Gab :"
				+ pc.getMap().getOriginalTile(locX, locY) + ",x :" + locX
				+ ",y :" + locY + ", mapId :" + pc.getMapId()), true);
		if (pc.getMap().isCloseZone(locX, locY)) {
			pc.sendPackets(new S_EffectLocation(locX, locY, (short) 10), true);
			Broadcaster.broadcastPacket(pc, new S_EffectLocation(locX, locY,
					(short) 10), true);
			pc.sendPackets(new S_SystemMessage("벽으로 인식중"), true);
		}
	}

	private void MapFixKeyWand(L1PcInstance pc, int locX, int locY) {
		String key = new StringBuilder().append(pc.getMapId()).append(locX).append(locY).toString();
		if (!pc.getMap().isCloseZone(locX, locY)) {
			if (!MapFixKeyTable.getInstance().isLockey(key)) {
				MapFixKeyTable.getInstance().storeLocFix(locX, locY, pc.getMapId());
				pc.sendPackets(new S_EffectLocation(locX, locY, (short) 1815), true);
				Broadcaster.broadcastPacket(pc, new S_EffectLocation(locX, locY, (short) 1815), true);
				pc.sendPackets(new S_SystemMessage("key추가 ,x :" + locX + ",y :" + locY + ", mapId :" + pc.getMapId()), true);
			}
		} else {
			pc.sendPackets(new S_SystemMessage("선택좌표는 벽이 아닙니다."), true);

			if (MapFixKeyTable.getInstance().isLockey(key)) {
				MapFixKeyTable.getInstance().deleteLocFix(locX, locY,
						pc.getMapId());
				pc.sendPackets(new S_EffectLocation(locX, locY, (short) 10),
						true);
				Broadcaster.broadcastPacket(pc, new S_EffectLocation(locX,
						locY, (short) 10), true);
				pc.sendPackets(new S_SystemMessage("key삭제 ,x :" + locX + ",y :"
						+ locY + ", mapId :" + pc.getMapId()), true);
			}
		}
	}

	private void StatInitialize(L1PcInstance pc) {
		L1SkillUse l1skilluse = new L1SkillUse();
		l1skilluse.handleCommands(pc, L1SkillId.CANCELLATION, pc.getId(), pc.getX(), pc.getY(), null, 0, L1SkillUse.TYPE_LOGIN);
		l1skilluse = null;
		if(pc.getSkillEffectTimerSet().hasSkillEffect(ADVANCE_SPIRIT)){
			pc.getSkillEffectTimerSet().removeSkillEffect(ADVANCE_SPIRIT);
		}
		if(pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.기간틱)){
			pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.기간틱);
		}
		if(pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.프라이드)){
			pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.프라이드);
		}
		if (pc.getWeapon() != null) {
			pc.getInventory().setEquipped(pc.getWeapon(), false, false, false, false);
		}
		
		for (L1ItemInstance armor : pc.getInventory().getItems()) {
			for (int type = 0; type <= 18; type++) {
				if (armor != null) {
					pc.getInventory().setEquipped(armor, false, false, false, false);
				}
			}
		}

		pc.getInventory().takeoffEquip(945);
		pc.sendPackets(new S_CharVisualUpdate(pc), true);
		pc.setReturnStat(pc.getExp());
		pc.setReturnStatus(1);
		pc.sendPackets(new S_SPMR(pc), true);
		pc.sendPackets(new S_OwnCharAttrDef(pc), true);
		pc.sendPackets(new S_OwnCharStatus2(pc), true);
		pc.sendPackets(new S_ReturnedStat(pc, S_ReturnedStat.START), true);
		try {
			pc.save();
		} catch (Exception e) {
			_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
		}
	}

	private void serchdroplist2(L1PcInstance pc, int targetId,
			L1ItemInstance item) {
		S_AttackPacket s_attackStatus = new S_AttackPacket(pc, 0,
				ActionCodes.ACTION_Wand);
		long curtime = System.currentTimeMillis() / 1000;
		pc.sendPackets(s_attackStatus);
		Broadcaster.broadcastPacket(pc, s_attackStatus);
		L1Object target = L1World.getInstance().findObject(targetId);
		if (target != null && target instanceof L1MonsterInstance) {
			L1MonsterInstance npc = (L1MonsterInstance) target;

			int transformId = npc.getNpcTemplate().getTransformId();
			if (transformId == -1) {
				pc.sendPackets(new S_Serchdrop2(npc.getNpcId()));
			} else {
				pc.sendPackets(new S_Serchdrop2(transformId));
			}
		}
	}

	private boolean CheckEffects(L1PcInstance pc) {
		if (pc.getSkillEffectTimerSet().hasSkillEffect(7671)
				|| pc.getSkillEffectTimerSet().hasSkillEffect(7672)
				|| pc.getSkillEffectTimerSet().hasSkillEffect(7673)
				|| pc.getSkillEffectTimerSet().hasSkillEffect(7674)
				|| pc.getSkillEffectTimerSet().hasSkillEffect(7675)
				|| pc.getSkillEffectTimerSet().hasSkillEffect(7676)
				|| pc.getSkillEffectTimerSet().hasSkillEffect(7677)) {
			pc.sendPackets(new S_ServerMessage(1594));
			return false;
		}
		return true;
	}

	public int checkObject(int x, int y, short m, int d) {
		L1Map map = L1WorldMap.getInstance().getMap(m);
		switch (d) {
		case 1:
			if (map.isPassable(x, y, 1)) {
				return 1;
			} else if (map.isPassable(x, y, 0)) {
				return 0;
			} else if (map.isPassable(x, y, 2)) {
				return 2;
			}
			break;
		case 2:
			if (map.isPassable(x, y, 2)) {
				return 2;
			} else if (map.isPassable(x, y, 1)) {
				return 1;
			} else if (map.isPassable(x, y, 3)) {
				return 3;
			}
			break;
		case 3:
			if (map.isPassable(x, y, 3)) {
				return 3;
			} else if (map.isPassable(x, y, 2)) {
				return 2;
			} else if (map.isPassable(x, y, 4)) {
				return 4;
			}
			break;
		case 4:
			if (map.isPassable(x, y, 4)) {
				return 4;
			} else if (map.isPassable(x, y, 3)) {
				return 3;
			} else if (map.isPassable(x, y, 5)) {
				return 5;
			}
			break;
		case 5:
			if (map.isPassable(x, y, 5)) {
				return 5;
			} else if (map.isPassable(x, y, 4)) {
				return 4;
			} else if (map.isPassable(x, y, 6)) {
				return 6;
			}
			break;
		case 6:
			if (map.isPassable(x, y, 6)) {
				return 6;
			} else if (map.isPassable(x, y, 5)) {
				return 5;
			} else if (map.isPassable(x, y, 7)) {
				return 7;
			}
			break;
		case 7:
			if (map.isPassable(x, y, 7)) {
				return 7;
			} else if (map.isPassable(x, y, 6)) {
				return 6;
			} else if (map.isPassable(x, y, 0)) {
				return 0;
			}
			break;
		case 0:
			if (map.isPassable(x, y, 0)) {
				return 0;
			} else if (map.isPassable(x, y, 7)) {
				return 7;
			} else if (map.isPassable(x, y, 1)) {
				return 1;
			}
			break;
		default:
			break;
		}
		return -1;
	}

	private static FastMap<String, Long> 시공의항아리_계정횟수 = new FastMap<String, Long>();

	public static boolean get시공항아리_계정횟수(String account, long time) {
		synchronized (시공의항아리_계정횟수) {
			try {
				if (시공의항아리_계정횟수 != null) {
					if (시공의항아리_계정횟수.containsKey(account)) {
						return false;
					} else {
						시공의항아리_계정횟수.put(account, time);
					}
				}
			} catch (Exception e) {
			}

			return true;
		}
	}

	public static void reset시공의항아리횟수() {
		synchronized (시공의항아리_계정횟수) {
			시공의항아리_계정횟수.clear();
		}
	}

	private static FastMap<String, Integer> 마빈주머니_계정횟수 = new FastMap<String, Integer>();

	public static boolean get마빈주머니_계정횟수(String account) {
		synchronized (마빈주머니_계정횟수) {
			int time = 0;
			try {
				time = 마빈주머니_계정횟수.get(account);
			} catch (Exception e) {
			}
			if (time >= 3)
				return false;
			마빈주머니_계정횟수.put(account, ++time);
			return true;
		}
	}

	public static void reset마빈주머니_계정횟수() {
		synchronized (마빈주머니_계정횟수) {
			마빈주머니_계정횟수.clear();
		}
	}
	@Override
	public String getType() {
		return C_ITEM_USE;
	}
}