/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */

package l1j.server.server.clientpackets;

import l1j.server.GameSystem.GameList;
import l1j.server.GameSystem.FireDragon.FireDragon;
import l1j.server.server.model.Broadcaster;
import l1j.server.server.model.Getback;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1ItemInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.model.skill.L1SkillId;
import l1j.server.server.serverpackets.S_CharVisualUpdate;
import l1j.server.server.serverpackets.S_HPUpdate;
import l1j.server.server.serverpackets.S_MPUpdate;
import l1j.server.server.serverpackets.S_MapID;
import l1j.server.server.serverpackets.S_NewUI;
import l1j.server.server.serverpackets.S_RemoveObject;
import l1j.server.server.serverpackets.S_SPMR;
import l1j.server.server.serverpackets.S_Weather;
import l1j.server.server.serverpackets.S_WorldPutObject;
import server.LineageClient;

// Referenced classes of package l1j.server.server.clientpackets:
// ClientBasePacket

public class C_RestartAfterDie extends ClientBasePacket {

	private static final String C_RESTART = "[C] C_Restart";

	public C_RestartAfterDie(byte abyte0[], LineageClient clientthread)
			throws Exception {
		super(abyte0);
		try {
			L1PcInstance pc = clientthread.getActiveChar();

			if (!pc.isDead()) {
				System.out.println("케릭터 : " + pc.getName() + " > 패킷 갖고 장난질이냐?");
				return;
			}

			int[] loc;

			if (pc.getHellTime() > 0) {
				loc = new int[3];
				loc[0] = 32701;
				loc[1] = 32777;
				loc[2] = 666;
			} else {
				/**
				 * 2011.03.11 고정수 죽고나서..
				 */
				// loc = Getback.GetBack_Location(pc, true);

				loc = Getback.GetBack_Restart(pc);
				if (pc.getMapId() >= 2100 && pc.getMapId() <= 2151&&pc.getMapId() == 2004&& pc.getMapId() == 15404&& pc.getMapId() == 208
						&& pc.getMapId() == 12862&& pc.getMapId() == 605) {
					loc[0] = 34063;
					loc[1] = 32270;
					loc[2] = (short) 4;
				}
				if (pc.getMapId() == 1703 || pc.getMapId() == 1708) {
					loc[0] = 32776;
					loc[1] = 32756;
					loc[2] = (short) 1710;
				}
			}


			/** 2011.07.31 고정수 복사 버그 방지 */
			/*
			 * int gab =
			 * L1WorldMap.getInstance().getMap(pc.getMapId()).getOriginalTile
			 * (pc.getX(), pc.getY());
			 * 
			 * if (gab == 0 || gab == 15) { loc[0] = 33437; loc[1] = 32812;
			 * loc[2] = 4; }
			 */
			if (pc.getInventory().checkItem(61013)) {
				pc.getInventory().consumeItem(61013,
						pc.getInventory().countItems(61013));
			}
			if (pc.getInventory().checkItem(6013)) {
				pc.getInventory().consumeItem(6013,
						pc.getInventory().countItems(6013));
			}
			if (pc.getInventory().checkItem(6014)) {
				pc.getInventory().consumeItem(6014,
						pc.getInventory().countItems(6014));
			}
			if (pc.getInventory().checkItem(60512)) {
				pc.getInventory().consumeItem(60512,
						pc.getInventory().countItems(60512));
			}
			if (pc.getInventory().checkItem(60513)) {
				pc.getInventory().consumeItem(60513,
						pc.getInventory().countItems(60513));
			}
			try {
				if (pc.getInventory().checkItem(7236)) {
					L1ItemInstance item = pc.getInventory().checkEquippedItem(
							7236);
					if (item != null) {
						pc.getInventory()
								.setEquipped(item, false, false, false);
					}
					pc.getInventory().consumeItem(7236,
							pc.getInventory().countItems(7236));
					FireDragon fd = null;
					synchronized (GameList.FDList) {
						fd = GameList.getFD(pc.getMapId());
					}
					if (fd != null) {
						if (fd._pc.getId() == pc.getId()) {
							fd.Reset();
						}
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
				// _log.log(Level.SEVERE, e.getLocalizedMessage(), e);
			}
			pc.getNearObjects().removeAllKnownObjects();
			S_RemoveObject sro = new S_RemoveObject(pc);
			Broadcaster.broadcastPacket(pc, sro, true);

			pc.setCurrentHp(pc.getLevel());
			pc.set_food(39); // 죽었을때 겟지? 10%
			pc.setDead(false);
			pc.setActionStatus(0);
			L1World.getInstance().moveVisibleObject(pc, loc[0], loc[1], loc[2]);
			pc.setX(loc[0]);
			pc.setY(loc[1]);
			pc.setMap((short) loc[2]);

			pc.tempx = pc.getX();
			pc.tempy = pc.getY();
			pc.tempm = pc.getMapId();
			pc.temph = pc.getMoveState().getHeading();

			S_MapID smp = new S_MapID(pc.getMapId(), pc.getMap().isUnderwater());
			pc.sendPackets(smp, true);

			for (L1PcInstance pc2 : L1World.getInstance().getVisiblePlayer(pc)) {
				pc2.sendPackets(new S_WorldPutObject(pc,pc2));
			}
			long sysTime = System.currentTimeMillis();
			if (pc.getNetConnection().getAccount().getBuff_HPMP() != null) {
				if (sysTime <= pc.getNetConnection().getAccount().getBuff_HPMP().getTime()) {
					long bufftime = pc.getNetConnection().getAccount().getBuff_HPMP().getTime() - sysTime;
					pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.강화버프_활력);
					pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.강화버프_활력, (int) bufftime);
					pc.sendPackets(new S_NewUI("활력", (long) bufftime), true);
					pc.addMaxHp(50);
					pc.addMaxMp(50);
					pc.addWeightReduction(3);
					pc.sendPackets(new S_HPUpdate(pc));
					pc.sendPackets(new S_MPUpdate(pc.getCurrentMp(), pc.getMaxMp()));
				}
			}

			if (pc.getNetConnection().getAccount().getBuff_DMG() != null) {
				if (sysTime <= pc.getNetConnection().getAccount().getBuff_DMG().getTime()) {
					long bufftime = pc.getNetConnection().getAccount().getBuff_DMG().getTime() - sysTime;
					pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.강화버프_공격);
					pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.강화버프_공격, (int) bufftime);
					pc.sendPackets(new S_NewUI("공격", (long) bufftime), true);
					pc.addDmgup(1);
					pc.addBowDmgup(1);
				}
			}

			if (pc.getNetConnection().getAccount().getBuff_REDUC() != null) {
				if (sysTime <= pc.getNetConnection().getAccount().getBuff_REDUC().getTime()) {
					long bufftime = pc.getNetConnection().getAccount().getBuff_REDUC().getTime() - sysTime;
					pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.강화버프_방어);
					pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.강화버프_방어, (int) bufftime);
					pc.sendPackets(new S_NewUI("방어", (long) bufftime), true);
					pc.addDamageReductionByArmor(1);
				}
			}

			if (pc.getNetConnection().getAccount().getBuff_MAGIC() != null) {
				if (sysTime <= pc.getNetConnection().getAccount().getBuff_MAGIC().getTime()) {
					long bufftime = pc.getNetConnection().getAccount().getBuff_MAGIC().getTime() - sysTime;
					pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.강화버프_마법);
					pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.강화버프_마법, (int) bufftime);
					pc.sendPackets(new S_NewUI("마법", (long) bufftime), true);
					pc.getAbility().addSp(1);
					pc.sendPackets(new S_SPMR(pc));
				}
			}

			if (pc.getNetConnection().getAccount().getBuff_STUN() != null) {
				if (sysTime <= pc.getNetConnection().getAccount().getBuff_STUN().getTime()) {
					long bufftime = pc.getNetConnection().getAccount().getBuff_STUN().getTime() - sysTime;
					pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.강화버프_스턴);
					pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.강화버프_스턴, (int) bufftime);
					pc.sendPackets(new S_NewUI("스턴", (long) bufftime), true);
					pc.getResistance().addTechnique(2);
				}
			}

			if (pc.getNetConnection().getAccount().getBuff_HOLD() != null) {
				if (sysTime <= pc.getNetConnection().getAccount().getBuff_HOLD().getTime()) {
					long bufftime = pc.getNetConnection().getAccount().getBuff_HOLD().getTime() - sysTime;
					pc.getSkillEffectTimerSet().removeSkillEffect(L1SkillId.강화버프_홀드);
					pc.getSkillEffectTimerSet().setSkillEffect(L1SkillId.강화버프_홀드, (int) bufftime);
					pc.sendPackets(new S_NewUI("홀드", (long) bufftime), true);
					pc.getResistance().addSpirit(2);
				}
			}
			S_WorldPutObject ocp2 = new S_WorldPutObject(pc);
			pc.sendPackets(ocp2, true);
			S_CharVisualUpdate cvu = new S_CharVisualUpdate(pc);
			pc.sendPackets(cvu, true);
			S_Weather sw = new S_Weather(L1World.getInstance().getWeather());
			pc.sendPackets(sw, true);
			if (pc.getHellTime() > 0) {
				pc.beginHell(false);
			}
			L1ItemInstance item = pc.getInventory().checkEquippedItem(20344);
			if (item != null) { //
				if (item.isEquipped()) {
					pc.getInventory().setEquipped(item, false);
				}
			}
			loc = null;
		} catch (Exception e) {

		} finally {
			clear();
		}
	}

	@Override
	public String getType() {
		return C_RESTART;
	}
}