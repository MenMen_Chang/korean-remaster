package l1j.server.server;

import java.util.Calendar;

import l1j.server.server.model.L1BugBearRace;
import l1j.server.server.model.L1World;
import l1j.server.server.serverpackets.S_PacketBox;
import l1j.server.server.utils.L1SpawnUtil;

public class FieldBossController3 extends Thread {
	
		private static FieldBossController3 _instance;

		private boolean _FieldBossStart;
		
		public boolean getFieldBossStart() {
			return _FieldBossStart;
		}
		public void setFieldBossStart(boolean FieldBoss) {
			_FieldBossStart = FieldBoss;
		}

		public boolean isGmOpen = false;
		
		public static FieldBossController3 getInstance() {
			if(_instance == null) {
				_instance = new FieldBossController3();
			}
			return _instance;
		}
		
		@Override
			public void run() {
			while (true) {
				try	{
					if(isOpen()){
						FieldBossController3.getInstance().setFieldBossStart(true);
						Thread.sleep(1000);
						Open_Ment();
						Thread.sleep(61000);
					}
					if(isClose()){
						FieldBossController3.getInstance().setFieldBossStart(false);
						Thread.sleep(1000);
						Close_Ment();
						Thread.sleep(61000);
					}
				} catch(Exception e){
					e.printStackTrace();
				} finally {
					try{
					Thread.sleep(1000);
				} catch(Exception e){
				}
				}
			}
		}
		 
			 private void Open_Ment(){
				 try{
					 L1BugBearRace.getInstance();
					 L1BugBearRace.getInstance().loadingGame();
					 L1World.getInstance().broadcastServerMessage("\\aD[인형 운동회 알림]: 인형 운동회가 시작되었습니다.");
					 L1World.getInstance().broadcastPacketToAll(
					 new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\fO[인형 운동회 알림]: 인형 운동회가 시작되었습니다."), true);
				 } catch(Exception e2){
						e2.printStackTrace();
				 }
				}
			 
			 private void Close_Ment(){
				 try{
					setFieldBossStart(false);
					L1World.getInstance().broadcastServerMessage("\\aD[인형 운동회 알림]: 인형 운동회가 종료되었습니다.");
					L1World.getInstance().broadcastPacketToAll(
					new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\fO[인형 운동회 알림]: 인형 운동회가 종료되었습니다."), true);
				 } catch(Exception e2){
						e2.printStackTrace();
				 }
				}
			 
			 private boolean isOpen() {
				  Calendar calender = Calendar.getInstance();
				  int hour, minute;
				  hour = calender.get(Calendar.HOUR_OF_DAY);
				  minute = calender.get(Calendar.MINUTE);	 
			   if (hour == 20 && minute == 00) {
				  return true;
				  }
				  return false;
				 }
			 private boolean isClose() {
				  Calendar calender = Calendar.getInstance();
				  int hour, minute;
				  hour = calender.get(Calendar.HOUR_OF_DAY);
				  minute = calender.get(Calendar.MINUTE);	 
			   if (hour == 01 && minute == 0) {
				  return true;
				  }
				  return false;
				 }
			 
			 /** 종료 **/
			 public void End() {
				 setFieldBossStart(false);
			 }
}