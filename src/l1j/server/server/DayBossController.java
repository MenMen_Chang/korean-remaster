package l1j.server.server;

import java.util.Calendar;
import java.util.Random;

import l1j.server.Config;
import l1j.server.GameSystem.BossTimer;
import l1j.server.server.datatables.DoorSpawnTable;
import l1j.server.server.model.L1Teleport;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1DoorInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.serverpackets.S_PacketBox;
import l1j.server.server.utils.L1SpawnUtil;

public class DayBossController extends Thread {
	
		private static DayBossController _instance;
		
		private static Random _random = new Random(System.nanoTime());
		
		private boolean _DayBossStart;
		
		public boolean getDayBossStart() {
			return _DayBossStart;
		}
		public void setDayBossStart(boolean DayBoss) {
			_DayBossStart = DayBoss;
		}

		public boolean isGmOpen = false;
		
		public static DayBossController getInstance() {
			if(_instance == null) {
				_instance = new DayBossController();
			}
			return _instance;
		}
		
		@Override
			public void run() {
			while (true) {
				try	{
					if(isOpen1() && Config.결계발록여부){
						Spawn1(); 
						Thread.sleep(120000);
					}
					if(isOpen2() && Config.요일보스여부){
						Spawn2(); 
						Thread.sleep(120000);
					}
				} catch(Exception e){
					e.printStackTrace();
				} finally {
					try{
					Thread.sleep(1000);
				} catch(Exception e){
				}
				}
			}
		}
		
		private void Spawn1(){
			 try{
				 String[] weekDay = { "일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일" };     
				   Calendar cal = Calendar.getInstance(); 
				      int num = cal.get(Calendar.DAY_OF_WEEK)-1; 
				      String today = weekDay[num]; 
				      if(today.equalsIgnoreCase("월요일") || today.equalsIgnoreCase("화요일") || today.equalsIgnoreCase("수요일")  
				    		  || today.equalsIgnoreCase("목요일")|| today.equalsIgnoreCase("금요일")|| today.equalsIgnoreCase("토요일")
				    		  || today.equalsIgnoreCase("일요일")){
							L1World.getInstance().broadcastServerMessage("\\aI[보스 알림]: 지배의 결계 2층에서 발록이 출현합니다.");
							L1World.getInstance().broadcastPacketToAll(
							new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\fO[보스 알림]: 지배의 결계 2층에서 발록이 출현합니다."), true);

							L1SpawnUtil.spawn2(32731, 32864, (short) 15404, 45752, 0, 3600*1000, 0);//발록
							for(L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
								if (pc.getMapId() == 15403 || pc.getMapId() == 15404){
									L1Teleport.teleport(pc, 33429, 32829, (short) 4, 4, true);
								}
							}
						/**	L1DoorInstance door = DoorSpawnTable.getInstance().getDoor(225);
					if (door != null) {
						if (door.getOpenStatus() == ActionCodes.ACTION_Close) {
							door.open();
						}
					}
					L1DoorInstance door1 = DoorSpawnTable.getInstance().getDoor(226);
					if (door1 != null) {
					if (door1.getOpenStatus() == ActionCodes.ACTION_Close) {
					door1.open();
						}
					}
					L1DoorInstance door2 = DoorSpawnTable.getInstance().getDoor(227);
					if (door2 != null) {
					if (door2.getOpenStatus() == ActionCodes.ACTION_Close) {
					door2.open();
						}
					}**/
				 }
			 } catch(Exception e2){
					e2.printStackTrace();
			 }
			}
		
			 private void Spawn2(){
				 try{
					    	  int rnd = 0;
					    	  rnd = _random.nextInt(16)+1;
					    	  if(rnd == 1){
									L1World.getInstance().broadcastServerMessage("\\aD[보스 알림]: 왜곡의 제니스 퀸이 출현합니다. ( 지배의 탑 1층 )");
									L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\fO[보스 알림]: 왜곡의 제니스 퀸이 출현합니다.( 지배의 탑 1층 )"), true);
									 L1SpawnUtil.spawn2(32819, 32800, (short) 12852, 78000, 0, 3600*1000, 0);//제니스
					    	  } else if(rnd == 2){
									L1World.getInstance().broadcastServerMessage("\\aD[보스 알림]: 불신의 시어가 출현합니다. ( 지배의 탑 2층 )");
									L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\fO[보스 알림]: 불신의 시어가 출현합니다. ( 지배의 탑 2층 )"), true);
									 L1SpawnUtil.spawn2(32801, 32798, (short) 12853, 78001, 0, 3600*1000, 0);//시어
					    	  } else if(rnd == 3){
									L1World.getInstance().broadcastServerMessage("\\aD[보스 알림]: 공포의 뱀파이어가 출현합니다. ( 지배의 탑 3층 )");
									L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\fO[보스 알림]: 공포의 뱀파이어가 출현합니다. ( 지배의 탑 3층 )"), true);
									 L1SpawnUtil.spawn2(32801, 32795, (short) 12854, 78002, 0, 3600*1000, 0);//뱀파이어
					    	  } else if(rnd == 4){
									L1World.getInstance().broadcastServerMessage("\\aD[보스 알림]: 죽음의 좀비 로드가 출현합니다. ( 지배의 탑 4층 )");
									L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\fO[보스 알림]: 왜곡의 죽음의 좀비 로드가 출현합니다. ( 지배의 탑 4층 )"), true);
									L1SpawnUtil.spawn2(32672, 32863, (short) 12855, 78003, 0, 3600*1000, 0);//좀비 로드
					    	  } else if(rnd == 5){
									L1World.getInstance().broadcastServerMessage("\\aD[보스 알림]: 지옥의 쿠거가 출현합니다. ( 지배의 탑 5층 )");
									L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\fO[보스 알림]: 지옥의 쿠거가 출현합니다. ( 지배의 탑 5층 )"), true);
									 L1SpawnUtil.spawn2(32669, 32868, (short) 12856, 78004, 0, 3600*1000, 0);//쿠거
					    	  } else if(rnd == 6){
									L1World.getInstance().broadcastServerMessage("\\aD[보스 알림]: 불사의 머미로드가 출현합니다. ( 지배의 탑 6층 )");
									L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\fO[보스 알림]: 불사의 머미로드가 출현합니다. ( 지배의 탑 6층 )"), true);
									L1SpawnUtil.spawn2(32671, 32854, (short) 12857, 78005, 0, 3600*1000, 0);//머미로드
					    	  } else if(rnd == 7){
									L1World.getInstance().broadcastServerMessage("\\aD[보스 알림]: 냉혹한 아이리스가 출현합니다. ( 지배의 탑 7층 )");
									L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\fO[보스 알림]: 냉혹한 아이리스가 출현합니다. ( 지배의 탑 7층 )"), true);
									 L1SpawnUtil.spawn2(32672, 32853, (short) 12858, 78006, 0, 3600*1000, 0);//아이리스
					    	  } else if(rnd == 8){
									GeneralThreadPool.getInstance().execute(new 나발등장());
					    	  } else if(rnd == 9){
									L1World.getInstance().broadcastServerMessage("\\aD[보스 알림]: 불멸의 리치가 출현합니다. ( 지배의 탑 9층 )");
									L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\fO[보스 알림]: 불멸의 리치가 출현합니다. ( 지배의 탑 9층 )"), true);
									 L1SpawnUtil.spawn2(32678, 32851, (short) 12860, 78008, 0, 3600*1000, 0);//리치
					    	  } else if(rnd == 10) {
									GeneralThreadPool.getInstance().execute(new 우그등장());
					    	  } else if(rnd == 11) {
									L1World.getInstance().broadcastServerMessage("\\aD[보스 알림]:  거인 모닝스타가 출현합니다. ( 황혼의 산맥 )");
									L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\fO[보스 알림]: 거인 모닝스타가 출현합니다. ( 황혼의 산맥 )"), true);
									 L1SpawnUtil.spawn2(34242, 33370, (short) 4, 45610, 0, 3600*1000, 0);
					      	  } else if(rnd == 12) {
									L1World.getInstance().broadcastServerMessage("\\aD[보스 알림]: 피닉스가 출현합니다. ( 화룡의 둥지 )");
									L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\fO[보스 알림]: 피닉스가 출현합니다. ( 화룡의 둥지 )"), true);
									L1SpawnUtil.spawn2(33737, 32263, (short) 15440, 45617, 0, 3600*1000, 0);//피닉스
					      	  } else if(rnd == 13) {
									L1World.getInstance().broadcastServerMessage("\\aD[보스 알림]: 드레이크 킹이 출현합니다. (용의 계곡)");
									L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\fO[보스 알림]: 드레이크 킹이 출현합니다. (용의 계곡)"), true);
									 L1SpawnUtil.spawn2(33396,32341, (short) 15430, 707017, 0, 3600*1000, 0);//드레이크 킹
					      	  } else if(rnd == 14) {
									L1World.getInstance().broadcastServerMessage("\\aD[보스 알림]: 마족신전에서 아크모가 출현합니다.");
									L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\fO[보스 알림]: 마족신전에서 아크모가 출현합니다."), true);
									 L1SpawnUtil.spawn2(32903, 32792, (short) 410, 202083, 0, 3600*1000, 0);//아크모
					      	  } else if(rnd == 15) {
									L1World.getInstance().broadcastServerMessage("\\aD[보스 알림]: 테베 아누비스가 출현합니다. ( 테베 신전 )");
									L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\fO[보스 알림]: 테베 아누비스가 출현합니다. ( 테베 신전 )"), true);
									 L1SpawnUtil.spawn2(32778, 32832, (short) 782, 400016, 0, 3600*1000, 0);//테베보스
									
					      	  
									
					    	  }else{ 
									L1World.getInstance().broadcastServerMessage("\\aD[보스 알림]: 5분 후 윈다우드 사막에서 샌드웜과 에르자베가 출현합니다.");
									L1World.getInstance().broadcastPacketToAll(
									new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\fO[보스 알림]: 10분 후 윈다우드 사막에서 샌드웜과 에르자베가 출현합니다."), true);
									Thread.sleep(300000);
									BossTimer.getInstance().isOpen2 = true;
									L1World.getInstance().broadcastPacketToAll(
											new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\fO[보스 알림]: 샌드 웜 출현."), true);
									Thread.sleep(600000);
									BossTimer.getInstance().isOpen = true;
									L1World.getInstance().broadcastPacketToAll(
											new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\fO[보스 알림]: 에르자베 출현."), true);
					    	  }
					      
			 
				 } catch(Exception e2){
						e2.printStackTrace();
				 }
				}
			 

			 
			 private boolean isOpen1() {
				  Calendar calender = Calendar.getInstance();
				  int hour, minute;
				  hour = calender.get(Calendar.HOUR_OF_DAY);
				  minute = calender.get(Calendar.MINUTE);	 
			   if (hour == 01 && minute == 00||hour ==5 && minute == 00||hour == 9 && minute == 00
					   ||hour == 13 && minute == 00||hour == 17 && minute == 00||hour == 21 && minute == 00
					   ) {
				  return true;
				  }
				  return false;
				 }
			 
			 private boolean isOpen2() {
				  Calendar calender = Calendar.getInstance();
				  int hour, minute;
				  hour = calender.get(Calendar.HOUR_OF_DAY);
				  minute = calender.get(Calendar.MINUTE);	
			   if (hour == 2 && minute == 00||hour == 4 && minute ==00||hour == 6 && minute ==00
					   ||hour == 10 && minute ==00||hour == 14 && minute ==00||hour == 16 && minute ==00
					   ||hour == 18 && minute ==00||hour == 22 && minute ==00) {
				  return true;
				  }
				  return false;
				 }
			 
			 /** 종료 **/
			 public void End() {
				 setDayBossStart(false);
			 }
}