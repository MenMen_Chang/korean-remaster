package l1j.server.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Calendar;

import l1j.server.L1DatabaseFactory;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.serverpackets.S_PacketBox;
import l1j.server.server.utils.L1SpawnUtil;
import l1j.server.server.utils.SQLUtil;

public class DayQuestController extends Thread {
	
		private static DayQuestController _instance;

		private boolean _DayQuestStart;
		public boolean getDayQuestStart() {
			return _DayQuestStart;
		}
		public void setDayQuestStart(boolean DayQuest) {
			_DayQuestStart = DayQuest;
		}

		public boolean isGmOpen = false;
		
		public static DayQuestController getInstance() {
			if(_instance == null) {
				_instance = new DayQuestController();
			}
			return _instance;
		}
		
		@Override
			public void run() {
		
					while (true) {
						try	{
						if(!isOpen() && !isGmOpen)
						continue;
						setDayQuestStart(true);
						isGmOpen = false;
						
						Thread.sleep(1000);
						ResetOnline();
						L1World.getInstance().broadcastServerMessage("\\aD[일일 퀘스트 알림]: 일일 퀘스트가 초기화 되었습니다.");
						L1World.getInstance().broadcastPacketToAll(
						new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\fO[일일 퀘스트 알림]: 일일 퀘스트가 초기화 되었습니다."), true);
						ResetDB();
						Thread.sleep(60000);
						End();
				} catch(Exception e){
					e.printStackTrace();
				} finally {
					try{
						Thread.sleep(1000L);
					}catch(Exception e){
						e.printStackTrace();
				}
				}
				}
			}
		
			 private void ResetOnline(){
				 try{
					 for (L1PcInstance pc: L1World.getInstance().getAllPlayers()){
							if (L1World.getInstance().getPlayer(pc.getName()) == null || pc.getNetConnection() == null) {
								continue;
							}
							pc.getNetConnection().getAccount().setDayQuest(0);
					 }
				 }catch(Exception e2){
						e2.printStackTrace();
				 }
				}
			 
			  
			private void ResetDB() {
					Connection con = null;
					PreparedStatement pstm = null;
					try {
						con = L1DatabaseFactory.getInstance().getConnection();
						pstm = con.prepareStatement("UPDATE accounts SET dayquest=?");
						pstm.setInt(1, 0);
						pstm.executeUpdate();
					} catch (Exception e) {
						e.printStackTrace();
					} finally {
						SQLUtil.close(pstm);
						SQLUtil.close(con);
					}
				}
			 
			 private boolean isOpen() {
				  Calendar calender = Calendar.getInstance();
				  int hour, minute;
				  hour = calender.get(Calendar.HOUR_OF_DAY);
				  minute = calender.get(Calendar.MINUTE);	 
			   if (hour == 9 && minute == 00) {
				  return true;
				  }
				  return false;
				 }
			 
			 /** 종료 **/
			 public void End() {
				 setDayQuestStart(false);
			 }
}