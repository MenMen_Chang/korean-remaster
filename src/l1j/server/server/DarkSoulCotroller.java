package l1j.server.server;

import java.util.Calendar;
import java.util.Locale;
import java.text.SimpleDateFormat;

import l1j.server.server.model.Broadcaster;
import l1j.server.server.model.L1Inventory;
import l1j.server.server.model.L1Object;
import l1j.server.server.model.L1Teleport;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1ItemInstance;
import l1j.server.server.model.Instance.L1MonsterInstance;
import l1j.server.server.model.Instance.L1NpcInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.serverpackets.S_EffectLocation;
import l1j.server.server.serverpackets.S_NpcChatPacket;
import l1j.server.server.serverpackets.S_PacketBox;
import l1j.server.server.serverpackets.S_SystemMessage;
import l1j.server.server.utils.L1SpawnUtil;

public class DarkSoulCotroller extends Thread {
	
		private static DarkSoulCotroller _instance;

		private static long sTime = 0;	
		
		public boolean isGmOpen = false;
		
		public boolean _soulGive = false;
		
		public boolean _joinUser = false;
		
		public boolean _BossDie = false;
		
		private static final SimpleDateFormat s = new SimpleDateFormat("HH", Locale.KOREA);

		private static final SimpleDateFormat ss = new SimpleDateFormat("MM-dd HH:mm", Locale.KOREA);

		public static DarkSoulCotroller getInstance() {
			if(_instance == null) {
				_instance = new DarkSoulCotroller();
			}
			return _instance;
			}
		  	public boolean getJoinUser(){
			  return _joinUser;
			  }

			  public void setJoinUser(boolean flag){
			    _joinUser = flag;
			  }

			  public void setSoulGive(boolean flag){
			    _soulGive = flag;
			  }

			  public boolean getSoulGive(){
			    return _soulGive;
			  }
			  
			  public void setBossDie(boolean flag){
				    _BossDie = flag;
			  }

			  public boolean getBossDie(){
				    return _BossDie;
			  }

		@Override
			public void run() {
					while (true) {
						try	{
						DellMon();
						DellItem();
						
						if(!getJoinUser()){
						continue;
						}
						
						Thread.sleep(2000);
						
						NpcMent1();
						
						if(!getSoulGive()){
						continue;
						}
						
						NpcMent2();
						Thread.sleep(5000);
						Effect1();
						Sp1();
						Thread.sleep(5000);
						NpcMent3();
						Thread.sleep(35000);
						Effect2();
						Sp2();
						Thread.sleep(5000);
						NpcMent4();
						Thread.sleep(35000);
						Effect3();
						Sp3();
						Thread.sleep(5000);
						NpcMent5();
						Thread.sleep(40000);
						Effect4();
						Sp4();
						
						Thread.sleep(1000);
						BossMent6();
						Thread.sleep(2000);
						NpcMent6();
						Thread.sleep(60000);
						NpcMent9();
						
						for(L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
						if (pc.getMapId() == 5556){
							pc.sendPackets(new S_PacketBox(84, "10초후 마을로 이동합니다.."));
							}
						}
						
						Thread.sleep(10000);
						TelePort();
						setJoinUser(false);
						setSoulGive(false);
						
				} catch(Exception e){
					e.printStackTrace();
				} finally {
					try{
						Thread.sleep(1000L);
					} catch(Exception e){
						e.printStackTrace();
					}
				}
				}
			}

			private void TelePort() {
				for(L1PcInstance c : L1World.getInstance().getAllPlayers()) {
					if (c.getMapId() == 5556){
						 L1Teleport.teleport(c, 33970, 33246, (short) 4, 4, true);
					}
				}
			}
			/**
			 *오픈 시각을 가져온다
			 *
			 *@return (Strind) 오픈 시각(MM-dd HH:mm)
			 */
			 public String OpenTime() {
				 Calendar c = Calendar.getInstance();
				 c.setTimeInMillis(sTime);
				 return ss.format(c.getTime());
			 }
			private void SHOUT_MSG(L1NpcInstance npc, String msg) {
				Broadcaster.broadcastPacket(npc, new S_NpcChatPacket(npc, msg, 2), true);
			}
			
			private void SHOW_EFFECT(int locx, int locy, int num) {
				for (L1Object ob : L1World.getInstance().getVisibleObjects(5556).values()) {
					if (ob instanceof L1PcInstance) {
						L1PcInstance pc = (L1PcInstance) ob;
						pc.sendPackets(new S_EffectLocation(locx, locy,  num), true);
					}
				}
			}
			
			private void Effect1(){
						SHOW_EFFECT(33543, 32696, 10850);
						SHOW_EFFECT(33538, 32694, 10850);
						SHOW_EFFECT(33533, 32697, 10850);
						SHOW_EFFECT(33531, 32703, 10850);
						SHOW_EFFECT(33534, 32707, 10850);
						SHOW_EFFECT(33539, 32708, 10850);
						SHOW_EFFECT(33544, 32704, 10850);		
		 	}
			
			private void Effect2(){
						SHOW_EFFECT(33538, 32694, 10850);
						SHOW_EFFECT(33533, 32697, 10850);
						SHOW_EFFECT(33531, 32703, 10850);
						SHOW_EFFECT(33534, 32707, 10850);
						SHOW_EFFECT(33539, 32708, 10850);
						
					}

			private void Effect3(){
						SHOW_EFFECT(33538, 32694, 10850);
						SHOW_EFFECT(33531, 32703, 10850);
						SHOW_EFFECT(33539, 32708, 10850);
						
					}

			private void Effect4(){
						SHOW_EFFECT(33538, 32701, 11101);
		 	}
			
			private void Sp1(){
			    try{
			        L1SpawnUtil.spawn2(33543, 32696, (short) 5556, 11400, 0, 1200000, 0);
			        L1SpawnUtil.spawn2(33538, 32694, (short) 5556, 11400, 0, 1200000, 0);
			        L1SpawnUtil.spawn2(33533, 32697, (short) 5556, 11400, 0, 1200000, 0);
			        L1SpawnUtil.spawn2(33531, 32703, (short) 5556, 11400, 0, 1200000, 0);
			        L1SpawnUtil.spawn2(33534, 32707, (short) 5556, 11400, 0, 1200000, 0);
			        L1SpawnUtil.spawn2(33539, 32708, (short) 5556, 11400, 0, 1200000, 0);
			        L1SpawnUtil.spawn2(33544, 32704, (short) 5556, 11400, 0, 1200000, 0);
			      } catch (Exception e)
			      {
			        e.printStackTrace();
			      }
			}
			private void Sp2(){
			    try{
			      //  L1SpawnUtil.spawn2(33543, 32696, (short) 5556, 11400, 0, 1200000, 0);
			        L1SpawnUtil.spawn2(33538, 32694, (short) 5556, 11401, 0, 1200000, 0);
			        L1SpawnUtil.spawn2(33533, 32697, (short) 5556, 11401, 0, 1200000, 0);
			        L1SpawnUtil.spawn2(33531, 32703, (short) 5556, 11401, 0, 1200000, 0);
			        L1SpawnUtil.spawn2(33534, 32707, (short) 5556, 11401, 0, 1200000, 0);
			        L1SpawnUtil.spawn2(33539, 32708, (short) 5556, 11401, 0, 1200000, 0);
			     //   L1SpawnUtil.spawn2(32544, 32704, (short) 5556, 11400, 0, 1200000, 0);
			      } catch (Exception e)
			      {
			        e.printStackTrace();
			      }
			}
			private void Sp3(){
			    try{
			      //  L1SpawnUtil.spawn2(33543, 32696, (short) 5556, 11400, 0, 1200000, 0);
			        L1SpawnUtil.spawn2(33538, 32694, (short) 5556, 11402, 0, 1200000, 0);
			     //   L1SpawnUtil.spawn2(33533, 32697, (short) 5556, 11401, 0, 1200000, 0);
			        L1SpawnUtil.spawn2(33531, 32703, (short) 5556, 11402, 0, 1200000, 0);
			      //  L1SpawnUtil.spawn2(32534, 32707, (short) 5556, 11401, 0, 1200000, 0);
			        L1SpawnUtil.spawn2(33539, 32708, (short) 5556, 11402, 0, 1200000, 0);
			     //   L1SpawnUtil.spawn2(32544, 32704, (short) 5556, 11400, 0, 1200000, 0);
			      } catch (Exception e)
			      {
			        e.printStackTrace();
			      }
			}
			private void Sp4(){
			    try{
			        L1SpawnUtil.spawn2(33538, 32701, (short) 5556, 11403, 0, 1800000, 0);
			      } catch (Exception e)
			      {
			        e.printStackTrace();
			      }
			}
			
			private void NpcMent1(){
					for (L1Object ob : L1World.getInstance().getVisibleObjects(5556).values()) {
						if (ob instanceof L1NpcInstance) {
							L1NpcInstance npc = (L1NpcInstance) ob;
							if (npc.getNpcId() == 14816){
								SHOUT_MSG(npc, "용사님 암흑의 기운을 저에게 주세요!");
							}
						}
					}
			 	}
			private void NpcMent2(){
				for (L1Object ob : L1World.getInstance().getVisibleObjects(5556).values()) {
					if (ob instanceof L1NpcInstance) {
						L1NpcInstance npc = (L1NpcInstance) ob;
						if (npc.getNpcId() == 14816){
							SHOUT_MSG(npc, "자! 암흑의 결계를 풀겠습니다. 부디 성공하세요!");
						}
					}
				}
		 	}
			
			private void NpcMent3(){
				for (L1Object ob : L1World.getInstance().getVisibleObjects(5556).values()) {
					if (ob instanceof L1NpcInstance) {
						L1NpcInstance npc = (L1NpcInstance) ob;
						if (npc.getNpcId() == 14816){
						SHOUT_MSG(npc, "첫번째 결계가 풀렷어요! 다음 결계가 풀릴때까지 서두르세요. 용사님!");
						}
					}
				}
		 	}
			private void NpcMent4(){
				for (L1Object ob : L1World.getInstance().getVisibleObjects(5556).values()) {
					if (ob instanceof L1NpcInstance) {
						L1NpcInstance npc = (L1NpcInstance) ob;
						if (npc.getNpcId() == 14816){
						SHOUT_MSG(npc, "두번째 결계가 풀렷어요! 조금 더 힘을 내주세요. 용사님!");
						}
					}
				}
		 	}
			private void NpcMent5(){
				for (L1Object ob : L1World.getInstance().getVisibleObjects(5556).values()) {
					if (ob instanceof L1NpcInstance) {
						L1NpcInstance npc = (L1NpcInstance) ob;
						if (npc.getNpcId() == 14816){
						SHOUT_MSG(npc, "세번째 결계가 풀렷어요! 얼마 남지 않았습니다!");
						}
					}
				}
		 	}
			private void NpcMent6(){
				for (L1Object ob : L1World.getInstance().getVisibleObjects(5556).values()) {
					if (ob instanceof L1NpcInstance) {
						L1NpcInstance npc = (L1NpcInstance) ob;
						if (npc.getNpcId() == 14816){
						SHOUT_MSG(npc, "마지막 결계가 풀렷어요! 사악한 데스나이트입니다. 조심하세요!");
						}
					}
				}
		 	}
			
			private void NpcMent7(){
				for (L1Object ob : L1World.getInstance().getVisibleObjects(5556).values()) {
					if (ob instanceof L1NpcInstance) {
						L1NpcInstance npc = (L1NpcInstance) ob;
						if (npc.getNpcId() == 14816){
						SHOUT_MSG(npc, "시간이 없습니다. 용사님! 빨리 서두르셔야해요!");
						}
					}
				}
		 	}
			private void NpcMent8(){
				for (L1Object ob : L1World.getInstance().getVisibleObjects(5556).values()) {
					if (ob instanceof L1NpcInstance) {
						L1NpcInstance npc = (L1NpcInstance) ob;
						if (npc.getNpcId() == 14816){
						SHOUT_MSG(npc, "저는 이만 돌아가야합니다! 다음을 기약해요 용사님...");
						}
					}
				}
		 	}
			
			private void NpcMent9(){
				for (L1Object ob : L1World.getInstance().getVisibleObjects(5556).values()) {
					if (ob instanceof L1NpcInstance) {
						L1NpcInstance npc = (L1NpcInstance) ob;
						if (npc.getNpcId() == 14816){
						SHOUT_MSG(npc, "감사합니다! 용사님의 도움으로 저주가 풀렸습니다! 다음생에 용사님에게 꼭 보답할께요!");
						}
					}
				}
		 	}
			
			private void DellNpc(){
				for (L1Object ob : L1World.getInstance().getVisibleObjects(5556).values()) {
					if (ob instanceof L1NpcInstance) {
						L1NpcInstance npc = (L1NpcInstance) ob;
						if (npc == null || npc._destroyed || npc.isDead())
							continue;
							npc.deleteMe();
						}
					}
				}
			
			private void DellMon(){
				for (L1Object ob : L1World.getInstance().getVisibleObjects(5556).values()) {
					if (ob instanceof L1MonsterInstance) {
						L1MonsterInstance npc = (L1MonsterInstance) ob;
						if (npc == null || npc._destroyed || npc.isDead())
							continue;
							npc.deleteMe();
						}
					}
				}
			private void DellItem(){
			for (L1ItemInstance obj : L1World.getInstance().getAllItem()) {
				if (obj.getMapId() != 5556)
					continue;
				L1Inventory groundInventory = L1World.getInstance().getInventory(obj.getX(), obj.getY(), obj.getMapId());
				groundInventory.removeItem(obj);
				}
			}
			
			private boolean Success(){
				for (L1Object ob : L1World.getInstance().getVisibleObjects(5556).values()) {
					if (ob instanceof L1MonsterInstance) {
						L1MonsterInstance npc = (L1MonsterInstance) ob;
						if(npc.getNpcId() == 11403){
						if (npc == null || npc._destroyed || npc.isDead())
							return true;
							}
						}
					}
					return false;
				}
			
			
			
			private void BossMent6(){
				for (L1Object ob : L1World.getInstance().getVisibleObjects(5556).values()) {
					if (ob instanceof L1MonsterInstance) {
						L1MonsterInstance npc = (L1MonsterInstance) ob;
						if (npc.getNpcId() == 11403){
						SHOUT_MSG(npc, "나의 단잠을 깨우는자 누구든 용서치 않는다. 너는 어디서 온 풋내기인게냐!");
						}
					}
				}
		 	}
			
			
}