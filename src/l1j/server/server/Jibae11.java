package l1j.server.server;

import java.util.Calendar;
import java.util.Locale;
import java.text.SimpleDateFormat;
import l1j.server.server.model.L1Object;
import l1j.server.server.model.L1Teleport;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1MonsterInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.serverpackets.S_ChatPacket;
import l1j.server.server.serverpackets.S_PacketBox;


public class Jibae11 extends Thread {
	
	private static Jibae11 _instance;

	private boolean _J_B11Start;
	public boolean getJ_B11Start() {
		return _J_B11Start;
	}
	public void setJ_B11Start(boolean J_B11) {
		_J_B11Start = J_B11;
	}
	private static long sTime = 0;	
	
	public boolean isGmOpen = false;

	private static final SimpleDateFormat s = new SimpleDateFormat("HH", Locale.KOREA);

	private static final SimpleDateFormat ss = new SimpleDateFormat("MM-dd HH:mm", Locale.KOREA);

	public static Jibae11 getInstance() {
		if(_instance == null) {
			_instance = new Jibae11();
		}
		return _instance;
	}
	
	@Override
	public void run() {
		while (true) {
			try	{
				if(isJbae11Open()){
					Thread.sleep(60000);
					
					if(isClose()){
						End();
						Thread.sleep(10000);
					}
				}
				
			
	} catch(Exception e){
		e.printStackTrace();
	} finally{
		try{
			Thread.sleep(1000L);
		} catch(Exception e){
			e.printStackTrace();
	}

	}
		}
}



		 
		 private boolean isJbae11Open(){
			 try{
				 String[] weekDay = { "일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일" };     
				   Calendar cal = Calendar.getInstance(); 
				      int num = cal.get(Calendar.DAY_OF_WEEK)-1; 
				      String today = weekDay[num];
				      int hour, minute;
					  hour = cal.get(Calendar.HOUR_OF_DAY);
					  minute = cal.get(Calendar.MINUTE);
				      if(today.equalsIgnoreCase("금요일") || today.equalsIgnoreCase("토요일") || today.equalsIgnoreCase("일요일")
				    		  || today.equalsIgnoreCase("월요일")|| today.equalsIgnoreCase("화요일")|| today.equalsIgnoreCase("수요일")
				    		  || today.equalsIgnoreCase("목요일")){
				    	  if (hour == 3 && minute == 00) {
								
						    	
								L1World.getInstance().broadcastServerMessage("\\aD[이벤트던전]: 지배의탑 정상이 개방되었습니다.");
								L1World.getInstance().broadcastPacketToAll(
								new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\fO[이벤트던전]: 지배의탑 정상이 개방되었습니다."), true);
								setJ_B11Start(true);
					    	  }
				    	  if (hour == 7 && minute == 00) {
								
						    	
								L1World.getInstance().broadcastServerMessage("\\aD[이벤트던전]: 지배의탑 정상이 개방되었습니다.");
								L1World.getInstance().broadcastPacketToAll(
								new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\fO[이벤트던전]: 지배의탑 정상이 개방되었습니다."), true);
								setJ_B11Start(true);
					    	  }
				    	  if (hour == 11 && minute == 00) {
								
						    	
								L1World.getInstance().broadcastServerMessage("\\aD[이벤트던전]: 지배의탑 정상이 개방되었습니다.");
								L1World.getInstance().broadcastPacketToAll(
								new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\fO[이벤트던전]: 지배의탑 정상이 개방되었습니다."), true);
								setJ_B11Start(true);
					    	  }
				    	  if (hour == 15 && minute == 00) {
								
						    	
								L1World.getInstance().broadcastServerMessage("\\aD[이벤트던전]: 지배의탑 정상이 개방되었습니다.");
								L1World.getInstance().broadcastPacketToAll(
								new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\fO[이벤트던전]: 지배의탑 정상이 개방되었습니다."), true);
								setJ_B11Start(true);
					    	  }
				    	  if (hour == 19 && minute == 00) {
								
						    	
								L1World.getInstance().broadcastServerMessage("\\aD[이벤트던전]: 지배의탑 정상이 개방되었습니다.");
								L1World.getInstance().broadcastPacketToAll(
								new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\fO[이벤트던전]: 지배의탑 정상이 개방되었습니다."), true);
								setJ_B11Start(true);
					    	  }
				    	  if (hour == 23 && minute == 00) {
								
						    	
								L1World.getInstance().broadcastServerMessage("\\aD[이벤트던전]: 지배의탑 정상이 개방되었습니다.");
								L1World.getInstance().broadcastPacketToAll(
								new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\fO[이벤트던전]: 지배의탑 정상이 개방되었습니다."), true);
								setJ_B11Start(true);
					    	  }
					      }
				      
				      
			 } catch(Exception e2){
					e2.printStackTrace();
			 }
			return _J_B11Start;
			}
		 
		 private boolean isClose() {
			  Calendar calender = Calendar.getInstance();
			  int hour, minute;
			  hour = calender.get(Calendar.HOUR_OF_DAY);
			  minute = calender.get(Calendar.MINUTE);	 
			  if ((hour == 4 && minute == 00))
			   {

return true;
}
			  if ((hour == 8 && minute == 00))
			   {

return true;
}
			  if ((hour == 12 && minute == 00))
			   {

return true;
}
			  if ((hour == 16 && minute == 00))
			   {

return true;
}
			  if ((hour == 20 && minute == 00))
			   {

return true;
}
			  if ((hour == 00 && minute == 00))
			   {

return true;
}
	   
		  return false;
		 }
	 
		 /** 종료 **/
		 public void End() {
			 setJ_B11Start(false);
			 for(L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
		     if ( pc.getMapId() == 12862){
			 L1Teleport.teleport(pc, 33436, 32814, (short) 4, 4, true);
			 pc.sendPackets(new S_ChatPacket(pc, "지배의탑 정상 사용시간이 종료되었습니다.", Opcodes.S_MESSAGE, 17));
		     }
			}
			for (L1Object ob : L1World.getInstance().getVisibleObjects(12862).values()) {
				if (ob instanceof L1MonsterInstance) {
					L1MonsterInstance npc = (L1MonsterInstance) ob;
					if (npc == null || npc._destroyed || npc.isDead())
						continue;
						npc.deleteMe();
					}
				}

		 	}
	}