package l1j.server.server;

import java.util.Calendar;
import java.util.Random;

import l1j.server.server.model.L1World;
import l1j.server.server.serverpackets.S_PacketBox;
import l1j.server.server.utils.L1SpawnUtil;

public class BaphoController extends Thread {
	
		private static BaphoController _instance;
		Random random = new Random();
		private boolean _BaphoStart;
		public boolean getBaphoStart() {
			return _BaphoStart;
		}
		public void setBaphoStart(boolean Bapho) {
			_BaphoStart = Bapho;
		}

		public boolean isGmOpen = false;
		
		public static BaphoController getInstance() {
			if(_instance == null) {
				_instance = new BaphoController();
			}
			return _instance;
		}
		
		@Override
			public void run() {
					while (true) {
						try	{
						if(isOpen()){
						Spawn1(); 
						Thread.sleep(120000);
						End();
					}
				} catch(Exception e){
					e.printStackTrace();
				} finally {
					try{
						Thread.sleep(1000L);
					} catch(Exception e){
						e.printStackTrace();
					}
					}
					}
					}
		 
			 private void Spawn1(){ 
				 try{
					int locX1 = 32795;
					int locY1 = 32798;
					int locX2 = 32759;
					int locY2 = 32886;
					int locX3 = 32707;
					int locY3 = 32846;
					int rnd = random.nextInt(3);
					if(rnd == 1){
						L1SpawnUtil.spawn2(locX1, locY1, (short) 2, 45573, 0, 3600*1000, 0);//바포
					} else if (rnd == 2){
						L1SpawnUtil.spawn2(locX2, locY2, (short) 2, 45573, 0, 3600*1000, 0);//바포
					} else {
						L1SpawnUtil.spawn2(locX3, locY3, (short) 2, 45573, 0, 3600*1000, 0);//바포
					}
						L1SpawnUtil.spawn2(32783, 32738, (short) 28, 45829, 0, 3600*1000, 0);//바포
						/**L1World.getInstance().broadcastServerMessage("\\aI[보스 알림]: 바포메트[말던 2층], 키메라이드[사던 4층]가 나타났습니다.");
						L1World.getInstance().broadcastPacketToAll(
						new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\fO[보스 알림]: 바포메트 [말던 2층], 키메라이드[사던 4층]가 나타났습니다."));**/
				 }catch(Exception e2){
						e2.printStackTrace();
				 }
				}
			 private boolean isOpen() {
				  Calendar calender = Calendar.getInstance();
				  int hour, minute;
				  hour = calender.get(Calendar.HOUR_OF_DAY);
				  minute = calender.get(Calendar.MINUTE); 
				   if ((hour == 1 && minute == 40) || (hour == 3 && minute == 40) || (hour == 5 && minute == 40) || (hour == 7 && minute == 40)
						|| (hour == 9 && minute == 40) || (hour == 11 && minute == 40) || (hour == 13 && minute == 40) || (hour == 15 && minute == 40)
						|| (hour == 17 && minute == 40) || (hour == 19 && minute == 40) || (hour == 21 && minute == 40) || (hour == 23 && minute == 40)) {
							  return true;
							  }
							  return false;
							 }
			 
			 /** 종료 **/
			 public void End() {
				 setBaphoStart(false);
			 }
}