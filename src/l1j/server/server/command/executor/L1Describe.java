/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.   See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package l1j.server.server.command.executor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.StringTokenizer;
//import java.util.logging.Logger;

import java.util.logging.Level;

import l1j.server.L1DatabaseFactory;
import l1j.server.server.Account;
import l1j.server.server.datatables.ExpTable;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.serverpackets.S_ServerMessage;
import l1j.server.server.serverpackets.S_SystemMessage;
import l1j.server.server.utils.SQLUtil;

public class L1Describe implements L1CommandExecutor {
	//private static Logger _log = Logger.getLogger(L1Describe.class.getName());

	private L1Describe() {
	}

	public static L1CommandExecutor getInstance() {
		return new L1Describe();
	}

	@Override
	public void execute(L1PcInstance pc, String cmdName, String arg) {
		try {
			StringTokenizer st = new StringTokenizer(arg);
			String name = st.nextToken();
			L1PcInstance target = L1World.getInstance().getPlayer(name);
			if (target == null) {
				pc.sendPackets(new S_ServerMessage(73, name)); // \f1%0은 게임을 하고 있지 않습니다.
				return;
			}
			int lv = target.getLevel();
			int currentLvExp = ExpTable.getExpByLevel(lv);
			int nextLvExp = ExpTable.getExpByLevel(lv + 1);
			double neededExp = nextLvExp - currentLvExp ;
			double currentExp =  target.getExp() - currentLvExp;
			int per = (int)((currentExp / neededExp) * 100.0);
			
			String ClassName = "";
			
			if (target.isCrown()) {
				ClassName = "군주";
			} else if (target.isKnight()) {
				ClassName = "기사";
			} else if (target.isElf()) {
				ClassName = "요정";
			} else if (target.isDarkelf()) {
				ClassName = "다크엘프";
			} else if (target.isWizard()) {
				ClassName = "마법사";
			} else if (target.isDragonknight()) {
				ClassName = "용기사";
			} else if (target.isIllusionist()) {
				ClassName = "환술사";
			} else if (target.isWarrior()) {
				ClassName = "전사";
			}
						
			pc.sendPackets(new S_SystemMessage("(정보) 케릭터: " + target.getName() + " / 혈맹: " + target.getClanname()));
			pc.sendPackets(new S_SystemMessage("계정: " + target.getAccountName() + " / 비번: " + Account.load(target.getAccountName()).get_password()));
			pc.sendPackets(new S_SystemMessage("IP: " + target.getNetConnection().getIp() + " / 퀴즈: " + Account.load(target.getAccountName()).getquize()));
			pc.sendPackets(new S_SystemMessage("연락처: " +CheckPhone(target) + " / 이름: "+CheckName(target)));
			pc.sendPackets(new S_SystemMessage("----------------------------------------------------"));
			pc.sendPackets(new S_SystemMessage("* Lv: " + lv +"( " + per + "% )" + " (Ac: " + target.getAC().getAc() + " / Mr: " + target.getResistance().getMr() + ')')); 
			pc.sendPackets(new S_SystemMessage("* Lawful: " + target.getLawful() + " / " + "PkCnt: " + target.get_PKcount() + " / Class : " + ClassName));
			
			pc.sendPackets(new S_SystemMessage("* [일반] 공격성공 : (" + target.getHitup() +") / 추가타격 : (" + target.getDmgup() + ")"));
			pc.sendPackets(new S_SystemMessage("* [일반] 활명중치 : (" + target.getBowHitup() + ") / 활타격치 : (" + target.getBowDmgup() + ")"));
			pc.sendPackets(new S_SystemMessage("* [갑옷] 공격성공 : (" + target.getHitupByArmor() +") / 추가타격 : (" + target.getDmgupByArmor() + ")"));
			pc.sendPackets(new S_SystemMessage("* [갑옷] 활명중치 : (" + target.getBowHitupByArmor() + ") / 활타격치 : (" + target.getBowDmgupByArmor() + ")"));
			pc.sendPackets(new S_SystemMessage("* [인형] 활명중치 : (" + target.getBowHitupByDoll() + ") / 활타격치 : (" + target.getBowDmgupByDoll() + ")"));
			pc.sendPackets(new S_SystemMessage("* [갑옷] 데미지리덕 : (" + target.getDamageReductionByArmor() +") / 스펠 : ("+ target.getAbility().getSp() +")"));
	//		pc.sendPackets(new S_SystemMessage("* [캐릭스페셜] 추타 : (" + target.getAddDamage() +") / 확률 : " + target.getAddDamageRate()));
	//		pc.sendPackets(new S_SystemMessage("* [캐릭스페셜] 리덕 : (" + target.getAddReduction() +") / 확률 : " + target.getAddReductionRate()));
			pc.sendPackets(new S_SystemMessage("* [내성 관련] 기술 : (" + target.getResistance().getTechnique() +") / 정령 : (" + target.getResistance().getSpirit()+")"));
			pc.sendPackets(new S_SystemMessage("* [내성 관련] 용언 : (" + target.getResistance().getDragonLang() +") / 공포 : (" + target.getResistance().getFear()+")"));
			pc.sendPackets(new S_SystemMessage("* [적중 관련] 기술 : (" + target.getResistance().getTechniqueHit() +") / 정령 : (" + target.getResistance().getSpiritHit()+")"));
			pc.sendPackets(new S_SystemMessage("* [적중 관련] 용언 : (" + target.getResistance().getDragonLangHit() +") / 공포 : (" + target.getResistance().getFearHit()+")"));
			
			int hpr = target.getHpr() + target.getInventory(). hpRegenPerTick();
			int mpr = target.getMpr() + target.getInventory(). mpRegenPerTick();
			
			pc.sendPackets(new S_SystemMessage("* HP: (" + target.getCurrentHp() + '/' + target.getMaxHp() + ") (HPr: " + hpr + ')' 
											+ " / MP: (" + target.getCurrentMp() + '/' + target.getMaxMp() + ") (MPr: " + mpr + ')'));
			pc.sendPackets(new S_SystemMessage("\\aDBASE:(S: " + target.getAbility().getBaseStr() + " / " + "D: " + target.getAbility().getBaseDex() + " / " 
					                            + "C: " + target.getAbility().getBaseCon() + " / " + "I: " + target.getAbility().getBaseInt() + " / " 
					                            + "W: " + target.getAbility().getBaseWis() + " / " + "C: " + target.getAbility().getBaseCha() + ')'));
			pc.sendPackets(new S_SystemMessage("\\aHREAL:(S: " + target.getAbility().getTotalStr() + " / " + "D: " + target.getAbility().getTotalDex() + " / "
					    						+ "C: " + target.getAbility().getTotalCon() + " / " + "I: " + target.getAbility().getTotalInt() + " / "
					    						+ "W: " + target.getAbility().getTotalWis() + " / " + "C: " + target.getAbility().getTotalCha() + ')'));
			
			pc.sendPackets(new S_SystemMessage("----------------------------------------------------"));
		} catch (Exception e) {
			pc.sendPackets(new S_SystemMessage(".정보 [케릭터명] 으로 입력하세요."));
		}
	}
	
	private String CheckPhone(L1PcInstance pc) {
		String result = null;
		Connection con = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("SELECT * FROM userphone WHERE chaname=?");
			pstm.setString(1, pc.getName());
			rs = pstm.executeQuery();
			if (rs.next()) {
				result = rs.getString("pnumber");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(rs);
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
		return result;
	}
	
	private String CheckName(L1PcInstance pc) {
		String result = null;
		Connection con = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("SELECT * FROM userphone WHERE chaname=?");
			pstm.setString(1, pc.getName());
			rs = pstm.executeQuery();
			if (rs.next()) {
				result = rs.getString("name");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(rs);
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
		return result;
	}
}