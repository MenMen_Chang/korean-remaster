package l1j.server.server.serverpackets;


public class KeyPacket extends ServerBasePacket {
	private byte[] _byte = null;

	public KeyPacket() {
		byte[] _byte1 ={ // size
				(byte) 0xeb, (byte) 0x97, (byte) 0x35, (byte) 0x54, (byte) 0x16, (byte) 0xd2, (byte) 0x73, (byte) 0x27, (byte) 0x34};	
		
		for (int i = 0; i < _byte1.length; i++) {
			writeC(_byte1[i]);
		}
	}
	
	@Override
	public byte[] getContent() {
		if (_byte == null) {
			_byte = getBytes();
		}
		return _byte;
	}
}