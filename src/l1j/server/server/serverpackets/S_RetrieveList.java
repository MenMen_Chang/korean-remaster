/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package l1j.server.server.serverpackets;

import java.io.IOException;

import l1j.server.Warehouse.PrivateWarehouse;
import l1j.server.Warehouse.WarehouseManager;
import l1j.server.server.Opcodes;
import l1j.server.server.model.Instance.L1ItemInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.templates.L1Item;
import l1j.server.server.utils.BinaryOutputStream;

public class S_RetrieveList extends ServerBasePacket {

	private static final String _S_RetrieveList = "[S] S_RetrieveList";

	public boolean NonValue = false;

	public S_RetrieveList(int objid, L1PcInstance pc) {
		if (pc.getInventory().getSize() < 180) {
			PrivateWarehouse warehouse = WarehouseManager.getInstance().getPrivateWarehouse(pc.getAccountName());
			int size = warehouse.getSize();

			if (size > 0) {
				writeC(Opcodes.S_EXTENDED_PROTOBUF);
				writeC(0x08);
				writeC(0x04);
				writeC(0x08);
				writeBit(objid);
				writeC(0x10);
				writeBit(size);
				writeC(0x18);
				writeC(3);
				writeC(0x20);
				writeBit(100);
				L1ItemInstance item = null;
				byte[] status = null;
				int index = 0;
				for (Object itemObject : warehouse.getItems()) {
					item = (L1ItemInstance) itemObject;
					BinaryOutputStream detail = new BinaryOutputStream();
					L1Item temp = item.getItem();
					detail.writeC(0x08);
					detail.writeBit(item.getId());
					detail.writeC(0x10);
					detail.writeBit(temp.getItemDescId() == 0 ? -1 : temp.getItemDescId());
					detail.writeC(0x20);
					detail.writeBit(item.getCount());
					if (temp.getUseType() > 0) {
						detail.writeC(0x28);
						detail.writeBit(temp.getUseType());
					}
					detail.writeC(0x38);
					detail.writeBit(item.get_gfxid());
					detail.writeC(0x40);
					detail.writeC(item.getBless());
					//컴파일 쑤아리 질럿~!!~!
					if (item.getBless() >= 127) { // 봉인 상태일경우
						//detail.writeC(0x28); // 다른패킷 
					} else {
						detail.writeC(0x48); // 그렇지 않을경우 패킷을 내보낸다
					}
					detail.writeBit(item.getStatusBit());
					detail.writeC(0x58);
					detail.writeBit(0);
					detail.writeC(0x68);
					detail.writeBit(item.getEnchantLevel());
					detail.writeC(0x70);
					detail.writeBit(item.getTradeBit());
					if (temp.getType2() == 1 && item.getAttrEnchantLevel() > 0) {
						int attrlevel = item.getAttrEnchantLevel();
						int attrtype = 0;
						if (attrlevel >= 1 && attrlevel <= 5)
							attrtype = 1;
						else if (attrlevel >= 6 && attrlevel <= 10)
							attrtype = 2;
						else if (attrlevel >= 11 && attrlevel <= 15)
							attrtype = 3;
						else if (attrlevel >= 16 && attrlevel <= 20)
							attrtype = 4;
						detail.writeBit(128);
						detail.writeBit(attrtype);
						detail.writeBit(136);
						detail.writeBit(attrtype == 1 ? attrlevel : attrlevel - (5 * (attrtype - 1)));
					}

					detail.writeBit(146);
					detail.writeS3(item.getViewName());

					if (item.isIdentified()) {
						detail.writeBit(154);
						status = item.getStatusBytes();
						detail.writeBit(status.length);
						for (byte b : status)
							detail.writeC(b);
					}
					int bsize = getBitSize(detail.getLength() - 2) + getBitSize(index) + detail.getBytes().length + 2;

					writeC(0x32);
					writeBit(bsize);
					writeC(0x08);
					writeBit(index++);
					writeC(0x12);
					writeBit(detail.getLength() - 2);
					writeByte(detail.getBytes());
				}
				writeC(0x38);
				writeC(0x01);
				writeH(0x00);
			} else
				NonValue = true;
		} else {
			pc.sendPackets(new S_ServerMessage(263)); // \f1한사람의 캐릭터가 가지고 걸을 수
														// 있는 아이템은 최대 180개까지입니다.
		}
	}

	@Override
	public byte[] getContent() throws IOException {
		return getBytes();
	}

	@Override
	public String getType() {
		return _S_RetrieveList;
	}

}
