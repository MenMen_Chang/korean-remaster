package l1j.server.server.serverpackets;

import l1j.server.GameSystem.Robot.L1RobotInstance;
import l1j.server.server.ActionCodes;
import l1j.server.server.Opcodes;
import l1j.server.server.datatables.NPCTalkDataTable;
import l1j.server.server.model.L1Clan;
import l1j.server.server.model.L1NpcTalkData;
import l1j.server.server.model.Instance.L1FieldObjectInstance;
import l1j.server.server.model.Instance.L1NpcInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.model.Instance.L1SignboardInstance;
import l1j.server.server.model.skill.L1SkillId;

public class S_WorldPutObject extends ServerBasePacket {
	private static final int SC_WORLD_PUT_OBJECT_NOTI = 0x77;
	private static final int STATUS_POISON = 1;
	private static final int STATUS_INVISIBLE = 2;
	private static final int STATUS_PC = 4;
	private static final int STATUS_FREEZE = 8;
	private static final int STATUS_BRAVE = 16;
	private static final int STATUS_ELFBRAVE = 32;
	private static final int STATUS_FASTMOVABLE = 64;
	// private static final int BLOOD_LUST = 100;
	private static final int STATUS_GHOST = 128;
	private byte[] _byte = null;
	public  S_WorldPutObject(L1PcInstance pc, L1PcInstance user) {
		build(pc, user, false);
	}

	public  S_WorldPutObject(L1PcInstance pc, L1PcInstance user, boolean invis) {
		build(pc, user, invis);
	}

	public void build(L1PcInstance pc, L1PcInstance user, boolean invis) {
	
	}
	
	public S_WorldPutObject(L1PcInstance pc) {
		buildPacket(pc);
	}
	
	public S_WorldPutObject(L1NpcInstance npc) {
		buildPacket(npc);
	}
	
	private void buildPacket(L1PcInstance pc) {
		int status = STATUS_PC;

		// 굴독같은 초록의 독
		// if (pc.isPoison()) {
		// status |= STATUS_POISON;
		// }

		if (pc.isInvisble() || pc.isGmInvis()) {
			status |= STATUS_INVISIBLE;
		}
		if (pc.isBrave() || pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.FIRE_BLESS) || pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.샌드스톰)
				|| pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.허리케인)
				|| pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.포커스웨이브2단)
				|| pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.포커스웨이브3단)) {
			status |= STATUS_BRAVE;
		}

		if (pc.isElfBrave() || pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.포커스웨이브)) {
			status |= STATUS_BRAVE;
			status |= STATUS_ELFBRAVE;
		}
		
		if(pc.isUgdraFruit()){
			status |= STATUS_FASTMOVABLE;
		}
		 
		if (pc.isBloodLust()) {
			status |= STATUS_BRAVE;
		}
		if (pc.isFastMovable() || pc.getMapId() == 5143) {
			status |= STATUS_FASTMOVABLE;
		}
		if (pc.isGhost()) {
			status |= STATUS_GHOST;
		}
		if (pc.isParalyzed()) {
			status |= STATUS_FREEZE;
		}

		// int addbyte = 0;
		writeC(Opcodes.S_PUT_OBJECT);
		writeH(pc.getX());
		writeH(pc.getY());
		writeD(pc.getId());
		/*
		 * String s = "00 00 04 05 00 01 01 "+
		 * "00 00 00 ff 7f c6 e4 b8 a3 b3 ad 73 00 74 69 74 "+
		 * "6c 65 32 32 32 00 04 31 0a 08 00 b2 c3 c5 eb c5 "+
		 * "ac b7 b4 00 00 40 ff 00 00 00 ff ff 00 04 00 37 "; StringTokenizer
		 * st = new StringTokenizer(s); while(st.hasMoreTokens()){
		 * writeC(Integer.parseInt(st.nextToken(), 16)); }
		 */

		if (pc.isDead()) {
			writeH(pc.getTempCharGfxAtDead());
		} else {
			writeH(pc.getGfxId().getTempCharGfx());
		}

		if (pc.isDead()) {
			writeC(pc.getActionStatus());
		} else if (pc.isPrivateShop()) {
			writeC(0x46);
		} else {
			int polyId = pc.getGfxId().getTempCharGfx();
			int weaponId = pc.getCurrentWeapon();

			if (polyId == 3784 || polyId == 6137 || polyId == 6142
					|| polyId == 6147 || polyId == 6152 || polyId == 6157
					|| polyId == 9205 || polyId == 9206) {
				if (weaponId == 24 && pc.getWeapon() != null
						&& pc.getWeapon().getItem().getType() == 18)
					weaponId = 83;
			} else if (polyId == 13152 || polyId == 13153 || polyId == 12702
					|| polyId == 15154 || polyId == 15232 || polyId == 14923 || polyId == 15223
					|| polyId == 12681 || polyId == 8812 || polyId == 8817
					|| polyId == 6267 || polyId == 6270 || polyId == 6273
					|| polyId == 6276 || polyId == 16014 
					|| polyId == 15986 || polyId == 16027 || polyId == 16284 || polyId == 16053
					|| polyId == 16040 || polyId == 17541 || polyId == 17515 || polyId == 17531) {
				if (weaponId == 24 && pc.getWeapon() != null && pc.getWeapon().getItem().getType() == 18)
					weaponId = 50;
			}
			writeC(weaponId);
		}

		writeC(pc.getMoveState().getHeading());
		// writeC(addbyte);
		writeC(pc.getLight().getOwnLightSize());
		// System.out.println(pc.getMoveState().getMoveSpeed());
		writeC(pc.getMoveState().getMoveSpeed());
		writeD(pc.getExp());
		writeH(pc.getLawful());

		writeS(pc.getName());
         {
			writeS(pc.getTitle());
		}
		writeC(status);
		writeD(pc.getClanid());

		writeS(pc.getClanname()); // 크란명
		writeS(null);
		if (pc.getClanid() == 0 || pc.getClan() == null) {// 이시발놈!
			writeC(0);
		} else
			writeC(pc.getClanRank() * 0x10);
		writeC(0xFF);
		writeC(0); // 타르쿡크 거리(대로)
		writeC(0); // PC = 0, Mon = Lv
		if (pc.isPrivateShop())
			writeByte(pc.getShopChat());
		writeC(0); // ?
		writeC(0xFF);
		writeC(0xFF);
		writeC(0x00);
		int stype = 0;
		if (pc.getLevel() >= 15)
			stype++;
		if (pc.getLevel() >= 30)
			stype++;
		if (pc.getLevel() >= 45)
			stype++;
		if (pc.getLevel() >= 50)
			stype++;
		if (pc.getLevel() >= 52)
			stype++;
		if (pc.getLevel() >= 55)
			stype++;
		if (pc.getLevel() >= 60)
			stype++;
		if (pc.getLevel() >= 65)
			stype++;
		if (pc.getLevel() >= 70)
			stype++;
		if (pc.getLevel() >= 75)
			stype++;
		if (pc.getLevel() >= 80)
			stype++;
		if (pc.getLevel() >= 82)
			stype++;
		if (pc.getLevel() >= 85)
			stype++;
		if (stype > 13)
			stype = 13;
		writeC(stype);
		writeC(0xFF);
		writeH(0x00);
	
		writeC(Opcodes.S_EXTENDED_PROTOBUF);
		writeH(SC_WORLD_PUT_OBJECT_NOTI);
		
		writeC(0x08);			// point
		writeBit(pc.getX(), pc.getY());
		writeC(0x10);			// objectnumber
		writeBit(pc.getId());
		writeC(0x18); 		// objectsprite
		if (pc.isDead()) {
			writeBit(pc.getTempCharGfxAtDead());
		} else {
			writeBit(pc.getGfxId().getTempCharGfx());
		}
		
		writeC(0x20); 		// action
		if (pc.isDead())			writeBit(ActionCodes.ACTION_Die);
		else if (pc.isPrivateShop())writeBit(70L);
		else if (pc.isFishing())	writeBit(71L);
		else						writeBit(pc.getCurrentWeapon());
		
		writeC(0x28); 		// direction
		writeC(pc.getMoveState().getHeading());
		
		writeC(0x30); 		// lightRadius
		if(pc.getLight() == null)
			writeC(0x00);
		else
			writeBit(pc.getLight().getOwnLightSize());
		
		writeC(0x38); 		// objectcount
		writeC(0x01);
		
		writeC(0x40); 		// alignment(lawful)
		writeBit(pc.getLawful());
		
		writeC(0x4A);			// desc
		StringBuilder sb = new StringBuilder();
		sb.append(pc.getName());
		
		String name = sb.toString();
		writeC(name.getBytes().length);
		writeByte(name.getBytes());
		
		writeC(0x52); 		// title
		if (pc.getTitle().equals("")) {
			writeC(0);
		} else {
			writeC(pc.getTitle().getBytes().length);
			writeByte(pc.getTitle().getBytes());
		}
		
		writeC(0x58); 		// speed data
		writeC((pc.isHaste()) ? 1 : 0);
		
		writeC(0x60); 		// emotion
		if(pc.isBrave() || pc.isBloodLust()) writeC(0x01);
		else if(pc.isElfBrave())	writeC(0x03);
		else if(pc.isFastMovable() || pc.isUgdraFruit()) writeC(0x04);
		else						writeC(0x00);
		
		writeC(0x68);			// drunken
		writeBit(0x00);
		
		writeC(0x70); 		// isghost
		if (pc.isGhost()) {
			writeC(1);
		} else {
			writeC(0);
		}
		
		writeC(0x78); 		// isparalyzed
		writeC(pc.getParalysis() != null ? 1 : 0);
		
		writeBit(0x80);		// isuser
		writeC(0x01);
		
		writeBit(0x88);		// isinvisible
		writeBit((pc.isInvisble()) || (pc.isGmInvis()) ? 1L : 0L);
		
		writeBit(0x90); 		// ispoisoned
		writeC(pc.getPoison() != null ? 1 : 0);
		
		writeBit(0x98);		// emblemid
	      L1Clan clan = pc.getClan();
	        if (clan != null) {
	            writeBit((long) clan.getmarkon());
	        } else {
	            writeC(0);
	        }
	        
	    writeBit(0xA2);
	    writeS2(pc.getClanname());
		
		writeBit(0xAA); 		// mastername
		writeC(0x00);
		
		writeBit(0xB0); 		// altitude
		writeC(0x00);
		
		writeBit(0xB8);		// hitratio
		if (pc.isInParty()) {
			writeC(100 * pc.getCurrentHp() / pc.getMaxHp());
		} else if (pc.get_DuelLine() != 0) {
			writeC(100 * pc.getCurrentHp() / pc.getMaxHp());
		} else {
			writeC(255);
			writeC(255);
			writeC(255);
			writeC(255);
			writeC(255);
			writeC(255);
			writeC(255);
			writeC(255);
			writeC(255);
			writeC(1);
		}
		
		writeBit(0xC0); 		// safelevel
		writeBit(pc.getLevel());
		
		writeBit(0xCA); 		// shop title
		if(pc.getShopChat() != null && pc.getShopChat().length > 0){
			writeC(pc.getShopChat().length);
			writeByte(pc.getShopChat());
		}else
			writeC(0x00);
		
		writeBit(0xD0); 		// weapon sprite
		writeBit(-1);
		
		writeBit(0xD8); 		// couplestate
		writeBit(0);
		
		writeBit(0xE0); 		// boundarylevelindex
		int value = 0;
		if (pc.getLevel() >= 80)
			value = 11;
		else if (pc.getLevel() >= 55)
			value = (pc.getLevel() - 25) / 5;
		else if (pc.getLevel() >= 52)
			value = 5;
		else if (pc.getLevel() >= 50)
			value = 4;
		else if (pc.getLevel() >= 15) {
			value = pc.getLevel() / 15;
		}
		writeBit(value);
		
		writeBit(0xE8); 		// weakelemental
		writeBit(0x00);
		
		writeBit(0xF0);		// manaratio
		if (pc.isInParty()) {
			writeC(100 * pc.getCurrentMp() / pc.getMaxMp());
		} else if (pc.get_DuelLine() != 0) {
			writeC(100 * pc.getCurrentMp() / pc.getMaxMp());
		} else {
			writeC(255);
			writeC(255);
			writeC(255);
			writeC(255);
			writeC(255);
			writeC(255);
			writeC(255);
			writeC(255);
			writeC(255);
			writeC(1);
		}
		
		writeBit(0xF8);		// botindex
		writeBit(0x00);
		
		writeBit(0x100); 		// home server no
		writeC(0x00);
		
		writeBit(0x108); 		// team_id
		writeBit(0); 	
		
		writeBit(0x118); 		// speed_value_flag
		writeC(0x00);
		
		writeBit(0x120);		// second_speed_type
		writeC(0x00);
		
		writeH(0x00);
		
		if (clan != null) {
			pc.sendPackets(new S_ACTION_UI(clan.getClanName(), pc.getClanRank()));
			pc.sendPackets(new S_ClanWindow(S_ClanWindow.혈마크띄우기, pc.getClan().getmarkon()), true);
		}
	}
	
	private void buildPacket(L1NpcInstance npc) {
		
		writeC(Opcodes.S_EXTENDED_PROTOBUF);
		writeH(SC_WORLD_PUT_OBJECT_NOTI);
		
		writeC(0x08);			// point
		writeBit(npc.getX(), npc.getY());
		
		writeC(0x10);			// objectnumber
		writeBit(npc.getId());
		
		writeC(0x18); 		// objectsprite
		writeBit(npc.getGfxId().getTempCharGfx());
		
		writeC(0x20); 		// action
		int spriteId = npc.getGfxId().getTempCharGfx();
		if (((npc.getNpcTemplate().is_doppel()) && (spriteId != 31)) || (spriteId == 727) || (spriteId == 985)
				|| (spriteId == 986) || (spriteId == 6632) || (spriteId == 6634) || (spriteId == 6636)
				|| (spriteId == 6638))
			writeBit(4L);
		else if (spriteId == 110)
			writeBit(24L);
		else if (spriteId == 111)
			writeBit(4L);
		else if ((spriteId == 51))
			writeBit(24L);
		else if (spriteId == 816)
			writeBit(20L);
		else
			writeBit(npc.getActionStatus());
		
		writeC(0x28); 		// direction
		writeC(npc.getMoveState().getHeading());
		
		writeC(0x30); 		// lightRadius
		if(npc.getLight() == null)
			writeC(0x00);
		else
			writeBit(npc.getLight().getOwnLightSize());
		
		writeC(0x38); 		// objectcount
		writeC(0x01);
		
		writeC(0x40); 		// alignment(lawful)
		writeBit(npc.getLawful());
		
		writeC(0x4A);			// desc
		writeS2(npc.getNameId());
		
		writeC(0x52); 		// title
		if ((npc instanceof L1FieldObjectInstance)) {
			L1NpcTalkData talkdata = NPCTalkDataTable.getInstance().getTemplate(npc.getNpcTemplate().get_npcId());
			if (talkdata != null) 	writeS2(talkdata.getNormalAction());
			 else					writeC(0);
		} else if ((npc instanceof L1SignboardInstance)) 
			writeS2(npc.getNameId());
		 else if ((npc.getTitle() != null) && (!npc.getTitle().isEmpty())) 
			writeS2(npc.getTitle());
		 else 
			writeC(0);
		
		writeC(0x58); 			// speed data
		writeBit(npc.getMoveState().getMoveSpeed());
		
		writeC(0x60); 			// emotion
		writeBit(npc.getMoveState().getBraveSpeed());
		
		writeC(0x68);			// drunken
		writeBit(0x00);
		
		writeC(0x70); 		// isghost
		writeC(0);
		
		writeC(0x78); 		// isparalyzed
		writeC(npc.getParalysis() != null ? 1 : 0);
		
		writeBit(0x80);		// isuser
		writeC(0x00);
		
		writeBit(0x88);		// isinvisible
		writeBit((npc.isInvisble()) ? 1L : 0L);
		
		writeBit(0x90); 		// ispoisoned
		writeC(npc.getPoison() != null ? 1 : 0);
		
		writeBit(0x98);		// emblemid
		writeBit(0L);
		
		writeBit(0xA2); 		// pledgename
		writeC(0);

		writeBit(0xAA); 		// mastername
		if (npc.getMaster() != null) 	writeS2(npc.getMaster().getName());
		else							writeC(0);
		
		writeBit(0xB0); 		// altitude
		writeBit(0x5);
		
		writeBit(0xB8);		// hitratio
		writeBit(-1);
		
		writeBit(0xC0); 		// safelevel
		writeBit(npc.getLevel());
		
		writeBit(0xCA); 		// shop title
		writeC(0x00);
		
		writeBit(0xD0); 		// weapon sprite
		writeBit(1);
		
		writeBit(0xD8); 		// couplestate
		writeBit(0);
		
		writeBit(0xE0); 		// boundarylevelindex
		writeBit(0x00);
		
		writeBit(0xE8); 		// weakelemental
		writeBit(npc.getNpcTemplate().get_weakAttr());
		
		writeBit(0xF0);			// manaratio
		writeBit(npc.getCurrentMp());
		
		writeBit(0xF8);			// botindex
		writeBit(0x00);
		
		writeBit(0x100); 		// home server no
		writeC(0x00);
		
		writeBit(0x108); 		// team_id
		writeBit(0); 	
		
		writeBit(0x118); 		// speed_value_flag
		writeC(0x00);
		
		writeBit(0x120);		// second_speed_type
		writeC(0x00);
		
		writeH(0x00);
	}
	
	@Override
	public byte[] getContent() {
		if (this._byte == null) {
			this._byte = this._bao.toByteArray();
		}
		return this._byte;
	}

	@Override
	public String getType() {
		return "S_WorldPutObject";
	}
}
