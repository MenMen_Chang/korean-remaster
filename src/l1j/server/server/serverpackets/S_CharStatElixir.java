/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package l1j.server.server.serverpackets;

import l1j.server.server.Opcodes;
import l1j.server.server.model.Instance.L1PcInstance;

public class S_CharStatElixir extends ServerBasePacket {
	
 private static final String S_CharStatElixir = "[S] S_CharStatElixir";

 private byte[] _byte = null;

 /*
  * Ctrl + A 케릭 정보창 엘릭서 수량표기 By. 푼치
  */
 public S_CharStatElixir(L1PcInstance pc) {
  writeC(Opcodes.S_EXTENDED_PROTOBUF);  //  S_OPCODE_ACTION_UI
  writeC(0xe9);
  writeC(0x01);
  writeC(0x08);
  writeC(pc.getAbility().getElixirCount());
  writeH(0);
 }

 @Override
 public byte[] getContent() {
  if (_byte == null) {
   _byte = getBytes();
  }
  return _byte;
 }

 @Override
 public String getType() {
  return S_CharStatElixir;
 }
}
