package l1j.server.server.serverpackets;

import kr.PeterMonk.DogFightSystem.DogFightController;
import l1j.server.server.Opcodes;

public class S_FightBoard extends ServerBasePacket {

	private static final String S_RaceBoard = "[C] S_RaceBoard";

	public S_FightBoard(int number) {
		buildPacket(number);
	}

	private void buildPacket(int number) {
	writeC(Opcodes.S_HYPERTEXT);
	writeD(number);
	writeS("maeno4");
	writeC(0);                        
	writeH(15);
	
	for( int i = 0; i < 2; ++i ) {
		writeS(DogFightController.getInstance()._dogfight[i].getName()); 				//���� �̸�
		writeS(DogFightController.getInstance()._FightCondition[i]); 					//����
		writeS(Double.toString(DogFightController.getInstance()._winRate[i]) + "%"); 	//�·�
	}

	writeS("");
	writeS("");
	writeS("");
	writeS("");
	writeS("");
	writeS("");
	writeS("");
	writeS("");
	writeS("");
}

	@Override
	public byte[] getContent() {
		return getBytes();
	}

	public String getType() {
		return S_RaceBoard;
	}
}

