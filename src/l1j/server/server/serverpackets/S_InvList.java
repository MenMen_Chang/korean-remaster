/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307, USA.
 * 
 * http://www.gnu.org/copyleft/gpl.html
 */

package l1j.server.server.serverpackets;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;

import l1j.server.server.Opcodes;
import l1j.server.server.datatables.PetTable;
import l1j.server.server.model.ItemClientCode;
import l1j.server.server.model.Instance.L1ItemInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.model.item.L1ItemId;
import l1j.server.server.templates.L1Item;
import l1j.server.server.templates.L1Pet;
import l1j.server.server.utils.BinaryOutputStream;

// Referenced classes of package l1j.server.server.serverpackets:
// ServerBasePacket

public class S_InvList extends ServerBasePacket {

	private static final String S_INV_LIST = "[S] S_InvList";

	/**
	 * 목록에 아이템을 복수개 정리해 추가한다.
	 */

	public S_InvList(L1PcInstance pc) {
		if (pc.getInventory().checkItem(6013)) {
			pc.getInventory().consumeItem(6013, pc.getInventory().countItems(6013));
		}
		if (pc.getInventory().checkItem(61013)) {
			pc.getInventory().consumeItem(61013, pc.getInventory().countItems(61013));
		}
		if (pc.getInventory().checkItem(6014)) {
			pc.getInventory().consumeItem(6014, pc.getInventory().countItems(6014));
		}
		if (pc.getInventory().checkItem(60512)) {
			pc.getInventory().consumeItem(60512, pc.getInventory().countItems(60512));
		}
		if (pc.getInventory().checkItem(60513)) {
			pc.getInventory().consumeItem(60513, pc.getInventory().countItems(60513));
		}
		if (pc.getInventory().checkItem(7236)) {
			L1ItemInstance item = pc.getInventory().checkEquippedItem(7236);
			if (item != null) {
				pc.getInventory().setEquipped(item, false, false, false);
			}
			pc.getInventory().consumeItem(7236, pc.getInventory().countItems(7236));
		}

		List<L1ItemInstance> items = pc.getInventory().getItems();
		long currentTimeMillis = System.currentTimeMillis();
		for (L1ItemInstance item : items) {
			if (item.getItemId() == 20082) {
				if (item.getEndTime() == null) {
					Timestamp deleteTime = null;
					deleteTime = new Timestamp(System.currentTimeMillis() + (3600000 * 24 * 7));// 3일
					item.setEndTime(deleteTime);
				}
			}

			if (item.getItemId() == L1ItemId.DRAGON_KEY) {
				if (System.currentTimeMillis() > item.getEndTime().getTime()) {
					pc.getInventory().deleteItem(item);
				}
			}
		}

		writeC(Opcodes.S_EXTENDED_PROTOBUF);
		writeC(0x4c);writeC(0x02);
		byte s[] = null;
		L1Item temp = null;
		BinaryOutputStream detail;
		for (L1ItemInstance item : items) {
			detail = new BinaryOutputStream();
			temp = item.getItem();
			detail.writeC(0x08);
			detail.writeBit(item.getId());
			detail.writeC(0x10);
			detail.writeBit(temp.getItemDescId() == 0 ? -1 : temp.getItemDescId());
			detail.writeC(0x18);
			detail.writeBit(item.getItemId());
			detail.writeC(0x20);
			detail.writeBit(item.getCount());
			if(temp.getUseType() > 0){
				detail.writeC(0x28);
				detail.writeBit(temp.getUseType());
			}
			detail.writeC(0x38);
			detail.writeBit(item.get_gfxid());
			detail.writeC(0x40);
			detail.writeBit(item.getBless());
			detail.writeC(0x48);
			detail.writeBit(item.getStatusBit());
			detail.writeC(0x58);
			detail.writeBit(0);
			detail.writeC(0x68);
			detail.writeBit(item.getEnchantLevel());
			detail.writeC(0x70);
			detail.writeBit(item.getTradeBit());
			if(temp.getType2() == 1 &&item.getAttrEnchantLevel() > 0){
				int attrlevel = item.getAttrEnchantLevel();
				int attrtype = 0;
				if(attrlevel >=1 && attrlevel <=5) attrtype = 1;
				else 	if(attrlevel >=6 && attrlevel <=10) attrtype = 2;
				else 	if(attrlevel >=11 && attrlevel <=15) attrtype = 3;
				else 	if(attrlevel >=16 && attrlevel <=20) attrtype = 4;
				detail.writeBit(128);
				detail.writeBit(attrtype);
				detail.writeBit(136);
				detail.writeBit(attrtype == 1 ? attrlevel : attrlevel - (5 * (attrtype -1)));
			}
			
			detail.writeBit(146);
			detail.writeS3(item.getViewName());
			
			if(item.isIdentified()){
				detail.writeBit(154);
				s =item.getStatusBytes();
				detail.writeC(s.length);
				for(byte b : s)
					detail.writeC(b);
			}

			L1Pet l1pet = PetTable.getTemplate(item.getId());
			if (l1pet != null) {
				detail.writeBit(170L);
				byte[] petinfo = petItemInfo(item, l1pet);
				detail.writeBit(petinfo.length);
				detail.writeByte(petinfo);
			}
			
			writeC(0x0a);
			writeBit(detail.getLength() -2);
			writeByte(detail.getBytes());
			
			detail.reset();
		}
		
		writeC(0x10);
		writeC(0x01);
		writeH(0);
	}
	public int getItemStatus(L1ItemInstance item) {
		int b=0;
		if(item.isIdentified())
			b = 1;
		if(!item.getItem().isTradable()){
			b |= 2;
//			if(item.getItem()){ //창고
//				b |= 16;
//			}
		}
		
		if(item.getItem().isCantDelete())
			b |= 4;
		if(item.getItem().get_safeenchant() < 0)
			b |= 8;
		
		int bless = item.getBless();
		if(bless >= 128 && bless <= 131){
			b |= 2;
			b |= 4;
			b |= 8;
			b |= 32;
		}else if(bless > 131){
			b |= 64;
		}
		
		
		if(item.getItem().isStackable())
			b |= 128;
		return b;
	}
	
	public byte[] petItemInfo(L1ItemInstance item, L1Pet l1pet) {
		BinaryOutputStream os = new BinaryOutputStream();
		if (l1pet != null) {
			os.writeC(8);
			os.writeBit(l1pet.isProduct() ? 1L : 0L);

			os.writeC(16);
			os.writeBit(l1pet.isPetDead() ? 1L : 0L);

			os.writeC(24);
			os.writeBit(0L);

			os.writeC(32);
			os.writeBit(l1pet.getPetInfo());

			os.writeC(40);
			os.writeBit(l1pet.getLevel());

			os.writeC(50);
			os.writeBit(l1pet.getName().getBytes().length);
			os.writeByte(l1pet.getName().getBytes());

			os.writeC(56);
			os.writeBit(l1pet.getElixir());
		}
		try {
			os.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return os.getBytes();
	}
	@Override
	public byte[] getContent() {
		return _bao.toByteArray();
	}

	@Override
	public String getType() {
		return S_INV_LIST;
	}
}
