/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307, USA.
 * 
 * http://www.gnu.org/copyleft/gpl.html
 */

package l1j.server.server.serverpackets;

import java.io.IOException;

import l1j.server.server.Opcodes;
import l1j.server.server.datatables.PetTable;
import l1j.server.server.model.ItemClientCode;
import l1j.server.server.model.Instance.L1ItemInstance;
import l1j.server.server.templates.L1Item;
import l1j.server.server.templates.L1Pet;
import l1j.server.server.utils.BinaryOutputStream;

// Referenced classes of package l1j.server.server.serverpackets:
// ServerBasePacket

public class S_AddItem extends ServerBasePacket {

	private static final String S_ADD_ITEM = "[S] S_AddItem";

	String s = "00 00 EB 00 01 01 00 00 00 81 "
			+ "24 35 30 38 00 06 17 14 02 00 00 00 12 00 00 00 "
			+ "00 00 00 DF F8 4F 96 00 00 00 00 00 00 00 00";

	/**
	 * 목록에 아이템을 1개 추가한다.
	 */
	public S_AddItem(L1ItemInstance item) {
		
		writeC(Opcodes.S_EXTENDED_PROTOBUF);
		writeC(0x4c);writeC(0x02);
		
		BinaryOutputStream detail = new BinaryOutputStream();
		L1Item temp = item.getItem();
		detail.writeC(0x08);
		detail.writeBit(item.getId());
		detail.writeC(0x10);
		detail.writeBit(temp.getItemDescId() == 0 ? -1 : temp.getItemDescId());
		detail.writeC(0x18);
		detail.writeBit(item.getItemId());
		detail.writeC(0x20);
		detail.writeBit(item.getCount());
		if(temp.getUseType() > 0){
			detail.writeC(0x28);
			detail.writeBit(temp.getUseType());
		}
		detail.writeC(0x38);
		detail.writeBit(item.get_gfxid());
		detail.writeC(0x40);
		detail.writeBit(item.getBless());
		detail.writeC(0x48);
		detail.writeBit(item.getStatusBit());
//		detail.writeC(0x50);
//		detail.writeBit(item.getTypeBit());
		detail.writeC(0x58);
		detail.writeBit(0);
		detail.writeC(0x68);
		detail.writeBit(item.getEnchantLevel());
		detail.writeC(0x70);
		detail.writeBit(item.getTradeBit());
		if(temp.getType2() == 1 &&item.getAttrEnchantLevel() > 0){
			int attrlevel = item.getAttrEnchantLevel();
			int attrtype = 0;
			if(attrlevel >=1 && attrlevel <=5) attrtype = 1;
			else 	if(attrlevel >=6 && attrlevel <=10) attrtype = 2;
			else 	if(attrlevel >=11 && attrlevel <=15) attrtype = 3;
			else 	if(attrlevel >=16 && attrlevel <=20) attrtype = 4;
			detail.writeBit(128);
			detail.writeBit(attrtype);
			detail.writeBit(136);
			detail.writeBit(attrtype == 1 ? attrlevel : attrlevel - (5 * (attrtype -1)));
		}
		
		detail.writeBit(146);
		detail.writeS3(item.getViewName());
		
		if(item.isIdentified()){
			detail.writeBit(154);
			byte[] s =item.getStatusBytes();
			detail.writeBit(s.length);
			for(byte b : s)
				detail.writeC(b);
		}
		
		L1Pet l1pet = PetTable.getTemplate(item.getId());
		if (l1pet != null) {
			detail.writeBit(170L);
			byte[] petinfo = petItemInfo(item, l1pet);
			detail.writeBit(petinfo.length);
			detail.writeByte(petinfo);
		}
		
		writeC(0x0a);
		writeBit(detail.getLength() -2);
		writeByte(detail.getBytes());
		writeH(0);
		
		try {
			detail.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public byte[] petItemInfo(L1ItemInstance item, L1Pet l1pet) {
		BinaryOutputStream os = new BinaryOutputStream();
		if (l1pet != null) {
			os.writeC(8);
			os.writeBit(l1pet.isProduct() ? 1L : 0L);

			os.writeC(16);
			os.writeBit(l1pet.isPetDead() ? 1L : 0L);

			os.writeC(24);
			os.writeBit(0L);

			os.writeC(32);
			os.writeBit(l1pet.getPetInfo());

			os.writeC(40);
			os.writeBit(l1pet.getLevel());

			os.writeC(50);
			os.writeBit(l1pet.getName().getBytes().length);
			os.writeByte(l1pet.getName().getBytes());

			os.writeC(56);
			os.writeBit(l1pet.getElixir());
		}
		try {
			os.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return os.getBytes();
	}

	@Override
	public byte[] getContent() {
		return _bao.toByteArray();
	}

	@Override
	public String getType() {
		return S_ADD_ITEM;
	}
}
