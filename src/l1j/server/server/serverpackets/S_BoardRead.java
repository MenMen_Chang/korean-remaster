/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */

package l1j.server.server.serverpackets;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import l1j.server.L1DatabaseFactory;
import l1j.server.server.Opcodes;
import l1j.server.server.model.Instance.L1NpcInstance;
import l1j.server.server.utils.SQLUtil;

public class S_BoardRead extends ServerBasePacket {

	private static final String S_BoardRead = "[S] S_BoardRead";

	private static Logger _log = Logger.getLogger(S_BoardRead.class.getName());

	private byte[] _byte = null;

	public S_BoardRead(L1NpcInstance board, int number) {
		if (board.getNpcId() == 4200020)
			buildPacketUser(board, number);
		else if (board.getNpcId() == 4200016)
			buildPacketHunt(board, number);
		else if (board.getNpcId() == 4500302)
			buildPacketfree(board, number);
		else
			buildPacket(board, number);
	}
	public S_BoardRead(L1NpcInstance board, int number, int step) {
		buildPacketGM(board, number, 0);
	}
	private void buildPacketfree(L1NpcInstance board, int number) {
		Connection con = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("SELECT * FROM board_free WHERE id=?");
			pstm.setInt(1, number);
			rs = pstm.executeQuery();
			while (rs.next()) {
				writeC(Opcodes.S_BOARD_READ);
				// if(board.getNpcId() == 4500302){
				// writeD(number-100);
				// }else{
				writeD(number);
				// }
				writeS(rs.getString(2));
				writeS(rs.getString(4));
				writeS(rs.getString(3));
				writeS(rs.getString(5));
			}
		} catch (SQLException e) {
			_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
		} finally {
			SQLUtil.close(rs);
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	private void buildPacket(L1NpcInstance board, int number) {
		Connection con = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("SELECT * FROM board WHERE id=? AND board_id=?");
			pstm.setInt(1, number);
			pstm.setInt(2, board.getNpcId());
			rs = pstm.executeQuery();
			while (rs.next()) {
				writeC(Opcodes.S_BOARD_READ);
				// if(board.getNpcId() == 4500302){
				// writeD(number-100);
				// }else{
				writeD(number);
				// }
				writeS(rs.getString(2));
				writeS(rs.getString(4));
				writeS(rs.getString(3));
				writeS(rs.getString(5));
			}
		} catch (SQLException e) {
			_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
		} finally {
			SQLUtil.close(rs);
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	private void buildPacketUser(L1NpcInstance board, int number) {
		Connection con = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("SELECT * FROM board_user WHERE id=?");
			pstm.setInt(1, number);
			rs = pstm.executeQuery();
			while (rs.next()) {
				writeC(Opcodes.S_BOARD_READ);
				writeD(number);
				writeS(rs.getString(2));
				writeS(rs.getString(4));
				writeS(rs.getString(3));
				writeS("판매금액: "+rs.getString(4)+"아데나\n\n받을금액: "+rs.getString(7)+"원\n\n입금계좌:\n 110 275 348653(신한) 신선아"
						+ "\n\n\n\n\n※거래 완료시 거래금액의 10%로 수수료가 발생합니다. 판매금액 정산은 실시간으로 진행되오며, 정산시 수수료가 차감되어 송금됩니다. 해당 수수료는 서버 운영기금의 일환으로 사용됩니다.");
			}
		} catch (SQLException e) {
			_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
		} finally {
			SQLUtil.close(rs);
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}
	
	private void buildPacketGM(L1NpcInstance board, int number, int step) {
		Connection con = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("SELECT * FROM board_user WHERE id=?");
			pstm.setInt(1, number);
			rs = pstm.executeQuery();
			while (rs.next()) {
				writeC(Opcodes.S_BOARD_READ);
				writeD(number);
				writeS(rs.getString(2));
				writeS(rs.getString(4));
				writeS(rs.getString(3));
				writeS("판매금액: "+rs.getString(4)+"아데나\n\n받을금액: "+rs.getString(7)+
						"원\n\n입금계좌: "+rs.getString(8)+"\n\n은행명: " +rs.getString(9)+
						"\n\n계좌주이름: "+rs.getString(10)+"\n\n입금 대기자 케릭명: "+rs.getString(11)+"\n\n입금 대기자명: "+rs.getString(12));
			}
		} catch (SQLException e) {
			_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
		} finally {
			SQLUtil.close(rs);
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	private void buildPacketHunt(L1NpcInstance board, int number) {
		writeC(Opcodes.S_BOARD_READ);
		writeD(number);
		Connection c = null;
		PreparedStatement p = null;
		ResultSet r = null;
		try {
			c = L1DatabaseFactory.getInstance().getConnection();
			p = c.prepareStatement("select * from characters where HuntCount=1 and objid=?");
			p.setInt(1, number);
			r = p.executeQuery();
			if (r.next()) {
				writeS("서버");
				writeS("수배 : " + r.getString(3));
				writeS("");
				StringBuffer sb = new StringBuffer();
				sb.append("\r\n\r\n");
				sb.append("수배 캐릭명 : ").append(r.getString(3))
						.append("\r\n\r\n");
				sb.append(" 이유 : \r\n\r\n  ").append(r.getString(60))
						.append("\r\n\r\n");
				writeS(sb.toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(r);
			SQLUtil.close(p);
			SQLUtil.close(c);
		}
	}

	@Override
	public byte[] getContent() {
		if (_byte == null) {
			_byte = getBytes();
		}
		return _byte;
	}

	@Override
	public String getType() {
		return S_BoardRead;
	}
}
