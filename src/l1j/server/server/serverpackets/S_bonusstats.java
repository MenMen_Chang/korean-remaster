/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */

package l1j.server.server.serverpackets;

import l1j.server.server.Opcodes;

// Referenced classes of package l1j.server.server.serverpackets:
// ServerBasePacket

public class S_bonusstats extends ServerBasePacket {

	private byte[] _byte = null;

	public S_bonusstats(int i, int j) {
		buildPacket(i, j);
	}

	// 0000: 77 00 00 7c 0e 00 00 df 01 31 00 w..|.....1.
	private void buildPacket(int i, int j) {
		int 한자리수 = 0;
		int 두자리수 = 0;
		if (j >= 10) {
			한자리수 = (j / 10 + 48);
			두자리수 = (j % 10) + 48;
		} else {
			한자리수 = j + 48;
		}
		writeC(Opcodes.S_ASK);
		writeH(0);
		writeD(0x00000e7c);
		writeH(0x01df);
		writeS(Integer.toString(j));
		// writeC(한자리수);
		// writeC(두자리수);
	}

	@Override
	public byte[] getContent() {
		if (_byte == null) {
			_byte = getBytes();
		}

		return _byte;
	}

	@Override
	public String getType() {
		return "[S] S_bonusstats";
	}
}
