package l1j.server.server.serverpackets;

import l1j.server.server.Opcodes;

public class S_UserCommands extends ServerBasePacket {

	private static final String S_UserCommands = "[C] S_UserCommands";

	// private static Logger _log =
	// Logger.getLogger(S_UserCommands.class.getName());

	private byte[] _byte = null;

	public S_UserCommands(int number) {
		buildPacket(number);
	}

	private void buildPacket(int number) {
		writeC(Opcodes.S_BOARD_READ);
		writeD(number);// 넘버
		writeS("");// 글쓴이?
		writeS(" 명령어 설명 ");// 날짜?
		writeS("");// 제목?
		writeS("[====================] [==]\n"  
				+ "[ .무인상점(종료후 상점유지)  ]\n"
				+ "[ .텔렉풀기(캐릭터 낑김풀림)  ]\n"
				+ "[ .혈맹파티(혈맹원 일괄파티)  ]\n"
				+ "[ .비번변경(계정비번 변경)     ]\n"
				+ "[ .퀴즈설정(계정 보안관리)     ]\n" 
				+ "[ .나이(혈맹채팅 나이표기)     ] \n" 
				+ "[====================] [==] ");

	}

	@Override
	public byte[] getContent() {
		if (_byte == null) {
			_byte = getBytes();
		}
		return _byte;
	}

	public String getType() {
		return S_UserCommands;
	}
}
