package l1j.server.server.serverpackets;

import l1j.server.server.Opcodes;

public class S_DRAGONPERL extends ServerBasePacket {

	public S_DRAGONPERL(int objecId, int type) {
		writeC(Opcodes.S_DRUNKEN);
		writeD(objecId); // 케릭터 객체 아이디
		writeC(type); // 1~7 술취한 효과 8 드진
	}

	@Override
	public byte[] getContent() {
		return getBytes();
	}

	@Override
	public String getType() {
		return S_DRAGONPERL;
	}
	
	private static final String S_DRAGONPERL = "[S] S_DRUNKEN";
}
