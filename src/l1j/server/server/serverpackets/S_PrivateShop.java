/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */

package l1j.server.server.serverpackets;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import l1j.server.server.Opcodes;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1ItemInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.templates.L1Item;
import l1j.server.server.templates.L1PrivateShopBuyList;
import l1j.server.server.templates.L1PrivateShopSellList;
import l1j.server.server.utils.BinaryOutputStream;

// Referenced classes of package l1j.server.server.serverpackets:
// ServerBasePacket

public class S_PrivateShop extends ServerBasePacket {

	public S_PrivateShop(L1PcInstance pc, int objectId, int type) {
		try {
			L1PcInstance shopPc = (L1PcInstance) L1World.getInstance()
					.findObject(objectId);

			if (shopPc == null) {
				return;
			}

			if (!pc.isGm()
					&& pc.getAccountName().equalsIgnoreCase(
							shopPc.getAccountName())) {
				return;
			}
			/*
			 * if (type == 0) { ArrayList<?> list = shopPc.getSellList(); int
			 * size = list.size(); if(size <= 0) return; } else
			 */if (type == 1) {
				ArrayList<?> list = shopPc.getBuyList();
				int size = list.size();
				if (size <= 0)
					return;
			}
			 writeC(Opcodes.S_EXTENDED_PROTOBUF);
			 writeC(0x07);
			 writeC(0x04);
			 
			 writeC(0x08);
			 writeC(type);
			 writeC(0x10);
			 writeBit(shopPc.getId());

			if (type == 0) {
				ArrayList<?> list = shopPc.getSellList();
				int size = list.size();
				pc.setPartnersPrivateShopItemCount(size);
				writeC(0x18);
				writeBit(size);
				L1PrivateShopSellList pssl = null;
				L1ItemInstance item = null;
				byte[] b;
				BinaryOutputStream detail;
				L1Item temp;
				for (int i = 0; i < size; i++) {
					pssl = (L1PrivateShopSellList) list.get(i);
					int itemObjectId = pssl.getItemObjectId();
					int count = pssl.getSellTotalCount() - pssl.getSellCount();
					int price = pssl.getSellPrice();
					item = shopPc.getInventory().getItem(itemObjectId);
					if (item != null) {
						detail = new BinaryOutputStream();
						temp = item.getItem();
						detail.writeC(0x08);
						detail.writeBit(item.getId());
						detail.writeC(0x10);
						detail.writeBit(temp.getItemDescId() == 0 ? -1 : temp.getItemDescId());
						detail.writeC(0x20);
						detail.writeBit(count);
						if(temp.getUseType() > 0){
							detail.writeC(0x28);
							detail.writeBit(temp.getUseType());
						}
						detail.writeC(0x38);
						detail.writeBit(item.get_gfxid());
						detail.writeC(0x40);
						detail.writeC(item.getBless());
						detail.writeC(0x48);
						detail.writeBit(item.getStatusBit());
						detail.writeC(0x58);
						detail.writeBit(0);
						detail.writeC(0x68);
						detail.writeBit(item.getEnchantLevel());
						detail.writeC(0x70);
						detail.writeBit(item.getTradeBit());
						if(temp.getType2() == 1 &&item.getAttrEnchantLevel() > 0){
							int attrlevel = item.getAttrEnchantLevel();
							int attrtype = 0;
							if(attrlevel >=1 && attrlevel <=5) attrtype = 1;
							else 	if(attrlevel >=6 && attrlevel <=10) attrtype = 2;
							else 	if(attrlevel >=11 && attrlevel <=15) attrtype = 3;
							else 	if(attrlevel >=16 && attrlevel <=20) attrtype = 4;
							detail.writeBit(128);
							detail.writeBit(attrtype);
							detail.writeBit(136);
							detail.writeBit(attrtype == 1 ? attrlevel : attrlevel - (5 * (attrtype -1)));
						}
						
						detail.writeBit(146);
						detail.writeS3(item.getNumberedName(count));
						
						if(item.isIdentified()){
							detail.writeBit(154);
							b =item.getStatusBytes();
							detail.writeBit(b.length);
							for(byte s : b)
								detail.writeC(s);
						}
						
						int bsize = getBitSize(detail.getLength() - 2) + getBitSize(i) +
								getBitSize(count) + getBitSize(price)
								+ detail.getLength() + 2;
						writeC(0x22);
						writeBit(bsize);
						writeC(0x08);
						writeBit(i);
						writeC(0x10);
						writeBit(count);
						writeC(0x18);
						writeBit(price);
						
						writeC(0x22);
						writeBit(detail.getLength() - 2);
						writeByte(detail.getBytes());
						
					}
				}
				writeH(0x00);
			} else if (type == 1) {
				ArrayList<L1PrivateShopBuyList> list = shopPc.getBuyList();
				Map<Integer, L1PrivateShopBuyList> havelist = new HashMap<Integer, L1PrivateShopBuyList>();
				// ArrayList<L1ItemInstance> pchavelist = new
				// ArrayList<L1ItemInstance>();

				L1PrivateShopBuyList psbl = null;
				L1ItemInstance item = null;
				int havesize = 0;
				int havelistsize = 0;
				int tempi = 0;
				for (L1PrivateShopBuyList psb : list) {
					item = shopPc.getInventory().getItem(psb.getItemObjectId());
					for (L1ItemInstance pcItem : pc.getInventory().getItems()) {
						if (!pcItem.isEquipped()
								&& item.getItemId() == pcItem.getItemId()
								&& item.getEnchantLevel() == pcItem
										.getEnchantLevel()
								&& item.getAttrEnchantLevel() == pcItem
										.getAttrEnchantLevel()
								&& item.getBless() == pcItem.getBless()) {
							// pchavelist.add(pcItem);
							// System.out.println(havesize+" : "
							// +item.getName());
							havesize++;
						}
					}

					if (pc.getInventory().CheckSellPrivateShopItem(
							item.getItemId(), item.getEnchantLevel(),
							item.getAttrEnchantLevel(), item.getBless())) {
						havelist.put(havelistsize, psb);
						havelistsize++;
					}
				}

				// int size = havelist.size();

				writeH(havesize);
				if (havesize <= 0)
					return;

				for (int i = 0; i < havelistsize; i++) {
					psbl = havelist.get(i);
					int itemObjectId = psbl.getItemObjectId();
					int count = psbl.getBuyTotalCount();
					int price = psbl.getBuyPrice();
					item = shopPc.getInventory().getItem(itemObjectId);
					// System.out.println(havesize+" : " +item.getName());
					for (L1ItemInstance pcItem : pc.getInventory().getItems()) {
						try {
							if (!pcItem.isEquipped()
									&& item.getItemId() == pcItem.getItemId()
									&& item.getEnchantLevel() == pcItem
											.getEnchantLevel()
									&& item.getAttrEnchantLevel() == pcItem
											.getAttrEnchantLevel()
									&& item.getBless() == pcItem.getBless()) {
								writeC(i + tempi);
								writeD(count);
								writeD(price);
								writeD(pcItem.getId());
								writeC(0x00);
								tempi++;
								/*
								 * writeD(pcItem.getId()); writeD(count);
								 * writeD(price);
								 */
							}
						} catch (Exception e) {
							if (shopPc != null)
								System.out
										.println("유저 상점 오류 좌표 x :"
												+ shopPc.getX() + " y :"
												+ shopPc.getY() + " m :"
												+ shopPc.getMapId() + " 아이템 ="
												+ item != null ? item.getName()
												: "NULL");
						}
					}
				}
				writeH(0x00);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public byte[] getContent() {
		return getBytes();
	}
}
