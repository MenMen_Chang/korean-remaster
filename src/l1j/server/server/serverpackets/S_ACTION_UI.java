package l1j.server.server.serverpackets;

import l1j.server.Config;
import l1j.server.GameSystem.EventShop.EventShop_아인_따스한_시선;
import l1j.server.server.Opcodes;
import l1j.server.server.model.Instance.L1ItemInstance;
import l1j.server.server.model.Instance.L1NpcInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.model.skill.L1SkillId;

public class S_ACTION_UI extends ServerBasePacket {

	private byte[] _byte = null;

	public static final int TAM = 0xc2;
	public static final int CRAFT_ITEM = 0x37; // 제작 아이템
	public static final int CRAFT_ITEMLIST = 0x39; // 제작 리스트
	public static final int CRAFT_OK = 0x3b; // 제작 완료
	public static final int CLAN_JOIN_MESSAGE = 0x43;
	public static final int TEST = 0xcc;
	public static final int TEST2 = 0x3A;
	public static final int DOLL_MAKING = 0x7b; // 인형합성창 띄우기.
	public static final int DOLL_MAKING_RESULT = 0x7d; // 인형합성 결과.

	public static final int CHAR_REGIST_NOTICE = 1015;
	public static final int EINHASAD = 1020;
	
	public static final int 신스킬 = 0x6e;

	public static final int PCBANG_SET = 0x7e;

	private static final String S_ACTION_UI = "S_ACTION_UI";

	public S_ACTION_UI(int skillid, int time, int gfxid, int msg) {
		writeC(Opcodes.S_EXTENDED_PROTOBUF);
		writeC(0x6e);
		writeC(0x00);
		writeC(0x08);
		writeC(0x02);
		writeC(0x10);
		write7B(skillid);
		writeC(0x18);
		write7B(time);
		writeC(0x20);
		int duration_show_type = 8;
		if (skillid == L1SkillId.CLAN_BLESS_BUFF)
			duration_show_type = 10;
		writeC(duration_show_type);
		
		writeC(0x28);
		write7B(gfxid);
		writeC(0x30);
		writeC(0x00);
		writeC(0x38);
		writeC(0x03);
		writeC(0x40);
		write7B(msg);
		writeC(0x48);
		writeC(0x00);
		writeC(0x50);
		writeC(0x00);
		writeC(0x58);
		writeC(0x01);
		writeC(0x60);
		writeC(0x00);
		writeC(0x68);
		writeC(0x00);
		writeC(0x70);
		writeC(0x00);
		writeH(0x00);
	}

	/**
	 * 파티 표식 설정시 사용함.
	 */
	public S_ACTION_UI(byte[] flag) {
		writeC(Opcodes.S_EXTENDED_PROTOBUF);
		writeH(339);
		writeByte(flag);
		writeH(0);
	}

	public S_ACTION_UI(int subCode, int value1, int value2, int value3, int value4, boolean flag) {
		writeC(Opcodes.S_EXTENDED_PROTOBUF);
		writeC(110);
		writeC(0);
		writeC(8);
		if (!flag)
			writeC(3);
		else {
			writeC(2);
		}

		writeC(16);
		writeBit(value1);

		writeC(24);
		writeBit(value2);

		writeC(32);
		writeC(10);

		writeC(40);
		writeBit(value3);

		writeC(48);
		writeC(0);

		writeC(56);
		writeC(3);

		writeC(64);
		writeBit(value4);

		writeC(72);
		writeC(0);

		writeC(80);
		writeC(0);

		writeC(88);
		writeC(1);

		writeC(96);
		writeC(0);

		writeC(104);
		writeC(0);

		writeC(112);
		writeC(0);

		writeH(0);
	}

	public S_ACTION_UI(int type) {
		writeC(Opcodes.S_EXTENDED_PROTOBUF);
		writeH(type);
		switch (type) {
		case CRAFT_ITEM:
			writeH(0x08);
			writeC(0x08);
			writeC(0x03);
			break;
		case CRAFT_OK:
			writeH(0x08);
			break;
		}
		writeH(0x00);
	}

	public S_ACTION_UI(int type, Object... etc) {
		writeC(Opcodes.S_EXTENDED_PROTOBUF);
		writeH(type);
		switch (type) {
		case DOLL_MAKING:
			writeC(0x08);
			writeC(0x03);
			break;
		case DOLL_MAKING_RESULT:
			boolean success = (boolean)etc[0];
			L1ItemInstance doll_item = (L1ItemInstance)etc[1];
			writeC(0x08);						// 
			writeC(success ? 0 : 1);			// 0:성공, 1:실패
			writeC(0x12);						// 
			int size = getBitSize(doll_item.getId()) + getBitSize(doll_item.get_gfxid()) + getBitSize(doll_item.getBless()) + 5;
			writeC(size);
			writeC(0x08);						 
			write4bit(doll_item.getId());		// objId
			writeC(0x10);
			write4bit(doll_item.get_gfxid());	// inv_gfxzz
			writeC(0x18);
			write4bit(doll_item.getBless());	// bless
			break;
		}
		writeH(0);
	}

	/**
	 * @param PC방
	 *            설정 by 맞고
	 * @param isOpen
	 *            on/off
	 **/
	public S_ACTION_UI(int code, boolean isOpen) {
		writeC(Opcodes.S_EXTENDED_PROTOBUF);
		writeC(code);
		switch (code) {
		case PCBANG_SET: {
			writeC(0x00);
			writeC(0x08);
			writeC(isOpen ? 1 : 0);
			writeC(0x10);
			writeC(0x01); // 랭킹버튼활성화 패킷
			writeH(0);
			break;
		}
		}
	}

	public S_ACTION_UI(L1NpcInstance npc) {
		writeC(Opcodes.S_EXTENDED_PROTOBUF);
		writeH(CRAFT_ITEMLIST);
		writeH(0x08);
		int craftlist[] = null;
		try {
			switch (npc.getNpcId()) { // 69,70,71,72,73,74,75,76,77,78얇은판금 ~
										// 백금판금
			/****************************************************************************************************/
			/** 기란마을 - 란달 **/// 스냅퍼 946,952,958,964,970,976,982
			case 70028:
				craftlist = new int[] { 264, 265, 266, 267, 268, 269, 270 };
				break;
			/** 기란마을 - 허버트 **/
			case 70641:
				craftlist = new int[] {2228,2229,2230, 4934,4935,4936,4937,4938,4939,4940,4941,4942,4943,4944,4945,4946,4947,4948,4949,199, 200, 570, 571 };
				break;
			/** 기란마을 - 헥터 **/
			case 70642:
				craftlist = new int[] { 3043, 3044, 3045, 3296, 518, 519, 1963, 1964, 1965, 194, 195, 196, 819, 821,
						189, 190, 191, 192, 193, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78 };
				break;
			/** 기란마을 - 바무트 **/
			case 70690:
				craftlist = new int[] { 4564, 4565, 4566, 4567, 4568, 4569, 4570, 4571, 4572, 4573, 4574, 4575, 4576,
						4577, 4578, 4579, 4580, 4581, 4582, 4583, 3419, 2862, 255, 260, 261, 262, 263, 820, 481, 482,
						483, 484, 485, 486, 487, 488 };
				break;
			/** 기란마을 - 액세서리 - 금속 - **/
			case 101001:
				craftlist = new int[] { 3413, 3414, 3415, 3416, 3417, 3418, 2764, 2765, 2766, 2767, 2859, 2860, 2760,
						2761, 2762, 722, 723, 724, 725 };
				break;
			/** 기란마을 - 액세서리 - 보석 - **/
			case 101000:
				craftlist = new int[] { 3599, 3393, 3394, 3395, 3396, 3397, 3398, 3399, 3400, 3401, 3402, 3403, 3404,
						3405, 3406, 3407, 3456, 3457, 3458, 3459, 3460, 3461, 3462, 3463, 3464, 3465, 3466, 3467, 3468,
						3469, 3470, 2775, 2776, 2777, 2778, 712, 713, 1729, 1730, 1731, 1732, 1733, 1734, 1735, 1736,
						1737 };
				break;
			/** 기란마을 - 액세서리 - 천 - **/
			case 101002:
				craftlist = new int[] { 714, 715, 716, 717, 2878, 2863, 702, 703, 704, 705, 708, 709, 714, 715, 716,
						717 };
				break;
			/** 기란마을 - 액세서리 - 가죽 - **/
			case 101003:
				craftlist = new int[] { 706, 707, 710, 711 };
				break;
			/** 기란마을 - 액세서리 - 룸티스 - **/
			case 101027:
				craftlist = new int[] { 928, 929, 930, 931, 932, 933, 934, 935, 936, 937, 938, 939, 940, 941, 942, 943,
						944, 945, 1539, 1540, 1541, 1542, 1543, 1544, 2976, 2977, 2978, 2979, 2980, 2981, 2982, 2983,
						2984, 2985, 2986, 2987, 2988, 2989, 2990, 2991 };
				break;
			// 3203,3204,3205,3206,3208
			/** 기란마을 - 액세서리 - 스냅퍼 - **/
			case 101028:
				craftlist = new int[] { 946, 947, 948, 949, 950, 951, 952, 953, 954, 955, 956, 957, 976, 977, 978, 979,
						980, 981, 982, 983, 984, 985, 986, 987, 2948, 2949, 2950, 2951, 2952, 2953, 2954, 2955, 2956,
						2957, 2958, 2959, 2960, 2961, 2962, 2963, 2964, 2965, 2966, 2967, 2968, 2969, 2970, 2971, 2972, 2973, 2974, 2975 };
				break;
			/** 기란마을 - 엘릭서 - **/
			case 101035:
				craftlist = new int[] { 1043, 1044, 1045, 1046, 1047, 1048 };
				break;

			case 101136:
				craftlist = new int[] { 3503 };
				break;
			/****************************************************************************************************/
			/** 웰던마을 - 칼루아 **/
			case 100629:
				craftlist = new int[] { 157, 158 };
				break;
			/** 웰던마을 - 슈에르메 **/
			case 4212002:
				craftlist = new int[] { 103, 104, 105, 106, 107, 108, 109, 496, 497 };
				break;

			/** 웰던마을 - 세심한 슈누(마력) **/
			case 4212003:
				craftlist = new int[] { 83, 84, 85, 86, 2583, 2584, 2585, 2586, 2587, 2588, 2589, 2590, 2591, 2592,
						2593, 2594 };
				break;

			/** 웰던마을 - 끈질긴 도오호(인내력) **/
			case 4212004:
				craftlist = new int[] { 87, 88, 89, 90, 2595, 2596, 2597, 2598, 2599, 2600, 2601, 2602, 2603, 2604,
						2605, 2606 };
				break;

			/** 웰던마을 - 강인한 하이오스(완력) **/
			case 4212005:
				craftlist = new int[] { 79, 80, 81, 82, 2571, 2572, 2573, 2574, 2575, 2576, 2577, 2578, 2579, 2580,
						2581, 2582 };
				break;

			/** 웰던마을 - 찬란한 바에미(예지력) **/
			case 4212006:
				craftlist = new int[] { 91, 92, 93, 94, 2607, 2608, 2609, 2610, 2611, 2612, 2613, 2614, 2615, 2616,
						2617, 2618 };
				break;

			/** 웰던마을 - 이벨빈 **/
			case 70662:
				craftlist = new int[] {/* 2622, 2623, 2624, 2625, 2626, 2627, 
						2628, 2629, 2630, 2631, 2632, 2633, 2634, 2635, 2636, 
						2637, 2638, 2639, 2640, 2641, 2642, 2643, 2644, 2645, 
						2646, 2647, 2648, 2649, 2650, 2651,*/ 3715, 3716, 3717, 
						3718, 3719, 3720, 3721, 3722, 3723, 3724, 3728, 3729, 
						3730, 3731, 3732, 3733, 3734, 3735, 3736, 3737, 3741, 
						3742, 3743, 3744, 3745, 3746, 3747, 3748, 3749, 3750, 
						3754, 3755, 3756, 3757, 3758, 3759, 3760, 3761, 3762, 
						3763, 3767, 3768, 3769, 3770, 3771, 3772, 3773, 3774, 
						3775, 3776, 3780, 3781, 3782, 3783, 3784, 3785, 3786, 
						3787, 3788, 3789, 3793, 3794, 3795, 3796, 3797, 3798, 
						3799, 3800, 3801, 3802, 3806, 3807, 3808, 3809, 3810, 
						3811, 3812, 3813, 3814, 3815 };
				break;
			/** 웰던마을 - 조우의 불골렘 **/
			case 100029:
				craftlist = new int[] { 515, 514, 516, 517, 116, 123, 159, 160, 161, 162, 271, 272, 273, 274, 275, 276,
						277, 278, 279, 280, 281, 282, 283, 284, 285, 286, 287, 288, 289, 290, 423, 424, 425, 2182, 2183,
						2227, 2184, 2185 };
				break;
			/****************************************************************************************************/
			/** 침동 - 대장장이 쿠프 **/
			case 70904:
				craftlist = new int[] { 219, 220, 221, 222, 223, 224, 225, 226, 215, 216, 217, 218, 69, 70, 71, 72, 73,
						74, 75, 76, 77, 78 };
				break;
			/** 용기사마을 - 대장장이 퓨알 **/
			case 4218001:
				craftlist = new int[] { 62, 65, 63, 64, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78 };
				break;
			/** 환술사마을 - 대장장이 바트르 **/
			case 4219001:
				craftlist = new int[] { 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78 };
				break;
			/** 글루딘마을 - 대장장이 쿠엔 **/
			case 80101:
				craftlist = new int[] { 69, 70, 71, 72, 73, 74, 75, 76, 77, 78 };
				break;
			/** 은기사마을 - 대장장이 아논 **/
			case 70802:
				craftlist = new int[] { 69, 70, 71, 72, 73, 74, 75, 76, 77, 78 };
				break;
			/** 글루딘마을 - 박제상 필리스 **/
			case 80102:
				craftlist = new int[] { 136, 137, 138, 139, 140 };
				break;
			/** 오렌마을 - 바레트(연금술사) **/
			case 70520:
				craftlist = new int[] { 3598, 2528, 95, 96, 97, 98, 99, 1960, 1961, 1861 };
				break;
			/** 깃털마을 - 언라크 **/
			case 6500:
				craftlist = new int[] { 620, 621, 622, 623, 624, 625, 626, 627, 628, 629, 630, 631, 632, 633, 634, 635,
						636, 637, 638, 639, 640, 641, 642, 643, 644, 645, 646, 647, 648 };
				break;
			/****************************************************************************************************/
			/** 화전민마을 - 라이라 **/
			case 70811:
				craftlist = new int[] { 417, 418, 419, 420 };
				break;
			/****************************************************************************************************/
			/** 오렌마을3층 - 타라스 **/
			case 70763:
				craftlist = new int[] { 211, 212, 213, 214, 2057, 2058, 2059, 2060, 2061, 2063, 2064, 2065 };
				break;
			/****************************************************************************************************/
			/** 아덴마을 - 무브니 **/
			case 6000101:
				craftlist = new int[] {4589, 823, 824, 825, 826, 827, 828, 829, 830, 831, 832, 833, 834, 835, 836, 837, 838,
						839, 840, 841, 842, 843, 844, 845, 846, 847, 848, 849, 850, 851, 852 };
				break;
			/****************************************************************************************************/
			/** 연구실 - 세이룬 **/
			case 100644:
				craftlist = new int[] { 577, 578, 579, 580, 581,4354,4355,4356,4357,4358,4359,4360,4361,4362,4363,4364,4365,4366,4367,4368 };
				break;
			/****************************************************************************************************/
			/** 아타로제 **/
			case 71119:
				craftlist = new int[] { 56 };
				break;
			/** 아델리오 **/
			case 71125:
				craftlist = new int[] { 46, 47, 48, 49, 50, 51, 52, 53, 54, 55 };
				break;
			/** 모리아 **/
			case 70598:
				craftlist = new int[] { 198 };
				break;
			/** 네루파 **/
			case 70838:
				craftlist = new int[] { 2857, 2861, 2864, 2865, 2769, 2770, 2771, 2772, 2759, 184, 211, 2773, 2774, 173, 183 };
				break; // 네루파
			/** 레서 데몬(발록) **/
			case 80069:
				craftlist = new int[] { 35, 36, 37, 38 };
				break;
			/** 발록진영 - 발록의 분신 **/
			case 80068:
				craftlist = new int[] { 39, 40, 41, 42, };
				break;
			/** 엔트 **/
			case 70848:
				craftlist = new int[] { 188, 189, 190 };
				break;
			/** 나르엔 **/
			case 70837:
				craftlist = new int[] { 186 };
				break;
			/** 주티스 (솔로부대이벤트상자) **/
			case 100692:
				craftlist = new int[] { 360, 361, 362, 363 };
				break;
			/** 가웨인 **/
			case 100630:
				craftlist = new int[] { 159, 160 };
				break;
			/** 루디엘 **/
			case 70841:
				craftlist = new int[] { 187, 188 };
				break;
			/** 아라크네 **/
			case 70846:
				craftlist = new int[] { 163, 164, 165 };
				break;
			/** 판 **/
			case 70850:
				craftlist = new int[] { 166, 167, 168 };
				break;
			/** 페어리 **/
			case 70851:
				craftlist = new int[] { 169, 170 };
				break;
			/** 페어리 퀸 **/
			case 70852:
				craftlist = new int[] { 169, 170, 171 };
				break;
			/** 야히진영 - 연구원 **/
			case 80054:
				craftlist = new int[] { 7, 8, 9 };
				break;
			/** 야히진영 - 야히 **/
			case 80051:
				craftlist = new int[] { 6 };
				break;
			/** 류미엘 **/
			case 70676:
				craftlist = new int[] { 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239, 240, 241, 242 };
				break;
			/** 마법문자 **/
			case 6700:
				craftlist = new int[] { 3879, 3880, 3881, 3882, 110, 111, 112, 113, 114, 115, 520 };
				break;
			/** 야히의 분신 **/
			case 81114:
				craftlist = new int[] { 243, 244, 245 };
				break;
			/** 퀸 스지(용의 호박갑옷) **/
			case 100671:
				craftlist = new int[] { 357, 358 };
				break;
			/** 페리타 (군왕반지) **/
			case 71128:
				craftlist = new int[] { 1734, 1735, 1736, 1737 };
				break;
			/** 룸티스 (지배/관용/불사 귀걸이) **/
			case 71129:
				craftlist = new int[] { 1731, 1732, 1733 };
				break;
			/** 야히의 보좌관 (우호도목걸이) **/
			case 80055:
				craftlist = new int[] { 3377, 2044, 156 };
				break;
			/** 발록의 보좌관 (우호도귀걸이) **/
			case 80071:
				craftlist = new int[] { 35, 36, 37, 38, 39, 40, 3378, 2036, 1805, 1806, 1807, 1808, 1809, 141, 142, 143,
						144, 145, 146, 147, 148 };
				break;
			/** 레옹 (잊섬) **/
			case 6600:
				craftlist = new int[] { 1763, 1764, 1765, 1766, 1767, 1768, 1769, 1770, 1771, 1772, 1773, 1774, 1775,
						1776, 1777, 1778 };
				break;
			/** 디오(기란) **/
			case 70027:
				craftlist = new int[] { 2731, 2732, 2733, 2734, 2735, 2736, 2737, 2738, 2739, 2792, 2788, 2789, 3196 };
				break;
			/** 문장강화석상 **/
			case 7010:
				craftlist = new int[] { 4396, 4397, 4398, 4399, 4400, 4401, 4402, 4403, 4404, 4405, 4406, 4407 };
				break;
			/** 드래곤의 석상 **/
			case 75114:
				craftlist = new int[] {/* 4819,4820,4821, *//*3385, 3386, 3387,*/ 2747, 2398, 2397, 2514 };
				break;
			/** 휘장강화석상 **/
			case 75112:
				craftlist = new int[] { 4078, 4079, 4080, 4081, 4082, 4083, 4084, 4085, 4086, 4087, 4088, 4089 };
				break;
			/** 탐 석상 **/
			case 100804:
				craftlist = new int[] { 4927, 4928, 4929, 4930, 4931, 4932, 4933 };
				break;
			/** 소생 엔피시 **/
			case 700080:
				craftlist = new int[] { 5050,5051,5052,5053,5054,5055,5056,5057,5058,5059,5060,5061,5062 };
				break;
			case 700081:
				craftlist = new int[] { 5075,5076,5077,5078,5079,5080,5081,5082,5083,5084,5085,5086,5087,5088,5089,5090,5091,5092,5093,5094,5095,5096,5097,5098,5099,5100,5101,5102,5103,5104,5105,5106,5107,5108,5109,5110,5111,5112 };
				break;
			case 700082:
				craftlist = new int[] { 5130,5131,5132,5133,5134,5135,5136,5137,5138,5139,5140,5141,5142,5143,5144,5145,5146,5147,5148,5149,5150,5151,5152 };
				break;
			case 700083:
				craftlist = new int[] { 5020,5021,5022,5023,5024,5025,5026,5027,5028,5029,5030,5031,5032,5193,5194 };
				break;
			case 700084:
				craftlist = new int[] { 5171,5172,5173,5174,5175,5176,5177,5178,5179,5180,5181,5182,5183,5184,5185,5186,5187,5188,5189,5190 };
				break;
				
			case 7011:
				craftlist = new int[] { 2726, 2727, 2728 };
				break;
			// 아놀드 이벤트 엔피씨
			case 6:
				craftlist = new int[] { 1629, 1630, 1631, 1632, 1633, 1634, 1635, 1636, 1637, 1638, 1639, 1640, 1641,1642, 1643, 1644, 1645, 1646 };
				break;
			case 8:
				craftlist = new int[] { 1647, 1648, 1649, 1650, 1651, 1652, 1653, 1654, 1655 };
				break;
			case 82000:
			case 82001:
			case 82002:
			case 82003:
			case 82004:
			case 82005:
			case 82006:
			case 82007:
			case 82008:
			case 82009:
			case 82010:
			case 82011:
				craftlist = new int[] { 3870 };
				break;
			case 83000:
				craftlist = new int[] { 1863, 1959 };
				break;
			case 14921:
				craftlist = new int[] { 1862, 1863, 1959 };
				break;
			case 14922:
				craftlist = new int[] { 4429, 4430,4431,4432,4433,4434,4435,4436,4437,4438,4439, 
						4442,4443,4444,4445,4446,4447,4448,4449,4450,4451, 
						4455,4456,4457,4458,4459,4460,4461,4462,4463,4464,
						4468,4469,4470,4471,4472,4473,4474,4475,4476,4477,
						4481,4482,4483,4484,4485,4486,4487,4488,4489,4490,
						4494,4495,4496,4497,4498,4499,4500,4501,4502,4503,
						4507,4508,4509,4510,4511,4512,4513,4514,4515,4516,
						4520,4521,4522,4523,4524,4525,4526,4527,4528,4529};
				break;
			case 14923:
				craftlist = new int[] { 4626,4627,4628,4629,4630,4631,4632,4633,4735,4736,4737,4738,4819,4820,4821,4827,4828};
				break;
			case 14924:
				craftlist = new int[] { 4819,4820,4821 };
				break;
			case 14925:
				craftlist = new int[] { Config.상점데스크번호5 };
				break;
			case 14926:
				craftlist = new int[] { Config.상점데스크번호6 };
				break;
			case 14927:
				craftlist = new int[] { Config.상점데스크번호7 };
				break;
			case 14928:
				craftlist = new int[] { Config.상점데스크번호8 };
				break;
			case 14929:
				craftlist = new int[] { Config.상점데스크번호9 };
				break;
			case 14930:
				craftlist = new int[] { Config.상점데스크번호10 };
				break;
			case 14931:
				craftlist = new int[] { Config.상점데스크번호11 };
				break;
			case 14932:
				craftlist = new int[] { Config.상점데스크번호12 };
				break;
			case 14933:
				craftlist = new int[] { Config.상점데스크번호13 };
				break;
			case 14934:
				craftlist = new int[] { Config.상점데스크번호14 };
				break;
			case 14935:
				craftlist = new int[] { Config.상점데스크번호15 };
				break;
			case 14936:
				craftlist = new int[] { Config.상점데스크번호16 };
				break;
			case 14937:
				craftlist = new int[] { Config.상점데스크번호17 };
				break;
			case 14938:
				craftlist = new int[] { Config.상점데스크번호18 };
				break;
			case 14939:
				craftlist = new int[] { Config.상점데스크번호19 };
				break;
			case 14940:
				craftlist = new int[] { Config.상점데스크번호20 };
				break;

			// default:
			// break;
			}
			int num;
			for (int i = 0; i < craftlist.length; i++) {
				writeC(0x12);
				num = craftlist[i];
				if (num > 127) {
					writeC(0x07);
				} else {
					writeC(0x06);
				}
				writeC(0x08);
				write4bit(num);
				writeH(0x10);
				writeH(0x18);
			}
			writeH(0x00);
		} catch (Exception e) {

		}
	}

	/**
	 * 혈맹관련
	 */
	public S_ACTION_UI(String clanname, int rank) {
		writeC(Opcodes.S_EXTENDED_PROTOBUF);
		writeC(0x19);
		writeC(0x02);
		writeC(0x0a);
		int length = 0;
		if (clanname != null)
			length = clanname.getBytes().length;
		if (length > 0) {
			writeC(length); // 클랜명 SIZE
			writeByte(clanname.getBytes()); // 클랜명
			writeC(0x10);
			writeC(rank); // 클랜 랭크
		} else {
			writeC(0x00);
		}
		writeH(0x00);
	}

	/**
	 * 전사스킬을 위해
	 */
	public S_ACTION_UI(int type, int skillnum) {
		writeC(Opcodes.S_EXTENDED_PROTOBUF);
		writeC(type);
		if (type == 110) {
			writeC(0x00);
			writeC(0x08);
			writeC(0x03);
			writeC(0x10);
			write7B(skillnum);
			writeC(0x30);
			writeC(0x00);
			writeC(0x50);
			writeC(0x00);
			writeH(0x00);
		} else if (type == 145) { // 로그인
			// b3 91 01 0a 02 08 03 8c c7
			writeC(0x01);
			writeC(0x0a);
			writeC(skillnum != 5 ? 0x02 : 0x04);
			writeC(0x08);
			writeC(skillnum);
			if (skillnum == 5) { // 아머가드
				writeC(0x10);
				writeC(0x0a);
			}
			writeH(0xf18d);
		} else if (type == 146) { // 새로 생성시
			// b3 92 01 08 03 c2 33
			writeC(0x01);
			writeC(0x08);
			writeC(skillnum);
			if (skillnum == 5) { // 아머가드
				writeC(0x10);
				writeC(0x0a);
			}
			writeH(0x00);
		} else if (type == TAM) {
			writeC(0x01);
			writeC(0x08);
			write4bit(skillnum);
			writeH(0x00);
		}
		// test
		else if (type == TEST) {
			writeC(0x0d);
			write4bit(skillnum);
		} else if (type == CLAN_JOIN_MESSAGE) {
			writeH(0x0801);
			writeC(skillnum);
			writeH(0x00);
		}
	}
	
	public S_ACTION_UI(int type, L1PcInstance pc) {
		writeC(Opcodes.S_EXTENDED_PROTOBUF);
		writeH(type);
		switch(type) {
		case EINHASAD:
			int value = pc.getAinHasad();
			int ration = 0;
			int extra = 0;
			
			value /= 10000;
			writeC(0x08);
			writeBit(value);// % 수치 1~200
			// writeD(0x00001E14);//
			writeC(0x10);
			writeBit(10000);
			
			if (value <= 200) {
				ration += 100;
			} else if (value >= 201 && value <= 1800) {
				ration += 60;
				extra = 40;
			} else if (value >= 1801 && value <= 3400) {
				ration += 80;
				extra = 50;
			} else if (value >= 3401 && value <= 5000) {
				ration += 100;
				extra = 60;
			}
			
			if (EventShop_아인_따스한_시선.진행()) {
				if (EventShop_아인_따스한_시선.EventZone(pc)) {
					ration += 50;
					return;
				}
			}
			
			writeC(0x10);
			writeBit(ration * 100); // 추가 축복 EXP
			
			writeC(0x18);
			writeBit(extra); // 추가 축복 EXP
			
			writeC(0x20);
			writeBit(pc.getResistance().getAinBooster()); // 축복 효율
			break;
		}
		writeH(0);
	}

	@Override
	public byte[] getContent() {
		if (_byte == null) {
			_byte = getBytes();
		}
		return _byte;
	}

	public String getType() {
		return S_ACTION_UI;
	}
}
