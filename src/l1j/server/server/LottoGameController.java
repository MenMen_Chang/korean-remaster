package l1j.server.server;

import java.util.Calendar;
import java.util.Locale;
import java.util.Random;
import java.text.SimpleDateFormat;

import javolution.util.FastTable;
import l1j.server.Config;
import l1j.server.server.datatables.AutoShopTable;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.serverpackets.S_Message_YN;
import l1j.server.server.serverpackets.S_PacketBox;
import l1j.server.server.serverpackets.S_SystemMessage;
import server.manager.eva;

public class LottoGameController extends Thread {
	
		private static LottoGameController _instance;
		
		private static Random _random = new Random(System.nanoTime());

		private boolean _LottoStart;
		
		public boolean getLottoStart() {
			return _LottoStart;
		}
		public void setLottoStart(boolean Lotto) {
			_LottoStart = Lotto;
		}
		private static long sTime = 0;	
		
		public boolean isGmOpen = false;
		
		private int lottoAden = 0;
		
		public void addLottoAden(int i){
			lottoAden += i;
		}
		
		public int getLottoAden(){
			return lottoAden;
		}
		
		public void setLottoAden(int i) { 
			lottoAden = i;
		}
		private static final FastTable<L1PcInstance> sList = new FastTable<L1PcInstance>();

		private static final SimpleDateFormat s = new SimpleDateFormat("HH", Locale.KOREA);

		private static final SimpleDateFormat ss = new SimpleDateFormat("MM-dd HH:mm", Locale.KOREA);

		public static LottoGameController getInstance() {
			if(_instance == null) {
			_instance = new LottoGameController();
			}
			return _instance;
		}
		
		@Override
			public void run() {
			try	{
					while (true) {
						Thread.sleep(1000); 
						/** 오픈 **/
						if(!isOpen() && !isGmOpen)
							continue;
						if(L1World.getInstance().getAllPlayers().size() <= 0)
							continue;
						
						isGmOpen = false;
						
						/** 상점 물품 초기화 **/
	/*					AutoShopTable.getInstance().reload(1);
						eva.EventLogAppend("[오토상점 1단계 자동 리셋]: 완료");*/
						
						/** 시작 메세지 **/
						for(L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
							pc.sendPackets(new S_SystemMessage("\\aC실시간 로또게임이 시작됩니다. 응모 메세지에 Y를 클릭"));
							pc.sendPackets(new S_SystemMessage("\\aC하시거나, \\aD[.로또참여]\\aC를 사용해주세요. 로또게임은"));
							pc.sendPackets(new S_SystemMessage("\\aD2분간 참여\\aC가 가능합니다. 게임방식은 참여자가 부담하는"));
							pc.sendPackets(new S_SystemMessage("\\aD"+Config.LOTTO_BATTING+"아데나를 총합\\aC하여 당첨자에게 지급합니다."));
							pc.sendPackets(new S_SystemMessage("\\aC중복 당첨자 발생시 총액에 \\aD당첨자수를 나눈\\aC 금액이"));
							pc.sendPackets(new S_SystemMessage("\\aC인벤토리에 자동 지급됩니다."));
						}
						L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, 
								"\\f7                 실시간 로또게임이 \\f=시작\\f7됩니다. 응모 메세지에 \\f=Y를 클릭\\f7하거나, \\f=.로또참여\\f7를 사용하세요."));

						/** 로또게임 시작 **/
						setLottoStart(true);
						
						Thread.sleep(5000);
						
						for(L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
						pc.setLottoGame(true);
						pc.system = 6;
						pc.setLotto(0);
						
						if(pc.getLevel() >= Config.LOTTO_LEVEL){
						pc.sendPackets(new S_Message_YN(622, "실시간 로또게임에 응모하시겠습니까?"));
							}
						}
						
						Thread.sleep(120000L); //120000L 2분
						for(L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
							pc.setLottoGame(false);
							pc.sendPackets(new S_SystemMessage("\\aC실시간 로또게임 \\aD응모시간이 종료\\aC되었습니다."));
						}
						
						Thread.sleep(1000);
						
						L1World.getInstance().broadcastPacketToAll
						(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "\\f=          5초뒤 \\f7실시간 로또게임 \\f=당첨번호 발표\\f7가 있겠습니다."));
						
						Thread.sleep(5000);
						
						/** 당첨번호 발표 **/
						getNumber();
						
						Thread.sleep(5000);
						
						/** 당첨자 발표 및 금액 배분 **/
					    giveAden();
					    
						/** 로또 게임 종료 **/
					    
						setLottoStart(false);
						End();
					}
					} catch(Exception e) {
					e.printStackTrace();
				}
			}

			/**
			 *오픈 시각을 가져온다
			 *
			 *@return (Strind) 오픈 시각(MM-dd HH:mm)
			 */
			 public String OpenTime() {
				 Calendar c = Calendar.getInstance();
				 c.setTimeInMillis(sTime);
				 return ss.format(c.getTime());
			 }

			 private void giveAden(){
				 int count = 0;
				 int lcount = 0;
				 if(sList.size() > 0){
					 lcount = getLottoAden()+Config.LOTTO_BONUS;
					 count = lcount / sList.size();
				 for(L1PcInstance c : sList){
					 if(getLottoAden() > 150000000){	setLottoAden(150000000);	}
					 c.sendPackets(new S_SystemMessage("\\aE축하드립니다. 실시간 로또에 당첨 되었습니다."));
					 c.sendPackets(new S_SystemMessage("\\aE총 금액에서 당첨자 수로 나눈 금액이 지급됩니다."));
					 c.sendPackets(new S_SystemMessage("\\aE당첨금 \\aD"+count+"아데나 \\aE가 지급 되었습니다."));
					 L1World.getInstance().broadcastServerMessage("\\aD"+c.getName()+"님이 로또에 당첨 되었습니다. 축하드립니다.");
					 c.getInventory().storeItem(40308, count);
				 }
				 L1World.getInstance().broadcastPacketToAll
				 	(new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
					"\\f7              이번 실시간 로또의 총상금은 \\f="+lcount+"아데나 \\f7이며, 총 \\f="+sList.size()+"명\\f7의 당첨자가 나왔습니다"));
				 L1World.getInstance().broadcastPacketToAll(new S_SystemMessage("\\aC이번 로또게임의 총 상금은 \\aD"+lcount+"아데나 \\aC입니다."));
				 L1World.getInstance().broadcastPacketToAll(new S_SystemMessage("\\aC총 당첨자는 \\aD"+sList.size()+"명 \\aC입니다."));
				 setLottoAden(0);
				 } else {
					 lcount = getLottoAden()+Config.LOTTO_BONUS;
					 L1World.getInstance().broadcastPacketToAll
						(new S_PacketBox(S_PacketBox.GREEN_MESSAGE,"\\f7          실시간 로또게임의 \\f=당첨자가 없는 \\f7관계로 이번 로또게임은 \\f=이월처리\\f7가 됩니다."));
					 L1World.getInstance().broadcastPacketToAll(new S_SystemMessage("\\aC이번 로또게임의 총상금은 \\aD"+lcount+"아데나 \\aC입니다."));
					 L1World.getInstance().broadcastPacketToAll(new S_SystemMessage("\\aC당첨자가 없으므로 \\aD당첨금은 이월처리 \\aC되었습니다."));
					 setLottoAden(lcount);
				 }
				 }
			
			 private void getNumber(){
					int number = 0;
					number = _random.nextInt(Config.LOTTO_SIZE)+1;
					for(L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
					if(pc.getLotto() == number){
						if (!sList.contains(pc)) {
							sList.add(pc); //당첨자수 추가
					}
					}
					pc.sendPackets(new S_SystemMessage("\\aC실시간 로또게임 당첨번호는 \\aD"+number+"번 \\aC입니다."));
					}
					L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, 
							"\\f7            실시간 로또게임 당첨번호는 \\f="+number+"번 \\f7입니다. 5초뒤 당첨자 발표 및 당첨금이 자동지급 됩니다."));
			 }
			 
			 private boolean isOpen() {
				  Calendar calender = Calendar.getInstance();
				  int minute;
				  minute = calender.get(Calendar.MINUTE);
				  if (minute == 30) {
				   return true;
				  }
				  return false;
				 }


			 /**
			 *실제 현재시각을 가져온다
			 *
			 *@return (String) 현재 시각(HH:mm)
			 */
			 private String getTime() {
				 return s.format(Calendar.getInstance().getTime());
			 }
			 /** 종료 **/
			 private void End() {
				 sList.clear(); // 당첨자수 초기화
				 setLottoStart(false);
			 }
}