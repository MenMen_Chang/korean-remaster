/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */

package l1j.server.server.datatables;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javolution.util.FastMap;
import l1j.server.L1DatabaseFactory;
import l1j.server.server.utils.SQLUtil;

public class WeaponAddHitRate {
	public class WeaponHitrate {
		int Hit = 0;
	}

	private static Logger _log = Logger.getLogger(WeaponAddHitRate.class.getName());

	private static WeaponAddHitRate _instance;
	

	private final Map<Integer, WeaponHitrate> _idlist = new FastMap<Integer, WeaponHitrate>();
	
	public static WeaponAddHitRate getInstance() {
		if (_instance == null) {
			_instance = new WeaponAddHitRate();
		}
		return _instance;
	}

	private WeaponAddHitRate() {
		System.out.print("☞ 무기별공성 .......................... ");		
		weaponAddHitRate();
		System.out.println("☞ 정상 완료");			
	}
	
	public void weaponAddHitRate() {
		Connection con = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;

		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("select item_id, addHitRate from weapon_damege");
			rs = pstm.executeQuery();
			WeaponHitrate weaponhit = null;
			while (rs.next()) {
				weaponhit = new WeaponHitrate();
				weaponhit.Hit = rs.getInt("addHitRate");
				_idlist.put(rs.getInt("item_id"), weaponhit);
			}

		} catch (SQLException e) {
			_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
		} finally {
			SQLUtil.close(rs);
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	public static void reload() {
		WeaponAddHitRate oldInstance = _instance;
		_instance = new WeaponAddHitRate ();
		if (oldInstance != null)
			oldInstance._idlist.clear();
	}

	public double getWeaponAddHitRate(int itemId) {
		WeaponHitrate weaponhit = _idlist.get(itemId);

		if (weaponhit == null) {
			return 0;
		}

		return weaponhit.Hit;
	}
}