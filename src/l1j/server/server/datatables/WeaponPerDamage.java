/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */

package l1j.server.server.datatables;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javolution.util.FastMap;

import l1j.server.L1DatabaseFactory;
import l1j.server.server.templates.L1EnchantDamage;
import l1j.server.server.utils.SQLUtil;

public class WeaponPerDamage {//enchant_per_damage
	private static Logger _log = Logger.getLogger(WeaponPerDamage.class.getName());

	private static WeaponPerDamage _instance;
	private final FastMap<String, L1EnchantDamage> _weaponName = new FastMap<String, L1EnchantDamage>();
	private final FastMap<Integer, L1EnchantDamage> _weaponIdList = new FastMap<Integer, L1EnchantDamage>();

	public static WeaponPerDamage getInstance() {
		if (_instance == null) {
			_instance = new WeaponPerDamage();
		}
		return _instance;
	}

	private WeaponPerDamage() {
		System.out.print("☞ 인챈별추타 .......................... ");		
		load();
		System.out.println("☞ 정상 완료");	
		
	}

	private void load() {
		Connection con = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("SELECT * FROM enchant_per_damage");
			rs = pstm.executeQuery();
			fillTable(rs);
		} catch (SQLException e) {
			_log.log(Level.SEVERE, "error while creating enchant_per_damage table", e);
		} finally {
			SQLUtil.close(rs);
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}
	
	public static void reload() {
		 WeaponPerDamage oldInstance = _instance;
		_instance = new  WeaponPerDamage();
		if (oldInstance != null)
			oldInstance._weaponName.clear();
			oldInstance._weaponIdList.clear();
	}

	private void fillTable(ResultSet rs) throws SQLException {
		L1EnchantDamage ed  = null;
		while (rs.next()) {
			int weaponId = rs.getInt("weaponId");
			String weaponname = rs.getString("weaponname");
			int e0 = rs.getInt("0");
			int e1 = rs.getInt("1");
			int e2 = rs.getInt("2");
			int e3 = rs.getInt("3");
			int e4 = rs.getInt("4");
			int e5 = rs.getInt("5");
			int e6 = rs.getInt("6");
			int e7 = rs.getInt("7");
			int e8 = rs.getInt("8");
			int e9 = rs.getInt("9");
			int e10 = rs.getInt("10");
			int e11 = rs.getInt("11");
			int e12 = rs.getInt("12");
			int e13 = rs.getInt("13");
			int e14 = rs.getInt("14");
			
			ed = new L1EnchantDamage(weaponId, weaponname, e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14);
			
			_weaponName.put(weaponname, ed);
			_weaponIdList.put(weaponId, ed);
		}
		_log.config("인챈데미지 리스트 " + _weaponName.size() + "건 로드");
		SQLUtil.close(rs);
	}
	
	public L1EnchantDamage getTemplate(String name) {
		return _weaponName.get(name);
	}

	public L1EnchantDamage getTemplate(int weaponId) {
		return _weaponIdList.get(weaponId);
	}

}
