/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */

package l1j.server.server.datatables;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import kr.PeterMonk.GameSystem.SupportSystem.L1SupportMap;
import kr.PeterMonk.GameSystem.SupportSystem.SupportMapTable;
import l1j.server.Config;
import l1j.server.L1DatabaseFactory;
import l1j.server.GameSystem.EventShop.EventShop_드래곤;
import l1j.server.GameSystem.EventShop.EventShop_루피주먹;
import l1j.server.GameSystem.EventShop.EventShop_벚꽃;
import l1j.server.GameSystem.Robot.L1RobotInstance;
import l1j.server.server.model.L1Character;
import l1j.server.server.model.L1GroundInventory;
import l1j.server.server.model.L1Inventory;
import l1j.server.server.model.L1Object;
import l1j.server.server.model.L1Quest;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1DollInstance;
import l1j.server.server.model.Instance.L1ItemInstance;
import l1j.server.server.model.Instance.L1NpcInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.model.Instance.L1PetInstance;
import l1j.server.server.model.Instance.L1SummonInstance;
import l1j.server.server.model.item.L1ItemId;
import l1j.server.server.model.skill.L1SkillId;
//Referenced classes of package l1j.server.server.templates:
//L1Npc, L1Item, ItemTable
import l1j.server.server.serverpackets.S_ServerMessage;
import l1j.server.server.serverpackets.S_SystemMessage;
import l1j.server.server.templates.L1Drop;
import l1j.server.server.templates.L1Item;
import l1j.server.server.utils.SQLUtil;

public class DropTable {

	private static Logger _log = Logger.getLogger(DropTable.class.getName());

	private static DropTable _instance;
	
	private static final Random _random = new Random(); //이것도 있다면 해주지마세요
	
	private final HashMap<Integer, ArrayList<L1Drop>> _droplists; // monster 마다의
																	// 드롭 리스트

	private static final byte HEADING_TABLE_X[] = { 0, 1, 1, 1, 0, -1, -1, -1 };

	private static final byte HEADING_TABLE_Y[] = { -1, -1, 0, 1, 1, 1, 0, -1 };

	public static DropTable getInstance() {
		if (_instance == null) {
			_instance = new DropTable();
		}
		return _instance;
	}

	private DropTable() {
		_droplists = allDropList();
	}

	public static void reload() {
		synchronized (_instance) {
			DropTable oldInstance = _instance;
			_instance = new DropTable();
			oldInstance._droplists.clear();
		}
	}

	public static final int CLASSID_PRINCE = 0;
	public static final int CLASSID_PRINCESS = 1;
	public static final int CLASSID_KNIGHT_MALE = 61;
	public static final int CLASSID_KNIGHT_FEMALE = 48;
	public static final int CLASSID_ELF_MALE = 138;
	public static final int CLASSID_ELF_FEMALE = 37;
	public static final int CLASSID_WIZARD_MALE = 734;
	public static final int CLASSID_WIZARD_FEMALE = 1186;
	public static final int CLASSID_DARKELF_MALE = 2786;
	public static final int CLASSID_DARKELF_FEMALE = 2796;
	public static final int CLASSID_DRAGONKNIGHT_MALE = 6658;
	public static final int CLASSID_DRAGONKNIGHT_FEMALE = 6661;
	public static final int CLASSID_ILLUSIONIST_MALE = 6671;
	public static final int CLASSID_ILLUSIONIST_FEMALE = 6650;
	public static final int CLASSID_WARRIOR_MALE = 12490;
	public static final int CLASSID_WARRIOR_FEMALE = 12494;

	private HashMap<Integer, ArrayList<L1Drop>> allDropList() {
		HashMap<Integer, ArrayList<L1Drop>> droplistMap = new HashMap<Integer, ArrayList<L1Drop>>();

		Connection con = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("select * from droplist");
			rs = pstm.executeQuery();
			L1Drop drop = null;
			while (rs.next()) {
				int mobId = rs.getInt("mobId");
				int itemId = rs.getInt("itemId");
				int min = rs.getInt("min");
				int max = rs.getInt("max");
				int chance = rs.getInt("chance");
				int enchant = rs.getInt("Enchant");
				int Rate = rs.getInt("Rate");
				drop = new L1Drop(mobId, itemId, min, max, chance, enchant, Rate);
				ArrayList<L1Drop> dropList = droplistMap.get(drop.getMobid());
				if (dropList == null) {
					dropList = new ArrayList<L1Drop>();
					droplistMap.put(new Integer(drop.getMobid()), dropList);
				}
				dropList.add(drop);
			}
		} catch (SQLException e) {
			_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
		} finally {
			SQLUtil.close(rs);
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
		return droplistMap;
	}

	@SuppressWarnings("resource")
	public static boolean SabuDrop(int npcid, int itemid, int max, int rate, String mobname, String itemname, String note) {
		Connection con = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("select * from droplist where mobId = ? and itemId = ?");
			pstm.setInt(1, npcid);
			pstm.setInt(2, itemid);
			rs = pstm.executeQuery();
			while (rs.next()) {
				SQLUtil.close(rs);
				SQLUtil.close(pstm);
				SQLUtil.close(con);
				return false;
			}
			pstm = con.prepareStatement("INSERT INTO droplist SET mobId=?,itemId=?,min=?,max=?,chance=?,mobname=?,itemname=?,mobnote=?");
			pstm.setInt(1, npcid);
			pstm.setInt(2, itemid);
			pstm.setInt(3, 1);
			pstm.setInt(4, max);
			pstm.setInt(5, rate);
			pstm.setString(6, mobname);
			pstm.setString(7, itemname);
			pstm.setString(8, note);
			pstm.execute();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(rs);
			SQLUtil.close(pstm);
			SQLUtil.close(con);

		}
		return true;
	}

	// 인벤트리에 드롭을 설정
	public void setDrop(L1NpcInstance npc, L1Inventory inventory) {
		/** 서버 오픈 대기 */
		if (Config.STANDBY_SERVER) {
			return;
		}
		// 드롭 리스트의 취득
		int mobId = npc.getNpcTemplate().get_npcId();
		ArrayList<L1Drop> dropList = _droplists.get(mobId);
		if (dropList == null) {
			return;
		}

		// 레이트 취득
		double droprate = Config.RATE_DROP_ITEMS;
		if (droprate <= 0) {
			droprate = 0;
		}
		double adenarate = Config.RATE_DROP_ADENA;
		if (adenarate <= 0) {
			adenarate = 0;
		}
		if (droprate <= 0 && adenarate <= 0) {
			return;
		}
		if (inventory.getSize() >= 1) {
			System.out.println("[드랍오류2] npcid : " + npc.getNpcId() + " / x : "
					+ npc.getX() + " y : " + npc.getY() + " m : "
					+ npc.getMapId());
		}
		int itemId;
		int itemCount;
		int addCount;
		int randomChance;
		L1ItemInstance item;

		L1ItemInstance Citem;
		
		Random random = new Random(System.nanoTime());
		for (L1Drop drop : dropList) {// 드롭 아이템의 취득
			itemId = drop.getItemid();
			if (adenarate == 0 && itemId == L1ItemId.ADENA) {
				continue; // 아데나레이트 0으로 드롭이 아데나의 경우는 스르
			}

			// 드롭 찬스 판정
			randomChance = random.nextInt(0xf4240) + 1;
			
			double rateOfMapId = MapsTable.getInstance().getDropRate(npc.getMapId());
			double rateOfItem = DropItemTable.getInstance().getDropRate(itemId);

			if (droprate == 0 || drop.getChance() * droprate * rateOfMapId * rateOfItem < randomChance) {
				continue;
			}

			// 드롭 개수를 설정
			double amount = DropItemTable.getInstance().getDropAmount(itemId);
			int min = (int)(drop.getMin() * amount);
			int max = (int)(drop.getMax() * amount);

			itemCount = min;
			addCount = max - min + 1;

			if (addCount > 1) {
				itemCount += random.nextInt(addCount);
			}

			if (itemId == L1ItemId.ADENA) { // 드롭이 아데나의 경우는 아데나레이트를 건다
				itemCount *= adenarate;
			} else {
				if (itemCount > max) {
					itemCount = max;
				}
			}
			if (itemCount < 0) {
				itemCount = 0;
			}
			if (itemCount > 2000000000) {
				itemCount = 2000000000;
			}

			// 아이템의 생성
			if (ItemTable.getInstance().getTemplate(itemId) != null) {
				item = ItemTable.getInstance().createItem(itemId);
				if (item == null) continue;
				item.setCount(itemCount);		
				if (drop.getEnchant() != 0) {
					//System.out.println("[오류] droplist : 겹쳐지는 아이템에 인첸됨.(" + item.getItemId() + ")");
					item.setEnchantLevel(drop.getEnchant());
				}
				// 아이템 격납
				inventory.storeItem(item);
			} else {
				_log.info("[드랍 리스트 로딩중]없는 아이템입니다: " + itemId);
			}
		}

		if(npc.getMapId() == 53 || npc.getMapId() == 54){// 파우스트 귀걸이
			int itemRandom = random.nextInt(100)+1;
			if (itemRandom <= 3) {// 20%
				Citem = ItemTable.getInstance().createItem(8500);
				Citem.setCount(1);
				inventory.storeItem(Citem);
			}
		}
		
	/*	if(npc.getMapId() == 53 || npc.getMapId() == 54 || npc.getMapId() == 55 || npc.getMapId() == 56
			|| npc.getMapId() == 10 || npc.getMapId() == 11 || npc.getMapId() == 12
			|| npc.getMapId() == 812 || npc.getMapId() == 35 || npc.getMapId() == 36
			|| (npc.getMapId() >= 12852 && npc.getMapId() <= 12862)){// 애플 캔디
			int itemRandom = random.nextInt(100)+1;
			if (itemRandom <= 5) {// 20%
				Citem = ItemTable.getInstance().createItem(6133);
				Citem.setCount(1);
				inventory.storeItem(Citem);
			}
		}
		*/
		if(npc.getMapId() == 479){ // 중앙광장 환생보석
			int itemRandom = random.nextInt(100)+1;
			if (itemRandom <=  Config.중앙광장환보) {// 20%
				Citem = ItemTable.getInstance().createItem(6670);
				Citem.setCount(1);
				inventory.storeItem(Citem);
				}
			}

		if(npc.getMapId() == Config.주말이벤트코드 || npc.getMapId() == Config.주말이벤트코드2){ // 용계 지룡비늘
			int itemRandom = random.nextInt(100)+1;
				if (itemRandom <=  Config.교련깃털) {// 20%
					Citem = ItemTable.getInstance().createItem(Config.깃털번호);
					Citem.setCount(1);
					inventory.storeItem(Citem);
				}
			}
		
		
		if(npc.getMapId() == 15430){ // 용계 지룡비늘
			int itemRandom = random.nextInt(100)+1;
			if (itemRandom <=  Config.용계지비) {// 20%
				Citem = ItemTable.getInstance().createItem(40396);
				Citem.setCount(1);
				inventory.storeItem(Citem);
				}
			}
		
		if(npc.getMapId() == 15420){ // 설벽 수룡비늘
			int itemRandom = random.nextInt(100)+1;
			if (itemRandom <=  Config.설벽수비) {// 20%
				Citem = ItemTable.getInstance().createItem(40395);
				Citem.setCount(1);
				inventory.storeItem(Citem);
				}
			}
		
		if(npc.getMapId() == 15410){ // 풍둥 풍룡비늘
			int itemRandom = random.nextInt(100)+1;
			if (itemRandom <=  Config.풍둥풍비) {// 20%
				Citem = ItemTable.getInstance().createItem(40394);
				Citem.setCount(1);
				inventory.storeItem(Citem);
				}
			}
		
		if(npc.getMapId() == 15440){ // 화둥 화룡비늘
			int itemRandom = random.nextInt(100)+1;
			if (itemRandom <= Config.화둥화비) {// 20%
				Citem = ItemTable.getInstance().createItem(40393);
				Citem.setCount(1);
				inventory.storeItem(Citem);
				}
			}
		random = null;
	}

	public boolean autook(L1PcInstance pc) {
		L1PcInstance ckpc = L1World.getInstance().getPlayer(pc.getName());
		/*
		 * if(ckpc instanceof L1RobotInstance){ return true; }
		 */
		if (ckpc == null) {
			return false;
		}
		if (ckpc.getNetConnection() == null) {
			return false;
		}
		if (ckpc.getNetConnection().isClosed()) {
			return false;
		}
		if (ckpc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.STATUS_AUTOROOT)) {
			return false;
		}
		return true;
	}

	// 드롭을 분배
	public void dropShare(L1NpcInstance npc, ArrayList<?> acquisitorList, ArrayList<?> hateList, L1PcInstance pc) {
		/** 서버 오픈 대기 */
		if (Config.STANDBY_SERVER) {
			return;
		}
		L1Inventory inventory = npc.getInventory();
		int mobId = npc.getNpcTemplate().get_npcId();
		if (inventory.getSize() == 0) {
			return;
		}
		if (acquisitorList.size() != hateList.size()) {
			return;
		}
		if (pc.getLevel() >= 45) {
			if ((mobId >= 100225 && mobId <= 100231) || (mobId >= 100236 && mobId <= 100241)) {
				return;
			}
		}

		/** 퀘스트 관련 **/
		if (!npc.isResurrect()) {
		// 헤이트의 합계를 취득
		int totalHate = 0;
		L1Character acquisitor;
		for (int i = hateList.size() - 1; i >= 0; i--) {
			acquisitor = (L1Character) acquisitorList.get(i);
			if ((Config.AUTO_LOOT == 2) && (acquisitor instanceof L1SummonInstance || acquisitor instanceof L1PetInstance)) {
				acquisitorList.remove(i);
				hateList.remove(i);
			} else if (acquisitor != null && acquisitor.isDead() && (npc.getNpcId() == 4200011 || npc.getNpcId() == 4039007
							|| npc.getNpcId() == 100014
							|| npc.getNpcId() == 400016
							|| npc.getNpcId() == 400017
							|| npc.getNpcId() == 145684
							|| npc.getNpcId() == 4036016 || npc.getNpcId() == 4036017)) {
				acquisitorList.remove(i);
				hateList.remove(i);
			} else if (acquisitor != null && acquisitor.getMapId() == npc.getMapId() && (acquisitor.getLocation().getTileLineDistance(
							npc.getLocation()) <= Config.LOOTING_RANGE || (npc.getNpcId() == 4200011
							|| npc.getNpcId() == 4039007
							|| npc.getNpcId() == 100014
							|| npc.getNpcId() == 400016
							|| npc.getNpcId() == 400017
							|| npc.getNpcId() == 145684
							|| npc.getNpcId() == 4036016 || npc.getNpcId() == 4036017))) {
				totalHate += (Integer) hateList.get(i);
			} else { // null였거나 죽기도 하고 멀었으면 배제
				acquisitorList.remove(i);
				hateList.remove(i);
			}
		}
		// 드롭의 분배
		L1ItemInstance item;
		L1Inventory targetInventory = null;
		L1PcInstance player = null;
		Random random = new Random();
		int randomInt;
		int chanceHate;
		int itemId;

		for (int i = inventory.getSize(); i > 0; i--) {
			item = null;
			try {
				item = (L1ItemInstance) inventory.getItems().get(0);
				if (item == null) {
					continue;
				}
			} catch (Exception e) {
				System.out.println("드랍리스트 오류 표시 아이디 :" + npc.getNpcId() + " [이름] :" + npc.getName());
			}
			/** 감시자 리퍼 -> 트랜스된 보스몹들 부적 드랍안되게 수정. **/
			itemId = item.getItem().getItemId();
			boolean isGround = false;
			if (item.getItem().getType2() == 0 && item.getItem().getType() == 2) { // light계 아이템
				item.setNowLighting(false);
			}
			
			int count = item.getCount();
			
			if (pc.isAutoPlay()) {
				if (SupportMapTable.getInstance().isSupportMap(pc.getMapId())) {
					L1SupportMap SM = SupportMapTable.getInstance().getSupportMap(pc.getMapId());
					if (SM != null) {
						if (item != null && item.getItemId() == L1ItemId.ADENA) {
							count *= SM.getAdenaChance();
							item.setCount(count);
						}
					}
				}
			}
			
			if ((Config.AUTO_LOOT != 0 || AutoLoot.getInstance().isAutoLoot(itemId)) && totalHate > 0) {
				randomInt = random.nextInt(totalHate);
				chanceHate = 0;

				for (int j = hateList.size() - 1; j >= 0; j--) {
					chanceHate += (Integer) hateList.get(j);
					if (chanceHate > randomInt) {
						acquisitor = (L1Character) acquisitorList.get(j);
						if (acquisitor.getInventory().checkAddItem(item, count) == L1Inventory.OK) {
							targetInventory = acquisitor.getInventory();
							if (acquisitor instanceof L1PcInstance) {
								player = (L1PcInstance) acquisitor;
								L1ItemInstance l1iteminstance = player.getInventory().findItemId(L1ItemId.ADENA);
								// 소지 아데나를 체크
								if (l1iteminstance != null && l1iteminstance.getCount() > 2000000000) {
									targetInventory = L1World.getInstance().getInventory(acquisitor.getX(), acquisitor.getY(), acquisitor.getMapId()); // 가질 수 없기 때문에 발밑에 떨어뜨린다
									player.sendPackets(new S_ServerMessage(166, "소지하고 있는 아데나","2,000,000,000을 초과하고 있습니다."));		
								} else {
									if (npc.getNpcId() == 4200011 // 아이템 드랍메세지..
											|| npc.getNpcId() == 4039007
											|| npc.getNpcId() == 100014
											|| npc.getNpcId() == 400016
											|| npc.getNpcId() == 145684
											|| npc.getNpcId() == 400017
											|| npc.getNpcId() == 4036016
											|| npc.getNpcId() == 4036017) {
										Collection<L1Object> DragonDropTargetList = L1World.getInstance().getVisibleObjects(npc.getMapId()) .values();
										for (L1Object DUser : DragonDropTargetList) {
											if (DUser instanceof L1PcInstance && player != DUser) 
												((L1PcInstance) DUser).sendPackets(new S_ServerMessage(813, npc.getName(), item.getLogName(), player.getName()));
										}
										player.sendPackets(new S_ServerMessage(813, npc.getName(), item.getLogName(), player .getName()));
									} else if (player.isInParty()) { // 파티의 경우
											 if(item.getItemId() == L1ItemId.ADENA){
												 int 아데나 = count / (serchCount(player));
												 for (L1PcInstance pc2 : L1World.getInstance().getVisiblePartyPlayer(player, 14)) {
													targetInventory = pc2.getInventory();
													inventory.tradeItem(item, 아데나, targetInventory);
													for (L1PcInstance partymember : player.getParty().getMembers()) {
													if(partymember.PartyRootMent){
														partymember.sendPackets(new S_SystemMessage("아데나 ("+아데나+") 획득: "+pc2.getName()+"("+npc.getName()+")"));
													}
													}
												}
											} else {
												for (L1PcInstance partymember : player.getParty().getMembers()) {
													if(partymember.PartyRootMent){
													partymember.sendPackets(new S_ServerMessage(813, npc.getName(), item.getLogName(), player.getName()));
													}
													}
											}
											break;
									} else if (player.RootMent) {
										player.sendPackets(new S_ServerMessage(143, npc.getName(), item.getLogName()));
									}
								}
							} else {
								targetInventory = L1World.getInstance().getInventory(npc.getX(), npc.getY(), npc.getMapId()); 
							}
						} else {
							targetInventory = L1World.getInstance().getInventory(acquisitor.getX(), acquisitor.getY(),acquisitor.getMapId()); 
							// 가질 수 없기 때문에발밑에떨어뜨린다
						}
						break;
					}
				}
			} else { // Non 오토 루팅
				
				int maxHatePc = -1;
				int maxHate = -1;

				for (int j = hateList.size() - 1; j >= 0; j--) {
					if (maxHate < (Integer) hateList.get(j)) {
						maxHatePc = j;
						maxHate = (Integer) hateList.get(j);
					}
				}

				if (maxHatePc != -1 && acquisitorList.get(maxHatePc) instanceof L1PcInstance) {
					item.startItemOwnerTimer((L1PcInstance) acquisitorList.get(maxHatePc));
				} else {
					item.startItemOwnerTimer(pc);
				}

				List<Integer> dirList = new ArrayList<Integer>();
				for (int j = 0; j < 8; j++) {
					dirList.add(j);
				}
				int x = 0;
				int y = 0;
				int dir = 0;
				do {
					if (dirList.size() == 0) {
						x = 0;
						y = 0;
						break;
					}
					randomInt = random.nextInt(dirList.size());
					dir = dirList.get(randomInt);
					dirList.remove(randomInt);
					switch (dir) {
					case 0:
						x = 0;
						y = -1;
						break;
					case 1:
						x = 1;
						y = -1;
						break;
					case 2:
						x = 1;
						y = 0;
						break;
					case 3:
						x = 1;
						y = 1;
						break;
					case 4:
						x = 0;
						y = 1;
						break;
					case 5:
						x = -1;
						y = 1;
						break;
					case 6:
						x = -1;
						y = 0;
						break;
					case 7:
						x = -1;
						y = -1;
						break;
					}
				} while (!npc.getMap().isPassable(npc.getX(), npc.getY(), dir));

				targetInventory = L1World.getInstance().getInventory(npc.getX() + x, npc.getY() + y, npc.getMapId());
				isGround = true;
				dirList = null;
			}
			if (itemId >= 40131 && itemId <= 40135) {
				if (isGround || targetInventory == null) {
					inventory.removeItem(item, count);
					continue;
				}
			}

			L1ItemInstance resultItem = inventory.tradeItem(item, count, targetInventory);

			if (resultItem != null && targetInventory instanceof L1GroundInventory) {
				resultItem.setDropMobId(mobId);
			}
		}
		npc.getLight().turnOnOffLight();
		random = null;
		}
	}
	
	private int serchCount(L1PcInstance player){
	int pmember = 0;
	for (L1PcInstance pc2 : L1World.getInstance().getVisiblePartyPlayer(player, 14)) {
		pmember ++;
		}
	return pmember;
	}
	
	public void setPainwandDrop(L1NpcInstance npc, L1Inventory inventory) {
		/** 서버 오픈 대기 */
		if (Config.STANDBY_SERVER) {
			return;
		}

		// 드롭 리스트의 취득
		int mobId = npc.getNpcTemplate().get_npcId();
		ArrayList<L1Drop> dropList = _droplists.get(mobId);
		if (dropList == null) {
			return;
		}

		// 레이트 취득
		double droprate = Config.RATE_DROP_ITEMS;
		if (droprate <= 0) {
			droprate = 0;
		}
		double adenarate = Config.RATE_DROP_ADENA;
		if (adenarate <= 0) {
			adenarate = 0;
		}
		if (droprate <= 0 && adenarate <= 0) {
			return;
		}

		int itemId;
		int itemCount;
		int addCount;
		int randomChance;
		L1ItemInstance item;
		Random random = new Random(System.nanoTime());

		for (L1Drop drop : dropList) {
			// 드롭 아이템의 취득
			itemId = drop.getItemid();
			if (adenarate == 0 && itemId == L1ItemId.ADENA) {
				continue; // 아데나레이트 0으로 드롭이 아데나의 경우는 스르
			}
			if (itemId != L1ItemId.ADENA) {
				continue;
			}

			// 드롭 찬스 판정
			randomChance = random.nextInt(0xf4240) + 1;
			double rateOfMapId = MapsTable.getInstance().getDropRate(
					npc.getMapId());
			double rateOfItem = DropItemTable.getInstance().getDropRate(itemId);
			if (droprate == 0
					|| drop.getChance() * droprate * rateOfMapId * rateOfItem < randomChance) {
				continue;
			}

			// 드롭 개수를 설정
			double amount = DropItemTable.getInstance().getDropAmount(itemId);
			int min = drop.getMin();
			int max = drop.getMax();
			if (amount < 0) {
				min = (int) (min / amount);
				max = (int) (max / amount);
			} else {
				min = (int) (min * amount);
				max = (int) (max * amount);
			}

			itemCount = min;
			addCount = max - min + 1;

			if (addCount > 1) {
				itemCount += random.nextInt(addCount);
			}
			if (itemId == L1ItemId.ADENA) { // 드롭이 아데나의 경우는 아데나레이트를 건다
				if (npc.getMapId() == 410) {
					itemCount = 0;
				} else {
					itemCount *= adenarate;
				}
			}
			if (itemCount < 0) {
				itemCount = 0;
			}
			if (itemCount > 2000000000) {
				itemCount = 2000000000;
			}

			// 아이템의 생성
			item = ItemTable.getInstance().createItem(itemId);
			item.setCount(itemCount);
			// 아이템 격납
			inventory.storeItem(item);
		}
	}

}
