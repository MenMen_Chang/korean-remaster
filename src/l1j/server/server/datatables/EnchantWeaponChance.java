/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */

package l1j.server.server.datatables;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javolution.util.FastMap;

import l1j.server.L1DatabaseFactory;
import l1j.server.server.templates.L1EnchantWeaponChance;
import l1j.server.server.utils.SQLUtil;

public class EnchantWeaponChance {//enchant_weapon_chance
	private static Logger _log = Logger.getLogger(EnchantWeaponChance.class.getName());

	private static EnchantWeaponChance _instance;
	private final FastMap<String, L1EnchantWeaponChance> _weaponName = new FastMap<String, L1EnchantWeaponChance>();
	private final FastMap<Integer, L1EnchantWeaponChance> _weaponIdList = new FastMap<Integer, L1EnchantWeaponChance>();

	public static EnchantWeaponChance getInstance() {
		if (_instance == null) {
			_instance = new EnchantWeaponChance();
		}
		return _instance;
	}

	private EnchantWeaponChance() {
		System.out.print("☞ 무기류인첸 .......................... ");		
		load();
		System.out.println("☞ 정상 완료");	
		
	}

	private void load() {
		Connection con = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con.prepareStatement("SELECT * FROM enchant_weapon_chance");
			rs = pstm.executeQuery();
			fillTable(rs);
		} catch (SQLException e) {
			_log.log(Level.SEVERE, "error while creating enchant_weapon_chance table", e);
		} finally {
			SQLUtil.close(rs);
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}
	
	public static void reload() {
		EnchantWeaponChance oldInstance = _instance;
		_instance = new EnchantWeaponChance();
		if (oldInstance != null)
			oldInstance._weaponName.clear();
			oldInstance._weaponIdList.clear();
	}

	private void fillTable(ResultSet rs) throws SQLException {
		L1EnchantWeaponChance ed  = null;
		while (rs.next()) {
			int weaponId = rs.getInt("weapon_id");
			String weaponname = rs.getString("weapon_name");
			int e0 = rs.getInt("e0");
			int e1 = rs.getInt("e1");
			int e2 = rs.getInt("e2");
			int e3 = rs.getInt("e3");
			int e4 = rs.getInt("e4");
			int e5 = rs.getInt("e5");
			int e6 = rs.getInt("e6");
			int e7 = rs.getInt("e7");
			int e8 = rs.getInt("e8");
			int e9 = rs.getInt("e9");
			int e10 = rs.getInt("e10");
			int e11 = rs.getInt("e11");
			int e12 = rs.getInt("e12");
			int e13 = rs.getInt("e13");
			int e14 = rs.getInt("e14");
			
			ed = new L1EnchantWeaponChance(weaponId, weaponname, e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14);
			
			_weaponName.put(weaponname, ed);
			_weaponIdList.put(weaponId, ed);
		}
		_log.config("무기인첸률 리스트 " + _weaponName.size() + "건 로드");
		SQLUtil.close(rs);
	}
	
	public L1EnchantWeaponChance getTemplate(String name) {
		return _weaponName.get(name);
	}

	public L1EnchantWeaponChance getTemplate(int weaponId) {
		return _weaponIdList.get(weaponId);
	}

}
