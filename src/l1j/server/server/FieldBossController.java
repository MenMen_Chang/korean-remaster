package l1j.server.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Calendar;

import l1j.server.L1DatabaseFactory;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1ItemInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.serverpackets.S_PacketBox;
import l1j.server.server.serverpackets.S_SystemMessage;
import l1j.server.server.utils.L1SpawnUtil;
import l1j.server.server.utils.SQLUtil;
import server.manager.eva;

public class FieldBossController extends Thread {
	
		private static FieldBossController _instance;

		private boolean _FieldBossStart;
		
		public boolean getFieldBossStart() {
			return _FieldBossStart;
		}
		public void setFieldBossStart(boolean FieldBoss) {
			_FieldBossStart = FieldBoss;
		}

		public boolean isGmOpen = false;
		
		public static FieldBossController getInstance() {
			if(_instance == null) {
				_instance = new FieldBossController();
			}
			return _instance;
		}
		
		@Override
			public void run() {
			while (true) {
				try	{
					if(isOpen()){
					Spawn1(); 
					Thread.sleep(120000);
					End();
					}
					if(isOpen2()){
						for (L1PcInstance tempPc : L1World.getInstance().getAllPlayers()) {
							if (tempPc == null)
								continue;
						L1ItemInstance[] item = tempPc.getInventory().findItemsId(16701);
						if (item != null && item.length > 0) {
							for (int o = 0; o < item.length; o++) {
								tempPc.getInventory().removeItem(item[o]);
								tempPc.sendPackets(new S_SystemMessage(tempPc, "얼어붙은 심장이 소멸되었습니다."), true);
								}
							}
						}
						Delete(16701);
						System.out.println("[시스템 알림] 얼어붙은 심장 일괄 삭제 완료");
						Thread.sleep(60000);
					}
				} catch(Exception e){
					e.printStackTrace();
				} finally {
					try{
					Thread.sleep(1000);
				} catch(Exception e){
				}
				}
			}
		}
			
			 private void Spawn1(){
				 try{
				//	 L1SpawnUtil.spawn2(34242, 33370, (short) 4, 45610, 0, 3600*1000, 0);//거인 모닝스타
					 L1SpawnUtil.spawn2(33750, 33358, (short) 4, 45546, 0, 3600*1000, 0);// 도펠겡어 보스
					 

		
				 } catch(Exception e2){
						e2.printStackTrace();
				 }
				}
			 
			 private boolean isOpen() {
				  Calendar calender = Calendar.getInstance();
				  int hour, minute;
				  hour = calender.get(Calendar.HOUR_OF_DAY);
				  minute = calender.get(Calendar.MINUTE);	 
			   if ((hour == 12 && minute == 05)
					   || (hour == 4 && minute == 05)
					   || (hour == 8 && minute == 05)
					   || (hour == 16 && minute == 05)
					   || (hour == 20 && minute == 05)
					   || (hour == 00 && minute == 05)) {
				  return true;
				  }
				  return false;
				 }
			 
			 private boolean isOpen2() {
				  Calendar calender = Calendar.getInstance();
				  int hour, minute;
				  hour = calender.get(Calendar.HOUR_OF_DAY);
				  minute = calender.get(Calendar.MINUTE);	 
			   if (hour == 8 && minute == 59) {
				  return true;
				  }
				  return false;
				 }
			 
				private static void Delete(int itemid) {
					Connection con = null;
					PreparedStatement pstm = null;
					try {
						con = L1DatabaseFactory.getInstance().getConnection();
						pstm = con.prepareStatement("delete FROM character_items WHERE item_id=?");
						pstm.setInt(1, itemid);
						pstm.executeUpdate();
					} catch (Exception e) {
						e.printStackTrace();
					} finally {
						SQLUtil.close(pstm);
						SQLUtil.close(con);
					}
				}
			 
			 /** 종료 **/
			 public void End() {
				 setFieldBossStart(false);
			 }
}