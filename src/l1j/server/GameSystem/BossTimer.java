package l1j.server.GameSystem;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Random;

import l1j.server.Config;
import l1j.server.L1DatabaseFactory;
import l1j.server.GameSystem.EventShop.EventShopList;
import l1j.server.GameSystem.EventShop.EventShopTable;
import l1j.server.GameSystem.EventShop.EventShop_10검;
import l1j.server.GameSystem.EventShop.EventShop_그렘린;
import l1j.server.GameSystem.EventShop.EventShop_다이노스;
import l1j.server.GameSystem.EventShop.EventShop_단테스;
import l1j.server.GameSystem.EventShop.EventShop_단테스_2Day;
import l1j.server.GameSystem.EventShop.EventShop_드래곤;
import l1j.server.GameSystem.EventShop.EventShop_드래곤_2Day;
import l1j.server.GameSystem.EventShop.EventShop_드래곤의큐브;
import l1j.server.GameSystem.EventShop.EventShop_루피주먹;
import l1j.server.GameSystem.EventShop.EventShop_룸티스;
import l1j.server.GameSystem.EventShop.EventShop_바칸스;
import l1j.server.GameSystem.EventShop.EventShop_벚꽃;
import l1j.server.GameSystem.EventShop.EventShop_붉은기사단;
import l1j.server.GameSystem.EventShop.EventShop_붉은사자;
import l1j.server.GameSystem.EventShop.EventShop_셸로브;
import l1j.server.GameSystem.EventShop.EventShop_수렵;
import l1j.server.GameSystem.EventShop.EventShop_스냅퍼의반지;
import l1j.server.GameSystem.EventShop.EventShop_신기한반지;
import l1j.server.GameSystem.EventShop.EventShop_신묘한;
import l1j.server.GameSystem.EventShop.EventShop_신묘한삭제;
import l1j.server.GameSystem.EventShop.EventShop_싸이;
import l1j.server.GameSystem.EventShop.EventShop_아툰기사단;
import l1j.server.GameSystem.EventShop.EventShop_오림;
import l1j.server.GameSystem.EventShop.EventShop_음유성;
import l1j.server.GameSystem.EventShop.EventShop_인나드릴;
import l1j.server.GameSystem.EventShop.EventShop_크리스마스;
import l1j.server.GameSystem.EventShop.EventShop_판도라;
import l1j.server.GameSystem.EventShop.EventShop_할로윈;
import l1j.server.GameSystem.Robot.L1RobotInstance;
import l1j.server.Warehouse.ClanWarehouse;
import l1j.server.Warehouse.PrivateWarehouse;
import l1j.server.Warehouse.WarehouseManager;
import l1j.server.server.Account;
import l1j.server.server.GeneralThreadPool;
import l1j.server.server.clientpackets.C_ItemUSe;
import l1j.server.server.datatables.NpcTable;
import l1j.server.server.model.Broadcaster;
import l1j.server.server.model.L1Inventory;
import l1j.server.server.model.L1Location;
import l1j.server.server.model.L1MobGroupSpawn;
import l1j.server.server.model.L1NpcDeleteTimer;
import l1j.server.server.model.L1Object;
import l1j.server.server.model.L1PcInventory;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1ItemInstance;
import l1j.server.server.model.Instance.L1MerchantInstance;
import l1j.server.server.model.Instance.L1NpcInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.model.Instance.L1TeleporterInstance;
import l1j.server.server.model.gametime.GameTimeClock;
import l1j.server.server.serverpackets.S_MatizAlarm;
import l1j.server.server.serverpackets.S_NpcChatPacket;
import l1j.server.server.serverpackets.S_PacketBox;
import l1j.server.server.templates.L1Npc;
import l1j.server.server.utils.L1SpawnUtil;
import l1j.server.server.utils.SQLUtil;

public class BossTimer implements Runnable {
	private static BossTimer _instance;

	public static BossTimer getInstance() {
		if (_instance == null) {
			_instance = new BossTimer();
		}
		return _instance;
	}

	public boolean 젠사용 = false;
	public boolean isOpen = false;
	public boolean isOpen2 = false;
	
	public boolean nowErzarbe = false;
	public boolean nowsandworm = false;

	public boolean 공지사용 = false;

	private Date day = new Date(System.currentTimeMillis());

	public BossTimer() {
		// super("l1j.server.GameSystem.BossTimer");
		GeneralThreadPool.getInstance().execute(this);
	}

	@Override
	public void run() {
		try {
			// while (true) {
			day.setTime(System.currentTimeMillis());
			boss();
			EventShopCheck();
			fairlyQueen();
			MerchantOneDayBuyReset();
			// Thread.sleep(1000L);
			// }
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		GeneralThreadPool.getInstance().schedule(this, 1000);
	}

	private void MerchantOneDayBuyReset() {
		// TODO 자동 생성된 메소드 스텁
		if (day.getMinutes() == 0 && day.getSeconds() == 0
				&& day.getHours() == 0) {
			L1MerchantInstance.resetOneDayBuy();
			C_ItemUSe.reset시공의항아리횟수();
			C_ItemUSe.reset마빈주머니_계정횟수();
			// C_Shop.reset상점개설계정횟수();
		}
	}

	private boolean QueenAMspawn = false;
	private boolean QueenPMspawn = false;

	private void fairlyQueen() {
		long time = GameTimeClock.getInstance().getGameTime().getSeconds() % 86400;
		// if(time < 0){
		// time=time-time-time;
		// }
		if ((time > 60 * 60 * 9 && time < 60 * 60 * 10)
				|| (time < -60 * 60 * 9 && time > -60 * 60 * 10)) {
			if (!QueenAMspawn) {
				QueenAMspawn = true;
				// 9~12
				fairlyQueenSpawn();
			}
		} else {
			QueenAMspawn = false;
		}

		if ((time > 60 * 60 * 19 && time < 60 * 60 * 20)
				|| (time < -60 * 60 * 19 && time > -60 * 60 * 20)) {
			if (!QueenPMspawn) {
				QueenPMspawn = true;
				// 19~24
				fairlyQueenSpawn();
			}
		} else {
			QueenPMspawn = false;
		}
	}

	private void fairlyQueenSpawn() {
		Random _rnd = new Random(System.nanoTime());
		int delay = _rnd.nextInt(600000 * 3) + 1;
		GeneralThreadPool.getInstance().schedule(new Runnable() {
			@Override
			public void run() {
				Random _rnd = new Random(System.nanoTime());
				int deletetime = (_rnd.nextInt(11) + 10) * 60000;
				L1NpcInstance n = L1SpawnUtil.spawn2(33164, 32284, (short) 4, 70852, 0, deletetime, 0);
				L1MobGroupSpawn.getInstance().doSpawn(n, 107, true, false);
				for (L1NpcInstance npc : n.getMobGroupInfo().getMember()) {
					L1NpcDeleteTimer timer = new L1NpcDeleteTimer(npc, deletetime);
					timer.begin();
				}
			}

		}, delay);
	}

	// private boolean 신묘삭제체크 = false;
	@SuppressWarnings("deprecation")
	public void EventShopCheck() {
		int rh = day.getHours();
		if (day.getMinutes() == 0
				&& day.getSeconds() == 0
				&& (rh == 0 || rh == 2 || rh == 4 || rh == 6 || rh == 8
						|| rh == 10 || rh == 12 || rh == 14 || rh == 16
						|| rh == 18 || rh == 20 || rh == 22)) {
			셸로브스폰();
			if (EventShop_바칸스.진행()) {
				Random _rnd = new Random(System.nanoTime());
				int rnd = _rnd.nextInt(3);
				L1SpawnUtil.spawn2(
						더위소x[rnd][0]
								+ _rnd.nextInt(더위소x[rnd][1] - 더위소x[rnd][0]),
						더위소y[rnd][0]
								+ _rnd.nextInt(더위소y[rnd][1] - 더위소y[rnd][0]),
						(short) 4, 100397, 5, bossdietime, 0);// 더위 먹은 소
				if (rnd == 0)
					L1World.getInstance().broadcastPacketToAll(
							new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
									"화룡의 둥지에 '더위 먹은 소'가 등장했습니다."), true);
				else if (rnd == 1)
					L1World.getInstance().broadcastPacketToAll(
							new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
									"용의 계곡에 '더위 먹은 소'가 등장했습니다."), true);
				else if (rnd == 2)
					L1World.getInstance().broadcastPacketToAll(
							new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
									"사막에 '더위 먹은 소'가 등장했습니다."), true);
				_rnd = null;
			}
			if (rh == 9)
				EventShop_신묘한.신묘리스트삭제();
			else if (rh == 0) {
				if (EventShop_수렵.진행() == true) {
					EventShop_수렵.deleteItem();
				}
			}
		}
		for (L1PcInstance tempPc : L1World.getInstance().getAllPlayers()) {
			try {
				if (tempPc == null || tempPc.getNetConnection() == null)
					continue;
				if (tempPc.getInventory().checkItem(20344))
					RabitHelmTimeCheck(tempPc);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if (공지사용 == true) {
			return;
		}
		long now = System.currentTimeMillis();

		if (day.getDay() == 3) {
			if (day.getMinutes() == 0 && day.getSeconds() == 0 && rh == 9) {
				try {
					for (L1PcInstance pp : L1World.getInstance()
							.getAllPlayers()) {
						if (pp == null || pp.getNetConnection() == null
								|| pp.getNetConnection().getAccount() == null)
							continue;
						pp.getNetConnection().getAccount().Shop_open_count = 0;
					}
					Account.resetShopOpenCount();
				} catch (Exception e) {
				}
			}
		}

		if (Config.DRAGON_2DAY_EVENT) {// 1월 2화 3수 4목 5금 6토 0일?
			if ((day.getDay() == 6 && rh >= 18) || day.getDay() == 0) {
				/*
				 * if(day.getDay() == 3 || day.getDay() == 5){//월요일, 금요일 -> 수요일
				 * 금요일 if(rh >= 18 && rh <= 23){//6시~12시 사이일경우
				 */if (EventShop_드래곤_2Day.진행() == false) {
					Calendar cal = (Calendar) Calendar.getInstance().clone();
					cal.setTimeInMillis(cal.getTimeInMillis() + (3600000 * 30));
					cal.set(Calendar.HOUR_OF_DAY, 0);
					cal.set(Calendar.MINUTE, 0);
					cal.set(Calendar.SECOND, 0);
					Timestamp DelTime = new Timestamp(cal.getTimeInMillis());
					L1Npc npc = NpcTable.getInstance().getTemplate(100764);
					if (npc != null) {
						EventShopTable.Store(npc, 33454, 32794, 4, 6, DelTime);
						EventShopTable.Spawn(100764, 33454, 32794, (short) 4, 6, DelTime);
						L1World.getInstance().broadcastServerMessage(
										"드래곤의 상자 이벤트가 시작되었습니다.\n이벤트는 매주 토요일 저녁6시부터 일요일 자정까지 진행이 됩니다.");
					}
				}
			}
			// }
			// }
		}

		if (Config.RuphyBrave_3DAY_EVENT) {// 1월 2화 3수 4목 5금 6토 0일?
			if ((day.getDay() == 5 && rh >= 18) || day.getDay() == 6
					|| day.getDay() == 0) {// 금요일, 토요일 일요일
				if (EventShop_루피주먹.진행() == false) {
					Calendar cal = (Calendar) Calendar.getInstance().clone();
					cal.setTimeInMillis(cal.getTimeInMillis() + (3600000 * 54));
					cal.set(Calendar.HOUR_OF_DAY, 0);
					cal.set(Calendar.MINUTE, 0);
					cal.set(Calendar.SECOND, 0);
					Timestamp DelTime = new Timestamp(cal.getTimeInMillis());
					L1Npc npc = NpcTable.getInstance().getTemplate(100883);
					if (npc != null) {
						EventShopTable.Store(npc, 33436, 32811, 4, 4, DelTime);
						EventShopTable.Spawn(100883, 33436, 32811, (short) 4,
								4, DelTime);
						L1World.getInstance()
								.broadcastServerMessage(
										"루피의 용기의 주먹 이벤트가 시작되었습니다.\n이벤트는 매주 금요일 오후 6시부터 일요일 자정까지 진행 됩니다.");
					}
				}
			}
		}

		if (Config.DANTES_2DAY_EVENT) {// 1월 2화 3수 4목 5금 6토 0일?
			if (day.getDay() == 2 || day.getDay() == 4) {// 수요일, 토요일 -> 화요일 목요일
				if (rh >= 18 && rh <= 23) {// 6시~12시 사이일경우
					if (EventShop_단테스_2Day.진행() == false) {
						Calendar cal = (Calendar) Calendar.getInstance()
								.clone();
						cal.setTimeInMillis(cal.getTimeInMillis()
								+ (3600000 * 24));
						cal.set(Calendar.HOUR_OF_DAY, 0);
						cal.set(Calendar.MINUTE, 0);
						cal.set(Calendar.SECOND, 0);
						Timestamp DelTime = new Timestamp(cal.getTimeInMillis());
						L1Npc npc = NpcTable.getInstance().getTemplate(100765);
						if (npc != null) {
							EventShopTable.Store(npc, 33436, 32811, 4, 4,
									DelTime);
							EventShopTable.Spawn(100765, 33436, 32811,
									(short) 4, 4, DelTime);
							L1World.getInstance()
									.broadcastServerMessage(
											"단테스의 유물상자 이벤트가 시작되었습니다.\n이벤트는 매주 화요일,목요일 오후 6시부터 12시까지 (6시간) 진행이 됩니다.");
						}
					}
				}
			}
		}

		if (Config.Orim_EVENT) {
			if (day.getDay() == 1 || day.getDay() == 6) {// 월요일, 토요일
				if (rh >= 18 && rh <= 23) {// 6시~12시 사이일경우
					if (EventShop_오림.진행() == false) {
						Calendar cal = (Calendar) Calendar.getInstance()
								.clone();
						cal.setTimeInMillis(cal.getTimeInMillis()
								+ (3600000 * 24));
						cal.set(Calendar.HOUR_OF_DAY, 0);
						cal.set(Calendar.MINUTE, 0);
						cal.set(Calendar.SECOND, 0);
						Timestamp DelTime = new Timestamp(cal.getTimeInMillis());
						L1Npc npc = NpcTable.getInstance().getTemplate(100900);
						if (npc != null) {
							EventShopTable.Store(npc, 33436, 32811, 4, 4,
									DelTime);
							EventShopTable.Spawn(100900, 33436, 32811,
									(short) 4, 4, DelTime);
							L1World.getInstance()
									.broadcastServerMessage(
											"오림의 연구 발표회 이벤트가 시작되었습니다.\n이벤트는 매주 월요일,토요일 오후 6시부터 12시까지 (6시간) 진행이 됩니다.");
						}
					}
				}
			}
		}
		if (day.getDay() == 4 || day.getDay() == 5 || day.getDay() == 6
				|| day.getDay() == 0) {// 토요일, 일요일
			if (Config.할로윈_Event) {
				if (Config.할로윈진행중 == false) {
					System.out.println("이벤트 진행 - 할로윈 (목요일0시~일24시까지)");
					// L1World.getInstance().broadcastPacketToAll(new
					// S_PacketBox(S_PacketBox.GREEN_MESSAGE,
					// "[이벤트 진행] 할로윈 이벤트"), true);
					// L1World.getInstance().broadcastServerMessage("할로윈 이벤트가 시작되었습니다.\n이벤트는 이번 수요일 0시부터 일요일자정 까지 진행이 됩니다.");
					Config.할로윈진행중 = true;
					if (Config.할로윈아루엔피씨 == null) {
						Config.할로윈아루엔피씨 = EventShopTable.드다엔피씨스폰(101029, 33428,
								32797, (short) 4, 4);
					}
					if (Config.할로윈바루엔피씨 == null) {
						Config.할로윈바루엔피씨 = EventShopTable.드다엔피씨스폰(101030, 33426,
								32797, (short) 4, 4);
					}
				}
			} else {
				if (Config.할로윈진행중 == true) {
					System.out.println("이벤트 종료 - 할로윈 (목요일0시~일24시까지)");
					// L1World.getInstance().broadcastPacketToAll(new
					// S_PacketBox(S_PacketBox.GREEN_MESSAGE,
					// "[이벤트 종료] 할로윈 이벤트"), true);
					// L1World.getInstance().broadcastServerMessage("할로윈 이벤트가 종료 되었습니다.");
					Config.할로윈진행중 = false;
					if (Config.할로윈아루엔피씨 != null) {
						Config.할로윈아루엔피씨.deleteMe();
					}
					if (Config.할로윈바루엔피씨 != null) {
						Config.할로윈바루엔피씨.deleteMe();
					}
					EventItemDelete();
				}
			}
		} else {
			if (Config.할로윈진행중 == true) {
				System.out.println("이벤트 종료 - 할로윈 (목요일0시~일24시까지)");
				// L1World.getInstance().broadcastPacketToAll(new
				// S_PacketBox(S_PacketBox.GREEN_MESSAGE, "[이벤트 종료] 할로윈 이벤트"),
				// true);
				// L1World.getInstance().broadcastServerMessage("할로윈 이벤트가 종료 되었습니다.");
				Config.할로윈진행중 = false;
				if (Config.할로윈아루엔피씨 != null) {
					Config.할로윈아루엔피씨.deleteMe();
				}
				if (Config.할로윈바루엔피씨 != null) {
					Config.할로윈바루엔피씨.deleteMe();
				}
				EventItemDelete();
			}
		}

		if ((day.getDay() == 5 && rh >= 20)// 금요일이고 8시 이상이라면
				|| (day.getDay() == 6 || day.getDay() == 0)// 토요일, 일요일
		) {
			if (Config.Chopa_Event) {
				if (Config.초파드랍진행중 == false) {
					System.out.println("이벤트 진행 - 쵸파의 뿔 (금20시~일24시까지)");
					L1World.getInstance().broadcastPacketToAll(
							new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
									"[이벤트 진행] 쵸파의 뿔 이벤트"), true);
					L1World.getInstance()
							.broadcastServerMessage(
									"쵸파의 뿔 이벤트가 시작되었습니다.\n이벤트는 매주 금요일 오후 8시부터 일요일자정 까지 진행이 됩니다.");
					Config.초파드랍진행중 = true;
					if (Config.쵸파엔피씨 == null) {
						Config.쵸파엔피씨 = EventShopTable.드다엔피씨스폰(200000, 33453,
								32793, (short) 4, 6);
					}
				}
			} else {
				if (Config.초파드랍진행중 == true) {
					System.out.println("이벤트 종료 - 쵸파의 뿔(금20시~일24시까지)");
					L1World.getInstance().broadcastPacketToAll(
							new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
									"[이벤트 종료] 쵸파의 뿔 이벤트"), true);
					L1World.getInstance()
							.broadcastServerMessage(
									"쵸파의 뿔 이벤트가 종료 되었습니다.\n이벤트는 매주 금요일 오후 8시부터 일요일자정 까지 진행이 됩니다.");
					Config.초파드랍진행중 = false;
					if (Config.쵸파엔피씨 != null) {
						Config.쵸파엔피씨.deleteMe();
					}
				}
			}
		} else {
			if (Config.초파드랍진행중 == true) {
				System.out.println("이벤트 종료 - 쵸파의 뿔(금20시~일24시까지)");
				L1World.getInstance().broadcastPacketToAll(
						new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
								"[이벤트 종료] 쵸파의 뿔 이벤트"), true);
				L1World.getInstance()
						.broadcastServerMessage(
								"쵸파의 뿔 (이벤트가 종료 되었습니다.\n이벤트는 매주 금요일 오후 8시부터 일요일자정 까지 진행이 됩니다.");
				Config.초파드랍진행중 = false;
				if (Config.쵸파엔피씨 != null) {
					Config.쵸파엔피씨.deleteMe();
				}
			}
		}

		if ((day.getDay() == 5 && rh >= 20)// 금요일이고 8시 이상이라면
				|| (day.getDay() == 6 || day.getDay() == 0)// 토요일, 일요일
		) {
			if (Config.Dragon_3DAY_Event) {
				if (Config.드다드랍진행중 == false) {
					System.out.println("이벤트 진행 - 드래곤의 보물상자(금20시~일24시까지)");
					L1World.getInstance().broadcastPacketToAll(
							new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
									"[이벤트 진행] 드래곤의 보물상자 이벤트"), true);
					L1World.getInstance()
							.broadcastServerMessage(
									"드래곤의 보물상자 이벤트가 시작되었습니다.\n이벤트는 매주 금요일 오후 8시부터 일요일자정 까지 진행이 됩니다.");
					Config.드다드랍진행중 = true;
					if (Config.드다엔피씨 == null) {
						Config.드다엔피씨 = EventShopTable.드다엔피씨스폰(4500167, 33453,
								32793, (short) 4, 6);
					}
				}
			} else {
				if (Config.드다드랍진행중 == true) {
					System.out.println("이벤트 종료 - 드래곤의 보물상자(금20시~일24시까지)");
					L1World.getInstance().broadcastPacketToAll(
							new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
									"[이벤트 종료] 드래곤의 보물상자 이벤트"), true);
					L1World.getInstance()
							.broadcastServerMessage(
									"드래곤의 보물상자 이벤트가 종료 되었습니다.\n이벤트는 매주 금요일 오후 8시부터 일요일자정 까지 진행이 됩니다.");
					Config.드다드랍진행중 = false;
					if (Config.드다엔피씨 != null) {
						Config.드다엔피씨.deleteMe();
					}
				}
			}
		} else {
			if (Config.드다드랍진행중 == true) {
				System.out.println("이벤트 종료 - 드래곤의 보물상자(금20시~일24시까지)");
				L1World.getInstance().broadcastPacketToAll(
						new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
								"[이벤트 종료] 드래곤의 보물상자 이벤트"), true);
				L1World.getInstance()
						.broadcastServerMessage(
								"드래곤의 보물상자 이벤트가 종료 되었습니다.\n이벤트는 매주 금요일 오후 8시부터 일요일자정 까지 진행이 됩니다.");
				Config.드다드랍진행중 = false;
				if (Config.드다엔피씨 != null) {
					Config.드다엔피씨.deleteMe();
				}
			}
		}

		if ((day.getDay() == 5 && rh >= 20)// 금요일이고 8시 이상이라면
				|| (day.getDay() == 6 || day.getDay() == 0)// 토요일, 일요일
		) {
			if (Config.Dragon_14_12_12_Event) {
				if (Config.드다1212드랍진행중 == false) {
					System.out.println("이벤트 진행 - 튼튼한 감자 이벤트(금20시~일24시까지)");
					L1World.getInstance().broadcastPacketToAll(
							new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
									"[이벤트 진행] 튼튼한 감자 이벤트"), true);
					L1World.getInstance()
							.broadcastServerMessage(
									"튼튼한 감자 이벤트가 시작되었습니다.\n이벤트는 매주 금요일 오후 8시부터 일요일자정 까지 진행이 됩니다.");
					Config.드다1212드랍진행중 = true;
					if (Config.드다1212엔피씨 == null) {
						Config.드다1212엔피씨 = EventShopTable.드다엔피씨스폰(101032,
								33453, 32793, (short) 4, 6);
					}
				}
			} else {
				if (Config.드다1212드랍진행중 == true) {
					System.out.println("이벤트 종료 - 튼튼한 감자 이벤트(금20시~일24시까지)");
					L1World.getInstance().broadcastPacketToAll(
							new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
									"[이벤트 종료] 튼튼한 감자 이벤트"), true);
					L1World.getInstance()
							.broadcastServerMessage(
									"튼튼한 감자 이벤트가 종료 되었습니다.\n이벤트는 매주 금요일 오후 8시부터 일요일자정 까지 진행이 됩니다.");
					Config.드다1212드랍진행중 = false;
					if (Config.드다1212엔피씨 != null) {
						Config.드다1212엔피씨.deleteMe();
					}
				}
			}
		} else {
			if (Config.드다1212드랍진행중 == true) {
				System.out.println("이벤트 종료 - 튼튼한 감자 이벤트(금20시~일24시까지)");
				L1World.getInstance().broadcastPacketToAll(
						new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
								"[이벤트 종료] 튼튼한 감자 이벤트"), true);
				L1World.getInstance()
						.broadcastServerMessage(
								"튼튼한 감자 이벤트가 종료 되었습니다.\n이벤트는 매주 금요일 오후 8시부터 일요일자정 까지 진행이 됩니다.");
				Config.드다1212드랍진행중 = false;
				if (Config.드다1212엔피씨 != null) {
					Config.드다1212엔피씨.deleteMe();
				}
			}
		}

		if ((day.getDay() == 5 && rh >= 20)// 금요일이고 8시 이상이라면
				|| (day.getDay() == 6 || day.getDay() == 0)// 토요일, 일요일
		) {
			if (Config.케플리샤_Event) {
				if (Config.케플리샤드랍진행중 == false) {
					System.out.println("이벤트 진행 - 케플리샤의 축복(금20시~일24시까지)");
					L1World.getInstance().broadcastPacketToAll(
							new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
									"[이벤트 진행] 케플리샤의 축복 이벤트"), true);
					L1World.getInstance()
							.broadcastServerMessage(
									"케플리샤의 축복 이벤트가 시작되었습니다.\n이벤트는 매주 금요일 오후 8시부터 일요일자정 까지 진행이 됩니다.");
					Config.케플리샤드랍진행중 = true;
					if (Config.케플리샤엔피씨 == null) {
						Config.케플리샤엔피씨 = EventShopTable.드다엔피씨스폰(101034, 33453,
								32793, (short) 4, 6);
					}
				}
			} else {
				if (Config.케플리샤드랍진행중 == true) {
					System.out.println("이벤트 종료 - 케플리샤의 축복(금20시~일24시까지)");
					L1World.getInstance().broadcastPacketToAll(
							new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
									"[이벤트 종료] 케플리샤의 축복 이벤트"), true);
					L1World.getInstance()
							.broadcastServerMessage(
									"케플리샤의 축복 이벤트가 종료 되었습니다.\n이벤트는 매주 금요일 오후 8시부터 일요일자정 까지 진행이 됩니다.");
					Config.케플리샤드랍진행중 = false;
					if (Config.케플리샤엔피씨 != null) {
						Config.케플리샤엔피씨.deleteMe();
					}
				}
			}
		} else {
			if (Config.케플리샤드랍진행중 == true) {
				System.out.println("이벤트 종료 - 케플리샤의 축복(금20시~일24시까지)");
				L1World.getInstance().broadcastPacketToAll(
						new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
								"[이벤트 종료] 케플리샤의 축복 이벤트"), true);
				L1World.getInstance()
						.broadcastServerMessage(
								"케플리샤의 축복 이벤트가 종료 되었습니다.\n이벤트는 매주 금요일 오후 8시부터 일요일자정 까지 진행이 됩니다.");
				Config.케플리샤드랍진행중 = false;
				if (Config.케플리샤엔피씨 != null) {
					Config.케플리샤엔피씨.deleteMe();
				}
			}
		}

		if (day.getDay() == 4
				|| (day.getDay() == 5 || day.getDay() == 6 || day.getDay() == 0)) {// 토요일,
																					// 일요일
			if (Config.아르카_Event) {
				if (Config.아르카드랍진행중 == false) {
					System.out.println("이벤트 진행 - 아르카의 마법인형(목 0시~일24시까지)");
					Config.아르카드랍진행중 = true;
					if (Config.아르카엔피씨 == null) {
						Config.아르카엔피씨 = EventShopTable.드다엔피씨스폰(4500167, 33448,
								32790, (short) 4, 5);
					}
				}
			} else {
				if (Config.아르카드랍진행중 == true) {
					System.out.println("이벤트 종료 - 아르카의 마법인형(목 0시~일24시까지)");
					Config.아르카드랍진행중 = false;
					if (Config.아르카엔피씨 != null) {
						Config.아르카엔피씨.deleteMe();
					}
				}
			}
		} else {
			if (Config.아르카드랍진행중 == true) {
				System.out.println("이벤트 종료 - 아르카의 마법인형(목 0시~일24시까지)");
				Config.아르카드랍진행중 = false;
				if (Config.아르카엔피씨 != null) {
					Config.아르카엔피씨.deleteMe();
				}
			}
		}

		if ((day.getDay() == 4 && rh >= 20)// 금요일이고 8시 이상이라면
				|| (day.getDay() == 5 || day.getDay() == 6 || day.getDay() == 0)// 토요일,
																				// 일요일
		) {
			if (Config.리퍼_Event) {
				if (Config.리퍼드랍진행중 == false) {
					System.out.println("이벤트 진행 - 그림 리퍼의 유물(목20시~일24시까지)");
					L1World.getInstance().broadcastPacketToAll(
							new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
									"[이벤트 진행] 그림 리퍼의 유물 이벤트"), true);
					L1World.getInstance()
							.broadcastServerMessage(
									"그림 리퍼의 유물 이벤트가 시작되었습니다.\n이벤트는 목요일 오후 8시부터 일요일자정 까지 진행이 됩니다.");
					Config.리퍼드랍진행중 = true;
					if (Config.리퍼엔피씨 == null) {
						Config.리퍼엔피씨 = EventShopTable.드다엔피씨스폰(101036, 33453,
								32793, (short) 4, 6);
					}
				}
			} else {
				if (Config.리퍼드랍진행중 == true) {
					System.out.println("이벤트 종료 - 그림 리퍼의 유물(목20시~일24시까지)");
					L1World.getInstance().broadcastPacketToAll(
							new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
									"[이벤트 종료] 그림 리퍼의 유물 이벤트"), true);
					L1World.getInstance()
							.broadcastServerMessage(
									"그림 리퍼의 유물 이벤트가 종료 되었습니다.\n이벤트는 목요일 오후 8시부터 일요일자정 까지 진행이 됩니다.");
					Config.리퍼드랍진행중 = false;
					if (Config.리퍼엔피씨 != null) {
						Config.리퍼엔피씨.deleteMe();
					}
				}
			}
		} else {
			if (Config.리퍼드랍진행중 == true) {
				System.out.println("이벤트 종료 - 그림 리퍼의 유물(목20시~일24시까지)");
				L1World.getInstance().broadcastPacketToAll(
						new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
								"[이벤트 종료] 그림 리퍼의 유물 이벤트"), true);
				L1World.getInstance()
						.broadcastServerMessage(
								"그림 리퍼의 유물 이벤트가 종료 되었습니다.\n이벤트는 목요일 오후 8시부터 일요일자정 까지 진행이 됩니다.");
				Config.리퍼드랍진행중 = false;
				if (Config.리퍼엔피씨 != null) {
					Config.리퍼엔피씨.deleteMe();
				}
			}
		}

		if ((day.getDay() == 6 && rh >= 20) || (day.getDay() == 0)// 토요일, 일요일
		) {
			if (Config.Dragon_1DAY_Event) {
				if (Config.드다드랍진행중1 == false) {
					System.out.println("이벤트 진행 - 드래곤의 보물상자(일요일 자정 엔피씨 삭제)");
					L1World.getInstance().broadcastPacketToAll(
							new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
									"기란마을에 이벤트상점이 생성 되었습니다."), true);
					Config.드다드랍진행중1 = true;
					if (Config.드다엔피씨 == null) {
						Config.드다엔피씨 = EventShopTable.드다엔피씨스폰(4500167, 33448,
								32789, (short) 4, 5);
					}
				}
			} else {
				if (Config.드다드랍진행중1 == true) {
					System.out.println("이벤트 종료 - 드래곤의 보물상자(일요일 자정 엔피씨 삭제))");
					L1World.getInstance().broadcastPacketToAll(
							new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
									"기란마을에 이벤트상점이 삭제 되었습니다."), true);
					Config.드다드랍진행중1 = false;
					if (Config.드다엔피씨 != null) {
						Config.드다엔피씨.deleteMe();
					}
				}
			}
		} else {
			if (Config.드다드랍진행중1 == true) {
				System.out.println("이벤트 종료 - 드래곤의 보물상자(일요일 자정 엔피씨 삭제))");
				L1World.getInstance().broadcastPacketToAll(
						new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
								"기란마을에 이벤트상점이 생성 되었습니다."), true);
				Config.드다드랍진행중1 = false;
				if (Config.드다엔피씨 != null) {
					Config.드다엔피씨.deleteMe();
				}
			}
		}

		if (day.getDay() == 3 || day.getDay() == 4 || day.getDay() == 5
				|| day.getDay() == 6 || day.getDay() == 0// 토요일, 일요일
		) {
			if (Config.세뱃돈_Event) {
				if (Config.세뱃돈드랍진행중 == false) {
					System.out.println("이벤트 진행 - 설날 이벤트(수 0시~일24시까지)");
					L1World.getInstance().broadcastPacketToAll(
							new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
									"[이벤트 진행] 설날 이벤트"), true);
					L1World.getInstance()
							.broadcastServerMessage(
									"설날 이벤트가 시작되었습니다.\n이벤트는 수요일 0시부터 일요일자정 까지 진행이 됩니다.");
					Config.세뱃돈드랍진행중 = true;
				}
			} else {
				if (Config.세뱃돈드랍진행중 == true) {
					System.out.println("이벤트 종료 - 설날 이벤트(수 0시~일24시까지)");
					L1World.getInstance().broadcastPacketToAll(
							new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
									"[이벤트 종료] 설날 이벤트"), true);
					L1World.getInstance()
							.broadcastServerMessage(
									"설날 이벤트가 종료 되었습니다.\n이벤트는 수요일 오후 0시부터 일요일자정 까지 진행이 됩니다.");
					Config.세뱃돈드랍진행중 = false;
				}
			}
		} else {
			if (Config.세뱃돈드랍진행중 == true) {
				System.out.println("이벤트 종료 - 설날 이벤트(수 0시~일24시까지)");
				L1World.getInstance().broadcastPacketToAll(
						new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
								"[이벤트 종료] 설날 이벤트"), true);
				L1World.getInstance()
						.broadcastServerMessage(
								"설날 이벤트가 종료 되었습니다.\n이벤트는 수요일 오후 0시부터 일요일자정 까지 진행이 됩니다.");
				Config.세뱃돈드랍진행중 = false;
			}
		}

		if ((day.getDay() == 4 && rh >= 20)// 금요일이고 8시 이상이라면
				|| (day.getDay() == 5 || day.getDay() == 6 || day.getDay() == 0)// 토요일,
																				// 일요일
		) {
			if (Config.각반_Event) {
				if (Config.각반드랍진행중 == false) {
					System.out.println("이벤트 진행 - 룸티스(목20시~일24시까지)");
					L1World.getInstance().broadcastPacketToAll(
							new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
									"[이벤트 진행] 룸티스 이벤트"), true);
					L1World.getInstance()
							.broadcastServerMessage(
									"룸티스 이벤트가 시작되었습니다.\n이벤트는 목요일 오후 8시부터 일요일자정 까지 진행이 됩니다.");
					Config.각반드랍진행중 = true;
					/*
					 * if(Config.리퍼엔피씨 == null){ Config.리퍼엔피씨 =
					 * EventShopTable.드다엔피씨스폰(101036, 33453, 32793, (short)4,
					 * 6); }
					 */
				}
			} else {
				if (Config.각반드랍진행중 == true) {
					System.out.println("이벤트 종료 - 각반(목20시~일24시까지)");
					L1World.getInstance().broadcastPacketToAll(
							new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
									"[이벤트 종료] 룸티스 이벤트"), true);
					L1World.getInstance()
							.broadcastServerMessage(
									"룸티스 이벤트가 종료 되었습니다.\n이벤트는 목요일 오후 8시부터 일요일자정 까지 진행이 됩니다.");
					Config.각반드랍진행중 = false;
					/*
					 * if(Config.리퍼엔피씨 != null){ Config.리퍼엔피씨.deleteMe(); }
					 */
				}
			}
		} else {
			if (Config.각반드랍진행중 == true) {
				System.out.println("이벤트 종료 - 그각반(목20시~일24시까지)");
				L1World.getInstance().broadcastPacketToAll(
						new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
								"[이벤트 종료] 룸티스 이벤트"), true);
				L1World.getInstance()
						.broadcastServerMessage(
								"룸티스 이벤트가 종료 되었습니다.\n이벤트는 목요일 오후 8시부터 일요일자정 까지 진행이 됩니다.");
				Config.각반드랍진행중 = false;
				/*
				 * if(Config.리퍼엔피씨 != null){ Config.리퍼엔피씨.deleteMe(); }
				 */
			}
		}

		if ((day.getDay() == 4)// 금요일이고 8시 이상이라면
				|| (day.getDay() == 5 || day.getDay() == 6 || day.getDay() == 0)// 토요일,
																				// 일요일
		) {
			if (Config.강화상자_Event) {
				if (Config.강화상자드랍진행중 == false) {
					System.out.println("이벤트 진행 - 숨겨진 조언자(목요일0시~일24시까지)");
					L1World.getInstance().broadcastPacketToAll(
							new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
									"[이벤트 진행] 숨겨진 조언자 이벤트"), true);
					L1World.getInstance()
							.broadcastServerMessage(
									"숨겨진 조언자 이벤트가 시작되었습니다.\n이벤트는 목요일 0시부터 일요일자정 까지 진행이 됩니다.");
					Config.강화상자드랍진행중 = true;
				}
			} else {
				if (Config.강화상자드랍진행중 == true) {
					System.out.println("이벤트 종료 - 숨겨진 조언자(목요일0시~일24시까지)");
					L1World.getInstance().broadcastPacketToAll(
							new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
									"[이벤트 종료] 숨겨진 조언자 이벤트"), true);
					L1World.getInstance()
							.broadcastServerMessage(
									"숨겨진 조언자 이벤트가 종료 되었습니다.\n이벤트는 목요일 0시부터 일요일자정 까지 진행이 됩니다.");
					Config.강화상자드랍진행중 = false;
				}
			}
		} else {
			if (Config.강화상자드랍진행중 == true) {
				System.out.println("이벤트 종료 - 숨겨진 조언자(목요일0시~일24시까지)");
				L1World.getInstance().broadcastPacketToAll(
						new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
								"[이벤트 종료] 숨겨진 조언자 이벤트"), true);
				L1World.getInstance()
						.broadcastServerMessage(
								"숨겨진 조언자 이벤트가 종료 되었습니다.\n이벤트는 목요일 0시부터 일요일자정 까지 진행이 됩니다.");
				Config.강화상자드랍진행중 = false;
			}
		}

		if ((day.getDay() == 4)// 금요일이고 8시 이상이라면
				|| (day.getDay() == 5 || day.getDay() == 6 || day.getDay() == 0)// 토요일,
																				// 일요일
		) {
			if (Config.룸티스_Event) {
				if (Config.룸티스드랍진행중 == false) {
					System.out.println("이벤트 진행 - 룸티스(목요일0시~일24시까지)");
					L1World.getInstance().broadcastPacketToAll(
							new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
									"[이벤트 진행] 룸티스 이벤트"), true);
					L1World.getInstance()
							.broadcastServerMessage(
									"룸티스 이벤트가 시작되었습니다.\n이벤트는 목요일 0시부터 일요일자정 까지 진행이 됩니다.");
					Config.룸티스드랍진행중 = true;
				}
			} else {
				if (Config.룸티스드랍진행중 == true) {
					System.out.println("이벤트 종료 - 룸티스(목요일0시~일24시까지)");
					L1World.getInstance().broadcastPacketToAll(
							new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
									"[이벤트 종료] 룸티스 이벤트"), true);
					L1World.getInstance()
							.broadcastServerMessage(
									"룸티스 이벤트가 종료 되었습니다.\n이벤트는 목요일 0시부터 일요일자정 까지 진행이 됩니다.");
					Config.룸티스드랍진행중 = false;
				}
			}
		} else {
			if (Config.룸티스드랍진행중 == true) {
				System.out.println("이벤트 종료 - 룸티스(목요일0시~일24시까지)");
				L1World.getInstance().broadcastPacketToAll(
						new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
								"[이벤트 종료] 룸티스 이벤트"), true);
				L1World.getInstance().broadcastServerMessage(
						"룸티스 이벤트가 종료 되었습니다.\n이벤트는 목요일 0시부터 일요일자정 까지 진행이 됩니다.");
				Config.룸티스드랍진행중 = false;
			}
		}

		if ((day.getDay() == 5 || day.getDay() == 6 || day.getDay() == 0)// 토요일,
																			// 일요일
		) {
			if (Config.리퍼선물_Event) {
				if (Config.리퍼2드랍진행중 == false) {
					System.out.println("이벤트 진행 - 리퍼 선물(금~일요일 24시까지)");
					L1World.getInstance().broadcastPacketToAll(
							new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
									"[이벤트 진행] 그림리퍼의 선물 이벤트"), true);
					L1World.getInstance()
							.broadcastServerMessage(
									"그림리퍼의 선물 이벤트가 시작되었습니다.\n이벤트는 금요일 0시부터 일요일자정 까지 진행이 됩니다.");
					Config.리퍼2드랍진행중 = true;
				}
			} else {
				if (Config.리퍼2드랍진행중 == true) {
					System.out.println("이벤트 종료 - 리퍼 선물(금~일요일 24시까지)");
					L1World.getInstance().broadcastPacketToAll(
							new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
									"[이벤트 종료] 그림리퍼의 선물 이벤트"), true);
					L1World.getInstance()
							.broadcastServerMessage(
									"그림리퍼의 선물 이벤트가 종료 되었습니다.\n이벤트는 금요일 0시부터 일요일자정 까지 진행이 됩니다.");
					Config.리퍼2드랍진행중 = false;
				}
			}
		} else {
			if (Config.리퍼2드랍진행중 == true) {
				System.out.println("이벤트 종료 - 리퍼 선물(금~일요일 24시까지)");
				L1World.getInstance().broadcastPacketToAll(
						new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
								"[이벤트 종료] 그림리퍼의 선물 이벤트"), true);
				L1World.getInstance()
						.broadcastServerMessage(
								"그림리퍼의 선물 이벤트가 종료 되었습니다.\n이벤트는 금요일 0시부터 일요일자정 까지 진행이 됩니다.");
				Config.리퍼2드랍진행중 = false;
			}
		}

		boolean swich = false;
		ArrayList<L1NpcInstance> allnpc = EventShopList.getAllEventShop();
		if (allnpc.size() <= 0) {
			return;
		}

		if (EventShop_루피주먹.진행() == true) {
			if (EventShop_루피주먹.남은시간() != null) {
				if (now >= EventShop_루피주먹.남은시간().getTime()) {
					Object[] 드래곤npc = null;
					드래곤npc = EventShop_루피주먹.getEventShopNpc().toArray();
					if (드래곤npc.length > 0) {
						EventShop_루피주먹.진행(false);
						for (Object DelNpc : 드래곤npc) {
							L1NpcInstance npc = (L1NpcInstance) DelNpc;
							npc.deleteMe();
							EventShop_루피주먹.oddEventShopNpc(npc);
							EventShopTable.Delete(EventShop_루피주먹.NPCID);
						}
					}
					L1World.getInstance().broadcastPacketToAll(
							new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
									"루피의 용기의 주먹 주머니 판매가 종료되었습니다."), true);
					L1World.getInstance().broadcastServerMessage(
							"루피의 용기의 주먹 주머니 판매가 종료되었습니다.");
					루피주먹GreenMessage();
					드래곤npc = null;
				} else {
					long time = EventShop_루피주먹.남은시간().getTime() - now;
					if (time >= 1000 * 60 * 10 - 1000
							&& time <= 1000 * 60 * 10 + 1000) {
						L1World.getInstance().broadcastPacketToAll(
								new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
										"루피의 용기의 주먹 주머니 판매가 종료되기 10분 전입니다."),
								true);
						L1World.getInstance().broadcastServerMessage(
								"루피의 용기의 주먹 주머니 판매가 종료되기 10분 전입니다.");
						루피주먹GreenMessage();
					}
					if (time >= 1000 * 60 * 5 - 1000
							&& time <= 1000 * 60 * 5 + 1000) {
						L1World.getInstance().broadcastPacketToAll(
								new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
										"루피의 용기의 주먹 주머니 판매가 종료되기 5분 전입니다."),
								true);
						L1World.getInstance().broadcastServerMessage(
								"루피의 용기의 주먹 주머니 판매가 종료되기 5분 전입니다.");
						루피주먹GreenMessage();
					}
					if (time >= 1000 * 60 - 1000 && time <= 1000 * 60 + 1000) {
						L1World.getInstance().broadcastPacketToAll(
								new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
										"루피의 용기의 주먹 주머니 판매가 종료되기 1분 전입니다."),
								true);
						L1World.getInstance().broadcastServerMessage(
								"루피의 용기의 주먹 주머니 판매가 종료되기 1분 전입니다.");
						루피주먹GreenMessage();
					}
				}
				swich = true;
			}
		}

		if (EventShop_오림.진행() == true) {
			if (EventShop_오림.남은시간() != null) {
				if (now >= EventShop_오림.남은시간().getTime()) {
					Object[] 드래곤npc = null;
					드래곤npc = EventShop_오림.getEventShopNpc().toArray();
					if (드래곤npc.length > 0) {
						EventShop_오림.진행(false);
						for (Object DelNpc : 드래곤npc) {
							L1NpcInstance npc = (L1NpcInstance) DelNpc;
							npc.deleteMe();
							EventShop_오림.oddEventShopNpc(npc);
							EventShopTable.Delete(EventShop_오림.NPCID);
						}
					}
					L1World.getInstance().broadcastPacketToAll(
							new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
									"오림의 주문서 상자 판매가 종료되었습니다."), true);
					L1World.getInstance().broadcastServerMessage(
							"오림의 주문서 상자 판매가 종료되었습니다.");
					오림GreenMessage();
					드래곤npc = null;
				} else {
					long time = EventShop_오림.남은시간().getTime() - now;
					if (time >= 1000 * 60 * 10 - 1000
							&& time <= 1000 * 60 * 10 + 1000) {
						L1World.getInstance().broadcastPacketToAll(
								new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
										"오림의 주문서 상자 판매가 종료되기 10분 전입니다."), true);
						L1World.getInstance().broadcastServerMessage(
								"오림의 주문서 상자 판매가 종료되기 10분 전입니다.");
						오림GreenMessage();
					}
					if (time >= 1000 * 60 * 5 - 1000
							&& time <= 1000 * 60 * 5 + 1000) {
						L1World.getInstance().broadcastPacketToAll(
								new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
										"오림의 주문서 상자 판매가 종료되기 5분 전입니다."), true);
						L1World.getInstance().broadcastServerMessage(
								"오림의 주문서 상자 판매가 종료되기 5분 전입니다.");
						오림GreenMessage();
					}
					if (time >= 1000 * 60 - 1000 && time <= 1000 * 60 + 1000) {
						L1World.getInstance().broadcastPacketToAll(
								new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
										"오림의 주문서 상자 판매가 종료되기 1분 전입니다."), true);
						L1World.getInstance().broadcastServerMessage(
								"오림의 주문서 상자 판매가 종료되기 1분 전입니다.");
						오림GreenMessage();
					}
				}
				swich = true;
			}
		}

		if (EventShop_단테스_2Day.진행() == true) {
			if (EventShop_단테스_2Day.남은시간() != null) {
				if (now >= EventShop_단테스_2Day.남은시간().getTime()) {
					Object[] 드래곤npc = null;
					드래곤npc = EventShop_단테스_2Day.getEventShopNpc().toArray();
					if (드래곤npc.length > 0) {
						EventShop_단테스_2Day.진행(false);
						for (Object DelNpc : 드래곤npc) {
							L1NpcInstance npc = (L1NpcInstance) DelNpc;
							npc.deleteMe();
							EventShop_단테스_2Day.oddEventShopNpc(npc);
							EventShopTable.Delete(EventShop_단테스_2Day.NPCID);
						}
					}
					L1World.getInstance().broadcastPacketToAll(
							new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
									"단테스의 유물상자 판매가 종료되었습니다."), true);
					L1World.getInstance().broadcastServerMessage(
							"단테스의 유물상자 판매가 종료되었습니다.");
					단테스2DayGreenMessage();
					드래곤npc = null;
				} else {
					long time = EventShop_단테스_2Day.남은시간().getTime() - now;
					if (time >= 1000 * 60 * 10 - 1000
							&& time <= 1000 * 60 * 10 + 1000) {
						L1World.getInstance().broadcastPacketToAll(
								new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
										"단테스의 유물상자 판매가 종료되기 10분 전입니다."), true);
						L1World.getInstance().broadcastServerMessage(
								"단테스의 유물상자 판매가 종료되기 10분 전입니다.");
						단테스2DayGreenMessage();
					}
					if (time >= 1000 * 60 * 5 - 1000
							&& time <= 1000 * 60 * 5 + 1000) {
						L1World.getInstance().broadcastPacketToAll(
								new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
										"단테스의 유물상자 판매가 종료되기 5분 전입니다."), true);
						L1World.getInstance().broadcastServerMessage(
								"단테스의 유물상자 판매가 종료되기 5분 전입니다.");
						단테스2DayGreenMessage();
					}
					if (time >= 1000 * 60 - 1000 && time <= 1000 * 60 + 1000) {
						L1World.getInstance().broadcastPacketToAll(
								new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
										"단테스의 유물상자 판매가 종료되기 1분 전입니다."), true);
						L1World.getInstance().broadcastServerMessage(
								"단테스의 유물상자 판매가 종료되기 1분 전입니다.");
						단테스2DayGreenMessage();
					}
				}
				swich = true;
			}
		}
		if (EventShop_드래곤_2Day.진행() == true) {
			if (EventShop_드래곤_2Day.남은시간() != null) {
				if (now >= EventShop_드래곤_2Day.남은시간().getTime()) {
					Object[] 드래곤npc = null;
					드래곤npc = EventShop_드래곤_2Day.getEventShopNpc().toArray();
					if (드래곤npc.length > 0) {
						EventShop_드래곤_2Day.진행(false);
						for (Object DelNpc : 드래곤npc) {
							L1NpcInstance npc = (L1NpcInstance) DelNpc;
							npc.deleteMe();
							EventShop_드래곤_2Day.oddEventShopNpc(npc);
							EventShopTable.Delete(EventShop_드래곤_2Day.NPCID);
						}
					}
					L1World.getInstance().broadcastPacketToAll(
							new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
									"드래곤 상자 판매가 종료되었습니다."), true);
					L1World.getInstance().broadcastServerMessage(
							"드래곤 상자 판매가 종료되었습니다.");
					드다2DayGreenMessage();
					드래곤npc = null;
				} else {
					long time = EventShop_드래곤_2Day.남은시간().getTime() - now;
					if (time >= 1000 * 60 * 10 - 1000
							&& time <= 1000 * 60 * 10 + 1000) {
						L1World.getInstance().broadcastPacketToAll(
								new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
										"드래곤 상자 판매 기간 종료되기 10분 전입니다."), true);
						L1World.getInstance().broadcastServerMessage(
								"드래곤 상자 판매 기간 종료되기 10분 전입니다.");
						드다2DayGreenMessage();
					}
					if (time >= 1000 * 60 * 5 - 1000
							&& time <= 1000 * 60 * 5 + 1000) {
						L1World.getInstance().broadcastPacketToAll(
								new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
										"드래곤 상자 판매 기간 종료되기 5분 전입니다."), true);
						L1World.getInstance().broadcastServerMessage(
								"드래곤 상자 판매 기간 종료되기 5분 전입니다.");
						드다2DayGreenMessage();
					}
					if (time >= 1000 * 60 - 1000 && time <= 1000 * 60 + 1000) {
						L1World.getInstance().broadcastPacketToAll(
								new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
										"드래곤 상자 판매 기간 종료되기 1분 전입니다."), true);
						L1World.getInstance().broadcastServerMessage(
								"드래곤 상자 판매 기간 종료되기 1분 전입니다.");
						드다2DayGreenMessage();
					}
				}
				swich = true;
			}
		}
		if (EventShop_벚꽃.진행() == true) {
			if (EventShop_벚꽃.남은시간() != null) {
				if (now >= EventShop_벚꽃.남은시간().getTime()) {
					Object[] 벚꽃npc = null;
					벚꽃npc = EventShop_벚꽃.getEventShopNpc().toArray();
					if (벚꽃npc.length > 0) {
						EventShop_벚꽃.진행(false);
						for (Object DelNpc : 벚꽃npc) {
							L1NpcInstance npc = (L1NpcInstance) DelNpc;
							npc.deleteMe();
							EventShopTable.Delete(EventShop_벚꽃.NPCID);
							EventShop_벚꽃.oddEventShopNpc(npc);
						}
					}
					L1World.getInstance().broadcastServerMessage(
							"감자캐기 이벤트가 종료되었습니다. 아덴 월드 유저 여러분의 많은 참여에 감사드립니다.");
					벚꽃npc = null;
				} else {
					long time = EventShop_벚꽃.남은시간().getTime() - now;
					if (time >= 1000 * 60 * 60 * 5 - 1000
							&& time <= 1000 * 60 * 60 * 5 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"감자캐기 이벤트가 종료되기 5시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 4 - 1000
							&& time <= 1000 * 60 * 60 * 4 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"감자캐기 이벤트가 종료되기 4시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 3 - 1000
							&& time <= 1000 * 60 * 60 * 3 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"감자캐기 이벤트가 종료되기 3시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 2 - 1000
							&& time <= 1000 * 60 * 60 * 2 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"감자캐기 이벤트가 종료되기 2시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 1 - 1000
							&& time <= 1000 * 60 * 60 * 1 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"감자캐기 이벤트가 종료되기 1시간 전입니다.");
					}
					if (time >= 1000 * 60 * 50 - 1000
							&& time <= 1000 * 60 * 50 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"감자캐기 이벤트가 종료되기 50분 전입니다.");
					}
					if (time >= 1000 * 60 * 40 - 1000
							&& time <= 1000 * 60 * 40 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"감자캐기 이벤트가 종료되기 40분 전입니다.");
					}
					if (time >= 1000 * 60 * 30 - 1000
							&& time <= 1000 * 60 * 30 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"감자캐기 이벤트가 종료되기 30분 전입니다.");
					}
					if (time >= 1000 * 60 * 20 - 1000
							&& time <= 1000 * 60 * 20 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"감자캐기 이벤트가 종료되기 20분 전입니다.");
					}
					if (time >= 1000 * 60 * 10 - 1000
							&& time <= 1000 * 60 * 10 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"감자캐기 이벤트가 종료되기 10분 전입니다.");
					}
					if (time >= 1000 * 60 * 5 - 1000
							&& time <= 1000 * 60 * 5 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"감자캐기 이벤트가 종료되기 5분 전입니다.");
					}
				}
				swich = true;
			}
		}
		if (EventShop_10검.진행() == true) {
			if (EventShop_10검.남은시간() != null) {
				if (now >= EventShop_10검.남은시간().getTime()) {
					Object[] 고퍼드10검npc = null;
					고퍼드10검npc = EventShop_10검.getEventShopNpc().toArray();
					if (고퍼드10검npc.length > 0) {
						EventShop_10검.진행(false);
						for (Object DelNpc : 고퍼드10검npc) {
							L1NpcInstance npc = (L1NpcInstance) DelNpc;
							npc.deleteMe();
							EventShop_10검.oddEventShopNpc(npc);
							EventShopTable.Delete(EventShop_10검.NPCID);
						}
					}
					L1World.getInstance()
							.broadcastServerMessage(
									"내 10검에 자비란 없다!! 이벤트가 종료되었습니다. 아덴 월드 유저 여러분의 많은 참여에 감사드립니다.");
					고퍼드10검npc = null;
				} else {
					long time = EventShop_10검.남은시간().getTime() - now;
					if (time >= 1000 * 60 * 60 * 5 - 1000
							&& time <= 1000 * 60 * 60 * 5 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"내 10검에 자비란 없다!! 이벤트가 종료되기 5시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 4 - 1000
							&& time <= 1000 * 60 * 60 * 4 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"내 10검에 자비란 없다!! 이벤트가 종료되기 4시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 3 - 1000
							&& time <= 1000 * 60 * 60 * 3 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"내 10검에 자비란 없다!! 이벤트가 종료되기 3시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 2 - 1000
							&& time <= 1000 * 60 * 60 * 2 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"내 10검에 자비란 없다!! 이벤트가 종료되기 2시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 1 - 1000
							&& time <= 1000 * 60 * 60 * 1 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"내 10검에 자비란 없다!! 이벤트가 종료되기 1시간 전입니다.");
					}
					if (time >= 1000 * 60 * 50 - 1000
							&& time <= 1000 * 60 * 50 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"내 10검에 자비란 없다!! 이벤트가 종료되기 50분 전입니다.");
					}
					if (time >= 1000 * 60 * 40 - 1000
							&& time <= 1000 * 60 * 40 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"내 10검에 자비란 없다!! 이벤트가 종료되기 40분 전입니다.");
					}
					if (time >= 1000 * 60 * 30 - 1000
							&& time <= 1000 * 60 * 30 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"내 10검에 자비란 없다!! 이벤트가 종료되기 30분 전입니다.");
					}
					if (time >= 1000 * 60 * 20 - 1000
							&& time <= 1000 * 60 * 20 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"내 10검에 자비란 없다!! 이벤트가 종료되기 20분 전입니다.");
					}
					if (time >= 1000 * 60 * 10 - 1000
							&& time <= 1000 * 60 * 10 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"내 10검에 자비란 없다!! 이벤트가 종료되기 10분 전입니다.");
					}
					if (time >= 1000 * 60 * 5 - 1000
							&& time <= 1000 * 60 * 5 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"내 10검에 자비란 없다!! 이벤트가 종료되기 5분 전입니다.");
					}
				}
				swich = true;
			}
		}
		/*
		 * if(EventShop_아인_따스한_시선.진행() == true){ if
		 * (EventShop_아인_따스한_시선.남은시간()!=null){ if(now >=
		 * EventShop_아인_따스한_시선.남은시간().getTime()){ Object[] 아인_따스한_시선npc = null;
		 * 아인_따스한_시선npc = EventShop_아인_따스한_시선.getEventShopNpc().toArray();
		 * if(아인_따스한_시선npc.length>0){ EventShop_아인_따스한_시선.진행(false); for(Object
		 * DelNpc : 아인_따스한_시선npc){ L1NpcInstance npc = (L1NpcInstance)DelNpc;
		 * npc.deleteMe(); EventShop_아인_따스한_시선.oddEventShopNpc(npc);
		 * EventShopTable.Delete(EventShop_아인_따스한_시선.NPCID); } }
		 * L1World.getInstance().broadcastServerMessage(
		 * "아인하사드의 따스한 시선 이벤트가 종료되었습니다. 아덴 월드 유저 여러분의 많은 참여에 감사드립니다.");
		 * 아인_따스한_시선npc = null; }else{ long time =
		 * EventShop_아인_따스한_시선.남은시간().getTime() - now; if(time >=
		 * 1000*60*60*5-1000 && time <= 1000*60*60*5+1000 ){
		 * L1World.getInstance(
		 * ).broadcastServerMessage("아인하사드의 따스한 시선 이벤트가 종료되기 5시간 전입니다."); }
		 * if(time >= 1000*60*60*4-1000 && time <= 1000*60*60*4+1000 ){
		 * L1World.getInstance
		 * ().broadcastServerMessage("아인하사드의 따스한 시선 이벤트가 종료되기 4시간 전입니다."); }
		 * if(time >= 1000*60*60*3-1000 && time <= 1000*60*60*3+1000 ){
		 * L1World.getInstance
		 * ().broadcastServerMessage("아인하사드의 따스한 시선 이벤트가 종료되기 3시간 전입니다."); }
		 * if(time >= 1000*60*60*2-1000 && time <= 1000*60*60*2+1000 ){
		 * L1World.getInstance
		 * ().broadcastServerMessage("아인하사드의 따스한 시선 이벤트가 종료되기 2시간 전입니다."); }
		 * if(time >= 1000*60*60*1-1000 && time <= 1000*60*60*1+1000 ){
		 * L1World.getInstance
		 * ().broadcastServerMessage("아인하사드의 따스한 시선 이벤트가 종료되기 1시간 전입니다."); }
		 * if(time >= 1000*60*50 -1000 && time <= 1000*60*50 +1000 ){
		 * L1World.getInstance
		 * ().broadcastServerMessage("아인하사드의 따스한 시선 이벤트가 종료되기 50분 전입니다."); }
		 * if(time >= 1000*60*40 -1000 && time <= 1000*60*40 +1000 ){
		 * L1World.getInstance
		 * ().broadcastServerMessage("아인하사드의 따스한 시선 이벤트가 종료되기 40분 전입니다."); }
		 * if(time >= 1000*60*30 -1000 && time <= 1000*60*30 +1000 ){
		 * L1World.getInstance
		 * ().broadcastServerMessage("아인하사드의 따스한 시선 이벤트가 종료되기 30분 전입니다."); }
		 * if(time >= 1000*60*20 -1000 && time <= 1000*60*20 +1000 ){
		 * L1World.getInstance
		 * ().broadcastServerMessage("아인하사드의 따스한 시선 이벤트가 종료되기 20분 전입니다."); }
		 * if(time >= 1000*60*10 -1000 && time <= 1000*60*10 +1000 ){
		 * L1World.getInstance
		 * ().broadcastServerMessage("아인하사드의 따스한 시선 이벤트가 종료되기 10분 전입니다."); }
		 * if(time >= 1000*60*5 -1000 && time <= 1000*60*5 +1000 ){
		 * L1World.getInstance
		 * ().broadcastServerMessage("아인하사드의 따스한 시선 이벤트가 종료되기 5분 전입니다."); } }
		 * swich = true; } }
		 */
		if (EventShop_크리스마스.진행() == true) {
			if (EventShop_크리스마스.남은시간() != null) {
				if (now >= EventShop_크리스마스.남은시간().getTime()) {
					Object[] 크리스마스npc = null;
					크리스마스npc = EventShop_크리스마스.getEventShopNpc().toArray();
					if (크리스마스npc.length > 0) {
						EventShop_크리스마스.진행(false);
						for (Object DelNpc : 크리스마스npc) {
							L1NpcInstance npc = (L1NpcInstance) DelNpc;
							npc.deleteMe();
							EventShop_크리스마스.oddEventShopNpc(npc);
							EventShopTable.Delete(EventShop_크리스마스.NPCID);
						}
					}
					L1World.getInstance().broadcastServerMessage(
							"크리스마스 이벤트가 종료되었습니다. 아덴 월드 유저 여러분의 많은 참여에 감사드립니다.");
					크리스마스npc = null;
				} else {
					long time = EventShop_크리스마스.남은시간().getTime() - now;
					if (time >= 1000 * 60 * 60 * 5 - 1000
							&& time <= 1000 * 60 * 60 * 5 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"크리스마스 이벤트가 종료되기 5시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 4 - 1000
							&& time <= 1000 * 60 * 60 * 4 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"크리스마스 이벤트가 종료되기 4시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 3 - 1000
							&& time <= 1000 * 60 * 60 * 3 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"크리스마스 이벤트가 종료되기 3시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 2 - 1000
							&& time <= 1000 * 60 * 60 * 2 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"크리스마스 이벤트가 종료되기 2시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 1 - 1000
							&& time <= 1000 * 60 * 60 * 1 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"크리스마스 이벤트가 종료되기 1시간 전입니다.");
					}
					if (time >= 1000 * 60 * 50 - 1000
							&& time <= 1000 * 60 * 50 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"크리스마스 이벤트가 종료되기 50분 전입니다.");
					}
					if (time >= 1000 * 60 * 40 - 1000
							&& time <= 1000 * 60 * 40 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"크리스마스 이벤트가 종료되기 40분 전입니다.");
					}
					if (time >= 1000 * 60 * 30 - 1000
							&& time <= 1000 * 60 * 30 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"크리스마스 이벤트가 종료되기 30분 전입니다.");
					}
					if (time >= 1000 * 60 * 20 - 1000
							&& time <= 1000 * 60 * 20 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"크리스마스 이벤트가 종료되기 20분 전입니다.");
					}
					if (time >= 1000 * 60 * 10 - 1000
							&& time <= 1000 * 60 * 10 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"크리스마스 이벤트가 종료되기 10분 전입니다.");
					}
					if (time >= 1000 * 60 * 5 - 1000
							&& time <= 1000 * 60 * 5 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"크리스마스 이벤트가 종료되기 5분 전입니다.");
					}
				}
				swich = true;
			}
		}
		if (EventShop_스냅퍼의반지.진행() == true) {
			if (EventShop_스냅퍼의반지.남은시간() != null) {
				if (now >= EventShop_스냅퍼의반지.남은시간().getTime()) {
					Object[] 스냅퍼의반지npc = null;
					스냅퍼의반지npc = EventShop_스냅퍼의반지.getEventShopNpc().toArray();
					if (스냅퍼의반지npc.length > 0) {
						EventShop_스냅퍼의반지.진행(false);
						for (Object DelNpc : 스냅퍼의반지npc) {
							L1NpcInstance npc = (L1NpcInstance) DelNpc;
							npc.deleteMe();
							EventShop_스냅퍼의반지.oddEventShopNpc(npc);
							EventShopTable.Delete(EventShop_스냅퍼의반지.NPCID);
						}
					}
					L1World.getInstance()
							.broadcastServerMessage(
									"스냅퍼의 반지 이벤트가 종료되었습니다. 아덴 월드 유저 여러분의 많은 참여에 감사드립니다.");
					스냅퍼의반지npc = null;
				} else {
					long time = EventShop_스냅퍼의반지.남은시간().getTime() - now;
					if (time >= 1000 * 60 * 60 * 5 - 1000
							&& time <= 1000 * 60 * 60 * 5 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"스냅퍼의 반지 이벤트가 종료되기 5시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 4 - 1000
							&& time <= 1000 * 60 * 60 * 4 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"스냅퍼의 반지 이벤트가 종료되기 4시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 3 - 1000
							&& time <= 1000 * 60 * 60 * 3 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"스냅퍼의 반지 이벤트가 종료되기 3시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 2 - 1000
							&& time <= 1000 * 60 * 60 * 2 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"스냅퍼의 반지 이벤트가 종료되기 2시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 1 - 1000
							&& time <= 1000 * 60 * 60 * 1 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"스냅퍼의 반지 이벤트가 종료되기 1시간 전입니다.");
					}

					if (time >= 1000 * 60 * 50 - 1000
							&& time <= 1000 * 60 * 50 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"스냅퍼의 반지 이벤트가 종료되기 50분 전입니다.");
					}
					if (time >= 1000 * 60 * 40 - 1000
							&& time <= 1000 * 60 * 40 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"스냅퍼의 반지 이벤트가 종료되기 40분 전입니다.");
					}
					if (time >= 1000 * 60 * 30 - 1000
							&& time <= 1000 * 60 * 30 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"스냅퍼의 반지 이벤트가 종료되기 30분 전입니다.");
					}
					if (time >= 1000 * 60 * 20 - 1000
							&& time <= 1000 * 60 * 20 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"스냅퍼의 반지 이벤트가 종료되기 20분 전입니다.");
					}
					if (time >= 1000 * 60 * 10 - 1000
							&& time <= 1000 * 60 * 10 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"스냅퍼의 반지 이벤트가 종료되기 10분 전입니다.");
					}
					if (time >= 1000 * 60 * 5 - 1000
							&& time <= 1000 * 60 * 5 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"스냅퍼의 반지 이벤트가 종료되기 5분 전입니다.");
					}
				}
				swich = true;
			}
		}

		if (EventShop_단테스.진행() == true) {
			if (EventShop_단테스.남은시간() != null) {
				if (now >= EventShop_단테스.남은시간().getTime()) {
					Object[] 단테스npc = null;
					단테스npc = EventShop_단테스.getEventShopNpc().toArray();
					if (단테스npc.length > 0) {
						EventShop_단테스.진행(false);
						for (Object DelNpc : 단테스npc) {
							L1NpcInstance npc = (L1NpcInstance) DelNpc;
							npc.deleteMe();
							EventShop_단테스.oddEventShopNpc(npc);
							EventShopTable.Delete(EventShop_단테스.NPCID);
						}
					}
					L1World.getInstance().broadcastServerMessage(
							"단테스 이벤트가 종료되었습니다. 아덴 월드 유저 여러분의 많은 참여에 감사드립니다.");
					단테스npc = null;
				} else {
					long time = EventShop_단테스.남은시간().getTime() - now;
					if (time >= 1000 * 60 * 60 * 5 - 1000
							&& time <= 1000 * 60 * 60 * 5 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"단테스 이벤트가 종료되기 5시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 4 - 1000
							&& time <= 1000 * 60 * 60 * 4 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"단테스 이벤트가 종료되기 4시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 3 - 1000
							&& time <= 1000 * 60 * 60 * 3 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"단테스 이벤트가 종료되기 3시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 2 - 1000
							&& time <= 1000 * 60 * 60 * 2 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"단테스 이벤트가 종료되기 2시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 1 - 1000
							&& time <= 1000 * 60 * 60 * 1 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"단테스 이벤트가 종료되기 1시간 전입니다.");
					}
					if (time >= 1000 * 60 * 50 - 1000
							&& time <= 1000 * 60 * 50 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"단테스 이벤트가 종료되기 50분 전입니다.");
					}
					if (time >= 1000 * 60 * 40 - 1000
							&& time <= 1000 * 60 * 40 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"단테스 이벤트가 종료되기 40분 전입니다.");
					}
					if (time >= 1000 * 60 * 30 - 1000
							&& time <= 1000 * 60 * 30 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"단테스 이벤트가 종료되기 30분 전입니다.");
					}
					if (time >= 1000 * 60 * 20 - 1000
							&& time <= 1000 * 60 * 20 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"단테스 이벤트가 종료되기 20분 전입니다.");
					}
					if (time >= 1000 * 60 * 10 - 1000
							&& time <= 1000 * 60 * 10 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"단테스 이벤트가 종료되기 10분 전입니다.");
					}
					if (time >= 1000 * 60 * 5 - 1000
							&& time <= 1000 * 60 * 5 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"단테스 이벤트가 종료되기 5분 전입니다.");
					}
				}
				swich = true;
			}
		}
		if (EventShop_붉은사자.진행() == true) {
			if (EventShop_붉은사자.남은시간() != null) {
				if (now >= EventShop_붉은사자.남은시간().getTime()) {
					Object[] 붉은사자npc = null;
					붉은사자npc = EventShop_붉은사자.getEventShopNpc().toArray();
					if (붉은사자npc.length > 0) {
						EventShop_붉은사자.진행(false);
						for (Object DelNpc : 붉은사자npc) {
							L1NpcInstance npc = (L1NpcInstance) DelNpc;
							npc.deleteMe();
							EventShop_붉은사자.oddEventShopNpc(npc);
							EventShopTable.Delete(EventShop_붉은사자.NPCID);
						}
					}
					L1World.getInstance()
							.broadcastServerMessage(
									"붉은 사자의 투지 이벤트가 종료되었습니다. 아덴 월드 유저 여러분의 많은 참여에 감사드립니다.");
					붉은사자npc = null;
				} else {
					long time = EventShop_붉은사자.남은시간().getTime() - now;
					if (time >= 1000 * 60 * 60 * 5 - 1000
							&& time <= 1000 * 60 * 60 * 5 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"붉은 사자의 투지 이벤트가 종료되기 5시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 4 - 1000
							&& time <= 1000 * 60 * 60 * 4 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"붉은 사자의 투지 이벤트가 종료되기 4시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 3 - 1000
							&& time <= 1000 * 60 * 60 * 3 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"붉은 사자의 투지 이벤트가 종료되기 3시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 2 - 1000
							&& time <= 1000 * 60 * 60 * 2 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"붉은 사자의 투지 이벤트가 종료되기 2시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 1 - 1000
							&& time <= 1000 * 60 * 60 * 1 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"붉은 사자의 투지 이벤트가 종료되기 1시간 전입니다.");
					}

					if (time >= 1000 * 60 * 50 - 1000
							&& time <= 1000 * 60 * 50 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"붉은 사자의 투지 이벤트가 종료되기 50분 전입니다.");
					}
					if (time >= 1000 * 60 * 40 - 1000
							&& time <= 1000 * 60 * 40 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"붉은 사자의 투지 이벤트가 종료되기 40분 전입니다.");
					}
					if (time >= 1000 * 60 * 30 - 1000
							&& time <= 1000 * 60 * 30 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"붉은 사자의 투지 이벤트가 종료되기 30분 전입니다.");
					}
					if (time >= 1000 * 60 * 20 - 1000
							&& time <= 1000 * 60 * 20 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"붉은 사자의 투지 이벤트가 종료되기 20분 전입니다.");
					}
					if (time >= 1000 * 60 * 10 - 1000
							&& time <= 1000 * 60 * 10 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"붉은 사자의 투지 이벤트가 종료되기 10분 전입니다.");
					}
					if (time >= 1000 * 60 * 5 - 1000
							&& time <= 1000 * 60 * 5 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"붉은 사자의 투지 이벤트가 종료되기 5분 전입니다.");
					}
				}
				swich = true;
			}
		}
		if (EventShop_붉은기사단.진행() == true) {
			if (EventShop_붉은기사단.남은시간() != null) {
				if (now >= EventShop_붉은기사단.남은시간().getTime()) {
					Object[] 붉은기사단npc = null;
					붉은기사단npc = EventShop_붉은기사단.getEventShopNpc().toArray();
					if (붉은기사단npc.length > 0) {
						EventShop_붉은기사단.진행(false);
						for (Object DelNpc : 붉은기사단npc) {
							L1NpcInstance npc = (L1NpcInstance) DelNpc;
							npc.deleteMe();
							EventShop_붉은기사단.oddEventShopNpc(npc);
							EventShopTable.Delete(EventShop_붉은기사단.NPCID);
						}
					}
					L1World.getInstance()
							.broadcastServerMessage(
									"붉은기사단의 보급물자 수색작전 이벤트가 종료되었습니다. 아덴 월드 유저 여러분의 많은 참여에 감사드립니다.");
					붉은기사단npc = null;
				} else {
					long time = EventShop_붉은기사단.남은시간().getTime() - now;
					if (time >= 1000 * 60 * 60 * 5 - 1000
							&& time <= 1000 * 60 * 60 * 5 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"붉은기사단의 보급물자 수색작전 이벤트가 종료되기 5시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 4 - 1000
							&& time <= 1000 * 60 * 60 * 4 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"붉은기사단의 보급물자 수색작전 이벤트가 종료되기 4시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 3 - 1000
							&& time <= 1000 * 60 * 60 * 3 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"붉은기사단의 보급물자 수색작전 이벤트가 종료되기 3시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 2 - 1000
							&& time <= 1000 * 60 * 60 * 2 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"붉은기사단의 보급물자 수색작전 이벤트가 종료되기 2시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 1 - 1000
							&& time <= 1000 * 60 * 60 * 1 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"붉은기사단의 보급물자 수색작전 이벤트가 종료되기 1시간 전입니다.");
					}

					if (time >= 1000 * 60 * 50 - 1000
							&& time <= 1000 * 60 * 50 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"붉은기사단의 보급물자 수색작전 이벤트가 종료되기 50분 전입니다.");
					}
					if (time >= 1000 * 60 * 40 - 1000
							&& time <= 1000 * 60 * 40 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"붉은기사단의 보급물자 수색작전 이벤트가 종료되기 40분 전입니다.");
					}
					if (time >= 1000 * 60 * 30 - 1000
							&& time <= 1000 * 60 * 30 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"붉은기사단의 보급물자 수색작전 이벤트가 종료되기 30분 전입니다.");
					}
					if (time >= 1000 * 60 * 20 - 1000
							&& time <= 1000 * 60 * 20 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"붉은기사단의 보급물자 수색작전 이벤트가 종료되기 20분 전입니다.");
					}
					if (time >= 1000 * 60 * 10 - 1000
							&& time <= 1000 * 60 * 10 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"붉은기사단의 보급물자 수색작전 이벤트가 종료되기 10분 전입니다.");
					}
					if (time >= 1000 * 60 * 5 - 1000
							&& time <= 1000 * 60 * 5 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"붉은기사단의 보급물자 수색작전 이벤트가 종료되기 5분 전입니다.");
					}
				}
				swich = true;
			}
		}
		if (EventShop_다이노스.진행() == true) {
			if (EventShop_다이노스.남은시간() != null) {
				if (now >= EventShop_다이노스.남은시간().getTime()) {
					Object[] 다이노스npc = null;
					다이노스npc = EventShop_다이노스.getEventShopNpc().toArray();
					if (다이노스npc.length > 0) {
						EventShop_다이노스.진행(false);
						for (Object DelNpc : 다이노스npc) {
							L1NpcInstance npc = (L1NpcInstance) DelNpc;
							npc.deleteMe();
							EventShop_다이노스.oddEventShopNpc(npc);
							EventShopTable.Delete(EventShop_다이노스.NPCID);
						}
					}
					L1World.getInstance().broadcastServerMessage(
							"다이노스 이벤트가 종료되었습니다. 아덴 월드 유저 여러분의 많은 참여에 감사드립니다.");
					다이노스npc = null;
				} else {
					long time = EventShop_다이노스.남은시간().getTime() - now;
					if (time >= 1000 * 60 * 60 * 5 - 1000
							&& time <= 1000 * 60 * 60 * 5 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"다이노스 이벤트가 종료되기 5시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 4 - 1000
							&& time <= 1000 * 60 * 60 * 4 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"다이노스 이벤트가 종료되기 4시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 3 - 1000
							&& time <= 1000 * 60 * 60 * 3 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"다이노스 이벤트가 종료되기 3시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 2 - 1000
							&& time <= 1000 * 60 * 60 * 2 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"다이노스 이벤트가 종료되기 2시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 1 - 1000
							&& time <= 1000 * 60 * 60 * 1 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"다이노스 이벤트가 종료되기 1시간 전입니다.");
					}

					if (time >= 1000 * 60 * 50 - 1000
							&& time <= 1000 * 60 * 50 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"다이노스 이벤트가 종료되기 50분 전입니다.");
					}
					if (time >= 1000 * 60 * 40 - 1000
							&& time <= 1000 * 60 * 40 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"다이노스 이벤트가 종료되기 40분 전입니다.");
					}
					if (time >= 1000 * 60 * 30 - 1000
							&& time <= 1000 * 60 * 30 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"다이노스 이벤트가 종료되기 30분 전입니다.");
					}
					if (time >= 1000 * 60 * 20 - 1000
							&& time <= 1000 * 60 * 20 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"다이노스 이벤트가 종료되기 20분 전입니다.");
					}
					if (time >= 1000 * 60 * 10 - 1000
							&& time <= 1000 * 60 * 10 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"다이노스 이벤트가 종료되기 10분 전입니다.");
					}
					if (time >= 1000 * 60 * 5 - 1000
							&& time <= 1000 * 60 * 5 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"다이노스 이벤트가 종료되기 5분 전입니다.");
					}
				}
				swich = true;
			}
		}
		if (EventShop_할로윈.진행() == true) {
			if (EventShop_할로윈.남은시간() != null) {
				if (now >= EventShop_할로윈.남은시간().getTime()) {
					Object[] 할로윈npc = null;
					할로윈npc = EventShop_할로윈.getEventShopNpc().toArray();
					if (할로윈npc.length > 0) {
						EventShop_할로윈.진행(false);
						for (Object DelNpc : 할로윈npc) {
							L1NpcInstance npc = (L1NpcInstance) DelNpc;
							npc.deleteMe();
							EventShop_할로윈.oddEventShopNpc(npc);
							EventShopTable.Delete(EventShop_할로윈.NPCID);
						}
					}
					L1World.getInstance().broadcastServerMessage(
							"할로윈 이벤트가 종료되었습니다. 아덴 월드 유저 여러분의 많은 참여에 감사드립니다.");
					할로윈npc = null;
				} else {
					long time = EventShop_할로윈.남은시간().getTime() - now;
					if (time >= 1000 * 60 * 60 * 5 - 1000
							&& time <= 1000 * 60 * 60 * 5 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"할로윈 이벤트가 종료되기 5시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 4 - 1000
							&& time <= 1000 * 60 * 60 * 4 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"할로윈 이벤트가 종료되기 4시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 3 - 1000
							&& time <= 1000 * 60 * 60 * 3 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"할로윈 이벤트가 종료되기 3시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 2 - 1000
							&& time <= 1000 * 60 * 60 * 2 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"할로윈 이벤트가 종료되기 2시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 1 - 1000
							&& time <= 1000 * 60 * 60 * 1 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"할로윈 이벤트가 종료되기 1시간 전입니다.");
					}

					if (time >= 1000 * 60 * 50 - 1000
							&& time <= 1000 * 60 * 50 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"할로윈 이벤트가 종료되기 50분 전입니다.");
					}
					if (time >= 1000 * 60 * 40 - 1000
							&& time <= 1000 * 60 * 40 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"할로윈 이벤트가 종료되기 40분 전입니다.");
					}
					if (time >= 1000 * 60 * 30 - 1000
							&& time <= 1000 * 60 * 30 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"할로윈 이벤트가 종료되기 30분 전입니다.");
					}
					if (time >= 1000 * 60 * 20 - 1000
							&& time <= 1000 * 60 * 20 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"할로윈 이벤트가 종료되기 20분 전입니다.");
					}
					if (time >= 1000 * 60 * 10 - 1000
							&& time <= 1000 * 60 * 10 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"할로윈 이벤트가 종료되기 10분 전입니다.");
					}
					if (time >= 1000 * 60 * 5 - 1000
							&& time <= 1000 * 60 * 5 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"할로윈 이벤트가 종료되기 5분 전입니다.");
					}
				}
				swich = true;
			}
		}
		if (EventShop_싸이.진행() == true) {
			if (EventShop_싸이.남은시간() != null) {
				if (now >= EventShop_싸이.남은시간().getTime()) {
					Object[] 싸이npc = null;
					싸이npc = EventShop_싸이.getEventShopNpc().toArray();
					if (싸이npc.length > 0) {
						EventShop_싸이.진행(false);
						for (Object DelNpc : 싸이npc) {
							L1NpcInstance npc = (L1NpcInstance) DelNpc;
							npc.deleteMe();
							EventShop_싸이.oddEventShopNpc(npc);
							EventShopTable.Delete(EventShop_싸이.NPCID);
						}
					}
					L1World.getInstance().broadcastServerMessage(
							"싸이 이벤트가 종료되었습니다. 아덴 월드 유저 여러분의 많은 참여에 감사드립니다.");
					싸이npc = null;
				} else {
					long time = EventShop_싸이.남은시간().getTime() - now;
					if (time >= 1000 * 60 * 60 * 5 - 1000
							&& time <= 1000 * 60 * 60 * 5 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"싸이 이벤트가 종료되기 5시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 4 - 1000
							&& time <= 1000 * 60 * 60 * 4 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"싸이 이벤트가 종료되기 4시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 3 - 1000
							&& time <= 1000 * 60 * 60 * 3 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"싸이 이벤트가 종료되기 3시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 2 - 1000
							&& time <= 1000 * 60 * 60 * 2 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"싸이 이벤트가 종료되기 2시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 1 - 1000
							&& time <= 1000 * 60 * 60 * 1 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"싸이 이벤트가 종료되기 1시간 전입니다.");
					}

					if (time >= 1000 * 60 * 50 - 1000
							&& time <= 1000 * 60 * 50 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"싸이 이벤트가 종료되기 50분 전입니다.");
					}
					if (time >= 1000 * 60 * 40 - 1000
							&& time <= 1000 * 60 * 40 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"싸이 이벤트가 종료되기 40분 전입니다.");
					}
					if (time >= 1000 * 60 * 30 - 1000
							&& time <= 1000 * 60 * 30 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"싸이 이벤트가 종료되기 30분 전입니다.");
					}
					if (time >= 1000 * 60 * 20 - 1000
							&& time <= 1000 * 60 * 20 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"싸이 이벤트가 종료되기 20분 전입니다.");
					}
					if (time >= 1000 * 60 * 10 - 1000
							&& time <= 1000 * 60 * 10 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"싸이 이벤트가 종료되기 10분 전입니다.");
					}
					if (time >= 1000 * 60 * 5 - 1000
							&& time <= 1000 * 60 * 5 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"싸이 이벤트가 종료되기 5분 전입니다.");
					}
				}
				swich = true;
			}
		}
		if (EventShop_음유성.진행() == true) {
			if (EventShop_음유성.남은시간() != null) {
				if (now >= EventShop_음유성.남은시간().getTime()) {
					Object[] 음유성npc = null;
					음유성npc = EventShop_음유성.getEventShopNpc().toArray();
					if (음유성npc.length > 0) {
						EventShop_음유성.진행(false);
						for (Object DelNpc : 음유성npc) {
							L1NpcInstance npc = (L1NpcInstance) DelNpc;
							npc.deleteMe();
							EventShop_음유성.oddEventShopNpc(npc);
							EventShopTable.Delete(EventShop_음유성.NPCID);
						}
					}
					L1World.getInstance().broadcastServerMessage(
							"음유성 이벤트가 종료되었습니다. 아덴 월드 유저 여러분의 많은 참여에 감사드립니다.");
					음유성npc = null;
				} else {
					long time = EventShop_음유성.남은시간().getTime() - now;
					if (time >= 1000 * 60 * 60 * 5 - 1000
							&& time <= 1000 * 60 * 60 * 5 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"음유성 이벤트가 종료되기 5시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 4 - 1000
							&& time <= 1000 * 60 * 60 * 4 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"음유성 이벤트가 종료되기 4시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 3 - 1000
							&& time <= 1000 * 60 * 60 * 3 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"음유성 이벤트가 종료되기 3시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 2 - 1000
							&& time <= 1000 * 60 * 60 * 2 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"음유성 이벤트가 종료되기 2시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 1 - 1000
							&& time <= 1000 * 60 * 60 * 1 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"음유성 이벤트가 종료되기 1시간 전입니다.");
					}

					if (time >= 1000 * 60 * 50 - 1000
							&& time <= 1000 * 60 * 50 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"음유성 이벤트가 종료되기 50분 전입니다.");
					}
					if (time >= 1000 * 60 * 40 - 1000
							&& time <= 1000 * 60 * 40 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"음유성 이벤트가 종료되기 40분 전입니다.");
					}
					if (time >= 1000 * 60 * 30 - 1000
							&& time <= 1000 * 60 * 30 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"음유성 이벤트가 종료되기 30분 전입니다.");
					}
					if (time >= 1000 * 60 * 20 - 1000
							&& time <= 1000 * 60 * 20 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"음유성 이벤트가 종료되기 20분 전입니다.");
					}
					if (time >= 1000 * 60 * 10 - 1000
							&& time <= 1000 * 60 * 10 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"음유성 이벤트가 종료되기 10분 전입니다.");
					}
					if (time >= 1000 * 60 * 5 - 1000
							&& time <= 1000 * 60 * 5 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"음유성 이벤트가 종료되기 5분 전입니다.");
					}
				}
				swich = true;
			}
		}
		if (EventShop_바칸스.진행() == true) {
			if (EventShop_바칸스.남은시간() != null) {
				if (now >= EventShop_바칸스.남은시간().getTime()) {
					Object[] 바칸스npc = null;
					바칸스npc = EventShop_바칸스.getEventShopNpc().toArray();
					if (바칸스npc.length > 0) {
						EventShop_바칸스.진행(false);
						for (Object DelNpc : 바칸스npc) {
							L1NpcInstance npc = (L1NpcInstance) DelNpc;
							npc.deleteMe();
							EventShop_바칸스.oddEventShopNpc(npc);
							EventShopTable.Delete(EventShop_바칸스.NPCID);
						}
					}
					L1World.getInstance().broadcastServerMessage(
							"바칸스 이벤트가 종료되었습니다. 아덴 월드 유저 여러분의 많은 참여에 감사드립니다.");
					바칸스npc = null;
				} else {
					long time = EventShop_바칸스.남은시간().getTime() - now;
					if (time >= 1000 * 60 * 60 * 5 - 1000
							&& time <= 1000 * 60 * 60 * 5 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"바칸스 이벤트가 종료되기 5시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 4 - 1000
							&& time <= 1000 * 60 * 60 * 4 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"바칸스 이벤트가 종료되기 4시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 3 - 1000
							&& time <= 1000 * 60 * 60 * 3 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"바칸스 이벤트가 종료되기 3시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 2 - 1000
							&& time <= 1000 * 60 * 60 * 2 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"바칸스 이벤트가 종료되기 2시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 1 - 1000
							&& time <= 1000 * 60 * 60 * 1 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"바칸스 이벤트가 종료되기 1시간 전입니다.");
					}

					if (time >= 1000 * 60 * 50 - 1000
							&& time <= 1000 * 60 * 50 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"바칸스 이벤트가 종료되기 50분 전입니다.");
					}
					if (time >= 1000 * 60 * 40 - 1000
							&& time <= 1000 * 60 * 40 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"바칸스 이벤트가 종료되기 40분 전입니다.");
					}
					if (time >= 1000 * 60 * 30 - 1000
							&& time <= 1000 * 60 * 30 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"바칸스 이벤트가 종료되기 30분 전입니다.");
					}
					if (time >= 1000 * 60 * 20 - 1000
							&& time <= 1000 * 60 * 20 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"바칸스 이벤트가 종료되기 20분 전입니다.");
					}
					if (time >= 1000 * 60 * 10 - 1000
							&& time <= 1000 * 60 * 10 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"바칸스 이벤트가 종료되기 10분 전입니다.");
					}
					if (time >= 1000 * 60 * 5 - 1000
							&& time <= 1000 * 60 * 5 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"바칸스 이벤트가 종료되기 5분 전입니다.");
					}
				}
				swich = true;
			}
		}
		if (EventShop_드래곤의큐브.진행() == true) {
			if (EventShop_드래곤의큐브.남은시간() != null) {
				if (now >= EventShop_드래곤의큐브.남은시간().getTime()) {
					Object[] 드큐npc = null;
					드큐npc = EventShop_드래곤의큐브.getEventShopNpc().toArray();
					if (드큐npc.length > 0) {
						EventShop_드래곤의큐브.진행(false);
						for (Object DelNpc : 드큐npc) {
							L1NpcInstance npc = (L1NpcInstance) DelNpc;
							npc.deleteMe();
							EventShop_드래곤의큐브.oddEventShopNpc(npc);
							EventShopTable.Delete(EventShop_드래곤의큐브.NPCID);
						}
					}
					L1World.getInstance().broadcastServerMessage(
							"드래곤의 큐브 이벤트가 종료되었습니다. 많은 참여 감사드립니다.");
					드큐npc = null;
				} else {
					long time = EventShop_드래곤의큐브.남은시간().getTime() - now;
					if (time >= 1000 * 60 * 60 * 5 - 1000
							&& time <= 1000 * 60 * 60 * 5 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"드래곤의 큐브 이벤트가 종료되기 5시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 4 - 1000
							&& time <= 1000 * 60 * 60 * 4 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"드래곤의 큐브 이벤트가 종료되기 4시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 3 - 1000
							&& time <= 1000 * 60 * 60 * 3 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"드래곤의 큐브 이벤트가 종료되기 3시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 2 - 1000
							&& time <= 1000 * 60 * 60 * 2 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"드래곤의 큐브 이벤트가 종료되기 2시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 1 - 1000
							&& time <= 1000 * 60 * 60 * 1 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"드래곤의 큐브 이벤트가 종료되기 1시간 전입니다.");
					}

					if (time >= 1000 * 60 * 50 - 1000
							&& time <= 1000 * 60 * 50 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"드래곤의 큐브 이벤트가 종료되기 50분 전입니다.");
					}
					if (time >= 1000 * 60 * 40 - 1000
							&& time <= 1000 * 60 * 40 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"드래곤의 큐브 이벤트가 종료되기 40분 전입니다.");
					}
					if (time >= 1000 * 60 * 30 - 1000
							&& time <= 1000 * 60 * 30 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"드래곤의 큐브 이벤트가 종료되기 30분 전입니다.");
					}
					if (time >= 1000 * 60 * 20 - 1000
							&& time <= 1000 * 60 * 20 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"드래곤의 큐브 이벤트가 종료되기 20분 전입니다.");
					}
					if (time >= 1000 * 60 * 10 - 1000
							&& time <= 1000 * 60 * 10 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"드래곤의 큐브 이벤트가 종료되기 10분 전입니다.");
					}
					if (time >= 1000 * 60 * 5 - 1000
							&& time <= 1000 * 60 * 5 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"드래곤의 큐브 이벤트가 종료되기 5분 전입니다.");
					}
				}
				swich = true;
			}
		}
		if (EventShop_수렵.진행() == true) {
			if (EventShop_수렵.남은시간() != null) {
				if (now >= EventShop_수렵.남은시간().getTime()) {
					Object[] 수렵npc = null;
					수렵npc = EventShop_수렵.getEventShopNpc().toArray();
					if (수렵npc.length > 0) {
						EventShop_수렵.진행(false);
						for (Object DelNpc : 수렵npc) {
							L1NpcInstance npc = (L1NpcInstance) DelNpc;
							npc.deleteMe();
							EventShop_수렵.oddEventShopNpc(npc);
							EventShopTable.Delete(EventShop_수렵.NPCID);
							EventShop_수렵.deleteItem();
						}
					}
					L1World.getInstance().broadcastServerMessage(
							"수렵 이벤트가 종료되었습니다. 많은 참여 감사드립니다.");
					수렵npc = null;
				} else {
					long time = EventShop_수렵.남은시간().getTime() - now;
					if (time >= 1000 * 60 * 60 * 5 - 1000
							&& time <= 1000 * 60 * 60 * 5 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"수렵 이벤트가 종료되기 5시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 4 - 1000
							&& time <= 1000 * 60 * 60 * 4 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"수렵 이벤트가 종료되기 4시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 3 - 1000
							&& time <= 1000 * 60 * 60 * 3 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"수렵 이벤트가 종료되기 3시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 2 - 1000
							&& time <= 1000 * 60 * 60 * 2 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"수렵 이벤트가 종료되기 2시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 1 - 1000
							&& time <= 1000 * 60 * 60 * 1 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"수렵 이벤트가 종료되기 1시간 전입니다.");
					}

					if (time >= 1000 * 60 * 50 - 1000
							&& time <= 1000 * 60 * 50 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"수렵 이벤트가 종료되기 50분 전입니다.");
					}
					if (time >= 1000 * 60 * 40 - 1000
							&& time <= 1000 * 60 * 40 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"수렵 이벤트가 종료되기 40분 전입니다.");
					}
					if (time >= 1000 * 60 * 30 - 1000
							&& time <= 1000 * 60 * 30 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"수렵 이벤트가 종료되기 30분 전입니다.");
					}
					if (time >= 1000 * 60 * 20 - 1000
							&& time <= 1000 * 60 * 20 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"수렵 이벤트가 종료되기 20분 전입니다.");
					}
					if (time >= 1000 * 60 * 10 - 1000
							&& time <= 1000 * 60 * 10 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"수렵 이벤트가 종료되기 10분 전입니다.");
					}
					if (time >= 1000 * 60 * 5 - 1000
							&& time <= 1000 * 60 * 5 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"수렵 이벤트가 종료되기 5분 전입니다.");
					}
				}
				swich = true;
			}
		}
		if (EventShop_아툰기사단.진행() == true) {
			if (EventShop_아툰기사단.남은시간() != null) {
				if (now >= EventShop_아툰기사단.남은시간().getTime()) {
					Object[] 아툰npc = null;
					아툰npc = EventShop_아툰기사단.getEventShopNpc().toArray();
					if (아툰npc.length > 0) {
						EventShop_아툰기사단.진행(false);
						for (Object DelNpc : 아툰npc) {
							L1NpcInstance npc = (L1NpcInstance) DelNpc;
							npc.deleteMe();
							EventShop_아툰기사단.oddEventShopNpc(npc);
							EventShopTable.Delete(EventShop_아툰기사단.NPCID);
						}
					}
					L1World.getInstance().broadcastServerMessage(
							"아툰의 기사단원 이벤트가 종료되었습니다. 많은 참여 감사드립니다.");
					아툰npc = null;
				} else {
					long time = EventShop_아툰기사단.남은시간().getTime() - now;
					if (time >= 1000 * 60 * 60 * 5 - 1000
							&& time <= 1000 * 60 * 60 * 5 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"아툰의 기사단원 이벤트가 종료되기 5시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 4 - 1000
							&& time <= 1000 * 60 * 60 * 4 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"아툰의 기사단원 이벤트가 종료되기 4시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 3 - 1000
							&& time <= 1000 * 60 * 60 * 3 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"아툰의 기사단원 이벤트가 종료되기 3시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 2 - 1000
							&& time <= 1000 * 60 * 60 * 2 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"아툰의 기사단원 이벤트가 종료되기 2시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 1 - 1000
							&& time <= 1000 * 60 * 60 * 1 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"아툰의 기사단원 이벤트가 종료되기 1시간 전입니다.");
					}

					if (time >= 1000 * 60 * 50 - 1000
							&& time <= 1000 * 60 * 50 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"아툰의 기사단원 이벤트가 종료되기 50분 전입니다.");
					}
					if (time >= 1000 * 60 * 40 - 1000
							&& time <= 1000 * 60 * 40 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"아툰의 기사단원 이벤트가 종료되기 40분 전입니다.");
					}
					if (time >= 1000 * 60 * 30 - 1000
							&& time <= 1000 * 60 * 30 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"아툰의 기사단원 이벤트가 종료되기 30분 전입니다.");
					}
					if (time >= 1000 * 60 * 20 - 1000
							&& time <= 1000 * 60 * 20 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"아툰의 기사단원 이벤트가 종료되기 20분 전입니다.");
					}
					if (time >= 1000 * 60 * 10 - 1000
							&& time <= 1000 * 60 * 10 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"아툰의 기사단원 이벤트가 종료되기 10분 전입니다.");
					}
					if (time >= 1000 * 60 * 5 - 1000
							&& time <= 1000 * 60 * 5 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"아툰의 기사단원 이벤트가 종료되기 5분 전입니다.");
					}
				}
				swich = true;
			}
		}
		if (EventShop_판도라.진행() == true) {
			if (EventShop_판도라.남은시간() != null) {
				if (now >= EventShop_판도라.남은시간().getTime()) {
					Object[] 판도라npc = null;
					판도라npc = EventShop_판도라.getEventShopNpc().toArray();
					if (판도라npc.length > 0) {
						EventShop_판도라.진행(false);
						for (Object DelNpc : 판도라npc) {
							L1NpcInstance npc = (L1NpcInstance) DelNpc;
							npc.deleteMe();
							EventShop_판도라.oddEventShopNpc(npc);
							EventShopTable.Delete(EventShop_판도라.NPCID);
						}
					}
					L1World.getInstance()
							.broadcastServerMessage(
									"판도라의 상자 이벤트가 종료되었습니다. 아덴 월드 유저 여러분의 많은 참여에 감사드립니다.");
					판도라npc = null;
				} else {
					long time = EventShop_판도라.남은시간().getTime() - now;
					if (time >= 1000 * 60 * 60 * 5 - 1000
							&& time <= 1000 * 60 * 60 * 5 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"판도라의 상자 이벤트가 종료되기 5시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 4 - 1000
							&& time <= 1000 * 60 * 60 * 4 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"판도라의 상자 이벤트가 종료되기 4시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 3 - 1000
							&& time <= 1000 * 60 * 60 * 3 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"판도라의 상자 이벤트가 종료되기 3시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 2 - 1000
							&& time <= 1000 * 60 * 60 * 2 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"판도라의 상자 이벤트가 종료되기 2시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 1 - 1000
							&& time <= 1000 * 60 * 60 * 1 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"판도라의 상자 이벤트가 종료되기 1시간 전입니다.");
					}

					if (time >= 1000 * 60 * 50 - 1000
							&& time <= 1000 * 60 * 50 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"판도라의 상자 이벤트가 종료되기 50분 전입니다.");
					}
					if (time >= 1000 * 60 * 40 - 1000
							&& time <= 1000 * 60 * 40 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"판도라의 상자 이벤트가 종료되기 40분 전입니다.");
					}
					if (time >= 1000 * 60 * 30 - 1000
							&& time <= 1000 * 60 * 30 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"판도라의 상자 이벤트가 종료되기 30분 전입니다.");
					}
					if (time >= 1000 * 60 * 20 - 1000
							&& time <= 1000 * 60 * 20 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"판도라의 상자 이벤트가 종료되기 20분 전입니다.");
					}
					if (time >= 1000 * 60 * 10 - 1000
							&& time <= 1000 * 60 * 10 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"판도라의 상자 이벤트가 종료되기 10분 전입니다.");
					}
					if (time >= 1000 * 60 * 5 - 1000
							&& time <= 1000 * 60 * 5 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"판도라의 상자 이벤트가 종료되기 5분 전입니다.");
					}
				}
				swich = true;
			}
		}

		if (EventShop_그렘린.진행() == true) {
			if (EventShop_그렘린.남은시간() != null) {
				if (now >= EventShop_그렘린.남은시간().getTime()) {
					Object[] 그렘린npc = null;
					그렘린npc = EventShop_그렘린.getEventShopNpc().toArray();
					if (그렘린npc.length > 0) {
						EventShop_그렘린.진행(false);
						for (Object DelNpc : 그렘린npc) {
							L1NpcInstance npc = (L1NpcInstance) DelNpc;
							npc.deleteMe();
							EventShop_그렘린.oddEventShopNpc(npc);
							EventShopTable.Delete(EventShop_그렘린.NPCID);
						}
					}
					L1World.getInstance().broadcastServerMessage(
							"그렘린 마법인형 이벤트가 종료되었습니다. 많은 참여 감사드립니다.");
					그렘린npc = null;
				} else {
					long time = EventShop_그렘린.남은시간().getTime() - now;
					if (time >= 1000 * 60 * 60 * 5 - 1000
							&& time <= 1000 * 60 * 60 * 5 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"그렘린 마법인형 이벤트가 종료되기 5시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 4 - 1000
							&& time <= 1000 * 60 * 60 * 4 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"그렘린 마법인형 이벤트가 종료되기 4시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 3 - 1000
							&& time <= 1000 * 60 * 60 * 3 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"그렘린 마법인형 이벤트가 종료되기 3시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 2 - 1000
							&& time <= 1000 * 60 * 60 * 2 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"그렘린 마법인형 이벤트가 종료되기 2시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 1 - 1000
							&& time <= 1000 * 60 * 60 * 1 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"그렘린 마법인형 이벤트가 종료되기 1시간 전입니다.");
					}

					if (time >= 1000 * 60 * 50 - 1000
							&& time <= 1000 * 60 * 50 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"그렘린 마법인형 이벤트가 종료되기 50분 전입니다.");
					}
					if (time >= 1000 * 60 * 40 - 1000
							&& time <= 1000 * 60 * 40 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"그렘린 마법인형 이벤트가 종료되기 40분 전입니다.");
					}
					if (time >= 1000 * 60 * 30 - 1000
							&& time <= 1000 * 60 * 30 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"그렘린 마법인형 이벤트가 종료되기 30분 전입니다.");
					}
					if (time >= 1000 * 60 * 20 - 1000
							&& time <= 1000 * 60 * 20 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"그렘린 마법인형 이벤트가 종료되기 20분 전입니다.");
					}
					if (time >= 1000 * 60 * 10 - 1000
							&& time <= 1000 * 60 * 10 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"그렘린 마법인형 이벤트가 종료되기 10분 전입니다.");
					}
					if (time >= 1000 * 60 * 5 - 1000
							&& time <= 1000 * 60 * 5 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"그렘린 마법인형 이벤트가 종료되기 5분 전입니다.");
					}
				}
				swich = true;
			}
		}

		if (EventShop_셸로브.진행() == true) {
			if (EventShop_셸로브.남은시간() != null) {
				if (now >= EventShop_셸로브.남은시간().getTime()) {
					Object[] 셸로브npc = null;
					셸로브npc = EventShop_셸로브.getEventShopNpc().toArray();
					if (셸로브npc.length > 0) {
						EventShop_셸로브.진행(false);
						for (Object DelNpc : 셸로브npc) {
							L1NpcInstance npc = (L1NpcInstance) DelNpc;
							npc.deleteMe();
							EventShop_셸로브.oddEventShopNpc(npc);
							EventShopTable.Delete(EventShop_셸로브.NPCID);
						}
					}
					L1World.getInstance().broadcastServerMessage(
							"공포의 대상 셀로브 이벤트가 종료되었습니다. 많은 참여 감사드립니다.");
					셸로브npc = null;
				} else {
					long time = EventShop_셸로브.남은시간().getTime() - now;
					if (time >= 1000 * 60 * 60 * 5 - 1000
							&& time <= 1000 * 60 * 60 * 5 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"공포의 대상 셀로브 이벤트가 종료되기 5시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 4 - 1000
							&& time <= 1000 * 60 * 60 * 4 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"공포의 대상 셀로브 이벤트가 종료되기 4시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 3 - 1000
							&& time <= 1000 * 60 * 60 * 3 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"공포의 대상 셀로브 이벤트가 종료되기 3시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 2 - 1000
							&& time <= 1000 * 60 * 60 * 2 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"공포의 대상 셀로브 이벤트가 종료되기 2시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 1 - 1000
							&& time <= 1000 * 60 * 60 * 1 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"공포의 대상 셀로브 이벤트가 종료되기 1시간 전입니다.");
					}

					if (time >= 1000 * 60 * 50 - 1000
							&& time <= 1000 * 60 * 50 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"공포의 대상 셀로브 이벤트가 종료되기 50분 전입니다.");
					}
					if (time >= 1000 * 60 * 40 - 1000
							&& time <= 1000 * 60 * 40 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"공포의 대상 셀로브 이벤트가 종료되기 40분 전입니다.");
					}
					if (time >= 1000 * 60 * 30 - 1000
							&& time <= 1000 * 60 * 30 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"공포의 대상 셀로브 이벤트가 종료되기 30분 전입니다.");
					}
					if (time >= 1000 * 60 * 20 - 1000
							&& time <= 1000 * 60 * 20 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"공포의 대상 셀로브 이벤트가 종료되기 20분 전입니다.");
					}
					if (time >= 1000 * 60 * 10 - 1000
							&& time <= 1000 * 60 * 10 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"공포의 대상 셀로브 이벤트가 종료되기 10분 전입니다.");
					}
					if (time >= 1000 * 60 * 5 - 1000
							&& time <= 1000 * 60 * 5 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"공포의 대상 셀로브 이벤트가 종료되기 5분 전입니다.");
					}
				}
				swich = true;
			}
		}
		if (EventShop_신묘한삭제.진행() == true) {
			if (EventShop_신묘한삭제.남은시간() != null) {
				if (now >= EventShop_신묘한삭제.남은시간().getTime()) {
					Object[] 신묘한npc = null;
					신묘한npc = EventShop_신묘한삭제.getEventShopNpc().toArray();
					if (신묘한npc.length > 0) {
						EventShop_신묘한삭제.진행(false);
						for (Object DelNpc : 신묘한npc) {
							L1NpcInstance npc = (L1NpcInstance) DelNpc;
							npc.deleteMe();
							EventShop_신묘한삭제.oddEventShopNpc(npc);
							EventShopTable.Delete(EventShop_신묘한삭제.NPCID);
						}
					}
					신묘한npc = null;
					L1World.getInstance().broadcastServerMessage(
							"신묘한 이벤트가 종료되었습니다. 아덴 월드 유저 여러분의 많은 참여에 감사드립니다.");
				} else {
					long time = EventShop_신묘한삭제.남은시간().getTime() - now;
					if (time >= 1000 * 60 * 60 * 5 - 1000
							&& time <= 1000 * 60 * 60 * 5 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"신묘한 이벤트가 종료되기 5시간 전입니다.");
					} else if (time >= 1000 * 60 * 60 * 4 - 1000
							&& time <= 1000 * 60 * 60 * 4 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"신묘한 이벤트가 종료되기 4시간 전입니다.");
					} else if (time >= 1000 * 60 * 60 * 3 - 1000
							&& time <= 1000 * 60 * 60 * 3 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"신묘한 이벤트가 종료되기 3시간 전입니다.");
					} else if (time >= 1000 * 60 * 60 * 2 - 1000
							&& time <= 1000 * 60 * 60 * 2 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"신묘한 이벤트가 종료되기 2시간 전입니다.");
					} else if (time >= 1000 * 60 * 60 * 1 - 1000
							&& time <= 1000 * 60 * 60 * 1 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"신묘한 이벤트가 종료되기 1시간 전입니다.");
					} else if (time >= 1000 * 60 * 50 - 1000
							&& time <= 1000 * 60 * 50 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"신묘한 이벤트가 종료되기 50분 전입니다.");
					} else if (time >= 1000 * 60 * 40 - 1000
							&& time <= 1000 * 60 * 40 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"신묘한 이벤트가 종료되기 40분 전입니다.");
					} else if (time >= 1000 * 60 * 30 - 1000
							&& time <= 1000 * 60 * 30 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"신묘한 이벤트가 종료되기 30분 전입니다.");
					} else if (time >= 1000 * 60 * 20 - 1000
							&& time <= 1000 * 60 * 20 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"신묘한 이벤트가 종료되기 20분 전입니다.");
					} else if (time >= 1000 * 60 * 10 - 1000
							&& time <= 1000 * 60 * 10 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"신묘한 이벤트가 종료되기 10분 전입니다.");
					} else if (time >= 1000 * 60 * 5 - 1000
							&& time <= 1000 * 60 * 5 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"신묘한 이벤트가 종료되기 5분 전입니다.");
					}
				}
				swich = true;
			}
		}
		if (EventShop_신묘한.진행() == true) {
			if (EventShop_신묘한.남은시간() != null) {
				if (now >= EventShop_신묘한.남은시간().getTime()) {
					Object[] 신묘한npc = null;
					신묘한npc = EventShop_신묘한.getEventShopNpc().toArray();
					if (신묘한npc.length > 0) {
						EventShop_신묘한.진행(false);
						for (Object DelNpc : 신묘한npc) {
							L1NpcInstance npc = (L1NpcInstance) DelNpc;
							npc.deleteMe();
							EventShop_신묘한.oddEventShopNpc(npc);
							EventShopTable.Delete(EventShop_신묘한.NPCID);
						}
					}
					신묘한npc = null;
					EventShop_신묘한삭제.기본시간후삭제시간(now);
				}
				swich = true;
			}
		}
		if (EventShop_신기한반지.진행() == true) {
			if (EventShop_신기한반지.남은시간() != null) {
				if (now >= EventShop_신기한반지.남은시간().getTime()) {
					Object[] 신기한반지npc = null;
					신기한반지npc = EventShop_신기한반지.getEventShopNpc().toArray();
					if (신기한반지npc.length > 0) {
						EventShop_신기한반지.진행(false);
						for (Object DelNpc : 신기한반지npc) {
							L1NpcInstance npc = (L1NpcInstance) DelNpc;
							npc.deleteMe();
							EventShop_신기한반지.oddEventShopNpc(npc);
							EventShopTable.Delete(EventShop_신기한반지.NPCID);
						}
					}
					L1World.getInstance()
							.broadcastServerMessage(
									"신기한 반지 이벤트가 종료되었습니다. 아덴 월드 유저 여러분의 많은 참여에 감사드립니다.");
					신기한반지npc = null;
				} else {
					long time = EventShop_신기한반지.남은시간().getTime() - now;
					if (time >= 1000 * 60 * 60 * 5 - 1000
							&& time <= 1000 * 60 * 60 * 5 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"신기한 반지 이벤트가 종료되기 5시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 4 - 1000
							&& time <= 1000 * 60 * 60 * 4 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"신기한 반지 이벤트가 종료되기 4시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 3 - 1000
							&& time <= 1000 * 60 * 60 * 3 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"신기한 반지 이벤트가 종료되기 3시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 2 - 1000
							&& time <= 1000 * 60 * 60 * 2 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"신기한 반지 이벤트가 종료되기 2시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 1 - 1000
							&& time <= 1000 * 60 * 60 * 1 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"신기한 반지 이벤트가 종료되기 1시간 전입니다.");
					}

					if (time >= 1000 * 60 * 50 - 1000
							&& time <= 1000 * 60 * 50 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"신기한 반지 이벤트가 종료되기 50분 전입니다.");
					}
					if (time >= 1000 * 60 * 40 - 1000
							&& time <= 1000 * 60 * 40 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"신기한 반지 이벤트가 종료되기 40분 전입니다.");
					}
					if (time >= 1000 * 60 * 30 - 1000
							&& time <= 1000 * 60 * 30 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"신기한 반지 이벤트가 종료되기 30분 전입니다.");
					}
					if (time >= 1000 * 60 * 20 - 1000
							&& time <= 1000 * 60 * 20 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"신기한 반지 이벤트가 종료되기 20분 전입니다.");
					}
					if (time >= 1000 * 60 * 10 - 1000
							&& time <= 1000 * 60 * 10 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"신기한 반지 이벤트가 종료되기 10분 전입니다.");
					}
					if (time >= 1000 * 60 * 5 - 1000
							&& time <= 1000 * 60 * 5 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"신기한 반지 이벤트가 종료되기 5분 전입니다.");
					}
				}
				swich = true;
			}
		}
		if (EventShop_룸티스.진행() == true) {
			if (EventShop_룸티스.남은시간() != null) {
				if (now >= EventShop_룸티스.남은시간().getTime()) {
					Object[] 룸티스npc = null;
					룸티스npc = EventShop_룸티스.getEventShopNpc().toArray();
					if (룸티스npc.length > 0) {
						EventShop_룸티스.진행(false);
						for (Object DelNpc : 룸티스npc) {
							L1NpcInstance npc = (L1NpcInstance) DelNpc;
							npc.deleteMe();
							EventShop_룸티스.oddEventShopNpc(npc);
							EventShopTable.Delete(EventShop_룸티스.NPCID);
						}
					}
					L1World.getInstance()
							.broadcastServerMessage(
									"룸티스 귀걸이 이벤트가 종료되었습니다. 아덴 월드 유저 여러분의 많은 참여에 감사드립니다.");
					룸티스npc = null;
				} else {
					long time = EventShop_룸티스.남은시간().getTime() - now;
					if (time >= 1000 * 60 * 60 * 5 - 1000
							&& time <= 1000 * 60 * 60 * 5 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"룸티스 귀걸이 이벤트가 종료되기 5시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 4 - 1000
							&& time <= 1000 * 60 * 60 * 4 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"룸티스 귀걸이 이벤트가 종료되기 4시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 3 - 1000
							&& time <= 1000 * 60 * 60 * 3 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"룸티스 귀걸이 이벤트가 종료되기 3시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 2 - 1000
							&& time <= 1000 * 60 * 60 * 2 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"룸티스 귀걸이 이벤트가 종료되기 2시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 1 - 1000
							&& time <= 1000 * 60 * 60 * 1 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"룸티스 귀걸이 이벤트가 종료되기 1시간 전입니다.");
					}

					if (time >= 1000 * 60 * 50 - 1000
							&& time <= 1000 * 60 * 50 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"룸티스 귀걸이 이벤트가 종료되기 50분 전입니다.");
					}
					if (time >= 1000 * 60 * 40 - 1000
							&& time <= 1000 * 60 * 40 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"룸티스 귀걸이 이벤트가 종료되기 40분 전입니다.");
					}
					if (time >= 1000 * 60 * 30 - 1000
							&& time <= 1000 * 60 * 30 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"룸티스 귀걸이 이벤트가 종료되기 30분 전입니다.");
					}
					if (time >= 1000 * 60 * 20 - 1000
							&& time <= 1000 * 60 * 20 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"룸티스 귀걸이 이벤트가 종료되기 20분 전입니다.");
					}
					if (time >= 1000 * 60 * 10 - 1000
							&& time <= 1000 * 60 * 10 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"룸티스 귀걸이 이벤트가 종료되기 10분 전입니다.");
					}
					if (time >= 1000 * 60 * 5 - 1000
							&& time <= 1000 * 60 * 5 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"룸티스 귀걸이 이벤트가 종료되기 5분 전입니다.");
					}
				}
				swich = true;
			}
		}
		if (EventShop_인나드릴.진행() == true) {
			if (EventShop_인나드릴.남은시간() != null) {
				if (now >= EventShop_인나드릴.남은시간().getTime()) {
					Object[] 인나드릴npc = null;
					인나드릴npc = EventShop_인나드릴.getEventShopNpc().toArray();
					if (인나드릴npc.length > 0) {
						EventShop_인나드릴.진행(false);
						for (Object DelNpc : 인나드릴npc) {
							L1NpcInstance npc = (L1NpcInstance) DelNpc;
							npc.deleteMe();
							EventShop_인나드릴.oddEventShopNpc(npc);
							EventShopTable.Delete(EventShop_인나드릴.NPCID);
						}
					}
					L1World.getInstance()
							.broadcastServerMessage(
									"인나드릴 티셔츠 이벤트가 종료되었습니다. 아덴 월드 유저 여러분의 많은 참여에 감사드립니다.");
					인나드릴npc = null;
				} else {
					long time = EventShop_인나드릴.남은시간().getTime() - now;
					if (time >= 1000 * 60 * 60 * 5 - 1000
							&& time <= 1000 * 60 * 60 * 5 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"인나드릴 티셔츠 이벤트가 종료되기 5시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 4 - 1000
							&& time <= 1000 * 60 * 60 * 4 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"인나드릴 티셔츠 이벤트가 종료되기 4시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 3 - 1000
							&& time <= 1000 * 60 * 60 * 3 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"인나드릴 티셔츠 이벤트가 종료되기 3시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 2 - 1000
							&& time <= 1000 * 60 * 60 * 2 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"인나드릴 티셔츠 이벤트가 종료되기 2시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 1 - 1000
							&& time <= 1000 * 60 * 60 * 1 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"인나드릴 티셔츠 이벤트가 종료되기 1시간 전입니다.");
					}

					if (time >= 1000 * 60 * 50 - 1000
							&& time <= 1000 * 60 * 50 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"인나드릴 티셔츠 이벤트가 종료되기 50분 전입니다.");
					}
					if (time >= 1000 * 60 * 40 - 1000
							&& time <= 1000 * 60 * 40 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"인나드릴 티셔츠 이벤트가 종료되기 40분 전입니다.");
					}
					if (time >= 1000 * 60 * 30 - 1000
							&& time <= 1000 * 60 * 30 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"인나드릴 티셔츠 이벤트가 종료되기 30분 전입니다.");
					}
					if (time >= 1000 * 60 * 20 - 1000
							&& time <= 1000 * 60 * 20 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"인나드릴 티셔츠 이벤트가 종료되기 20분 전입니다.");
					}
					if (time >= 1000 * 60 * 10 - 1000
							&& time <= 1000 * 60 * 10 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"인나드릴 티셔츠 이벤트가 종료되기 10분 전입니다.");
					}
					if (time >= 1000 * 60 * 5 - 1000
							&& time <= 1000 * 60 * 5 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"인나드릴 티셔츠 이벤트가 종료되기 5분 전입니다.");
					}
				}
				swich = true;
			}
		}

		if (EventShop_드래곤.진행() == true) {
			if (EventShop_드래곤.남은시간() != null) {
				if (now >= EventShop_드래곤.남은시간().getTime()) {
					Object[] 드래곤npc = null;
					드래곤npc = EventShop_드래곤.getEventShopNpc().toArray();
					if (드래곤npc.length > 0) {
						EventShop_드래곤.진행(false);
						for (Object DelNpc : 드래곤npc) {
							L1NpcInstance npc = (L1NpcInstance) DelNpc;
							npc.deleteMe();
							EventShop_드래곤.oddEventShopNpc(npc);
							EventShopTable.Delete(EventShop_드래곤.NPCID);
						}
					}
					L1World.getInstance()
							.broadcastServerMessage(
									"드래곤의 보물상자 이벤트가 종료되었습니다. 아덴 월드 유저 여러분의 많은 참여에 감사드립니다.");
					드래곤npc = null;
				} else {
					long time = EventShop_드래곤.남은시간().getTime() - now;
					if (time >= 1000 * 60 * 60 * 5 - 1000
							&& time <= 1000 * 60 * 60 * 5 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"드래곤의 보물상자 이벤트가 종료되기 5시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 4 - 1000
							&& time <= 1000 * 60 * 60 * 4 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"드래곤의 보물상자 이벤트가 종료되기 4시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 3 - 1000
							&& time <= 1000 * 60 * 60 * 3 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"드래곤의 보물상자 이벤트가 종료되기 3시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 2 - 1000
							&& time <= 1000 * 60 * 60 * 2 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"드래곤의 보물상자 이벤트가 종료되기 2시간 전입니다.");
					}
					if (time >= 1000 * 60 * 60 * 1 - 1000
							&& time <= 1000 * 60 * 60 * 1 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"드래곤의 보물상자 이벤트가 종료되기 1시간 전입니다.");
					}

					if (time >= 1000 * 60 * 50 - 1000
							&& time <= 1000 * 60 * 50 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"드래곤의 보물상자 이벤트가 종료되기 50분 전입니다.");
					}
					if (time >= 1000 * 60 * 40 - 1000
							&& time <= 1000 * 60 * 40 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"드래곤의 보물상자 이벤트가 종료되기 40분 전입니다.");
					}
					if (time >= 1000 * 60 * 30 - 1000
							&& time <= 1000 * 60 * 30 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"드래곤의 보물상자 이벤트가 종료되기 30분 전입니다.");
					}
					if (time >= 1000 * 60 * 20 - 1000
							&& time <= 1000 * 60 * 20 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"드래곤의 보물상자 이벤트가 종료되기 20분 전입니다.");
					}
					if (time >= 1000 * 60 * 10 - 1000
							&& time <= 1000 * 60 * 10 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"드래곤의 보물상자 이벤트가 종료되기 10분 전입니다.");
					}
					if (time >= 1000 * 60 * 5 - 1000
							&& time <= 1000 * 60 * 5 + 1000) {
						L1World.getInstance().broadcastServerMessage(
								"드래곤의 보물상자 이벤트가 종료되기 5분 전입니다.");
					}
				}
				swich = true;
			}
		}
		if (swich) {
			공지사용 = true;
			EventNoticeTimer check = new EventNoticeTimer(this);
			check.begin();
		}
	}

	private static int 할로윈리스트[] = { 90085, 90086, 90087, 90088, 90089, 90090,
			90091, 90092, 160423, 435000, 160510, 160511, 21123, 21269 };
	private static int 할로윈리스트2[] = { 90085, 90086, 90087, 90088, 90089, 90090,
			90091, 90092, 160423, 435000, 160510, 160511, 21123 };

	public synchronized static void EventItemDelete() {
		try {
			for (L1PcInstance tempPc : L1World.getInstance().getAllPlayers()) {
				if (tempPc == null)
					continue;
				for (int i = 0; i < 할로윈리스트.length; i++) {
					L1ItemInstance[] item = tempPc.getInventory().findItemsId(
							할로윈리스트[i]);

					if (item != null && item.length > 0) {
						for (int o = 0; o < item.length; o++) {
							if (item[o].getItemId() == 21269) {
								if (item[o].getEnchantLevel() >= 6) {
									if (item[o].getBless() >= 128)
										item[o].setBless(128);
									else
										item[o].setBless(0);
									tempPc.getInventory().updateItem(item[o],
											L1PcInventory.COL_BLESS);
									tempPc.getInventory().saveItem(item[o],
											L1PcInventory.COL_BLESS);
									continue;
								}
							}
							if (item[o].isEquipped()) {
								tempPc.getInventory().setEquipped(item[o],
										false); // 추가해주세요.
							}
							tempPc.getInventory().removeItem(item[o]);
						}
					}
					try {
						PrivateWarehouse pw = WarehouseManager.getInstance()
								.getPrivateWarehouse(tempPc.getAccountName());
						L1ItemInstance[] item2 = pw.findItemsId(할로윈리스트[i]);
						if (item2 != null && item2.length > 0) {
							for (int o = 0; o < item2.length; o++) {
								if (item[o].getItemId() == 21269) {
									if (item[o].getEnchantLevel() >= 6) {
										if (item[o].getBless() >= 128)
											item[o].setBless(128);
										else
											item[o].setBless(0);
										tempPc.getInventory().updateItem(
												item[o],
												L1PcInventory.COL_BLESS);
										tempPc.getInventory().saveItem(item[o],
												L1PcInventory.COL_BLESS);
										continue;
									}
								}
								pw.removeItem(item2[o]);
							}
						}
					} catch (Exception e) {
					}
					try {
						if (tempPc.getClanid() > 0) {
							ClanWarehouse cw = WarehouseManager.getInstance()
									.getClanWarehouse(tempPc.getClanname());
							L1ItemInstance[] item3 = cw.findItemsId(할로윈리스트[i]);
							if (item3 != null && item3.length > 0) {
								for (int o = 0; o < item3.length; o++) {
									if (item3[o].getItemId() == 21269) {
										if (item3[o].getEnchantLevel() >= 6) {
											if (item3[o].getBless() >= 128)
												item3[o].setBless(128);
											else
												item3[o].setBless(0);
											tempPc.getInventory().updateItem(
													item3[o],
													L1PcInventory.COL_BLESS);
											tempPc.getInventory().saveItem(
													item3[o],
													L1PcInventory.COL_BLESS);
											continue;
										}
									}
									cw.removeItem(item3[o]);
								}
							}
						}
					} catch (Exception e) {
					}
					try {
						Collection<L1NpcInstance> pList = tempPc.getPetList();
						if (pList.size() > 0) {
							for (L1NpcInstance npc : pList) {
								L1ItemInstance[] pitem = npc.getInventory()
										.findItemsId(할로윈리스트[i]);
								if (pitem != null && pitem.length > 0) {
									for (int o = 0; o < pitem.length; o++) {
										if (pitem[o].getItemId() == 21269) {
											if (pitem[o].getEnchantLevel() >= 6) {
												if (pitem[o].getBless() >= 128)
													pitem[o].setBless(128);
												else
													pitem[o].setBless(0);
												tempPc.getInventory()
														.updateItem(
																pitem[o],
																L1PcInventory.COL_BLESS);
												tempPc.getInventory()
														.saveItem(
																pitem[o],
																L1PcInventory.COL_BLESS);
												continue;
											}
										}
										npc.getInventory().removeItem(pitem[o]);
									}
								}
							}
						}
					} catch (Exception e) {
					}
				}
			}
			try {
				for (L1Object obj : L1World.getInstance().getAllItem()) {
					if (!(obj instanceof L1ItemInstance))
						continue;
					L1ItemInstance temp_item = (L1ItemInstance) obj;
					if (temp_item.getItemOwner() == null
							|| !(temp_item.getItemOwner() instanceof L1RobotInstance)) {
						if (temp_item.getX() == 0 && temp_item.getY() == 0)
							continue;
					}
					for (int ii = 0; ii < 할로윈리스트.length; ii++) {
						if (할로윈리스트[ii] == temp_item.getItemId()) {
							if (temp_item.getItemId() == 21269) {
								if (temp_item.getEnchantLevel() >= 6) {
									if (temp_item.getBless() >= 128)
										temp_item.setBless(128);
									else
										temp_item.setBless(0);
									continue;
								}
							}
							L1Inventory groundInventory = L1World.getInstance()
									.getInventory(temp_item.getX(),
											temp_item.getY(),
											temp_item.getMapId());
							groundInventory.removeItem(temp_item);
							break;
						}
					}

				}
			} catch (Exception e) {
			}
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < 할로윈리스트2.length; i++) {
				sb.append(+할로윈리스트2[i]);
				if (i < 할로윈리스트2.length - 1) {
					sb.append(",");
				}
			}
			Delete(sb.toString());
			Delete21269();
			BlessUpdate(21269);
			BlessUpdatewearehose(21269);
			BlessUpdateclanwarehouse(21269);
			BlessUpdateelfwarehouse(21269);
		} catch (Exception e) {
		}
	}

	private static void Delete(String id_name) {
		Connection con = null;
		PreparedStatement pstm = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con
					.prepareStatement("delete FROM character_items WHERE item_id IN ("
							+ id_name + ")");
			pstm.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(pstm);
		}
		try {
			pstm = con
					.prepareStatement("delete FROM character_warehouse WHERE item_id in ("
							+ id_name + ")");
			pstm.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(pstm);
		}
		try {
			pstm = con
					.prepareStatement("delete FROM clan_warehouse WHERE item_id in ("
							+ id_name + ")");
			pstm.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(pstm);
		}
		try {
			pstm = con
					.prepareStatement("delete FROM character_elf_warehouse WHERE item_id in ("
							+ id_name + ")");
			pstm.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	private static void Delete21269() {
		Connection con = null;
		PreparedStatement pstm = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con
					.prepareStatement("delete FROM character_items WHERE item_id=21269 AND enchantlvl < 6");
			pstm.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(pstm);
		}
		try {
			pstm = con
					.prepareStatement("delete FROM character_warehouse WHERE item_id=21269 AND enchantlvl < 6");
			pstm.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(pstm);
		}
		try {
			pstm = con
					.prepareStatement("delete FROM clan_warehouse WHERE item_id=21269 AND enchantlvl < 6");
			pstm.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(pstm);
		}
		try {
			pstm = con
					.prepareStatement("delete FROM character_elf_warehouse WHERE item_id=21269 AND enchantlvl < 6");
			pstm.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	public static void BlessUpdate(int itemid) {
		Connection con = null;
		Connection con2 = null;
		PreparedStatement pstm = null;
		PreparedStatement pstm2 = null;
		ResultSet rs = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con
					.prepareStatement("SELECT id, bless, enchantlvl FROM character_items WHERE item_id=?");
			// pstm =
			// con.prepareStatement("SELECT * FROM character_items WHERE item_id=?");
			pstm.setInt(1, itemid);
			rs = pstm.executeQuery();
			while (rs.next()) {
				int id = rs.getInt("id");
				int bless = rs.getInt("bless");
				int ent = rs.getInt("enchantlvl");
				if (ent < 6)
					continue;
				try {
					con2 = L1DatabaseFactory.getInstance().getConnection();
					pstm2 = con2
							.prepareStatement("UPDATE character_items SET bless =? WHERE id=?");
					pstm2.setInt(1, bless > 128 ? 128 : 0);
					pstm2.setInt(2, id);
					pstm2.executeUpdate();
				} finally {
					SQLUtil.close(pstm2);
					SQLUtil.close(con2);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(rs);
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	public static void BlessUpdatewearehose(int itemid) {
		Connection con = null;
		Connection con2 = null;
		PreparedStatement pstm = null;
		PreparedStatement pstm2 = null;
		ResultSet rs = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con
					.prepareStatement("SELECT id, bless, enchantlvl FROM character_warehouse WHERE item_id=?");
			// pstm =
			// con.prepareStatement("SELECT * FROM character_items WHERE item_id=?");
			pstm.setInt(1, itemid);
			rs = pstm.executeQuery();
			while (rs.next()) {
				int id = rs.getInt("id");
				int bless = rs.getInt("bless");
				int ent = rs.getInt("enchantlvl");
				if (ent < 6)
					continue;
				try {
					con2 = L1DatabaseFactory.getInstance().getConnection();
					pstm2 = con2
							.prepareStatement("UPDATE character_warehouse SET bless =? WHERE id=?");
					pstm2.setInt(1, bless > 128 ? 128 : 0);
					pstm2.setInt(2, id);
					pstm2.executeUpdate();
				} finally {
					SQLUtil.close(pstm2);
					SQLUtil.close(con2);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(rs);
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	public static void BlessUpdateclanwarehouse(int itemid) {
		Connection con = null;
		Connection con2 = null;
		PreparedStatement pstm = null;
		PreparedStatement pstm2 = null;
		ResultSet rs = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con
					.prepareStatement("SELECT id, bless, enchantlvl FROM clan_warehouse WHERE item_id=?");
			// pstm =
			// con.prepareStatement("SELECT * FROM character_items WHERE item_id=?");
			pstm.setInt(1, itemid);
			rs = pstm.executeQuery();
			while (rs.next()) {
				int id = rs.getInt("id");
				int bless = rs.getInt("bless");
				int ent = rs.getInt("enchantlvl");
				if (ent < 6)
					continue;
				try {
					con2 = L1DatabaseFactory.getInstance().getConnection();
					pstm2 = con2
							.prepareStatement("UPDATE clan_warehouse SET bless =? WHERE id=?");
					pstm2.setInt(1, bless > 128 ? 128 : 0);
					pstm2.setInt(2, id);
					pstm2.executeUpdate();
				} finally {
					SQLUtil.close(pstm2);
					SQLUtil.close(con2);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(rs);
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	public static void BlessUpdateelfwarehouse(int itemid) {
		Connection con = null;
		Connection con2 = null;
		PreparedStatement pstm = null;
		PreparedStatement pstm2 = null;
		ResultSet rs = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con
					.prepareStatement("SELECT id, bless, enchantlvl FROM character_elf_warehouse WHERE item_id=?");
			// pstm =
			// con.prepareStatement("SELECT * FROM character_items WHERE item_id=?");
			pstm.setInt(1, itemid);
			rs = pstm.executeQuery();
			while (rs.next()) {
				int id = rs.getInt("id");
				int bless = rs.getInt("bless");
				int ent = rs.getInt("enchantlvl");
				if (ent < 6)
					continue;
				try {
					con2 = L1DatabaseFactory.getInstance().getConnection();
					pstm2 = con2
							.prepareStatement("UPDATE character_elf_warehouse SET bless =? WHERE id=?");
					pstm2.setInt(1, bless > 128 ? 128 : 0);
					pstm2.setInt(2, id);
					pstm2.executeUpdate();
				} finally {
					SQLUtil.close(pstm2);
					SQLUtil.close(con2);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(rs);
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	private static final int[][] 정무맘보loc = {// 맘보킹,정령감시자
	{ 32928, 32809, 430 }, { 32920, 32864, 430 }, { 32909, 32918, 430 },
			{ 32875, 32942, 430 }, { 32836, 32970, 430 },
			{ 32775, 32924, 430 }, { 32737, 32919, 430 },
			{ 32741, 32876, 430 }, { 32767, 32848, 430 },
			{ 32810, 32817, 430 }, { 32849, 32814, 430 },
			{ 32862, 32833, 430 }, { 32819, 32857, 430 } };
	private static final int[][] 상아탑미믹loc = {// 상아탑미믹
	{ 32903, 32798, 280 }, { 32873, 32758, 280 }, { 32905, 32736, 280 },
			{ 32936, 32767, 280 }, { 32807, 32828, 281 },
			{ 32769, 32795, 281 }, { 32773, 32821, 281 },
			{ 32775, 32843, 281 }, { 32739, 32826, 281 },
			{ 32752, 32793, 282 }, { 32761, 32806, 282 },
			{ 32755, 32860, 282 }, { 32746, 32818, 282 },
			{ 32803, 32858, 282 }, { 32798, 32871, 282 },
			{ 32808, 32848, 283 }, { 32806, 32813, 283 },
			{ 32788, 32805, 283 }, { 32759, 32796, 283 },
			{ 32738, 32808, 283 }, { 32749, 32835, 283 },
			{ 32757, 32859, 283 }, { 32693, 32799, 284 },
			{ 32728, 32791, 284 }, { 32742, 32818, 284 },
			{ 32744, 32855, 284 }, { 32707, 32862, 284 },
			{ 32677, 32855, 284 }, { 32703, 32820, 284 }, { 32717, 32836, 284 } };
	private static final int[][] 하딘분신loc = {// 하딘분신
	{ 32752, 32793, 282 }, { 32761, 32806, 282 }, { 32755, 32860, 282 },
			{ 32746, 32818, 282 }, { 32803, 32858, 282 },
			{ 32798, 32871, 282 }, { 32808, 32848, 283 },
			{ 32806, 32813, 283 }, { 32788, 32805, 283 },
			{ 32759, 32796, 283 }, { 32738, 32808, 283 },
			{ 32749, 32835, 283 }, { 32757, 32859, 283 },
			{ 32693, 32799, 284 }, { 32728, 32791, 284 },
			{ 32742, 32818, 284 }, { 32744, 32855, 284 },
			{ 32707, 32862, 284 }, { 32677, 32855, 284 },
			{ 32703, 32820, 284 }, { 32717, 32836, 284 } };
	private static final int[][] 흑마법사loc = {// 흑마법사
	{ 32807, 32828, 281 }, { 32769, 32795, 281 }, { 32773, 32821, 281 },
			{ 32775, 32843, 281 }, { 32739, 32826, 281 },
			{ 32752, 32793, 282 }, { 32761, 32806, 282 },
			{ 32755, 32860, 282 }, { 32746, 32818, 282 },
			{ 32803, 32858, 282 }, { 32798, 32871, 282 } };

	private static final int[][] 드레이크loc = { { 33405, 32411, 4 },
			{ 33361, 32382, 4 }, { 33402, 32342, 4 }, { 33317, 32319, 4 },
			{ 33356, 32355, 4 } };
	private static final int[][] 더위소x = { { 33660, 33761 }, { 33252, 33396 },
			{ 32687, 32954 } };
	private static final int[][] 더위소y = { { 32249, 32312 }, { 32338, 32404 },
			{ 33231, 33275 } };
	private int bossdietime = 1000 * 60 * 110;
	private int _5시간주기 = (60000 * 60 * 4) + (60000 * 50);
	private int _12시간주기 = (60000 * 60 * 8) + (60000 * 40);
	private int _6시간주기 = (60000 * 60 * 5) + (60000 * 40);
	private int _4시간주기 = (60000 * 60 * 3) + (60000 * 40);
	private int _2시간주기 = (60000 * 60) + (60000 * 40);
	private int _1시간주기 = 60000 * 50;

	private int _30분주기 = 60000 * 29;

	Random _random = new Random(System.nanoTime());

	@SuppressWarnings("deprecation")
	public void boss() {
		try {
			if (젠사용 == true) {
				return;
			}

			if (isOpen) {
				nowsandworm = true;
				isOpen = false;
				BossTimerCheck check = new BossTimerCheck(this);
				check.begin();
				
				L1SpawnUtil.bossspawn(100420, 32784, 33157, (short) 4, 1000 * 60 * 60 * 5, 99);
				L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE,"윈다우드 사막에 샌드웜이 출현하였습니다."), true);
				L1World.getInstance().broadcastPacketToAll(new S_MatizAlarm(2, 0, 3600, true));
				for (L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
					if (pc.getMapId() == 4) {
						if (pc.getX() >= 32512 && pc.getX() <= 32960 && pc.getY() >= 33023 && pc.getY() <= 33535) {
							pc.sendPackets(new S_PacketBox(83, 2), true);
						}
					}
					
				}
				GeneralThreadPool.getInstance().schedule(new Runnable() {
					@Override
					public void run() {
						// TODO 자동 생성된 메소드 스텁
						try {
							for (L1TeleporterInstance tel_npc : L1World.getInstance().getAllTeleporter()) {
								if (tel_npc.getNpcId() == 50036
										|| tel_npc.getNpcId() == 50020
										|| tel_npc.getNpcId() == 50056
										|| tel_npc.getNpcId() == 50054
										|| tel_npc.getNpcId() == 50024) {
									Broadcaster.broadcastPacket(tel_npc,
											new S_NpcChatPacket(tel_npc, "이 진동은..!! 사막에 샌드 웜이 나타났나봐요!", 0), true);
								}
							}
							Thread.sleep(2000);
							for (L1TeleporterInstance tel_npc : L1World
									.getInstance().getAllTeleporter()) {
								if (tel_npc.getNpcId() == 50036
										|| tel_npc.getNpcId() == 50020
										|| tel_npc.getNpcId() == 50056
										|| tel_npc.getNpcId() == 50054
										|| tel_npc.getNpcId() == 50024) {
									Broadcaster.broadcastPacket(
											tel_npc,
											new S_NpcChatPacket(
													tel_npc,
													"여러분! 어서 윈다우드 사막지역으로 가서 샌드 웜을 막아주세요!",
													0), true);
								}
							}
						} catch (Exception e) {
						}
					}
				}, 1);
			}
			if (isOpen2) {
				nowErzarbe = true;
				isOpen2 = false;
				BossTimerCheck check = new BossTimerCheck(this);
				check.begin();
				L1SpawnUtil.bossspawn(100338, 32908, 33222, (short) 4, 1000 * 60 * 60 * 5, 99);
				L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "윈다우드 사막에 에르자베가 출현하였습니다."), true);
				L1World.getInstance().broadcastPacketToAll(new S_MatizAlarm(1, 0, 3600, true));
				for (L1PcInstance pc : L1World.getInstance().getAllPlayers()) {
					if (pc.getMapId() == 4) {
						if (pc.getX() >= 32512 && pc.getX() <= 32960
								&& pc.getY() >= 33023 && pc.getY() <= 33535) {
							pc.sendPackets(new S_PacketBox(83, 2), true);
						}
					}
				}
				GeneralThreadPool.getInstance().schedule(new Runnable() {
					@Override
					public void run() {
						// TODO 자동 생성된 메소드 스텁
						try {
							for (L1TeleporterInstance tel_npc : L1World
									.getInstance().getAllTeleporter()) {
								if (tel_npc.getNpcId() == 50036
										|| tel_npc.getNpcId() == 50020
										|| tel_npc.getNpcId() == 50056
										|| tel_npc.getNpcId() == 50054
										|| tel_npc.getNpcId() == 50024) {
									Broadcaster.broadcastPacket(
											tel_npc,
											new S_NpcChatPacket(
													tel_npc,
													"이 진동은..!! 사막에 에르자베가 나타났나봐요!",
													0), true);
								}
							}
							Thread.sleep(2000);
							for (L1TeleporterInstance tel_npc : L1World
									.getInstance().getAllTeleporter()) {
								if (tel_npc.getNpcId() == 50036
										|| tel_npc.getNpcId() == 50020
										|| tel_npc.getNpcId() == 50056
										|| tel_npc.getNpcId() == 50054
										|| tel_npc.getNpcId() == 50024) {
									Broadcaster.broadcastPacket(
											tel_npc,
											new S_NpcChatPacket(
													tel_npc,
													"여러분! 어서 윈다우드 사막지역으로 가서 에르자베를 막아주세요!",
													0), true);
								}
							}
						} catch (Exception e) {
						}
					}
				}, 1);
			}

			if (day.getSeconds() == 0 && day.getMinutes() == 0) {
				int rh = day.getHours();
				젠사용 = true;
				BossTimerCheck check = new BossTimerCheck(this);
				check.begin();
				System.out.println("──────────────────────────────────");
				System.out.println("[서버 메세지] 정각 타임을 알림 현제 시각 : " + rh + "시");
				System.out.println("──────────────────────────────────");
				if (rh == 6 || rh == 18) {// 6~12랜덤 18~24 랜덤
					int 삭제시간 = _6시간주기;
					int type = 6;
					int 정무맘보 = _random.nextInt(정무맘보loc.length);
					int 정무심연 = _random.nextInt(정무맘보loc.length);
					int 미믹 = _random.nextInt(상아탑미믹loc.length);
					int 하딘분신 = _random.nextInt(하딘분신loc.length);
					int 흑마법사 = _random.nextInt(흑마법사loc.length);
					int 드레이크 = _random.nextInt(드레이크loc.length);
					int 드레이크랜덤 = _random.nextInt(100);

					L1SpawnUtil.bossspawn(45646, 정무맘보loc[정무심연][0],
							정무맘보loc[정무심연][1], (short) 정무맘보loc[정무심연][2], 삭제시간,
							type);// 심연의 주인
					L1SpawnUtil.bossspawn(45534, 정무맘보loc[정무맘보][0],
							정무맘보loc[정무맘보][1], (short) 정무맘보loc[정무맘보][2], 삭제시간,
							type);// 맘보
					L1SpawnUtil.bossspawn(7300168, 상아탑미믹loc[미믹][0],
							상아탑미믹loc[미믹][1], (short) 상아탑미믹loc[미믹][2], 삭제시간,
							type); // 상아탑 미믹
					L1SpawnUtil.bossspawn(7200185, 하딘분신loc[하딘분신][0],
							하딘분신loc[하딘분신][1], (short) 하딘분신loc[하딘분신][2], 삭제시간,
							type); // 하딘의 분신
					L1SpawnUtil.bossspawn(7280193, 32683, 32862, (short) 284,
							삭제시간, type);// 데몬
					L1SpawnUtil.bossspawn(45943, 32735, 32799, (short) 63,
							삭제시간, type);// 카푸
					L1SpawnUtil.bossspawn(45735, 32899, 33116, (short) 558,
							삭제시간, type);// 반어인
					L1SpawnUtil.bossspawn(45802, 32804, 32870, (short) 256,
							삭제시간, type);// 마이노샤먼
					L1SpawnUtil.bossspawn(45944, 32735, 32799, (short) 63,
							삭제시간, type);// 자이언트웜
					L1SpawnUtil.bossspawn(46026, 32797, 32790, (short) 251,
							삭제시간, type);// 맘몬
					L1SpawnUtil.bossspawn(100088, 32530, 32820, (short) 0,
							삭제시간, type);// 오르쿠스
					L1SpawnUtil.bossspawn(7140179, 흑마법사loc[흑마법사][0],
							흑마법사loc[흑마법사][1], (short) 흑마법사loc[흑마법사][2], 삭제시간,
							type);// 흑마법사
					L1SpawnUtil.bossspawn(100717, 드레이크loc[드레이크][0],
							드레이크loc[드레이크][1], (short) 드레이크loc[드레이크][2], 삭제시간,
							type);// 흑마법사
				}

				// 0
				// 1
				// 2 화
				// 3
				// 4 목
				// 5
				// 6 토

			} else if (day.getSeconds() == 0 && day.getMinutes() == 40) {
				int rh = day.getHours();
				젠사용 = true;
				BossTimerCheck check = new BossTimerCheck(this);
				check.begin();
				rh = day.getHours() + 1;
				if (rh >= 24)
					rh = 0;
				/** 오만 1층~100층까지 보스들 **/

			} else if (day.getSeconds() == 0 && day.getMinutes() == 50) {
				int rh = day.getHours() + 1;
				if (rh >= 24)
					rh = 0;
				젠사용 = true;
				BossTimerCheck check = new BossTimerCheck(this);
				check.begin();
				if (rh == 0 || rh == 2 || rh == 4 || rh == 6 || rh == 8
						|| rh == 10 || rh == 12 || rh == 14 || rh == 16
						|| rh == 18 || rh == 20 || rh == 22) {
					L1SpawnUtil.bossspawn(45456, 32727, 32751, (short) 812, _2시간주기, 2);// 네크
					
					L1SpawnUtil.bossspawn(45488, 32762, 32768, (short) 809, _2시간주기, 2);// 카스파
					L1SpawnUtil.bossspawn(45497, 32763, 32768, (short) 809, _2시간주기, 2);// 메르키오르
					L1SpawnUtil.bossspawn(45464, 32764, 32768, (short) 809, _2시간주기, 2);// 세마
					L1SpawnUtil.bossspawn(45473, 32765, 32768, (short) 809, _2시간주기, 2);// 발터자르
					
					L1SpawnUtil.bossspawn(45488, 32762, 32768, (short) 809, _2시간주기, 2);// 카파팸
					
					L1SpawnUtil.bossspawn(45573, 32706, 32846, (short) 2, _2시간주기, 2);// 바포
				}

				// ////////////////////////////////////1시간주기들/////////////////////////////////
				L1SpawnUtil.bossspawn(45942, 32700, 32830, (short) 61, _1시간주기,
						4);// 저주받은물대
			//	L1SpawnUtil.bossspawn(45941, 32735, 32799, (short) 63, _1시간주기,
					//	4);// 저주받은무녀사엘
				L1SpawnUtil.bossspawn(45931, 32735, 32799, (short) 63, _1시간주기,
						4);// 물의 정령
				L1SpawnUtil.bossspawn(46024, 32737, 32827, (short) 250, _1시간주기,
						4);// 백작친위대장

				// L1SpawnUtil.bossspawn(200281, 33332, 32451, (short)4, _1시간주기,
				// 4);//용계 버모스 이벤트용
				//

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		/**
		 * ==================== 1 시 간 젠 보 스 들
		 * =================================== 크로커다일.드레이크선장.맘보토끼. 이프리트 드레이크 맘보토끼
		 * 흑장로 도펠갱어 그레이트 미노타우르스,커츠,에이션트 자이언트,피닉스,대왕오징어,우두머리 반어인,
		 * 
		 * 오염된 오크투사,스피리드,쿠만.카스파 패밀리.유니콘,몽성대정령,저주받은 물의 대정령 ,저주받은 무녀 사엘,물의 정령,심연의
		 * 주인,마수 군왕 바란카,카푸,자이언트 웜,쿠만 네크로맨서
		 * ======================================
		 * =================================
		 **/
	}

	private L1Location mapRandomXY(int mapid) {
		L1Location loc = new L1Location();
		loc.setMap(mapid);
		loc.set(loc.getMap().getX(), loc.getMap().getY());
		loc = L1Location.randomRangeLocation(loc, loc.getMap().getWidth(), loc
				.getMap().getHeight(), false);
		return loc;
	}

	public class EventNoticeTimer implements Runnable {
		private BossTimer 공지체크 = null;

		public EventNoticeTimer(BossTimer bt) {
			공지체크 = bt;
		}

		@Override
		public void run() {
			try {
				공지체크.공지사용 = false;
				공지체크 = null;
				// this.cancel();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		public void begin() {
			// Timer timer = new Timer();
			// timer.schedule(this, 3000);
			GeneralThreadPool.getInstance().schedule(this, 3000);
		}
	}

	public class BossTimerCheck implements Runnable {
		private BossTimer 젠체크 = null;

		public BossTimerCheck(BossTimer bt) {
			젠체크 = bt;
		}

		@Override
		public void run() {
			try {
				젠체크.젠사용 = false;
				젠체크 = null;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		public void begin() {
			// Timer timer = new Timer();
			// timer.schedule(this, 3000);
			GeneralThreadPool.getInstance().schedule(this, 3000);
		}
	}

	private void RabitHelmTimeCheck(L1PcInstance pc) {
		if (pc instanceof L1RobotInstance)
			return;
		long nowtime = System.currentTimeMillis();
		L1ItemInstance[] itemList = pc.getInventory().findItemsId(20344);
		for (int i = 0; i < itemList.length; i++) {
			if (itemList[i].getEndTime() == null) {
				pc.getInventory().removeItem(itemList[i]);
			} else {
				if (nowtime > itemList[i].getEndTime().getTime())
					pc.getInventory().removeItem(itemList[i]);
			}
		}
		itemList = null;
	}

	private void 셸로브스폰() {
		if (EventShop_셸로브.진행() == true) {
			L1NpcInstance n = L1SpawnUtil.spawn2(32667, 32846, (short) 0,
					100035, 5, bossdietime, 0);// 옛 말하는 섬의 늑대인간
			L1MobGroupSpawn.getInstance().doSpawn(n, 95, false, false);
			L1SpawnUtil.spawn2(32633, 32959, (short) 0, 100036, 5, bossdietime,
					0);// 옛 말하는 섬의 커츠
			// L1MobGroupSpawn.getInstance().doSpawn(n2, 96, false, false);
			L1NpcInstance n3 = L1SpawnUtil.spawn2(32576, 33129, (short) 0,
					100044, 3, bossdietime, 0);// 흑장로
			L1MobGroupSpawn.getInstance().doSpawn(n3, 99, false, false);
			L1NpcInstance n4 = L1SpawnUtil.spawn2(32585, 33122, (short) 0,
					100044, 3, bossdietime, 0);// 흑장로
			L1MobGroupSpawn.getInstance().doSpawn(n4, 99, false, false);
			L1NpcInstance n5 = L1SpawnUtil.spawn2(32579, 33135, (short) 0,
					100044, 3, bossdietime, 0);// 흑장로
			L1MobGroupSpawn.getInstance().doSpawn(n5, 99, false, false);
			L1NpcInstance n6 = L1SpawnUtil.spawn2(32567, 33019, (short) 0,
					100038, 5, bossdietime, 0);// 드웨르그
			L1MobGroupSpawn.getInstance().doSpawn(n6, 100, false, false);
			L1NpcInstance n7 = L1SpawnUtil.spawn2(32565, 33027, (short) 0,
					100038, 5, bossdietime, 0);// 드웨르그
			L1MobGroupSpawn.getInstance().doSpawn(n7, 100, false, false);

			L1SpawnUtil.spawn2(32409, 32908, (short) 0, 100034, 5, bossdietime,
					0);// 옛 말하는 섬의 셸로브
			for (int i = 0; i < 7; i++) {
				L1SpawnUtil.spawn2(32409, 32908, (short) 0, 100042, 10,
						bossdietime, 0);// 옛 말하는 섬의 셸로브
			}
			// L1MobGroupSpawn.getInstance().doSpawn(n3, 97, false, false);
			L1SpawnUtil.spawn2(32491, 32869, (short) 0, 100037, 10,
					bossdietime, 0);// 옛 말하는 섬의 바포
			// L1MobGroupSpawn.getInstance().doSpawn(n4, 98, false, false);
			// L1World.getInstance().broadcastServerMessage("공포의 대상 셀로브 이벤트가 종료되기 10분 전입니다.");
			S_PacketBox pb = new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
					"말하는섬에 다시 돌아온 셸로브와 그 일당들이 나타났습니다!");
			L1World.getInstance().broadcastPacketToAll(pb, true);
		}
	}

	private void 드다2DayGreenMessage() {
		GeneralThreadPool.getInstance().schedule(new Runnable() {
			@Override
			public void run() {
				// TODO 자동 생성된 메소드 스텁
				Date date = new Date(System.currentTimeMillis());
				if (date.getDay() == 3 || date.getDay() == 4) {
					L1World.getInstance().broadcastPacketToAll(
							new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
									"다음 판매시간은 금요일 오후 6시 ~ 12시 입니다."), true);
					L1World.getInstance().broadcastServerMessage(
							"다음 판매시간은 금요일 오후 6시 ~ 12시 입니다.");
				} else {
					L1World.getInstance().broadcastPacketToAll(
							new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
									"다음 판매시간은 수요일 오후 6시 ~ 12시 입니다."), true);
					L1World.getInstance().broadcastServerMessage(
							"다음 판매시간은 수요일 오후 6시 ~ 12시 입니다.");
				}
			}
		}, 10000);
	}

	private void 오림GreenMessage() {
		GeneralThreadPool.getInstance().schedule(new Runnable() {
			@Override
			public void run() {
				// TODO 자동 생성된 메소드 스텁
				Date date = new Date(System.currentTimeMillis());
				if (date.getDay() == 1 || date.getDay() == 2) {// 1월 2화 3수 4목 5금
																// 6토 0일?
					L1World.getInstance()
							.broadcastPacketToAll(
									new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
											"다음 오림의 주문서 상자 판매 시간은 토요일 오후 6시 ~ 12시 입니다."),
									true);
					L1World.getInstance().broadcastServerMessage(
							"다음 오림의 주문서 상자 판매 시간은 토요일 오후 6시 ~ 12시 입니다.");
				} else {
					L1World.getInstance()
							.broadcastPacketToAll(
									new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
											"다음 오림의 주문서 상자 판매시간은 월요일 오후 6시 ~ 12시 입니다."),
									true);
					L1World.getInstance().broadcastServerMessage(
							"다음 오림의 주문서 상자 판매 시간은 월요일 오후 6시 ~ 12시 입니다.");
				}
			}
		}, 10000);
	}

	private void 루피주먹GreenMessage() {
		GeneralThreadPool.getInstance().schedule(new Runnable() {
			@Override
			public void run() {
				// TODO 자동 생성된 메소드 스텁
				Date date = new Date(System.currentTimeMillis());
				L1World.getInstance()
						.broadcastPacketToAll(
								new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
										"루피의 용기의 주먹 주머니 판매시간은 금요일 오후 6시 ~ 일요일 자정 12시 입니다."),
								true);
				L1World.getInstance().broadcastServerMessage(
						"루피의 용기의 주먹 주머니 판매시간은 금요일 오후 6시 ~ 일요일 자정 12시 입니다.");
			}
		}, 10000);
	}

	private void 단테스2DayGreenMessage() {
		GeneralThreadPool.getInstance().schedule(new Runnable() {
			@Override
			public void run() {
				// TODO 자동 생성된 메소드 스텁
				Date date = new Date(System.currentTimeMillis());
				if (date.getDay() == 2 || date.getDay() == 3) {
					L1World.getInstance().broadcastPacketToAll(
							new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
									"다음 단테스 유물 판매 시간은 목요일 오후 6시 ~ 12시 입니다."),
							true);
					L1World.getInstance().broadcastServerMessage(
							"다음 단테스 유물 판매 시간은 목요일 오후 6시 ~ 12시 입니다.");
				} else {
					L1World.getInstance().broadcastPacketToAll(
							new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
									"다음 판매시간은 화요일 오후 6시 ~ 12시 입니다."), true);
					L1World.getInstance().broadcastServerMessage(
							"다음 단테스 유물 판매 시간은 화요일 오후 6시 ~ 12시 입니다.");
				}
			}
		}, 10000);
	}
}