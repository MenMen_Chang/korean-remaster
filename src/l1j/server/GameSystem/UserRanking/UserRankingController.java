package l1j.server.GameSystem.UserRanking;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import server.manager.eva;
import l1j.server.L1DatabaseFactory;
import l1j.server.server.GeneralThreadPool;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.model.skill.L1SkillId;
import l1j.server.server.serverpackets.S_HPUpdate;
import l1j.server.server.serverpackets.S_NewCreateItem;
import l1j.server.server.serverpackets.S_OwnCharStatus;
import l1j.server.server.templates.L1UserRanking;
import l1j.server.server.utils.SQLUtil;

public class UserRankingController implements Runnable {

	private static Logger _log = Logger.getLogger(UserRankingController.class.getName());
	
	private static UserRankingController _instance;
	
	public static boolean isRenewal = false;

	public static UserRankingController getInstance(){
		if (_instance == null){
			_instance = new UserRankingController();
		}
		return _instance;
	}

	private static ArrayList<L1UserRanking> list = null;
	private static ArrayList<L1UserRanking> listPrince = null;
	private static ArrayList<L1UserRanking> listKnight = null;
	private static ArrayList<L1UserRanking> listElf = null;
	private static ArrayList<L1UserRanking> listWizard = null;
	private static ArrayList<L1UserRanking> listDarkElf = null;
	private static ArrayList<L1UserRanking> listDragonKnight = null;
	private static ArrayList<L1UserRanking> listIllusionist = null;
	private static ArrayList<L1UserRanking> listWarrior = null;


	public UserRankingController(){
		list = new ArrayList<L1UserRanking>();
		listPrince = new ArrayList<L1UserRanking>();
		listKnight = new ArrayList<L1UserRanking>();
		listElf = new ArrayList<L1UserRanking>();
		listWizard = new ArrayList<L1UserRanking>();
		listDarkElf = new ArrayList<L1UserRanking>();
		listDragonKnight = new ArrayList<L1UserRanking>();
		listIllusionist = new ArrayList<L1UserRanking>();
		listWarrior = new ArrayList<L1UserRanking>();
		load();
		GeneralThreadPool.getInstance().schedule(this, 1000);
	}

	@Override
	public void run(){
		try {
			Date day = new Date(System.currentTimeMillis());
			
			if(isRenewal || (day.getHours() == 1 && day.getMinutes() == 0 && day.getSeconds() == 0)
					|| (day.getHours() == 3 && day.getMinutes() == 0 && day.getSeconds() == 0)
					|| (day.getHours() == 5 && day.getMinutes() == 0 && day.getSeconds() == 0)
					|| (day.getHours() == 7 && day.getMinutes() == 0 && day.getSeconds() == 0)
					|| (day.getHours() == 9 && day.getMinutes() == 0 && day.getSeconds() == 0)
					|| (day.getHours() == 11 && day.getMinutes() == 0 && day.getSeconds() == 0)
					|| (day.getHours() == 13 && day.getMinutes() == 0 && day.getSeconds() == 0)
					|| (day.getHours() == 15 && day.getMinutes() == 0 && day.getSeconds() == 0)
					|| (day.getHours() == 17 && day.getMinutes() == 0 && day.getSeconds() == 0)
					|| (day.getHours() == 19 && day.getMinutes() == 0 && day.getSeconds() == 0)
					|| (day.getHours() == 21 && day.getMinutes() == 0 && day.getSeconds() == 0)
					|| (day.getHours() == 23 && day.getMinutes() == 0 && day.getSeconds() == 0)){
				isRenewal = false;
				
				for(L1PcInstance pc : L1World.getInstance().getAllPlayers()){
				try {
					pc.save();
				} catch (Exception ex) {
				}
				}
				
				load();
				
				for(L1PcInstance pc : L1World.getInstance().getAllPlayers()){
					int star = getStarCount(pc.getName());
					L1UserRanking rank = UserRankingController.getInstance().getTotalRank(pc.getName());
					if(rank != null){
						pc.sendPackets(new S_NewCreateItem(S_NewCreateItem.유저리스트랭킹, rank));
					}
					if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.rank_1_10_int)) {
						if (star != 11) {
							pc.getInventory().consumeItem(600258, 1);
							pc.getSkillEffectTimerSet().killSkillEffectTimer(L1SkillId.rank_1_10_int);
							pc.addMaxHp(-200);
							pc.sendPackets(new S_HPUpdate(pc.getCurrentHp(), pc.getMaxHp()));
							pc.setRankpvpdmg(pc.getRankpvpdmg() - 2);
							pc.setRankpvprdc(pc.getRankpvprdc() - 2);
							pc.getAbility().addAddedInt((byte)-1);
							pc.getAC().addAc(3);
							pc.sendPackets(new S_OwnCharStatus(pc));
							pc.setrankpoly(false);
							pc.setrankweight(false);
							pc.setrankbuff(false);
							pc.sendPackets(new S_NewCreateItem(L1SkillId.rank_1_10_int, false, pc.getType(), 0));
						}
					} else if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.rank_1_10_dex)) {
						if (star != 11) {
							pc.getInventory().consumeItem(600258, 1);
							pc.getSkillEffectTimerSet().killSkillEffectTimer(L1SkillId.rank_1_10_dex);
							pc.addMaxHp(-200);
							pc.sendPackets(new S_HPUpdate(pc.getCurrentHp(), pc.getMaxHp()));
							pc.setRankpvpdmg(pc.getRankpvpdmg() - 2);
							pc.setRankpvprdc(pc.getRankpvprdc() - 2);
							pc.getAbility().addAddedDex((byte)-1);
							pc.getAC().addAc(3);
							pc.sendPackets(new S_OwnCharStatus(pc));
							pc.setrankpoly(false);
							pc.setrankweight(false);
							pc.setrankbuff(false);
							pc.sendPackets(new S_NewCreateItem(L1SkillId.rank_1_10_dex, false, pc.getType(), 0));
						}
					} else if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.rank_1_10_str)) {
						if (star != 11) {
							pc.getInventory().consumeItem(600258, 1);
							pc.getSkillEffectTimerSet().killSkillEffectTimer(L1SkillId.rank_1_10_str);
							pc.addMaxHp(-200);
							pc.sendPackets(new S_HPUpdate(pc.getCurrentHp(), pc.getMaxHp()));
							pc.setRankpvpdmg(pc.getRankpvpdmg() - 2);
							pc.setRankpvprdc(pc.getRankpvprdc() - 2);
							pc.getAbility().addAddedStr((byte)-1);
							pc.getAC().addAc(3);
							pc.sendPackets(new S_OwnCharStatus(pc));
							pc.setrankpoly(false);
							pc.setrankweight(false);
							pc.setrankbuff(false);
							pc.sendPackets(new S_NewCreateItem(L1SkillId.rank_1_10_str, false, pc.getType(), 0));
						}
					} else if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.rank_11_20_int)) {
						if (star != 10) {
							pc.getSkillEffectTimerSet().killSkillEffectTimer(L1SkillId.rank_11_20_int);
							pc.addMaxHp(-200);
							pc.sendPackets(new S_HPUpdate(pc.getCurrentHp(), pc.getMaxHp()));
							pc.setRankpvpdmg(pc.getRankpvpdmg() - 2);
							pc.setRankpvprdc(pc.getRankpvprdc() - 2);
							pc.getAbility().addAddedInt((byte)-1);
							pc.getAC().addAc(3);
							pc.sendPackets(new S_OwnCharStatus(pc));
							pc.setrankpoly(false);
							pc.setrankbuff(false);
							pc.sendPackets(new S_NewCreateItem(L1SkillId.rank_11_20_int, false, pc.getType(), 0));
						}
					} else if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.rank_11_20_dex)) {
						if (star != 10) {
							pc.getSkillEffectTimerSet().killSkillEffectTimer(L1SkillId.rank_11_20_dex);
							pc.addMaxHp(-200);
							pc.sendPackets(new S_HPUpdate(pc.getCurrentHp(), pc.getMaxHp()));
							pc.setRankpvpdmg(pc.getRankpvpdmg() - 2);
							pc.setRankpvprdc(pc.getRankpvprdc() - 2);
							pc.getAbility().addAddedDex((byte)-1);
							pc.getAC().addAc(3);
							pc.sendPackets(new S_OwnCharStatus(pc));
							pc.setrankpoly(false);
							pc.setrankbuff(false);
							pc.sendPackets(new S_NewCreateItem(L1SkillId.rank_11_20_dex, false, pc.getType(), 0));
						}
					} else if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.rank_11_20_str)) {
						if (star != 10) {
							pc.getSkillEffectTimerSet().killSkillEffectTimer(L1SkillId.rank_11_20_str);
							pc.addMaxHp(-200);
							pc.sendPackets(new S_HPUpdate(pc.getCurrentHp(), pc.getMaxHp()));
							pc.setRankpvpdmg(pc.getRankpvpdmg() - 2);
							pc.setRankpvprdc(pc.getRankpvprdc() - 2);
							pc.getAbility().addAddedStr((byte)-1);
							pc.getAC().addAc(3);
							pc.sendPackets(new S_OwnCharStatus(pc));
							pc.setrankpoly(false);
							pc.setrankbuff(false);
							pc.sendPackets(new S_NewCreateItem(L1SkillId.rank_11_20_str, false, pc.getType(), 0));
						}
					} else if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.rank_21_40)) {
						if (star != 9) {
							pc.getSkillEffectTimerSet().killSkillEffectTimer(L1SkillId.rank_21_40);
							pc.addMaxHp(-200);
							pc.sendPackets(new S_HPUpdate(pc.getCurrentHp(), pc.getMaxHp()));
							pc.setRankpvpdmg(pc.getRankpvpdmg() - 2);
							pc.setRankpvprdc(pc.getRankpvprdc() - 2);
							pc.getAC().addAc(3);
							pc.sendPackets(new S_OwnCharStatus(pc));
							pc.setrankpoly(false);
							pc.setrankbuff(false);
							pc.sendPackets(new S_NewCreateItem(L1SkillId.rank_21_40, false, pc.getType(), 0));
						}	
					} else if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.rank_41_60)) {
						if (star != 8) {
							pc.getSkillEffectTimerSet().killSkillEffectTimer(L1SkillId.rank_41_60);
							pc.addMaxHp(-200);
							pc.sendPackets(new S_HPUpdate(pc.getCurrentHp(), pc.getMaxHp()));
							pc.setRankpvprdc(pc.getRankpvprdc() - 2);
							pc.getAC().addAc(3);
							pc.sendPackets(new S_OwnCharStatus(pc));
							pc.setrankbuff(false);
							pc.sendPackets(new S_NewCreateItem(L1SkillId.rank_41_60, false, pc.getType(), 0));
						}	
					} else if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.rank_61_80)) {
						if (star != 7) {
							pc.getSkillEffectTimerSet().killSkillEffectTimer(L1SkillId.rank_61_80);
							pc.addMaxHp(-200);
							pc.sendPackets(new S_HPUpdate(pc.getCurrentHp(), pc.getMaxHp()));
							pc.setRankpvprdc(pc.getRankpvprdc() - 1);
							pc.getAC().addAc(3);
							pc.sendPackets(new S_OwnCharStatus(pc));
							pc.setrankbuff(false);
							pc.sendPackets(new S_NewCreateItem(L1SkillId.rank_61_80, false, pc.getType(), 0));
						}	
					} else if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.rank_81_100)) {
						if (star != 6) {
							pc.getSkillEffectTimerSet().killSkillEffectTimer(L1SkillId.rank_81_100);
							pc.addMaxHp(-200);
							pc.sendPackets(new S_HPUpdate(pc.getCurrentHp(), pc.getMaxHp()));
							pc.setRankpvprdc(pc.getRankpvprdc() - 1);
							pc.getAC().addAc(2);
							pc.sendPackets(new S_OwnCharStatus(pc));
							pc.setrankbuff(false);
							pc.sendPackets(new S_NewCreateItem(L1SkillId.rank_81_100, false, pc.getType(), 0));
						}	
					} else if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.rank_101_120)) {
						if (star != 5) {
							pc.getSkillEffectTimerSet().killSkillEffectTimer(L1SkillId.rank_101_120);
							pc.addMaxHp(-200);
							pc.sendPackets(new S_HPUpdate(pc.getCurrentHp(), pc.getMaxHp()));
							pc.getAC().addAc(1);
							pc.sendPackets(new S_OwnCharStatus(pc));
							pc.setrankexp(false);
							pc.setrankbuff(false);
							pc.sendPackets(new S_NewCreateItem(L1SkillId.rank_101_120, false, pc.getType(), 0));
						}	
					} else if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.rank_121_140)) {
						if (star != 4) {
							pc.getSkillEffectTimerSet().killSkillEffectTimer(L1SkillId.rank_121_140);
							pc.addMaxHp(-200);
							pc.sendPackets(new S_HPUpdate(pc.getCurrentHp(), pc.getMaxHp()));
							pc.setrankexp(false);
							pc.setrankbuff(false);
							pc.sendPackets(new S_NewCreateItem(L1SkillId.rank_121_140, false, pc.getType(), 0));
						}	
					} else if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.rank_141_160)) {
						if (star != 3) {
							pc.getSkillEffectTimerSet().killSkillEffectTimer(L1SkillId.rank_141_160);
							pc.addMaxHp(-100);
							pc.sendPackets(new S_HPUpdate(pc.getCurrentHp(), pc.getMaxHp()));
							pc.setrankexp(false);
							pc.setrankbuff(false);
							pc.sendPackets(new S_NewCreateItem(L1SkillId.rank_141_160, false, pc.getType(), 0));
						}	
					} else if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.rank_161_180)) {
						if (star != 2) {
							pc.getSkillEffectTimerSet().killSkillEffectTimer(L1SkillId.rank_161_180);
							pc.addMaxHp(-50);
							pc.sendPackets(new S_HPUpdate(pc.getCurrentHp(), pc.getMaxHp()));
							pc.setrankexp(false);
							pc.setrankbuff(false);
							pc.sendPackets(new S_NewCreateItem(L1SkillId.rank_161_180, false, pc.getType(), 0));
						}	
					} else if (pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.rank_181_200)) {
						if (star != 1) {
							pc.getSkillEffectTimerSet().killSkillEffectTimer(L1SkillId.rank_181_200);
							pc.setrankexp(false);
							pc.setrankbuff(false);
							pc.sendPackets(new S_NewCreateItem(L1SkillId.rank_181_200, false, pc.getType(), 0));
						}			
					}

					if (star != 0) {
//						if (!pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.RANKING_BUFF_1)
//								&& !pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.RANKING_BUFF_2)
//								&& !pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.RANKING_BUFF_3)
//								&& !pc.getSkillEffectTimerSet().hasSkillEffect(L1SkillId.RANKING_BUFF_4)) {
						if(!pc.isrankbuff()){
							setBuffSetting(pc);
						}
					}
				}
				eva.EventLogAppend("[유저 랭킹 정보 갱신]: 완료");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		GeneralThreadPool.getInstance().schedule(this, 1000);
	}
	
	public ArrayList<L1UserRanking> getList(int classId) {
		if (classId == 8)
			return list;
		else if (classId == 0)
			return listPrince;
		else if (classId == 1)
			return listKnight;
		else if (classId == 2)
			return listElf;
		else if (classId == 3)
			return listWizard;
		else if (classId == 4)
			return listDarkElf;
		else if (classId == 5)
			return listDragonKnight;
		else if (classId == 6)
			return listIllusionist;
		else if (classId == 7)
			return listWarrior;
		return null;
	}

	public L1UserRanking getTotalRank(String name) {
		for (L1UserRanking user : list) {
			if (user.getName().equalsIgnoreCase(name))
				return user;
		}

		return null;
	}

	public L1UserRanking getClassRank(int classId, String name) {
		ArrayList<L1UserRanking> list = getList(classId);
		if(list == null || list.size() <= 0)
			return null;
		
		for (L1UserRanking rank : list) {
			if (rank.getName().equalsIgnoreCase(name)) {
				return rank;
			}
		}
		return null;
	}
	
	public boolean getClassRank1_3(int classId, String name) {
		for(L1UserRanking rank : getList(classId)) {
			if(rank != null){
				if(rank.getName().equalsIgnoreCase(name)) {
					if(rank.getCurRank() >=1 && rank.getCurRank() <=3){
						return true;
					}
				}
			}
		}
		return false;
	}

	public int getStarCount(String name)
	{
		L1UserRanking rank = getTotalRank(name);
		
		if(rank != null) {
			int curRank = rank.getCurRank();
			if ((curRank >= 1) && (curRank <= 10))
				return 11; //11 max
			else if ((curRank >= 11) && (curRank <= 20))
				return 10;
			else if ((curRank >= 21) && (curRank <= 40))
				return 9;
			else if ((curRank >= 41) && (curRank <= 60))
				return 8;
			else if ((curRank >= 61) && (curRank <= 80))
				return 7;
			else if ((curRank >= 81) && (curRank <= 100))
				return 6;
			else if ((curRank >= 101) && (curRank <= 120))
				return 5;
			else if ((curRank >= 121) && (curRank <= 140))
				return 4;
			else if ((curRank >= 141) && (curRank <= 160))
				return 3;
			else if ((curRank >= 161) && (curRank <= 180))
				return 2;
			else if ((curRank >= 181) && (curRank <= 200))
				return 1;
		}
		return 0;
	
	}

	public void setBuffSetting(L1PcInstance pc){
		int star = getStarCount(pc.getName());
		int skillid = 0;
		switch(star){
		case 11:
			if(pc.isElf()){
				skillid = L1SkillId.rank_1_10_dex;
				pc.getAbility().addAddedDex((byte)1);
			}else if(pc.isWizard() || pc.isIllusionist()){
				skillid = L1SkillId.rank_1_10_int;
				pc.getAbility().addAddedInt((byte)1);
			}else{
				skillid = L1SkillId.rank_1_10_str;
				pc.getAbility().addAddedStr((byte)1);
			}
			pc.getSkillEffectTimerSet().setSkillEffect(skillid, -1);
			pc.addMaxHp(200);
			pc.sendPackets(new S_HPUpdate(pc.getCurrentHp(), pc.getMaxHp()));
			pc.setRankpvpdmg(pc.getRankpvpdmg() + 2);
			pc.setRankpvprdc(pc.getRankpvprdc() + 2);
			pc.getAC().addAc(-3);
			pc.setrankpoly(true);
			pc.setrankweight(true);
			pc.setrankbuff(true);
			if(!pc.getInventory().checkItem(600258))
				pc.getInventory().storeItem(600258, 1); 
			pc.sendPackets(new S_NewCreateItem(skillid, true, pc.getType(), -1));
			pc.sendPackets(new S_OwnCharStatus(pc));
			break;
		case 10:
			if(pc.isElf()){
				skillid = L1SkillId.rank_11_20_dex;
				pc.getAbility().addAddedDex((byte)1);
			}else if(pc.isWizard() || pc.isIllusionist()){
				skillid = L1SkillId.rank_11_20_int;
				pc.getAbility().addAddedInt((byte)1);
			}else{
				skillid = L1SkillId.rank_11_20_str;
				pc.getAbility().addAddedStr((byte)1);
			}
			pc.getSkillEffectTimerSet().setSkillEffect(skillid, -1);
			pc.addMaxHp(200);
			pc.sendPackets(new S_HPUpdate(pc.getCurrentHp(), pc.getMaxHp()));
			pc.setRankpvpdmg(pc.getRankpvpdmg() + 2);
			pc.setRankpvprdc(pc.getRankpvprdc() + 2);
			pc.getAC().addAc(-3);
			pc.setrankpoly(true);
			pc.setrankbuff(true);
			pc.sendPackets(new S_NewCreateItem(skillid, true, pc.getType(), -1));
			pc.sendPackets(new S_OwnCharStatus(pc));
			break;
		case 9:
			skillid = L1SkillId.rank_21_40;
			pc.getSkillEffectTimerSet().setSkillEffect(skillid, -1);
			pc.addMaxHp(200);
			pc.sendPackets(new S_HPUpdate(pc.getCurrentHp(), pc.getMaxHp()));
			pc.setRankpvpdmg(pc.getRankpvpdmg() + 2);
			pc.setRankpvprdc(pc.getRankpvprdc() + 2);
			pc.getAC().addAc(-3);
			pc.setrankpoly(true);
			pc.setrankbuff(true);
			pc.sendPackets(new S_NewCreateItem(skillid, true, pc.getType(), -1));
			pc.sendPackets(new S_OwnCharStatus(pc));
			break;
		case 8:
			skillid = L1SkillId.rank_41_60;
			pc.getSkillEffectTimerSet().setSkillEffect(skillid, -1);
			pc.addMaxHp(200);
			pc.sendPackets(new S_HPUpdate(pc.getCurrentHp(), pc.getMaxHp()));
			pc.setRankpvprdc(pc.getRankpvprdc() + 2);
			pc.getAC().addAc(-3);
			pc.setrankbuff(true);
			pc.sendPackets(new S_NewCreateItem(skillid, true, pc.getType(), -1));
			pc.sendPackets(new S_OwnCharStatus(pc));
			break;
		case 7:
			skillid = L1SkillId.rank_61_80;
			pc.getSkillEffectTimerSet().setSkillEffect(skillid, -1);
			pc.addMaxHp(200);
			pc.sendPackets(new S_HPUpdate(pc.getCurrentHp(), pc.getMaxHp()));
			pc.setRankpvprdc(pc.getRankpvprdc() + 1);
			pc.getAC().addAc(-3);
			pc.setrankbuff(true);
			pc.sendPackets(new S_NewCreateItem(skillid, true, pc.getType(), -1));
			pc.sendPackets(new S_OwnCharStatus(pc));
			break;
		case 6:
			skillid = L1SkillId.rank_81_100;
			pc.getSkillEffectTimerSet().setSkillEffect(skillid, -1);
			pc.addMaxHp(200);
			pc.sendPackets(new S_HPUpdate(pc.getCurrentHp(), pc.getMaxHp()));
			pc.setRankpvprdc(pc.getRankpvprdc() + 1);
			pc.getAC().addAc(-2);
			pc.setrankbuff(true);
			pc.sendPackets(new S_NewCreateItem(skillid, true, pc.getType(), -1));
			pc.sendPackets(new S_OwnCharStatus(pc));
			break;
		case 5:
			skillid = L1SkillId.rank_101_120;
			pc.getSkillEffectTimerSet().setSkillEffect(skillid, -1);
			pc.addMaxHp(200);
			pc.sendPackets(new S_HPUpdate(pc.getCurrentHp(), pc.getMaxHp()));
			pc.getAC().addAc(-1);
			pc.setrankexp(true);
			pc.setrankbuff(true);
			pc.sendPackets(new S_NewCreateItem(skillid, true, pc.getType(), -1));
			pc.sendPackets(new S_OwnCharStatus(pc));
			break;
		case 4:
			skillid = L1SkillId.rank_121_140;
			pc.getSkillEffectTimerSet().setSkillEffect(skillid, -1);
			pc.addMaxHp(200);
			pc.sendPackets(new S_HPUpdate(pc.getCurrentHp(), pc.getMaxHp()));
			pc.setrankexp(true);
			pc.setrankbuff(true);
			pc.sendPackets(new S_NewCreateItem(skillid, true, pc.getType(), -1));
			pc.sendPackets(new S_OwnCharStatus(pc));
			break;
		case 3:
			skillid = L1SkillId.rank_141_160;
			pc.getSkillEffectTimerSet().setSkillEffect(skillid, -1);
			pc.addMaxHp(100);
			pc.sendPackets(new S_HPUpdate(pc.getCurrentHp(), pc.getMaxHp()));
			pc.setrankexp(true);
			pc.setrankbuff(true);
			pc.sendPackets(new S_NewCreateItem(skillid, true, pc.getType(), -1));
			pc.sendPackets(new S_OwnCharStatus(pc));
			break;
		case 2:
			skillid = L1SkillId.rank_161_180;
			pc.getSkillEffectTimerSet().setSkillEffect(skillid, -1);
			pc.addMaxHp(50);
			pc.sendPackets(new S_HPUpdate(pc.getCurrentHp(), pc.getMaxHp()));
			pc.setrankexp(true);
			pc.setrankbuff(true);
			pc.sendPackets(new S_NewCreateItem(skillid, true, pc.getType(), -1));
			pc.sendPackets(new S_OwnCharStatus(pc));
			break;
		case 1:
			skillid = L1SkillId.rank_181_200;
			pc.getSkillEffectTimerSet().setSkillEffect(skillid, -1);
			pc.setrankexp(true);
			pc.setrankbuff(true);
			pc.sendPackets(new S_NewCreateItem(skillid, true, pc.getType(), -1));
			pc.sendPackets(new S_OwnCharStatus(pc));
			break;
		}
		if(star != 11)
			pc.getInventory().consumeItem(600258, 1);
	}


	private void load() {

		ArrayList<L1UserRanking> templist = new ArrayList<L1UserRanking>();
		ArrayList<L1UserRanking> templistPrince = new ArrayList<L1UserRanking>();
		ArrayList<L1UserRanking> templistKnight = new ArrayList<L1UserRanking>();
		ArrayList<L1UserRanking> templistElf = new ArrayList<L1UserRanking>();
		ArrayList<L1UserRanking> templistWizard = new ArrayList<L1UserRanking>();
		ArrayList<L1UserRanking> templistDarkElf = new ArrayList<L1UserRanking>();
		ArrayList<L1UserRanking> templistDragonKnight = new ArrayList<L1UserRanking>();
		ArrayList<L1UserRanking> templistIllusionist = new ArrayList<L1UserRanking>();
		ArrayList<L1UserRanking> templistWarrior = new ArrayList<L1UserRanking>();

		for (int a = 0; a < 9; a++) {
			Connection con = null;
			PreparedStatement pstm = null;
			ResultSet rs = null;
			int i = 0;
			try {
				con = L1DatabaseFactory.getInstance().getConnection();
				if (a == 8)
					pstm = con.prepareStatement("SELECT char_name, Type FROM characters WHERE level > 1 AND AccessLevel = 0 order by Exp desc limit 200");
				else
					pstm = con.prepareStatement("SELECT char_name, Type FROM characters WHERE Type = " + a + " AND level > 1 AND AccessLevel = 0 order by Exp desc limit 200");
				rs = pstm.executeQuery();

				while (rs.next()) {
					String name = rs.getString("char_name");
					int type = rs.getInt("Type");

					L1UserRanking rank = new L1UserRanking();

					rank.setName(name);
					rank.setCurRank(++i);

					L1UserRanking oldRank = null;
					if (a == 8) {
						oldRank = getTotalRank(name);
					} else {
						oldRank = getClassRank(a, name);
					}

					if (oldRank == null)
						rank.setOldRank(rank.getCurRank());
					else
						rank.setOldRank(oldRank.getCurRank());

					rank.setClassId(type);

					if (a == 8)
						templist.add(rank);
					else if (a == 0)
						templistPrince.add(rank);
					else if (a == 1)
						templistKnight.add(rank);
					else if (a == 2)
						templistElf.add(rank);
					else if (a == 3)
						templistWizard.add(rank);
					else if (a == 4)
						templistDarkElf.add(rank);
					else if (a == 5)
						templistDragonKnight.add(rank);
					else if (a == 6)
						templistIllusionist.add(rank);
					else if (a == 7)
						templistWarrior.add(rank);
				}
			} catch (SQLException e) {
				_log.log(Level.SEVERE, e.getLocalizedMessage(), e);
			} finally {
				SQLUtil.close(rs);
				SQLUtil.close(pstm);
				SQLUtil.close(con);
			}
		}

		list.clear();
		listPrince.clear();
		listKnight.clear();
		listElf.clear();
		listWizard.clear();
		listDarkElf.clear();
		listDragonKnight.clear();
		listIllusionist.clear();
		listWarrior.clear();
		
		list.addAll(templist);
		listPrince.addAll(templistPrince);
		listKnight.addAll(templistKnight);
		listElf.addAll(templistElf);
		listWizard.addAll(templistWizard);
		listDarkElf.addAll(templistDarkElf);
		listDragonKnight.addAll(templistDragonKnight);
		listIllusionist.addAll(templistIllusionist);
		listWarrior.addAll(templistWarrior);
	}

}
