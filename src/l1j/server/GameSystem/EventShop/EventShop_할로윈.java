package l1j.server.GameSystem.EventShop;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.util.ArrayList;

import l1j.server.L1DatabaseFactory;
import l1j.server.GameSystem.Robot.L1RobotInstance;
import l1j.server.Warehouse.ClanWarehouse;
import l1j.server.Warehouse.PrivateWarehouse;
import l1j.server.Warehouse.WarehouseManager;
import l1j.server.server.model.L1Inventory;
import l1j.server.server.model.L1Object;
import l1j.server.server.model.L1PcInventory;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1ItemInstance;
import l1j.server.server.model.Instance.L1MonsterInstance;
import l1j.server.server.model.Instance.L1NpcInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.utils.L1SpawnUtil;
import l1j.server.server.utils.SQLUtil;

public class EventShop_할로윈 {
	private static EventShop_할로윈 _instance;

	public static EventShop_할로윈 getInstance() {
		if (_instance == null) {
			_instance = new EventShop_할로윈();
		}
		return _instance;
	}

	public static int NPCID = 100672;

	public static L1NpcInstance 블룸;
	public static L1NpcInstance 퀸스지;
	private static ArrayList<L1NpcInstance> 상점 = new ArrayList<L1NpcInstance>();

	public synchronized static ArrayList<L1NpcInstance> getEventShopNpc() {
		return 상점;
	}

	private static boolean 진행중 = false;

	private static Timestamp _남은시간 = null;

	public synchronized static void 진행(boolean f) {
		진행중 = f;
	}

	public synchronized static boolean 진행() {
		return 진행중;
	}

	public synchronized static void 남은시간(Timestamp f) {
		_남은시간 = f;
	}

	public synchronized static Timestamp 남은시간() {
		return _남은시간;
	}

	@SuppressWarnings("deprecation")
	public synchronized static void add남은시간(long f) {
		int month = _남은시간.getMonth() + 1;
		long time = 남은시간().getTime() + f;
		Timestamp deleteTime = null;
		deleteTime = new Timestamp(time);
		_남은시간 = deleteTime;
		L1World.getInstance().broadcastServerMessage(
				"운영자의 권한으로 할로윈 이벤트가 연장되었습니다.");
		L1World.getInstance().broadcastServerMessage(
				"종료시간은 [" + month + "월 " + _남은시간.getDate() + "일 "
						+ _남은시간.getHours() + "시 " + _남은시간.getMinutes() + "분 "
						+ _남은시간.getSeconds() + "초]입니다.");
		EventShopTable.Update(NPCID, deleteTime);
	}

	public synchronized static void addEventShopNpc(L1NpcInstance npc,
			Timestamp time) {
		if (npc.getNpcId() == NPCID) {
			if (상점.contains(npc)) {
				return;
			}
			상점.add(npc);
			진행(true);
			남은시간(time);
			System.out.print("■ 진행중이벤트 브리핑 ........................ ");
			System.out.println("■ 호박 할로윈 진행");
			System.out.println("──────────────────────────────────");
			EventShopList.Add(npc);
			블룸 = L1SpawnUtil.spawn2(33447, 32812, (short) 4, 100387, 0, 0, 0);
			퀸스지 = L1SpawnUtil.spawn2(33447, 32815, (short) 4, 100671, 0, 0, 0);
		}
	}

	public synchronized static void oddEventShopNpc(L1NpcInstance npc) {
		if (npc.getNpcId() == NPCID) {
			if (!상점.contains(npc)) {
				return;
			}
			상점.remove(npc);
			진행(false);
			남은시간(null);
			System.out.println("[이벤트상점] 종료 - 할로윈");
			EventShopList.Odd(npc);
			블룸.deleteMe();
			퀸스지.deleteMe();
			EventMonsterDelete();
			EventItemDelete();
		}
	}

	private static void EventMonsterDelete() {
		// TODO 자동 생성된 메소드 스텁
		try {
			for (L1Object obj : L1World.getInstance().getObject()) {
				if (obj == null)
					continue;
				if (obj instanceof L1MonsterInstance) {
					L1MonsterInstance mon = (L1MonsterInstance) obj;
					if (!mon._destroyed && !mon.isDead()
							&& mon.getNpcId() == 100388) {
						mon.deleteMe();
					}
				}
			}
		} catch (Exception e) {
		}
	}

	private static int delItemlist[] = { 435000, 60190, 60191, 21123, 21124,
			427306, 256, 4500027, 263, 4500026, 265, 264, 60190, 60191, 60192,
			60193, 60194, 60195, 60196, 60197, 60198, 60423, 21255, 60424,
			60425, 60426 };

	public synchronized static void EventItemDelete() {
		try {
			for (L1PcInstance tempPc : L1World.getInstance().getAllPlayers()) {
				if (tempPc == null)
					continue;
				for (int i = 0; i < delItemlist.length; i++) {
					int enc = 8;
					if (delItemlist[i] == 21255)
						enc = 7;
					L1ItemInstance[] item = tempPc.getInventory().findItemsId(
							delItemlist[i]);
					if (item != null && item.length > 0) {
						for (int o = 0; o < item.length; o++) {
							if (item[o].getEnchantLevel() < enc)
								tempPc.getInventory().removeItem(item[o]);
							else {
								if (item[o].getItemId() == 21255
										&& item[o].getEnchantLevel() >= 9) {
									item[o].setBless(0);
									tempPc.getInventory().updateItem(item[o],
											L1PcInventory.COL_BLESS);
									tempPc.getInventory().saveItem(item[o],
											L1PcInventory.COL_BLESS);
								}
							}
						}
					}
					try {
						PrivateWarehouse pw = WarehouseManager.getInstance()
								.getPrivateWarehouse(tempPc.getAccountName());
						L1ItemInstance[] item2 = pw.findItemsId(delItemlist[i]);
						if (item2 != null && item2.length > 0) {
							for (int o = 0; o < item2.length; o++) {
								if (item2[o].getEnchantLevel() < enc)
									pw.removeItem(item2[o]);
								else {
									if (item2[o].getItemId() == 21255
											&& item2[o].getEnchantLevel() >= 9)
										item2[o].setBless(0);
								}
							}
						}
					} catch (Exception e) {
					}
					try {
						if (tempPc.getClanid() > 0) {
							ClanWarehouse cw = WarehouseManager.getInstance()
									.getClanWarehouse(tempPc.getClanname());
							L1ItemInstance[] item3 = cw
									.findItemsId(delItemlist[i]);
							if (item3 != null && item3.length > 0) {
								for (int o = 0; o < item3.length; o++) {
									if (item3[o].getEnchantLevel() < enc)
										cw.removeItem(item3[o]);
									else {
										if (item3[o].getItemId() == 21255
												&& item3[o].getEnchantLevel() >= 9)
											item3[o].setBless(0);
									}
								}
							}
						}
					} catch (Exception e) {
					}
					try {
						if (tempPc.getPetListSize() > 0) {
							for (L1NpcInstance npc : tempPc.getPetList()) {
								L1ItemInstance[] pitem = npc.getInventory()
										.findItemsId(delItemlist[i]);
								if (pitem != null && pitem.length > 0) {
									for (int o = 0; o < pitem.length; o++) {
										if (pitem[o].getEnchantLevel() < enc)
											npc.getInventory().removeItem(
													pitem[o]);
										else {
											if (pitem[o].getItemId() == 21255
													&& pitem[o]
															.getEnchantLevel() >= 9)
												pitem[o].setBless(0);
										}
									}
								}
							}
						}
					} catch (Exception e) {
					}
				}
			}

			try {
				for (L1Object obj : L1World.getInstance().getAllItem()) {
					if (!(obj instanceof L1ItemInstance))
						continue;
					L1ItemInstance temp_item = (L1ItemInstance) obj;
					if (temp_item.getItemOwner() == null
							|| !(temp_item.getItemOwner() instanceof L1RobotInstance)) {
						if (temp_item.getX() == 0 && temp_item.getY() == 0)
							continue;
					}
					for (int ii = 0; ii < delItemlist.length; ii++) {
						int enc = 8;
						if (delItemlist[ii] == 21255)
							enc = 7;
						if (delItemlist[ii] == temp_item.getItemId()) {
							if (temp_item.getEnchantLevel() < enc) {
								L1Inventory groundInventory = L1World
										.getInstance().getInventory(
												temp_item.getX(),
												temp_item.getY(),
												temp_item.getMapId());
								groundInventory.removeItem(temp_item);
							} else {
								if (temp_item.getItemId() == 21255
										&& temp_item.getEnchantLevel() >= 9)
									temp_item.setBless(0);
							}
							break;
						}
					}
				}
			} catch (Exception e) {
			}

			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < delItemlist.length; i++) {
				if (delItemlist[i] == 21255) {
					Delete(delItemlist[i]);
					wareDelete(delItemlist[i]);
					ClanwareDelete(delItemlist[i]);
					continue;
				}
				sb.append(+delItemlist[i]);
				if (i < delItemlist.length - 1) {
					sb.append(",");
				}
			}
			Delete(sb.toString());

			/*
			 * for(int i = 0; i < delItemlist.length; i++){
			 * Delete(delItemlist[i]); wareDelete(delItemlist[i]);
			 * ClanwareDelete(delItemlist[i]); }
			 */
		} catch (Exception e) {
		}
	}

	private static void Delete(String id_name) {
		Connection con = null;
		PreparedStatement pstm = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con
					.prepareStatement("delete FROM character_items WHERE item_id IN ("
							+ id_name + ") AND enchantlvl < 8");
			pstm.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(pstm);
		}
		try {
			pstm = con
					.prepareStatement("delete FROM character_warehouse WHERE item_id in ("
							+ id_name + ") AND enchantlvl < 8");
			pstm.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(pstm);
		}
		try {
			pstm = con
					.prepareStatement("delete FROM clan_warehouse WHERE item_id in ("
							+ id_name + ") AND enchantlvl < 8");
			pstm.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	private static void Delete(int itemid) {
		Connection con = null;
		PreparedStatement pstm = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			if (itemid == 21255)
				pstm = con
						.prepareStatement("delete FROM character_items WHERE item_id=? AND enchantlvl < 7");
			else
				pstm = con
						.prepareStatement("delete FROM character_items WHERE item_id=? AND enchantlvl < 8");
			pstm.setInt(1, itemid);
			pstm.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
		if (itemid == 21255) {
			try {
				con = L1DatabaseFactory.getInstance().getConnection();
				pstm = con
						.prepareStatement("UPDATE character_items SET bless = 0 WHERE item_id=? AND enchantlvl > 8");
				pstm.setInt(1, itemid);
				pstm.executeUpdate();
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				SQLUtil.close(pstm);
				SQLUtil.close(con);
			}
		}
	}

	private static void wareDelete(int itemid) {
		Connection con = null;
		PreparedStatement pstm = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			if (itemid == 21255)
				pstm = con
						.prepareStatement("delete FROM character_warehouse WHERE item_id=? AND enchantlvl < 7");
			else
				pstm = con
						.prepareStatement("delete FROM character_warehouse WHERE item_id=? AND enchantlvl < 8");
			pstm.setInt(1, itemid);
			pstm.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
		if (itemid == 21255) {
			try {
				con = L1DatabaseFactory.getInstance().getConnection();
				pstm = con
						.prepareStatement("UPDATE character_warehouse SET bless = 0 WHERE item_id=? AND enchantlvl > 8");
				pstm.setInt(1, itemid);
				pstm.executeUpdate();
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				SQLUtil.close(pstm);
				SQLUtil.close(con);
			}
		}
	}

	private static void ClanwareDelete(int itemid) {
		Connection con = null;
		PreparedStatement pstm = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			if (itemid == 21255)
				pstm = con
						.prepareStatement("delete FROM clan_warehouse WHERE item_id=? AND enchantlvl < 7");
			else
				pstm = con
						.prepareStatement("delete FROM clan_warehouse WHERE item_id=? AND enchantlvl < 8");
			pstm.setInt(1, itemid);
			pstm.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}
}