package l1j.server.GameSystem.EventShop;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;

import l1j.server.L1DatabaseFactory;
import l1j.server.GameSystem.Robot.L1RobotInstance;
import l1j.server.Warehouse.ClanWarehouse;
import l1j.server.Warehouse.PrivateWarehouse;
import l1j.server.Warehouse.WarehouseManager;
import l1j.server.server.datatables.NpcTable;
import l1j.server.server.model.L1Inventory;
import l1j.server.server.model.L1Object;
import l1j.server.server.model.L1PcInventory;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1ItemInstance;
import l1j.server.server.model.Instance.L1NpcInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.templates.L1Npc;
import l1j.server.server.utils.SQLUtil;

public class EventShop_신묘한삭제 {
	private static EventShop_신묘한삭제 _instance;

	public static EventShop_신묘한삭제 getInstance() {
		if (_instance == null) {
			_instance = new EventShop_신묘한삭제();
		}
		return _instance;
	}

	public static int NPCID = 100411;

	private static ArrayList<L1NpcInstance> 상점 = new ArrayList<L1NpcInstance>();

	public synchronized static ArrayList<L1NpcInstance> getEventShopNpc() {
		return 상점;
	}

	private static boolean 진행중 = false;

	private static Timestamp _남은시간 = null;

	public synchronized static void 진행(boolean f) {
		진행중 = f;
	}

	public synchronized static boolean 진행() {
		return 진행중;
	}

	public synchronized static void 남은시간(Timestamp f) {
		_남은시간 = f;
	}

	public synchronized static Timestamp 남은시간() {
		return _남은시간;
	}

	@SuppressWarnings("deprecation")
	public synchronized static void add남은시간(long f) {
		int month = _남은시간.getMonth() + 1;
		long time = 남은시간().getTime() + f;
		Timestamp deleteTime = null;
		deleteTime = new Timestamp(time);
		_남은시간 = deleteTime;
		EventShopTable.Update(NPCID, deleteTime);
	}

	public synchronized static void addEventShopNpc(L1NpcInstance npc,
			Timestamp time) {
		if (npc.getNpcId() == NPCID) {
			if (상점.contains(npc)) {
				return;
			}
			상점.add(npc);
			진행(true);
			남은시간(time);
			System.out.print("■ 진행중이벤트 브리핑 ........................ ");
			System.out.println("■ 신묘삭제 진행");
			System.out.println("──────────────────────────────────");
			EventShopList.Add(npc);
		}
	}

	public synchronized static void oddEventShopNpc(L1NpcInstance npc) {
		if (npc.getNpcId() == NPCID) {
			if (!상점.contains(npc)) {
				return;
			}
			상점.remove(npc);
			진행(false);
			남은시간(null);
			System.out.println("[이벤트상점] 종료 - 신묘한삭제");
			EventShopList.Odd(npc);
			아이템삭제();
		}
	}

	private static int[] 신묘아이템들 = { 427110, 427111, 427112, 450022, 450023,
			450024, 450025 };

	public static void 아이템삭제() {
		for (L1PcInstance tempPc : L1World.getInstance().getAllPlayers()) {
			if (tempPc == null)
				continue;
			for (int i = 0; i < 신묘아이템들.length; i++) {
				L1ItemInstance[] item = tempPc.getInventory().findItemsId(
						신묘아이템들[i]);
				if (item.length > 0) {
					for (int o = 0; o < item.length; o++) {
						if ((item[o].getItem().getType2() == 1 && item[o]
								.getEnchantLevel() < 8)
								|| (item[o].getItem().getType2() == 2 && item[o]
										.getEnchantLevel() < 6))
							tempPc.getInventory().removeItem(item[o]);
						else {
							if (item[o].getBless() >= 128)
								item[o].setBless(128);
							else
								item[o].setBless(0);
							tempPc.getInventory().updateItem(item[o],
									L1PcInventory.COL_BLESS);
							tempPc.getInventory().saveItem(item[o],
									L1PcInventory.COL_BLESS);
						}
					}
				}
				item = null;
				try {
					PrivateWarehouse pw = WarehouseManager.getInstance()
							.getPrivateWarehouse(tempPc.getAccountName());
					L1ItemInstance[] item2 = pw.findItemsId(신묘아이템들[i]);
					if (item2 != null && item2.length > 0) {
						for (int o = 0; o < item2.length; o++) {
							if ((item2[o].getItem().getType2() == 1 && item2[o]
									.getEnchantLevel() < 8)
									|| (item2[o].getItem().getType2() == 2 && item2[o]
											.getEnchantLevel() < 6))
								pw.removeItem(item2[o]);
							else {
								if (item2[o].getBless() >= 128)
									item2[o].setBless(128);
								else
									item2[o].setBless(0);
							}
						}
					}
				} catch (Exception e) {
				}
				try {
					if (tempPc.getClanid() > 0) {
						ClanWarehouse cw = WarehouseManager.getInstance()
								.getClanWarehouse(tempPc.getClanname());
						L1ItemInstance[] item3 = cw.findItemsId(신묘아이템들[i]);
						if (item3 != null && item3.length > 0) {
							for (int o = 0; o < item3.length; o++) {
								if ((item3[o].getItem().getType2() == 1 && item3[o]
										.getEnchantLevel() < 8)
										|| (item3[o].getItem().getType2() == 2 && item3[o]
												.getEnchantLevel() < 6))
									cw.removeItem(item3[o]);
								else {
									if (item3[o].getBless() >= 128)
										item3[o].setBless(128);
									else
										item3[o].setBless(0);
								}
							}
						}
					}
				} catch (Exception e) {
				}
				try {
					if (tempPc.getPetListSize() > 0) {
						for (L1NpcInstance npc : tempPc.getPetList()) {
							L1ItemInstance[] pitem = npc.getInventory()
									.findItemsId(신묘아이템들[i]);
							if (pitem != null && pitem.length > 0) {
								for (int o = 0; o < pitem.length; o++) {
									if ((pitem[o].getItem().getType2() == 1 && pitem[o]
											.getEnchantLevel() < 8)
											|| (pitem[o].getItem().getType2() == 2 && pitem[o]
													.getEnchantLevel() < 6))
										npc.getInventory().removeItem(pitem[o]);
									else {
										if (pitem[o].getBless() >= 128)
											pitem[o].setBless(128);
										else
											pitem[o].setBless(0);
									}
								}
							}
						}
					}
				} catch (Exception e) {
				}
			}
			L1ItemInstance item2 = tempPc.getInventory().findItemId(20344);
			if (item2 != null)
				tempPc.getInventory().removeItem(item2);
		}
		try {
			for (L1Object obj : L1World.getInstance().getAllItem()) {
				if (!(obj instanceof L1ItemInstance))
					continue;
				L1ItemInstance temp_item = (L1ItemInstance) obj;
				if (temp_item.getItemOwner() == null
						|| !(temp_item.getItemOwner() instanceof L1RobotInstance)) {
					if (temp_item.getX() == 0 && temp_item.getY() == 0)
						continue;
				}
				for (int ii = 0; ii < 신묘아이템들.length; ii++) {
					if (신묘아이템들[ii] == temp_item.getItemId()) {
						if ((temp_item.getItem().getType2() == 1 && temp_item
								.getEnchantLevel() < 8)
								|| (temp_item.getItem().getType2() == 2 && temp_item
										.getEnchantLevel() < 6)) {
							L1Inventory groundInventory = L1World.getInstance()
									.getInventory(temp_item.getX(),
											temp_item.getY(),
											temp_item.getMapId());
							groundInventory.removeItem(temp_item);
						} else {
							if (temp_item.getBless() >= 128)
								temp_item.setBless(128);
							else
								temp_item.setBless(0);
						}
						break;
					}
				}

			}
		} catch (Exception e) {
		}
		boolean wa = false;
		for (int i = 0; i < 신묘아이템들.length; i++) {
			if (i >= 3)
				wa = true;
			EventShop_신묘한삭제.Delete(신묘아이템들[i], wa);
			EventShop_신묘한삭제.wareDelete(신묘아이템들[i], wa);
			EventShop_신묘한삭제.ClanwareDelete(신묘아이템들[i], wa);
		}
		for (int i = 0; i < 신묘아이템들.length; i++) {
			EventShop_신묘한삭제.BlessUpdate(신묘아이템들[i]);
		}
		EventShop_신묘한삭제.Delete(20344);
	}

	public static void 기본시간후삭제시간(long time) {
		L1Npc npc = NpcTable.getInstance().getTemplate(NPCID);
		Timestamp DelTime = new Timestamp((long) time + (long) 604800000);// 7일
		EventShopTable.Store(npc, 0, 0, 0, 0, DelTime);
		EventShopTable.Spawn(NPCID, 0, 0, (short) 0, 0, DelTime);
	}

	public static void BlessUpdate(int itemid) {
		Connection con = null;
		Connection con2 = null;
		PreparedStatement pstm = null;
		PreparedStatement pstm2 = null;
		ResultSet rs = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con
					.prepareStatement("SELECT id, bless, enchantlvl FROM character_items WHERE item_id=?");
			// pstm =
			// con.prepareStatement("SELECT * FROM character_items WHERE item_id=?");
			pstm.setInt(1, itemid);
			rs = pstm.executeQuery();
			while (rs.next()) {
				int id = rs.getInt("id");
				int bless = rs.getInt("bless");
				int ent = rs.getInt("enchantlvl");
				if ((itemid >= 450022 && itemid <= 450025 && ent >= 8)
						|| (itemid >= 427110 && itemid <= 427112 && ent >= 6)) {
				} else
					continue;
				if (bless == 128 || bless == 0)
					continue;
				try {
					con2 = L1DatabaseFactory.getInstance().getConnection();
					pstm2 = con2
							.prepareStatement("UPDATE character_items SET bless =? WHERE id=?");
					pstm2.setInt(1, bless > 128 ? 128 : 0);
					pstm2.setInt(2, id);
					pstm2.executeUpdate();
				} finally {
					SQLUtil.close(pstm2);
					SQLUtil.close(con2);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(rs);
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	public static void Delete(int itemid, boolean weapon) {
		Connection con = null;
		PreparedStatement pstm = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			if (weapon)
				pstm = con
						.prepareStatement("delete FROM character_items WHERE item_id=? AND enchantlvl < 8 ");
			else
				pstm = con
						.prepareStatement("delete FROM character_items WHERE item_id=? AND enchantlvl < 6 ");
			pstm.setInt(1, itemid);
			pstm.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	public static void wareDelete(int itemid, boolean weapon) {
		Connection con = null;
		PreparedStatement pstm = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			if (weapon)
				pstm = con
						.prepareStatement("delete FROM character_warehouse WHERE item_id=? AND enchantlvl < 8 ");
			else
				pstm = con
						.prepareStatement("delete FROM character_warehouse WHERE item_id=? AND enchantlvl < 6 ");
			pstm.setInt(1, itemid);
			pstm.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	public static void ClanwareDelete(int itemid, boolean weapon) {
		Connection con = null;
		PreparedStatement pstm = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			if (weapon)
				pstm = con
						.prepareStatement("delete FROM clan_warehouse WHERE item_id=? AND enchantlvl < 8 ");
			else
				pstm = con
						.prepareStatement("delete FROM clan_warehouse WHERE item_id=? AND enchantlvl < 6 ");
			pstm.setInt(1, itemid);
			pstm.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}

	public static void Delete(int itemid) {
		Connection con = null;
		PreparedStatement pstm = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con
					.prepareStatement("delete FROM character_items WHERE item_id=?");
			pstm.setInt(1, itemid);
			pstm.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}
}