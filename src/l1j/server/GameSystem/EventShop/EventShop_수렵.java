package l1j.server.GameSystem.EventShop;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.util.ArrayList;

import l1j.server.L1DatabaseFactory;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1NpcInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.utils.L1SpawnUtil;
import l1j.server.server.utils.SQLUtil;

public class EventShop_수렵 {
	private static EventShop_수렵 _instance;

	public static EventShop_수렵 getInstance() {
		if (_instance == null) {
			_instance = new EventShop_수렵();
		}
		return _instance;
	}

	public static int NPCID = 100324;

	public static L1NpcInstance 노무르;
	public static L1NpcInstance 시이니;
	private static ArrayList<L1NpcInstance> 상점 = new ArrayList<L1NpcInstance>();

	public synchronized static ArrayList<L1NpcInstance> getEventShopNpc() {
		return 상점;
	}

	private static boolean 진행중 = false;

	private static Timestamp _남은시간 = null;

	public synchronized static void 진행(boolean f) {
		진행중 = f;
	}

	public synchronized static boolean 진행() {
		return 진행중;
	}

	public synchronized static void 남은시간(Timestamp f) {
		_남은시간 = f;
	}

	public synchronized static Timestamp 남은시간() {
		return _남은시간;
	}

	@SuppressWarnings("deprecation")
	public synchronized static void add남은시간(long f) {
		int month = _남은시간.getMonth() + 1;
		long time = 남은시간().getTime() + f;
		Timestamp deleteTime = null;
		deleteTime = new Timestamp(time);
		_남은시간 = deleteTime;
		L1World.getInstance().broadcastServerMessage("운영자의 권한으로 수렵 이벤트가 연장되었습니다.");
		L1World.getInstance().broadcastServerMessage(
				"종료시간은 [" + month + "월 " + _남은시간.getDate() + "일 "
						+ _남은시간.getHours() + "시 " + _남은시간.getMinutes() + "분 "
						+ _남은시간.getSeconds() + "초]입니다.");
		EventShopTable.Update(NPCID, deleteTime);
	}

	public synchronized static void addEventShopNpc(L1NpcInstance npc,
			Timestamp time) {
		if (npc.getNpcId() == NPCID) {
			if (상점.contains(npc)) {
				return;
			}
			상점.add(npc);
			진행(true);
			남은시간(time);
			//System.out.println("[이벤트상점] 진행 - 수렵");
			System.out.print("■ 진행중이벤트 브리핑 ........................ ");
			System.out.println("■ 국왕 수렵 진행");
			System.out.println("──────────────────────────────────");
			EventShopList.Add(npc);
			노무르 = L1SpawnUtil.spawn2(33445, 32804, (short) 4, 100325, 0, 0, 0);
			시이니 = L1SpawnUtil.spawn2(33445, 32808, (short) 4, 100326, 0, 0, 0);
		}
	}

	public synchronized static void oddEventShopNpc(L1NpcInstance npc) {
		if (npc.getNpcId() == NPCID) {
			if (!상점.contains(npc)) {
				return;
			}
			상점.remove(npc);
			진행(false);
			남은시간(null);
			System.out.println("[이벤트상점] 종료 - 수렵");
			EventShopList.Odd(npc);
			노무르.deleteMe();
			시이니.deleteMe();
		}
	}

	public synchronized static void deleteItem() {
		for (L1PcInstance tempPc : L1World.getInstance().getAllPlayers()) {
			try {
				if (tempPc == null || tempPc.getNetConnection() == null)
					continue;
				if (tempPc.getInventory().checkItem(60177))
					tempPc.getInventory().consumeItem(60177,
							tempPc.getInventory().countItems(60177));
				if (tempPc.getInventory().checkItem(60180))
					tempPc.getInventory().consumeItem(60180,
							tempPc.getInventory().countItems(60180));
				if (tempPc.getInventory().checkItem(60179))
					tempPc.getInventory().consumeItem(60179,
							tempPc.getInventory().countItems(60179));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		Delete(60177);
		Delete(60180);
		Delete(60179);
	}

	public static void Delete(int itemid) {
		Connection con = null;
		PreparedStatement pstm = null;
		try {
			con = L1DatabaseFactory.getInstance().getConnection();
			pstm = con
					.prepareStatement("delete FROM character_items WHERE item_id=?");
			pstm.setInt(1, itemid);
			pstm.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			SQLUtil.close(pstm);
			SQLUtil.close(con);
		}
	}
}