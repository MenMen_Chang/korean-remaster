package l1j.server.GameSystem.EventShop;

import java.sql.Timestamp;
import java.util.ArrayList;

import l1j.server.server.model.Instance.L1NpcInstance;

public class EventShopList {

	private static EventShopList _instance;

	public static EventShopList getInstance() {
		if (_instance == null) {
			_instance = new EventShopList();
		}
		return _instance;
	}

	private static ArrayList<L1NpcInstance> AllEventShop = new ArrayList<L1NpcInstance>();

	public synchronized static ArrayList<L1NpcInstance> getEventShopNpc(
			int npcid) {
		ArrayList<L1NpcInstance> npc = null;
		if (npcid == 4500163) {
			npc = EventShop_신기한반지.getEventShopNpc();
		} else if (npcid == 4500164) {
			npc = EventShop_룸티스.getEventShopNpc();
		} else if (npcid == 4500165) {
			npc = EventShop_인나드릴.getEventShopNpc();
		} else if (npcid == 4500167) {
			npc = EventShop_드래곤.getEventShopNpc();
		} else if (npcid == 7000043) {
			npc = EventShop_신묘한.getEventShopNpc();
		} else if (npcid == 100031) {
			npc = EventShop_셸로브.getEventShopNpc();
		} else if (npcid == 100093) {
			npc = EventShop_판도라.getEventShopNpc();
		} else if (npcid == 100324) {
			npc = EventShop_수렵.getEventShopNpc();
		} else if (npcid == 100375) {
			npc = EventShop_드래곤의큐브.getEventShopNpc();
		} else if (npcid == 100672) {
			npc = EventShop_할로윈.getEventShopNpc();
		} else if (npcid == 100213) {
			npc = EventShop_아툰기사단.getEventShopNpc();
		} else if (npcid == 100396) {
			npc = EventShop_바칸스.getEventShopNpc();
		} else if (npcid == 100411) {
			npc = EventShop_신묘한삭제.getEventShopNpc();
		} else if (npcid == 100412) {
			npc = EventShop_음유성.getEventShopNpc();
		} else if (npcid == 100429) {
			npc = EventShop_싸이.getEventShopNpc();
		} else if (npcid == 100578) {
			npc = EventShop_다이노스.getEventShopNpc();
		} else if (npcid == 100577) {
			npc = EventShop_단테스.getEventShopNpc();
		} else if (npcid == 100645) {
			npc = EventShop_붉은기사단.getEventShopNpc();
		} else if (npcid == 100662) {
			npc = EventShop_붉은사자.getEventShopNpc();
		} else if (npcid == 100670) {
			npc = EventShop_스냅퍼의반지.getEventShopNpc();
		} else if (npcid == 100691) {
			npc = EventShop_크리스마스.getEventShopNpc();
		} else if (npcid == 100708) {
			npc = EventShop_아인_따스한_시선.getEventShopNpc();
		} else if (npcid == 100709) {
			npc = EventShop_10검.getEventShopNpc();
		} else if (npcid == 100764) {
			npc = EventShop_드래곤_2Day.getEventShopNpc();
		} else if (npcid == 100765) {
			npc = EventShop_단테스_2Day.getEventShopNpc();

		} else if (npcid == 100900) {
			npc = EventShop_오림.getEventShopNpc();

		} else if (npcid == 100883) {
			npc = EventShop_루피주먹.getEventShopNpc();

		} else if (npcid == 100766) {
			npc = EventShop_벚꽃.getEventShopNpc();

		} else if (npcid == 100881) {
			npc = EventShop_그렘린.getEventShopNpc();
		}
		return npc;
	}

	public synchronized static ArrayList<L1NpcInstance> getAllEventShop() {
		ArrayList<L1NpcInstance> npc = null;
		npc = AllEventShop;
		return npc;
	}

	public synchronized static void AddEventsTime(int id, long l) {
		if (id == 4500163) {
			EventShop_신기한반지.add남은시간(l);
		} else if (id == 4500164) {
			EventShop_룸티스.add남은시간(l);
		} else if (id == 4500165) {
			EventShop_인나드릴.add남은시간(l);
		} else if (id == 4500167) {
			EventShop_드래곤.add남은시간(l);
		} else if (id == 7000043) {
			EventShop_신묘한.add남은시간(l);
		} else if (id == 100031) {
			EventShop_셸로브.add남은시간(l);
		} else if (id == 100093) {
			EventShop_판도라.add남은시간(l);
		} else if (id == 100324) {
			EventShop_수렵.add남은시간(l);
		} else if (id == 100375) {
			EventShop_드래곤의큐브.add남은시간(l);
		} else if (id == 100672) {
			EventShop_할로윈.add남은시간(l);
		} else if (id == 100213) {
			EventShop_아툰기사단.add남은시간(l);
		} else if (id == 100396) {
			EventShop_바칸스.add남은시간(l);
		} else if (id == 100411) {
			EventShop_신묘한삭제.add남은시간(l);
		} else if (id == 100412) {
			EventShop_음유성.add남은시간(l);
		} else if (id == 100429) {
			EventShop_싸이.add남은시간(l);
		} else if (id == 100578) {
			EventShop_다이노스.add남은시간(l);
		} else if (id == 100577) {
			EventShop_단테스.add남은시간(l);
		} else if (id == 100645) {
			EventShop_붉은기사단.add남은시간(l);
		} else if (id == 100662) {
			EventShop_붉은사자.add남은시간(l);
		} else if (id == 100670) {
			EventShop_스냅퍼의반지.add남은시간(l);
		} else if (id == 100691) {
			EventShop_크리스마스.add남은시간(l);
		} else if (id == 100708) {
			EventShop_아인_따스한_시선.add남은시간(l);
		} else if (id == 100709) {
			EventShop_10검.add남은시간(l);
		} else if (id == 100764) {
			EventShop_드래곤_2Day.add남은시간(l);
		} else if (id == 100765) {
			EventShop_단테스_2Day.add남은시간(l);

		} else if (id == 100900) {
			EventShop_오림.add남은시간(l);

		} else if (id == 100883) {
			EventShop_루피주먹.add남은시간(l);

		} else if (id == 100766) {
			EventShop_벚꽃.add남은시간(l);

		} else if (id == 100881) {
			EventShop_그렘린.add남은시간(l);
		}
	}

	public synchronized static void AddEvents(L1NpcInstance npc, Timestamp time) {
		if (npc.getNpcId() == EventShop_신기한반지.NPCID) {
			EventShop_신기한반지.addEventShopNpc(npc, time);
		}
		if (npc.getNpcId() == EventShop_룸티스.NPCID) {
			EventShop_룸티스.addEventShopNpc(npc, time);
		}
		if (npc.getNpcId() == EventShop_인나드릴.NPCID) {
			EventShop_인나드릴.addEventShopNpc(npc, time);
		}
		if (npc.getNpcId() == EventShop_드래곤.NPCID) {
			EventShop_드래곤.addEventShopNpc(npc, time);
		}
		if (npc.getNpcId() == EventShop_신묘한.NPCID) {
			EventShop_신묘한.addEventShopNpc(npc, time);
		}
		if (npc.getNpcId() == EventShop_셸로브.NPCID) {
			EventShop_셸로브.addEventShopNpc(npc, time);
		}
		if (npc.getNpcId() == EventShop_판도라.NPCID) {
			EventShop_판도라.addEventShopNpc(npc, time);
		}
		if (npc.getNpcId() == EventShop_수렵.NPCID) {
			EventShop_수렵.addEventShopNpc(npc, time);
		}
		if (npc.getNpcId() == EventShop_드래곤의큐브.NPCID) {
			EventShop_드래곤의큐브.addEventShopNpc(npc, time);
		}
		if (npc.getNpcId() == EventShop_할로윈.NPCID) {
			EventShop_할로윈.addEventShopNpc(npc, time);
		}
		if (npc.getNpcId() == EventShop_아툰기사단.NPCID) {
			EventShop_아툰기사단.addEventShopNpc(npc, time);
		}
		if (npc.getNpcId() == EventShop_바칸스.NPCID) {
			EventShop_바칸스.addEventShopNpc(npc, time);
		}
		if (npc.getNpcId() == EventShop_신묘한삭제.NPCID) {
			EventShop_신묘한삭제.addEventShopNpc(npc, time);
		}
		if (npc.getNpcId() == EventShop_음유성.NPCID) {
			EventShop_음유성.addEventShopNpc(npc, time);
		}
		if (npc.getNpcId() == EventShop_싸이.NPCID) {
			EventShop_싸이.addEventShopNpc(npc, time);
		}
		if (npc.getNpcId() == EventShop_다이노스.NPCID) {
			EventShop_다이노스.addEventShopNpc(npc, time);
		}
		if (npc.getNpcId() == EventShop_단테스.NPCID) {
			EventShop_단테스.addEventShopNpc(npc, time);
		}
		if (npc.getNpcId() == EventShop_붉은기사단.NPCID) {
			EventShop_붉은기사단.addEventShopNpc(npc, time);
		}
		if (npc.getNpcId() == EventShop_붉은사자.NPCID) {
			EventShop_붉은사자.addEventShopNpc(npc, time);
		}
		if (npc.getNpcId() == EventShop_스냅퍼의반지.NPCID) {
			EventShop_스냅퍼의반지.addEventShopNpc(npc, time);
		}
		if (npc.getNpcId() == EventShop_크리스마스.NPCID) {
			EventShop_크리스마스.addEventShopNpc(npc, time);
		}
		if (npc.getNpcId() == EventShop_아인_따스한_시선.NPCID) {
			EventShop_아인_따스한_시선.addEventShopNpc(npc, time);
		}
		if (npc.getNpcId() == EventShop_10검.NPCID) {
			EventShop_10검.addEventShopNpc(npc, time);
		}
		if (npc.getNpcId() == EventShop_드래곤_2Day.NPCID) {
			EventShop_드래곤_2Day.addEventShopNpc(npc, time);
		}
		if (npc.getNpcId() == EventShop_단테스_2Day.NPCID) {
			EventShop_단테스_2Day.addEventShopNpc(npc, time);
		}

		if (npc.getNpcId() == EventShop_오림.NPCID) {
			EventShop_오림.addEventShopNpc(npc, time);
		}

		if (npc.getNpcId() == EventShop_루피주먹.NPCID) {
			EventShop_루피주먹.addEventShopNpc(npc, time);
		}
		if (npc.getNpcId() == EventShop_벚꽃.NPCID) {
			EventShop_벚꽃.addEventShopNpc(npc, time);
		}

		if (npc.getNpcId() == EventShop_그렘린.NPCID) {
			EventShop_그렘린.addEventShopNpc(npc, time);
		}
	}

	public synchronized static void OddEvents(L1NpcInstance npc) {
		if (npc.getNpcId() == EventShop_신기한반지.NPCID) {
			EventShop_신기한반지.oddEventShopNpc(npc);
		}
		if (npc.getNpcId() == EventShop_룸티스.NPCID) {
			EventShop_룸티스.oddEventShopNpc(npc);
		}
		if (npc.getNpcId() == EventShop_인나드릴.NPCID) {
			EventShop_인나드릴.oddEventShopNpc(npc);
		}
		if (npc.getNpcId() == EventShop_드래곤.NPCID) {
			EventShop_드래곤.oddEventShopNpc(npc);
		}
		if (npc.getNpcId() == EventShop_신묘한.NPCID) {
			EventShop_신묘한.oddEventShopNpc(npc);
		}
		if (npc.getNpcId() == EventShop_셸로브.NPCID) {
			EventShop_셸로브.oddEventShopNpc(npc);
		}
		if (npc.getNpcId() == EventShop_판도라.NPCID) {
			EventShop_판도라.oddEventShopNpc(npc);
		}
		if (npc.getNpcId() == EventShop_수렵.NPCID) {
			EventShop_수렵.oddEventShopNpc(npc);
		}
		if (npc.getNpcId() == EventShop_드래곤의큐브.NPCID) {
			EventShop_드래곤의큐브.oddEventShopNpc(npc);
		}
		if (npc.getNpcId() == EventShop_할로윈.NPCID) {
			EventShop_할로윈.oddEventShopNpc(npc);
		}
		if (npc.getNpcId() == EventShop_아툰기사단.NPCID) {
			EventShop_아툰기사단.oddEventShopNpc(npc);
		}
		if (npc.getNpcId() == EventShop_바칸스.NPCID) {
			EventShop_바칸스.oddEventShopNpc(npc);
		}
		if (npc.getNpcId() == EventShop_신묘한삭제.NPCID) {
			EventShop_신묘한삭제.oddEventShopNpc(npc);
		}
		if (npc.getNpcId() == EventShop_음유성.NPCID) {
			EventShop_음유성.oddEventShopNpc(npc);
		}
		if (npc.getNpcId() == EventShop_싸이.NPCID) {
			EventShop_싸이.oddEventShopNpc(npc);
		}
		if (npc.getNpcId() == EventShop_다이노스.NPCID) {
			EventShop_다이노스.oddEventShopNpc(npc);
		}
		if (npc.getNpcId() == EventShop_단테스.NPCID) {
			EventShop_단테스.oddEventShopNpc(npc);
		}
		if (npc.getNpcId() == EventShop_붉은기사단.NPCID) {
			EventShop_붉은기사단.oddEventShopNpc(npc);
		}
		if (npc.getNpcId() == EventShop_붉은사자.NPCID) {
			EventShop_붉은사자.oddEventShopNpc(npc);
		}
		if (npc.getNpcId() == EventShop_스냅퍼의반지.NPCID) {
			EventShop_스냅퍼의반지.oddEventShopNpc(npc);
		}
		if (npc.getNpcId() == EventShop_크리스마스.NPCID) {
			EventShop_크리스마스.oddEventShopNpc(npc);
		}
		if (npc.getNpcId() == EventShop_아인_따스한_시선.NPCID) {
			EventShop_아인_따스한_시선.oddEventShopNpc(npc);
		}
		if (npc.getNpcId() == EventShop_10검.NPCID) {
			EventShop_10검.oddEventShopNpc(npc);
		}
		if (npc.getNpcId() == EventShop_드래곤_2Day.NPCID) {
			EventShop_드래곤_2Day.oddEventShopNpc(npc);
		}
		if (npc.getNpcId() == EventShop_단테스_2Day.NPCID) {
			EventShop_단테스_2Day.oddEventShopNpc(npc);
		}

		if (npc.getNpcId() == EventShop_오림.NPCID) {
			EventShop_오림.oddEventShopNpc(npc);
		}

		if (npc.getNpcId() == EventShop_루피주먹.NPCID) {
			EventShop_루피주먹.oddEventShopNpc(npc);
		}
		if (npc.getNpcId() == EventShop_벚꽃.NPCID) {
			EventShop_벚꽃.oddEventShopNpc(npc);
		}

		if (npc.getNpcId() == EventShop_그렘린.NPCID) {
			EventShop_그렘린.oddEventShopNpc(npc);
		}
	}

	public synchronized static void Add(L1NpcInstance npc) {
		if (AllEventShop.contains(npc)) {
			return;
		}
		AllEventShop.add(npc);
	}

	public synchronized static void Odd(L1NpcInstance npc) {
		if (!AllEventShop.contains(npc)) {
			return;
		}
		AllEventShop.remove(npc);
	}
}